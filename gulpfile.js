var gulp = require("gulp"),
    fs = require("fs"),
    clean = require("gulp-clean"),
    zip = require("gulp-zip"),
    runSequence = require("run-sequence"),
    minifyCss = require("gulp-minify-css"),
    minifyHtml = require("gulp-minify-html"),
    browserify = require("browserify"),
    livereload = require("gulp-livereload"),
    source = require("vinyl-source-stream");

var paths = {
    scripts: {
        source: "./main.js",
        destination: "./",
        filename: "bundle.js",
        watch: "./main.js"
    }
}; //Source main file and output dir and filename

var mapPaths = {
    scripts: {
        source: "./mapMain.js",
        destination: "./",
        filename: "mapBundle.js",
        watch: "./mapMain.js"
    }
};

var getVersion = function() {
    info = require("./package.json");
    return info.version;
};

var getCopyright = function() {
    return fs.readFileSync("Copyright");
};

gulp.task("scripts", function() { //browserify task to compile scripts. Generates bundle.js
    var bundle = browserify({
        entries: [paths.scripts.source],
        debug: true
    });

    return bundle
        .bundle()
        .pipe(source(paths.scripts.filename))
        .pipe(gulp.dest(paths.scripts.destination))
        .pipe(livereload());
});

gulp.task("mapScripts", function() {
    var mapBundle = browserify({
        entries: [mapPaths.scripts.source],
        debug: true
    });

    return mapBundle
        .bundle()
        .pipe(source(mapPaths.scripts.filename))
        .pipe(gulp.dest(mapPaths.scripts.destination))
        .pipe(livereload());
});

gulp.task("watch", function() {
    livereload.listen();
    gulp.watch(paths.scripts.watch, ["scripts"]);
});

gulp.task("html", function() {
    // Minify HTML
    gulp
        .src("./*.html") // path to your files
        .pipe(minifyHtml())
        .pipe(gulp.dest("./dist"));
});

gulp.task("bundle", function() {
    // mapsbundle.js
    return gulp.src(["./bundle.js"]).pipe(gulp.dest("./dist"));
});

gulp.task("mapsBundle", function() {
    // bundle.js
    return gulp.src(["./mapBundle.js"]).pipe(gulp.dest("./dist"));
});

gulp.task("conf", function() {
    return gulp.src(["./conf/*.*"]).pipe(gulp.dest("./dist/conf"));
});

gulp.task("levelsJSON", function() {
    return gulp
        .src(["./src/levels/definitions/*.*"])
        .pipe(gulp.dest("./dist/src/levels/definitions"));
});

gulp.task("i18n", function() {
    return gulp.src(["./src/i18n/*.*"]).pipe(gulp.dest("./dist/src/i18n"));
});

gulp.task("css", function() {
    // CSS: Concatenate and Minify
    return gulp
        .src(["./assets/css/*.css"])
        .pipe(minifyCss())
        .pipe(gulp.dest("./dist/assets/css"));
});

gulp.task("blocks", function() {
    return gulp
        .src(["./assets/blocks/custom/*.*"])
        .pipe(gulp.dest("./dist/assets/blocks/custom"));
});

gulp.task("images", function() {
    return gulp
        .src(["./assets/images/*.*"])
        .pipe(gulp.dest("./dist/assets/images"));
});

gulp.task("readme", function() {
    // Readme
    return gulp.src(["./README.md"]).pipe(gulp.dest("./dist"));
});

gulp.task("clean", function() {
    return gulp.src("./dist", { read: false }).pipe(clean());
});

gulp.task("dist", function() {
    // Create a ZIP File
    return gulp
        .src(["./dist/**/*.*"])
        .pipe(zip("blockrain.zip"))
        .pipe(gulp.dest("./dist"));
});

gulp.task("build", function(callback) {
    runSequence(
        "clean",
        "html",
        "scripts",
        "mapScripts",
        "bundle",
        "mapsBundle",
        "conf",
        "levelsJSON",
        "i18n",
        "css",
        "blocks",
        "images",
        "readme",
        "dist",
        callback
    );
});