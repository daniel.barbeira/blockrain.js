var jsonLoader = require("./jsonLoader.js");
var linesSubLevel = require("./levels/linesSubLevel.js");
var pointsSubLevel = require("./levels/pointsSubLevel.js");
var scoreRegister = require("./scoreRegister");

var scoreRegisterInstance = new scoreRegister.scoreRegister();
var levelData;
var globalConfig = jsonLoader.loadJsonFile("conf/global_config.json");

function lightenDarkenColor(col, amt) {

    var usePound = false;

    if (col[0] == "#") {
        col = col.slice(1);
        usePound = true;
    }

    var num = parseInt(col, 16);

    var r = (num >> 16) + amt;

    if (r > 255) r = 255;
    else if (r < 0) r = 0;

    var b = ((num >> 8) & 0x00FF) + amt;

    if (b > 255) b = 255;
    else if (b < 0) b = 0;

    var g = (num & 0x0000FF) + amt;

    if (g > 255) g = 255;
    else if (g < 0) g = 0;

    return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);

}

function setLevelName(currentLevel) {
    $("#level-name").text($.i18n("level_".concat(currentLevel)));
}

function setBlockTextColor(blockTextColor) {
    var blockText = document.getElementById("block-text");
    var lightBlockTextColor = lightenDarkenColor(blockTextColor, 70); //30% brighter
    blockText.style.color = lightBlockTextColor;
}

function setBlockText(shapeType, currentLevel) {
    var blockText = $.i18n(shapeType.concat("_level_").concat(currentLevel));
    var blockTextPos;
    var arr = blockText.split("~");

    blockTextPos = getRandomInt(0, arr.length);

    if (typeof arr[blockTextPos] === "undefined") {
        blockTextPos = 0;
    }

    $("#block-text").text(arr[blockTextPos]);
}

function setSpecialBlockText(prefix, currentLevel) {
    var blockTexts = $.i18n(prefix.concat("_level_").concat(currentLevel));
    var specialBlockTextPos;
    var arr = blockTexts.split("~");

    specialBlockTextPos = getRandomInt(0, arr.length);

    if (typeof arr[specialBlockTextPos] === "undefined") {
        specialBlockTextPos = 0;
    }

    $("#block-text").text(arr[specialBlockTextPos]);
}

function hideBlocktext() {
    $("#block-text").hide();
}

function showBlocktext() {
    $("#block-text").toggle();
}

function getSubLevelText(level) {
    var levelText = $.i18n("description_level_".concat(level));
    var toret = "";
    var arr = levelText.split("~");
    for (index = 0; index < arr.length; ++index) {
        var str = arr[index].replace(/^/, "<li>").concat("</li>");
        toret += str;
    }
    toret.replace(/^/, "<ul>").concat("</ul>");
    return toret;
}

function showSubLevelDescription(currentLevel, levelText) {
    hideBlocktext();
    swal({
        title: "Nivel " + currentLevel + ": " + $.i18n("level_".concat(currentLevel)),
        html: levelText,
        allowOutsideClick: false,
        allowEscapeKey: false,
        timer: 6000,

        onOpen: function() {
            var b = swal.getConfirmButton();
            b.hidden = true;
            b.disabled = true;
        }
    }).then(function() {
        swal({
            title: "Nivel " + currentLevel + ": " + $.i18n("level_".concat(currentLevel)),
            html: levelText,
            allowOutsideClick: false,
            allowEscapeKey: false,
            animation: false
        }).then(value => {
            if (value) {
                $game.blockrain("resume");
                showBlocktext();
            }
        });
    });
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return (Math.floor(Math.random() * (max - min + 1)) + min);
}

function getRandomDecimal(min, max) {
    return (Math.floor(Math.random() * (max - min + 1)) + min) / 10;
}

function genRand(min, max, decimalPlaces) {
    var rand = Math.random() * (max - min) + min;
    var power = Math.pow(10, decimalPlaces);
    return Math.floor(rand * power) / power;
}

function saveSubLevelScore(level, subLevel, score) {
    var subLevelNum = subLevel.split("_")[1] - 1;
    scoreRegisterInstance.setSubLevelScore(level, subLevelNum, score);
}

function saveSubLevelLines(level, subLevel, numLines) {
    var subLevelNum = subLevel.split("_")[1] - 1;
    scoreRegisterInstance.setSubLevelLines(level, subLevelNum, numLines);
}

$(document).ready(function() {
    var levelScript = sessionStorage.getItem("levelFile");

    var levelsDefintionDir = globalConfig.levels_definition_dir;

    var levelPosLoaded = 0;

    levelScript = levelsDefintionDir + levelScript;

    levelData = jsonLoader.loadJsonFile(levelScript);

    var levelsParams = levelData.levels_params;

    var params = levelsParams[levelPosLoaded];

    var subLevel = new linesSubLevel.LinesSubLevel(params);

    var numLinesLevel = 0;

    var randNum;

    var specialBlockTextShown = false;

    $game = $("#tetris-ddhh").blockrain({
        speed: subLevel.getSpeed(),
        difficulty: subLevel.getDifficulty(),
        theme: subLevel.getTheme(),
        playText: $.i18n(globalConfig.play_text),
        playButtonText: $.i18n(globalConfig.play_button_text),
        gameOverText: $.i18n(globalConfig.game_over_text),
        restartButtonText: $.i18n(globalConfig.restart_button_text),
        scoreText: $.i18n(globalConfig.score_text),
        blockWidth: globalConfig.default_block_width,
        obstacles: subLevel.getObstacles(),
        numObstacles: subLevel.getNumObstacles(),
        pauseButton: subLevel.getShowPauseButton(),
        shapeTypeProbs: subLevel.getShapeTypeProbabilities(),

        onStart: function() {
            $game.blockrain("touchControls", true);
            $game.blockrain("pause");
            setLevelName(subLevel.getSubLevel());
            var levelText = getSubLevelText(subLevel.getSubLevel());
            showSubLevelDescription(subLevel.getSubLevel(), levelText);
        },

        onRestart: function() {
            $game.blockrain("touchControls", true);
            setLevelName(subLevel.getSubLevel());
            $game.blockrain("resume");
        },

        onLine: function(lines, scoreIncrement, score) {
            numLinesLevel += lines;
            if (
                subLevel.isLevelCompleted(numLinesLevel) &&
                !subLevel.isFinalLevel()
            ) {
                $game.blockrain("pause");
                $game.blockrain("levelOver");
                hideBlocktext();
                console.log("level passed");
                saveSubLevelScore(levelData.level, subLevel.getSubLevel(), score);
                saveSubLevelLines(levelData.level, subLevel.getSubLevel(), numLinesLevel);

                numLinesLevel = 0;
                levelPosLoaded++;

                params = levelsParams[levelPosLoaded];

                subLevel = new linesSubLevel.LinesSubLevel(params);

                swal({
                    title: $.i18n("level_won"),
                    type: "success",
                    text: $.i18n("next_level")
                }).then(value => {
                    if (value) {
                        // Restart the game
                        $game.blockrain("speed", subLevel.getSpeed());
                        $game.blockrain("difficulty", subLevel.getDifficulty());
                        $game.blockrain("obstacles", subLevel.getObstacles());
                        $game.blockrain("numObstacles", subLevel.getNumObstacles());
                        $game.blockrain("pauseButton", subLevel.getShowPauseButton());
                        $game.blockrain("theme", subLevel.getTheme());

                        $game.blockrain("start");
                    }
                });
            } else if (
                subLevel.isLevelCompleted(numLinesLevel) &&
                subLevel.isFinalLevel()
            ) {
                $game.blockrain("pause");
                $game.blockrain("levelOver");
                hideBlocktext();
                console.log("final level passed");
                saveSubLevelScore(levelData.level, subLevel.getSubLevel(), score);
                saveSubLevelLines(levelData.level, subLevel.getSubLevel(), numLinesLevel);

                swal({
                    type: "success",
                    text: $.i18n("game_won_".concat(levelData.level))
                }).then(value => {
                    if (value) {
                        if (levelData.next_level_file != "") {
                            var levelsUnlocked = JSON.parse(sessionStorage.getItem("levelsUnlocked"));
                            var levelUnlocked = {
                                code: levelData.next_level_code,
                                shown: false
                            };
                            levelsUnlocked.push(levelUnlocked);
                            sessionStorage.setItem("levelsUnlocked", JSON.stringify(levelsUnlocked));

                            var levelFiles = JSON.parse(sessionStorage.getItem("levelFiles"));
                            var nextLevelCode = levelData.next_level_code;
                            levelFiles[nextLevelCode] = levelData.next_level_file;
                            sessionStorage.setItem("levelFiles", JSON.stringify(levelFiles));

                            var colors = JSON.parse(sessionStorage.getItem("colors"));
                            colors[nextLevelCode] = globalConfig.unlocked_level_color;
                            sessionStorage.setItem("colors", JSON.stringify(colors));
                        }

                        location.replace("index.html");

                    }
                });
            }
        },

        onNext: function(next) {
            if (typeof next != "undefined" && typeof $game != "undefined") {

                if (specialBlockTextShown) {
                    $game.blockrain("theme", subLevel.getTheme());
                    setBlockTextColor(subLevel.getBlockTextColor());
                    specialBlockTextShown = false;
                }

                randNum = genRand(0, 1, 2);

                var specialPieces = subLevel.getSpecialPieces();
                var accumulatedProb = 0;

                if (typeof specialPieces != "undefined") {
                    for (var i = 0; i < specialPieces.length; i++) {
                        accumulatedProb += specialPieces[i].piece_probability;
                        if (randNum <= accumulatedProb) {
                            $game.blockrain('theme', {
                                background: '#000000',
                                backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAIAAAC0Ujn1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjZTg0NzU4MC00ODk3LTRkNjAtOWNhYi1mZTk1NzQ5NzhiNjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTEzOEQwMDc5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTEzOEQwMDY5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDplNDRjOWZiNC0yNzE5LTQ3NDYtYmRmMi0wMmY2ZTA4ZjAxMmUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzMwNTNEOTk5MDM1MTFFNDlBMzlFNzY4RjBCNkNENzMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Y01+zAAAAMklEQVR42mJgGAWjYBSMgkEJGIlUd+j/WjjbjjGYGC1MtHP10DR6FIyCUTAKBikACDAA0NoDCLGGjH8AAAAASUVORK5CYII=',
                                primary: specialPieces[i].color,
                                secondary: subLevel.getBlockTextColor(),
                                stroke: '#000000',
                                innerStroke: '#000000'
                            });
                            setBlockTextColor(specialPieces[i].color);
                            if (typeof specialPieces[i].piece_type === "number") {
                                var lev = levelData.level + "_" + specialPieces[i].piece_type;
                                setBlockText(next.shapeType, lev);
                            } else {
                                setSpecialBlockText(specialPieces[i].piece_type, subLevel.getSubLevel());
                            }
                            specialBlockTextShown = true;
                            break;
                        }
                    }
                }
                if (!specialBlockTextShown) {
                    setBlockTextColor(subLevel.getBlockTextColor());
                    setBlockText(next.shapeType, subLevel.getSubLevel());
                }
            }
        }
    });

    module.exports.updatePageElements = function updatePageElements() {
        $("#tetris-header").text($.i18n("tetris_header"));
        $("#game-instructions").text($.i18n("game_instructions"));
        $(".blockrain-start-msg").text($.i18n(globalConfig.play_text));
        $(".blockrain-start-btn").text($.i18n(globalConfig.play_button_text));
        $(".blockrain-score-msg").text($.i18n(globalConfig.score_text));
        $(".blockrain-game-over-msg").text($.i18n(globalConfig.game_over_text));
        $(".blockrain-game-over-btn").text(
            $.i18n(globalConfig.restart_button_text)
        );
        if (!$game.blockrain("isGamePaused")) {
            setLevelName(subLevel.getSubLevel());
        }
    };
});