var level = require("./level.js");

module.exports.PointsSubLevel = function pointsSubLevel(levelData) {
  var that = level.Level(levelData);

  that.isLevelCompleted = function(currentPoints) {
    if (currentPoints >= 2000) {
      return true;
    }
    return false;
  };

  return that;
};
