/**
 @constructor
 @abstract
 */
module.exports.Level = function level(levelData) {
    var that = {};

    if (this.constructor === level) {
        throw new Error("Can't instantiate abstract class!");
    }

    that.getSpeed = function() {
        return levelData.speed;
    };

    that.getDifficulty = function() {
        return levelData.difficulty;
    };

    that.getTheme = function() {
        return levelData.theme;
    };

    that.getBlockTextColor = function() {
        return levelData.block_text_color;
    }

    that.getSubLevel = function() {
        return levelData.sub_level;
    };

    that.getNumLines = function() {
        return levelData.num_lines;
    };

    /**
 @abstract
 */
    that.isLevelCompleted = function() {
        throw new Error("Abstract method!");
    };

    that.isFinalLevel = function() {
        return levelData.final_level;
    };

    that.getSpecialPieces = function() {
        return levelData.special_pieces;
    };

    that.getObstacles = function() {
        return levelData.obstacles;
    };

    that.getNumObstacles = function() {
        return levelData.num_obstacles;
    };

    that.getShowPauseButton = function() {
        return levelData.show_pause_button;
    };

    that.getShapeTypeProbabilities = function() {
        return levelData.shapeTypeProbabilities;
    };

    return that;
};