var level = require("./level.js");

module.exports.LinesSubLevel = function linesSubLevel(levelData) {
  var that = level.Level(levelData);

  that.isLevelCompleted = function(currentNumLines) {
    if (currentNumLines >= that.getNumLines()) {
      return true;
    }
    return false;
  };

  return that;
};
