module.exports.scoreRegister = function ScoreRegister() {
    var that = {};

    that.setSubLevelScore = function(level, subLevel, subLevelScore) {
        var scores, levelScores;
        if (sessionStorage["scores"] == null) {
            levelScores = [subLevelScore];
            scores = {}
            scores[level] = levelScores;
            sessionStorage.setItem("scores", JSON.stringify(scores));
        } else {
            scores = JSON.parse(sessionStorage.getItem("scores"));
            if (scores[level] === undefined) {
                levelScores = [subLevelScore];
                scores[level] = levelScores;
            } else {
                scores[level][subLevel] = subLevelScore;
            }
            sessionStorage.setItem("scores", JSON.stringify(scores));
        }
    };

    that.setSubLevelLines = function(level, subLevel, subLevelLines) {
        var lines, levelLines;
        if (sessionStorage["lines"] == null) {
            levelLines = [subLevelLines];
            lines = {}
            lines[level] = levelLines;
            sessionStorage.setItem("lines", JSON.stringify(lines));
        } else {
            lines = JSON.parse(sessionStorage.getItem("lines"));
            if (lines[level] === undefined) {
                levelLines = [subLevelLines];
                lines[level] = levelLines;
            } else {
                lines[level][subLevel] = subLevelLines;
            }
            sessionStorage.setItem("lines", JSON.stringify(lines));
        }
    };

    that.getLevelScore = function(level) {
        var scores = JSON.parse(sessionStorage.getItem("scores"));
        var levelScore = 0;
        for (var subLevelScore in scores[level]) {
            levelScore += scores[level][subLevelScore];
        }
        return levelScore;
    };

    that.getLevelLines = function(level) {
        var lines = JSON.parse(sessionStorage.getItem("lines"));
        var levelLines = 0;
        for (var subLevelLines in lines[level]) {
            levelLines += lines[level][subLevelLines];
        }
        return levelLines;
    };

    that.getTotalScore = function() {
        var scores = JSON.parse(sessionStorage.getItem("scores"));
        var scoresArr = Object.values(scores);
        var totalScore = 0;
        for (var levelScores in scoresArr) {
            for (var subLevelScore in scoresArr[levelScores]) {
                totalScore += scoresArr[levelScores][subLevelScore];
            }
        }
        return totalScore;
    };

    that.getTotalLines = function() {
        var lines = JSON.parse(sessionStorage.getItem("lines"));
        var linesArr = Object.values(lines);
        var totalLines = 0;
        for (var levelLines in linesArr) {
            for (var subLevelLines in linesArr[levelLines]) {
                totalLines += linesArr[levelLines][subLevelLines];
            }
        }
        return totalLines;
    };

    return that;

};