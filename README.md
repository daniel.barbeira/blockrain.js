# Blockrain.js (Tetris of human rights)

## General information

Tetris of human rights is a tetris game in HTML5 + Javascript (fully adapated for mobile devices!).
As mentioned in the description, it's my final degree project.
The official title of the project is *"Design and implementation of a videogame in the field of Human Rights based on free tools"*

As it's based on free tools, I decided to use the code avialable in the blockrain.js repository as a basis and adding my own code and HTML5 pages,
transform it into a fully featured online tetris game, adaptative and responsive to be also played on mobile devices.

**[Check out the demo](http://aerolab.github.io/blockrain.js/)**

![Blockrain Screenshot](http://aerolab.github.io/blockrain.js/assets/images/blockrain.png)

## Contextualization

Right. So what is the *"Tetris of human rights"*?

The main idea is the concept of gamification (usage of game principles and mechanics in non gaming contexts to encourage motivation and enhance learning). Specifically, the aim is to enhance learning of new concepts and moral values in the **field of human rights** adding sensitizing content.

The videogame uses color to differentiate basic services related to a human right. Text information is related to each type of piece and there are different levels of difficulty. The levels correspond to a country and the difficulty is established according to the state of each human right treated in that country. Each human right is a different sub-level in the country's level.

Each piece is associated to a different colour based on the human right:

* **Access to water**: Blue

* **Alimentation**: Green

* **Energy**: Yellow

* **ICT**: Orange

The shape of each piece determines the facility to assure a basic service related to each human right.

Obstacles have also being included, that can appear at the start of a sub-level and create an additional difficulty.

This project is executed in collaboration with the ONG [**Ingeniería sin fronteras**](https://www.isf.es/). Specifically, with the galician delegation [**Enxeñaría Sen Fronteiras Galicia**](https://galicia.isf.es/).

## Setup

Create any element (a div, article, figure, whatever you want). You can use any class, but we are using *.game* for this example.

```html
<div id="tetris-ddhh" class="game"></div>
```

Then you just include **jquery** and **blockrain** and setup the game with **$('#tetris-ddhh').blockrain()**. Adding the CSS file is *strongly recommended*, as it provides some important styles for the UI (but you can customize them as needed).


```html
<!-- The stylesheet should go in the <head>, or be included in your CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/blockrain.css">

<!-- jQuery and Blockrain.js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="blockrain.js"></script>
<script>
    $('#tetris-ddhh').blockrain();
</script>
```

For extra fun, you can **enable continuous autoplay**, so the game plays itself continuously:

```js
$('#tetris-ddhh').blockrain({ autoplay: true, autoplayRestart: true });
```

## Themes

Blockrain comes with many themes out-of-the-box, but you can create custom ones by **adding them to BlockrainThemes**. You have multiple settings and can even use **custom textures** (base64-encoded).

```js
'water': {
        background: '#000000', // The main background color.
        backgroundGrid: '#10101', // You can draw a small background grid as well.
        primary: null, // Color of the falling blocks. This overrides the standard block color.
        secondary: null,  // Color of the placed blocks. This overrides the standard block color.
        stroke: '#000000', // Border color for the blocks.
        innerStroke: '#000000', // A small border inside the blocks to give some texture.
        // The following are the colors of each piece
        blocks: {
            line: '#3399ff',
            square: '#3399ff',
            arrow: '#3399ff',
            rightHook: '#3399ff',
            leftHook: '#3399ff',
            rightZag: '#3399ff',
            leftZag: '#3399ff',
            dot: '#3399ff'
        }
    }
```

Here's an example of a retro theme (vim) with a **custom texture**:

```js
{
  background: '#000000',
  backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAA{AND SO ON}',
  primary: '#C2FFAE',
  secondary: '#C2FFAE',
  stroke: '#000000',
  strokeWidth: 3,
  innerStroke: null
}
```


### Creating custom designs

You can now use **completely custom designs for each block**! You just link to one image for each block when creating your theme and the plugin takes care of the rest. You can even use lists of images if you want BlockRain to pick a random design for each block.

**Keep in mind that the images need to follow the exact same format (rotation and width/height ratio) as [the blocks that are bundled with the custom theme](https://github.com/Aerolab/blockrain.js/tree/gh-pages/assets/blocks/custom)**.

Fun fact: Now you can rotate the square!


```js
'custom': {
  background: '#040304',
  backgroundGrid: '#000',
  complexBlocks: {
    line:     ['assets/blocks/custom/line.png', 'assets/blocks/custom/line.png'],
    square:   'assets/blocks/custom/square.png',
    arrow:    'assets/blocks/custom/arrow.png',
    rightHook:'assets/blocks/custom/rightHook.png',
    leftHook: 'assets/blocks/custom/leftHook.png',
    rightZag: 'assets/blocks/custom/rightZag.png',
    leftZag:  'assets/blocks/custom/leftZag.png'
  }
}
```


### Available themes:

* custom **NEW!**
* candy
* modern
* retro
* vim
* monochrome
* gameboy
* aerolab
* water
* alimentation
* energy
* ict
* mix

Remember you can create custom themes or modify these to better suit your design needs.


## Options

Blockrain comes with many options to help customize the game:

```js
{
  autoplay: false, // Let a bot play the game
  autoplayRestart: true, // Restart the game automatically once a bot loses
  showFieldOnStart: true, // Show a bunch of random blocks on the start screen (it looks nice)
  theme: null, // The theme name or a theme object
  blockWidth: 10, // How many blocks wide the field is (The standard is 10 blocks)
  autoBlockWidth: false, // The blockWidth is dinamically calculated based on the autoBlockSize. Disabled blockWidth. Useful for responsive backgrounds
  autoBlockSize: 24, // The max size of a block for autowidth mode
  difficulty: 'normal', // Difficulty (normal|nice|evil).
  speed: 20, // The speed of the game. The higher, the faster the pieces go.
  asdwKeys: true, // Enable ASDW keys.
  quickDrop: true, // Enable quick drop (double-pressing drop button for instant drop).
  doublePressTime: 500, // Sets the time window for double-pressing the drop.
  obstacles: 0, // Indicates if optional obstacles are activated or not.
  numObstacles: 0,// Number of obstacles rendered.
  pauseButton: true, // Pause button shown or not.
  shapeTypeProbs: [0.33, 0.33, 0.33], // Probabilities of and easy, normal or hard type of shape generated


  // Copy
  playText: 'Let\'s play some Tetris',
  playButtonText: 'Play',
  gameOverText: 'Game Over',
  restartButtonText: 'Play Again',
  scoreText: 'Score',

  // Basic Callbacks
  onStart: function(){},
  onRestart: function(){},
  onGameOver: function(score){},

// When a block is placed
  onPlaced: function() {},
  // When a line is made. Returns the number of lines, score assigned and total score
  onLine: function(lines, scoreIncrement, score){},
  // When next shape is generated. Returns the type of block
  onNext: function(next) {}
}
```

## Methods

There are a few utility methods available to control the game. $game represents your game selector (like *$('.game')*, for example).

```js
// Start the game
$game.blockrain('start');

// Restart the game
$game.blockrain('restart');

// Trigger a game over
$game.blockrain('gameover');

// Trigger level over (stops animation of pieces)
$game.blockrain('levelOver');
```


```js
// Pause
$game.blockrain('pause');

// Resume
$game.blockrain('resume');
```

```js
// Enable or Disable Autoplay (true|false)
$game.blockrain('autoplay', true);
```

```js
// Enable or Disable Controls (true|false)
$game.blockrain('controls', true);
// Enable or Disable Touch Controls (true|false)
$game.blockrain("touchControls", true);
```

```js
// Change game parameters

// Changes game speed
$game.blockrain("speed", 40);

// Change game difficulty (nice|normal|evil)
$game.blockrain("difficulty", 'evil');

// Sets the probabilities of the types of shapes [easy, normal, hard]
$game.blockrain("setShapeTypeProbabilites", [0.3, 0.3, 0.4]);

// Generate obstacle
$game.blockrain("obstacles", 1);

// Number of obstacles generated
$game.blockrain("numObstacles", 5);

// Show or hide pause button (true|false)
$game.blockrain("pauseButton", false);

```

```js
// Change the theme. 

// You can provide a theme name...
$game.blockrain('theme', 'vim');

// Or a theme object. **Check out src/blockrain.jquery.themes.js** for examples.
$game.blockrain('theme', {
  background: '#ffffff',
  primary: '#ff7b00',
  secondary: '#000000'
});
```

```js
// Return the current score
var score = $game.blockrain('score');

// Is the game paused or not
$game.blockrain("isGamePaused")
```


## Building Blockrain.js

This will generate the full minified source under /dist . It's basically License + libs + themes + src, concatenated and minified.

```gulp build```


## Credits

Blockrain.js is based on the [work by @mrcoles](http://mrcoles.com/tetris/), which is one of the best HTML5 tetris versions out there. The code was then modified and refactored to build a jQuery Plugin and added quite a few methods to easily add themes to it and make it simpler to implement.
