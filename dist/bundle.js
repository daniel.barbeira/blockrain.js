(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
var cldrp = require('./src/lib/CLDRPluralRuleParser/CLDRPluralRuleParser.js');

var jqueryi18n = require('./src/lib/jquery.i18n/jquery.i18n.js');

var messagestore = require('./src/lib/jquery.i18n/jquery.i18n.messagestore.js');

var fallbacks = require('./src/lib/jquery.i18n/jquery.i18n.fallbacks.js');

var language = require('./src/lib/jquery.i18n/jquery.i18n.language.js');

var parser = require('./src/lib/jquery.i18n/jquery.i18n.parser.js');

var emitter = require('./src/lib/jquery.i18n/jquery.i18n.emitter.js');

var emitterbidi = require('./src/lib/jquery.i18n/jquery.i18n.emitter.bidi.js');

var history = require('./src/lib/history/jquery.history.js');

var url = require('./src/lib/url/url.min.js');

var languagePicker = require('./src/languagePicker.js');

var jsonLoader = require('./src/jsonLoader.js');

var gamePresenter = require('./src/gamePresenter.js');

var blockrainlibs = require('./src/blockrain.jquery.libs.js');

var blockrain = require('./src/blockrain.jquery.src.js');

var blockrainthemes = require('./src/blockrain.jquery.themes.js');
},{"./src/blockrain.jquery.libs.js":2,"./src/blockrain.jquery.src.js":3,"./src/blockrain.jquery.themes.js":4,"./src/gamePresenter.js":5,"./src/jsonLoader.js":6,"./src/languagePicker.js":7,"./src/lib/CLDRPluralRuleParser/CLDRPluralRuleParser.js":11,"./src/lib/history/jquery.history.js":12,"./src/lib/jquery.i18n/jquery.i18n.emitter.bidi.js":13,"./src/lib/jquery.i18n/jquery.i18n.emitter.js":14,"./src/lib/jquery.i18n/jquery.i18n.fallbacks.js":15,"./src/lib/jquery.i18n/jquery.i18n.js":16,"./src/lib/jquery.i18n/jquery.i18n.language.js":17,"./src/lib/jquery.i18n/jquery.i18n.messagestore.js":18,"./src/lib/jquery.i18n/jquery.i18n.parser.js":19,"./src/lib/url/url.min.js":20}],2:[function(require,module,exports){
// jQuery Widget
(function(e){"function"==typeof define&&define.amd?define(["jquery"],e):e(jQuery)})(function(e){var t=0,i=Array.prototype.slice;e.cleanData=function(t){return function(i){var s,n,a;for(a=0;null!=(n=i[a]);a++)try{s=e._data(n,"events"),s&&s.remove&&e(n).triggerHandler("remove")}catch(o){}t(i)}}(e.cleanData),e.widget=function(t,i,s){var n,a,o,r,h={},l=t.split(".")[0];return t=t.split(".")[1],n=l+"-"+t,s||(s=i,i=e.Widget),e.expr[":"][n.toLowerCase()]=function(t){return!!e.data(t,n)},e[l]=e[l]||{},a=e[l][t],o=e[l][t]=function(e,t){return this._createWidget?(arguments.length&&this._createWidget(e,t),void 0):new o(e,t)},e.extend(o,a,{version:s.version,_proto:e.extend({},s),_childConstructors:[]}),r=new i,r.options=e.widget.extend({},r.options),e.each(s,function(t,s){return e.isFunction(s)?(h[t]=function(){var e=function(){return i.prototype[t].apply(this,arguments)},n=function(e){return i.prototype[t].apply(this,e)};return function(){var t,i=this._super,a=this._superApply;return this._super=e,this._superApply=n,t=s.apply(this,arguments),this._super=i,this._superApply=a,t}}(),void 0):(h[t]=s,void 0)}),o.prototype=e.widget.extend(r,{widgetEventPrefix:a?r.widgetEventPrefix||t:t},h,{constructor:o,namespace:l,widgetName:t,widgetFullName:n}),a?(e.each(a._childConstructors,function(t,i){var s=i.prototype;e.widget(s.namespace+"."+s.widgetName,o,i._proto)}),delete a._childConstructors):i._childConstructors.push(o),e.widget.bridge(t,o),o},e.widget.extend=function(t){for(var s,n,a=i.call(arguments,1),o=0,r=a.length;r>o;o++)for(s in a[o])n=a[o][s],a[o].hasOwnProperty(s)&&void 0!==n&&(t[s]=e.isPlainObject(n)?e.isPlainObject(t[s])?e.widget.extend({},t[s],n):e.widget.extend({},n):n);return t},e.widget.bridge=function(t,s){var n=s.prototype.widgetFullName||t;e.fn[t]=function(a){var o="string"==typeof a,r=i.call(arguments,1),h=this;return a=!o&&r.length?e.widget.extend.apply(null,[a].concat(r)):a,o?this.each(function(){var i,s=e.data(this,n);return"instance"===a?(h=s,!1):s?e.isFunction(s[a])&&"_"!==a.charAt(0)?(i=s[a].apply(s,r),i!==s&&void 0!==i?(h=i&&i.jquery?h.pushStack(i.get()):i,!1):void 0):e.error("no such method '"+a+"' for "+t+" widget instance"):e.error("cannot call methods on "+t+" prior to initialization; "+"attempted to call method '"+a+"'")}):this.each(function(){var t=e.data(this,n);t?(t.option(a||{}),t._init&&t._init()):e.data(this,n,new s(a,this))}),h}},e.Widget=function(){},e.Widget._childConstructors=[],e.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{disabled:!1,create:null},_createWidget:function(i,s){s=e(s||this.defaultElement||this)[0],this.element=e(s),this.uuid=t++,this.eventNamespace="."+this.widgetName+this.uuid,this.bindings=e(),this.hoverable=e(),this.focusable=e(),s!==this&&(e.data(s,this.widgetFullName,this),this._on(!0,this.element,{remove:function(e){e.target===s&&this.destroy()}}),this.document=e(s.style?s.ownerDocument:s.document||s),this.window=e(this.document[0].defaultView||this.document[0].parentWindow)),this.options=e.widget.extend({},this.options,this._getCreateOptions(),i),this._create(),this._trigger("create",null,this._getCreateEventData()),this._init()},_getCreateOptions:e.noop,_getCreateEventData:e.noop,_create:e.noop,_init:e.noop,destroy:function(){this._destroy(),this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(e.camelCase(this.widgetFullName)),this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName+"-disabled "+"ui-state-disabled"),this.bindings.unbind(this.eventNamespace),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")},_destroy:e.noop,widget:function(){return this.element},option:function(t,i){var s,n,a,o=t;if(0===arguments.length)return e.widget.extend({},this.options);if("string"==typeof t)if(o={},s=t.split("."),t=s.shift(),s.length){for(n=o[t]=e.widget.extend({},this.options[t]),a=0;s.length-1>a;a++)n[s[a]]=n[s[a]]||{},n=n[s[a]];if(t=s.pop(),1===arguments.length)return void 0===n[t]?null:n[t];n[t]=i}else{if(1===arguments.length)return void 0===this.options[t]?null:this.options[t];o[t]=i}return this._setOptions(o),this},_setOptions:function(e){var t;for(t in e)this._setOption(t,e[t]);return this},_setOption:function(e,t){return this.options[e]=t,"disabled"===e&&(this.widget().toggleClass(this.widgetFullName+"-disabled",!!t),t&&(this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus"))),this},enable:function(){return this._setOptions({disabled:!1})},disable:function(){return this._setOptions({disabled:!0})},_on:function(t,i,s){var n,a=this;"boolean"!=typeof t&&(s=i,i=t,t=!1),s?(i=n=e(i),this.bindings=this.bindings.add(i)):(s=i,i=this.element,n=this.widget()),e.each(s,function(s,o){function r(){return t||a.options.disabled!==!0&&!e(this).hasClass("ui-state-disabled")?("string"==typeof o?a[o]:o).apply(a,arguments):void 0}"string"!=typeof o&&(r.guid=o.guid=o.guid||r.guid||e.guid++);var h=s.match(/^([\w:-]*)\s*(.*)$/),l=h[1]+a.eventNamespace,u=h[2];u?n.delegate(u,l,r):i.bind(l,r)})},_off:function(t,i){i=(i||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace,t.unbind(i).undelegate(i),this.bindings=e(this.bindings.not(t).get()),this.focusable=e(this.focusable.not(t).get()),this.hoverable=e(this.hoverable.not(t).get())},_delay:function(e,t){function i(){return("string"==typeof e?s[e]:e).apply(s,arguments)}var s=this;return setTimeout(i,t||0)},_hoverable:function(t){this.hoverable=this.hoverable.add(t),this._on(t,{mouseenter:function(t){e(t.currentTarget).addClass("ui-state-hover")},mouseleave:function(t){e(t.currentTarget).removeClass("ui-state-hover")}})},_focusable:function(t){this.focusable=this.focusable.add(t),this._on(t,{focusin:function(t){e(t.currentTarget).addClass("ui-state-focus")},focusout:function(t){e(t.currentTarget).removeClass("ui-state-focus")}})},_trigger:function(t,i,s){var n,a,o=this.options[t];if(s=s||{},i=e.Event(i),i.type=(t===this.widgetEventPrefix?t:this.widgetEventPrefix+t).toLowerCase(),i.target=this.element[0],a=i.originalEvent)for(n in a)n in i||(i[n]=a[n]);return this.element.trigger(i,s),!(e.isFunction(o)&&o.apply(this.element[0],[i].concat(s))===!1||i.isDefaultPrevented())}},e.each({show:"fadeIn",hide:"fadeOut"},function(t,i){e.Widget.prototype["_"+t]=function(s,n,a){"string"==typeof n&&(n={effect:n});var o,r=n?n===!0||"number"==typeof n?i:n.effect||i:t;n=n||{},"number"==typeof n&&(n={duration:n}),o=!e.isEmptyObject(n),n.complete=a,n.delay&&s.delay(n.delay),o&&e.effects&&e.effects.effect[r]?s[t](n):r!==t&&s[r]?s[r](n.duration,n.easing,a):s.queue(function(i){e(this)[t](),a&&a.call(s[0]),i()})}}),e.widget});
},{}],3:[function(require,module,exports){
((function($) {

    "use strict";

    $.widget('aerolab.blockrain', {

        options: {
            autoplay: false, // Let a bot play the game
            autoplayRestart: true, // Restart the game automatically once a bot loses
            showFieldOnStart: true, // Show a bunch of random blocks on the start screen (it looks nice)
            theme: null, // The theme name or a theme object
            blockWidth: 10, // How many blocks wide the field is (The standard is 10 blocks)
            autoBlockWidth: false, // The blockWidth is dinamically calculated based on the autoBlockSize. Disabled blockWidth. Useful for responsive backgrounds
            autoBlockSize: 24, // The max size of a block for autowidth mode
            difficulty: 'normal', // Difficulty (normal|nice|evil).
            speed: 20, // The speed of the game. The higher, the faster the pieces go.
            asdwKeys: true, // Enable ASDW keys
            quickDrop: true, // Enable quick drop (double-pressing drop button for instant drop)
            doublePressTime: 500, // Sets the time window for double-pressing the drop
            obstacles: 0, // Indicates if optional obstacles are activated or not
            numObstacles: 0,
            pauseButton: true,
            shapeTypeProbs: [0.33, 0.33, 0.33], // Probabilities of and easy, normal or hard type of shape generated

            // Copy
            playText: 'Let\'s play some Tetris',
            playButtonText: 'Play',
            gameOverText: 'Game Over',
            restartButtonText: 'Play Again',
            scoreText: 'Score',

            // Basic Callbacks
            onStart: function() {},
            onRestart: function() {},
            onGameOver: function(score) {},

            // When a block is placed
            onPlaced: function() {},
            // When a line is made. Returns the number of lines, score assigned and total score
            onLine: function(lines, scoreIncrement, score) {},
            // When next shape is generated. Returns the type of block
            onNext: function(next) {}
        },


        /**
         * Start/Restart Game
         */
        start: function() {
            this._doStart();
            this.options.onStart.call(this.element);
        },

        restart: function() {
            this._doStart();
            this.options.onRestart.call(this.element);
        },

        pauseResume: function() {
            var pauseButton = document.getElementById("pauseButton");
            if (this.isGamePaused()) {
                console.log("game resumed!");
                pauseButton.style.borderStyle = "double";
                pauseButton.style.borderWidth = "0px 0 0px 15px";
                this.resume();
            } else {
                console.log("game paused!");
                pauseButton.style.borderStyle = "solid";
                pauseButton.style.borderWidth = "8px 0 8px 15px";
                this.pause();
            }
        },

        gameover: function() {
            this.showGameOverMessage();
            this._board.gameover = true;
            this.options.onGameOver.call(this.element, this._filled.score);
        },

        levelOver: function() {
            this._board.levelOver = true;
        },

        _doStart: function() {
            this._filled.clearAll();
            this._filled._resetScore();

            if (this.options.obstacles == 1) {
                console.log("Obstacles activated");
                this._board.createObstacles();
            }

            if (this.options.pauseButton) {
                this._$pause.show();
            } else {
                this._$pause.hide();
            }

            this._board.cur = this._board.nextShape();
            this._board.started = true;
            this._board.gameover = false;
            this._board.levelOver = false;
            this._board.dropDelay = 5;
            this._board.render(true);

            this._board.animate();


            this._$start.fadeOut(150);
            this._$gameover.fadeOut(150);
            this._$score.fadeIn(150);
        },


        pause: function() {
            this._board.paused = true;
        },

        resume: function() {
            this._board.paused = false;
        },

        autoplay: function(enable) {
            if (typeof enable !== 'boolean') {
                enable = true;
            }

            // On autoplay, start the game right away
            this.options.autoplay = enable;
            if (enable && !this._board.started) {
                this._doStart();
            }
            this._setupControls(!enable);
            this._setupTouchControls(!enable);
        },

        controls: function(enable) {
            if (typeof enable !== 'boolean') {
                enable = true;
            }
            this._setupControls(enable);
        },

        touchControls: function(enable) {
            if (typeof enable !== 'boolean') {
                enable = true;
            }
            if (this.isMobileDevice()) {
                this._setupTouchControls(enable);
            }
        },

        score: function(newScore) {
            if (typeof newScore !== 'undefined' && parseInt(newScore) >= 0) {
                this._filled.score = parseInt(newScore);
                this._$scoreText.text(this._filled_score);
            }
            return this._filled.score;
        },

        freesquares: function() {
            return this._filled.getFreeSpaces();
        },

        showStartMessage: function() {
            this._$start.show();
        },

        showGameOverMessage: function() {
            this._$gameover.show();
        },

        /**
         * Update the sizes of the renderer (this makes the game responsive)
         */
        updateSizes: function() {

            this._PIXEL_WIDTH = this.element.innerWidth();
            this._PIXEL_HEIGHT = this.element.innerHeight();

            this._BLOCK_WIDTH = this.options.blockWidth;
            this._BLOCK_HEIGHT = Math.floor(this.element.innerHeight() / this.element.innerWidth() * this._BLOCK_WIDTH);

            this._block_size = Math.floor(this._PIXEL_WIDTH / this._BLOCK_WIDTH);
            this._border_width = 2;

            // Recalculate the pixel width and height so the canvas always has the best possible size
            this._PIXEL_WIDTH = this._block_size * this._BLOCK_WIDTH;
            this._PIXEL_HEIGHT = this._block_size * this._BLOCK_HEIGHT;

            this._$canvas.attr('width', this._PIXEL_WIDTH)
                .attr('height', this._PIXEL_HEIGHT);
        },


        theme: function(newTheme) {

            if (typeof newTheme === 'undefined') {
                return this.options.theme || this._theme;
            }

            // Setup the theme properly
            if (typeof newTheme === 'string') {
                this.options.theme = newTheme;
                this._theme = $.extend(true, {}, BlockrainThemes[newTheme]);
            } else {
                this.options.theme = null;
                this._theme = newTheme;
            }

            if (typeof this._theme === 'undefined' || this._theme === null) {
                this._theme = $.extend(true, {}, BlockrainThemes['retro']);
                this.options.theme = 'retro';
            }

            if (isNaN(parseInt(this._theme.strokeWidth)) || typeof parseInt(this._theme.strokeWidth) !== 'number') {
                this._theme.strokeWidth = 2;
            }

            // Load the image assets
            this._preloadThemeAssets();

            if (this._board !== null) {
                if (typeof this._theme.background === 'string') {
                    this._$canvas.css('background-color', this._theme.background);
                }
                this._board.render();
            }
        },

        speed: function(newSpeed) {
            this.options.speed = newSpeed;
        },

        difficulty: function(newDifficulty) {
            var game = this;
            this.options.difficulty = newDifficulty;
            this._info.setMode(newDifficulty);

        },

        isGamePaused: function() {
            return this._board.paused;
        },

        obstacles: function(obstacles) {
            this.options.obstacles = obstacles;
        },

        numObstacles: function(numObstacles) {
            this.options.numObstacles = numObstacles;
        },

        pauseButton: function(showPauseButton) {
            this.options.pauseButton = showPauseButton;
        },

        setShapeTypeProbabilites: function(shapeTypeProbabilities) {
            if (typeof Array.isArray(shapeTypeProbabilities)) {
                if (shapeTypeProbabilities.length == this.options.shapeTypeProbs.length)
                    this.options.shapeTypeProbs = shapeTypeProbabilities;
            }
        },


        // Theme
        _theme: {

        },


        // UI Elements
        _$game: null,
        _$canvas: null,
        _$gameholder: null,
        _$start: null,
        _$gameover: null,
        _$score: null,
        _$scoreText: null,


        // Canvas
        _canvas: null,
        _ctx: null,

        //shapeFactories
        _shapeFactories: null,


        // Initialization
        _create: function() {

            var game = this;

            this.theme(this.options.theme);

            this._createHolder();
            this._createUI();

            this._refreshBlockSizes();

            this.updateSizes();

            $(window).resize(function() {
                //game.updateSizes();
            });

            this._SetupShapeFactories();
            this._SetupFilled();
            this._SetupInfo();
            this._SetupBoard();

            this._info.init();
            this._board.init();

            var renderLoop = function() {
                requestAnimationFrame(renderLoop);
                game._board.render();
            };
            renderLoop();

            if (this.options.autoplay) {
                this.autoplay(true);
                this._setupTouchControls(false);
            } else {
                this._setupControls(true);
                this._setupTouchControls(false);
            }

        },

        _checkCollisions: function(x, y, blocks, checkDownOnly) {
            // x & y should be aspirational values
            var i = 0,
                len = blocks.length,
                a, b;
            for (; i < len; i += 2) {
                a = x + blocks[i];
                b = y + blocks[i + 1];

                if (b >= this._BLOCK_HEIGHT || this._filled.check(a, b)) {
                    return true;
                } else if (!checkDownOnly && a < 0 || a >= this._BLOCK_WIDTH) {
                    return true;
                }
            }
            return false;
        },

        _selectRandShapeFactory: function() {
            var randProbability = this._genRand(0, 1, 2);
            var shapeFactory, accumulatedProbability = 0;

            for (var prob in this.options.shapeTypeProbs) {
                accumulatedProbability += this.options.shapeTypeProbs[prob];
                if (randProbability <= accumulatedProbability) {
                    shapeFactory = this._shapeFactories[prob];
                    break;
                }
            }

            return shapeFactory;
        },

        _genRand: function(min, max, decimalPlaces) {
            var rand = Math.random() * (max - min) + min;
            var power = Math.pow(10, decimalPlaces);
            return Math.floor(rand * power) / power;
        },


        _board: null,
        _info: null,
        _filled: null,


        /**
         * Draws the background
         */
        _drawBackground: function() {

            if (typeof this._theme.background !== 'string') {
                return;
            }

            if (this._theme.backgroundGrid instanceof Image) {

                // Not loaded
                if (this._theme.backgroundGrid.width === 0 || this._theme.backgroundGrid.height === 0) {
                    return;
                }

                this._ctx.globalAlpha = 1.0;

                for (var x = 0; x < this._BLOCK_WIDTH; x++) {
                    for (var y = 0; y < this._BLOCK_HEIGHT; y++) {
                        var cx = x * this._block_size;
                        var cy = y * this._block_size;

                        this._ctx.drawImage(this._theme.backgroundGrid,
                            0, 0, this._theme.backgroundGrid.width, this._theme.backgroundGrid.height,
                            cx, cy, this._block_size, this._block_size);
                    }
                }

            } else if (typeof this._theme.backgroundGrid === 'string') {

                var borderWidth = this._theme.strokeWidth;
                var borderDistance = Math.round(this._block_size * 0.23);
                var squareDistance = Math.round(this._block_size * 0.30);

                this._ctx.globalAlpha = 1.0;
                this._ctx.fillStyle = this._theme.backgroundGrid;

                for (var x = 0; x < this._BLOCK_WIDTH; x++) {
                    for (var y = 0; y < this._BLOCK_HEIGHT; y++) {
                        var cx = x * this._block_size;
                        var cy = y * this._block_size;

                        this._ctx.fillRect(cx + borderWidth, cy + borderWidth, this._block_size - borderWidth * 2, this._block_size - borderWidth * 2);
                    }
                }

            }

            this._ctx.globalAlpha = 1.0;
        },


        /**
         * Shapes
         */
        _easyShapeFactory: null,
        _normalShapeFactory: null,
        _hardShapeFactory: null,

        _shapes: {
            /**
             * The shapes have a reference point (the dot) and always rotate left.
             * Keep in mind that the blocks should keep in the same relative position when rotating,
             * to allow for custom per-block themes.
             */
            /*            
             *   X      
             *   O  XOXX
             *   X      
             *   X
             *   .   .      
             */
            line: [
                [0, -1, 0, -2, 0, -3, 0, -4],
                [2, -2, 1, -2, 0, -2, -1, -2],
                [0, -4, 0, -3, 0, -2, 0, -1],
                [-1, -2, 0, -2, 1, -2, 2, -2]
            ],
            /*
             *  XX
             *  XX
             */
            square: [
                [0, 0, 1, 0, 0, -1, 1, -1],
                [1, 0, 1, -1, 0, 0, 0, -1],
                [1, -1, 0, -1, 1, 0, 0, 0],
                [0, -1, 0, 0, 1, -1, 1, 0]
            ],
            /*
             *    X   X       X
             *   XOX XO  XOX  OX
             *   .   .X  .X  .X
             */
            arrow: [
                [0, -1, 1, -1, 2, -1, 1, -2],
                [1, 0, 1, -1, 1, -2, 0, -1],
                [2, -1, 1, -1, 0, -1, 1, 0],
                [1, -2, 1, -1, 1, 0, 2, -1]
            ],
            /*
             *    X    X XX 
             *    O  XOX  O XOX 
             *   .XX .   .X X   
             */
            rightHook: [
                [2, 0, 1, 0, 1, -1, 1, -2],
                [2, -2, 2, -1, 1, -1, 0, -1],
                [0, -2, 1, -2, 1, -1, 1, 0],
                [0, 0, 0, -1, 1, -1, 2, -1]
            ],
            /*
             *    X      XX X  
             *    O XOX  O  XOX
             *   XX . X .X  .  
             */
            leftHook: [
                [0, 0, 1, 0, 1, -1, 1, -2],
                [2, 0, 2, -1, 1, -1, 0, -1],
                [2, -2, 1, -2, 1, -1, 1, 0],
                [0, -2, 0, -1, 1, -1, 2, -1]
            ],
            /*
             *    X  XX 
             *   XO   OX
             *   X   .  
             */
            leftZag: [
                [0, 0, 0, -1, 1, -1, 1, -2],
                [2, -1, 1, -1, 1, -2, 0, -2],
                [1, -2, 1, -1, 0, -1, 0, 0],
                [0, -2, 1, -2, 1, -1, 2, -1]
            ],
            /*
             *   X    
             *   XO   OX
             *   .X  XX   
             */
            rightZag: [
                [1, 0, 1, -1, 0, -1, 0, -2],
                [2, -1, 1, -1, 1, 0, 0, 0],
                [0, -2, 0, -1, 1, -1, 1, 0],
                [0, 0, 1, 0, 1, -1, 2, -1]
            ],
            /*
             *  
             *   O   O   O   O
             *   
             */
            dot: [
                [0, 0],
                [0, 0],
                [0, 0],
                [0, 0]
            ],
            /*
             *   X
             *   O   XO
             *   
             */
            doubleDot: [
                [0, -1, 0, -2],
                [-1, -2, 0, -2],
                [0, -2, 0, -1],
                [0, -2, -1, -2]
            ],
            /*
             *   X
             *   O   XOX
             *   X
             */
            tripleDot: [
                [0, -1, 0, -2, 0, -3],
                [-1, -2, 0, -2, 1, -2],
                [0, -3, 0, -2, 0, -1],
                [1, -2, 0, -2, -1, -2]
            ],
            /*            
             *    X      
             *   X0X  
             *    X    
             *       
             */
            emptyCross: [
                [0, 0, 1, -1, 0, -2, -1, -1],
                [0, 0, 1, -1, 0, -2, -1, -1],
                [0, 0, 1, -1, 0, -2, -1, -1],
                [0, 0, 1, -1, 0, -2, -1, -1]
            ],
            /*
             *  X X     X
             *   O       OX
             *   X      X  
             * 
             */
            tHook: [
                [-1, 2, -1, 1, 0, 0, -2, 0],
                [0, 1, -1, 1, -2, 0, -2, 2],
                [-1, 0, -1, 1, 0, 2, -2, 2],
                [-2, 1, -1, 1, 0, 0, 0, 2]
            ],
            /*
             *     X    X
             *    X      X
             *   X        X
             *  X          X
             */
            diagonal: [
                [1, 1, 0, 0, -1, -1, -2, -2],
                [-1, 1, 0, 0, 1, -1, 2, -2],
                [1, 1, 0, 0, -1, -1, -2, -2],
                [-1, 1, 0, 0, 1, -1, 2, -2]
            ],
            /*            
             *    X      
             *   XXX  
             *    X    
             *       
             */
            fullCross: [
                [0, 0, 1, -1, 0, -1, 0, -2, -1, -1],
                [0, 0, 1, -1, 0, -1, 0, -2, -1, -1],
                [0, 0, 1, -1, 0, -1, 0, -2, -1, -1],
                [0, 0, 1, -1, 0, -1, 0, -2, -1, -1]
            ],
            /*            
             *   X      
             *   O  XOXXX
             *   X      
             *   X
             *   X   .      
             */
            longLine: [
                [0, -1, 0, -2, 0, -3, 0, -4, 0, -5],
                [2, -2, 1, -2, 0, -2, -1, -2, -2, -2],
                [0, -4, 0, -3, 0, -2, 0, -1, 0, 0],
                [-2, -2, -1, -2, 0, -2, 1, -2, 2, -2]
            ],
            /*
             *   XX      XX X X 
             *    O XOX  O  XOX
             *   XX X X .XX  .  
             */
            doubleHook: [
                [0, 0, 1, 0, 1, -1, 1, -2, 0, -2],
                [2, 0, 2, -1, 1, -1, 0, -1, 0, 0],
                [2, -2, 1, -2, 1, -1, 1, 0, 2, 0],
                [0, -2, 0, -1, 1, -1, 2, -1, 2, -2]
            ]
        },

        _SetupShapeFactories: function() {
            var game = this;

            if (this._easyShapeFactory !== null || this._normalShapeFactory !== null || this._hardShapeFactory !== null) {
                return;
            }

            function Shape(game, orientations, symmetrical, blockType, shapeType) {

                $.extend(this, {
                    x: 0,
                    y: 0,
                    symmetrical: symmetrical,
                    init: function() {
                        $.extend(this, {
                            orientation: 0,
                            x: Math.floor(game._BLOCK_WIDTH / 2) - 1,
                            y: -1
                        });
                        return this;
                    },

                    blockType: blockType,
                    shapeType: shapeType,
                    blockVariation: null,
                    blocksLen: orientations[0].length,
                    orientations: orientations,
                    orientation: 0, // 4 possible

                    rotate: function(direction) {
                        var orientation =
                            (this.orientation +
                                (direction === "left" ? 1 : -1) +
                                4) %
                            4;

                        if (!game._checkCollisions(
                                this.x,
                                this.y,
                                this.getBlocks(orientation)
                            )) {
                            this.orientation = orientation;
                            game._board.renderChanged = true;
                        } else {
                            var ogOrientation = this.orientation;
                            var ogX = this.x;
                            var ogY = this.y;

                            this.orientation = orientation;

                            while (this.x >= game._BLOCK_WIDTH - 2) {
                                this.x--;
                            }
                            while (this.x < 0) {
                                this.x++;
                            }

                            if (
                                this.blockType === "line" &&
                                this.x === 0
                            )
                                this.x++;

                            if (
                                game._checkCollisions(
                                    this.x,
                                    this.y,
                                    this.getBlocks(orientation)
                                )
                            ) {
                                this.y--;
                                if (
                                    game._checkCollisions(
                                        this.x,
                                        this.y,
                                        this.getBlocks(orientation)
                                    )
                                ) {
                                    this.x = ogX;
                                    this.y = ogY;
                                    this.orientation = ogOrientation;
                                }
                            }
                            game._board.renderChanged = true;
                        }
                    },

                    moveRight: function() {
                        if (!game._checkCollisions(
                                this.x + 1,
                                this.y,
                                this.getBlocks()
                            )) {
                            this.x++;
                            game._board.renderChanged = true;
                        }
                    },
                    moveLeft: function() {
                        if (!game._checkCollisions(
                                this.x - 1,
                                this.y,
                                this.getBlocks()
                            )) {
                            this.x--;
                            game._board.renderChanged = true;
                        }
                    },
                    drop: function() {
                        if (!game._checkCollisions(
                                this.x,
                                this.y + 1,
                                this.getBlocks()
                            )) {
                            this.y++;
                            // Reset the drop count, as we dropped the block sooner
                            game._board.dropCount = -1;
                            game._board.animate();
                            game._board.renderChanged = true;
                        }
                    },
                    quickDrop: function() {
                        for (
                            var i = this.y; i < game._BLOCK_HEIGHT; i++
                        ) {
                            if (!game._checkCollisions(
                                    this.x,
                                    this.y + 1,
                                    this.getBlocks()
                                )) {
                                this.y = i;
                            }
                        }
                        game._board.renderChanged = true;
                        game._board.dropFirstPress = true;
                    },

                    getBlocks: function(orientation) {
                        // optional param
                        return this.orientations[
                            orientation !== undefined ?
                            orientation :
                            this.orientation
                        ];
                    },
                    draw: function(_x, _y, _orientation) {
                        var blocks = this.getBlocks(_orientation),
                            x = _x === undefined ? this.x : _x,
                            y = _y === undefined ? this.y : _y,
                            i = 0,
                            index = 0;

                        for (; i < this.blocksLen; i += 2) {
                            game._board.drawBlock(
                                x + blocks[i],
                                y + blocks[i + 1],
                                this.blockType,
                                this.blockVariation,
                                index,
                                this.orientation,
                                true
                            );
                            index++;
                        }
                    },
                    drawObstacle: function(x, y, _orientation) {
                        var blocks = this.getBlocks(_orientation),
                            i = 0,
                            index = 0;

                        this.x = x;
                        this.y = y;

                        for (; i < this.blocksLen; i += 2) {
                            game._board.drawBlock(
                                x + blocks[i],
                                y + blocks[i + 1],
                                this.blockType,
                                this.blockVariation,
                                index,
                                this.orientation,
                                false
                            );
                            index++;
                        }
                    },

                    getBounds: function(_blocks) {
                        // _blocks can be an array of blocks, an orientation index, or undefined
                        var blocks = $.isArray(_blocks) ?
                            _blocks :
                            this.getBlocks(_blocks),
                            i = 0,
                            len = blocks.length,
                            minx = 999,
                            maxx = -999,
                            miny = 999,
                            maxy = -999;
                        for (; i < len; i += 2) {
                            if (blocks[i] < minx) {
                                minx = blocks[i];
                            }
                            if (blocks[i] > maxx) {
                                maxx = blocks[i];
                            }
                            if (blocks[i + 1] < miny) {
                                miny = blocks[i + 1];
                            }
                            if (blocks[i + 1] > maxy) {
                                maxy = blocks[i + 1];
                            }
                        }
                        return {
                            left: minx,
                            right: maxx,
                            top: miny,
                            bottom: maxy,
                            width: maxx - minx,
                            height: maxy - miny
                        };
                    }
                });

                return this.init();
            };

            this._easyShapeFactory = {
                line: function() {
                    return new Shape(game, game._shapes.line, false, 'line', 'easy');
                },
                square: function() {
                    return new Shape(game, game._shapes.square, false, 'square', 'easy');
                },
                dot: function() {
                    return new Shape(game, game._shapes.dot, false, 'dot', 'easy');
                },
                doubleDot: function() {
                    return new Shape(game, game._shapes.doubleDot, false, 'doubleDot', 'easy');
                },
                tripleDot: function() {
                    return new Shape(game, game._shapes.tripleDot, false, 'tripleDot', 'easy');
                }
            };

            this._normalShapeFactory = {
                arrow: function() {
                    return new Shape(game, game._shapes.arrow, false, 'arrow', 'normal');
                },
                leftHook: function() {
                    return new Shape(game, game._shapes.leftHook, false, 'leftHook', 'normal');
                },
                rightHook: function() {
                    return new Shape(game, game._shapes.rightHook, false, 'rightHook', 'normal');
                },
                leftZag: function() {
                    return new Shape(game, game._shapes.leftZag, false, 'leftZag', 'normal');
                },
                rightZag: function() {
                    return new Shape(game, game._shapes.rightZag, false, 'rightZag', 'normal');
                }
            };

            this._hardShapeFactory = {
                emptyCross: function() {
                    return new Shape(game, game._shapes.emptyCross, false, 'emptyCross', 'hard');
                },
                tHook: function() {
                    return new Shape(game, game._shapes.tHook, false, 'tHook', 'hard');
                },
                diagonal: function() {
                    return new Shape(game, game._shapes.diagonal, false, 'diagonal', 'hard');
                },
                fullCross: function() {
                    return new Shape(game, game._shapes.fullCross, false, 'fullCross', 'hard');
                },
                longLine: function() {
                    return new Shape(game, game._shapes.longLine, false, 'longLine', 'hard');
                },
                doubleHook: function() {
                    return new Shape(game, game._shapes.doubleHook, false, 'doubleHook', 'hard');
                }
            };

            this._obstacleShapeFactory = {
                dot: function() {
                    return new Shape(game, game._shapes.dot, false, 'dot');
                }
            };

            game._shapeFactories = new Array(this._easyShapeFactory, this._normalShapeFactory, this._hardShapeFactory);

        },


        _SetupFilled: function() {
            var game = this;
            if (this._filled !== null) {
                return;
            }

            this._filled = {
                data: new Array(game._BLOCK_WIDTH * game._BLOCK_HEIGHT),
                score: 0,
                toClear: {},
                check: function(x, y) {
                    return this.data[this.asIndex(x, y)];
                },
                add: function(x, y, blockType, blockVariation, blockIndex, blockOrientation) {
                    if (x >= 0 && x < game._BLOCK_WIDTH && y >= 0 && y < game._BLOCK_HEIGHT) {
                        this.data[this.asIndex(x, y)] = {
                            blockType: blockType,
                            blockVariation: blockVariation,
                            blockIndex: blockIndex,
                            blockOrientation: blockOrientation
                        };
                    }
                },
                getFreeSpaces: function() {
                    var count = 0;
                    for (var i = 0; i < this.data.length; i++) {
                        count += (this.data[i] ? 1 : 0);
                    }
                },
                asIndex: function(x, y) {
                    return x + y * game._BLOCK_WIDTH;
                },
                asX: function(index) {
                    return index % game._BLOCK_WIDTH;
                },
                asY: function(index) {
                    return Math.floor(index / game._BLOCK_WIDTH);
                },
                clearAll: function() {
                    delete this.data;
                    this.data = new Array(game._BLOCK_WIDTH * game._BLOCK_HEIGHT);
                },
                _popRow: function(row_to_pop) {
                    for (var i = game._BLOCK_WIDTH * (row_to_pop + 1) - 1; i >= 0; i--) {
                        this.data[i] = (i >= game._BLOCK_WIDTH ? this.data[i - game._BLOCK_WIDTH] : undefined);
                    }
                },
                checkForClears: function() {
                    var startLines = game._board.lines;
                    var rows = [],
                        i, len, count, mod;

                    for (i = 0, len = this.data.length; i < len; i++) {
                        mod = this.asX(i);
                        if (mod == 0) count = 0;
                        if (this.data[i] && typeof this.data[i] !== 'undefined' && typeof this.data[i].blockType === 'string') {
                            count += 1;
                        }
                        if (mod == game._BLOCK_WIDTH - 1 && count == game._BLOCK_WIDTH) {
                            rows.push(this.asY(i));
                        }
                    }

                    for (i = 0, len = rows.length; i < len; i++) {
                        this._popRow(rows[i]);
                        game._board.lines++;
                        if (game._board.lines % 10 == 0 && game._board.dropDelay > 1) {
                            game._board.dropDelay *= 0.9;
                        }
                    }

                    var clearedLines = game._board.lines - startLines;
                    this._updateScore(clearedLines);
                },
                _updateScore: function(numLines) {
                    if (numLines <= 0) {
                        return;
                    }
                    var scores = [0, 400, 1000, 3000, 12000];
                    if (numLines >= scores.length) {
                        numLines = scores.length - 1
                    }

                    this.score += scores[numLines];
                    game._$scoreText.text(this.score);

                    game.options.onLine.call(game.element, numLines, scores[numLines], this.score);
                },
                _resetScore: function() {
                    this.score = 0;
                    game._$scoreText.text(this.score);
                },
                draw: function() {
                    for (var i = 0, len = this.data.length, row, color; i < len; i++) {
                        if (this.data[i] !== undefined) {
                            row = this.asY(i);
                            var block = this.data[i];
                            game._board.drawBlock(this.asX(i), row, block.blockType, block.blockVariation, block.blockIndex, block.blockOrientation);
                        }
                    }
                }
            };
        },


        _SetupInfo: function() {

            var game = this;

            this._info = {
                mode: game.options.difficulty,
                modes: [
                    'normal',
                    'nice',
                    'evil'
                ],
                modesY: 170,
                autopilotY: null,

                init: function() {
                    this.mode = game.options.difficulty;
                },
                setMode: function(mode) {
                    this.mode = mode;
                    game._board.nextShape(true);
                }
            };

        },


        _SetupBoard: function() {

            var game = this;
            var info = this._info;

            this._board = {
                // This sets the tick rate for the game
                animateDelay: 1000 / game.options.speed,

                animateTimeoutId: null,
                cur: null,

                lines: 0,

                // DropCount increments on each animation frame. After n frames, the piece drops 1 square
                // By making dropdelay lower (down to 0), the pieces move faster, up to once per tick (animateDelay).
                dropCount: 0,
                dropDelay: 5, //5,

                holding: {
                    left: null,
                    right: null,
                    drop: null
                },
                holdingThreshold: 200, // How long do you have to hold a key to make commands repeat (in ms)

                started: false,
                gameover: false,
                levelOver: false,

                renderChanged: true,

                init: function() {
                    this.cur = this.nextShape();

                    if (game.options.showFieldOnStart) {
                        game._drawBackground();
                        game._board.createRandomBoard();
                        game._board.render();
                    }

                    this.showStartMessage();
                },

                showStartMessage: function() {
                    game._$start.show();
                },

                showGameOverMessage: function() {
                    game._$gameover.show();
                },

                createObstacles: function() {
                    console.log("Creating new obstacles");

                    var x, y;

                    for (var i = 0; i < game.options.numObstacles; i++) {
                        x = this.getRandomInt(0, game._BLOCK_WIDTH - 1);
                        y = this.getRandomInt(0, game._BLOCK_HEIGHT - 1);
                        var obstacleFunc = game._randomObstacles();
                        var obstacle = obstacleFunc(game._filled, game._checkCollisions, game._BLOCK_WIDTH, game._BLOCK_HEIGHT, info.mode);
                        obstacle.init();

                        this.cur = obstacle;

                        this.renderObstacle(true, x, y);

                        this.animateObstacle();

                    }
                },

                getRandomInt: function(min, max) {
                    return (Math.floor(Math.random() * (max - min + 1)) + min);
                },

                nextShape: function(_set_next_only) {
                    var next = this.next,
                        func, shape, result, shapeFactory;

                    shapeFactory = game._selectRandShapeFactory();

                    if (info.mode == 'nice' || info.mode == 'evil') {
                        func = game._niceShapes;
                    } else {
                        func = game._randomShapes(shapeFactory);
                    }

                    game.options.onNext.call(game.element, this.next);
                    if (game.options.no_preview) {
                        this.next = null;
                        if (_set_next_only) return null;
                        shape = func(game._filled, game._checkCollisions, game._BLOCK_WIDTH, game._BLOCK_HEIGHT, info.mode);
                        if (!shape) throw new Error('No shape returned from shape function!', func);
                        shape.init();
                        result = shape;
                    } else {
                        shape = func(game._filled, game._checkCollisions, game._BLOCK_WIDTH, game._BLOCK_HEIGHT, info.mode);
                        if (!shape) throw new Error('No shape returned from shape function!', func);
                        shape.init();
                        this.next = shape;
                        if (_set_next_only) return null;
                        result = next || this.nextShape();
                    }

                    if (game.options.autoplay) { //fun little hack...
                        game._niceShapes(game._filled, game._checkCollisions, game._BLOCK_WIDTH, game._BLOCK_HEIGHT, 'normal', result);
                        result.orientation = result.best_orientation;
                        result.x = result.best_x;
                    }

                    if (typeof game._theme.complexBlocks !== 'undefined') {
                        if ($.isArray(game._theme.complexBlocks[result.blockType])) {
                            result.blockVariation = game._randInt(0, game._theme.complexBlocks[result.blockType].length - 1);
                        } else {
                            result.blockVariation = null;
                        }
                    } else if (typeof game._theme.blocks !== 'undefined') {
                        if ($.isArray(game._theme.blocks[result.blockType])) {
                            result.blockVariation = game._randInt(0, game._theme.blocks[result.blockType].length - 1);
                        } else {
                            result.blockVariation = null;
                        }
                    }

                    return result;
                },

                animate: function() {
                    var drop = false,
                        moved = false,
                        gameOver = false,
                        now = Date.now();

                    if (this.animateTimeoutId) {
                        clearTimeout(this.animateTimeoutId);
                    }

                    //game.updateSizes();

                    if (!this.paused && !this.gameover) {

                        this.dropCount++;

                        // Drop by delay or holding
                        if ((this.dropCount >= this.dropDelay) ||
                            (game.options.autoplay) ||
                            (this.holding.drop && (now - this.holding.drop) >= this.holdingThreshold)) {
                            drop = true;
                            moved = true;
                            this.dropCount = 0;
                        }

                        // Move Left by holding
                        if (this.holding.left && (now - this.holding.left) >= this.holdingThreshold) {
                            moved = true;
                            this.cur.moveLeft();
                        }

                        // Move Right by holding
                        if (this.holding.right && (now - this.holding.right) >= this.holdingThreshold) {
                            moved = true;
                            this.cur.moveRight();
                        }

                        // Test for a collision, add the piece to the filled blocks and fetch the next one
                        if (drop) {
                            var cur = this.cur,
                                x = cur.x,
                                y = cur.y,
                                blocks = cur.getBlocks();
                            if (game._checkCollisions(x, y + 1, blocks, true)) {
                                drop = false;
                                var blockIndex = 0;
                                for (var i = 0; i < cur.blocksLen; i += 2) {
                                    game._filled.add(x + blocks[i], y + blocks[i + 1], cur.blockType, cur.blockVariation, blockIndex, cur.orientation);
                                    if (y + blocks[i] < 0) {
                                        gameOver = true;
                                    }
                                    blockIndex++;
                                }
                                game._filled.checkForClears();
                                this.cur = this.nextShape();
                                this.renderChanged = true;

                                // Stop holding drop (and any other buttons). Just in case the controls get sticky.
                                this.holding.left = null;
                                this.holding.right = null;
                                this.holding.drop = null;

                                game.options.onPlaced.call(game.element);
                            }
                        }
                    }

                    // Drop
                    if (drop) {
                        moved = true;
                        this.cur.y++;
                    }

                    if (drop || moved) {
                        this.renderChanged = true;
                    }

                    if (gameOver) {

                        this.gameover = true;

                        game.gameover();

                        if (game.options.autoplay && game.options.autoplayRestart) {
                            // On autoplay, restart the game automatically
                            game.restart();
                        }
                        this.renderChanged = true;

                    } else if (this.levelOver) {
                        if (game.options.autoplay && game.options.autoplayRestart) {
                            // On autoplay, restart the game automatically
                            game.restart();
                        }
                        this.renderChanged = true;

                    } else {

                        // Update the speed
                        this.animateDelay = 1000 / game.options.speed;

                        this.animateTimeoutId = window.setTimeout(function() {
                            game._board.animate();
                        }, this.animateDelay);

                    }

                },

                animateObstacle: function() {

                    if (this.animateTimeoutId) {
                        clearTimeout(this.animateTimeoutId);
                    }

                    var cur = this.cur,
                        x = cur.x,
                        y = cur.y,
                        blocks = cur.getBlocks();

                    var blockIndex = 0;
                    for (var i = 0; i < cur.blocksLen; i += 2) {
                        game._filled.add(x + blocks[i], y + blocks[i + 1], cur.blockType, cur.blockVariation, blockIndex, cur.orientation);
                        if (y + blocks[i] < 0) {
                            gameOver = true;
                        }
                        blockIndex++;
                    }
                    game._filled.checkForClears();
                    this.renderChanged = true;

                    // Stop holding drop (and any other buttons). Just in case the controls get sticky.
                    this.holding.left = null;
                    this.holding.right = null;
                    this.holding.drop = null;

                    //game.options.onPlaced.call(game.element);

                },

                createRandomBoard: function() {

                    var start = [],
                        blockTypes = [],
                        i, ilen, j, jlen, blockType;

                    // Draw a random blockrain screen
                    blockTypes = Object.keys(game._normalShapeFactory);

                    for (i = 0, ilen = game._BLOCK_WIDTH; i < ilen; i++) {
                        for (j = 0, jlen = game._randChoice([game._randInt(0, 8), game._randInt(5, 9)]); j < jlen; j++) {
                            if (!blockType || !game._randInt(0, 3)) blockType = game._randChoice(blockTypes);

                            // Use a random piece and orientation
                            // Todo: Use an actual random variation
                            game._filled.add(i, game._BLOCK_HEIGHT - j, blockType, game._randInt(0, 3), null, game._randInt(0, 3));
                        }
                    }

                    /*
                    for (i=0, ilen=WIDTH; i<ilen; i++) {
                      for (j=0, jlen=randChoice([randInt(0, 8), randInt(5, 9)]); j<jlen; j++) {
                        if (!blockType || !randInt(0, 3)) blockType = randChoice(blockTypes);
                        start.push([i, HEIGHT - j, blockType]);
                      }
                    }

                    if( options.showFieldOnStart ) {
                      drawBackground();
                      for (i=0, ilen=start.length; i<ilen; i++) {
                        drawBlock.apply(drawBlock, start[i]);
                      }
                    }
                    */

                    game._board.render(true);

                },

                render: function(forceRender) {
                    if (this.renderChanged || forceRender) {
                        this.renderChanged = false;
                        game._ctx.clearRect(0, 0, game._PIXEL_WIDTH, game._PIXEL_HEIGHT);
                        game._drawBackground();
                        game._filled.draw();
                        this.cur.draw();
                    }
                },

                renderObstacle: function(forceRender, x, y) {
                    if (this.renderChanged || forceRender) {
                        this.renderChanged = false;
                        game._ctx.clearRect(0, 0, game._PIXEL_WIDTH, game._PIXEL_HEIGHT);
                        game._drawBackground();
                        game._filled.draw();
                        this.cur.drawObstacle(x, y);
                    }
                },


                /**
                 * Draws one block (Each piece is made of 4 blocks)
                 * The blockType is used to draw any block. 
                 * The falling attribute is needed to apply different styles for falling and placed blocks.
                 */
                drawBlock: function(x, y, blockType, blockVariation, blockIndex, blockRotation, falling) {

                    // convert x and y to pixel
                    x = x * game._block_size;
                    y = y * game._block_size;


                    falling = typeof falling === 'boolean' ? falling : false;
                    var borderWidth = game._theme.strokeWidth;
                    var borderDistance = Math.round(game._block_size * 0.23);
                    var squareDistance = Math.round(game._block_size * 0.30);

                    var color = this.getBlockColor(blockType, blockVariation, blockIndex, falling);

                    // Draw the main square
                    game._ctx.globalAlpha = 1.0;

                    // If it's an image, the block has a specific texture. Use that.
                    if (color instanceof Image) {
                        game._ctx.globalAlpha = 1.0;

                        // Not loaded
                        if (color.width === 0 || color.height === 0) {
                            return;
                        }

                        // A square is the same style for all blocks
                        if (typeof game._theme.blocks !== 'undefined' && game._theme.blocks !== null) {
                            game._ctx.drawImage(color, 0, 0, color.width, color.height, x, y, game._block_size, game._block_size);
                        }
                        // A custom texture
                        else if (typeof game._theme.complexBlocks !== 'undefined' && game._theme.complexBlocks !== null) {
                            if (typeof blockIndex === 'undefined' || blockIndex === null) {
                                blockIndex = 0;
                            }

                            var getCustomBlockImageCoordinates = function(image, blockType, blockIndex) {
                                // The image is based on the first ("upright") orientation
                                var positions = game._shapes[blockType][0];
                                // Find the number of tiles it should have
                                var minX = Math.min(positions[0], positions[2], positions[4], positions[6]);
                                var maxX = Math.max(positions[0], positions[2], positions[4], positions[6]);
                                var minY = Math.min(positions[1], positions[3], positions[5], positions[7]);
                                var maxY = Math.max(positions[1], positions[3], positions[5], positions[7]);
                                var rangeX = maxX - minX + 1;
                                var rangeY = maxY - minY + 1;

                                // X and Y sizes should match. Should.
                                var tileSizeX = image.width / rangeX;
                                var tileSizeY = image.height / rangeY;

                                return {
                                    x: tileSizeX * (positions[blockIndex * 2] - minX),
                                    y: tileSizeY * Math.abs(minY - positions[blockIndex * 2 + 1]),
                                    w: tileSizeX,
                                    h: tileSizeY
                                };
                            };

                            var coords = getCustomBlockImageCoordinates(color, blockType, blockIndex);

                            game._ctx.save();

                            game._ctx.translate(x, y);
                            game._ctx.translate(game._block_size / 2, game._block_size / 2);
                            game._ctx.rotate(-Math.PI / 2 * blockRotation);
                            game._ctx.drawImage(color, coords.x, coords.y, coords.w, coords.h, -game._block_size / 2, -game._block_size / 2, game._block_size, game._block_size);

                            game._ctx.restore();

                        } else {
                            // ERROR
                            game._ctx.fillStyle = '#ff0000';
                            game._ctx.fillRect(x, y, game._block_size, game._block_size);
                        }
                    } else if (typeof color === 'string') {
                        game._ctx.fillStyle = color;
                        game._ctx.fillRect(x, y, game._block_size, game._block_size);

                        // Inner Shadow
                        if (typeof game._theme.innerShadow === 'string') {
                            game._ctx.globalAlpha = 1.0;
                            game._ctx.strokeStyle = game._theme.innerShadow;
                            game._ctx.lineWidth = 1.0;

                            // Draw the borders
                            game._ctx.strokeRect(x + 1, y + 1, game._block_size - 2, game._block_size - 2);
                        }

                        // Decoration (borders)
                        if (typeof game._theme.stroke === 'string') {
                            game._ctx.globalAlpha = 1.0;
                            game._ctx.fillStyle = game._theme.stroke;
                            game._ctx.strokeStyle = game._theme.stroke;
                            game._ctx.lineWidth = borderWidth;

                            // Draw the borders
                            game._ctx.strokeRect(x, y, game._block_size, game._block_size);
                        }
                        if (typeof game._theme.innerStroke === 'string') {
                            // Draw the inner dashes
                            game._ctx.fillStyle = game._theme.innerStroke;
                            game._ctx.fillRect(x + borderDistance, y + borderDistance, game._block_size - borderDistance * 2, borderWidth);
                            // The rects shouldn't overlap, to prevent issues with transparency
                            game._ctx.fillRect(x + borderDistance, y + borderDistance + borderWidth, borderWidth, game._block_size - borderDistance * 2 - borderWidth);
                        }
                        if (typeof game._theme.innerSquare === 'string') {
                            // Draw the inner square
                            game._ctx.fillStyle = game._theme.innerSquare;
                            game._ctx.globalAlpha = 0.2;
                            game._ctx.fillRect(x + squareDistance, y + squareDistance, game._block_size - squareDistance * 2, game._block_size - squareDistance * 2);
                        }
                    }

                    // Return the alpha back to 1.0 so we don't create any issues with other drawings.
                    game._ctx.globalAlpha = 1.0;
                },


                getBlockColor: function(blockType, blockVariation, blockIndex, falling) {
                    /**
                     * The theme allows us to do many things:
                     * - Use a specific color for the falling block (primary), regardless of the proper color.
                     * - Use another color for the placed blocks (secondary).
                     * - Default to the "original" block color in any of those cases by setting primary and/or secondary to null.
                     * - With primary and secondary as null, all blocks keep their original colors.
                     */

                    var getBlockVariation = function(blockTheme, blockVariation) {
                        if ($.isArray(blockTheme)) {
                            if (blockVariation !== null && typeof blockTheme[blockVariation] !== 'undefined') {
                                return blockTheme[blockVariation];
                            } else if (blockTheme.length > 0) {
                                return blockTheme[0];
                            } else {
                                return null;
                            }
                        } else {
                            return blockTheme;
                        }
                    }

                    if (typeof falling !== 'boolean') {
                        falling = true;
                    }
                    if (falling) {
                        if (typeof game._theme.primary === 'string' && game._theme.primary !== '') {
                            return game._theme.primary;
                        } else if (typeof game._theme.blocks !== 'undefined' && game._theme.blocks !== null) {
                            return getBlockVariation(game._theme.blocks[blockType], blockVariation);
                        } else {
                            return getBlockVariation(game._theme.complexBlocks[blockType], blockVariation);
                        }
                    } else {
                        if (typeof game._theme.secondary === 'string' && game._theme.secondary !== '') {
                            return game._theme.secondary;
                        } else if (typeof game._theme.blocks !== 'undefined' && game._theme.blocks !== null) {
                            return getBlockVariation(game._theme.blocks[blockType], blockVariation);
                        } else {
                            return getBlockVariation(game._theme.complexBlocks[blockType], blockVariation);
                        }
                    }
                }

            };

            game._niceShapes = game._getNiceShapes();
        },

        // Utility Functions
        _randInt: function(a, b) {
            return a + Math.floor(Math.random() * (1 + b - a));
        },
        _randSign: function() {
            return this._randInt(0, 1) * 2 - 1;
        },
        _randChoice: function(choices) {
            return choices[this._randInt(0, choices.length - 1)];
        },


        /**
         * Find base64 encoded images and load them as image objects, which can be used by the canvas renderer
         */
        _preloadThemeAssets: function() {

            var game = this;

            var hexColorcheck = new RegExp('^#[A-F0-9+]{3,6}', 'i');
            var base64check = new RegExp('^data:image/(png|gif|jpg);base64,', 'i');

            var handleAssetLoad = function() {
                // Rerender the board as soon as an asset loads
                if (game._board) {
                    game._board.render(true);
                }
            };

            var loadAsset = function(src) {
                var plainSrc = src;
                if (!hexColorcheck.test(plainSrc)) {
                    // It's an image
                    src = new Image();
                    src.src = plainSrc;
                    src.onload = handleAssetLoad;
                } else {
                    // It's a color
                    src = plainSrc;
                }
                return src;
            };

            var startAssetLoad = function(block) {
                // Assets can be an array of variation so they can change color/design randomly
                if ($.isArray(block) && block.length > 0) {
                    for (var i = 0; i < block.length; i++) {
                        block[i] = loadAsset(block[i]);
                    }
                } else if (typeof block === 'string') {
                    block = loadAsset(block);
                }
                return block;
            };


            if (typeof this._theme.complexBlocks !== 'undefined') {
                var keys = Object.keys(this._theme.complexBlocks);

                // Load the complexBlocks
                for (var i = 0; i < keys.length; i++) {
                    this._theme.complexBlocks[keys[i]] = startAssetLoad(this._theme.complexBlocks[keys[i]]);
                }
            } else if (typeof this._theme.blocks !== 'undefined') {
                var keys = Object.keys(this._theme.blocks);

                // Load the blocks
                for (var i = 0; i < keys.length; i++) {
                    this._theme.blocks[keys[i]] = startAssetLoad(this._theme.blocks[keys[i]]);
                }
            }

            // Load the bg
            if (typeof this._theme.backgroundGrid !== 'undefined') {
                if (typeof this._theme.backgroundGrid === 'string') {
                    if (!hexColorcheck.test(this._theme.backgroundGrid)) {
                        var src = this._theme.backgroundGrid;
                        this._theme.backgroundGrid = new Image();
                        this._theme.backgroundGrid.src = src;
                        this._theme.backgroundGrid.onload = handleAssetLoad;
                    }
                }
            }

        },


        _createHolder: function() {

            // Create the main holder (it holds all the ui elements, the original element is just the wrapper)
            this._$gameholder = $('<div class="blockrain-game-holder"></div>');
            this._$gameholder.css('position', 'relative').css('width', '100%').css('height', '100%');

            this.element.html('').append(this._$gameholder);

            // Create the game canvas and context
            this._$canvas = $('<canvas style="display:block; width:100%; height:100%; padding:0; margin:0; border:none;" />');
            if (typeof this._theme.background === 'string') {
                this._$canvas.css('background-color', this._theme.background);
            }
            this._$gameholder.append(this._$canvas);

            this._canvas = this._$canvas.get(0);
            this._ctx = this._canvas.getContext('2d');

        },


        _createUI: function() {

            var game = this;

            // Score
            game._$score = $(
                '<div class="blockrain-score-holder" style="position:absolute;">' +
                '<div class="blockrain-score">' +
                '<div class="blockrain-score-msg">' + this.options.scoreText + '</div>' +
                '<div class="blockrain-score-num">0</div>' +
                '</div>' +
                '</div>').hide();
            game._$scoreText = game._$score.find('.blockrain-score-num');
            game._$gameholder.append(game._$score);

            // Create the start menu
            game._$start = $(
                '<div class="blockrain-start-holder" style="position:absolute;">' +
                '<div class="blockrain-start">' +
                '<div class="blockrain-start-msg">' + this.options.playText + '</div>' +
                '<a class="blockrain-btn blockrain-start-btn">' + this.options.playButtonText + '</a>' +
                '</div>' +
                '</div>').hide();
            game._$gameholder.append(game._$start);

            game._$start.find('.blockrain-start-btn').click(function(event) {
                event.preventDefault();
                game.start();
            });


            game._$pause = $('<a id="pauseButton" class="blockrain-pause-btn"></a>').hide();
            game._$gameholder.append(game._$pause);

            game._$pause.click(function(event) {
                event.preventDefault();
                game.pauseResume();
            });


            // Create the game over menu
            game._$gameover = $(
                '<div class="blockrain-game-over-holder" style="position:absolute;">' +
                '<div class="blockrain-game-over">' +
                '<div class="blockrain-game-over-msg">' + this.options.gameOverText + '</div>' +
                '<a class="blockrain-btn blockrain-game-over-btn">' + this.options.restartButtonText + '</a>' +
                '</div>' +
                '</div>').hide();
            game._$gameover.find('.blockrain-game-over-btn').click(function(event) {
                event.preventDefault();
                game.restart();
            });
            game._$gameholder.append(game._$gameover);

            this._createControls();
        },


        _createControls: function() {

            var game = this;

            game._$touchLeft = $('<a class="blockrain-touch blockrain-touch-left" />').appendTo(game._$gameholder);
            game._$touchRight = $('<a class="blockrain-touch blockrain-touch-right" />').appendTo(game._$gameholder);
            game._$touchRotateRight = $('<a class="blockrain-touch blockrain-touch-rotate-right" />').appendTo(game._$gameholder);
            game._$touchRotateLeft = $('<a class="blockrain-touch blockrain-touch-rotate-left" />').appendTo(game._$gameholder);
            game._$touchDrop = $('<a class="blockrain-touch blockrain-touch-drop" />').appendTo(game._$gameholder);
            game._$touchDropSpace = $('<a class="blockrain-touch blockrain-touch-drop-space" />').appendTo(game._$gameholder);
        },


        _refreshBlockSizes: function() {

            if (this.options.autoBlockWidth) {
                this.options.blockWidth = Math.ceil(this.element.width() / this.options.autoBlockSize);
            }

        },


        _getNiceShapes: function() {
            /*
             * Things I need for this to work...
             *  - ability to test each shape with this._filled data
             *  - maybe give empty spots scores? and try to maximize the score?
             */

            var game = this;

            var shapes = {},
                attr;

            for (var attr in this._shapeFactory) {
                shapes[attr] = this._shapeFactory[attr]();
            }

            function scoreBlocks(possibles, blocks, x, y, filled, width, height) {
                var i, len = blocks.length,
                    score = 0,
                    bottoms = {},
                    tx, ty, overlaps;

                // base score
                for (i = 0; i < len; i += 2) {
                    score += possibles[game._filled.asIndex(x + blocks[i], y + blocks[i + 1])] || 0;
                }

                // overlap score -- //TODO - don't count overlaps if cleared?
                for (i = 0; i < len; i += 2) {
                    tx = blocks[i];
                    ty = blocks[i + 1];
                    if (bottoms[tx] === undefined || bottoms[tx] < ty) {
                        bottoms[tx] = ty;
                    }
                }
                overlaps = 0;
                for (tx in bottoms) {
                    tx = parseInt(tx);
                    for (ty = bottoms[tx] + 1, i = 0; y + ty < height; ty++, i++) {
                        if (!game._filled.check(x + tx, y + ty)) {
                            overlaps += i == 0 ? 2 : 1; //TODO-score better
                            //if (i == 0) overlaps += 1;
                            break;
                        }
                    }
                }

                score = score - overlaps;

                return score;
            }

            function resetShapes() {
                for (var attr in shapes) {
                    shapes[attr].x = 0;
                    shapes[attr].y = -1;
                }
            }

            //TODO -- evil mode needs to realize that overlap is bad...
            var func = function(filled, checkCollisions, width, height, mode, _one_shape) {
                if (!_one_shape) resetShapes();

                var possibles = new Array(width * height),
                    evil = mode == 'evil',
                    x, y, py,
                    attr, shape, i, blocks, bounds,
                    score, best_shape, best_score = (evil ? 1 : -1) * 999,
                    best_orientation, best_x,
                    best_score_for_shape, best_orientation_for_shape, best_x_for_shape;

                for (x = 0; x < width; x++) {
                    for (y = 0; y <= height; y++) {
                        if (y == height || filled.check(x, y)) {
                            for (py = y - 4; py < y; py++) {
                                possibles[filled.asIndex(x, py)] = py; //TODO - figure out better scoring?
                            }
                            break;
                        }
                    }
                }

                // for each shape...
                var opts = _one_shape === undefined ? shapes : {
                    cur: _one_shape
                }; //BOO
                for (attr in opts) { //TODO - check in random order to prevent later shapes from winning
                    shape = opts[attr];
                    best_score_for_shape = -999;

                    // for each orientation...
                    for (i = 0; i < (shape.symmetrical ? 2 : 4); i++) { //TODO - only look at unique orientations
                        blocks = shape.getBlocks(i);
                        bounds = shape.getBounds(blocks);

                        // try each possible position...
                        for (x = -bounds.left; x < width - bounds.width; x++) {
                            for (y = -1; y < height - bounds.bottom; y++) {
                                if (game._checkCollisions(x, y + 1, blocks, true)) {
                                    // collision
                                    score = scoreBlocks(possibles, blocks, x, y, filled, width, height);
                                    if (score > best_score_for_shape) {
                                        best_score_for_shape = score;
                                        best_orientation_for_shape = i;
                                        best_x_for_shape = x;
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    if ((evil && best_score_for_shape < best_score) ||
                        (!evil && best_score_for_shape > best_score)) {
                        best_shape = shape;
                        best_score = best_score_for_shape;
                        best_orientation = best_orientation_for_shape;
                        best_x = best_x_for_shape;
                    }
                }

                best_shape.best_orientation = best_orientation;
                best_shape.best_x = best_x;

                return best_shape;
            };

            func.no_preview = true;
            return func;
        },


        _randomShapes: function(shapeFactory) {
            // Todo: The shapefuncs should be cached.
            var shapeFuncs = [];
            $.each(shapeFactory, function(k, v) {
                shapeFuncs.push(v);
            });

            return this._randChoice(shapeFuncs);
        },

        _randomObstacles: function() {
            // Todo: The shapefuncs should be cached.
            var obstacleShapeFuncs = [];
            $.each(this._obstacleShapeFactory, function(k, v) {
                obstacleShapeFuncs.push(v);
            });

            return this._randChoice(obstacleShapeFuncs);
        },


        /**
         * Controls
         */
        _setupControls: function(enable) {

            var game = this;

            var moveLeft = function(start) {
                if (!start) {
                    game._board.holding.left = null;
                    return;
                }
                if (!game._board.holding.left) {
                    game._board.cur.moveLeft();
                    game._board.holding.left = Date.now();
                    game._board.holding.right = null;
                }
            }
            var moveRight = function(start) {
                if (!start) {
                    game._board.holding.right = null;
                    return;
                }
                if (!game._board.holding.right) {
                    game._board.cur.moveRight();
                    game._board.holding.right = Date.now();
                    game._board.holding.left = null;
                }
            }
            var drop = function(start) {
                if (!start) {
                    game._board.holding.drop = null;
                    return;
                }
                if (!game._board.holding.drop) {
                    if (game._board.dropFirstPress) {
                        game._board.cur.drop();
                        game._board.holding.drop = Date.now();
                    } else {
                        if (game.options.quickDrop) game._board.cur.quickDrop();
                    }
                    setTimeout(function() {
                        game._board.dropFirstPress = true;
                    }, game.options.doublePressTime);
                    game._board.dropFirstPress = false;
                }
            }

            var quickDrop = function(start) {
                if (!start) {
                    game._board.holding.drop = null;
                    return;
                }
                if (!game._board.holding.drop) {
                    if (game.options.quickDrop) game._board.cur.quickDrop();
                }
            }

            var rotateLeft = function() {
                game._board.cur.rotate('left');
            }
            var rotateRight = function() {
                game._board.cur.rotate('right');
            }

            // Handlers: These are used to be able to bind/unbind controls
            var handleKeyDown = function(evt) {
                if (!game._board.cur) {
                    return true;
                }
                var caught = false;

                caught = true;
                if (game.options.asdwKeys) {
                    switch (evt.keyCode) {
                        case 65:
                            /*a*/
                            moveLeft(true);
                            break;
                        case 68:
                            /*d*/
                            moveRight(true);
                            break;
                        case 83:
                            /*s*/
                            drop(true);
                            break;
                        case 87:
                            /*w*/
                            game._board.cur.rotate('right');
                            break;
                    }
                }
                switch (evt.keyCode) {
                    case 32:
                        /*spacebar*/
                        quickDrop(true);
                        break;
                    case 37:
                        /*left*/
                        moveLeft(true);
                        break;
                    case 39:
                        /*right*/
                        moveRight(true);
                        break;
                    case 40:
                        /*down*/
                        drop(true);
                        break;
                    case 38:
                        /*up*/
                        game._board.cur.rotate('right');
                        break;
                    case 80:
                        /*pause P*/
                        game.pauseResume();
                        break;
                    case 88:
                        /*x*/
                        game._board.cur.rotate('right');
                        break;
                    case 90:
                        /*z*/
                        game._board.cur.rotate('left');
                        break;
                    default:
                        caught = false;
                }
                if (caught) evt.preventDefault();
                return !caught;
            };


            var handleKeyUp = function(evt) {
                if (!game._board.cur) {
                    return true;
                }
                var caught = false;

                caught = true;
                if (game.options.asdwKeys) {
                    switch (evt.keyCode) {
                        case 65:
                            /*a*/
                            moveLeft(false);
                            break;
                        case 68:
                            /*d*/
                            moveRight(false);
                            break;
                        case 83:
                            /*s*/
                            drop(false);
                            break;
                    }
                }
                switch (evt.keyCode) {
                    case 32:
                        /*spacebar*/
                        quickDrop(false);
                        break;
                    case 37:
                        /*left*/
                        moveLeft(false);
                        break;
                    case 39:
                        /*right*/
                        moveRight(false);
                        break;
                    case 40:
                        /*down*/
                        drop(false);
                        break;
                    default:
                        caught = false;
                }
                if (caught) evt.preventDefault();
                return !caught;
            };

            function isStopKey(evt) {
                var cfg = {
                    stopKeys: {
                        32: 1,
                        37: 1,
                        38: 1,
                        39: 1,
                        40: 1
                    }
                };

                var isStop = (cfg.stopKeys[evt.keyCode] || (cfg.moreStopKeys && cfg.moreStopKeys[evt.keyCode]));
                if (isStop) evt.preventDefault();
                return isStop;
            }

            function getKey(evt) {
                return 'safekeypress.' + evt.keyCode;
            }

            function keydown(evt) {
                var key = getKey(evt);
                $.data(this, key, ($.data(this, key) || 0) - 1);
                return handleKeyDown.call(this, evt);
            }

            function keyup(evt) {
                $.data(this, getKey(evt), 0);
                handleKeyUp.call(this, evt);
                return isStopKey(evt);
            }

            // Unbind everything by default
            // Use event namespacing so we don't ruin other keypress events
            $(document).unbind('keydown.blockrain')
                .unbind('keyup.blockrain');

            if (!game.options.autoplay) {
                if (enable) {
                    $(document).bind('keydown.blockrain', keydown)
                        .bind('keyup.blockrain', keyup);
                }
                this._gyroControls();
            }
        },


        _setupTouchControls: function(enable) {

            var game = this;

            // Movements can be held for faster movement
            var moveLeft = function(event) {
                event.preventDefault();
                game._board.cur.moveLeft();
                game._board.holding.left = Date.now();
                game._board.holding.right = null;
                game._board.holding.drop = null;
            };
            var moveRight = function(event) {
                event.preventDefault();
                game._board.cur.moveRight();
                game._board.holding.right = Date.now();
                game._board.holding.left = null;
                game._board.holding.drop = null;
            };
            var drop = function(event) {
                event.preventDefault();
                game._board.cur.drop();
                game._board.holding.drop = Date.now();
            };
            var quickDrop = function(event) {
                event.preventDefault();
                game._board.cur.quickDrop();
                game._board.holding.drop = Date.now();
            };
            var endMoveLeft = function(event) {
                event.preventDefault();
                game._board.holding.left = null;
            };
            var endMoveRight = function(event) {
                event.preventDefault();
                game._board.holding.right = null;
            };
            var endDrop = function(event) {
                event.preventDefault();
                game._board.holding.drop = null;
            };
            var endQuickDrop = function(event) {
                event.preventDefault();
                game._board.holding.drop = null;
            };

            // Rotations can't be held
            var rotateLeft = function(event) {
                event.preventDefault();
                game._board.cur.rotate('left');
            };
            var rotateRight = function(event) {
                event.preventDefault();
                game._board.cur.rotate('right');
            };

            // Unbind everything by default
            game._$touchLeft.unbind('touchstart touchend click');
            game._$touchRight.unbind('touchstart touchend click');
            game._$touchRotateLeft.unbind('touchstart touchend click');
            game._$touchRotateRight.unbind('touchstart touchend click');
            game._$touchDrop.unbind('touchstart touchend click');
            game._$touchDropSpace.unbind('touchstart touchend click');

            if (!game.options.autoplay && enable) {
                game._$touchLeft.show().bind('touchstart click', moveLeft).bind('touchend', endMoveLeft);
                game._$touchRight.show().bind('touchstart click', moveRight).bind('touchend', endMoveRight);
                game._$touchDrop.show().bind('touchstart click', drop).bind('touchend', endDrop);
                game._$touchDropSpace.show().bind('touchstart click', quickDrop).bind('touchend', endQuickDrop);
                game._$touchRotateLeft.show().bind('touchstart click', rotateLeft);
                game._$touchRotateRight.show().bind('touchstart click', rotateRight);
            } else {
                game._$touchLeft.hide();
                game._$touchRight.hide();
                game._$touchRotateLeft.hide();
                game._$touchRotateRight.hide();
                game._$touchDrop.hide();
                game._$touchDropSpace.hide();
            }

        },

        isMobileDevice: function() {
            return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
        },

        _gyroControls: function() {
            if (window.DeviceMotionEvent !== undefined) {
                window.ondevicemotion = function(event) {
                    var accX = Math.round(event.accelerationIncludingGravity.x * 10) / 10;
                    var accZ = Math.round(event.accelerationIncludingGravity.z * 10) / 10;
                    if (Math.round(accZ) > 4) {
                        game._board.cur.drop();
                    }
                    if (Math.round(accX) > 1) {
                        game._board.cur.moveLeft();
                    }
                    if (Math.round(accX) < -1) {
                        game._board.cur.moveRight();
                    }
                };
            }



        }
    });

})(jQuery));
},{}],4:[function(require,module,exports){
/**
 * Themes. You can add more custom themes to this object.
 */
window.BlockrainThemes = {
    'custom': {
        background: '#040304',
        backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAIAAAAlC+aJAAAHHElEQVR4XsVZgZbkqgqkTO77//+dbuvtgQ7QcGwnO3PPZTPG1dhWASIxwP8OEcqTYhJ3ypsAuLqsB7KSNrQ14uMoXAXsnwNihoUDInKKbCdDf2YjPuL+KDRSyOpE1Q5k6JBJV7IJSfnvJUzf8RhyAOh9ADqN3vtz+am+zIXWHIK9l1D5ISuSTbv3aUAJZKfvmMYYBn3O6Y3W/lt2IFmmIHmbQDcCgOM4DCjJqeKsNgQAIe9ag13I4NNHoUWhomMn4BoiubXAqn27qAoNm9HLwhMAfQ10lgYxc5gqvgxcfuw8sdhMHKtD99IrGfCpkXZjBG9x9r8SizJ/JHF8Yww3hYszNDnz5uawDH3WsTESIZBcs6O5r36SVn4gmcFYJVmgSYZOMqmEdjf8vxV8riA4tG0Zo51qeeDQtQxhuP6hUmgYY/U/yu8JKYBVmGdZGznWhqBZoAefTTi7GYOY/jKHEPL57loObBU8zhL4z/P8UxbdN02sUzOSqKmlymZnCLckt2tdq41AOI8KyU4AQGfCrNEOkr0DPjxD767VBUls3qHNEfjdhdpWxa7++zkzVmMB+0PXcndy9yMogcwsd5fJAFzotccfgKBfArmukPKQQ8dCOvrGAXkNxBPekvMahyNbMZbfFFcDLcVPfgV8MoJOcgo2QcWDQZiNNh3lJ9IdaNRskCk0FMUZFJJhgTnpspxF3l5S/3UhuXgpq1EopxxQyX7V3pdB8ndxXo4aukmapDQaJAlSGGZzAu8bIdIDr/Lb6BnXTtgk/wLJnoCUbLSPR+PNTbAMmt3HCDPonnN/c0BrMU7MawAAmAQggOIweu9oGEUmiHLQBPxS+v2WSgDIwTgmjwrblgk1kBbtVId1p/453BAPR+5fJyKuQGQ49KLDWvnLSNQJse8e+SiunI/UcAQ5aTBo6ncj+HMLmGBH04WOqVkm+qPnQkwYBKR1GEpXcXOfpNVAOnSQmJS8euloqxd1fWLZUi2I4JCkvySWN/psMd8HDJhzyD/DdW5fBAFvIzvqKLsErOwcRkKUXT8D5CJdpkCvEG7Szz0r6qVFE6q0faCSxuV05kO8/GUBdOlNkL0wStgd/reRSgCE0FWPhoXfiS5Eg47P6CH8TBlSc+RSP31RCgjwytR5J0riVjsyh60AH3uVgKFPipkiQ/CBAyoUNsVvhE1HkL+SM6Gc6kW0QJrnSHENDa8J9jiYal07ND3uc75GAEkl4GWBkufc8hmsHYQeoUs3vb26TYfeoxBE6NBHxctbKwFV2eFvsdcU/2FdGsv/USX3nd01IfweWHx7i+qm6VmQ4ULBTAo+JrKjgHLXv386gveoiPIo1pEN5d4zyLVHnYYZYVkyjBAgmLUZzV3XPSHo6IMoe4p0U8Z6d/R7VRIoSwsINl5VzVSEXfdcL8P+gYPJD/CuEuAqus/FaQW70Vld/47EOiCawZRAiSBrZ+yooFy7+VG0yHcX4l8eTXLpQn0oIADxIUMBeoDtrsHW87EdsvtvbxgQSResFIHjRFZtj6KEX+ucgZ0D9+iL89avBCLvBMQ5RCUU3pOwvmVSwKwPMNWFoHvSTrXoCenqi8FwZMN7rYEOEN4bJnFBRcK4gi21nClKFOYZ7ZJLYxKwDRYEeXJs1tl92fv9tq/nQkguSVgF9FPonquwBi1ssdbxApQcgkvIAHbpdADKHsLw/C430332xJ8JYSJ6Z2emUHg6ehBCwB0JsQU1ENgmKz2WouXmWCUjKN4CYGOBqn4IWLlmxPTZuYUOh/Kqg6hnY/clDrbsh0jTsMe/lf0oflbRjYAlIiTXYRy3ImfbEN76xG+QT8c5KZPEVBKjKRgFY9vf4KTpkL2F1Ia6fK+2xTrvX5bmnO1Lvd6nkno8nxp6jkEBkOMNwi1GnS5MopWs7c6f9mMoKmlM4sDctT5VHo/Hi4DKgTF8LnLqPQbHLMNahn859fKCESuoLqtoBZC2zfj5LtHsun8+n19fX3/KOVXhyQLkyzknJylTcBw4j6GoHYCBLi/lNRKGC61fQZHA8yJe7AafzV3/oZJei5GjEC8ak4Q8XsobHFrJ2x9IYXtzjQAFpibC+kmUE3f6tJ4P0LGWU/c/Wi/ofYrzdR9G4eIqU54PhXoA42oXRi49BCNY2VCUPIgxiB47AYCC7HB8vgzBpAwgEVChSn2hiayfcZF8zikPOUXGIaBMDQBzUtEfA0Yg1Mp+YqU+eVVIRW8GiO8pIlNCGPfwnwg7RWiL+J+BEY3FK3wVTc7Hw9YPXaGkkDKZxAO0VTn1ojDaqaU1+lOqHuoVffkDducA9e4Th1sApnswouIEByhD5iRBe0TAMSzj85P8IAW3Rjp/prYL7E4CQu0IA033s1C/lUIO5QMBEQQOlHOhnogxciC+12k3l3DffqyXx01JP8p8CemsQ/9yGcwBFfk/Wqz6T1UU/3cAAAAASUVORK5CYII=',
        complexBlocks: {
            line: ['assets/blocks/custom/line.png', 'assets/blocks/custom/line.png'],
            square: ['assets/blocks/custom/square.png'],
            arrow: 'assets/blocks/custom/arrow.png',
            rightHook: ['assets/blocks/custom/rightHook.png'],
            leftHook: 'assets/blocks/custom/leftHook.png',
            rightZag: ['assets/blocks/custom/rightZag.png'],
            leftZag: 'assets/blocks/custom/leftZag.png'
        }
    },
    'candy': {
        background: '#040304',
        backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAIAAAAlC+aJAAAHHElEQVR4XsVZgZbkqgqkTO77//+dbuvtgQ7QcGwnO3PPZTPG1dhWASIxwP8OEcqTYhJ3ypsAuLqsB7KSNrQ14uMoXAXsnwNihoUDInKKbCdDf2YjPuL+KDRSyOpE1Q5k6JBJV7IJSfnvJUzf8RhyAOh9ADqN3vtz+am+zIXWHIK9l1D5ISuSTbv3aUAJZKfvmMYYBn3O6Y3W/lt2IFmmIHmbQDcCgOM4DCjJqeKsNgQAIe9ag13I4NNHoUWhomMn4BoiubXAqn27qAoNm9HLwhMAfQ10lgYxc5gqvgxcfuw8sdhMHKtD99IrGfCpkXZjBG9x9r8SizJ/JHF8Yww3hYszNDnz5uawDH3WsTESIZBcs6O5r36SVn4gmcFYJVmgSYZOMqmEdjf8vxV8riA4tG0Zo51qeeDQtQxhuP6hUmgYY/U/yu8JKYBVmGdZGznWhqBZoAefTTi7GYOY/jKHEPL57loObBU8zhL4z/P8UxbdN02sUzOSqKmlymZnCLckt2tdq41AOI8KyU4AQGfCrNEOkr0DPjxD767VBUls3qHNEfjdhdpWxa7++zkzVmMB+0PXcndy9yMogcwsd5fJAFzotccfgKBfArmukPKQQ8dCOvrGAXkNxBPekvMahyNbMZbfFFcDLcVPfgV8MoJOcgo2QcWDQZiNNh3lJ9IdaNRskCk0FMUZFJJhgTnpspxF3l5S/3UhuXgpq1EopxxQyX7V3pdB8ndxXo4aukmapDQaJAlSGGZzAu8bIdIDr/Lb6BnXTtgk/wLJnoCUbLSPR+PNTbAMmt3HCDPonnN/c0BrMU7MawAAmAQggOIweu9oGEUmiHLQBPxS+v2WSgDIwTgmjwrblgk1kBbtVId1p/453BAPR+5fJyKuQGQ49KLDWvnLSNQJse8e+SiunI/UcAQ5aTBo6ncj+HMLmGBH04WOqVkm+qPnQkwYBKR1GEpXcXOfpNVAOnSQmJS8euloqxd1fWLZUi2I4JCkvySWN/psMd8HDJhzyD/DdW5fBAFvIzvqKLsErOwcRkKUXT8D5CJdpkCvEG7Szz0r6qVFE6q0faCSxuV05kO8/GUBdOlNkL0wStgd/reRSgCE0FWPhoXfiS5Eg47P6CH8TBlSc+RSP31RCgjwytR5J0riVjsyh60AH3uVgKFPipkiQ/CBAyoUNsVvhE1HkL+SM6Gc6kW0QJrnSHENDa8J9jiYal07ND3uc75GAEkl4GWBkufc8hmsHYQeoUs3vb26TYfeoxBE6NBHxctbKwFV2eFvsdcU/2FdGsv/USX3nd01IfweWHx7i+qm6VmQ4ULBTAo+JrKjgHLXv386gveoiPIo1pEN5d4zyLVHnYYZYVkyjBAgmLUZzV3XPSHo6IMoe4p0U8Z6d/R7VRIoSwsINl5VzVSEXfdcL8P+gYPJD/CuEuAqus/FaQW70Vld/47EOiCawZRAiSBrZ+yooFy7+VG0yHcX4l8eTXLpQn0oIADxIUMBeoDtrsHW87EdsvtvbxgQSResFIHjRFZtj6KEX+ucgZ0D9+iL89avBCLvBMQ5RCUU3pOwvmVSwKwPMNWFoHvSTrXoCenqi8FwZMN7rYEOEN4bJnFBRcK4gi21nClKFOYZ7ZJLYxKwDRYEeXJs1tl92fv9tq/nQkguSVgF9FPonquwBi1ssdbxApQcgkvIAHbpdADKHsLw/C430332xJ8JYSJ6Z2emUHg6ehBCwB0JsQU1ENgmKz2WouXmWCUjKN4CYGOBqn4IWLlmxPTZuYUOh/Kqg6hnY/clDrbsh0jTsMe/lf0oflbRjYAlIiTXYRy3ImfbEN76xG+QT8c5KZPEVBKjKRgFY9vf4KTpkL2F1Ia6fK+2xTrvX5bmnO1Lvd6nkno8nxp6jkEBkOMNwi1GnS5MopWs7c6f9mMoKmlM4sDctT5VHo/Hi4DKgTF8LnLqPQbHLMNahn859fKCESuoLqtoBZC2zfj5LtHsun8+n19fX3/KOVXhyQLkyzknJylTcBw4j6GoHYCBLi/lNRKGC61fQZHA8yJe7AafzV3/oZJei5GjEC8ak4Q8XsobHFrJ2x9IYXtzjQAFpibC+kmUE3f6tJ4P0LGWU/c/Wi/ofYrzdR9G4eIqU54PhXoA42oXRi49BCNY2VCUPIgxiB47AYCC7HB8vgzBpAwgEVChSn2hiayfcZF8zikPOUXGIaBMDQBzUtEfA0Yg1Mp+YqU+eVVIRW8GiO8pIlNCGPfwnwg7RWiL+J+BEY3FK3wVTc7Hw9YPXaGkkDKZxAO0VTn1ojDaqaU1+lOqHuoVffkDducA9e4Th1sApnswouIEByhD5iRBe0TAMSzj85P8IAW3Rjp/prYL7E4CQu0IA033s1C/lUIO5QMBEQQOlHOhnogxciC+12k3l3DffqyXx01JP8p8CemsQ/9yGcwBFfk/Wqz6T1UU/3cAAAAASUVORK5CYII=',
        blocks: {
            line: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAIAAADZrBkAAAACuklEQVR4Xi2MzW4cRRSFz/2p6p4eT2zjjE2MHBQWvAALNjwDS8SOFc/FIyCxQgh2LFHkBQgJyQo/wXKCEZ7x9E9VV93LBPNJ516dxXfo/U8+HTZ9uzrZympTjLo4DbtDiBpXSBGMocI5lkaMhKv0fz+LVX9P6ZuvvxqBby+3L3eJu6Zt27DL4qgIWW0KsxPC3Kjxz5fPT3z48csvtHSHd0C/1374RddPX/9xK6JdcjEutNfqFJMBsQat3By+i/7VxqJ6c/zd5avvn1+N7dl2V9Ad92M2ViI2ksyWSJ2QXQXc5/H25jYcHHKl1YYebcJqw/rW03OOzfr89NHbR8vH3Wq9Olofrddni+XhycXJhBSkKFLxWeHBZFFDVyD343Rzc/0XeYSXeXZIJaam2V9pLsBOtRIKYAorXqvAg8giaKsCKw1zJAGFbKhwtxrdG9E3BQxAIW1xyYUKUIwNxMAwDEwuSgA5iAw2l5ITOeE/GLRMihIOsnQZnKqOlWXRUWidyYkFpCxtCF1s8D+seBggYVYRhChkqqjmld3NKhhu1XKuKUf4g6egEitkvqc5ewo+3ZOVZJO4Q4OSEDGRRyAQkYH8QfMUCR07KS8DIsO8xhjcnUXnSv4GMRYShSUG2MFCeby/awOJFyvF3TU22ckk3E3FQjci+mJVggwz12rKVEvWOk3dwWpIr7ezrU4XZ+9ciBAxWCVlhDb2o5cZTgBR17R3w45hgg8/fu+Dj643adZVXzCmebsbp5T6fhr2L9W8z5xLNsyDpl2btrsXPynS9NvVVc28xzMLPDCUYbVUMy+TMgdinupCEapdvPv0lqGwjW9fHsXjF39eIzRpnFbLdiy918qCWmclnq3mcWhVnzw5v+83XYz0+LPP/8lL704NB3BBdbCDKlChAq8QgRvmGSFouyo3vz7Lr/8FMHqie3VCpNQAAAAASUVORK5CYII=',
            square: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAIAAADZrBkAAAACfUlEQVR4Xh3LvY5cRRBA4VNV3ffe+fEaZARgWfBExEhEZEg8A69AgkC8AzEiICMgJCdAxggsGRuW3Z3dmenbXVWskT6d7MiXnz3qclu87esm1qW3NteRuqaCIAmQABwPvPU2NyeaUa6P159/9QV3l/HqlWqN41El0PaadEJAoZBKqS9+eybL/tvvvi8PrHA4ku33pz9v5rafKmc0I7QhbiGaJSikyeCh1T7OG6fMluOvP/5+8csb+/RxhUtlEXA5KUMpiEpWwZKU3D799Zl1CtpEjsoh++HRxUMBVhgry4yUXLvstnE8qWi/HZndTEwpppj5XLIg7fZ4+fJlb91jzSlGhjc2O7RMj995nApRIRW0BHRl9UXLona6PYiGGKpaa50qPtQ96/5BCgkpBKghpI7VJZK5bmYju5IxQkZONqtbu+txd5ZEQFI0KaRRJxEZY0wjh/dqKqbZ/z9tqmJIqpQSnaD8T9sYkGqWmuTIZHgoWYupKh4SqSQimqrJfeV1gTECF4NKmUHwDFRqNSuJjMTxFQANNKGUyTBxYqSX6J6USdc1BquYFhEMFEQCUvSeC5rheL/Y74kAvX/OPXRZojIkqHkaLpMwWdYJm1fHlZIJotdXN7tFtczvvfuB5MIyRZwQ11pora2BTKOvva3zPKtStpsKLNNmmcr53xtFMtLaGLSR61IKKaPHenO1L2+qThqrn5FvPuGjjz8s4x+NI6Ptdvtxckywo0cjstZZZfZhNhSWNuoPP/5Udhebc8+tbp4//zPd6dcaGtJtWtUiVkhTWSJkTn3y5P15M19ervL1p9UZFzX9zHYmO1Ph3KFQK+rkIBMScbY7DicOzn+QRoW5iamqhgAAAABJRU5ErkJggg==',
            arrow: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAIAAADZrBkAAAACsklEQVR4XhWPu25bVxREZz/OuQ9RVKhXjBSu8gX5h+QD0udD8ilByjRpUqZxa6RxY8DuYlhwHhAc2qRIieK9vOfsvUMNBoPVrGLoh6+++7y7mbFfTNIVdShCxCGuAgE8tSilhOVWU6lrtPjPK33ffvvb6xd4KG9/+f1kenIYoAgOIc+AF9u5Rdteuvubty9LU168fqW9z7AFanr36uPZSOIgWHB1IJABKE0keSr3QdLolXitjw1n717++sfPP/7Ula6pfbYmeU7eaBybnzg6ewwpT8DW/vth2acFkzf9Pi0OOU1+/eVlM2/m14vZ+Rfz88t+Np+fnZ8tns1OL66vLss0FIughgppDumLzIpmYBrHz+uVsZVSsraorJoMQU5dOs3K3jaiSXGciNYoG3cQcbJjwiKC3Ny9ljhEEUlGBnh4IJwR+qQ6q7FbuMPBLEQCVjaYA0mYwcyIY6p5qYFQQqOexRMhHOqg6sV8mtiqQki51gSHV6YAMYMMxgIC2IOcBcIsQipBBCIjNqEQBWARjjAOHAtSh1eqk9bqLpBKQUSZE4ccIqpJFnWQiRSKypjUA1DgEDw5janNkgA4zKmGQtpgsNqhMoRCmRni4AgKJh6DB842TffENWWqU+m448eYje186GZT31nXo5EqPhUGIqoGwd1gtWFtRBeLxcXF1TydYh+td1E5lMdpOOxGr9bPOzdrtFGWk2lAg84e9/fYbGmsync0xD7mfkITgUMbJaKc26GOVn2sB61Gf958OE992wsoQjSSjBHUwEmlISTsxjEsOIsxP3v+/M3tey3wrRVh29wuA2Vo3Xoe3KP6Endhzh3v92NjKefcXZzETKxR+Sa+vt2s3/3z127YrYfN+mGz2m4+rT+tNuu77d3yfvn3+vbh8eH4bb3dLrer97c3q7L6H15gvODKB5u4AAAAAElFTkSuQmCC',
            rightHook: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAIAAADZrBkAAAACqElEQVR4Xh3PO2tlZRSH8f9a73XvPTkTExl1dLAIKFpNY2Fl4UcT7G3FwmIaO3sbEfQjWAxDAkIORDHJyXFf3stayzC/9qke+ubrF0u7zqwfsJNFXEKtCAJriBGBoB0gNAZ5WIekcC0f+br8++3Pr1AP5Y/f6Xgfx4RqoB2kI27QgkYQw5BA2L95M+vTn379y0c2HI+Y5Ob6z3B/M/ZmlTieV7ISj9rXZ2n338NciEMedGshJacbZ9X16vK3H76vh/1paCOWiWvos5M1U9llLIebKeqz0zG4PmZ/dfl6XWc/9p4Ox/fMwnIcXrwL6ohPg54OZhYTUUc+w1YQ83z3kMadEKdp8EmE54cnKifTBLOr17cqtwIf4gAuKhUd8Lh4+TKPowjyOK1l8cxAm9nK2nWnsQeAyNtgldnClCLntkrRrTBcFyPjYRg8AEAY4JDhkhp7uOBIRbyqrmK+KcBm1qU30UbSxHcGnIGpKYk63dQxnC2sPcWo2slZUCCGYXPkkBnUqjcCSAGFsaOUQk5koqUqPFV5G2sDai+tG3vv6ZF3BojBzFrvpeij4KtDZTjPXTWGBA91uWJTo1WkqTAZoAbpT3ejd0YOBW1zoAHE/tG8FThm52KOLnDv1dA8DACTwbRU6TxSaX1WZII/1qBgwLvWtgN6ieQc2hC9hwLGTFS35eTJ7uOL5xxTkZI44l7BDGpwBke8FFCA9Pj2EAipHbd8Qre3fxOMDWykGryyM2Vv67JwmCJ7cQRRM6JXX+CrLz97/uHJ4bgHFUdKBi8B5gECLLAys4grAkr5Vt758ZdLTwhuPNvfHP7ZPzjrBAHYLBlChwPQ+jrtpq2UJv3i0/clni/Y03efYzxPvZdBkAzeAKATukENAoTA86ynZ3GrVQl3Eu/yJ/8D33mmeKR3Cz8AAAAASUVORK5CYII=',
            leftHook: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAIAAADZrBkAAAACq0lEQVR4Xg3HO24mVRBH8X/Vrfv4ut02BjS8GSBAQuyCmE0gwSLICFgCBLASAhLiiQjQICFBMAMSFn63v+77qCp89EsO5U+/rus/0zQPPW8thXRQABLAjtHAAZ2gzGk2HRCG1YMPwgdf/fnXj1fX+O7732s/A0kzbwEUCW5Fgh8HGVdFTOn5H79NiZ/98rPATBVS8OzX5xTehYsxd3KFwXcGEljVOGVJ8a7W4Q4YT8sb33z70xdf/oB8HufzhjI0m2a3JLykuAyV4RGxrLVN5+f/3dxSKXJc59qfUk77djhdXhGx0/mMAg/rpm30bXq97Fp3su1mrMe19e46BLLw9Nb1w0WaZoO+fPmCNbTW3D1Fcjci77Cnn3wMhCBpWU7vuwqo3evgaar6QHRqfRXKhQiELFG1O6H3MZUyLpTIx24UMkNASbqZwZiN3AQsgKhZ6/VYR9OcDw/blg5xmKqPnIRBLQUZw4iIWQKydRcEcvLOOZ2A80Nv8SRv1iiHQWPXewbztm3MbObMYgpGCMwiwgwAIhJCAGBmjMD+KDJJThJzTI8TKZm5E9S7U3c0s6PrkVujvYbm0Qr1iceJ+GZ9772OMawNDNAgVm8cADcAQSgzCSiChZj8MRLwXCTPufQEC8SlVKPWNTJJ4BTDcds58tCaMsVoIQ8Nq2AgS97uVg2h7v29Dz86W86MGqB922F+djjsdbWE0eu233fbwF0opd57KYU43l5ewQ43N7d17CwUHJnlsl4QmUwkJJKZ5uXaLdBrnzl6EGbTbV2nVIiMuMU44EMwMjn76GMHeWAKhKuLvyXoC+F3IvXrfy9zmO/WKyUM6c1riWUY7btKiA+9UkyvPnk7KFMPtLz/uS9vrjVFLNqTUSEJNLN5hQL7gIXpsGytO0ekCduR/fg/hV+olVqSm3YAAAAASUVORK5CYII=',
            rightZag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAIAAADZrBkAAAACt0lEQVR4Xg3LOW6eVRQG4PcM937DP9hYlkgUQ6RQsAiEWAC7YCWUtGyAFqhpEHRI0CCBIjpHJBSxEhzkxP6H+93pHPz0D3358IsbuyY/XlCs7/YU5zjFZgdGp07BEDq4KXwA0PvuqIfDGbT0u69/+hZvr/DrH1gc1WENwUAGM3RDcXQGRbA9/e3nJPX3F5dqdY/ja8ztx++/+YhX5S6P47REODW2Pra+qsbOR5VFWwjHVPKJJF1FevPn019++O7jeYo3h5VG8Ua5OLnCh25T7+RwYfZavVz993q5OehccfImnV3nqdLjD54gTiDAF3cnEJrBAbZNSS//vTKP1FcTk0ppsfYzkpEt58Pls8vb29vTabDWYZJzjsqnJ+vHjx9tVvf3qG3QVpXJ0Y5L2xWmYbPt7W5WeK7RKUqICic7poQY6q4Fw2AUDSykUGUlQ0cUy3XUqCxm8Obe4SypVoRwOC7kLE5szIoBHsmlO6NJO3Qv5GEozI3IiE25K2EeLAiYADiBOwi51NLdCRrGcQ5xTLVUcg1BVeFszXtaWinkIAcALigQDwISby0Vq3dpR8IA2r1cKNfZRTq9F2c1iLuYqQcHg2Dmvbam08QUG1V07zASqDCLouSSE0tojCbGablFoMgkpMxht9QukvICASIXtNLy7rDDakVDeGepzpIUup1mlIZs5Dbo9OknnwEELGgZJgBhHHDYIx/v8/r98+d/P2cVVSOkitRPN9uXfz0j2rRSh1CZDKRmFomgnns62Wyvrq+ncTy2JJ/rRXv76vx04ymr83pci9gYRYISS4N3cgRKvYb15EHOHzy8fPaPgtMe8zHG5y8uH6y3wa7LPbNOIIkQRpCOZt7KzatHFx+Ww0Ki9NX8ZHWy7Sltchtz2aj32nicjZkpLr1m760vp5tN2e8jtHX0Yf4f3da+1L4oEEQAAAAASUVORK5CYII=',
            leftZag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAIAAADZrBkAAAACtElEQVR4Xg2Lu25cZRhF93f5L3OOPeOQmJuDhEIHDa/CC/B4SHSICqS0CERDE2MJAYKEmITEcewZz/nvH7O0taq16eTL++P66nhCDegOAEhghFYhDDM0hSgowQkKkDIwJi0pP/72G9j24vIi8S7nN34lYG1VyOKg0uVKFFIiY/XL+R8s977+6nvttBMkovTDz9/Np3G3fTlvYkoFJswzaxtya2haQ0nUdGa2PLY6GD/9+vjJ+Y+bD+K2vFxtrLS3cQq9t1rLwUZ7IpuOAmQQ8u9/PgFDWbG5708/jK/Ti9OzExErhYVU1TMrCZqV7fY2Or2+uQ5H7zgPMNQF5HL7+upZeFeHLU//etrraM2YedQBAXsnoPXHn8yrcHVzE0LICWoECTQf+SG19dRqK3tMUxSRZtkIZtqbsblW+vF8/Pzy1XoNvdthjDas7He7swdno+JkM21v997r6A1MIKNBwiCiXssqhNsbcAwQosPTeek1MzBa947NmnewYcGR8Oh1sZG8s5LvQoACaK0Yegi+965MNWXnxIAx0AdyWnqDMjuh1KoIkYGNUPqh0zHaYcpwoin1ZiBFjFBVEWLm1ppzTkQAsAQYQdwhTap+dBCRXzkW6g2lIpdqRkYE1v1+AZgEuiRo8G2YaCBo75ZylSCdiMnoYPVefO1k8AYm7rlAjUDi7/ZN18HIffbp5zFO22Vf0bzXfsBQUu4DIJ6P1/t/XowB9RGl4Xj94M62zy+vou2sDUySyhJjTDXpAeL9YFKXaxYnuoL2BRfnzxghj7YKm4liHkszGkOUjtxIYjKM9ncpTnPr4+HDj8j+U5/B5R5k/vfv344m6rfXHb3NSqKvdrs5xKUWeN95nUd69Oi9vpQI0PtfTAn7eE/eLF0Y92eUjrcDISBmrBjbgkrIDPFYCfoCLPgflXOjuIEFgMYAAAAASUVORK5CYII='
        }
    },
    'modern': {
        background: '#000000',
        backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAIAAAC0Ujn1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjZTg0NzU4MC00ODk3LTRkNjAtOWNhYi1mZTk1NzQ5NzhiNjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTEzOEQwMDc5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTEzOEQwMDY5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDplNDRjOWZiNC0yNzE5LTQ3NDYtYmRmMi0wMmY2ZTA4ZjAxMmUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzMwNTNEOTk5MDM1MTFFNDlBMzlFNzY4RjBCNkNENzMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Y01+zAAAAMklEQVR42mJgGAWjYBSMgkEJGIlUd+j/WjjbjjGYGC1MtHP10DR6FIyCUTAKBikACDAA0NoDCLGGjH8AAAAASUVORK5CYII=',
        primary: null,
        secondary: null,
        stroke: null,
        blocks: {
            line: '#fa1e1e',
            square: '#f1fa1e',
            arrow: '#d838cb',
            rightHook: '#f5821f',
            leftHook: '#42c6f0',
            rightZag: '#4bd838',
            leftZag: '#fa1e1e'
        }
    },
    'retro': {
        background: '#000000',
        backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAIAAAC0Ujn1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjZTg0NzU4MC00ODk3LTRkNjAtOWNhYi1mZTk1NzQ5NzhiNjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTEzOEQwMDc5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTEzOEQwMDY5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDplNDRjOWZiNC0yNzE5LTQ3NDYtYmRmMi0wMmY2ZTA4ZjAxMmUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzMwNTNEOTk5MDM1MTFFNDlBMzlFNzY4RjBCNkNENzMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Y01+zAAAAMklEQVR42mJgGAWjYBSMgkEJGIlUd+j/WjjbjjGYGC1MtHP10DR6FIyCUTAKBikACDAA0NoDCLGGjH8AAAAASUVORK5CYII=',
        primary: null,
        secondary: null,
        stroke: '#000000',
        innerStroke: '#000000',
        blocks: {
            line: '#fa1e1e',
            square: '#f1fa1e',
            arrow: '#d838cb',
            rightHook: '#f5821f',
            leftHook: '#42c6f0',
            rightZag: '#4bd838',
            leftZag: '#fa1e1e'
        }
    },
    'monochrome': {
        background: '#000000',
        backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAIAAAC0Ujn1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjZTg0NzU4MC00ODk3LTRkNjAtOWNhYi1mZTk1NzQ5NzhiNjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTEzOEQwMDc5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTEzOEQwMDY5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDplNDRjOWZiNC0yNzE5LTQ3NDYtYmRmMi0wMmY2ZTA4ZjAxMmUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzMwNTNEOTk5MDM1MTFFNDlBMzlFNzY4RjBCNkNENzMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Y01+zAAAAMklEQVR42mJgGAWjYBSMgkEJGIlUd+j/WjjbjjGYGC1MtHP10DR6FIyCUTAKBikACDAA0NoDCLGGjH8AAAAASUVORK5CYII=',
        primary: '#ffffff',
        secondary: '#ffffff',
        stroke: '#000000',
        innerStroke: '#000000'
    },
    'aerolab': {
        background: '#ffffff',
        primary: '#ff7b00',
        secondary: '#000000'
    },
    'gameboy': {
        background: '#C4CFA1',
        primary: null,
        secondary: null,
        stroke: '#414141',
        innerStroke: '#414141',
        innerSquare: '#000000',
        blocks: {
            line: '#88926A',
            square: '#585E44',
            arrow: '#A4AC8C',
            rightHook: '#6B7353',
            leftHook: '#6B7353',
            rightZag: '#595F45',
            leftZag: '#595F45'
        }
    },
    'vim': {
        background: '#000000',
        backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAIAAAC0Ujn1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjZTg0NzU4MC00ODk3LTRkNjAtOWNhYi1mZTk1NzQ5NzhiNjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTEzOEQwMDc5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTEzOEQwMDY5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDplNDRjOWZiNC0yNzE5LTQ3NDYtYmRmMi0wMmY2ZTA4ZjAxMmUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzMwNTNEOTk5MDM1MTFFNDlBMzlFNzY4RjBCNkNENzMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Y01+zAAAAMklEQVR42mJgGAWjYBSMgkEJGIlUd+j/WjjbjjGYGC1MtHP10DR6FIyCUTAKBikACDAA0NoDCLGGjH8AAAAASUVORK5CYII=',
        primary: '#C2FFAE',
        secondary: '#C2FFAE',
        stroke: '#000000',
        strokeWidth: 3,
        innerStroke: null
    },
    'water': {
        background: '#000000',
        backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAIAAAC0Ujn1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjZTg0NzU4MC00ODk3LTRkNjAtOWNhYi1mZTk1NzQ5NzhiNjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTEzOEQwMDc5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTEzOEQwMDY5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDplNDRjOWZiNC0yNzE5LTQ3NDYtYmRmMi0wMmY2ZTA4ZjAxMmUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzMwNTNEOTk5MDM1MTFFNDlBMzlFNzY4RjBCNkNENzMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Y01+zAAAAMklEQVR42mJgGAWjYBSMgkEJGIlUd+j/WjjbjjGYGC1MtHP10DR6FIyCUTAKBikACDAA0NoDCLGGjH8AAAAASUVORK5CYII=',
        primary: '#3399ff',
        secondary: '#3399ff',
        stroke: '#000000',
        innerStroke: '#000000'
    },
    'alimentation': {
        background: '#000000',
        backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAIAAAC0Ujn1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjZTg0NzU4MC00ODk3LTRkNjAtOWNhYi1mZTk1NzQ5NzhiNjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTEzOEQwMDc5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTEzOEQwMDY5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDplNDRjOWZiNC0yNzE5LTQ3NDYtYmRmMi0wMmY2ZTA4ZjAxMmUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzMwNTNEOTk5MDM1MTFFNDlBMzlFNzY4RjBCNkNENzMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Y01+zAAAAMklEQVR42mJgGAWjYBSMgkEJGIlUd+j/WjjbjjGYGC1MtHP10DR6FIyCUTAKBikACDAA0NoDCLGGjH8AAAAASUVORK5CYII=',
        primary: '#009933',
        secondary: '#009933',
        stroke: '#000000',
        innerStroke: '#000000'
    },
    'energy': {
        background: '#000000',
        backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAIAAAC0Ujn1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjZTg0NzU4MC00ODk3LTRkNjAtOWNhYi1mZTk1NzQ5NzhiNjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTEzOEQwMDc5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTEzOEQwMDY5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDplNDRjOWZiNC0yNzE5LTQ3NDYtYmRmMi0wMmY2ZTA4ZjAxMmUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzMwNTNEOTk5MDM1MTFFNDlBMzlFNzY4RjBCNkNENzMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Y01+zAAAAMklEQVR42mJgGAWjYBSMgkEJGIlUd+j/WjjbjjGYGC1MtHP10DR6FIyCUTAKBikACDAA0NoDCLGGjH8AAAAASUVORK5CYII=',
        primary: '#ffff1a',
        secondary: '#ffff1a',
        stroke: '#000000',
        innerStroke: '#000000'
    },
    'ict': {
        background: '#000000',
        backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAIAAAC0Ujn1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjZTg0NzU4MC00ODk3LTRkNjAtOWNhYi1mZTk1NzQ5NzhiNjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTEzOEQwMDc5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTEzOEQwMDY5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDplNDRjOWZiNC0yNzE5LTQ3NDYtYmRmMi0wMmY2ZTA4ZjAxMmUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzMwNTNEOTk5MDM1MTFFNDlBMzlFNzY4RjBCNkNENzMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Y01+zAAAAMklEQVR42mJgGAWjYBSMgkEJGIlUd+j/WjjbjjGYGC1MtHP10DR6FIyCUTAKBikACDAA0NoDCLGGjH8AAAAASUVORK5CYII=',
        primary: '#ff751a',
        secondary: '#ff751a',
        stroke: '#000000',
        innerStroke: '#000000'
    },
    'mix': {
        background: '#000000',
        backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAIAAAC0Ujn1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjZTg0NzU4MC00ODk3LTRkNjAtOWNhYi1mZTk1NzQ5NzhiNjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTEzOEQwMDc5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTEzOEQwMDY5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDplNDRjOWZiNC0yNzE5LTQ3NDYtYmRmMi0wMmY2ZTA4ZjAxMmUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzMwNTNEOTk5MDM1MTFFNDlBMzlFNzY4RjBCNkNENzMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Y01+zAAAAMklEQVR42mJgGAWjYBSMgkEJGIlUd+j/WjjbjjGYGC1MtHP10DR6FIyCUTAKBikACDAA0NoDCLGGjH8AAAAASUVORK5CYII=',
        primary: '#e6e6e6',
        secondary: '#e6e6e6',
        stroke: '#000000',
        innerStroke: '#000000'
    },
};
},{}],5:[function(require,module,exports){
var jsonLoader = require("./jsonLoader.js");
var linesSubLevel = require("./levels/linesSubLevel.js");
var pointsSubLevel = require("./levels/pointsSubLevel.js");
var scoreRegister = require("./scoreRegister");

var scoreRegisterInstance = new scoreRegister.scoreRegister();
var levelData;
var globalConfig = jsonLoader.loadJsonFile("conf/global_config.json");

function lightenDarkenColor(col, amt) {

    var usePound = false;

    if (col[0] == "#") {
        col = col.slice(1);
        usePound = true;
    }

    var num = parseInt(col, 16);

    var r = (num >> 16) + amt;

    if (r > 255) r = 255;
    else if (r < 0) r = 0;

    var b = ((num >> 8) & 0x00FF) + amt;

    if (b > 255) b = 255;
    else if (b < 0) b = 0;

    var g = (num & 0x0000FF) + amt;

    if (g > 255) g = 255;
    else if (g < 0) g = 0;

    return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);

}

function setLevelName(currentLevel) {
    $("#level-name").text($.i18n("level_".concat(currentLevel)));
}

function setBlockTextColor(blockTextColor) {
    var blockText = document.getElementById("block-text");
    var lightBlockTextColor = lightenDarkenColor(blockTextColor, 70); //30% brighter
    blockText.style.color = lightBlockTextColor;
}

function setBlockText(shapeType, currentLevel) {
    var blockText = $.i18n(shapeType.concat("_level_").concat(currentLevel));
    var blockTextPos;
    var arr = blockText.split("~");

    blockTextPos = getRandomInt(0, arr.length);

    if (typeof arr[blockTextPos] === "undefined") {
        blockTextPos = 0;
    }

    $("#block-text").text(arr[blockTextPos]);
}

function setSpecialBlockText(prefix, currentLevel) {
    var blockTexts = $.i18n(prefix.concat("_level_").concat(currentLevel));
    var specialBlockTextPos;
    var arr = blockTexts.split("~");

    specialBlockTextPos = getRandomInt(0, arr.length);

    if (typeof arr[specialBlockTextPos] === "undefined") {
        specialBlockTextPos = 0;
    }

    $("#block-text").text(arr[specialBlockTextPos]);
}

function hideBlocktext() {
    $("#block-text").hide();
}

function showBlocktext() {
    $("#block-text").toggle();
}

function getSubLevelText(level) {
    var levelText = $.i18n("description_level_".concat(level));
    var toret = "";
    var arr = levelText.split("~");
    for (index = 0; index < arr.length; ++index) {
        var str = arr[index].replace(/^/, "<li>").concat("</li>");
        toret += str;
    }
    toret.replace(/^/, "<ul>").concat("</ul>");
    return toret;
}

function showSubLevelDescription(currentLevel, levelText) {
    hideBlocktext();
    swal({
        title: "Nivel " + currentLevel + ": " + $.i18n("level_".concat(currentLevel)),
        html: levelText,
        allowOutsideClick: false,
        allowEscapeKey: false,
        timer: 6000,

        onOpen: function() {
            var b = swal.getConfirmButton();
            b.hidden = true;
            b.disabled = true;
        }
    }).then(function() {
        swal({
            title: "Nivel " + currentLevel + ": " + $.i18n("level_".concat(currentLevel)),
            html: levelText,
            allowOutsideClick: false,
            allowEscapeKey: false,
            animation: false
        }).then(value => {
            if (value) {
                $game.blockrain("resume");
                showBlocktext();
            }
        });
    });
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return (Math.floor(Math.random() * (max - min + 1)) + min);
}

function getRandomDecimal(min, max) {
    return (Math.floor(Math.random() * (max - min + 1)) + min) / 10;
}

function genRand(min, max, decimalPlaces) {
    var rand = Math.random() * (max - min) + min;
    var power = Math.pow(10, decimalPlaces);
    return Math.floor(rand * power) / power;
}

function saveSubLevelScore(level, subLevel, score) {
    var subLevelNum = subLevel.split("_")[1] - 1;
    scoreRegisterInstance.setSubLevelScore(level, subLevelNum, score);
}

function saveSubLevelLines(level, subLevel, numLines) {
    var subLevelNum = subLevel.split("_")[1] - 1;
    scoreRegisterInstance.setSubLevelLines(level, subLevelNum, numLines);
}

$(document).ready(function() {
    var levelScript = sessionStorage.getItem("levelFile");

    var levelsDefintionDir = globalConfig.levels_definition_dir;

    var levelPosLoaded = 0;

    levelScript = levelsDefintionDir + levelScript;

    levelData = jsonLoader.loadJsonFile(levelScript);

    var levelsParams = levelData.levels_params;

    var params = levelsParams[levelPosLoaded];

    var subLevel = new linesSubLevel.LinesSubLevel(params);

    var numLinesLevel = 0;

    var randNum;

    var specialBlockTextShown = false;

    $game = $("#tetris-ddhh").blockrain({
        speed: subLevel.getSpeed(),
        difficulty: subLevel.getDifficulty(),
        theme: subLevel.getTheme(),
        playText: $.i18n(globalConfig.play_text),
        playButtonText: $.i18n(globalConfig.play_button_text),
        gameOverText: $.i18n(globalConfig.game_over_text),
        restartButtonText: $.i18n(globalConfig.restart_button_text),
        scoreText: $.i18n(globalConfig.score_text),
        blockWidth: globalConfig.default_block_width,
        obstacles: subLevel.getObstacles(),
        numObstacles: subLevel.getNumObstacles(),
        pauseButton: subLevel.getShowPauseButton(),
        shapeTypeProbs: subLevel.getShapeTypeProbabilities(),

        onStart: function() {
            $game.blockrain("touchControls", true);
            $game.blockrain("pause");
            setLevelName(subLevel.getSubLevel());
            var levelText = getSubLevelText(subLevel.getSubLevel());
            showSubLevelDescription(subLevel.getSubLevel(), levelText);
        },

        onRestart: function() {
            $game.blockrain("touchControls", true);
            setLevelName(subLevel.getSubLevel());
            $game.blockrain("resume");
        },

        onLine: function(lines, scoreIncrement, score) {
            numLinesLevel += lines;
            if (
                subLevel.isLevelCompleted(numLinesLevel) &&
                !subLevel.isFinalLevel()
            ) {
                $game.blockrain("pause");
                $game.blockrain("levelOver");
                hideBlocktext();
                console.log("level passed");
                saveSubLevelScore(levelData.level, subLevel.getSubLevel(), score);
                saveSubLevelLines(levelData.level, subLevel.getSubLevel(), numLinesLevel);

                numLinesLevel = 0;
                levelPosLoaded++;

                params = levelsParams[levelPosLoaded];

                subLevel = new linesSubLevel.LinesSubLevel(params);

                swal({
                    title: $.i18n("level_won"),
                    type: "success",
                    text: $.i18n("next_level")
                }).then(value => {
                    if (value) {
                        // Restart the game
                        $game.blockrain("speed", subLevel.getSpeed());
                        $game.blockrain("difficulty", subLevel.getDifficulty());
                        $game.blockrain("obstacles", subLevel.getObstacles());
                        $game.blockrain("numObstacles", subLevel.getNumObstacles());
                        $game.blockrain("pauseButton", subLevel.getShowPauseButton());
                        $game.blockrain("theme", subLevel.getTheme());

                        $game.blockrain("start");
                    }
                });
            } else if (
                subLevel.isLevelCompleted(numLinesLevel) &&
                subLevel.isFinalLevel()
            ) {
                $game.blockrain("pause");
                $game.blockrain("levelOver");
                hideBlocktext();
                console.log("final level passed");
                saveSubLevelScore(levelData.level, subLevel.getSubLevel(), score);
                saveSubLevelLines(levelData.level, subLevel.getSubLevel(), numLinesLevel);

                swal({
                    type: "success",
                    text: $.i18n("game_won_".concat(levelData.level))
                }).then(value => {
                    if (value) {
                        if (levelData.next_level_file != "") {
                            var levelsUnlocked = JSON.parse(sessionStorage.getItem("levelsUnlocked"));
                            var levelUnlocked = {
                                code: levelData.next_level_code,
                                shown: false
                            };
                            levelsUnlocked.push(levelUnlocked);
                            sessionStorage.setItem("levelsUnlocked", JSON.stringify(levelsUnlocked));

                            var levelFiles = JSON.parse(sessionStorage.getItem("levelFiles"));
                            var nextLevelCode = levelData.next_level_code;
                            levelFiles[nextLevelCode] = levelData.next_level_file;
                            sessionStorage.setItem("levelFiles", JSON.stringify(levelFiles));

                            var colors = JSON.parse(sessionStorage.getItem("colors"));
                            colors[nextLevelCode] = globalConfig.unlocked_level_color;
                            sessionStorage.setItem("colors", JSON.stringify(colors));
                        }

                        location.replace("index.html");

                    }
                });
            }
        },

        onNext: function(next) {
            if (typeof next != "undefined" && typeof $game != "undefined") {

                if (specialBlockTextShown) {
                    $game.blockrain("theme", subLevel.getTheme());
                    setBlockTextColor(subLevel.getBlockTextColor());
                    specialBlockTextShown = false;
                }

                randNum = genRand(0, 1, 2);

                var specialPieces = subLevel.getSpecialPieces();
                var accumulatedProb = 0;

                if (typeof specialPieces != "undefined") {
                    for (var i = 0; i < specialPieces.length; i++) {
                        accumulatedProb += specialPieces[i].piece_probability;
                        if (randNum <= accumulatedProb) {
                            $game.blockrain('theme', {
                                background: '#000000',
                                backgroundGrid: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAIAAAC0Ujn1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjZTg0NzU4MC00ODk3LTRkNjAtOWNhYi1mZTk1NzQ5NzhiNjkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTEzOEQwMDc5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTEzOEQwMDY5MDQyMTFFNDlBMzlFNzY4RjBCNkNENzMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDplNDRjOWZiNC0yNzE5LTQ3NDYtYmRmMi0wMmY2ZTA4ZjAxMmUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzMwNTNEOTk5MDM1MTFFNDlBMzlFNzY4RjBCNkNENzMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Y01+zAAAAMklEQVR42mJgGAWjYBSMgkEJGIlUd+j/WjjbjjGYGC1MtHP10DR6FIyCUTAKBikACDAA0NoDCLGGjH8AAAAASUVORK5CYII=',
                                primary: specialPieces[i].color,
                                secondary: subLevel.getBlockTextColor(),
                                stroke: '#000000',
                                innerStroke: '#000000'
                            });
                            setBlockTextColor(specialPieces[i].color);
                            if (typeof specialPieces[i].piece_type === "number") {
                                var lev = levelData.level + "_" + specialPieces[i].piece_type;
                                setBlockText(next.shapeType, lev);
                            } else {
                                setSpecialBlockText(specialPieces[i].piece_type, subLevel.getSubLevel());
                            }
                            specialBlockTextShown = true;
                            break;
                        }
                    }
                }
                if (!specialBlockTextShown) {
                    setBlockTextColor(subLevel.getBlockTextColor());
                    setBlockText(next.shapeType, subLevel.getSubLevel());
                }
            }
        }
    });

    module.exports.updatePageElements = function updatePageElements() {
        $("#tetris-header").text($.i18n("tetris_header"));
        $("#game-instructions").text($.i18n("game_instructions"));
        $(".blockrain-start-msg").text($.i18n(globalConfig.play_text));
        $(".blockrain-start-btn").text($.i18n(globalConfig.play_button_text));
        $(".blockrain-score-msg").text($.i18n(globalConfig.score_text));
        $(".blockrain-game-over-msg").text($.i18n(globalConfig.game_over_text));
        $(".blockrain-game-over-btn").text(
            $.i18n(globalConfig.restart_button_text)
        );
        if (!$game.blockrain("isGamePaused")) {
            setLevelName(subLevel.getSubLevel());
        }
    };
});
},{"./jsonLoader.js":6,"./levels/linesSubLevel.js":9,"./levels/pointsSubLevel.js":10,"./scoreRegister":21}],6:[function(require,module,exports){
function loadTextFileAjaxSync(file) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", file, false); // Replace file with the path to your file
  xobj.send();
  if (xobj.status == 200) {
    return xobj.responseText;
  } else {
    // TODO Throw exception
    return null;
  }
}

module.exports.loadJsonFile = function loadJson(filePath) {
  // Load json file;
  var json = loadTextFileAjaxSync(filePath);
  // Parse json
  return JSON.parse(json);
};


},{}],7:[function(require,module,exports){
var pageElementsUpdate = require("./gamePresenter.js");

var langImages = {
    gl: "assets/images/gl.png",
    es: "assets/images/es.png"
};

var langTexts = {
    gl: "Galego",
    es: "Castellano"
};

// Enable debug
$.i18n.debug = true;

var currentLocale = sessionStorage.getItem("currentLocale");

function set_locale_to(locale) {
    if (locale) $.i18n().locale = locale;
}

jQuery(function updateLocale($) {
    "use strict";
    var i18n = $.i18n();

    i18n.locale = currentLocale;

    i18n
        .load({
            es: "./src/i18n/es.json",
            gl: "./src/i18n/gl.json"
        })
        .done(function() {
            console.log("Internationalization files loaded correctly");

            $("#titleimage").attr(
                "src", langImages[i18n.locale]);

            $("#titletext").text(langTexts[i18n.locale]);

            History.Adapter.bind(window, "statechange", function() {
                set_locale_to(url("?locale"));
            });

            History.pushState(null, null, "?locale=" + i18n.locale);
            pageElementsUpdate.updatePageElements();

            $(".switch-locale").on("click", "a", function(e) {
                console.log("locale changed");
                e.preventDefault();
                History.pushState(null, null, "?locale=" + $(this).data("locale"));
                $.i18n().locale = $(this).data("locale");
                pageElementsUpdate.updatePageElements();
            });

            $(".languages > li").click(function() {
                $("#titleimage").attr(
                    "src",
                    $(this)
                    .children()
                    .children("img")
                    .attr("src")
                );
                $("#titletext").text($(this).text());
            });
        });
});
},{"./gamePresenter.js":5}],8:[function(require,module,exports){
/**
 @constructor
 @abstract
 */
module.exports.Level = function level(levelData) {
    var that = {};

    if (this.constructor === level) {
        throw new Error("Can't instantiate abstract class!");
    }

    that.getSpeed = function() {
        return levelData.speed;
    };

    that.getDifficulty = function() {
        return levelData.difficulty;
    };

    that.getTheme = function() {
        return levelData.theme;
    };

    that.getBlockTextColor = function() {
        return levelData.block_text_color;
    }

    that.getSubLevel = function() {
        return levelData.sub_level;
    };

    that.getNumLines = function() {
        return levelData.num_lines;
    };

    /**
 @abstract
 */
    that.isLevelCompleted = function() {
        throw new Error("Abstract method!");
    };

    that.isFinalLevel = function() {
        return levelData.final_level;
    };

    that.getSpecialPieces = function() {
        return levelData.special_pieces;
    };

    that.getObstacles = function() {
        return levelData.obstacles;
    };

    that.getNumObstacles = function() {
        return levelData.num_obstacles;
    };

    that.getShowPauseButton = function() {
        return levelData.show_pause_button;
    };

    that.getShapeTypeProbabilities = function() {
        return levelData.shapeTypeProbabilities;
    };

    return that;
};
},{}],9:[function(require,module,exports){
var level = require("./level.js");

module.exports.LinesSubLevel = function linesSubLevel(levelData) {
  var that = level.Level(levelData);

  that.isLevelCompleted = function(currentNumLines) {
    if (currentNumLines >= that.getNumLines()) {
      return true;
    }
    return false;
  };

  return that;
};

},{"./level.js":8}],10:[function(require,module,exports){
var level = require("./level.js");

module.exports.PointsSubLevel = function pointsSubLevel(levelData) {
  var that = level.Level(levelData);

  that.isLevelCompleted = function(currentPoints) {
    if (currentPoints >= 2000) {
      return true;
    }
    return false;
  };

  return that;
};

},{"./level.js":8}],11:[function(require,module,exports){
/**
 * cldrpluralparser.js
 * A parser engine for CLDR plural rules.
 *
 * Copyright 2012-2014 Santhosh Thottingal and other contributors
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 * @source https://github.com/santhoshtr/CLDRPluralRuleParser
 * @author Santhosh Thottingal <santhosh.thottingal@gmail.com>
 * @author Timo Tijhof
 * @author Amir Aharoni
 */

/**
 * Evaluates a plural rule in CLDR syntax for a number
 * @param {string} rule
 * @param {integer} number
 * @return {boolean} true if evaluation passed, false if evaluation failed.
 */

// UMD returnExports https://github.com/umdjs/umd/blob/master/returnExports.js
(function(root, factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module.
		define(factory);
	} else if (typeof exports === 'object') {
		// Node. Does not work with strict CommonJS, but
		// only CommonJS-like environments that support module.exports,
		// like Node.
		module.exports = factory();
	} else {
		// Browser globals (root is window)
		root.pluralRuleParser = factory();
	}
}(this, function() {

function pluralRuleParser(rule, number) {
	'use strict';

	/*
	Syntax: see http://unicode.org/reports/tr35/#Language_Plural_Rules
	-----------------------------------------------------------------
	condition     = and_condition ('or' and_condition)*
		('@integer' samples)?
		('@decimal' samples)?
	and_condition = relation ('and' relation)*
	relation      = is_relation | in_relation | within_relation
	is_relation   = expr 'is' ('not')? value
	in_relation   = expr (('not')? 'in' | '=' | '!=') range_list
	within_relation = expr ('not')? 'within' range_list
	expr          = operand (('mod' | '%') value)?
	operand       = 'n' | 'i' | 'f' | 't' | 'v' | 'w'
	range_list    = (range | value) (',' range_list)*
	value         = digit+
	digit         = 0|1|2|3|4|5|6|7|8|9
	range         = value'..'value
	samples       = sampleRange (',' sampleRange)* (',' ('…'|'...'))?
	sampleRange   = decimalValue '~' decimalValue
	decimalValue  = value ('.' value)?
	*/

	// We don't evaluate the samples section of the rule. Ignore it.
	rule = rule.split('@')[0].replace(/^\s*/, '').replace(/\s*$/, '');

	if (!rule.length) {
		// Empty rule or 'other' rule.
		return true;
	}

	// Indicates the current position in the rule as we parse through it.
	// Shared among all parsing functions below.
	var pos = 0,
		operand,
		expression,
		relation,
		result,
		whitespace = makeRegexParser(/^\s+/),
		value = makeRegexParser(/^\d+/),
		_n_ = makeStringParser('n'),
		_i_ = makeStringParser('i'),
		_f_ = makeStringParser('f'),
		_t_ = makeStringParser('t'),
		_v_ = makeStringParser('v'),
		_w_ = makeStringParser('w'),
		_is_ = makeStringParser('is'),
		_isnot_ = makeStringParser('is not'),
		_isnot_sign_ = makeStringParser('!='),
		_equal_ = makeStringParser('='),
		_mod_ = makeStringParser('mod'),
		_percent_ = makeStringParser('%'),
		_not_ = makeStringParser('not'),
		_in_ = makeStringParser('in'),
		_within_ = makeStringParser('within'),
		_range_ = makeStringParser('..'),
		_comma_ = makeStringParser(','),
		_or_ = makeStringParser('or'),
		_and_ = makeStringParser('and');

	function debug() {
		// console.log.apply(console, arguments);
	}

	debug('pluralRuleParser', rule, number);

	// Try parsers until one works, if none work return null
	function choice(parserSyntax) {
		return function() {
			var i, result;

			for (i = 0; i < parserSyntax.length; i++) {
				result = parserSyntax[i]();

				if (result !== null) {
					return result;
				}
			}

			return null;
		};
	}

	// Try several parserSyntax-es in a row.
	// All must succeed; otherwise, return null.
	// This is the only eager one.
	function sequence(parserSyntax) {
		var i, parserRes,
			originalPos = pos,
			result = [];

		for (i = 0; i < parserSyntax.length; i++) {
			parserRes = parserSyntax[i]();

			if (parserRes === null) {
				pos = originalPos;

				return null;
			}

			result.push(parserRes);
		}

		return result;
	}

	// Run the same parser over and over until it fails.
	// Must succeed a minimum of n times; otherwise, return null.
	function nOrMore(n, p) {
		return function() {
			var originalPos = pos,
				result = [],
				parsed = p();

			while (parsed !== null) {
				result.push(parsed);
				parsed = p();
			}

			if (result.length < n) {
				pos = originalPos;

				return null;
			}

			return result;
		};
	}

	// Helpers - just make parserSyntax out of simpler JS builtin types
	function makeStringParser(s) {
		var len = s.length;

		return function() {
			var result = null;

			if (rule.substr(pos, len) === s) {
				result = s;
				pos += len;
			}

			return result;
		};
	}

	function makeRegexParser(regex) {
		return function() {
			var matches = rule.substr(pos).match(regex);

			if (matches === null) {
				return null;
			}

			pos += matches[0].length;

			return matches[0];
		};
	}

	/**
	 * Integer digits of n.
	 */
	function i() {
		var result = _i_();

		if (result === null) {
			debug(' -- failed i', parseInt(number, 10));

			return result;
		}

		result = parseInt(number, 10);
		debug(' -- passed i ', result);

		return result;
	}

	/**
	 * Absolute value of the source number (integer and decimals).
	 */
	function n() {
		var result = _n_();

		if (result === null) {
			debug(' -- failed n ', number);

			return result;
		}

		result = parseFloat(number, 10);
		debug(' -- passed n ', result);

		return result;
	}

	/**
	 * Visible fractional digits in n, with trailing zeros.
	 */
	function f() {
		var result = _f_();

		if (result === null) {
			debug(' -- failed f ', number);

			return result;
		}

		result = (number + '.').split('.')[1] || 0;
		debug(' -- passed f ', result);

		return result;
	}

	/**
	 * Visible fractional digits in n, without trailing zeros.
	 */
	function t() {
		var result = _t_();

		if (result === null) {
			debug(' -- failed t ', number);

			return result;
		}

		result = (number + '.').split('.')[1].replace(/0$/, '') || 0;
		debug(' -- passed t ', result);

		return result;
	}

	/**
	 * Number of visible fraction digits in n, with trailing zeros.
	 */
	function v() {
		var result = _v_();

		if (result === null) {
			debug(' -- failed v ', number);

			return result;
		}

		result = (number + '.').split('.')[1].length || 0;
		debug(' -- passed v ', result);

		return result;
	}

	/**
	 * Number of visible fraction digits in n, without trailing zeros.
	 */
	function w() {
		var result = _w_();

		if (result === null) {
			debug(' -- failed w ', number);

			return result;
		}

		result = (number + '.').split('.')[1].replace(/0$/, '').length || 0;
		debug(' -- passed w ', result);

		return result;
	}

	// operand       = 'n' | 'i' | 'f' | 't' | 'v' | 'w'
	operand = choice([n, i, f, t, v, w]);

	// expr          = operand (('mod' | '%') value)?
	expression = choice([mod, operand]);

	function mod() {
		var result = sequence(
			[operand, whitespace, choice([_mod_, _percent_]), whitespace, value]
		);

		if (result === null) {
			debug(' -- failed mod');

			return null;
		}

		debug(' -- passed ' + parseInt(result[0], 10) + ' ' + result[2] + ' ' + parseInt(result[4], 10));

		return parseFloat(result[0]) % parseInt(result[4], 10);
	}

	function not() {
		var result = sequence([whitespace, _not_]);

		if (result === null) {
			debug(' -- failed not');

			return null;
		}

		return result[1];
	}

	// is_relation   = expr 'is' ('not')? value
	function is() {
		var result = sequence([expression, whitespace, choice([_is_]), whitespace, value]);

		if (result !== null) {
			debug(' -- passed is : ' + result[0] + ' == ' + parseInt(result[4], 10));

			return result[0] === parseInt(result[4], 10);
		}

		debug(' -- failed is');

		return null;
	}

	// is_relation   = expr 'is' ('not')? value
	function isnot() {
		var result = sequence(
			[expression, whitespace, choice([_isnot_, _isnot_sign_]), whitespace, value]
		);

		if (result !== null) {
			debug(' -- passed isnot: ' + result[0] + ' != ' + parseInt(result[4], 10));

			return result[0] !== parseInt(result[4], 10);
		}

		debug(' -- failed isnot');

		return null;
	}

	function not_in() {
		var i, range_list,
			result = sequence([expression, whitespace, _isnot_sign_, whitespace, rangeList]);

		if (result !== null) {
			debug(' -- passed not_in: ' + result[0] + ' != ' + result[4]);
			range_list = result[4];

			for (i = 0; i < range_list.length; i++) {
				if (parseInt(range_list[i], 10) === parseInt(result[0], 10)) {
					return false;
				}
			}

			return true;
		}

		debug(' -- failed not_in');

		return null;
	}

	// range_list    = (range | value) (',' range_list)*
	function rangeList() {
		var result = sequence([choice([range, value]), nOrMore(0, rangeTail)]),
			resultList = [];

		if (result !== null) {
			resultList = resultList.concat(result[0]);

			if (result[1][0]) {
				resultList = resultList.concat(result[1][0]);
			}

			return resultList;
		}

		debug(' -- failed rangeList');

		return null;
	}

	function rangeTail() {
		// ',' range_list
		var result = sequence([_comma_, rangeList]);

		if (result !== null) {
			return result[1];
		}

		debug(' -- failed rangeTail');

		return null;
	}

	// range         = value'..'value
	function range() {
		var i, array, left, right,
			result = sequence([value, _range_, value]);

		if (result !== null) {
			debug(' -- passed range');

			array = [];
			left = parseInt(result[0], 10);
			right = parseInt(result[2], 10);

			for (i = left; i <= right; i++) {
				array.push(i);
			}

			return array;
		}

		debug(' -- failed range');

		return null;
	}

	function _in() {
		var result, range_list, i;

		// in_relation   = expr ('not')? 'in' range_list
		result = sequence(
			[expression, nOrMore(0, not), whitespace, choice([_in_, _equal_]), whitespace, rangeList]
		);

		if (result !== null) {
			debug(' -- passed _in:' + result);

			range_list = result[5];

			for (i = 0; i < range_list.length; i++) {
				if (parseInt(range_list[i], 10) === parseFloat(result[0])) {
					return (result[1][0] !== 'not');
				}
			}

			return (result[1][0] === 'not');
		}

		debug(' -- failed _in ');

		return null;
	}

	/**
	 * The difference between "in" and "within" is that
	 * "in" only includes integers in the specified range,
	 * while "within" includes all values.
	 */
	function within() {
		var range_list, result;

		// within_relation = expr ('not')? 'within' range_list
		result = sequence(
			[expression, nOrMore(0, not), whitespace, _within_, whitespace, rangeList]
		);

		if (result !== null) {
			debug(' -- passed within');

			range_list = result[5];

			if ((result[0] >= parseInt(range_list[0], 10)) &&
				(result[0] < parseInt(range_list[range_list.length - 1], 10))) {

				return (result[1][0] !== 'not');
			}

			return (result[1][0] === 'not');
		}

		debug(' -- failed within ');

		return null;
	}

	// relation      = is_relation | in_relation | within_relation
	relation = choice([is, not_in, isnot, _in, within]);

	// and_condition = relation ('and' relation)*
	function and() {
		var i,
			result = sequence([relation, nOrMore(0, andTail)]);

		if (result) {
			if (!result[0]) {
				return false;
			}

			for (i = 0; i < result[1].length; i++) {
				if (!result[1][i]) {
					return false;
				}
			}

			return true;
		}

		debug(' -- failed and');

		return null;
	}

	// ('and' relation)*
	function andTail() {
		var result = sequence([whitespace, _and_, whitespace, relation]);

		if (result !== null) {
			debug(' -- passed andTail' + result);

			return result[3];
		}

		debug(' -- failed andTail');

		return null;

	}
	//  ('or' and_condition)*
	function orTail() {
		var result = sequence([whitespace, _or_, whitespace, and]);

		if (result !== null) {
			debug(' -- passed orTail: ' + result[3]);

			return result[3];
		}

		debug(' -- failed orTail');

		return null;
	}

	// condition     = and_condition ('or' and_condition)*
	function condition() {
		var i,
			result = sequence([and, nOrMore(0, orTail)]);

		if (result) {
			for (i = 0; i < result[1].length; i++) {
				if (result[1][i]) {
					return true;
				}
			}

			return result[0];
		}

		return false;
	}

	result = condition();

	/**
	 * For success, the pos must have gotten to the end of the rule
	 * and returned a non-null.
	 * n.b. This is part of language infrastructure,
	 * so we do not throw an internationalizable message.
	 */
	if (result === null) {
		throw new Error('Parse error at position ' + pos.toString() + ' for rule: ' + rule);
	}

	if (pos !== rule.length) {
		debug('Warning: Rule not parsed completely. Parser stopped at ' + rule.substr(0, pos) + ' for rule: ' + rule);
	}

	return result;
}

return pluralRuleParser;

}));

},{}],12:[function(require,module,exports){
typeof JSON!="object"&&(JSON={}),function(){"use strict";function f(e){return e<10?"0"+e:e}function quote(e){return escapable.lastIndex=0,escapable.test(e)?'"'+e.replace(escapable,function(e){var t=meta[e];return typeof t=="string"?t:"\\u"+("0000"+e.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+e+'"'}function str(e,t){var n,r,i,s,o=gap,u,a=t[e];a&&typeof a=="object"&&typeof a.toJSON=="function"&&(a=a.toJSON(e)),typeof rep=="function"&&(a=rep.call(t,e,a));switch(typeof a){case"string":return quote(a);case"number":return isFinite(a)?String(a):"null";case"boolean":case"null":return String(a);case"object":if(!a)return"null";gap+=indent,u=[];if(Object.prototype.toString.apply(a)==="[object Array]"){s=a.length;for(n=0;n<s;n+=1)u[n]=str(n,a)||"null";return i=u.length===0?"[]":gap?"[\n"+gap+u.join(",\n"+gap)+"\n"+o+"]":"["+u.join(",")+"]",gap=o,i}if(rep&&typeof rep=="object"){s=rep.length;for(n=0;n<s;n+=1)typeof rep[n]=="string"&&(r=rep[n],i=str(r,a),i&&u.push(quote(r)+(gap?": ":":")+i))}else for(r in a)Object.prototype.hasOwnProperty.call(a,r)&&(i=str(r,a),i&&u.push(quote(r)+(gap?": ":":")+i));return i=u.length===0?"{}":gap?"{\n"+gap+u.join(",\n"+gap)+"\n"+o+"}":"{"+u.join(",")+"}",gap=o,i}}typeof Date.prototype.toJSON!="function"&&(Date.prototype.toJSON=function(e){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null},String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(e){return this.valueOf()});var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={"\b":"\\b"," ":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},rep;typeof JSON.stringify!="function"&&(JSON.stringify=function(e,t,n){var r;gap="",indent="";if(typeof n=="number")for(r=0;r<n;r+=1)indent+=" ";else typeof n=="string"&&(indent=n);rep=t;if(!t||typeof t=="function"||typeof t=="object"&&typeof t.length=="number")return str("",{"":e});throw new Error("JSON.stringify")}),typeof JSON.parse!="function"&&(JSON.parse=function(text,reviver){function walk(e,t){var n,r,i=e[t];if(i&&typeof i=="object")for(n in i)Object.prototype.hasOwnProperty.call(i,n)&&(r=walk(i,n),r!==undefined?i[n]=r:delete i[n]);return reviver.call(e,t,i)}var j;text=String(text),cx.lastIndex=0,cx.test(text)&&(text=text.replace(cx,function(e){return"\\u"+("0000"+e.charCodeAt(0).toString(16)).slice(-4)}));if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return j=eval("("+text+")"),typeof reviver=="function"?walk({"":j},""):j;throw new SyntaxError("JSON.parse")})}(),function(e,t){"use strict";var n=e.History=e.History||{},r=e.jQuery;if(typeof n.Adapter!="undefined")throw new Error("History.js Adapter has already been loaded...");n.Adapter={bind:function(e,t,n){r(e).bind(t,n)},trigger:function(e,t,n){r(e).trigger(t,n)},extractEventData:function(e,n,r){var i=n&&n.originalEvent&&n.originalEvent[e]||r&&r[e]||t;return i},onDomLoad:function(e){r(e)}},typeof n.init!="undefined"&&n.init()}(window),function(e,t){"use strict";var n=e.document,r=e.setTimeout||r,i=e.clearTimeout||i,s=e.setInterval||s,o=e.History=e.History||{};if(typeof o.initHtml4!="undefined")throw new Error("History.js HTML4 Support has already been loaded...");o.initHtml4=function(){if(typeof o.initHtml4.initialized!="undefined")return!1;o.initHtml4.initialized=!0,o.enabled=!0,o.savedHashes=[],o.isLastHash=function(e){var t=o.getHashByIndex(),n;return n=e===t,n},o.isHashEqual=function(e,t){return e=encodeURIComponent(e).replace(/%25/g,"%"),t=encodeURIComponent(t).replace(/%25/g,"%"),e===t},o.saveHash=function(e){return o.isLastHash(e)?!1:(o.savedHashes.push(e),!0)},o.getHashByIndex=function(e){var t=null;return typeof e=="undefined"?t=o.savedHashes[o.savedHashes.length-1]:e<0?t=o.savedHashes[o.savedHashes.length+e]:t=o.savedHashes[e],t},o.discardedHashes={},o.discardedStates={},o.discardState=function(e,t,n){var r=o.getHashByState(e),i;return i={discardedState:e,backState:n,forwardState:t},o.discardedStates[r]=i,!0},o.discardHash=function(e,t,n){var r={discardedHash:e,backState:n,forwardState:t};return o.discardedHashes[e]=r,!0},o.discardedState=function(e){var t=o.getHashByState(e),n;return n=o.discardedStates[t]||!1,n},o.discardedHash=function(e){var t=o.discardedHashes[e]||!1;return t},o.recycleState=function(e){var t=o.getHashByState(e);return o.discardedState(e)&&delete o.discardedStates[t],!0},o.emulated.hashChange&&(o.hashChangeInit=function(){o.checkerFunction=null;var t="",r,i,u,a,f=Boolean(o.getHash());return o.isInternetExplorer()?(r="historyjs-iframe",i=n.createElement("iframe"),i.setAttribute("id",r),i.setAttribute("src","#"),i.style.display="none",n.body.appendChild(i),i.contentWindow.document.open(),i.contentWindow.document.close(),u="",a=!1,o.checkerFunction=function(){if(a)return!1;a=!0;var n=o.getHash(),r=o.getHash(i.contentWindow.document);return n!==t?(t=n,r!==n&&(u=r=n,i.contentWindow.document.open(),i.contentWindow.document.close(),i.contentWindow.document.location.hash=o.escapeHash(n)),o.Adapter.trigger(e,"hashchange")):r!==u&&(u=r,f&&r===""?o.back():o.setHash(r,!1)),a=!1,!0}):o.checkerFunction=function(){var n=o.getHash()||"";return n!==t&&(t=n,o.Adapter.trigger(e,"hashchange")),!0},o.intervalList.push(s(o.checkerFunction,o.options.hashChangeInterval)),!0},o.Adapter.onDomLoad(o.hashChangeInit)),o.emulated.pushState&&(o.onHashChange=function(t){var n=t&&t.newURL||o.getLocationHref(),r=o.getHashByUrl(n),i=null,s=null,u=null,a;return o.isLastHash(r)?(o.busy(!1),!1):(o.doubleCheckComplete(),o.saveHash(r),r&&o.isTraditionalAnchor(r)?(o.Adapter.trigger(e,"anchorchange"),o.busy(!1),!1):(i=o.extractState(o.getFullUrl(r||o.getLocationHref()),!0),o.isLastSavedState(i)?(o.busy(!1),!1):(s=o.getHashByState(i),a=o.discardedState(i),a?(o.getHashByIndex(-2)===o.getHashByState(a.forwardState)?o.back(!1):o.forward(!1),!1):(o.pushState(i.data,i.title,encodeURI(i.url),!1),!0))))},o.Adapter.bind(e,"hashchange",o.onHashChange),o.pushState=function(t,n,r,i){r=encodeURI(r).replace(/%25/g,"%");if(o.getHashByUrl(r))throw new Error("History.js does not support states with fragment-identifiers (hashes/anchors).");if(i!==!1&&o.busy())return o.pushQueue({scope:o,callback:o.pushState,args:arguments,queue:i}),!1;o.busy(!0);var s=o.createStateObject(t,n,r),u=o.getHashByState(s),a=o.getState(!1),f=o.getHashByState(a),l=o.getHash(),c=o.expectedStateId==s.id;return o.storeState(s),o.expectedStateId=s.id,o.recycleState(s),o.setTitle(s),u===f?(o.busy(!1),!1):(o.saveState(s),c||o.Adapter.trigger(e,"statechange"),!o.isHashEqual(u,l)&&!o.isHashEqual(u,o.getShortUrl(o.getLocationHref()))&&o.setHash(u,!1),o.busy(!1),!0)},o.replaceState=function(t,n,r,i){r=encodeURI(r).replace(/%25/g,"%");if(o.getHashByUrl(r))throw new Error("History.js does not support states with fragment-identifiers (hashes/anchors).");if(i!==!1&&o.busy())return o.pushQueue({scope:o,callback:o.replaceState,args:arguments,queue:i}),!1;o.busy(!0);var s=o.createStateObject(t,n,r),u=o.getHashByState(s),a=o.getState(!1),f=o.getHashByState(a),l=o.getStateByIndex(-2);return o.discardState(a,s,l),u===f?(o.storeState(s),o.expectedStateId=s.id,o.recycleState(s),o.setTitle(s),o.saveState(s),o.Adapter.trigger(e,"statechange"),o.busy(!1)):o.pushState(s.data,s.title,s.url,!1),!0}),o.emulated.pushState&&o.getHash()&&!o.emulated.hashChange&&o.Adapter.onDomLoad(function(){o.Adapter.trigger(e,"hashchange")})},typeof o.init!="undefined"&&o.init()}(window),function(e,t){"use strict";var n=e.console||t,r=e.document,i=e.navigator,s=!1,o=e.setTimeout,u=e.clearTimeout,a=e.setInterval,f=e.clearInterval,l=e.JSON,c=e.alert,h=e.History=e.History||{},p=e.history;try{s=e.sessionStorage,s.setItem("TEST","1"),s.removeItem("TEST")}catch(d){s=!1}l.stringify=l.stringify||l.encode,l.parse=l.parse||l.decode;if(typeof h.init!="undefined")throw new Error("History.js Core has already been loaded...");h.init=function(e){return typeof h.Adapter=="undefined"?!1:(typeof h.initCore!="undefined"&&h.initCore(),typeof h.initHtml4!="undefined"&&h.initHtml4(),!0)},h.initCore=function(d){if(typeof h.initCore.initialized!="undefined")return!1;h.initCore.initialized=!0,h.options=h.options||{},h.options.hashChangeInterval=h.options.hashChangeInterval||100,h.options.safariPollInterval=h.options.safariPollInterval||500,h.options.doubleCheckInterval=h.options.doubleCheckInterval||500,h.options.disableSuid=h.options.disableSuid||!1,h.options.storeInterval=h.options.storeInterval||1e3,h.options.busyDelay=h.options.busyDelay||250,h.options.debug=h.options.debug||!1,h.options.initialTitle=h.options.initialTitle||r.title,h.options.html4Mode=h.options.html4Mode||!1,h.options.delayInit=h.options.delayInit||!1,h.intervalList=[],h.clearAllIntervals=function(){var e,t=h.intervalList;if(typeof t!="undefined"&&t!==null){for(e=0;e<t.length;e++)f(t[e]);h.intervalList=null}},h.debug=function(){(h.options.debug||!1)&&h.log.apply(h,arguments)},h.log=function(){var e=typeof n!="undefined"&&typeof n.log!="undefined"&&typeof n.log.apply!="undefined",t=r.getElementById("log"),i,s,o,u,a;e?(u=Array.prototype.slice.call(arguments),i=u.shift(),typeof n.debug!="undefined"?n.debug.apply(n,[i,u]):n.log.apply(n,[i,u])):i="\n"+arguments[0]+"\n";for(s=1,o=arguments.length;s<o;++s){a=arguments[s];if(typeof a=="object"&&typeof l!="undefined")try{a=l.stringify(a)}catch(f){}i+="\n"+a+"\n"}return t?(t.value+=i+"\n-----\n",t.scrollTop=t.scrollHeight-t.clientHeight):e||c(i),!0},h.getInternetExplorerMajorVersion=function(){var e=h.getInternetExplorerMajorVersion.cached=typeof h.getInternetExplorerMajorVersion.cached!="undefined"?h.getInternetExplorerMajorVersion.cached:function(){var e=3,t=r.createElement("div"),n=t.getElementsByTagName("i");while((t.innerHTML="<!--[if gt IE "+ ++e+"]><i></i><![endif]-->")&&n[0]);return e>4?e:!1}();return e},h.isInternetExplorer=function(){var e=h.isInternetExplorer.cached=typeof h.isInternetExplorer.cached!="undefined"?h.isInternetExplorer.cached:Boolean(h.getInternetExplorerMajorVersion());return e},h.options.html4Mode?h.emulated={pushState:!0,hashChange:!0}:h.emulated={pushState:!Boolean(e.history&&e.history.pushState&&e.history.replaceState&&!/ Mobile\/([1-7][a-z]|(8([abcde]|f(1[0-8]))))/i.test(i.userAgent)&&!/AppleWebKit\/5([0-2]|3[0-2])/i.test(i.userAgent)),hashChange:Boolean(!("onhashchange"in e||"onhashchange"in r)||h.isInternetExplorer()&&h.getInternetExplorerMajorVersion()<8)},h.enabled=!h.emulated.pushState,h.bugs={setHash:Boolean(!h.emulated.pushState&&i.vendor==="Apple Computer, Inc."&&/AppleWebKit\/5([0-2]|3[0-3])/.test(i.userAgent)),safariPoll:Boolean(!h.emulated.pushState&&i.vendor==="Apple Computer, Inc."&&/AppleWebKit\/5([0-2]|3[0-3])/.test(i.userAgent)),ieDoubleCheck:Boolean(h.isInternetExplorer()&&h.getInternetExplorerMajorVersion()<8),hashEscape:Boolean(h.isInternetExplorer()&&h.getInternetExplorerMajorVersion()<7)},h.isEmptyObject=function(e){for(var t in e)if(e.hasOwnProperty(t))return!1;return!0},h.cloneObject=function(e){var t,n;return e?(t=l.stringify(e),n=l.parse(t)):n={},n},h.getRootUrl=function(){var e=r.location.protocol+"//"+(r.location.hostname||r.location.host);if(r.location.port||!1)e+=":"+r.location.port;return e+="/",e},h.getBaseHref=function(){var e=r.getElementsByTagName("base"),t=null,n="";return e.length===1&&(t=e[0],n=t.href.replace(/[^\/]+$/,"")),n=n.replace(/\/+$/,""),n&&(n+="/"),n},h.getBaseUrl=function(){var e=h.getBaseHref()||h.getBasePageUrl()||h.getRootUrl();return e},h.getPageUrl=function(){var e=h.getState(!1,!1),t=(e||{}).url||h.getLocationHref(),n;return n=t.replace(/\/+$/,"").replace(/[^\/]+$/,function(e,t,n){return/\./.test(e)?e:e+"/"}),n},h.getBasePageUrl=function(){var e=h.getLocationHref().replace(/[#\?].*/,"").replace(/[^\/]+$/,function(e,t,n){return/[^\/]$/.test(e)?"":e}).replace(/\/+$/,"")+"/";return e},h.getFullUrl=function(e,t){var n=e,r=e.substring(0,1);return t=typeof t=="undefined"?!0:t,/[a-z]+\:\/\//.test(e)||(r==="/"?n=h.getRootUrl()+e.replace(/^\/+/,""):r==="#"?n=h.getPageUrl().replace(/#.*/,"")+e:r==="?"?n=h.getPageUrl().replace(/[\?#].*/,"")+e:t?n=h.getBaseUrl()+e.replace(/^(\.\/)+/,""):n=h.getBasePageUrl()+e.replace(/^(\.\/)+/,"")),n.replace(/\#$/,"")},h.getShortUrl=function(e){var t=e,n=h.getBaseUrl(),r=h.getRootUrl();return h.emulated.pushState&&(t=t.replace(n,"")),t=t.replace(r,"/"),h.isTraditionalAnchor(t)&&(t="./"+t),t=t.replace(/^(\.\/)+/g,"./").replace(/\#$/,""),t},h.getLocationHref=function(e){return e=e||r,e.URL===e.location.href?e.location.href:e.location.href===decodeURIComponent(e.URL)?e.URL:e.location.hash&&decodeURIComponent(e.location.href.replace(/^[^#]+/,""))===e.location.hash?e.location.href:e.URL.indexOf("#")==-1&&e.location.href.indexOf("#")!=-1?e.location.href:e.URL||e.location.href},h.store={},h.idToState=h.idToState||{},h.stateToId=h.stateToId||{},h.urlToId=h.urlToId||{},h.storedStates=h.storedStates||[],h.savedStates=h.savedStates||[],h.normalizeStore=function(){h.store.idToState=h.store.idToState||{},h.store.urlToId=h.store.urlToId||{},h.store.stateToId=h.store.stateToId||{}},h.getState=function(e,t){typeof e=="undefined"&&(e=!0),typeof t=="undefined"&&(t=!0);var n=h.getLastSavedState();return!n&&t&&(n=h.createStateObject()),e&&(n=h.cloneObject(n),n.url=n.cleanUrl||n.url),n},h.getIdByState=function(e){var t=h.extractId(e.url),n;if(!t){n=h.getStateString(e);if(typeof h.stateToId[n]!="undefined")t=h.stateToId[n];else if(typeof h.store.stateToId[n]!="undefined")t=h.store.stateToId[n];else{for(;;){t=(new Date).getTime()+String(Math.random()).replace(/\D/g,"");if(typeof h.idToState[t]=="undefined"&&typeof h.store.idToState[t]=="undefined")break}h.stateToId[n]=t,h.idToState[t]=e}}return t},h.normalizeState=function(e){var t,n;if(!e||typeof e!="object")e={};if(typeof e.normalized!="undefined")return e;if(!e.data||typeof e.data!="object")e.data={};return t={},t.normalized=!0,t.title=e.title||"",t.url=h.getFullUrl(e.url?e.url:h.getLocationHref()),t.hash=h.getShortUrl(t.url),t.data=h.cloneObject(e.data),t.id=h.getIdByState(t),t.cleanUrl=t.url.replace(/\??\&_suid.*/,""),t.url=t.cleanUrl,n=!h.isEmptyObject(t.data),(t.title||n)&&h.options.disableSuid!==!0&&(t.hash=h.getShortUrl(t.url).replace(/\??\&_suid.*/,""),/\?/.test(t.hash)||(t.hash+="?"),t.hash+="&_suid="+t.id),t.hashedUrl=h.getFullUrl(t.hash),(h.emulated.pushState||h.bugs.safariPoll)&&h.hasUrlDuplicate(t)&&(t.url=t.hashedUrl),t},h.createStateObject=function(e,t,n){var r={data:e,title:t,url:n};return r=h.normalizeState(r),r},h.getStateById=function(e){e=String(e);var n=h.idToState[e]||h.store.idToState[e]||t;return n},h.getStateString=function(e){var t,n,r;return t=h.normalizeState(e),n={data:t.data,title:e.title,url:e.url},r=l.stringify(n),r},h.getStateId=function(e){var t,n;return t=h.normalizeState(e),n=t.id,n},h.getHashByState=function(e){var t,n;return t=h.normalizeState(e),n=t.hash,n},h.extractId=function(e){var t,n,r,i;return e.indexOf("#")!=-1?i=e.split("#")[0]:i=e,n=/(.*)\&_suid=([0-9]+)$/.exec(i),r=n?n[1]||e:e,t=n?String(n[2]||""):"",t||!1},h.isTraditionalAnchor=function(e){var t=!/[\/\?\.]/.test(e);return t},h.extractState=function(e,t){var n=null,r,i;return t=t||!1,r=h.extractId(e),r&&(n=h.getStateById(r)),n||(i=h.getFullUrl(e),r=h.getIdByUrl(i)||!1,r&&(n=h.getStateById(r)),!n&&t&&!h.isTraditionalAnchor(e)&&(n=h.createStateObject(null,null,i))),n},h.getIdByUrl=function(e){var n=h.urlToId[e]||h.store.urlToId[e]||t;return n},h.getLastSavedState=function(){return h.savedStates[h.savedStates.length-1]||t},h.getLastStoredState=function(){return h.storedStates[h.storedStates.length-1]||t},h.hasUrlDuplicate=function(e){var t=!1,n;return n=h.extractState(e.url),t=n&&n.id!==e.id,t},h.storeState=function(e){return h.urlToId[e.url]=e.id,h.storedStates.push(h.cloneObject(e)),e},h.isLastSavedState=function(e){var t=!1,n,r,i;return h.savedStates.length&&(n=e.id,r=h.getLastSavedState(),i=r.id,t=n===i),t},h.saveState=function(e){return h.isLastSavedState(e)?!1:(h.savedStates.push(h.cloneObject(e)),!0)},h.getStateByIndex=function(e){var t=null;return typeof e=="undefined"?t=h.savedStates[h.savedStates.length-1]:e<0?t=h.savedStates[h.savedStates.length+e]:t=h.savedStates[e],t},h.getCurrentIndex=function(){var e=null;return h.savedStates.length<1?e=0:e=h.savedStates.length-1,e},h.getHash=function(e){var t=h.getLocationHref(e),n;return n=h.getHashByUrl(t),n},h.unescapeHash=function(e){var t=h.normalizeHash(e);return t=decodeURIComponent(t),t},h.normalizeHash=function(e){var t=e.replace(/[^#]*#/,"").replace(/#.*/,"");return t},h.setHash=function(e,t){var n,i;return t!==!1&&h.busy()?(h.pushQueue({scope:h,callback:h.setHash,args:arguments,queue:t}),!1):(h.busy(!0),n=h.extractState(e,!0),n&&!h.emulated.pushState?h.pushState(n.data,n.title,n.url,!1):h.getHash()!==e&&(h.bugs.setHash?(i=h.getPageUrl(),h.pushState(null,null,i+"#"+e,!1)):r.location.hash=e),h)},h.escapeHash=function(t){var n=h.normalizeHash(t);return n=e.encodeURIComponent(n),h.bugs.hashEscape||(n=n.replace(/\%21/g,"!").replace(/\%26/g,"&").replace(/\%3D/g,"=").replace(/\%3F/g,"?")),n},h.getHashByUrl=function(e){var t=String(e).replace(/([^#]*)#?([^#]*)#?(.*)/,"$2");return t=h.unescapeHash(t),t},h.setTitle=function(e){var t=e.title,n;t||(n=h.getStateByIndex(0),n&&n.url===e.url&&(t=n.title||h.options.initialTitle));try{r.getElementsByTagName("title")[0].innerHTML=t.replace("<","&lt;").replace(">","&gt;").replace(" & "," &amp; ")}catch(i){}return r.title=t,h},h.queues=[],h.busy=function(e){typeof e!="undefined"?h.busy.flag=e:typeof h.busy.flag=="undefined"&&(h.busy.flag=!1);if(!h.busy.flag){u(h.busy.timeout);var t=function(){var e,n,r;if(h.busy.flag)return;for(e=h.queues.length-1;e>=0;--e){n=h.queues[e];if(n.length===0)continue;r=n.shift(),h.fireQueueItem(r),h.busy.timeout=o(t,h.options.busyDelay)}};h.busy.timeout=o(t,h.options.busyDelay)}return h.busy.flag},h.busy.flag=!1,h.fireQueueItem=function(e){return e.callback.apply(e.scope||h,e.args||[])},h.pushQueue=function(e){return h.queues[e.queue||0]=h.queues[e.queue||0]||[],h.queues[e.queue||0].push(e),h},h.queue=function(e,t){return typeof e=="function"&&(e={callback:e}),typeof t!="undefined"&&(e.queue=t),h.busy()?h.pushQueue(e):h.fireQueueItem(e),h},h.clearQueue=function(){return h.busy.flag=!1,h.queues=[],h},h.stateChanged=!1,h.doubleChecker=!1,h.doubleCheckComplete=function(){return h.stateChanged=!0,h.doubleCheckClear(),h},h.doubleCheckClear=function(){return h.doubleChecker&&(u(h.doubleChecker),h.doubleChecker=!1),h},h.doubleCheck=function(e){return h.stateChanged=!1,h.doubleCheckClear(),h.bugs.ieDoubleCheck&&(h.doubleChecker=o(function(){return h.doubleCheckClear(),h.stateChanged||e(),!0},h.options.doubleCheckInterval)),h},h.safariStatePoll=function(){var t=h.extractState(h.getLocationHref()),n;if(!h.isLastSavedState(t))return n=t,n||(n=h.createStateObject()),h.Adapter.trigger(e,"popstate"),h;return},h.back=function(e){return e!==!1&&h.busy()?(h.pushQueue({scope:h,callback:h.back,args:arguments,queue:e}),!1):(h.busy(!0),h.doubleCheck(function(){h.back(!1)}),p.go(-1),!0)},h.forward=function(e){return e!==!1&&h.busy()?(h.pushQueue({scope:h,callback:h.forward,args:arguments,queue:e}),!1):(h.busy(!0),h.doubleCheck(function(){h.forward(!1)}),p.go(1),!0)},h.go=function(e,t){var n;if(e>0)for(n=1;n<=e;++n)h.forward(t);else{if(!(e<0))throw new Error("History.go: History.go requires a positive or negative integer passed.");for(n=-1;n>=e;--n)h.back(t)}return h};if(h.emulated.pushState){var v=function(){};h.pushState=h.pushState||v,h.replaceState=h.replaceState||v}else h.onPopState=function(t,n){var r=!1,i=!1,s,o;return h.doubleCheckComplete(),s=h.getHash(),s?(o=h.extractState(s||h.getLocationHref(),!0),o?h.replaceState(o.data,o.title,o.url,!1):(h.Adapter.trigger(e,"anchorchange"),h.busy(!1)),h.expectedStateId=!1,!1):(r=h.Adapter.extractEventData("state",t,n)||!1,r?i=h.getStateById(r):h.expectedStateId?i=h.getStateById(h.expectedStateId):i=h.extractState(h.getLocationHref()),i||(i=h.createStateObject(null,null,h.getLocationHref())),h.expectedStateId=!1,h.isLastSavedState(i)?(h.busy(!1),!1):(h.storeState(i),h.saveState(i),h.setTitle(i),h.Adapter.trigger(e,"statechange"),h.busy(!1),!0))},h.Adapter.bind(e,"popstate",h.onPopState),h.pushState=function(t,n,r,i){if(h.getHashByUrl(r)&&h.emulated.pushState)throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");if(i!==!1&&h.busy())return h.pushQueue({scope:h,callback:h.pushState,args:arguments,queue:i}),!1;h.busy(!0);var s=h.createStateObject(t,n,r);return h.isLastSavedState(s)?h.busy(!1):(h.storeState(s),h.expectedStateId=s.id,p.pushState(s.id,s.title,s.url),h.Adapter.trigger(e,"popstate")),!0},h.replaceState=function(t,n,r,i){if(h.getHashByUrl(r)&&h.emulated.pushState)throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");if(i!==!1&&h.busy())return h.pushQueue({scope:h,callback:h.replaceState,args:arguments,queue:i}),!1;h.busy(!0);var s=h.createStateObject(t,n,r);return h.isLastSavedState(s)?h.busy(!1):(h.storeState(s),h.expectedStateId=s.id,p.replaceState(s.id,s.title,s.url),h.Adapter.trigger(e,"popstate")),!0};if(s){try{h.store=l.parse(s.getItem("History.store"))||{}}catch(m){h.store={}}h.normalizeStore()}else h.store={},h.normalizeStore();h.Adapter.bind(e,"unload",h.clearAllIntervals),h.saveState(h.storeState(h.extractState(h.getLocationHref(),!0))),s&&(h.onUnload=function(){var e,t,n;try{e=l.parse(s.getItem("History.store"))||{}}catch(r){e={}}e.idToState=e.idToState||{},e.urlToId=e.urlToId||{},e.stateToId=e.stateToId||{};for(t in h.idToState){if(!h.idToState.hasOwnProperty(t))continue;e.idToState[t]=h.idToState[t]}for(t in h.urlToId){if(!h.urlToId.hasOwnProperty(t))continue;e.urlToId[t]=h.urlToId[t]}for(t in h.stateToId){if(!h.stateToId.hasOwnProperty(t))continue;e.stateToId[t]=h.stateToId[t]}h.store=e,h.normalizeStore(),n=l.stringify(e);try{s.setItem("History.store",n)}catch(i){if(i.code!==DOMException.QUOTA_EXCEEDED_ERR)throw i;s.length&&(s.removeItem("History.store"),s.setItem("History.store",n))}},h.intervalList.push(a(h.onUnload,h.options.storeInterval)),h.Adapter.bind(e,"beforeunload",h.onUnload),h.Adapter.bind(e,"unload",h.onUnload));if(!h.emulated.pushState){h.bugs.safariPoll&&h.intervalList.push(a(h.safariStatePoll,h.options.safariPollInterval));if(i.vendor==="Apple Computer, Inc."||(i.appCodeName||"")==="Mozilla")h.Adapter.bind(e,"hashchange",function(){h.Adapter.trigger(e,"popstate")}),h.getHash()&&h.Adapter.onDomLoad(function(){h.Adapter.trigger(e,"hashchange")})}},(!h.options||!h.options.delayInit)&&h.init()}(window)

},{}],13:[function(require,module,exports){
/*!
 * BIDI embedding support for jQuery.i18n
 *
 * Copyright (C) 2015, David Chan
 *
 * This code is dual licensed GPLv2 or later and MIT. You don't have to do
 * anything special to choose one license or the other and you don't have to
 * notify anyone which license you are using. You are free to use this code
 * in commercial projects as long as the copyright header is left intact.
 * See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */

( function ( $ ) {
	'use strict';
	var strongDirRegExp;

	/**
	 * Matches the first strong directionality codepoint:
	 * - in group 1 if it is LTR
	 * - in group 2 if it is RTL
	 * Does not match if there is no strong directionality codepoint.
	 *
	 * Generated by UnicodeJS (see tools/strongDir) from the UCD; see
	 * https://phabricator.wikimedia.org/diffusion/GUJS/ .
	 */
	strongDirRegExp = new RegExp(
		'(?:' +
			'(' +
				'[\u0041-\u005a\u0061-\u007a\u00aa\u00b5\u00ba\u00c0-\u00d6\u00d8-\u00f6\u00f8-\u02b8\u02bb-\u02c1\u02d0\u02d1\u02e0-\u02e4\u02ee\u0370-\u0373\u0376\u0377\u037a-\u037d\u037f\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0482\u048a-\u052f\u0531-\u0556\u0559-\u055f\u0561-\u0587\u0589\u0903-\u0939\u093b\u093d-\u0940\u0949-\u094c\u094e-\u0950\u0958-\u0961\u0964-\u0980\u0982\u0983\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd-\u09c0\u09c7\u09c8\u09cb\u09cc\u09ce\u09d7\u09dc\u09dd\u09df-\u09e1\u09e6-\u09f1\u09f4-\u09fa\u0a03\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a3e-\u0a40\u0a59-\u0a5c\u0a5e\u0a66-\u0a6f\u0a72-\u0a74\u0a83\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd-\u0ac0\u0ac9\u0acb\u0acc\u0ad0\u0ae0\u0ae1\u0ae6-\u0af0\u0af9\u0b02\u0b03\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b3e\u0b40\u0b47\u0b48\u0b4b\u0b4c\u0b57\u0b5c\u0b5d\u0b5f-\u0b61\u0b66-\u0b77\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bbe\u0bbf\u0bc1\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcc\u0bd0\u0bd7\u0be6-\u0bf2\u0c01-\u0c03\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c39\u0c3d\u0c41-\u0c44\u0c58-\u0c5a\u0c60\u0c61\u0c66-\u0c6f\u0c7f\u0c82\u0c83\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd-\u0cc4\u0cc6-\u0cc8\u0cca\u0ccb\u0cd5\u0cd6\u0cde\u0ce0\u0ce1\u0ce6-\u0cef\u0cf1\u0cf2\u0d02\u0d03\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d-\u0d40\u0d46-\u0d48\u0d4a-\u0d4c\u0d4e\u0d57\u0d5f-\u0d61\u0d66-\u0d75\u0d79-\u0d7f\u0d82\u0d83\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0dcf-\u0dd1\u0dd8-\u0ddf\u0de6-\u0def\u0df2-\u0df4\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e4f-\u0e5b\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0ed0-\u0ed9\u0edc-\u0edf\u0f00-\u0f17\u0f1a-\u0f34\u0f36\u0f38\u0f3e-\u0f47\u0f49-\u0f6c\u0f7f\u0f85\u0f88-\u0f8c\u0fbe-\u0fc5\u0fc7-\u0fcc\u0fce-\u0fda\u1000-\u102c\u1031\u1038\u103b\u103c\u103f-\u1057\u105a-\u105d\u1061-\u1070\u1075-\u1081\u1083\u1084\u1087-\u108c\u108e-\u109c\u109e-\u10c5\u10c7\u10cd\u10d0-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1360-\u137c\u1380-\u138f\u13a0-\u13f5\u13f8-\u13fd\u1401-\u167f\u1681-\u169a\u16a0-\u16f8\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1735\u1736\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17b6\u17be-\u17c5\u17c7\u17c8\u17d4-\u17da\u17dc\u17e0-\u17e9\u1810-\u1819\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191e\u1923-\u1926\u1929-\u192b\u1930\u1931\u1933-\u1938\u1946-\u196d\u1970-\u1974\u1980-\u19ab\u19b0-\u19c9\u19d0-\u19da\u1a00-\u1a16\u1a19\u1a1a\u1a1e-\u1a55\u1a57\u1a61\u1a63\u1a64\u1a6d-\u1a72\u1a80-\u1a89\u1a90-\u1a99\u1aa0-\u1aad\u1b04-\u1b33\u1b35\u1b3b\u1b3d-\u1b41\u1b43-\u1b4b\u1b50-\u1b6a\u1b74-\u1b7c\u1b82-\u1ba1\u1ba6\u1ba7\u1baa\u1bae-\u1be5\u1be7\u1bea-\u1bec\u1bee\u1bf2\u1bf3\u1bfc-\u1c2b\u1c34\u1c35\u1c3b-\u1c49\u1c4d-\u1c7f\u1cc0-\u1cc7\u1cd3\u1ce1\u1ce9-\u1cec\u1cee-\u1cf3\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u200e\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u214f\u2160-\u2188\u2336-\u237a\u2395\u249c-\u24e9\u26ac\u2800-\u28ff\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d70\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u3005-\u3007\u3021-\u3029\u302e\u302f\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u3190-\u31ba\u31f0-\u321c\u3220-\u324f\u3260-\u327b\u327f-\u32b0\u32c0-\u32cb\u32d0-\u32fe\u3300-\u3376\u337b-\u33dd\u33e0-\u33fe\u3400-\u4db5\u4e00-\u9fd5\ua000-\ua48c\ua4d0-\ua60c\ua610-\ua62b\ua640-\ua66e\ua680-\ua69d\ua6a0-\ua6ef\ua6f2-\ua6f7\ua722-\ua787\ua789-\ua7ad\ua7b0-\ua7b7\ua7f7-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua824\ua827\ua830-\ua837\ua840-\ua873\ua880-\ua8c3\ua8ce-\ua8d9\ua8f2-\ua8fd\ua900-\ua925\ua92e-\ua946\ua952\ua953\ua95f-\ua97c\ua983-\ua9b2\ua9b4\ua9b5\ua9ba\ua9bb\ua9bd-\ua9cd\ua9cf-\ua9d9\ua9de-\ua9e4\ua9e6-\ua9fe\uaa00-\uaa28\uaa2f\uaa30\uaa33\uaa34\uaa40-\uaa42\uaa44-\uaa4b\uaa4d\uaa50-\uaa59\uaa5c-\uaa7b\uaa7d-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaaeb\uaaee-\uaaf5\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uab30-\uab65\uab70-\uabe4\uabe6\uabe7\uabe9-\uabec\uabf0-\uabf9\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\ue000-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc]|\ud800[\udc00-\udc0b]|\ud800[\udc0d-\udc26]|\ud800[\udc28-\udc3a]|\ud800\udc3c|\ud800\udc3d|\ud800[\udc3f-\udc4d]|\ud800[\udc50-\udc5d]|\ud800[\udc80-\udcfa]|\ud800\udd00|\ud800\udd02|\ud800[\udd07-\udd33]|\ud800[\udd37-\udd3f]|\ud800[\uddd0-\uddfc]|\ud800[\ude80-\ude9c]|\ud800[\udea0-\uded0]|\ud800[\udf00-\udf23]|\ud800[\udf30-\udf4a]|\ud800[\udf50-\udf75]|\ud800[\udf80-\udf9d]|\ud800[\udf9f-\udfc3]|\ud800[\udfc8-\udfd5]|\ud801[\udc00-\udc9d]|\ud801[\udca0-\udca9]|\ud801[\udd00-\udd27]|\ud801[\udd30-\udd63]|\ud801\udd6f|\ud801[\ude00-\udf36]|\ud801[\udf40-\udf55]|\ud801[\udf60-\udf67]|\ud804\udc00|\ud804[\udc02-\udc37]|\ud804[\udc47-\udc4d]|\ud804[\udc66-\udc6f]|\ud804[\udc82-\udcb2]|\ud804\udcb7|\ud804\udcb8|\ud804[\udcbb-\udcc1]|\ud804[\udcd0-\udce8]|\ud804[\udcf0-\udcf9]|\ud804[\udd03-\udd26]|\ud804\udd2c|\ud804[\udd36-\udd43]|\ud804[\udd50-\udd72]|\ud804[\udd74-\udd76]|\ud804[\udd82-\uddb5]|\ud804[\uddbf-\uddc9]|\ud804\uddcd|\ud804[\uddd0-\udddf]|\ud804[\udde1-\uddf4]|\ud804[\ude00-\ude11]|\ud804[\ude13-\ude2e]|\ud804\ude32|\ud804\ude33|\ud804\ude35|\ud804[\ude38-\ude3d]|\ud804[\ude80-\ude86]|\ud804\ude88|\ud804[\ude8a-\ude8d]|\ud804[\ude8f-\ude9d]|\ud804[\ude9f-\udea9]|\ud804[\udeb0-\udede]|\ud804[\udee0-\udee2]|\ud804[\udef0-\udef9]|\ud804\udf02|\ud804\udf03|\ud804[\udf05-\udf0c]|\ud804\udf0f|\ud804\udf10|\ud804[\udf13-\udf28]|\ud804[\udf2a-\udf30]|\ud804\udf32|\ud804\udf33|\ud804[\udf35-\udf39]|\ud804[\udf3d-\udf3f]|\ud804[\udf41-\udf44]|\ud804\udf47|\ud804\udf48|\ud804[\udf4b-\udf4d]|\ud804\udf50|\ud804\udf57|\ud804[\udf5d-\udf63]|\ud805[\udc80-\udcb2]|\ud805\udcb9|\ud805[\udcbb-\udcbe]|\ud805\udcc1|\ud805[\udcc4-\udcc7]|\ud805[\udcd0-\udcd9]|\ud805[\udd80-\uddb1]|\ud805[\uddb8-\uddbb]|\ud805\uddbe|\ud805[\uddc1-\udddb]|\ud805[\ude00-\ude32]|\ud805\ude3b|\ud805\ude3c|\ud805\ude3e|\ud805[\ude41-\ude44]|\ud805[\ude50-\ude59]|\ud805[\ude80-\udeaa]|\ud805\udeac|\ud805\udeae|\ud805\udeaf|\ud805\udeb6|\ud805[\udec0-\udec9]|\ud805[\udf00-\udf19]|\ud805\udf20|\ud805\udf21|\ud805\udf26|\ud805[\udf30-\udf3f]|\ud806[\udca0-\udcf2]|\ud806\udcff|\ud806[\udec0-\udef8]|\ud808[\udc00-\udf99]|\ud809[\udc00-\udc6e]|\ud809[\udc70-\udc74]|\ud809[\udc80-\udd43]|\ud80c[\udc00-\udfff]|\ud80d[\udc00-\udc2e]|\ud811[\udc00-\ude46]|\ud81a[\udc00-\ude38]|\ud81a[\ude40-\ude5e]|\ud81a[\ude60-\ude69]|\ud81a\ude6e|\ud81a\ude6f|\ud81a[\uded0-\udeed]|\ud81a\udef5|\ud81a[\udf00-\udf2f]|\ud81a[\udf37-\udf45]|\ud81a[\udf50-\udf59]|\ud81a[\udf5b-\udf61]|\ud81a[\udf63-\udf77]|\ud81a[\udf7d-\udf8f]|\ud81b[\udf00-\udf44]|\ud81b[\udf50-\udf7e]|\ud81b[\udf93-\udf9f]|\ud82c\udc00|\ud82c\udc01|\ud82f[\udc00-\udc6a]|\ud82f[\udc70-\udc7c]|\ud82f[\udc80-\udc88]|\ud82f[\udc90-\udc99]|\ud82f\udc9c|\ud82f\udc9f|\ud834[\udc00-\udcf5]|\ud834[\udd00-\udd26]|\ud834[\udd29-\udd66]|\ud834[\udd6a-\udd72]|\ud834\udd83|\ud834\udd84|\ud834[\udd8c-\udda9]|\ud834[\uddae-\udde8]|\ud834[\udf60-\udf71]|\ud835[\udc00-\udc54]|\ud835[\udc56-\udc9c]|\ud835\udc9e|\ud835\udc9f|\ud835\udca2|\ud835\udca5|\ud835\udca6|\ud835[\udca9-\udcac]|\ud835[\udcae-\udcb9]|\ud835\udcbb|\ud835[\udcbd-\udcc3]|\ud835[\udcc5-\udd05]|\ud835[\udd07-\udd0a]|\ud835[\udd0d-\udd14]|\ud835[\udd16-\udd1c]|\ud835[\udd1e-\udd39]|\ud835[\udd3b-\udd3e]|\ud835[\udd40-\udd44]|\ud835\udd46|\ud835[\udd4a-\udd50]|\ud835[\udd52-\udea5]|\ud835[\udea8-\udeda]|\ud835[\udedc-\udf14]|\ud835[\udf16-\udf4e]|\ud835[\udf50-\udf88]|\ud835[\udf8a-\udfc2]|\ud835[\udfc4-\udfcb]|\ud836[\udc00-\uddff]|\ud836[\ude37-\ude3a]|\ud836[\ude6d-\ude74]|\ud836[\ude76-\ude83]|\ud836[\ude85-\ude8b]|\ud83c[\udd10-\udd2e]|\ud83c[\udd30-\udd69]|\ud83c[\udd70-\udd9a]|\ud83c[\udde6-\ude02]|\ud83c[\ude10-\ude3a]|\ud83c[\ude40-\ude48]|\ud83c\ude50|\ud83c\ude51|[\ud840-\ud868][\udc00-\udfff]|\ud869[\udc00-\uded6]|\ud869[\udf00-\udfff]|[\ud86a-\ud86c][\udc00-\udfff]|\ud86d[\udc00-\udf34]|\ud86d[\udf40-\udfff]|\ud86e[\udc00-\udc1d]|\ud86e[\udc20-\udfff]|[\ud86f-\ud872][\udc00-\udfff]|\ud873[\udc00-\udea1]|\ud87e[\udc00-\ude1d]|[\udb80-\udbbe][\udc00-\udfff]|\udbbf[\udc00-\udffd]|[\udbc0-\udbfe][\udc00-\udfff]|\udbff[\udc00-\udffd]' +
			')|(' +
				'[\u0590\u05be\u05c0\u05c3\u05c6\u05c8-\u05ff\u07c0-\u07ea\u07f4\u07f5\u07fa-\u0815\u081a\u0824\u0828\u082e-\u0858\u085c-\u089f\u200f\ufb1d\ufb1f-\ufb28\ufb2a-\ufb4f\u0608\u060b\u060d\u061b-\u064a\u066d-\u066f\u0671-\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u0710\u0712-\u072f\u074b-\u07a5\u07b1-\u07bf\u08a0-\u08e2\ufb50-\ufd3d\ufd40-\ufdcf\ufdf0-\ufdfc\ufdfe\ufdff\ufe70-\ufefe]|\ud802[\udc00-\udd1e]|\ud802[\udd20-\ude00]|\ud802\ude04|\ud802[\ude07-\ude0b]|\ud802[\ude10-\ude37]|\ud802[\ude3b-\ude3e]|\ud802[\ude40-\udee4]|\ud802[\udee7-\udf38]|\ud802[\udf40-\udfff]|\ud803[\udc00-\ude5f]|\ud803[\ude7f-\udfff]|\ud83a[\udc00-\udccf]|\ud83a[\udcd7-\udfff]|\ud83b[\udc00-\uddff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\ude00-\udeef]|\ud83b[\udef2-\udeff]' +
			')' +
		')'
	);

	/**
	 * Gets directionality of the first strongly directional codepoint
	 *
	 * This is the rule the BIDI algorithm uses to determine the directionality of
	 * paragraphs ( http://unicode.org/reports/tr9/#The_Paragraph_Level ) and
	 * FSI isolates ( http://unicode.org/reports/tr9/#Explicit_Directional_Isolates ).
	 *
	 * TODO: Does not handle BIDI control characters inside the text.
	 * TODO: Does not handle unallocated characters.
	 *
	 * @param {string} text The text from which to extract initial directionality.
	 * @return {string} Directionality (either 'ltr' or 'rtl')
	 */
	function strongDirFromContent( text ) {
		var m = text.match( strongDirRegExp );
		if ( !m ) {
			return null;
		}
		if ( m[ 2 ] === undefined ) {
			return 'ltr';
		}
		return 'rtl';
	}

	$.extend( $.i18n.parser.emitter, {
		/**
		 * Wraps argument with unicode control characters for directionality safety
		 *
		 * This solves the problem where directionality-neutral characters at the edge of
		 * the argument string get interpreted with the wrong directionality from the
		 * enclosing context, giving renderings that look corrupted like "(Ben_(WMF".
		 *
		 * The wrapping is LRE...PDF or RLE...PDF, depending on the detected
		 * directionality of the argument string, using the BIDI algorithm's own "First
		 * strong directional codepoint" rule. Essentially, this works round the fact that
		 * there is no embedding equivalent of U+2068 FSI (isolation with heuristic
		 * direction inference). The latter is cleaner but still not widely supported.
		 *
		 * @param {string[]} nodes The text nodes from which to take the first item.
		 * @return {string} Wrapped String of content as needed.
		 */
		bidi: function ( nodes ) {
			var dir = strongDirFromContent( nodes[ 0 ] );
			if ( dir === 'ltr' ) {
				// Wrap in LEFT-TO-RIGHT EMBEDDING ... POP DIRECTIONAL FORMATTING
				return '\u202A' + nodes[ 0 ] + '\u202C';
			}
			if ( dir === 'rtl' ) {
				// Wrap in RIGHT-TO-LEFT EMBEDDING ... POP DIRECTIONAL FORMATTING
				return '\u202B' + nodes[ 0 ] + '\u202C';
			}
			// No strong directionality: do not wrap
			return nodes[ 0 ];
		}
	} );
}( jQuery ) );

},{}],14:[function(require,module,exports){
/*!
 * jQuery Internationalization library
 *
 * Copyright (C) 2011-2013 Santhosh Thottingal, Neil Kandalgaonkar
 *
 * jquery.i18n is dual licensed GPLv2 or later and MIT. You don't have to do
 * anything special to choose one license or the other and you don't have to
 * notify anyone which license you are using. You are free to use
 * UniversalLanguageSelector in commercial projects as long as the copyright
 * header is left intact. See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */

( function ( $ ) {
	'use strict';

	var MessageParserEmitter = function () {
		this.language = $.i18n.languages[ String.locale ] || $.i18n.languages[ 'default' ];
	};

	MessageParserEmitter.prototype = {
		constructor: MessageParserEmitter,

		/**
		 * (We put this method definition here, and not in prototype, to make
		 * sure it's not overwritten by any magic.) Walk entire node structure,
		 * applying replacements and template functions when appropriate
		 *
		 * @param {Mixed} node abstract syntax tree (top node or subnode)
		 * @param {Array} replacements for $1, $2, ... $n
		 * @return {Mixed} single-string node or array of nodes suitable for
		 *  jQuery appending.
		 */
		emit: function ( node, replacements ) {
			var ret, subnodes, operation,
				messageParserEmitter = this;

			switch ( typeof node ) {
				case 'string':
				case 'number':
					ret = node;
					break;
				case 'object':
				// node is an array of nodes
					subnodes = $.map( node.slice( 1 ), function ( n ) {
						return messageParserEmitter.emit( n, replacements );
					} );

					operation = node[ 0 ].toLowerCase();

					if ( typeof messageParserEmitter[ operation ] === 'function' ) {
						ret = messageParserEmitter[ operation ]( subnodes, replacements );
					} else {
						throw new Error( 'unknown operation "' + operation + '"' );
					}

					break;
				case 'undefined':
				// Parsing the empty string (as an entire expression, or as a
				// paramExpression in a template) results in undefined
				// Perhaps a more clever parser can detect this, and return the
				// empty string? Or is that useful information?
				// The logical thing is probably to return the empty string here
				// when we encounter undefined.
					ret = '';
					break;
				default:
					throw new Error( 'unexpected type in AST: ' + typeof node );
			}

			return ret;
		},

		/**
		 * Parsing has been applied depth-first we can assume that all nodes
		 * here are single nodes Must return a single node to parents -- a
		 * jQuery with synthetic span However, unwrap any other synthetic spans
		 * in our children and pass them upwards
		 *
		 * @param {Array} nodes Mixed, some single nodes, some arrays of nodes.
		 * @return {string}
		 */
		concat: function ( nodes ) {
			var result = '';

			$.each( nodes, function ( i, node ) {
				// strings, integers, anything else
				result += node;
			} );

			return result;
		},

		/**
		 * Return escaped replacement of correct index, or string if
		 * unavailable. Note that we expect the parsed parameter to be
		 * zero-based. i.e. $1 should have become [ 0 ]. if the specified
		 * parameter is not found return the same string (e.g. "$99" ->
		 * parameter 98 -> not found -> return "$99" ) TODO throw error if
		 * nodes.length > 1 ?
		 *
		 * @param {Array} nodes One element, integer, n >= 0
		 * @param {Array} replacements for $1, $2, ... $n
		 * @return {string} replacement
		 */
		replace: function ( nodes, replacements ) {
			var index = parseInt( nodes[ 0 ], 10 );

			if ( index < replacements.length ) {
				// replacement is not a string, don't touch!
				return replacements[ index ];
			} else {
				// index not found, fallback to displaying variable
				return '$' + ( index + 1 );
			}
		},

		/**
		 * Transform parsed structure into pluralization n.b. The first node may
		 * be a non-integer (for instance, a string representing an Arabic
		 * number). So convert it back with the current language's
		 * convertNumber.
		 *
		 * @param {Array} nodes List [ {String|Number}, {String}, {String} ... ]
		 * @return {string} selected pluralized form according to current
		 *  language.
		 */
		plural: function ( nodes ) {
			var count = parseFloat( this.language.convertNumber( nodes[ 0 ], 10 ) ),
				forms = nodes.slice( 1 );

			return forms.length ? this.language.convertPlural( count, forms ) : '';
		},

		/**
		 * Transform parsed structure into gender Usage
		 * {{gender:gender|masculine|feminine|neutral}}.
		 *
		 * @param {Array} nodes List [ {String}, {String}, {String} , {String} ]
		 * @return {string} selected gender form according to current language
		 */
		gender: function ( nodes ) {
			var gender = nodes[ 0 ],
				forms = nodes.slice( 1 );

			return this.language.gender( gender, forms );
		},

		/**
		 * Transform parsed structure into grammar conversion. Invoked by
		 * putting {{grammar:form|word}} in a message
		 *
		 * @param {Array} nodes List [{Grammar case eg: genitive}, {String word}]
		 * @return {string} selected grammatical form according to current
		 *  language.
		 */
		grammar: function ( nodes ) {
			var form = nodes[ 0 ],
				word = nodes[ 1 ];

			return word && form && this.language.convertGrammar( word, form );
		}
	};

	$.extend( $.i18n.parser.emitter, new MessageParserEmitter() );
}( jQuery ) );

},{}],15:[function(require,module,exports){
/*!
 * jQuery Internationalization library
 *
 * Copyright (C) 2012 Santhosh Thottingal
 *
 * jquery.i18n is dual licensed GPLv2 or later and MIT. You don't have to do anything special to
 * choose one license or the other and you don't have to notify anyone which license you are using.
 * You are free to use UniversalLanguageSelector in commercial projects as long as the copyright
 * header is left intact. See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */
( function ( $ ) {
	'use strict';

	$.i18n = $.i18n || {};
	$.extend( $.i18n.fallbacks, {
		ab: [ 'ru' ],
		ace: [ 'id' ],
		aln: [ 'sq' ],
		// Not so standard - als is supposed to be Tosk Albanian,
		// but in Wikipedia it's used for a Germanic language.
		als: [ 'gsw', 'de' ],
		an: [ 'es' ],
		anp: [ 'hi' ],
		arn: [ 'es' ],
		arz: [ 'ar' ],
		av: [ 'ru' ],
		ay: [ 'es' ],
		ba: [ 'ru' ],
		bar: [ 'de' ],
		'bat-smg': [ 'sgs', 'lt' ],
		bcc: [ 'fa' ],
		'be-x-old': [ 'be-tarask' ],
		bh: [ 'bho' ],
		bjn: [ 'id' ],
		bm: [ 'fr' ],
		bpy: [ 'bn' ],
		bqi: [ 'fa' ],
		bug: [ 'id' ],
		'cbk-zam': [ 'es' ],
		ce: [ 'ru' ],
		crh: [ 'crh-latn' ],
		'crh-cyrl': [ 'ru' ],
		csb: [ 'pl' ],
		cv: [ 'ru' ],
		'de-at': [ 'de' ],
		'de-ch': [ 'de' ],
		'de-formal': [ 'de' ],
		dsb: [ 'de' ],
		dtp: [ 'ms' ],
		egl: [ 'it' ],
		eml: [ 'it' ],
		ff: [ 'fr' ],
		fit: [ 'fi' ],
		'fiu-vro': [ 'vro', 'et' ],
		frc: [ 'fr' ],
		frp: [ 'fr' ],
		frr: [ 'de' ],
		fur: [ 'it' ],
		gag: [ 'tr' ],
		gan: [ 'gan-hant', 'zh-hant', 'zh-hans' ],
		'gan-hans': [ 'zh-hans' ],
		'gan-hant': [ 'zh-hant', 'zh-hans' ],
		gl: [ 'pt' ],
		glk: [ 'fa' ],
		gn: [ 'es' ],
		gsw: [ 'de' ],
		hif: [ 'hif-latn' ],
		hsb: [ 'de' ],
		ht: [ 'fr' ],
		ii: [ 'zh-cn', 'zh-hans' ],
		inh: [ 'ru' ],
		iu: [ 'ike-cans' ],
		jut: [ 'da' ],
		jv: [ 'id' ],
		kaa: [ 'kk-latn', 'kk-cyrl' ],
		kbd: [ 'kbd-cyrl' ],
		khw: [ 'ur' ],
		kiu: [ 'tr' ],
		kk: [ 'kk-cyrl' ],
		'kk-arab': [ 'kk-cyrl' ],
		'kk-latn': [ 'kk-cyrl' ],
		'kk-cn': [ 'kk-arab', 'kk-cyrl' ],
		'kk-kz': [ 'kk-cyrl' ],
		'kk-tr': [ 'kk-latn', 'kk-cyrl' ],
		kl: [ 'da' ],
		'ko-kp': [ 'ko' ],
		koi: [ 'ru' ],
		krc: [ 'ru' ],
		ks: [ 'ks-arab' ],
		ksh: [ 'de' ],
		ku: [ 'ku-latn' ],
		'ku-arab': [ 'ckb' ],
		kv: [ 'ru' ],
		lad: [ 'es' ],
		lb: [ 'de' ],
		lbe: [ 'ru' ],
		lez: [ 'ru' ],
		li: [ 'nl' ],
		lij: [ 'it' ],
		liv: [ 'et' ],
		lmo: [ 'it' ],
		ln: [ 'fr' ],
		ltg: [ 'lv' ],
		lzz: [ 'tr' ],
		mai: [ 'hi' ],
		'map-bms': [ 'jv', 'id' ],
		mg: [ 'fr' ],
		mhr: [ 'ru' ],
		min: [ 'id' ],
		mo: [ 'ro' ],
		mrj: [ 'ru' ],
		mwl: [ 'pt' ],
		myv: [ 'ru' ],
		mzn: [ 'fa' ],
		nah: [ 'es' ],
		nap: [ 'it' ],
		nds: [ 'de' ],
		'nds-nl': [ 'nl' ],
		'nl-informal': [ 'nl' ],
		no: [ 'nb' ],
		os: [ 'ru' ],
		pcd: [ 'fr' ],
		pdc: [ 'de' ],
		pdt: [ 'de' ],
		pfl: [ 'de' ],
		pms: [ 'it' ],
		pt: [ 'pt-br' ],
		'pt-br': [ 'pt' ],
		qu: [ 'es' ],
		qug: [ 'qu', 'es' ],
		rgn: [ 'it' ],
		rmy: [ 'ro' ],
		'roa-rup': [ 'rup' ],
		rue: [ 'uk', 'ru' ],
		ruq: [ 'ruq-latn', 'ro' ],
		'ruq-cyrl': [ 'mk' ],
		'ruq-latn': [ 'ro' ],
		sa: [ 'hi' ],
		sah: [ 'ru' ],
		scn: [ 'it' ],
		sg: [ 'fr' ],
		sgs: [ 'lt' ],
		sli: [ 'de' ],
		sr: [ 'sr-ec' ],
		srn: [ 'nl' ],
		stq: [ 'de' ],
		su: [ 'id' ],
		szl: [ 'pl' ],
		tcy: [ 'kn' ],
		tg: [ 'tg-cyrl' ],
		tt: [ 'tt-cyrl', 'ru' ],
		'tt-cyrl': [ 'ru' ],
		ty: [ 'fr' ],
		udm: [ 'ru' ],
		ug: [ 'ug-arab' ],
		uk: [ 'ru' ],
		vec: [ 'it' ],
		vep: [ 'et' ],
		vls: [ 'nl' ],
		vmf: [ 'de' ],
		vot: [ 'fi' ],
		vro: [ 'et' ],
		wa: [ 'fr' ],
		wo: [ 'fr' ],
		wuu: [ 'zh-hans' ],
		xal: [ 'ru' ],
		xmf: [ 'ka' ],
		yi: [ 'he' ],
		za: [ 'zh-hans' ],
		zea: [ 'nl' ],
		zh: [ 'zh-hans' ],
		'zh-classical': [ 'lzh' ],
		'zh-cn': [ 'zh-hans' ],
		'zh-hant': [ 'zh-hans' ],
		'zh-hk': [ 'zh-hant', 'zh-hans' ],
		'zh-min-nan': [ 'nan' ],
		'zh-mo': [ 'zh-hk', 'zh-hant', 'zh-hans' ],
		'zh-my': [ 'zh-sg', 'zh-hans' ],
		'zh-sg': [ 'zh-hans' ],
		'zh-tw': [ 'zh-hant', 'zh-hans' ],
		'zh-yue': [ 'yue' ]
	} );
}( jQuery ) );

},{}],16:[function(require,module,exports){
/*!
 * jQuery Internationalization library
 *
 * Copyright (C) 2012 Santhosh Thottingal
 *
 * jquery.i18n is dual licensed GPLv2 or later and MIT. You don't have to do
 * anything special to choose one license or the other and you don't have to
 * notify anyone which license you are using. You are free to use
 * UniversalLanguageSelector in commercial projects as long as the copyright
 * header is left intact. See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */

( function ( $ ) {
	'use strict';

	var nav, I18N,
		slice = Array.prototype.slice;
	/**
	 * @constructor
	 * @param {Object} options
	 */
	I18N = function ( options ) {
		// Load defaults
		this.options = $.extend( {}, I18N.defaults, options );

		this.parser = this.options.parser;
		this.locale = this.options.locale;
		this.messageStore = this.options.messageStore;
		this.languages = {};

		this.init();
	};

	I18N.prototype = {
		/**
		 * Initialize by loading locales and setting up
		 * String.prototype.toLocaleString and String.locale.
		 */
		init: function () {
			var i18n = this;

			// Set locale of String environment
			String.locale = i18n.locale;

			// Override String.localeString method
			String.prototype.toLocaleString = function () {
				var localeParts, localePartIndex, value, locale, fallbackIndex,
					tryingLocale, message;

				value = this.valueOf();
				locale = i18n.locale;
				fallbackIndex = 0;

				while ( locale ) {
					// Iterate through locales starting at most-specific until
					// localization is found. As in fi-Latn-FI, fi-Latn and fi.
					localeParts = locale.split( '-' );
					localePartIndex = localeParts.length;

					do {
						tryingLocale = localeParts.slice( 0, localePartIndex ).join( '-' );
						message = i18n.messageStore.get( tryingLocale, value );

						if ( message ) {
							return message;
						}

						localePartIndex--;
					} while ( localePartIndex );

					if ( locale === 'en' ) {
						break;
					}

					locale = ( $.i18n.fallbacks[ i18n.locale ] && $.i18n.fallbacks[ i18n.locale ][ fallbackIndex ] ) ||
						i18n.options.fallbackLocale;
					$.i18n.log( 'Trying fallback locale for ' + i18n.locale + ': ' + locale + ' (' + value + ')' );

					fallbackIndex++;
				}

				// key not found
				return '';
			};
		},

		/*
		 * Destroy the i18n instance.
		 */
		destroy: function () {
			$.removeData( document, 'i18n' );
		},

		/**
		 * General message loading API This can take a URL string for
		 * the json formatted messages. Example:
		 * <code>load('path/to/all_localizations.json');</code>
		 *
		 * To load a localization file for a locale:
		 * <code>
		 * load('path/to/de-messages.json', 'de' );
		 * </code>
		 *
		 * To load a localization file from a directory:
		 * <code>
		 * load('path/to/i18n/directory', 'de' );
		 * </code>
		 * The above method has the advantage of fallback resolution.
		 * ie, it will automatically load the fallback locales for de.
		 * For most usecases, this is the recommended method.
		 * It is optional to have trailing slash at end.
		 *
		 * A data object containing message key- message translation mappings
		 * can also be passed. Example:
		 * <code>
		 * load( { 'hello' : 'Hello' }, optionalLocale );
		 * </code>
		 *
		 * A source map containing key-value pair of languagename and locations
		 * can also be passed. Example:
		 * <code>
		 * load( {
		 * bn: 'i18n/bn.json',
		 * he: 'i18n/he.json',
		 * en: 'i18n/en.json'
		 * } )
		 * </code>
		 *
		 * If the data argument is null/undefined/false,
		 * all cached messages for the i18n instance will get reset.
		 *
		 * @param {string|Object} source
		 * @param {string} locale Language tag
		 * @return {jQuery.Promise}
		 */
		load: function ( source, locale ) {
			var fallbackLocales, locIndex, fallbackLocale, sourceMap = {};
			if ( !source && !locale ) {
				source = 'i18n/' + $.i18n().locale + '.json';
				locale = $.i18n().locale;
			}
			if ( typeof source === 'string' &&
				// source extension should be json, but can have query params after that.
				source.split( '?' )[ 0 ].split( '.' ).pop() !== 'json'
			) {
				// Load specified locale then check for fallbacks when directory is specified in load()
				sourceMap[ locale ] = source + '/' + locale + '.json';
				fallbackLocales = ( $.i18n.fallbacks[ locale ] || [] )
					.concat( this.options.fallbackLocale );
				for ( locIndex = 0; locIndex < fallbackLocales.length; locIndex++ ) {
					fallbackLocale = fallbackLocales[ locIndex ];
					sourceMap[ fallbackLocale ] = source + '/' + fallbackLocale + '.json';
				}
				return this.load( sourceMap );
			} else {
				return this.messageStore.load( source, locale );
			}

		},

		/**
		 * Does parameter and magic word substitution.
		 *
		 * @param {string} key Message key
		 * @param {Array} parameters Message parameters
		 * @return {string}
		 */
		parse: function ( key, parameters ) {
			var message = key.toLocaleString();
			// FIXME: This changes the state of the I18N object,
			// should probably not change the 'this.parser' but just
			// pass it to the parser.
			this.parser.language = $.i18n.languages[ $.i18n().locale ] || $.i18n.languages[ 'default' ];
			if ( message === '' ) {
				message = key;
			}
			return this.parser.parse( message, parameters );
		}
	};

	/**
	 * Process a message from the $.I18N instance
	 * for the current document, stored in jQuery.data(document).
	 *
	 * @param {string} key Key of the message.
	 * @param {string} param1 [param...] Variadic list of parameters for {key}.
	 * @return {string|$.I18N} Parsed message, or if no key was given
	 * the instance of $.I18N is returned.
	 */
	$.i18n = function ( key, param1 ) {
		var parameters,
			i18n = $.data( document, 'i18n' ),
			options = typeof key === 'object' && key;

		// If the locale option for this call is different then the setup so far,
		// update it automatically. This doesn't just change the context for this
		// call but for all future call as well.
		// If there is no i18n setup yet, don't do this. It will be taken care of
		// by the `new I18N` construction below.
		// NOTE: It should only change language for this one call.
		// Then cache instances of I18N somewhere.
		if ( options && options.locale && i18n && i18n.locale !== options.locale ) {
			String.locale = i18n.locale = options.locale;
		}

		if ( !i18n ) {
			i18n = new I18N( options );
			$.data( document, 'i18n', i18n );
		}

		if ( typeof key === 'string' ) {
			if ( param1 !== undefined ) {
				parameters = slice.call( arguments, 1 );
			} else {
				parameters = [];
			}

			return i18n.parse( key, parameters );
		} else {
			// FIXME: remove this feature/bug.
			return i18n;
		}
	};

	$.fn.i18n = function () {
		var i18n = $.data( document, 'i18n' );

		if ( !i18n ) {
			i18n = new I18N();
			$.data( document, 'i18n', i18n );
		}
		String.locale = i18n.locale;
		return this.each( function () {
			var $this = $( this ),
				messageKey = $this.data( 'i18n' ),
				lBracket, rBracket, type, key;

			if ( messageKey ) {
				lBracket = messageKey.indexOf( '[' );
				rBracket = messageKey.indexOf( ']' );
				if ( lBracket !== -1 && rBracket !== -1 && lBracket < rBracket ) {
					type = messageKey.slice( lBracket + 1, rBracket );
					key = messageKey.slice( rBracket + 1 );
					if ( type === 'html' ) {
						$this.html( i18n.parse( key ) );
					} else {
						$this.attr( type, i18n.parse( key ) );
					}
				} else {
					$this.text( i18n.parse( messageKey ) );
				}
			} else {
				$this.find( '[data-i18n]' ).i18n();
			}
		} );
	};

	String.locale = String.locale || $( 'html' ).attr( 'lang' );

	if ( !String.locale ) {
		if ( typeof window.navigator !== undefined ) {
			nav = window.navigator;
			String.locale = nav.language || nav.userLanguage || '';
		} else {
			String.locale = '';
		}
	}

	$.i18n.languages = {};
	$.i18n.messageStore = $.i18n.messageStore || {};
	$.i18n.parser = {
		// The default parser only handles variable substitution
		parse: function ( message, parameters ) {
			return message.replace( /\$(\d+)/g, function ( str, match ) {
				var index = parseInt( match, 10 ) - 1;
				return parameters[ index ] !== undefined ? parameters[ index ] : '$' + match;
			} );
		},
		emitter: {}
	};
	$.i18n.fallbacks = {};
	$.i18n.debug = false;
	$.i18n.log = function ( /* arguments */ ) {
		if ( window.console && $.i18n.debug ) {
			window.console.log.apply( window.console, arguments );
		}
	};
	/* Static members */
	I18N.defaults = {
		locale: String.locale,
		fallbackLocale: 'en',
		parser: $.i18n.parser,
		messageStore: $.i18n.messageStore
	};

	// Expose constructor
	$.i18n.constructor = I18N;
}( jQuery ) );

},{}],17:[function(require,module,exports){
/* global pluralRuleParser */
( function ( $ ) {
	'use strict';

	// jscs:disable
	var language = {
		// CLDR plural rules generated using
		// libs/CLDRPluralRuleParser/tools/PluralXML2JSON.html
		pluralRules: {
			ak: {
				one: 'n = 0..1'
			},
			am: {
				one: 'i = 0 or n = 1'
			},
			ar: {
				zero: 'n = 0',
				one: 'n = 1',
				two: 'n = 2',
				few: 'n % 100 = 3..10',
				many: 'n % 100 = 11..99'
			},
			ars: {
				zero: 'n = 0',
				one: 'n = 1',
				two: 'n = 2',
				few: 'n % 100 = 3..10',
				many: 'n % 100 = 11..99'
			},
			as: {
				one: 'i = 0 or n = 1'
			},
			be: {
				one: 'n % 10 = 1 and n % 100 != 11',
				few: 'n % 10 = 2..4 and n % 100 != 12..14',
				many: 'n % 10 = 0 or n % 10 = 5..9 or n % 100 = 11..14'
			},
			bh: {
				one: 'n = 0..1'
			},
			bn: {
				one: 'i = 0 or n = 1'
			},
			br: {
				one: 'n % 10 = 1 and n % 100 != 11,71,91',
				two: 'n % 10 = 2 and n % 100 != 12,72,92',
				few: 'n % 10 = 3..4,9 and n % 100 != 10..19,70..79,90..99',
				many: 'n != 0 and n % 1000000 = 0'
			},
			bs: {
				one: 'v = 0 and i % 10 = 1 and i % 100 != 11 or f % 10 = 1 and f % 100 != 11',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14 or f % 10 = 2..4 and f % 100 != 12..14'
			},
			cs: {
				one: 'i = 1 and v = 0',
				few: 'i = 2..4 and v = 0',
				many: 'v != 0'
			},
			cy: {
				zero: 'n = 0',
				one: 'n = 1',
				two: 'n = 2',
				few: 'n = 3',
				many: 'n = 6'
			},
			da: {
				one: 'n = 1 or t != 0 and i = 0,1'
			},
			dsb: {
				one: 'v = 0 and i % 100 = 1 or f % 100 = 1',
				two: 'v = 0 and i % 100 = 2 or f % 100 = 2',
				few: 'v = 0 and i % 100 = 3..4 or f % 100 = 3..4'
			},
			fa: {
				one: 'i = 0 or n = 1'
			},
			ff: {
				one: 'i = 0,1'
			},
			fil: {
				one: 'v = 0 and i = 1,2,3 or v = 0 and i % 10 != 4,6,9 or v != 0 and f % 10 != 4,6,9'
			},
			fr: {
				one: 'i = 0,1'
			},
			ga: {
				one: 'n = 1',
				two: 'n = 2',
				few: 'n = 3..6',
				many: 'n = 7..10'
			},
			gd: {
				one: 'n = 1,11',
				two: 'n = 2,12',
				few: 'n = 3..10,13..19'
			},
			gu: {
				one: 'i = 0 or n = 1'
			},
			guw: {
				one: 'n = 0..1'
			},
			gv: {
				one: 'v = 0 and i % 10 = 1',
				two: 'v = 0 and i % 10 = 2',
				few: 'v = 0 and i % 100 = 0,20,40,60,80',
				many: 'v != 0'
			},
			he: {
				one: 'i = 1 and v = 0',
				two: 'i = 2 and v = 0',
				many: 'v = 0 and n != 0..10 and n % 10 = 0'
			},
			hi: {
				one: 'i = 0 or n = 1'
			},
			hr: {
				one: 'v = 0 and i % 10 = 1 and i % 100 != 11 or f % 10 = 1 and f % 100 != 11',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14 or f % 10 = 2..4 and f % 100 != 12..14'
			},
			hsb: {
				one: 'v = 0 and i % 100 = 1 or f % 100 = 1',
				two: 'v = 0 and i % 100 = 2 or f % 100 = 2',
				few: 'v = 0 and i % 100 = 3..4 or f % 100 = 3..4'
			},
			hy: {
				one: 'i = 0,1'
			},
			is: {
				one: 't = 0 and i % 10 = 1 and i % 100 != 11 or t != 0'
			},
			iu: {
				one: 'n = 1',
				two: 'n = 2'
			},
			iw: {
				one: 'i = 1 and v = 0',
				two: 'i = 2 and v = 0',
				many: 'v = 0 and n != 0..10 and n % 10 = 0'
			},
			kab: {
				one: 'i = 0,1'
			},
			kn: {
				one: 'i = 0 or n = 1'
			},
			kw: {
				one: 'n = 1',
				two: 'n = 2'
			},
			lag: {
				zero: 'n = 0',
				one: 'i = 0,1 and n != 0'
			},
			ln: {
				one: 'n = 0..1'
			},
			lt: {
				one: 'n % 10 = 1 and n % 100 != 11..19',
				few: 'n % 10 = 2..9 and n % 100 != 11..19',
				many: 'f != 0'
			},
			lv: {
				zero: 'n % 10 = 0 or n % 100 = 11..19 or v = 2 and f % 100 = 11..19',
				one: 'n % 10 = 1 and n % 100 != 11 or v = 2 and f % 10 = 1 and f % 100 != 11 or v != 2 and f % 10 = 1'
			},
			mg: {
				one: 'n = 0..1'
			},
			mk: {
				one: 'v = 0 and i % 10 = 1 or f % 10 = 1'
			},
			mo: {
				one: 'i = 1 and v = 0',
				few: 'v != 0 or n = 0 or n != 1 and n % 100 = 1..19'
			},
			mr: {
				one: 'i = 0 or n = 1'
			},
			mt: {
				one: 'n = 1',
				few: 'n = 0 or n % 100 = 2..10',
				many: 'n % 100 = 11..19'
			},
			naq: {
				one: 'n = 1',
				two: 'n = 2'
			},
			nso: {
				one: 'n = 0..1'
			},
			pa: {
				one: 'n = 0..1'
			},
			pl: {
				one: 'i = 1 and v = 0',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14',
				many: 'v = 0 and i != 1 and i % 10 = 0..1 or v = 0 and i % 10 = 5..9 or v = 0 and i % 100 = 12..14'
			},
			prg: {
				zero: 'n % 10 = 0 or n % 100 = 11..19 or v = 2 and f % 100 = 11..19',
				one: 'n % 10 = 1 and n % 100 != 11 or v = 2 and f % 10 = 1 and f % 100 != 11 or v != 2 and f % 10 = 1'
			},
			pt: {
				one: 'i = 0..1'
			},
			ro: {
				one: 'i = 1 and v = 0',
				few: 'v != 0 or n = 0 or n != 1 and n % 100 = 1..19'
			},
			ru: {
				one: 'v = 0 and i % 10 = 1 and i % 100 != 11',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14',
				many: 'v = 0 and i % 10 = 0 or v = 0 and i % 10 = 5..9 or v = 0 and i % 100 = 11..14'
			},
			se: {
				one: 'n = 1',
				two: 'n = 2'
			},
			sh: {
				one: 'v = 0 and i % 10 = 1 and i % 100 != 11 or f % 10 = 1 and f % 100 != 11',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14 or f % 10 = 2..4 and f % 100 != 12..14'
			},
			shi: {
				one: 'i = 0 or n = 1',
				few: 'n = 2..10'
			},
			si: {
				one: 'n = 0,1 or i = 0 and f = 1'
			},
			sk: {
				one: 'i = 1 and v = 0',
				few: 'i = 2..4 and v = 0',
				many: 'v != 0'
			},
			sl: {
				one: 'v = 0 and i % 100 = 1',
				two: 'v = 0 and i % 100 = 2',
				few: 'v = 0 and i % 100 = 3..4 or v != 0'
			},
			sma: {
				one: 'n = 1',
				two: 'n = 2'
			},
			smi: {
				one: 'n = 1',
				two: 'n = 2'
			},
			smj: {
				one: 'n = 1',
				two: 'n = 2'
			},
			smn: {
				one: 'n = 1',
				two: 'n = 2'
			},
			sms: {
				one: 'n = 1',
				two: 'n = 2'
			},
			sr: {
				one: 'v = 0 and i % 10 = 1 and i % 100 != 11 or f % 10 = 1 and f % 100 != 11',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14 or f % 10 = 2..4 and f % 100 != 12..14'
			},
			ti: {
				one: 'n = 0..1'
			},
			tl: {
				one: 'v = 0 and i = 1,2,3 or v = 0 and i % 10 != 4,6,9 or v != 0 and f % 10 != 4,6,9'
			},
			tzm: {
				one: 'n = 0..1 or n = 11..99'
			},
			uk: {
				one: 'v = 0 and i % 10 = 1 and i % 100 != 11',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14',
				many: 'v = 0 and i % 10 = 0 or v = 0 and i % 10 = 5..9 or v = 0 and i % 100 = 11..14'
			},
			wa: {
				one: 'n = 0..1'
			},
			zu: {
				one: 'i = 0 or n = 1'
			}
		},
		// jscs:enable

		/**
		 * Plural form transformations, needed for some languages.
		 *
		 * @param {integer} count
		 *            Non-localized quantifier
		 * @param {Array} forms
		 *            List of plural forms
		 * @return {string} Correct form for quantifier in this language
		 */
		convertPlural: function ( count, forms ) {
			var pluralRules,
				pluralFormIndex,
				index,
				explicitPluralPattern = new RegExp( '\\d+=', 'i' ),
				formCount,
				form;

			if ( !forms || forms.length === 0 ) {
				return '';
			}

			// Handle for Explicit 0= & 1= values
			for ( index = 0; index < forms.length; index++ ) {
				form = forms[ index ];
				if ( explicitPluralPattern.test( form ) ) {
					formCount = parseInt( form.slice( 0, form.indexOf( '=' ) ), 10 );
					if ( formCount === count ) {
						return ( form.slice( form.indexOf( '=' ) + 1 ) );
					}
					forms[ index ] = undefined;
				}
			}

			forms = $.map( forms, function ( form ) {
				if ( form !== undefined ) {
					return form;
				}
			} );

			pluralRules = this.pluralRules[ $.i18n().locale ];

			if ( !pluralRules ) {
				// default fallback.
				return ( count === 1 ) ? forms[ 0 ] : forms[ 1 ];
			}

			pluralFormIndex = this.getPluralForm( count, pluralRules );
			pluralFormIndex = Math.min( pluralFormIndex, forms.length - 1 );

			return forms[ pluralFormIndex ];
		},

		/**
		 * For the number, get the plural for index
		 *
		 * @param {integer} number
		 * @param {Object} pluralRules
		 * @return {integer} plural form index
		 */
		getPluralForm: function ( number, pluralRules ) {
			var i,
				pluralForms = [ 'zero', 'one', 'two', 'few', 'many', 'other' ],
				pluralFormIndex = 0;

			for ( i = 0; i < pluralForms.length; i++ ) {
				if ( pluralRules[ pluralForms[ i ] ] ) {
					if ( pluralRuleParser( pluralRules[ pluralForms[ i ] ], number ) ) {
						return pluralFormIndex;
					}

					pluralFormIndex++;
				}
			}

			return pluralFormIndex;
		},

		/**
		 * Converts a number using digitTransformTable.
		 *
		 * @param {number} num Value to be converted
		 * @param {boolean} integer Convert the return value to an integer
		 * @return {string} The number converted into a String.
		 */
		convertNumber: function ( num, integer ) {
			var tmp, item, i,
				transformTable, numberString, convertedNumber;

			// Set the target Transform table:
			transformTable = this.digitTransformTable( $.i18n().locale );
			numberString = String( num );
			convertedNumber = '';

			if ( !transformTable ) {
				return num;
			}

			// Check if the restore to Latin number flag is set:
			if ( integer ) {
				if ( parseFloat( num, 10 ) === num ) {
					return num;
				}

				tmp = [];

				for ( item in transformTable ) {
					tmp[ transformTable[ item ] ] = item;
				}

				transformTable = tmp;
			}

			for ( i = 0; i < numberString.length; i++ ) {
				if ( transformTable[ numberString[ i ] ] ) {
					convertedNumber += transformTable[ numberString[ i ] ];
				} else {
					convertedNumber += numberString[ i ];
				}
			}

			return integer ? parseFloat( convertedNumber, 10 ) : convertedNumber;
		},

		/**
		 * Grammatical transformations, needed for inflected languages.
		 * Invoked by putting {{grammar:form|word}} in a message.
		 * Override this method for languages that need special grammar rules
		 * applied dynamically.
		 *
		 * @param {string} word
		 * @param {string} form
		 * @return {string}
		 */
		// eslint-disable-next-line no-unused-vars
		convertGrammar: function ( word, form ) {
			return word;
		},

		/**
		 * Provides an alternative text depending on specified gender. Usage
		 * {{gender:[gender|user object]|masculine|feminine|neutral}}. If second
		 * or third parameter are not specified, masculine is used.
		 *
		 * These details may be overriden per language.
		 *
		 * @param {string} gender
		 *      male, female, or anything else for neutral.
		 * @param {Array} forms
		 *      List of gender forms
		 *
		 * @return {string}
		 */
		gender: function ( gender, forms ) {
			if ( !forms || forms.length === 0 ) {
				return '';
			}

			while ( forms.length < 2 ) {
				forms.push( forms[ forms.length - 1 ] );
			}

			if ( gender === 'male' ) {
				return forms[ 0 ];
			}

			if ( gender === 'female' ) {
				return forms[ 1 ];
			}

			return ( forms.length === 3 ) ? forms[ 2 ] : forms[ 0 ];
		},

		/**
		 * Get the digit transform table for the given language
		 * See http://cldr.unicode.org/translation/numbering-systems
		 *
		 * @param {string} language
		 * @return {Array|boolean} List of digits in the passed language or false
		 * representation, or boolean false if there is no information.
		 */
		digitTransformTable: function ( language ) {
			var tables = {
				ar: '٠١٢٣٤٥٦٧٨٩',
				fa: '۰۱۲۳۴۵۶۷۸۹',
				ml: '൦൧൨൩൪൫൬൭൮൯',
				kn: '೦೧೨೩೪೫೬೭೮೯',
				lo: '໐໑໒໓໔໕໖໗໘໙',
				or: '୦୧୨୩୪୫୬୭୮୯',
				kh: '០១២៣៤៥៦៧៨៩',
				pa: '੦੧੨੩੪੫੬੭੮੯',
				gu: '૦૧૨૩૪૫૬૭૮૯',
				hi: '०१२३४५६७८९',
				my: '၀၁၂၃၄၅၆၇၈၉',
				ta: '௦௧௨௩௪௫௬௭௮௯',
				te: '౦౧౨౩౪౫౬౭౮౯',
				th: '๐๑๒๓๔๕๖๗๘๙', // FIXME use iso 639 codes
				bo: '༠༡༢༣༤༥༦༧༨༩' // FIXME use iso 639 codes
			};

			if ( !tables[ language ] ) {
				return false;
			}

			return tables[ language ].split( '' );
		}
	};

	$.extend( $.i18n.languages, {
		'default': language
	} );
}( jQuery ) );

},{}],18:[function(require,module,exports){
/*!
 * jQuery Internationalization library - Message Store
 *
 * Copyright (C) 2012 Santhosh Thottingal
 *
 * jquery.i18n is dual licensed GPLv2 or later and MIT. You don't have to do anything special to
 * choose one license or the other and you don't have to notify anyone which license you are using.
 * You are free to use UniversalLanguageSelector in commercial projects as long as the copyright
 * header is left intact. See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */

( function ( $ ) {
	'use strict';

	var MessageStore = function () {
		this.messages = {};
		this.sources = {};
	};

	function jsonMessageLoader( url ) {
		var deferred = $.Deferred();

		$.getJSON( url )
			.done( deferred.resolve )
			.fail( function ( jqxhr, settings, exception ) {
				$.i18n.log( 'Error in loading messages from ' + url + ' Exception: ' + exception );
				// Ignore 404 exception, because we are handling fallabacks explicitly
				deferred.resolve();
			} );

		return deferred.promise();
	}

	/**
	 * See https://github.com/wikimedia/jquery.i18n/wiki/Specification#wiki-Message_File_Loading
	 */
	MessageStore.prototype = {

		/**
		 * General message loading API This can take a URL string for
		 * the json formatted messages.
		 * <code>load('path/to/all_localizations.json');</code>
		 *
		 * This can also load a localization file for a locale <code>
		 * load( 'path/to/de-messages.json', 'de' );
		 * </code>
		 * A data object containing message key- message translation mappings
		 * can also be passed Eg:
		 * <code>
		 * load( { 'hello' : 'Hello' }, optionalLocale );
		 * </code> If the data argument is
		 * null/undefined/false,
		 * all cached messages for the i18n instance will get reset.
		 *
		 * @param {string|Object} source
		 * @param {string} locale Language tag
		 * @return {jQuery.Promise}
		 */
		load: function ( source, locale ) {
			var key = null,
				deferred = null,
				deferreds = [],
				messageStore = this;

			if ( typeof source === 'string' ) {
				// This is a URL to the messages file.
				$.i18n.log( 'Loading messages from: ' + source );
				deferred = jsonMessageLoader( source )
					.done( function ( localization ) {
						messageStore.set( locale, localization );
					} );

				return deferred.promise();
			}

			if ( locale ) {
				// source is an key-value pair of messages for given locale
				messageStore.set( locale, source );

				return $.Deferred().resolve();
			} else {
				// source is a key-value pair of locales and their source
				for ( key in source ) {
					if ( Object.prototype.hasOwnProperty.call( source, key ) ) {
						locale = key;
						// No {locale} given, assume data is a group of languages,
						// call this function again for each language.
						deferreds.push( messageStore.load( source[ key ], locale ) );
					}
				}
				return $.when.apply( $, deferreds );
			}

		},

		/**
		 * Set messages to the given locale.
		 * If locale exists, add messages to the locale.
		 *
		 * @param {string} locale
		 * @param {Object} messages
		 */
		set: function ( locale, messages ) {
			if ( !this.messages[ locale ] ) {
				this.messages[ locale ] = messages;
			} else {
				this.messages[ locale ] = $.extend( this.messages[ locale ], messages );
			}
		},

		/**
		 *
		 * @param {string} locale
		 * @param {string} messageKey
		 * @return {boolean}
		 */
		get: function ( locale, messageKey ) {
			return this.messages[ locale ] && this.messages[ locale ][ messageKey ];
		}
	};

	$.extend( $.i18n.messageStore, new MessageStore() );
}( jQuery ) );

},{}],19:[function(require,module,exports){
/*!
 * jQuery Internationalization library
 *
 * Copyright (C) 2011-2013 Santhosh Thottingal, Neil Kandalgaonkar
 *
 * jquery.i18n is dual licensed GPLv2 or later and MIT. You don't have to do
 * anything special to choose one license or the other and you don't have to
 * notify anyone which license you are using. You are free to use
 * UniversalLanguageSelector in commercial projects as long as the copyright
 * header is left intact. See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */

( function ( $ ) {
	'use strict';

	var MessageParser = function ( options ) {
		this.options = $.extend( {}, $.i18n.parser.defaults, options );
		this.language = $.i18n.languages[ String.locale ] || $.i18n.languages[ 'default' ];
		this.emitter = $.i18n.parser.emitter;
	};

	MessageParser.prototype = {

		constructor: MessageParser,

		simpleParse: function ( message, parameters ) {
			return message.replace( /\$(\d+)/g, function ( str, match ) {
				var index = parseInt( match, 10 ) - 1;

				return parameters[ index ] !== undefined ? parameters[ index ] : '$' + match;
			} );
		},

		parse: function ( message, replacements ) {
			if ( message.indexOf( '{{' ) < 0 ) {
				return this.simpleParse( message, replacements );
			}

			this.emitter.language = $.i18n.languages[ $.i18n().locale ] ||
				$.i18n.languages[ 'default' ];

			return this.emitter.emit( this.ast( message ), replacements );
		},

		ast: function ( message ) {
			var pipe, colon, backslash, anyCharacter, dollar, digits, regularLiteral,
				regularLiteralWithoutBar, regularLiteralWithoutSpace, escapedOrLiteralWithoutBar,
				escapedOrRegularLiteral, templateContents, templateName, openTemplate,
				closeTemplate, expression, paramExpression, result,
				pos = 0;

			// Try parsers until one works, if none work return null
			function choice( parserSyntax ) {
				return function () {
					var i, result;

					for ( i = 0; i < parserSyntax.length; i++ ) {
						result = parserSyntax[ i ]();

						if ( result !== null ) {
							return result;
						}
					}

					return null;
				};
			}

			// Try several parserSyntax-es in a row.
			// All must succeed; otherwise, return null.
			// This is the only eager one.
			function sequence( parserSyntax ) {
				var i, res,
					originalPos = pos,
					result = [];

				for ( i = 0; i < parserSyntax.length; i++ ) {
					res = parserSyntax[ i ]();

					if ( res === null ) {
						pos = originalPos;

						return null;
					}

					result.push( res );
				}

				return result;
			}

			// Run the same parser over and over until it fails.
			// Must succeed a minimum of n times; otherwise, return null.
			function nOrMore( n, p ) {
				return function () {
					var originalPos = pos,
						result = [],
						parsed = p();

					while ( parsed !== null ) {
						result.push( parsed );
						parsed = p();
					}

					if ( result.length < n ) {
						pos = originalPos;

						return null;
					}

					return result;
				};
			}

			// Helpers -- just make parserSyntax out of simpler JS builtin types

			function makeStringParser( s ) {
				var len = s.length;

				return function () {
					var result = null;

					if ( message.slice( pos, pos + len ) === s ) {
						result = s;
						pos += len;
					}

					return result;
				};
			}

			function makeRegexParser( regex ) {
				return function () {
					var matches = message.slice( pos ).match( regex );

					if ( matches === null ) {
						return null;
					}

					pos += matches[ 0 ].length;

					return matches[ 0 ];
				};
			}

			pipe = makeStringParser( '|' );
			colon = makeStringParser( ':' );
			backslash = makeStringParser( '\\' );
			anyCharacter = makeRegexParser( /^./ );
			dollar = makeStringParser( '$' );
			digits = makeRegexParser( /^\d+/ );
			regularLiteral = makeRegexParser( /^[^{}\[\]$\\]/ );
			regularLiteralWithoutBar = makeRegexParser( /^[^{}\[\]$\\|]/ );
			regularLiteralWithoutSpace = makeRegexParser( /^[^{}\[\]$\s]/ );

			// There is a general pattern:
			// parse a thing;
			// if it worked, apply transform,
			// otherwise return null.
			// But using this as a combinator seems to cause problems
			// when combined with nOrMore().
			// May be some scoping issue.
			function transform( p, fn ) {
				return function () {
					var result = p();

					return result === null ? null : fn( result );
				};
			}

			// Used to define "literals" within template parameters. The pipe
			// character is the parameter delimeter, so by default
			// it is not a literal in the parameter
			function literalWithoutBar() {
				var result = nOrMore( 1, escapedOrLiteralWithoutBar )();

				return result === null ? null : result.join( '' );
			}

			function literal() {
				var result = nOrMore( 1, escapedOrRegularLiteral )();

				return result === null ? null : result.join( '' );
			}

			function escapedLiteral() {
				var result = sequence( [ backslash, anyCharacter ] );

				return result === null ? null : result[ 1 ];
			}

			choice( [ escapedLiteral, regularLiteralWithoutSpace ] );
			escapedOrLiteralWithoutBar = choice( [ escapedLiteral, regularLiteralWithoutBar ] );
			escapedOrRegularLiteral = choice( [ escapedLiteral, regularLiteral ] );

			function replacement() {
				var result = sequence( [ dollar, digits ] );

				if ( result === null ) {
					return null;
				}

				return [ 'REPLACE', parseInt( result[ 1 ], 10 ) - 1 ];
			}

			templateName = transform(
				// see $wgLegalTitleChars
				// not allowing : due to the need to catch "PLURAL:$1"
				makeRegexParser( /^[ !"$&'()*,.\/0-9;=?@A-Z\^_`a-z~\x80-\xFF+\-]+/ ),

				function ( result ) {
					return result.toString();
				}
			);

			function templateParam() {
				var expr,
					result = sequence( [ pipe, nOrMore( 0, paramExpression ) ] );

				if ( result === null ) {
					return null;
				}

				expr = result[ 1 ];

				// use a "CONCAT" operator if there are multiple nodes,
				// otherwise return the first node, raw.
				return expr.length > 1 ? [ 'CONCAT' ].concat( expr ) : expr[ 0 ];
			}

			function templateWithReplacement() {
				var result = sequence( [ templateName, colon, replacement ] );

				return result === null ? null : [ result[ 0 ], result[ 2 ] ];
			}

			function templateWithOutReplacement() {
				var result = sequence( [ templateName, colon, paramExpression ] );

				return result === null ? null : [ result[ 0 ], result[ 2 ] ];
			}

			templateContents = choice( [
				function () {
					var res = sequence( [
						// templates can have placeholders for dynamic
						// replacement eg: {{PLURAL:$1|one car|$1 cars}}
						// or no placeholders eg:
						// {{GRAMMAR:genitive|{{SITENAME}}}
						choice( [ templateWithReplacement, templateWithOutReplacement ] ),
						nOrMore( 0, templateParam )
					] );

					return res === null ? null : res[ 0 ].concat( res[ 1 ] );
				},
				function () {
					var res = sequence( [ templateName, nOrMore( 0, templateParam ) ] );

					if ( res === null ) {
						return null;
					}

					return [ res[ 0 ] ].concat( res[ 1 ] );
				}
			] );

			openTemplate = makeStringParser( '{{' );
			closeTemplate = makeStringParser( '}}' );

			function template() {
				var result = sequence( [ openTemplate, templateContents, closeTemplate ] );

				return result === null ? null : result[ 1 ];
			}

			expression = choice( [ template, replacement, literal ] );
			paramExpression = choice( [ template, replacement, literalWithoutBar ] );

			function start() {
				var result = nOrMore( 0, expression )();

				if ( result === null ) {
					return null;
				}

				return [ 'CONCAT' ].concat( result );
			}

			result = start();

			/*
			 * For success, the pos must have gotten to the end of the input
			 * and returned a non-null.
			 * n.b. This is part of language infrastructure, so we do not throw an internationalizable message.
			 */
			if ( result === null || pos !== message.length ) {
				throw new Error( 'Parse error at position ' + pos.toString() + ' in input: ' + message );
			}

			return result;
		}

	};

	$.extend( $.i18n.parser, new MessageParser() );
}( jQuery ) );

},{}],20:[function(require,module,exports){
/*! js-url - v2.5.2 - 2017-08-30 */!function(){var a=function(){function a(){}function b(a){return decodeURIComponent(a.replace(/\+/g," "))}function c(a,b){var c=a.charAt(0),d=b.split(c);return c===a?d:(a=parseInt(a.substring(1),10),d[a<0?d.length+a:a-1])}function d(a,c){for(var d=a.charAt(0),e=c.split("&"),f=[],g={},h=[],i=a.substring(1),j=0,k=e.length;j<k;j++)if(f=e[j].match(/(.*?)=(.*)/),f||(f=[e[j],e[j],""]),""!==f[1].replace(/\s/g,"")){if(f[2]=b(f[2]||""),i===f[1])return f[2];h=f[1].match(/(.*)\[([0-9]+)\]/),h?(g[h[1]]=g[h[1]]||[],g[h[1]][h[2]]=f[2]):g[f[1]]=f[2]}return d===a?g:g[i]}return function(b,e){var f,g={};if("tld?"===b)return a();if(e=e||window.location.toString(),!b)return e;if(b=b.toString(),f=e.match(/^mailto:([^\/].+)/))g.protocol="mailto",g.email=f[1];else{if((f=e.match(/(.*?)\/#\!(.*)/))&&(e=f[1]+f[2]),(f=e.match(/(.*?)#(.*)/))&&(g.hash=f[2],e=f[1]),g.hash&&b.match(/^#/))return d(b,g.hash);if((f=e.match(/(.*?)\?(.*)/))&&(g.query=f[2],e=f[1]),g.query&&b.match(/^\?/))return d(b,g.query);if((f=e.match(/(.*?)\:?\/\/(.*)/))&&(g.protocol=f[1].toLowerCase(),e=f[2]),(f=e.match(/(.*?)(\/.*)/))&&(g.path=f[2],e=f[1]),g.path=(g.path||"").replace(/^([^\/])/,"/$1"),b.match(/^[\-0-9]+$/)&&(b=b.replace(/^([^\/])/,"/$1")),b.match(/^\//))return c(b,g.path.substring(1));if(f=c("/-1",g.path.substring(1)),f&&(f=f.match(/(.*?)\.(.*)/))&&(g.file=f[0],g.filename=f[1],g.fileext=f[2]),(f=e.match(/(.*)\:([0-9]+)$/))&&(g.port=f[2],e=f[1]),(f=e.match(/(.*?)@(.*)/))&&(g.auth=f[1],e=f[2]),g.auth&&(f=g.auth.match(/(.*)\:(.*)/),g.user=f?f[1]:g.auth,g.pass=f?f[2]:void 0),g.hostname=e.toLowerCase(),"."===b.charAt(0))return c(b,g.hostname);a()&&(f=g.hostname.match(a()),f&&(g.tld=f[3],g.domain=f[2]?f[2]+"."+f[3]:void 0,g.sub=f[1]||void 0)),g.port=g.port||("https"===g.protocol?"443":"80"),g.protocol=g.protocol||("443"===g.port?"https":"http")}return b in g?g[b]:"{}"===b?g:void 0}}();"function"==typeof window.define&&window.define.amd?window.define("js-url",[],function(){return a}):("undefined"!=typeof window.jQuery&&window.jQuery.extend({url:function(a,b){return window.url(a,b)}}),window.url=a)}();

},{}],21:[function(require,module,exports){
module.exports.scoreRegister = function ScoreRegister() {
    var that = {};

    that.setSubLevelScore = function(level, subLevel, subLevelScore) {
        var scores, levelScores;
        if (sessionStorage["scores"] == null) {
            levelScores = [subLevelScore];
            scores = {}
            scores[level] = levelScores;
            sessionStorage.setItem("scores", JSON.stringify(scores));
        } else {
            scores = JSON.parse(sessionStorage.getItem("scores"));
            if (scores[level] === undefined) {
                levelScores = [subLevelScore];
                scores[level] = levelScores;
            } else {
                scores[level][subLevel] = subLevelScore;
            }
            sessionStorage.setItem("scores", JSON.stringify(scores));
        }
    };

    that.setSubLevelLines = function(level, subLevel, subLevelLines) {
        var lines, levelLines;
        if (sessionStorage["lines"] == null) {
            levelLines = [subLevelLines];
            lines = {}
            lines[level] = levelLines;
            sessionStorage.setItem("lines", JSON.stringify(lines));
        } else {
            lines = JSON.parse(sessionStorage.getItem("lines"));
            if (lines[level] === undefined) {
                levelLines = [subLevelLines];
                lines[level] = levelLines;
            } else {
                lines[level][subLevel] = subLevelLines;
            }
            sessionStorage.setItem("lines", JSON.stringify(lines));
        }
    };

    that.getLevelScore = function(level) {
        var scores = JSON.parse(sessionStorage.getItem("scores"));
        var levelScore = 0;
        for (var subLevelScore in scores[level]) {
            levelScore += scores[level][subLevelScore];
        }
        return levelScore;
    };

    that.getLevelLines = function(level) {
        var lines = JSON.parse(sessionStorage.getItem("lines"));
        var levelLines = 0;
        for (var subLevelLines in lines[level]) {
            levelLines += lines[level][subLevelLines];
        }
        return levelLines;
    };

    that.getTotalScore = function() {
        var scores = JSON.parse(sessionStorage.getItem("scores"));
        var scoresArr = Object.values(scores);
        var totalScore = 0;
        for (var levelScores in scoresArr) {
            for (var subLevelScore in scoresArr[levelScores]) {
                totalScore += scoresArr[levelScores][subLevelScore];
            }
        }
        return totalScore;
    };

    that.getTotalLines = function() {
        var lines = JSON.parse(sessionStorage.getItem("lines"));
        var linesArr = Object.values(lines);
        var totalLines = 0;
        for (var levelLines in linesArr) {
            for (var subLevelLines in linesArr[levelLines]) {
                totalLines += linesArr[levelLines][subLevelLines];
            }
        }
        return totalLines;
    };

    return that;

};
},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJtYWluLmpzIiwic3JjL2Jsb2NrcmFpbi5qcXVlcnkubGlicy5qcyIsInNyYy9ibG9ja3JhaW4uanF1ZXJ5LnNyYy5qcyIsInNyYy9ibG9ja3JhaW4uanF1ZXJ5LnRoZW1lcy5qcyIsInNyYy9nYW1lUHJlc2VudGVyLmpzIiwic3JjL2pzb25Mb2FkZXIuanMiLCJzcmMvbGFuZ3VhZ2VQaWNrZXIuanMiLCJzcmMvbGV2ZWxzL2xldmVsLmpzIiwic3JjL2xldmVscy9saW5lc1N1YkxldmVsLmpzIiwic3JjL2xldmVscy9wb2ludHNTdWJMZXZlbC5qcyIsInNyYy9saWIvQ0xEUlBsdXJhbFJ1bGVQYXJzZXIvQ0xEUlBsdXJhbFJ1bGVQYXJzZXIuanMiLCJzcmMvbGliL2hpc3RvcnkvanF1ZXJ5Lmhpc3RvcnkuanMiLCJzcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLmVtaXR0ZXIuYmlkaS5qcyIsInNyYy9saWIvanF1ZXJ5LmkxOG4vanF1ZXJ5LmkxOG4uZW1pdHRlci5qcyIsInNyYy9saWIvanF1ZXJ5LmkxOG4vanF1ZXJ5LmkxOG4uZmFsbGJhY2tzLmpzIiwic3JjL2xpYi9qcXVlcnkuaTE4bi9qcXVlcnkuaTE4bi5qcyIsInNyYy9saWIvanF1ZXJ5LmkxOG4vanF1ZXJ5LmkxOG4ubGFuZ3VhZ2UuanMiLCJzcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLm1lc3NhZ2VzdG9yZS5qcyIsInNyYy9saWIvanF1ZXJ5LmkxOG4vanF1ZXJ5LmkxOG4ucGFyc2VyLmpzIiwic3JjL2xpYi91cmwvdXJsLm1pbi5qcyIsInNyYy9zY29yZVJlZ2lzdGVyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDOUJBO0FBQ0E7O0FDREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3ZyRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMxVkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNsRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNuRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNkQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9sQkE7QUFDQTs7QUNEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN4S0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDMUxBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDN1NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5SEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDclRBO0FBQ0E7O0FDREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsInZhciBjbGRycCA9IHJlcXVpcmUoJy4vc3JjL2xpYi9DTERSUGx1cmFsUnVsZVBhcnNlci9DTERSUGx1cmFsUnVsZVBhcnNlci5qcycpO1xyXG5cclxudmFyIGpxdWVyeWkxOG4gPSByZXF1aXJlKCcuL3NyYy9saWIvanF1ZXJ5LmkxOG4vanF1ZXJ5LmkxOG4uanMnKTtcclxuXHJcbnZhciBtZXNzYWdlc3RvcmUgPSByZXF1aXJlKCcuL3NyYy9saWIvanF1ZXJ5LmkxOG4vanF1ZXJ5LmkxOG4ubWVzc2FnZXN0b3JlLmpzJyk7XHJcblxyXG52YXIgZmFsbGJhY2tzID0gcmVxdWlyZSgnLi9zcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLmZhbGxiYWNrcy5qcycpO1xyXG5cclxudmFyIGxhbmd1YWdlID0gcmVxdWlyZSgnLi9zcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLmxhbmd1YWdlLmpzJyk7XHJcblxyXG52YXIgcGFyc2VyID0gcmVxdWlyZSgnLi9zcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLnBhcnNlci5qcycpO1xyXG5cclxudmFyIGVtaXR0ZXIgPSByZXF1aXJlKCcuL3NyYy9saWIvanF1ZXJ5LmkxOG4vanF1ZXJ5LmkxOG4uZW1pdHRlci5qcycpO1xyXG5cclxudmFyIGVtaXR0ZXJiaWRpID0gcmVxdWlyZSgnLi9zcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLmVtaXR0ZXIuYmlkaS5qcycpO1xyXG5cclxudmFyIGhpc3RvcnkgPSByZXF1aXJlKCcuL3NyYy9saWIvaGlzdG9yeS9qcXVlcnkuaGlzdG9yeS5qcycpO1xyXG5cclxudmFyIHVybCA9IHJlcXVpcmUoJy4vc3JjL2xpYi91cmwvdXJsLm1pbi5qcycpO1xyXG5cclxudmFyIGxhbmd1YWdlUGlja2VyID0gcmVxdWlyZSgnLi9zcmMvbGFuZ3VhZ2VQaWNrZXIuanMnKTtcclxuXHJcbnZhciBqc29uTG9hZGVyID0gcmVxdWlyZSgnLi9zcmMvanNvbkxvYWRlci5qcycpO1xyXG5cclxudmFyIGdhbWVQcmVzZW50ZXIgPSByZXF1aXJlKCcuL3NyYy9nYW1lUHJlc2VudGVyLmpzJyk7XHJcblxyXG52YXIgYmxvY2tyYWlubGlicyA9IHJlcXVpcmUoJy4vc3JjL2Jsb2NrcmFpbi5qcXVlcnkubGlicy5qcycpO1xyXG5cclxudmFyIGJsb2NrcmFpbiA9IHJlcXVpcmUoJy4vc3JjL2Jsb2NrcmFpbi5qcXVlcnkuc3JjLmpzJyk7XHJcblxyXG52YXIgYmxvY2tyYWludGhlbWVzID0gcmVxdWlyZSgnLi9zcmMvYmxvY2tyYWluLmpxdWVyeS50aGVtZXMuanMnKTsiLCIvLyBqUXVlcnkgV2lkZ2V0XHJcbihmdW5jdGlvbihlKXtcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQ/ZGVmaW5lKFtcImpxdWVyeVwiXSxlKTplKGpRdWVyeSl9KShmdW5jdGlvbihlKXt2YXIgdD0wLGk9QXJyYXkucHJvdG90eXBlLnNsaWNlO2UuY2xlYW5EYXRhPWZ1bmN0aW9uKHQpe3JldHVybiBmdW5jdGlvbihpKXt2YXIgcyxuLGE7Zm9yKGE9MDtudWxsIT0obj1pW2FdKTthKyspdHJ5e3M9ZS5fZGF0YShuLFwiZXZlbnRzXCIpLHMmJnMucmVtb3ZlJiZlKG4pLnRyaWdnZXJIYW5kbGVyKFwicmVtb3ZlXCIpfWNhdGNoKG8pe310KGkpfX0oZS5jbGVhbkRhdGEpLGUud2lkZ2V0PWZ1bmN0aW9uKHQsaSxzKXt2YXIgbixhLG8scixoPXt9LGw9dC5zcGxpdChcIi5cIilbMF07cmV0dXJuIHQ9dC5zcGxpdChcIi5cIilbMV0sbj1sK1wiLVwiK3Qsc3x8KHM9aSxpPWUuV2lkZ2V0KSxlLmV4cHJbXCI6XCJdW24udG9Mb3dlckNhc2UoKV09ZnVuY3Rpb24odCl7cmV0dXJuISFlLmRhdGEodCxuKX0sZVtsXT1lW2xdfHx7fSxhPWVbbF1bdF0sbz1lW2xdW3RdPWZ1bmN0aW9uKGUsdCl7cmV0dXJuIHRoaXMuX2NyZWF0ZVdpZGdldD8oYXJndW1lbnRzLmxlbmd0aCYmdGhpcy5fY3JlYXRlV2lkZ2V0KGUsdCksdm9pZCAwKTpuZXcgbyhlLHQpfSxlLmV4dGVuZChvLGEse3ZlcnNpb246cy52ZXJzaW9uLF9wcm90bzplLmV4dGVuZCh7fSxzKSxfY2hpbGRDb25zdHJ1Y3RvcnM6W119KSxyPW5ldyBpLHIub3B0aW9ucz1lLndpZGdldC5leHRlbmQoe30sci5vcHRpb25zKSxlLmVhY2gocyxmdW5jdGlvbih0LHMpe3JldHVybiBlLmlzRnVuY3Rpb24ocyk/KGhbdF09ZnVuY3Rpb24oKXt2YXIgZT1mdW5jdGlvbigpe3JldHVybiBpLnByb3RvdHlwZVt0XS5hcHBseSh0aGlzLGFyZ3VtZW50cyl9LG49ZnVuY3Rpb24oZSl7cmV0dXJuIGkucHJvdG90eXBlW3RdLmFwcGx5KHRoaXMsZSl9O3JldHVybiBmdW5jdGlvbigpe3ZhciB0LGk9dGhpcy5fc3VwZXIsYT10aGlzLl9zdXBlckFwcGx5O3JldHVybiB0aGlzLl9zdXBlcj1lLHRoaXMuX3N1cGVyQXBwbHk9bix0PXMuYXBwbHkodGhpcyxhcmd1bWVudHMpLHRoaXMuX3N1cGVyPWksdGhpcy5fc3VwZXJBcHBseT1hLHR9fSgpLHZvaWQgMCk6KGhbdF09cyx2b2lkIDApfSksby5wcm90b3R5cGU9ZS53aWRnZXQuZXh0ZW5kKHIse3dpZGdldEV2ZW50UHJlZml4OmE/ci53aWRnZXRFdmVudFByZWZpeHx8dDp0fSxoLHtjb25zdHJ1Y3RvcjpvLG5hbWVzcGFjZTpsLHdpZGdldE5hbWU6dCx3aWRnZXRGdWxsTmFtZTpufSksYT8oZS5lYWNoKGEuX2NoaWxkQ29uc3RydWN0b3JzLGZ1bmN0aW9uKHQsaSl7dmFyIHM9aS5wcm90b3R5cGU7ZS53aWRnZXQocy5uYW1lc3BhY2UrXCIuXCIrcy53aWRnZXROYW1lLG8saS5fcHJvdG8pfSksZGVsZXRlIGEuX2NoaWxkQ29uc3RydWN0b3JzKTppLl9jaGlsZENvbnN0cnVjdG9ycy5wdXNoKG8pLGUud2lkZ2V0LmJyaWRnZSh0LG8pLG99LGUud2lkZ2V0LmV4dGVuZD1mdW5jdGlvbih0KXtmb3IodmFyIHMsbixhPWkuY2FsbChhcmd1bWVudHMsMSksbz0wLHI9YS5sZW5ndGg7cj5vO28rKylmb3IocyBpbiBhW29dKW49YVtvXVtzXSxhW29dLmhhc093blByb3BlcnR5KHMpJiZ2b2lkIDAhPT1uJiYodFtzXT1lLmlzUGxhaW5PYmplY3Qobik/ZS5pc1BsYWluT2JqZWN0KHRbc10pP2Uud2lkZ2V0LmV4dGVuZCh7fSx0W3NdLG4pOmUud2lkZ2V0LmV4dGVuZCh7fSxuKTpuKTtyZXR1cm4gdH0sZS53aWRnZXQuYnJpZGdlPWZ1bmN0aW9uKHQscyl7dmFyIG49cy5wcm90b3R5cGUud2lkZ2V0RnVsbE5hbWV8fHQ7ZS5mblt0XT1mdW5jdGlvbihhKXt2YXIgbz1cInN0cmluZ1wiPT10eXBlb2YgYSxyPWkuY2FsbChhcmd1bWVudHMsMSksaD10aGlzO3JldHVybiBhPSFvJiZyLmxlbmd0aD9lLndpZGdldC5leHRlbmQuYXBwbHkobnVsbCxbYV0uY29uY2F0KHIpKTphLG8/dGhpcy5lYWNoKGZ1bmN0aW9uKCl7dmFyIGkscz1lLmRhdGEodGhpcyxuKTtyZXR1cm5cImluc3RhbmNlXCI9PT1hPyhoPXMsITEpOnM/ZS5pc0Z1bmN0aW9uKHNbYV0pJiZcIl9cIiE9PWEuY2hhckF0KDApPyhpPXNbYV0uYXBwbHkocyxyKSxpIT09cyYmdm9pZCAwIT09aT8oaD1pJiZpLmpxdWVyeT9oLnB1c2hTdGFjayhpLmdldCgpKTppLCExKTp2b2lkIDApOmUuZXJyb3IoXCJubyBzdWNoIG1ldGhvZCAnXCIrYStcIicgZm9yIFwiK3QrXCIgd2lkZ2V0IGluc3RhbmNlXCIpOmUuZXJyb3IoXCJjYW5ub3QgY2FsbCBtZXRob2RzIG9uIFwiK3QrXCIgcHJpb3IgdG8gaW5pdGlhbGl6YXRpb247IFwiK1wiYXR0ZW1wdGVkIHRvIGNhbGwgbWV0aG9kICdcIithK1wiJ1wiKX0pOnRoaXMuZWFjaChmdW5jdGlvbigpe3ZhciB0PWUuZGF0YSh0aGlzLG4pO3Q/KHQub3B0aW9uKGF8fHt9KSx0Ll9pbml0JiZ0Ll9pbml0KCkpOmUuZGF0YSh0aGlzLG4sbmV3IHMoYSx0aGlzKSl9KSxofX0sZS5XaWRnZXQ9ZnVuY3Rpb24oKXt9LGUuV2lkZ2V0Ll9jaGlsZENvbnN0cnVjdG9ycz1bXSxlLldpZGdldC5wcm90b3R5cGU9e3dpZGdldE5hbWU6XCJ3aWRnZXRcIix3aWRnZXRFdmVudFByZWZpeDpcIlwiLGRlZmF1bHRFbGVtZW50OlwiPGRpdj5cIixvcHRpb25zOntkaXNhYmxlZDohMSxjcmVhdGU6bnVsbH0sX2NyZWF0ZVdpZGdldDpmdW5jdGlvbihpLHMpe3M9ZShzfHx0aGlzLmRlZmF1bHRFbGVtZW50fHx0aGlzKVswXSx0aGlzLmVsZW1lbnQ9ZShzKSx0aGlzLnV1aWQ9dCsrLHRoaXMuZXZlbnROYW1lc3BhY2U9XCIuXCIrdGhpcy53aWRnZXROYW1lK3RoaXMudXVpZCx0aGlzLmJpbmRpbmdzPWUoKSx0aGlzLmhvdmVyYWJsZT1lKCksdGhpcy5mb2N1c2FibGU9ZSgpLHMhPT10aGlzJiYoZS5kYXRhKHMsdGhpcy53aWRnZXRGdWxsTmFtZSx0aGlzKSx0aGlzLl9vbighMCx0aGlzLmVsZW1lbnQse3JlbW92ZTpmdW5jdGlvbihlKXtlLnRhcmdldD09PXMmJnRoaXMuZGVzdHJveSgpfX0pLHRoaXMuZG9jdW1lbnQ9ZShzLnN0eWxlP3Mub3duZXJEb2N1bWVudDpzLmRvY3VtZW50fHxzKSx0aGlzLndpbmRvdz1lKHRoaXMuZG9jdW1lbnRbMF0uZGVmYXVsdFZpZXd8fHRoaXMuZG9jdW1lbnRbMF0ucGFyZW50V2luZG93KSksdGhpcy5vcHRpb25zPWUud2lkZ2V0LmV4dGVuZCh7fSx0aGlzLm9wdGlvbnMsdGhpcy5fZ2V0Q3JlYXRlT3B0aW9ucygpLGkpLHRoaXMuX2NyZWF0ZSgpLHRoaXMuX3RyaWdnZXIoXCJjcmVhdGVcIixudWxsLHRoaXMuX2dldENyZWF0ZUV2ZW50RGF0YSgpKSx0aGlzLl9pbml0KCl9LF9nZXRDcmVhdGVPcHRpb25zOmUubm9vcCxfZ2V0Q3JlYXRlRXZlbnREYXRhOmUubm9vcCxfY3JlYXRlOmUubm9vcCxfaW5pdDplLm5vb3AsZGVzdHJveTpmdW5jdGlvbigpe3RoaXMuX2Rlc3Ryb3koKSx0aGlzLmVsZW1lbnQudW5iaW5kKHRoaXMuZXZlbnROYW1lc3BhY2UpLnJlbW92ZURhdGEodGhpcy53aWRnZXRGdWxsTmFtZSkucmVtb3ZlRGF0YShlLmNhbWVsQ2FzZSh0aGlzLndpZGdldEZ1bGxOYW1lKSksdGhpcy53aWRnZXQoKS51bmJpbmQodGhpcy5ldmVudE5hbWVzcGFjZSkucmVtb3ZlQXR0cihcImFyaWEtZGlzYWJsZWRcIikucmVtb3ZlQ2xhc3ModGhpcy53aWRnZXRGdWxsTmFtZStcIi1kaXNhYmxlZCBcIitcInVpLXN0YXRlLWRpc2FibGVkXCIpLHRoaXMuYmluZGluZ3MudW5iaW5kKHRoaXMuZXZlbnROYW1lc3BhY2UpLHRoaXMuaG92ZXJhYmxlLnJlbW92ZUNsYXNzKFwidWktc3RhdGUtaG92ZXJcIiksdGhpcy5mb2N1c2FibGUucmVtb3ZlQ2xhc3MoXCJ1aS1zdGF0ZS1mb2N1c1wiKX0sX2Rlc3Ryb3k6ZS5ub29wLHdpZGdldDpmdW5jdGlvbigpe3JldHVybiB0aGlzLmVsZW1lbnR9LG9wdGlvbjpmdW5jdGlvbih0LGkpe3ZhciBzLG4sYSxvPXQ7aWYoMD09PWFyZ3VtZW50cy5sZW5ndGgpcmV0dXJuIGUud2lkZ2V0LmV4dGVuZCh7fSx0aGlzLm9wdGlvbnMpO2lmKFwic3RyaW5nXCI9PXR5cGVvZiB0KWlmKG89e30scz10LnNwbGl0KFwiLlwiKSx0PXMuc2hpZnQoKSxzLmxlbmd0aCl7Zm9yKG49b1t0XT1lLndpZGdldC5leHRlbmQoe30sdGhpcy5vcHRpb25zW3RdKSxhPTA7cy5sZW5ndGgtMT5hO2ErKyluW3NbYV1dPW5bc1thXV18fHt9LG49bltzW2FdXTtpZih0PXMucG9wKCksMT09PWFyZ3VtZW50cy5sZW5ndGgpcmV0dXJuIHZvaWQgMD09PW5bdF0/bnVsbDpuW3RdO25bdF09aX1lbHNle2lmKDE9PT1hcmd1bWVudHMubGVuZ3RoKXJldHVybiB2b2lkIDA9PT10aGlzLm9wdGlvbnNbdF0/bnVsbDp0aGlzLm9wdGlvbnNbdF07b1t0XT1pfXJldHVybiB0aGlzLl9zZXRPcHRpb25zKG8pLHRoaXN9LF9zZXRPcHRpb25zOmZ1bmN0aW9uKGUpe3ZhciB0O2Zvcih0IGluIGUpdGhpcy5fc2V0T3B0aW9uKHQsZVt0XSk7cmV0dXJuIHRoaXN9LF9zZXRPcHRpb246ZnVuY3Rpb24oZSx0KXtyZXR1cm4gdGhpcy5vcHRpb25zW2VdPXQsXCJkaXNhYmxlZFwiPT09ZSYmKHRoaXMud2lkZ2V0KCkudG9nZ2xlQ2xhc3ModGhpcy53aWRnZXRGdWxsTmFtZStcIi1kaXNhYmxlZFwiLCEhdCksdCYmKHRoaXMuaG92ZXJhYmxlLnJlbW92ZUNsYXNzKFwidWktc3RhdGUtaG92ZXJcIiksdGhpcy5mb2N1c2FibGUucmVtb3ZlQ2xhc3MoXCJ1aS1zdGF0ZS1mb2N1c1wiKSkpLHRoaXN9LGVuYWJsZTpmdW5jdGlvbigpe3JldHVybiB0aGlzLl9zZXRPcHRpb25zKHtkaXNhYmxlZDohMX0pfSxkaXNhYmxlOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuX3NldE9wdGlvbnMoe2Rpc2FibGVkOiEwfSl9LF9vbjpmdW5jdGlvbih0LGkscyl7dmFyIG4sYT10aGlzO1wiYm9vbGVhblwiIT10eXBlb2YgdCYmKHM9aSxpPXQsdD0hMSkscz8oaT1uPWUoaSksdGhpcy5iaW5kaW5ncz10aGlzLmJpbmRpbmdzLmFkZChpKSk6KHM9aSxpPXRoaXMuZWxlbWVudCxuPXRoaXMud2lkZ2V0KCkpLGUuZWFjaChzLGZ1bmN0aW9uKHMsbyl7ZnVuY3Rpb24gcigpe3JldHVybiB0fHxhLm9wdGlvbnMuZGlzYWJsZWQhPT0hMCYmIWUodGhpcykuaGFzQ2xhc3MoXCJ1aS1zdGF0ZS1kaXNhYmxlZFwiKT8oXCJzdHJpbmdcIj09dHlwZW9mIG8/YVtvXTpvKS5hcHBseShhLGFyZ3VtZW50cyk6dm9pZCAwfVwic3RyaW5nXCIhPXR5cGVvZiBvJiYoci5ndWlkPW8uZ3VpZD1vLmd1aWR8fHIuZ3VpZHx8ZS5ndWlkKyspO3ZhciBoPXMubWF0Y2goL14oW1xcdzotXSopXFxzKiguKikkLyksbD1oWzFdK2EuZXZlbnROYW1lc3BhY2UsdT1oWzJdO3U/bi5kZWxlZ2F0ZSh1LGwscik6aS5iaW5kKGwscil9KX0sX29mZjpmdW5jdGlvbih0LGkpe2k9KGl8fFwiXCIpLnNwbGl0KFwiIFwiKS5qb2luKHRoaXMuZXZlbnROYW1lc3BhY2UrXCIgXCIpK3RoaXMuZXZlbnROYW1lc3BhY2UsdC51bmJpbmQoaSkudW5kZWxlZ2F0ZShpKSx0aGlzLmJpbmRpbmdzPWUodGhpcy5iaW5kaW5ncy5ub3QodCkuZ2V0KCkpLHRoaXMuZm9jdXNhYmxlPWUodGhpcy5mb2N1c2FibGUubm90KHQpLmdldCgpKSx0aGlzLmhvdmVyYWJsZT1lKHRoaXMuaG92ZXJhYmxlLm5vdCh0KS5nZXQoKSl9LF9kZWxheTpmdW5jdGlvbihlLHQpe2Z1bmN0aW9uIGkoKXtyZXR1cm4oXCJzdHJpbmdcIj09dHlwZW9mIGU/c1tlXTplKS5hcHBseShzLGFyZ3VtZW50cyl9dmFyIHM9dGhpcztyZXR1cm4gc2V0VGltZW91dChpLHR8fDApfSxfaG92ZXJhYmxlOmZ1bmN0aW9uKHQpe3RoaXMuaG92ZXJhYmxlPXRoaXMuaG92ZXJhYmxlLmFkZCh0KSx0aGlzLl9vbih0LHttb3VzZWVudGVyOmZ1bmN0aW9uKHQpe2UodC5jdXJyZW50VGFyZ2V0KS5hZGRDbGFzcyhcInVpLXN0YXRlLWhvdmVyXCIpfSxtb3VzZWxlYXZlOmZ1bmN0aW9uKHQpe2UodC5jdXJyZW50VGFyZ2V0KS5yZW1vdmVDbGFzcyhcInVpLXN0YXRlLWhvdmVyXCIpfX0pfSxfZm9jdXNhYmxlOmZ1bmN0aW9uKHQpe3RoaXMuZm9jdXNhYmxlPXRoaXMuZm9jdXNhYmxlLmFkZCh0KSx0aGlzLl9vbih0LHtmb2N1c2luOmZ1bmN0aW9uKHQpe2UodC5jdXJyZW50VGFyZ2V0KS5hZGRDbGFzcyhcInVpLXN0YXRlLWZvY3VzXCIpfSxmb2N1c291dDpmdW5jdGlvbih0KXtlKHQuY3VycmVudFRhcmdldCkucmVtb3ZlQ2xhc3MoXCJ1aS1zdGF0ZS1mb2N1c1wiKX19KX0sX3RyaWdnZXI6ZnVuY3Rpb24odCxpLHMpe3ZhciBuLGEsbz10aGlzLm9wdGlvbnNbdF07aWYocz1zfHx7fSxpPWUuRXZlbnQoaSksaS50eXBlPSh0PT09dGhpcy53aWRnZXRFdmVudFByZWZpeD90OnRoaXMud2lkZ2V0RXZlbnRQcmVmaXgrdCkudG9Mb3dlckNhc2UoKSxpLnRhcmdldD10aGlzLmVsZW1lbnRbMF0sYT1pLm9yaWdpbmFsRXZlbnQpZm9yKG4gaW4gYSluIGluIGl8fChpW25dPWFbbl0pO3JldHVybiB0aGlzLmVsZW1lbnQudHJpZ2dlcihpLHMpLCEoZS5pc0Z1bmN0aW9uKG8pJiZvLmFwcGx5KHRoaXMuZWxlbWVudFswXSxbaV0uY29uY2F0KHMpKT09PSExfHxpLmlzRGVmYXVsdFByZXZlbnRlZCgpKX19LGUuZWFjaCh7c2hvdzpcImZhZGVJblwiLGhpZGU6XCJmYWRlT3V0XCJ9LGZ1bmN0aW9uKHQsaSl7ZS5XaWRnZXQucHJvdG90eXBlW1wiX1wiK3RdPWZ1bmN0aW9uKHMsbixhKXtcInN0cmluZ1wiPT10eXBlb2YgbiYmKG49e2VmZmVjdDpufSk7dmFyIG8scj1uP249PT0hMHx8XCJudW1iZXJcIj09dHlwZW9mIG4/aTpuLmVmZmVjdHx8aTp0O249bnx8e30sXCJudW1iZXJcIj09dHlwZW9mIG4mJihuPXtkdXJhdGlvbjpufSksbz0hZS5pc0VtcHR5T2JqZWN0KG4pLG4uY29tcGxldGU9YSxuLmRlbGF5JiZzLmRlbGF5KG4uZGVsYXkpLG8mJmUuZWZmZWN0cyYmZS5lZmZlY3RzLmVmZmVjdFtyXT9zW3RdKG4pOnIhPT10JiZzW3JdP3Nbcl0obi5kdXJhdGlvbixuLmVhc2luZyxhKTpzLnF1ZXVlKGZ1bmN0aW9uKGkpe2UodGhpcylbdF0oKSxhJiZhLmNhbGwoc1swXSksaSgpfSl9fSksZS53aWRnZXR9KTsiLCIoKGZ1bmN0aW9uKCQpIHtcclxuXHJcbiAgICBcInVzZSBzdHJpY3RcIjtcclxuXHJcbiAgICAkLndpZGdldCgnYWVyb2xhYi5ibG9ja3JhaW4nLCB7XHJcblxyXG4gICAgICAgIG9wdGlvbnM6IHtcclxuICAgICAgICAgICAgYXV0b3BsYXk6IGZhbHNlLCAvLyBMZXQgYSBib3QgcGxheSB0aGUgZ2FtZVxyXG4gICAgICAgICAgICBhdXRvcGxheVJlc3RhcnQ6IHRydWUsIC8vIFJlc3RhcnQgdGhlIGdhbWUgYXV0b21hdGljYWxseSBvbmNlIGEgYm90IGxvc2VzXHJcbiAgICAgICAgICAgIHNob3dGaWVsZE9uU3RhcnQ6IHRydWUsIC8vIFNob3cgYSBidW5jaCBvZiByYW5kb20gYmxvY2tzIG9uIHRoZSBzdGFydCBzY3JlZW4gKGl0IGxvb2tzIG5pY2UpXHJcbiAgICAgICAgICAgIHRoZW1lOiBudWxsLCAvLyBUaGUgdGhlbWUgbmFtZSBvciBhIHRoZW1lIG9iamVjdFxyXG4gICAgICAgICAgICBibG9ja1dpZHRoOiAxMCwgLy8gSG93IG1hbnkgYmxvY2tzIHdpZGUgdGhlIGZpZWxkIGlzIChUaGUgc3RhbmRhcmQgaXMgMTAgYmxvY2tzKVxyXG4gICAgICAgICAgICBhdXRvQmxvY2tXaWR0aDogZmFsc2UsIC8vIFRoZSBibG9ja1dpZHRoIGlzIGRpbmFtaWNhbGx5IGNhbGN1bGF0ZWQgYmFzZWQgb24gdGhlIGF1dG9CbG9ja1NpemUuIERpc2FibGVkIGJsb2NrV2lkdGguIFVzZWZ1bCBmb3IgcmVzcG9uc2l2ZSBiYWNrZ3JvdW5kc1xyXG4gICAgICAgICAgICBhdXRvQmxvY2tTaXplOiAyNCwgLy8gVGhlIG1heCBzaXplIG9mIGEgYmxvY2sgZm9yIGF1dG93aWR0aCBtb2RlXHJcbiAgICAgICAgICAgIGRpZmZpY3VsdHk6ICdub3JtYWwnLCAvLyBEaWZmaWN1bHR5IChub3JtYWx8bmljZXxldmlsKS5cclxuICAgICAgICAgICAgc3BlZWQ6IDIwLCAvLyBUaGUgc3BlZWQgb2YgdGhlIGdhbWUuIFRoZSBoaWdoZXIsIHRoZSBmYXN0ZXIgdGhlIHBpZWNlcyBnby5cclxuICAgICAgICAgICAgYXNkd0tleXM6IHRydWUsIC8vIEVuYWJsZSBBU0RXIGtleXNcclxuICAgICAgICAgICAgcXVpY2tEcm9wOiB0cnVlLCAvLyBFbmFibGUgcXVpY2sgZHJvcCAoZG91YmxlLXByZXNzaW5nIGRyb3AgYnV0dG9uIGZvciBpbnN0YW50IGRyb3ApXHJcbiAgICAgICAgICAgIGRvdWJsZVByZXNzVGltZTogNTAwLCAvLyBTZXRzIHRoZSB0aW1lIHdpbmRvdyBmb3IgZG91YmxlLXByZXNzaW5nIHRoZSBkcm9wXHJcbiAgICAgICAgICAgIG9ic3RhY2xlczogMCwgLy8gSW5kaWNhdGVzIGlmIG9wdGlvbmFsIG9ic3RhY2xlcyBhcmUgYWN0aXZhdGVkIG9yIG5vdFxyXG4gICAgICAgICAgICBudW1PYnN0YWNsZXM6IDAsXHJcbiAgICAgICAgICAgIHBhdXNlQnV0dG9uOiB0cnVlLFxyXG4gICAgICAgICAgICBzaGFwZVR5cGVQcm9iczogWzAuMzMsIDAuMzMsIDAuMzNdLCAvLyBQcm9iYWJpbGl0aWVzIG9mIGFuZCBlYXN5LCBub3JtYWwgb3IgaGFyZCB0eXBlIG9mIHNoYXBlIGdlbmVyYXRlZFxyXG5cclxuICAgICAgICAgICAgLy8gQ29weVxyXG4gICAgICAgICAgICBwbGF5VGV4dDogJ0xldFxcJ3MgcGxheSBzb21lIFRldHJpcycsXHJcbiAgICAgICAgICAgIHBsYXlCdXR0b25UZXh0OiAnUGxheScsXHJcbiAgICAgICAgICAgIGdhbWVPdmVyVGV4dDogJ0dhbWUgT3ZlcicsXHJcbiAgICAgICAgICAgIHJlc3RhcnRCdXR0b25UZXh0OiAnUGxheSBBZ2FpbicsXHJcbiAgICAgICAgICAgIHNjb3JlVGV4dDogJ1Njb3JlJyxcclxuXHJcbiAgICAgICAgICAgIC8vIEJhc2ljIENhbGxiYWNrc1xyXG4gICAgICAgICAgICBvblN0YXJ0OiBmdW5jdGlvbigpIHt9LFxyXG4gICAgICAgICAgICBvblJlc3RhcnQ6IGZ1bmN0aW9uKCkge30sXHJcbiAgICAgICAgICAgIG9uR2FtZU92ZXI6IGZ1bmN0aW9uKHNjb3JlKSB7fSxcclxuXHJcbiAgICAgICAgICAgIC8vIFdoZW4gYSBibG9jayBpcyBwbGFjZWRcclxuICAgICAgICAgICAgb25QbGFjZWQ6IGZ1bmN0aW9uKCkge30sXHJcbiAgICAgICAgICAgIC8vIFdoZW4gYSBsaW5lIGlzIG1hZGUuIFJldHVybnMgdGhlIG51bWJlciBvZiBsaW5lcywgc2NvcmUgYXNzaWduZWQgYW5kIHRvdGFsIHNjb3JlXHJcbiAgICAgICAgICAgIG9uTGluZTogZnVuY3Rpb24obGluZXMsIHNjb3JlSW5jcmVtZW50LCBzY29yZSkge30sXHJcbiAgICAgICAgICAgIC8vIFdoZW4gbmV4dCBzaGFwZSBpcyBnZW5lcmF0ZWQuIFJldHVybnMgdGhlIHR5cGUgb2YgYmxvY2tcclxuICAgICAgICAgICAgb25OZXh0OiBmdW5jdGlvbihuZXh0KSB7fVxyXG4gICAgICAgIH0sXHJcblxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBTdGFydC9SZXN0YXJ0IEdhbWVcclxuICAgICAgICAgKi9cclxuICAgICAgICBzdGFydDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2RvU3RhcnQoKTtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25zLm9uU3RhcnQuY2FsbCh0aGlzLmVsZW1lbnQpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHJlc3RhcnQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICB0aGlzLl9kb1N0YXJ0KCk7XHJcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5vblJlc3RhcnQuY2FsbCh0aGlzLmVsZW1lbnQpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHBhdXNlUmVzdW1lOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgdmFyIHBhdXNlQnV0dG9uID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwYXVzZUJ1dHRvblwiKTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuaXNHYW1lUGF1c2VkKCkpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZ2FtZSByZXN1bWVkIVwiKTtcclxuICAgICAgICAgICAgICAgIHBhdXNlQnV0dG9uLnN0eWxlLmJvcmRlclN0eWxlID0gXCJkb3VibGVcIjtcclxuICAgICAgICAgICAgICAgIHBhdXNlQnV0dG9uLnN0eWxlLmJvcmRlcldpZHRoID0gXCIwcHggMCAwcHggMTVweFwiO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXN1bWUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZ2FtZSBwYXVzZWQhXCIpO1xyXG4gICAgICAgICAgICAgICAgcGF1c2VCdXR0b24uc3R5bGUuYm9yZGVyU3R5bGUgPSBcInNvbGlkXCI7XHJcbiAgICAgICAgICAgICAgICBwYXVzZUJ1dHRvbi5zdHlsZS5ib3JkZXJXaWR0aCA9IFwiOHB4IDAgOHB4IDE1cHhcIjtcclxuICAgICAgICAgICAgICAgIHRoaXMucGF1c2UoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGdhbWVvdmVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgdGhpcy5zaG93R2FtZU92ZXJNZXNzYWdlKCk7XHJcbiAgICAgICAgICAgIHRoaXMuX2JvYXJkLmdhbWVvdmVyID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25zLm9uR2FtZU92ZXIuY2FsbCh0aGlzLmVsZW1lbnQsIHRoaXMuX2ZpbGxlZC5zY29yZSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgbGV2ZWxPdmVyOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgdGhpcy5fYm9hcmQubGV2ZWxPdmVyID0gdHJ1ZTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBfZG9TdGFydDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2ZpbGxlZC5jbGVhckFsbCgpO1xyXG4gICAgICAgICAgICB0aGlzLl9maWxsZWQuX3Jlc2V0U2NvcmUoKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMub2JzdGFjbGVzID09IDEpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiT2JzdGFjbGVzIGFjdGl2YXRlZFwiKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2JvYXJkLmNyZWF0ZU9ic3RhY2xlcygpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnBhdXNlQnV0dG9uKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl8kcGF1c2Uuc2hvdygpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fJHBhdXNlLmhpZGUoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5fYm9hcmQuY3VyID0gdGhpcy5fYm9hcmQubmV4dFNoYXBlKCk7XHJcbiAgICAgICAgICAgIHRoaXMuX2JvYXJkLnN0YXJ0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLl9ib2FyZC5nYW1lb3ZlciA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLl9ib2FyZC5sZXZlbE92ZXIgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5fYm9hcmQuZHJvcERlbGF5ID0gNTtcclxuICAgICAgICAgICAgdGhpcy5fYm9hcmQucmVuZGVyKHRydWUpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5fYm9hcmQuYW5pbWF0ZSgpO1xyXG5cclxuXHJcbiAgICAgICAgICAgIHRoaXMuXyRzdGFydC5mYWRlT3V0KDE1MCk7XHJcbiAgICAgICAgICAgIHRoaXMuXyRnYW1lb3Zlci5mYWRlT3V0KDE1MCk7XHJcbiAgICAgICAgICAgIHRoaXMuXyRzY29yZS5mYWRlSW4oMTUwKTtcclxuICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgcGF1c2U6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICB0aGlzLl9ib2FyZC5wYXVzZWQgPSB0cnVlO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHJlc3VtZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2JvYXJkLnBhdXNlZCA9IGZhbHNlO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGF1dG9wbGF5OiBmdW5jdGlvbihlbmFibGUpIHtcclxuICAgICAgICAgICAgaWYgKHR5cGVvZiBlbmFibGUgIT09ICdib29sZWFuJykge1xyXG4gICAgICAgICAgICAgICAgZW5hYmxlID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gT24gYXV0b3BsYXksIHN0YXJ0IHRoZSBnYW1lIHJpZ2h0IGF3YXlcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25zLmF1dG9wbGF5ID0gZW5hYmxlO1xyXG4gICAgICAgICAgICBpZiAoZW5hYmxlICYmICF0aGlzLl9ib2FyZC5zdGFydGVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9kb1N0YXJ0KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5fc2V0dXBDb250cm9scyghZW5hYmxlKTtcclxuICAgICAgICAgICAgdGhpcy5fc2V0dXBUb3VjaENvbnRyb2xzKCFlbmFibGUpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGNvbnRyb2xzOiBmdW5jdGlvbihlbmFibGUpIHtcclxuICAgICAgICAgICAgaWYgKHR5cGVvZiBlbmFibGUgIT09ICdib29sZWFuJykge1xyXG4gICAgICAgICAgICAgICAgZW5hYmxlID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLl9zZXR1cENvbnRyb2xzKGVuYWJsZSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgdG91Y2hDb250cm9sczogZnVuY3Rpb24oZW5hYmxlKSB7XHJcbiAgICAgICAgICAgIGlmICh0eXBlb2YgZW5hYmxlICE9PSAnYm9vbGVhbicpIHtcclxuICAgICAgICAgICAgICAgIGVuYWJsZSA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMuaXNNb2JpbGVEZXZpY2UoKSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fc2V0dXBUb3VjaENvbnRyb2xzKGVuYWJsZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzY29yZTogZnVuY3Rpb24obmV3U2NvcmUpIHtcclxuICAgICAgICAgICAgaWYgKHR5cGVvZiBuZXdTY29yZSAhPT0gJ3VuZGVmaW5lZCcgJiYgcGFyc2VJbnQobmV3U2NvcmUpID49IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2ZpbGxlZC5zY29yZSA9IHBhcnNlSW50KG5ld1Njb3JlKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuXyRzY29yZVRleHQudGV4dCh0aGlzLl9maWxsZWRfc2NvcmUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9maWxsZWQuc2NvcmU7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZnJlZXNxdWFyZXM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fZmlsbGVkLmdldEZyZWVTcGFjZXMoKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzaG93U3RhcnRNZXNzYWdlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgdGhpcy5fJHN0YXJ0LnNob3coKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzaG93R2FtZU92ZXJNZXNzYWdlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgdGhpcy5fJGdhbWVvdmVyLnNob3coKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBVcGRhdGUgdGhlIHNpemVzIG9mIHRoZSByZW5kZXJlciAodGhpcyBtYWtlcyB0aGUgZ2FtZSByZXNwb25zaXZlKVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIHVwZGF0ZVNpemVzOiBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX1BJWEVMX1dJRFRIID0gdGhpcy5lbGVtZW50LmlubmVyV2lkdGgoKTtcclxuICAgICAgICAgICAgdGhpcy5fUElYRUxfSEVJR0hUID0gdGhpcy5lbGVtZW50LmlubmVySGVpZ2h0KCk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9CTE9DS19XSURUSCA9IHRoaXMub3B0aW9ucy5ibG9ja1dpZHRoO1xyXG4gICAgICAgICAgICB0aGlzLl9CTE9DS19IRUlHSFQgPSBNYXRoLmZsb29yKHRoaXMuZWxlbWVudC5pbm5lckhlaWdodCgpIC8gdGhpcy5lbGVtZW50LmlubmVyV2lkdGgoKSAqIHRoaXMuX0JMT0NLX1dJRFRIKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX2Jsb2NrX3NpemUgPSBNYXRoLmZsb29yKHRoaXMuX1BJWEVMX1dJRFRIIC8gdGhpcy5fQkxPQ0tfV0lEVEgpO1xyXG4gICAgICAgICAgICB0aGlzLl9ib3JkZXJfd2lkdGggPSAyO1xyXG5cclxuICAgICAgICAgICAgLy8gUmVjYWxjdWxhdGUgdGhlIHBpeGVsIHdpZHRoIGFuZCBoZWlnaHQgc28gdGhlIGNhbnZhcyBhbHdheXMgaGFzIHRoZSBiZXN0IHBvc3NpYmxlIHNpemVcclxuICAgICAgICAgICAgdGhpcy5fUElYRUxfV0lEVEggPSB0aGlzLl9ibG9ja19zaXplICogdGhpcy5fQkxPQ0tfV0lEVEg7XHJcbiAgICAgICAgICAgIHRoaXMuX1BJWEVMX0hFSUdIVCA9IHRoaXMuX2Jsb2NrX3NpemUgKiB0aGlzLl9CTE9DS19IRUlHSFQ7XHJcblxyXG4gICAgICAgICAgICB0aGlzLl8kY2FudmFzLmF0dHIoJ3dpZHRoJywgdGhpcy5fUElYRUxfV0lEVEgpXHJcbiAgICAgICAgICAgICAgICAuYXR0cignaGVpZ2h0JywgdGhpcy5fUElYRUxfSEVJR0hUKTtcclxuICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgdGhlbWU6IGZ1bmN0aW9uKG5ld1RoZW1lKSB7XHJcblxyXG4gICAgICAgICAgICBpZiAodHlwZW9mIG5ld1RoZW1lID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucy50aGVtZSB8fCB0aGlzLl90aGVtZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gU2V0dXAgdGhlIHRoZW1lIHByb3Blcmx5XHJcbiAgICAgICAgICAgIGlmICh0eXBlb2YgbmV3VGhlbWUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMudGhlbWUgPSBuZXdUaGVtZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RoZW1lID0gJC5leHRlbmQodHJ1ZSwge30sIEJsb2NrcmFpblRoZW1lc1tuZXdUaGVtZV0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vcHRpb25zLnRoZW1lID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RoZW1lID0gbmV3VGhlbWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0eXBlb2YgdGhpcy5fdGhlbWUgPT09ICd1bmRlZmluZWQnIHx8IHRoaXMuX3RoZW1lID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90aGVtZSA9ICQuZXh0ZW5kKHRydWUsIHt9LCBCbG9ja3JhaW5UaGVtZXNbJ3JldHJvJ10pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vcHRpb25zLnRoZW1lID0gJ3JldHJvJztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGlzTmFOKHBhcnNlSW50KHRoaXMuX3RoZW1lLnN0cm9rZVdpZHRoKSkgfHwgdHlwZW9mIHBhcnNlSW50KHRoaXMuX3RoZW1lLnN0cm9rZVdpZHRoKSAhPT0gJ251bWJlcicpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RoZW1lLnN0cm9rZVdpZHRoID0gMjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gTG9hZCB0aGUgaW1hZ2UgYXNzZXRzXHJcbiAgICAgICAgICAgIHRoaXMuX3ByZWxvYWRUaGVtZUFzc2V0cygpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuX2JvYXJkICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHRoaXMuX3RoZW1lLmJhY2tncm91bmQgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fJGNhbnZhcy5jc3MoJ2JhY2tncm91bmQtY29sb3InLCB0aGlzLl90aGVtZS5iYWNrZ3JvdW5kKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuX2JvYXJkLnJlbmRlcigpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc3BlZWQ6IGZ1bmN0aW9uKG5ld1NwZWVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5zcGVlZCA9IG5ld1NwZWVkO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGRpZmZpY3VsdHk6IGZ1bmN0aW9uKG5ld0RpZmZpY3VsdHkpIHtcclxuICAgICAgICAgICAgdmFyIGdhbWUgPSB0aGlzO1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMuZGlmZmljdWx0eSA9IG5ld0RpZmZpY3VsdHk7XHJcbiAgICAgICAgICAgIHRoaXMuX2luZm8uc2V0TW9kZShuZXdEaWZmaWN1bHR5KTtcclxuXHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgaXNHYW1lUGF1c2VkOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2JvYXJkLnBhdXNlZDtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBvYnN0YWNsZXM6IGZ1bmN0aW9uKG9ic3RhY2xlcykge1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMub2JzdGFjbGVzID0gb2JzdGFjbGVzO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIG51bU9ic3RhY2xlczogZnVuY3Rpb24obnVtT2JzdGFjbGVzKSB7XHJcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5udW1PYnN0YWNsZXMgPSBudW1PYnN0YWNsZXM7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgcGF1c2VCdXR0b246IGZ1bmN0aW9uKHNob3dQYXVzZUJ1dHRvbikge1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMucGF1c2VCdXR0b24gPSBzaG93UGF1c2VCdXR0b247XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc2V0U2hhcGVUeXBlUHJvYmFiaWxpdGVzOiBmdW5jdGlvbihzaGFwZVR5cGVQcm9iYWJpbGl0aWVzKSB7XHJcbiAgICAgICAgICAgIGlmICh0eXBlb2YgQXJyYXkuaXNBcnJheShzaGFwZVR5cGVQcm9iYWJpbGl0aWVzKSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHNoYXBlVHlwZVByb2JhYmlsaXRpZXMubGVuZ3RoID09IHRoaXMub3B0aW9ucy5zaGFwZVR5cGVQcm9icy5sZW5ndGgpXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vcHRpb25zLnNoYXBlVHlwZVByb2JzID0gc2hhcGVUeXBlUHJvYmFiaWxpdGllcztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcblxyXG5cclxuICAgICAgICAvLyBUaGVtZVxyXG4gICAgICAgIF90aGVtZToge1xyXG5cclxuICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgLy8gVUkgRWxlbWVudHNcclxuICAgICAgICBfJGdhbWU6IG51bGwsXHJcbiAgICAgICAgXyRjYW52YXM6IG51bGwsXHJcbiAgICAgICAgXyRnYW1laG9sZGVyOiBudWxsLFxyXG4gICAgICAgIF8kc3RhcnQ6IG51bGwsXHJcbiAgICAgICAgXyRnYW1lb3ZlcjogbnVsbCxcclxuICAgICAgICBfJHNjb3JlOiBudWxsLFxyXG4gICAgICAgIF8kc2NvcmVUZXh0OiBudWxsLFxyXG5cclxuXHJcbiAgICAgICAgLy8gQ2FudmFzXHJcbiAgICAgICAgX2NhbnZhczogbnVsbCxcclxuICAgICAgICBfY3R4OiBudWxsLFxyXG5cclxuICAgICAgICAvL3NoYXBlRmFjdG9yaWVzXHJcbiAgICAgICAgX3NoYXBlRmFjdG9yaWVzOiBudWxsLFxyXG5cclxuXHJcbiAgICAgICAgLy8gSW5pdGlhbGl6YXRpb25cclxuICAgICAgICBfY3JlYXRlOiBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgICAgIHZhciBnYW1lID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgIHRoaXMudGhlbWUodGhpcy5vcHRpb25zLnRoZW1lKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX2NyZWF0ZUhvbGRlcigpO1xyXG4gICAgICAgICAgICB0aGlzLl9jcmVhdGVVSSgpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5fcmVmcmVzaEJsb2NrU2l6ZXMoKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlU2l6ZXMoKTtcclxuXHJcbiAgICAgICAgICAgICQod2luZG93KS5yZXNpemUoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAvL2dhbWUudXBkYXRlU2l6ZXMoKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9TZXR1cFNoYXBlRmFjdG9yaWVzKCk7XHJcbiAgICAgICAgICAgIHRoaXMuX1NldHVwRmlsbGVkKCk7XHJcbiAgICAgICAgICAgIHRoaXMuX1NldHVwSW5mbygpO1xyXG4gICAgICAgICAgICB0aGlzLl9TZXR1cEJvYXJkKCk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9pbmZvLmluaXQoKTtcclxuICAgICAgICAgICAgdGhpcy5fYm9hcmQuaW5pdCgpO1xyXG5cclxuICAgICAgICAgICAgdmFyIHJlbmRlckxvb3AgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShyZW5kZXJMb29wKTtcclxuICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLnJlbmRlcigpO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICByZW5kZXJMb29wKCk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmF1dG9wbGF5KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmF1dG9wbGF5KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fc2V0dXBUb3VjaENvbnRyb2xzKGZhbHNlKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3NldHVwQ29udHJvbHModHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9zZXR1cFRvdWNoQ29udHJvbHMoZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIF9jaGVja0NvbGxpc2lvbnM6IGZ1bmN0aW9uKHgsIHksIGJsb2NrcywgY2hlY2tEb3duT25seSkge1xyXG4gICAgICAgICAgICAvLyB4ICYgeSBzaG91bGQgYmUgYXNwaXJhdGlvbmFsIHZhbHVlc1xyXG4gICAgICAgICAgICB2YXIgaSA9IDAsXHJcbiAgICAgICAgICAgICAgICBsZW4gPSBibG9ja3MubGVuZ3RoLFxyXG4gICAgICAgICAgICAgICAgYSwgYjtcclxuICAgICAgICAgICAgZm9yICg7IGkgPCBsZW47IGkgKz0gMikge1xyXG4gICAgICAgICAgICAgICAgYSA9IHggKyBibG9ja3NbaV07XHJcbiAgICAgICAgICAgICAgICBiID0geSArIGJsb2Nrc1tpICsgMV07XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGIgPj0gdGhpcy5fQkxPQ0tfSEVJR0hUIHx8IHRoaXMuX2ZpbGxlZC5jaGVjayhhLCBiKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICghY2hlY2tEb3duT25seSAmJiBhIDwgMCB8fCBhID49IHRoaXMuX0JMT0NLX1dJRFRIKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIF9zZWxlY3RSYW5kU2hhcGVGYWN0b3J5OiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgdmFyIHJhbmRQcm9iYWJpbGl0eSA9IHRoaXMuX2dlblJhbmQoMCwgMSwgMik7XHJcbiAgICAgICAgICAgIHZhciBzaGFwZUZhY3RvcnksIGFjY3VtdWxhdGVkUHJvYmFiaWxpdHkgPSAwO1xyXG5cclxuICAgICAgICAgICAgZm9yICh2YXIgcHJvYiBpbiB0aGlzLm9wdGlvbnMuc2hhcGVUeXBlUHJvYnMpIHtcclxuICAgICAgICAgICAgICAgIGFjY3VtdWxhdGVkUHJvYmFiaWxpdHkgKz0gdGhpcy5vcHRpb25zLnNoYXBlVHlwZVByb2JzW3Byb2JdO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJhbmRQcm9iYWJpbGl0eSA8PSBhY2N1bXVsYXRlZFByb2JhYmlsaXR5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2hhcGVGYWN0b3J5ID0gdGhpcy5fc2hhcGVGYWN0b3JpZXNbcHJvYl07XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiBzaGFwZUZhY3Rvcnk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgX2dlblJhbmQ6IGZ1bmN0aW9uKG1pbiwgbWF4LCBkZWNpbWFsUGxhY2VzKSB7XHJcbiAgICAgICAgICAgIHZhciByYW5kID0gTWF0aC5yYW5kb20oKSAqIChtYXggLSBtaW4pICsgbWluO1xyXG4gICAgICAgICAgICB2YXIgcG93ZXIgPSBNYXRoLnBvdygxMCwgZGVjaW1hbFBsYWNlcyk7XHJcbiAgICAgICAgICAgIHJldHVybiBNYXRoLmZsb29yKHJhbmQgKiBwb3dlcikgLyBwb3dlcjtcclxuICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgX2JvYXJkOiBudWxsLFxyXG4gICAgICAgIF9pbmZvOiBudWxsLFxyXG4gICAgICAgIF9maWxsZWQ6IG51bGwsXHJcblxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBEcmF3cyB0aGUgYmFja2dyb3VuZFxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIF9kcmF3QmFja2dyb3VuZDogZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICAgICBpZiAodHlwZW9mIHRoaXMuX3RoZW1lLmJhY2tncm91bmQgIT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLl90aGVtZS5iYWNrZ3JvdW5kR3JpZCBpbnN0YW5jZW9mIEltYWdlKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gTm90IGxvYWRlZFxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuX3RoZW1lLmJhY2tncm91bmRHcmlkLndpZHRoID09PSAwIHx8IHRoaXMuX3RoZW1lLmJhY2tncm91bmRHcmlkLmhlaWdodCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLl9jdHguZ2xvYmFsQWxwaGEgPSAxLjA7XHJcblxyXG4gICAgICAgICAgICAgICAgZm9yICh2YXIgeCA9IDA7IHggPCB0aGlzLl9CTE9DS19XSURUSDsgeCsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgeSA9IDA7IHkgPCB0aGlzLl9CTE9DS19IRUlHSFQ7IHkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgY3ggPSB4ICogdGhpcy5fYmxvY2tfc2l6ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGN5ID0geSAqIHRoaXMuX2Jsb2NrX3NpemU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9jdHguZHJhd0ltYWdlKHRoaXMuX3RoZW1lLmJhY2tncm91bmRHcmlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgMCwgMCwgdGhpcy5fdGhlbWUuYmFja2dyb3VuZEdyaWQud2lkdGgsIHRoaXMuX3RoZW1lLmJhY2tncm91bmRHcmlkLmhlaWdodCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGN4LCBjeSwgdGhpcy5fYmxvY2tfc2l6ZSwgdGhpcy5fYmxvY2tfc2l6ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgfSBlbHNlIGlmICh0eXBlb2YgdGhpcy5fdGhlbWUuYmFja2dyb3VuZEdyaWQgPT09ICdzdHJpbmcnKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgdmFyIGJvcmRlcldpZHRoID0gdGhpcy5fdGhlbWUuc3Ryb2tlV2lkdGg7XHJcbiAgICAgICAgICAgICAgICB2YXIgYm9yZGVyRGlzdGFuY2UgPSBNYXRoLnJvdW5kKHRoaXMuX2Jsb2NrX3NpemUgKiAwLjIzKTtcclxuICAgICAgICAgICAgICAgIHZhciBzcXVhcmVEaXN0YW5jZSA9IE1hdGgucm91bmQodGhpcy5fYmxvY2tfc2l6ZSAqIDAuMzApO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuX2N0eC5nbG9iYWxBbHBoYSA9IDEuMDtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2N0eC5maWxsU3R5bGUgPSB0aGlzLl90aGVtZS5iYWNrZ3JvdW5kR3JpZDtcclxuXHJcbiAgICAgICAgICAgICAgICBmb3IgKHZhciB4ID0gMDsgeCA8IHRoaXMuX0JMT0NLX1dJRFRIOyB4KyspIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciB5ID0gMDsgeSA8IHRoaXMuX0JMT0NLX0hFSUdIVDsgeSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjeCA9IHggKiB0aGlzLl9ibG9ja19zaXplO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgY3kgPSB5ICogdGhpcy5fYmxvY2tfc2l6ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX2N0eC5maWxsUmVjdChjeCArIGJvcmRlcldpZHRoLCBjeSArIGJvcmRlcldpZHRoLCB0aGlzLl9ibG9ja19zaXplIC0gYm9yZGVyV2lkdGggKiAyLCB0aGlzLl9ibG9ja19zaXplIC0gYm9yZGVyV2lkdGggKiAyKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9jdHguZ2xvYmFsQWxwaGEgPSAxLjA7XHJcbiAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIFNoYXBlc1xyXG4gICAgICAgICAqL1xyXG4gICAgICAgIF9lYXN5U2hhcGVGYWN0b3J5OiBudWxsLFxyXG4gICAgICAgIF9ub3JtYWxTaGFwZUZhY3Rvcnk6IG51bGwsXHJcbiAgICAgICAgX2hhcmRTaGFwZUZhY3Rvcnk6IG51bGwsXHJcblxyXG4gICAgICAgIF9zaGFwZXM6IHtcclxuICAgICAgICAgICAgLyoqXHJcbiAgICAgICAgICAgICAqIFRoZSBzaGFwZXMgaGF2ZSBhIHJlZmVyZW5jZSBwb2ludCAodGhlIGRvdCkgYW5kIGFsd2F5cyByb3RhdGUgbGVmdC5cclxuICAgICAgICAgICAgICogS2VlcCBpbiBtaW5kIHRoYXQgdGhlIGJsb2NrcyBzaG91bGQga2VlcCBpbiB0aGUgc2FtZSByZWxhdGl2ZSBwb3NpdGlvbiB3aGVuIHJvdGF0aW5nLFxyXG4gICAgICAgICAgICAgKiB0byBhbGxvdyBmb3IgY3VzdG9tIHBlci1ibG9jayB0aGVtZXMuXHJcbiAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAvKiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgKiAgIFggICAgICBcclxuICAgICAgICAgICAgICogICBPICBYT1hYXHJcbiAgICAgICAgICAgICAqICAgWCAgICAgIFxyXG4gICAgICAgICAgICAgKiAgIFhcclxuICAgICAgICAgICAgICogICAuICAgLiAgICAgIFxyXG4gICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgbGluZTogW1xyXG4gICAgICAgICAgICAgICAgWzAsIC0xLCAwLCAtMiwgMCwgLTMsIDAsIC00XSxcclxuICAgICAgICAgICAgICAgIFsyLCAtMiwgMSwgLTIsIDAsIC0yLCAtMSwgLTJdLFxyXG4gICAgICAgICAgICAgICAgWzAsIC00LCAwLCAtMywgMCwgLTIsIDAsIC0xXSxcclxuICAgICAgICAgICAgICAgIFstMSwgLTIsIDAsIC0yLCAxLCAtMiwgMiwgLTJdXHJcbiAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICAqICBYWFxyXG4gICAgICAgICAgICAgKiAgWFhcclxuICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgIHNxdWFyZTogW1xyXG4gICAgICAgICAgICAgICAgWzAsIDAsIDEsIDAsIDAsIC0xLCAxLCAtMV0sXHJcbiAgICAgICAgICAgICAgICBbMSwgMCwgMSwgLTEsIDAsIDAsIDAsIC0xXSxcclxuICAgICAgICAgICAgICAgIFsxLCAtMSwgMCwgLTEsIDEsIDAsIDAsIDBdLFxyXG4gICAgICAgICAgICAgICAgWzAsIC0xLCAwLCAwLCAxLCAtMSwgMSwgMF1cclxuICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgLypcclxuICAgICAgICAgICAgICogICAgWCAgIFggICAgICAgWFxyXG4gICAgICAgICAgICAgKiAgIFhPWCBYTyAgWE9YICBPWFxyXG4gICAgICAgICAgICAgKiAgIC4gICAuWCAgLlggIC5YXHJcbiAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICBhcnJvdzogW1xyXG4gICAgICAgICAgICAgICAgWzAsIC0xLCAxLCAtMSwgMiwgLTEsIDEsIC0yXSxcclxuICAgICAgICAgICAgICAgIFsxLCAwLCAxLCAtMSwgMSwgLTIsIDAsIC0xXSxcclxuICAgICAgICAgICAgICAgIFsyLCAtMSwgMSwgLTEsIDAsIC0xLCAxLCAwXSxcclxuICAgICAgICAgICAgICAgIFsxLCAtMiwgMSwgLTEsIDEsIDAsIDIsIC0xXVxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICAgKiAgICBYICAgIFggWFggXHJcbiAgICAgICAgICAgICAqICAgIE8gIFhPWCAgTyBYT1ggXHJcbiAgICAgICAgICAgICAqICAgLlhYIC4gICAuWCBYICAgXHJcbiAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICByaWdodEhvb2s6IFtcclxuICAgICAgICAgICAgICAgIFsyLCAwLCAxLCAwLCAxLCAtMSwgMSwgLTJdLFxyXG4gICAgICAgICAgICAgICAgWzIsIC0yLCAyLCAtMSwgMSwgLTEsIDAsIC0xXSxcclxuICAgICAgICAgICAgICAgIFswLCAtMiwgMSwgLTIsIDEsIC0xLCAxLCAwXSxcclxuICAgICAgICAgICAgICAgIFswLCAwLCAwLCAtMSwgMSwgLTEsIDIsIC0xXVxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICAgKiAgICBYICAgICAgWFggWCAgXHJcbiAgICAgICAgICAgICAqICAgIE8gWE9YICBPICBYT1hcclxuICAgICAgICAgICAgICogICBYWCAuIFggLlggIC4gIFxyXG4gICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgbGVmdEhvb2s6IFtcclxuICAgICAgICAgICAgICAgIFswLCAwLCAxLCAwLCAxLCAtMSwgMSwgLTJdLFxyXG4gICAgICAgICAgICAgICAgWzIsIDAsIDIsIC0xLCAxLCAtMSwgMCwgLTFdLFxyXG4gICAgICAgICAgICAgICAgWzIsIC0yLCAxLCAtMiwgMSwgLTEsIDEsIDBdLFxyXG4gICAgICAgICAgICAgICAgWzAsIC0yLCAwLCAtMSwgMSwgLTEsIDIsIC0xXVxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICAgKiAgICBYICBYWCBcclxuICAgICAgICAgICAgICogICBYTyAgIE9YXHJcbiAgICAgICAgICAgICAqICAgWCAgIC4gIFxyXG4gICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgbGVmdFphZzogW1xyXG4gICAgICAgICAgICAgICAgWzAsIDAsIDAsIC0xLCAxLCAtMSwgMSwgLTJdLFxyXG4gICAgICAgICAgICAgICAgWzIsIC0xLCAxLCAtMSwgMSwgLTIsIDAsIC0yXSxcclxuICAgICAgICAgICAgICAgIFsxLCAtMiwgMSwgLTEsIDAsIC0xLCAwLCAwXSxcclxuICAgICAgICAgICAgICAgIFswLCAtMiwgMSwgLTIsIDEsIC0xLCAyLCAtMV1cclxuICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgLypcclxuICAgICAgICAgICAgICogICBYICAgIFxyXG4gICAgICAgICAgICAgKiAgIFhPICAgT1hcclxuICAgICAgICAgICAgICogICAuWCAgWFggICBcclxuICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgIHJpZ2h0WmFnOiBbXHJcbiAgICAgICAgICAgICAgICBbMSwgMCwgMSwgLTEsIDAsIC0xLCAwLCAtMl0sXHJcbiAgICAgICAgICAgICAgICBbMiwgLTEsIDEsIC0xLCAxLCAwLCAwLCAwXSxcclxuICAgICAgICAgICAgICAgIFswLCAtMiwgMCwgLTEsIDEsIC0xLCAxLCAwXSxcclxuICAgICAgICAgICAgICAgIFswLCAwLCAxLCAwLCAxLCAtMSwgMiwgLTFdXHJcbiAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICAqICBcclxuICAgICAgICAgICAgICogICBPICAgTyAgIE8gICBPXHJcbiAgICAgICAgICAgICAqICAgXHJcbiAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICBkb3Q6IFtcclxuICAgICAgICAgICAgICAgIFswLCAwXSxcclxuICAgICAgICAgICAgICAgIFswLCAwXSxcclxuICAgICAgICAgICAgICAgIFswLCAwXSxcclxuICAgICAgICAgICAgICAgIFswLCAwXVxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICAgKiAgIFhcclxuICAgICAgICAgICAgICogICBPICAgWE9cclxuICAgICAgICAgICAgICogICBcclxuICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgIGRvdWJsZURvdDogW1xyXG4gICAgICAgICAgICAgICAgWzAsIC0xLCAwLCAtMl0sXHJcbiAgICAgICAgICAgICAgICBbLTEsIC0yLCAwLCAtMl0sXHJcbiAgICAgICAgICAgICAgICBbMCwgLTIsIDAsIC0xXSxcclxuICAgICAgICAgICAgICAgIFswLCAtMiwgLTEsIC0yXVxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICAgKiAgIFhcclxuICAgICAgICAgICAgICogICBPICAgWE9YXHJcbiAgICAgICAgICAgICAqICAgWFxyXG4gICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgdHJpcGxlRG90OiBbXHJcbiAgICAgICAgICAgICAgICBbMCwgLTEsIDAsIC0yLCAwLCAtM10sXHJcbiAgICAgICAgICAgICAgICBbLTEsIC0yLCAwLCAtMiwgMSwgLTJdLFxyXG4gICAgICAgICAgICAgICAgWzAsIC0zLCAwLCAtMiwgMCwgLTFdLFxyXG4gICAgICAgICAgICAgICAgWzEsIC0yLCAwLCAtMiwgLTEsIC0yXVxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAvKiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgKiAgICBYICAgICAgXHJcbiAgICAgICAgICAgICAqICAgWDBYICBcclxuICAgICAgICAgICAgICogICAgWCAgICBcclxuICAgICAgICAgICAgICogICAgICAgXHJcbiAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICBlbXB0eUNyb3NzOiBbXHJcbiAgICAgICAgICAgICAgICBbMCwgMCwgMSwgLTEsIDAsIC0yLCAtMSwgLTFdLFxyXG4gICAgICAgICAgICAgICAgWzAsIDAsIDEsIC0xLCAwLCAtMiwgLTEsIC0xXSxcclxuICAgICAgICAgICAgICAgIFswLCAwLCAxLCAtMSwgMCwgLTIsIC0xLCAtMV0sXHJcbiAgICAgICAgICAgICAgICBbMCwgMCwgMSwgLTEsIDAsIC0yLCAtMSwgLTFdXHJcbiAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICAqICBYIFggICAgIFhcclxuICAgICAgICAgICAgICogICBPICAgICAgIE9YXHJcbiAgICAgICAgICAgICAqICAgWCAgICAgIFggIFxyXG4gICAgICAgICAgICAgKiBcclxuICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgIHRIb29rOiBbXHJcbiAgICAgICAgICAgICAgICBbLTEsIDIsIC0xLCAxLCAwLCAwLCAtMiwgMF0sXHJcbiAgICAgICAgICAgICAgICBbMCwgMSwgLTEsIDEsIC0yLCAwLCAtMiwgMl0sXHJcbiAgICAgICAgICAgICAgICBbLTEsIDAsIC0xLCAxLCAwLCAyLCAtMiwgMl0sXHJcbiAgICAgICAgICAgICAgICBbLTIsIDEsIC0xLCAxLCAwLCAwLCAwLCAyXVxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICAgKiAgICAgWCAgICBYXHJcbiAgICAgICAgICAgICAqICAgIFggICAgICBYXHJcbiAgICAgICAgICAgICAqICAgWCAgICAgICAgWFxyXG4gICAgICAgICAgICAgKiAgWCAgICAgICAgICBYXHJcbiAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICBkaWFnb25hbDogW1xyXG4gICAgICAgICAgICAgICAgWzEsIDEsIDAsIDAsIC0xLCAtMSwgLTIsIC0yXSxcclxuICAgICAgICAgICAgICAgIFstMSwgMSwgMCwgMCwgMSwgLTEsIDIsIC0yXSxcclxuICAgICAgICAgICAgICAgIFsxLCAxLCAwLCAwLCAtMSwgLTEsIC0yLCAtMl0sXHJcbiAgICAgICAgICAgICAgICBbLTEsIDEsIDAsIDAsIDEsIC0xLCAyLCAtMl1cclxuICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgLyogICAgICAgICAgICBcclxuICAgICAgICAgICAgICogICAgWCAgICAgIFxyXG4gICAgICAgICAgICAgKiAgIFhYWCAgXHJcbiAgICAgICAgICAgICAqICAgIFggICAgXHJcbiAgICAgICAgICAgICAqICAgICAgIFxyXG4gICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgZnVsbENyb3NzOiBbXHJcbiAgICAgICAgICAgICAgICBbMCwgMCwgMSwgLTEsIDAsIC0xLCAwLCAtMiwgLTEsIC0xXSxcclxuICAgICAgICAgICAgICAgIFswLCAwLCAxLCAtMSwgMCwgLTEsIDAsIC0yLCAtMSwgLTFdLFxyXG4gICAgICAgICAgICAgICAgWzAsIDAsIDEsIC0xLCAwLCAtMSwgMCwgLTIsIC0xLCAtMV0sXHJcbiAgICAgICAgICAgICAgICBbMCwgMCwgMSwgLTEsIDAsIC0xLCAwLCAtMiwgLTEsIC0xXVxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAvKiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgKiAgIFggICAgICBcclxuICAgICAgICAgICAgICogICBPICBYT1hYWFxyXG4gICAgICAgICAgICAgKiAgIFggICAgICBcclxuICAgICAgICAgICAgICogICBYXHJcbiAgICAgICAgICAgICAqICAgWCAgIC4gICAgICBcclxuICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgIGxvbmdMaW5lOiBbXHJcbiAgICAgICAgICAgICAgICBbMCwgLTEsIDAsIC0yLCAwLCAtMywgMCwgLTQsIDAsIC01XSxcclxuICAgICAgICAgICAgICAgIFsyLCAtMiwgMSwgLTIsIDAsIC0yLCAtMSwgLTIsIC0yLCAtMl0sXHJcbiAgICAgICAgICAgICAgICBbMCwgLTQsIDAsIC0zLCAwLCAtMiwgMCwgLTEsIDAsIDBdLFxyXG4gICAgICAgICAgICAgICAgWy0yLCAtMiwgLTEsIC0yLCAwLCAtMiwgMSwgLTIsIDIsIC0yXVxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICAgKiAgIFhYICAgICAgWFggWCBYIFxyXG4gICAgICAgICAgICAgKiAgICBPIFhPWCAgTyAgWE9YXHJcbiAgICAgICAgICAgICAqICAgWFggWCBYIC5YWCAgLiAgXHJcbiAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICBkb3VibGVIb29rOiBbXHJcbiAgICAgICAgICAgICAgICBbMCwgMCwgMSwgMCwgMSwgLTEsIDEsIC0yLCAwLCAtMl0sXHJcbiAgICAgICAgICAgICAgICBbMiwgMCwgMiwgLTEsIDEsIC0xLCAwLCAtMSwgMCwgMF0sXHJcbiAgICAgICAgICAgICAgICBbMiwgLTIsIDEsIC0yLCAxLCAtMSwgMSwgMCwgMiwgMF0sXHJcbiAgICAgICAgICAgICAgICBbMCwgLTIsIDAsIC0xLCAxLCAtMSwgMiwgLTEsIDIsIC0yXVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgX1NldHVwU2hhcGVGYWN0b3JpZXM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICB2YXIgZ2FtZSA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5fZWFzeVNoYXBlRmFjdG9yeSAhPT0gbnVsbCB8fCB0aGlzLl9ub3JtYWxTaGFwZUZhY3RvcnkgIT09IG51bGwgfHwgdGhpcy5faGFyZFNoYXBlRmFjdG9yeSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBTaGFwZShnYW1lLCBvcmllbnRhdGlvbnMsIHN5bW1ldHJpY2FsLCBibG9ja1R5cGUsIHNoYXBlVHlwZSkge1xyXG5cclxuICAgICAgICAgICAgICAgICQuZXh0ZW5kKHRoaXMsIHtcclxuICAgICAgICAgICAgICAgICAgICB4OiAwLFxyXG4gICAgICAgICAgICAgICAgICAgIHk6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgc3ltbWV0cmljYWw6IHN5bW1ldHJpY2FsLFxyXG4gICAgICAgICAgICAgICAgICAgIGluaXQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkLmV4dGVuZCh0aGlzLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcmllbnRhdGlvbjogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHg6IE1hdGguZmxvb3IoZ2FtZS5fQkxPQ0tfV0lEVEggLyAyKSAtIDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB5OiAtMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgYmxvY2tUeXBlOiBibG9ja1R5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgc2hhcGVUeXBlOiBzaGFwZVR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgYmxvY2tWYXJpYXRpb246IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgYmxvY2tzTGVuOiBvcmllbnRhdGlvbnNbMF0ubGVuZ3RoLFxyXG4gICAgICAgICAgICAgICAgICAgIG9yaWVudGF0aW9uczogb3JpZW50YXRpb25zLFxyXG4gICAgICAgICAgICAgICAgICAgIG9yaWVudGF0aW9uOiAwLCAvLyA0IHBvc3NpYmxlXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHJvdGF0ZTogZnVuY3Rpb24oZGlyZWN0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBvcmllbnRhdGlvbiA9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAodGhpcy5vcmllbnRhdGlvbiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRpcmVjdGlvbiA9PT0gXCJsZWZ0XCIgPyAxIDogLTEpICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA0KSAlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA0O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFnYW1lLl9jaGVja0NvbGxpc2lvbnMoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy54LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMueSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEJsb2NrcyhvcmllbnRhdGlvbilcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3JpZW50YXRpb24gPSBvcmllbnRhdGlvbjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLnJlbmRlckNoYW5nZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG9nT3JpZW50YXRpb24gPSB0aGlzLm9yaWVudGF0aW9uO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG9nWCA9IHRoaXMueDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBvZ1kgPSB0aGlzLnk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vcmllbnRhdGlvbiA9IG9yaWVudGF0aW9uO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdoaWxlICh0aGlzLnggPj0gZ2FtZS5fQkxPQ0tfV0lEVEggLSAyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy54LS07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aGlsZSAodGhpcy54IDwgMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMueCsrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmJsb2NrVHlwZSA9PT0gXCJsaW5lXCIgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnggPT09IDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLngrKztcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fY2hlY2tDb2xsaXNpb25zKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLngsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMueSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRCbG9ja3Mob3JpZW50YXRpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy55LS07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jaGVja0NvbGxpc2lvbnMoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLngsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEJsb2NrcyhvcmllbnRhdGlvbilcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnggPSBvZ1g7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMueSA9IG9nWTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vcmllbnRhdGlvbiA9IG9nT3JpZW50YXRpb247XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQucmVuZGVyQ2hhbmdlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgICAgICAgICBtb3ZlUmlnaHQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWdhbWUuX2NoZWNrQ29sbGlzaW9ucyhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnggKyAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMueSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEJsb2NrcygpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLngrKztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLnJlbmRlckNoYW5nZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBtb3ZlTGVmdDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghZ2FtZS5fY2hlY2tDb2xsaXNpb25zKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMueCAtIDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy55LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0QmxvY2tzKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMueC0tO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQucmVuZGVyQ2hhbmdlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGRyb3A6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWdhbWUuX2NoZWNrQ29sbGlzaW9ucyhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLngsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy55ICsgMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEJsb2NrcygpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnkrKztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIFJlc2V0IHRoZSBkcm9wIGNvdW50LCBhcyB3ZSBkcm9wcGVkIHRoZSBibG9jayBzb29uZXJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmRyb3BDb3VudCA9IC0xO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuYW5pbWF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQucmVuZGVyQ2hhbmdlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHF1aWNrRHJvcDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgaSA9IHRoaXMueTsgaSA8IGdhbWUuX0JMT0NLX0hFSUdIVDsgaSsrXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFnYW1lLl9jaGVja0NvbGxpc2lvbnMoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMueCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy55ICsgMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRCbG9ja3MoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnkgPSBpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLnJlbmRlckNoYW5nZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5kcm9wRmlyc3RQcmVzcyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZ2V0QmxvY2tzOiBmdW5jdGlvbihvcmllbnRhdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBvcHRpb25hbCBwYXJhbVxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5vcmllbnRhdGlvbnNbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcmllbnRhdGlvbiAhPT0gdW5kZWZpbmVkID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9yaWVudGF0aW9uIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3JpZW50YXRpb25cclxuICAgICAgICAgICAgICAgICAgICAgICAgXTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGRyYXc6IGZ1bmN0aW9uKF94LCBfeSwgX29yaWVudGF0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBibG9ja3MgPSB0aGlzLmdldEJsb2Nrcyhfb3JpZW50YXRpb24pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeCA9IF94ID09PSB1bmRlZmluZWQgPyB0aGlzLnggOiBfeCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHkgPSBfeSA9PT0gdW5kZWZpbmVkID8gdGhpcy55IDogX3ksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpID0gMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGluZGV4ID0gMDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoOyBpIDwgdGhpcy5ibG9ja3NMZW47IGkgKz0gMikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuZHJhd0Jsb2NrKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHggKyBibG9ja3NbaV0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeSArIGJsb2Nrc1tpICsgMV0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ibG9ja1R5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ibG9ja1ZhcmlhdGlvbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmRleCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9yaWVudGF0aW9uLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRydWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmRleCsrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBkcmF3T2JzdGFjbGU6IGZ1bmN0aW9uKHgsIHksIF9vcmllbnRhdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgYmxvY2tzID0gdGhpcy5nZXRCbG9ja3MoX29yaWVudGF0aW9uKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGkgPSAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5kZXggPSAwO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy54ID0geDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy55ID0geTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoOyBpIDwgdGhpcy5ibG9ja3NMZW47IGkgKz0gMikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuZHJhd0Jsb2NrKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHggKyBibG9ja3NbaV0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeSArIGJsb2Nrc1tpICsgMV0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ibG9ja1R5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ibG9ja1ZhcmlhdGlvbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmRleCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9yaWVudGF0aW9uLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZhbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5kZXgrKztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGdldEJvdW5kczogZnVuY3Rpb24oX2Jsb2Nrcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBfYmxvY2tzIGNhbiBiZSBhbiBhcnJheSBvZiBibG9ja3MsIGFuIG9yaWVudGF0aW9uIGluZGV4LCBvciB1bmRlZmluZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGJsb2NrcyA9ICQuaXNBcnJheShfYmxvY2tzKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYmxvY2tzIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0QmxvY2tzKF9ibG9ja3MpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaSA9IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZW4gPSBibG9ja3MubGVuZ3RoLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWlueCA9IDk5OSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heHggPSAtOTk5LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWlueSA9IDk5OSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heHkgPSAtOTk5O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKDsgaSA8IGxlbjsgaSArPSAyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYmxvY2tzW2ldIDwgbWlueCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1pbnggPSBibG9ja3NbaV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYmxvY2tzW2ldID4gbWF4eCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heHggPSBibG9ja3NbaV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYmxvY2tzW2kgKyAxXSA8IG1pbnkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtaW55ID0gYmxvY2tzW2kgKyAxXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChibG9ja3NbaSArIDFdID4gbWF4eSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heHkgPSBibG9ja3NbaSArIDFdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiBtaW54LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IG1heHgsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3A6IG1pbnksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3R0b206IG1heHksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogbWF4eCAtIG1pbngsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IG1heHkgLSBtaW55XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuaW5pdCgpO1xyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgdGhpcy5fZWFzeVNoYXBlRmFjdG9yeSA9IHtcclxuICAgICAgICAgICAgICAgIGxpbmU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgU2hhcGUoZ2FtZSwgZ2FtZS5fc2hhcGVzLmxpbmUsIGZhbHNlLCAnbGluZScsICdlYXN5Jyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgc3F1YXJlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFNoYXBlKGdhbWUsIGdhbWUuX3NoYXBlcy5zcXVhcmUsIGZhbHNlLCAnc3F1YXJlJywgJ2Vhc3knKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBkb3Q6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgU2hhcGUoZ2FtZSwgZ2FtZS5fc2hhcGVzLmRvdCwgZmFsc2UsICdkb3QnLCAnZWFzeScpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGRvdWJsZURvdDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBTaGFwZShnYW1lLCBnYW1lLl9zaGFwZXMuZG91YmxlRG90LCBmYWxzZSwgJ2RvdWJsZURvdCcsICdlYXN5Jyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgdHJpcGxlRG90OiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFNoYXBlKGdhbWUsIGdhbWUuX3NoYXBlcy50cmlwbGVEb3QsIGZhbHNlLCAndHJpcGxlRG90JywgJ2Vhc3knKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX25vcm1hbFNoYXBlRmFjdG9yeSA9IHtcclxuICAgICAgICAgICAgICAgIGFycm93OiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFNoYXBlKGdhbWUsIGdhbWUuX3NoYXBlcy5hcnJvdywgZmFsc2UsICdhcnJvdycsICdub3JtYWwnKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBsZWZ0SG9vazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBTaGFwZShnYW1lLCBnYW1lLl9zaGFwZXMubGVmdEhvb2ssIGZhbHNlLCAnbGVmdEhvb2snLCAnbm9ybWFsJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgcmlnaHRIb29rOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFNoYXBlKGdhbWUsIGdhbWUuX3NoYXBlcy5yaWdodEhvb2ssIGZhbHNlLCAncmlnaHRIb29rJywgJ25vcm1hbCcpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGxlZnRaYWc6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgU2hhcGUoZ2FtZSwgZ2FtZS5fc2hhcGVzLmxlZnRaYWcsIGZhbHNlLCAnbGVmdFphZycsICdub3JtYWwnKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICByaWdodFphZzogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBTaGFwZShnYW1lLCBnYW1lLl9zaGFwZXMucmlnaHRaYWcsIGZhbHNlLCAncmlnaHRaYWcnLCAnbm9ybWFsJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9oYXJkU2hhcGVGYWN0b3J5ID0ge1xyXG4gICAgICAgICAgICAgICAgZW1wdHlDcm9zczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBTaGFwZShnYW1lLCBnYW1lLl9zaGFwZXMuZW1wdHlDcm9zcywgZmFsc2UsICdlbXB0eUNyb3NzJywgJ2hhcmQnKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB0SG9vazogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBTaGFwZShnYW1lLCBnYW1lLl9zaGFwZXMudEhvb2ssIGZhbHNlLCAndEhvb2snLCAnaGFyZCcpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGRpYWdvbmFsOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFNoYXBlKGdhbWUsIGdhbWUuX3NoYXBlcy5kaWFnb25hbCwgZmFsc2UsICdkaWFnb25hbCcsICdoYXJkJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZnVsbENyb3NzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFNoYXBlKGdhbWUsIGdhbWUuX3NoYXBlcy5mdWxsQ3Jvc3MsIGZhbHNlLCAnZnVsbENyb3NzJywgJ2hhcmQnKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBsb25nTGluZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBTaGFwZShnYW1lLCBnYW1lLl9zaGFwZXMubG9uZ0xpbmUsIGZhbHNlLCAnbG9uZ0xpbmUnLCAnaGFyZCcpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGRvdWJsZUhvb2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgU2hhcGUoZ2FtZSwgZ2FtZS5fc2hhcGVzLmRvdWJsZUhvb2ssIGZhbHNlLCAnZG91YmxlSG9vaycsICdoYXJkJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9vYnN0YWNsZVNoYXBlRmFjdG9yeSA9IHtcclxuICAgICAgICAgICAgICAgIGRvdDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBTaGFwZShnYW1lLCBnYW1lLl9zaGFwZXMuZG90LCBmYWxzZSwgJ2RvdCcpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgZ2FtZS5fc2hhcGVGYWN0b3JpZXMgPSBuZXcgQXJyYXkodGhpcy5fZWFzeVNoYXBlRmFjdG9yeSwgdGhpcy5fbm9ybWFsU2hhcGVGYWN0b3J5LCB0aGlzLl9oYXJkU2hhcGVGYWN0b3J5KTtcclxuXHJcbiAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgIF9TZXR1cEZpbGxlZDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHZhciBnYW1lID0gdGhpcztcclxuICAgICAgICAgICAgaWYgKHRoaXMuX2ZpbGxlZCAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9maWxsZWQgPSB7XHJcbiAgICAgICAgICAgICAgICBkYXRhOiBuZXcgQXJyYXkoZ2FtZS5fQkxPQ0tfV0lEVEggKiBnYW1lLl9CTE9DS19IRUlHSFQpLFxyXG4gICAgICAgICAgICAgICAgc2NvcmU6IDAsXHJcbiAgICAgICAgICAgICAgICB0b0NsZWFyOiB7fSxcclxuICAgICAgICAgICAgICAgIGNoZWNrOiBmdW5jdGlvbih4LCB5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGF0YVt0aGlzLmFzSW5kZXgoeCwgeSldO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGFkZDogZnVuY3Rpb24oeCwgeSwgYmxvY2tUeXBlLCBibG9ja1ZhcmlhdGlvbiwgYmxvY2tJbmRleCwgYmxvY2tPcmllbnRhdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh4ID49IDAgJiYgeCA8IGdhbWUuX0JMT0NLX1dJRFRIICYmIHkgPj0gMCAmJiB5IDwgZ2FtZS5fQkxPQ0tfSEVJR0hUKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0YVt0aGlzLmFzSW5kZXgoeCwgeSldID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYmxvY2tUeXBlOiBibG9ja1R5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBibG9ja1ZhcmlhdGlvbjogYmxvY2tWYXJpYXRpb24sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBibG9ja0luZGV4OiBibG9ja0luZGV4LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYmxvY2tPcmllbnRhdGlvbjogYmxvY2tPcmllbnRhdGlvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBnZXRGcmVlU3BhY2VzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgY291bnQgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5kYXRhLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvdW50ICs9ICh0aGlzLmRhdGFbaV0gPyAxIDogMCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGFzSW5kZXg6IGZ1bmN0aW9uKHgsIHkpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4geCArIHkgKiBnYW1lLl9CTE9DS19XSURUSDtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBhc1g6IGZ1bmN0aW9uKGluZGV4KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGluZGV4ICUgZ2FtZS5fQkxPQ0tfV0lEVEg7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgYXNZOiBmdW5jdGlvbihpbmRleCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBNYXRoLmZsb29yKGluZGV4IC8gZ2FtZS5fQkxPQ0tfV0lEVEgpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGNsZWFyQWxsOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICBkZWxldGUgdGhpcy5kYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0YSA9IG5ldyBBcnJheShnYW1lLl9CTE9DS19XSURUSCAqIGdhbWUuX0JMT0NLX0hFSUdIVCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgX3BvcFJvdzogZnVuY3Rpb24ocm93X3RvX3BvcCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSBnYW1lLl9CTE9DS19XSURUSCAqIChyb3dfdG9fcG9wICsgMSkgLSAxOyBpID49IDA7IGktLSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGFbaV0gPSAoaSA+PSBnYW1lLl9CTE9DS19XSURUSCA/IHRoaXMuZGF0YVtpIC0gZ2FtZS5fQkxPQ0tfV0lEVEhdIDogdW5kZWZpbmVkKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgY2hlY2tGb3JDbGVhcnM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBzdGFydExpbmVzID0gZ2FtZS5fYm9hcmQubGluZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIHJvd3MgPSBbXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaSwgbGVuLCBjb3VudCwgbW9kO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGkgPSAwLCBsZW4gPSB0aGlzLmRhdGEubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbW9kID0gdGhpcy5hc1goaSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtb2QgPT0gMCkgY291bnQgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5kYXRhW2ldICYmIHR5cGVvZiB0aGlzLmRhdGFbaV0gIT09ICd1bmRlZmluZWQnICYmIHR5cGVvZiB0aGlzLmRhdGFbaV0uYmxvY2tUeXBlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY291bnQgKz0gMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAobW9kID09IGdhbWUuX0JMT0NLX1dJRFRIIC0gMSAmJiBjb3VudCA9PSBnYW1lLl9CTE9DS19XSURUSCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93cy5wdXNoKHRoaXMuYXNZKGkpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChpID0gMCwgbGVuID0gcm93cy5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9wb3BSb3cocm93c1tpXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmxpbmVzKys7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChnYW1lLl9ib2FyZC5saW5lcyAlIDEwID09IDAgJiYgZ2FtZS5fYm9hcmQuZHJvcERlbGF5ID4gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuZHJvcERlbGF5ICo9IDAuOTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGNsZWFyZWRMaW5lcyA9IGdhbWUuX2JvYXJkLmxpbmVzIC0gc3RhcnRMaW5lcztcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl91cGRhdGVTY29yZShjbGVhcmVkTGluZXMpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIF91cGRhdGVTY29yZTogZnVuY3Rpb24obnVtTGluZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAobnVtTGluZXMgPD0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHZhciBzY29yZXMgPSBbMCwgNDAwLCAxMDAwLCAzMDAwLCAxMjAwMF07XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG51bUxpbmVzID49IHNjb3Jlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbnVtTGluZXMgPSBzY29yZXMubGVuZ3RoIC0gMVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zY29yZSArPSBzY29yZXNbbnVtTGluZXNdO1xyXG4gICAgICAgICAgICAgICAgICAgIGdhbWUuXyRzY29yZVRleHQudGV4dCh0aGlzLnNjb3JlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZ2FtZS5vcHRpb25zLm9uTGluZS5jYWxsKGdhbWUuZWxlbWVudCwgbnVtTGluZXMsIHNjb3Jlc1tudW1MaW5lc10sIHRoaXMuc2NvcmUpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIF9yZXNldFNjb3JlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNjb3JlID0gMDtcclxuICAgICAgICAgICAgICAgICAgICBnYW1lLl8kc2NvcmVUZXh0LnRleHQodGhpcy5zY29yZSk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZHJhdzogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IHRoaXMuZGF0YS5sZW5ndGgsIHJvdywgY29sb3I7IGkgPCBsZW47IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5kYXRhW2ldICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdyA9IHRoaXMuYXNZKGkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGJsb2NrID0gdGhpcy5kYXRhW2ldO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuZHJhd0Jsb2NrKHRoaXMuYXNYKGkpLCByb3csIGJsb2NrLmJsb2NrVHlwZSwgYmxvY2suYmxvY2tWYXJpYXRpb24sIGJsb2NrLmJsb2NrSW5kZXgsIGJsb2NrLmJsb2NrT3JpZW50YXRpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH0sXHJcblxyXG5cclxuICAgICAgICBfU2V0dXBJbmZvOiBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgICAgIHZhciBnYW1lID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX2luZm8gPSB7XHJcbiAgICAgICAgICAgICAgICBtb2RlOiBnYW1lLm9wdGlvbnMuZGlmZmljdWx0eSxcclxuICAgICAgICAgICAgICAgIG1vZGVzOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgJ25vcm1hbCcsXHJcbiAgICAgICAgICAgICAgICAgICAgJ25pY2UnLFxyXG4gICAgICAgICAgICAgICAgICAgICdldmlsJ1xyXG4gICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgIG1vZGVzWTogMTcwLFxyXG4gICAgICAgICAgICAgICAgYXV0b3BpbG90WTogbnVsbCxcclxuXHJcbiAgICAgICAgICAgICAgICBpbml0OiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1vZGUgPSBnYW1lLm9wdGlvbnMuZGlmZmljdWx0eTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBzZXRNb2RlOiBmdW5jdGlvbihtb2RlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tb2RlID0gbW9kZTtcclxuICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5uZXh0U2hhcGUodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgIH0sXHJcblxyXG5cclxuICAgICAgICBfU2V0dXBCb2FyZDogZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICAgICB2YXIgZ2FtZSA9IHRoaXM7XHJcbiAgICAgICAgICAgIHZhciBpbmZvID0gdGhpcy5faW5mbztcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX2JvYXJkID0ge1xyXG4gICAgICAgICAgICAgICAgLy8gVGhpcyBzZXRzIHRoZSB0aWNrIHJhdGUgZm9yIHRoZSBnYW1lXHJcbiAgICAgICAgICAgICAgICBhbmltYXRlRGVsYXk6IDEwMDAgLyBnYW1lLm9wdGlvbnMuc3BlZWQsXHJcblxyXG4gICAgICAgICAgICAgICAgYW5pbWF0ZVRpbWVvdXRJZDogbnVsbCxcclxuICAgICAgICAgICAgICAgIGN1cjogbnVsbCxcclxuXHJcbiAgICAgICAgICAgICAgICBsaW5lczogMCxcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBEcm9wQ291bnQgaW5jcmVtZW50cyBvbiBlYWNoIGFuaW1hdGlvbiBmcmFtZS4gQWZ0ZXIgbiBmcmFtZXMsIHRoZSBwaWVjZSBkcm9wcyAxIHNxdWFyZVxyXG4gICAgICAgICAgICAgICAgLy8gQnkgbWFraW5nIGRyb3BkZWxheSBsb3dlciAoZG93biB0byAwKSwgdGhlIHBpZWNlcyBtb3ZlIGZhc3RlciwgdXAgdG8gb25jZSBwZXIgdGljayAoYW5pbWF0ZURlbGF5KS5cclxuICAgICAgICAgICAgICAgIGRyb3BDb3VudDogMCxcclxuICAgICAgICAgICAgICAgIGRyb3BEZWxheTogNSwgLy81LFxyXG5cclxuICAgICAgICAgICAgICAgIGhvbGRpbmc6IHtcclxuICAgICAgICAgICAgICAgICAgICBsZWZ0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgIHJpZ2h0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgIGRyb3A6IG51bGxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBob2xkaW5nVGhyZXNob2xkOiAyMDAsIC8vIEhvdyBsb25nIGRvIHlvdSBoYXZlIHRvIGhvbGQgYSBrZXkgdG8gbWFrZSBjb21tYW5kcyByZXBlYXQgKGluIG1zKVxyXG5cclxuICAgICAgICAgICAgICAgIHN0YXJ0ZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgZ2FtZW92ZXI6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgbGV2ZWxPdmVyOiBmYWxzZSxcclxuXHJcbiAgICAgICAgICAgICAgICByZW5kZXJDaGFuZ2VkOiB0cnVlLFxyXG5cclxuICAgICAgICAgICAgICAgIGluaXQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY3VyID0gdGhpcy5uZXh0U2hhcGUoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGdhbWUub3B0aW9ucy5zaG93RmllbGRPblN0YXJ0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2RyYXdCYWNrZ3JvdW5kKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmNyZWF0ZVJhbmRvbUJvYXJkKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLnJlbmRlcigpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaG93U3RhcnRNZXNzYWdlKCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgICAgIHNob3dTdGFydE1lc3NhZ2U6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGdhbWUuXyRzdGFydC5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgICAgIHNob3dHYW1lT3Zlck1lc3NhZ2U6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGdhbWUuXyRnYW1lb3Zlci5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgICAgIGNyZWF0ZU9ic3RhY2xlczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJDcmVhdGluZyBuZXcgb2JzdGFjbGVzXCIpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB2YXIgeCwgeTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBnYW1lLm9wdGlvbnMubnVtT2JzdGFjbGVzOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgeCA9IHRoaXMuZ2V0UmFuZG9tSW50KDAsIGdhbWUuX0JMT0NLX1dJRFRIIC0gMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHkgPSB0aGlzLmdldFJhbmRvbUludCgwLCBnYW1lLl9CTE9DS19IRUlHSFQgLSAxKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG9ic3RhY2xlRnVuYyA9IGdhbWUuX3JhbmRvbU9ic3RhY2xlcygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgb2JzdGFjbGUgPSBvYnN0YWNsZUZ1bmMoZ2FtZS5fZmlsbGVkLCBnYW1lLl9jaGVja0NvbGxpc2lvbnMsIGdhbWUuX0JMT0NLX1dJRFRILCBnYW1lLl9CTE9DS19IRUlHSFQsIGluZm8ubW9kZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic3RhY2xlLmluaXQoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3VyID0gb2JzdGFjbGU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlbmRlck9ic3RhY2xlKHRydWUsIHgsIHkpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hbmltYXRlT2JzdGFjbGUoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgICAgICBnZXRSYW5kb21JbnQ6IGZ1bmN0aW9uKG1pbiwgbWF4KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobWF4IC0gbWluICsgMSkpICsgbWluKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICAgICAgbmV4dFNoYXBlOiBmdW5jdGlvbihfc2V0X25leHRfb25seSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBuZXh0ID0gdGhpcy5uZXh0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBmdW5jLCBzaGFwZSwgcmVzdWx0LCBzaGFwZUZhY3Rvcnk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHNoYXBlRmFjdG9yeSA9IGdhbWUuX3NlbGVjdFJhbmRTaGFwZUZhY3RvcnkoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGluZm8ubW9kZSA9PSAnbmljZScgfHwgaW5mby5tb2RlID09ICdldmlsJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmdW5jID0gZ2FtZS5fbmljZVNoYXBlcztcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmdW5jID0gZ2FtZS5fcmFuZG9tU2hhcGVzKHNoYXBlRmFjdG9yeSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBnYW1lLm9wdGlvbnMub25OZXh0LmNhbGwoZ2FtZS5lbGVtZW50LCB0aGlzLm5leHQpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChnYW1lLm9wdGlvbnMubm9fcHJldmlldykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5leHQgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoX3NldF9uZXh0X29ubHkpIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFwZSA9IGZ1bmMoZ2FtZS5fZmlsbGVkLCBnYW1lLl9jaGVja0NvbGxpc2lvbnMsIGdhbWUuX0JMT0NLX1dJRFRILCBnYW1lLl9CTE9DS19IRUlHSFQsIGluZm8ubW9kZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghc2hhcGUpIHRocm93IG5ldyBFcnJvcignTm8gc2hhcGUgcmV0dXJuZWQgZnJvbSBzaGFwZSBmdW5jdGlvbiEnLCBmdW5jKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2hhcGUuaW5pdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBzaGFwZTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFwZSA9IGZ1bmMoZ2FtZS5fZmlsbGVkLCBnYW1lLl9jaGVja0NvbGxpc2lvbnMsIGdhbWUuX0JMT0NLX1dJRFRILCBnYW1lLl9CTE9DS19IRUlHSFQsIGluZm8ubW9kZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghc2hhcGUpIHRocm93IG5ldyBFcnJvcignTm8gc2hhcGUgcmV0dXJuZWQgZnJvbSBzaGFwZSBmdW5jdGlvbiEnLCBmdW5jKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2hhcGUuaW5pdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5leHQgPSBzaGFwZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKF9zZXRfbmV4dF9vbmx5KSByZXR1cm4gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gbmV4dCB8fCB0aGlzLm5leHRTaGFwZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGdhbWUub3B0aW9ucy5hdXRvcGxheSkgeyAvL2Z1biBsaXR0bGUgaGFjay4uLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9uaWNlU2hhcGVzKGdhbWUuX2ZpbGxlZCwgZ2FtZS5fY2hlY2tDb2xsaXNpb25zLCBnYW1lLl9CTE9DS19XSURUSCwgZ2FtZS5fQkxPQ0tfSEVJR0hULCAnbm9ybWFsJywgcmVzdWx0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0Lm9yaWVudGF0aW9uID0gcmVzdWx0LmJlc3Rfb3JpZW50YXRpb247XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC54ID0gcmVzdWx0LmJlc3RfeDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgZ2FtZS5fdGhlbWUuY29tcGxleEJsb2NrcyAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCQuaXNBcnJheShnYW1lLl90aGVtZS5jb21wbGV4QmxvY2tzW3Jlc3VsdC5ibG9ja1R5cGVdKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmJsb2NrVmFyaWF0aW9uID0gZ2FtZS5fcmFuZEludCgwLCBnYW1lLl90aGVtZS5jb21wbGV4QmxvY2tzW3Jlc3VsdC5ibG9ja1R5cGVdLmxlbmd0aCAtIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmJsb2NrVmFyaWF0aW9uID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGdhbWUuX3RoZW1lLmJsb2NrcyAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCQuaXNBcnJheShnYW1lLl90aGVtZS5ibG9ja3NbcmVzdWx0LmJsb2NrVHlwZV0pKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQuYmxvY2tWYXJpYXRpb24gPSBnYW1lLl9yYW5kSW50KDAsIGdhbWUuX3RoZW1lLmJsb2Nrc1tyZXN1bHQuYmxvY2tUeXBlXS5sZW5ndGggLSAxKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5ibG9ja1ZhcmlhdGlvbiA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgICAgIGFuaW1hdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBkcm9wID0gZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1vdmVkID0gZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdhbWVPdmVyID0gZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5vdyA9IERhdGUubm93KCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmFuaW1hdGVUaW1lb3V0SWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRoaXMuYW5pbWF0ZVRpbWVvdXRJZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAvL2dhbWUudXBkYXRlU2l6ZXMoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCF0aGlzLnBhdXNlZCAmJiAhdGhpcy5nYW1lb3Zlcikge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kcm9wQ291bnQrKztcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIERyb3AgYnkgZGVsYXkgb3IgaG9sZGluZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoKHRoaXMuZHJvcENvdW50ID49IHRoaXMuZHJvcERlbGF5KSB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKGdhbWUub3B0aW9ucy5hdXRvcGxheSkgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICh0aGlzLmhvbGRpbmcuZHJvcCAmJiAobm93IC0gdGhpcy5ob2xkaW5nLmRyb3ApID49IHRoaXMuaG9sZGluZ1RocmVzaG9sZCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRyb3AgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbW92ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kcm9wQ291bnQgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBNb3ZlIExlZnQgYnkgaG9sZGluZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5ob2xkaW5nLmxlZnQgJiYgKG5vdyAtIHRoaXMuaG9sZGluZy5sZWZ0KSA+PSB0aGlzLmhvbGRpbmdUaHJlc2hvbGQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vdmVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3VyLm1vdmVMZWZ0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIE1vdmUgUmlnaHQgYnkgaG9sZGluZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5ob2xkaW5nLnJpZ2h0ICYmIChub3cgLSB0aGlzLmhvbGRpbmcucmlnaHQpID49IHRoaXMuaG9sZGluZ1RocmVzaG9sZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbW92ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXIubW92ZVJpZ2h0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFRlc3QgZm9yIGEgY29sbGlzaW9uLCBhZGQgdGhlIHBpZWNlIHRvIHRoZSBmaWxsZWQgYmxvY2tzIGFuZCBmZXRjaCB0aGUgbmV4dCBvbmVcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRyb3ApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjdXIgPSB0aGlzLmN1cixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB4ID0gY3VyLngsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeSA9IGN1ci55LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJsb2NrcyA9IGN1ci5nZXRCbG9ja3MoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChnYW1lLl9jaGVja0NvbGxpc2lvbnMoeCwgeSArIDEsIGJsb2NrcywgdHJ1ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkcm9wID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGJsb2NrSW5kZXggPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY3VyLmJsb2Nrc0xlbjsgaSArPSAyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2ZpbGxlZC5hZGQoeCArIGJsb2Nrc1tpXSwgeSArIGJsb2Nrc1tpICsgMV0sIGN1ci5ibG9ja1R5cGUsIGN1ci5ibG9ja1ZhcmlhdGlvbiwgYmxvY2tJbmRleCwgY3VyLm9yaWVudGF0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHkgKyBibG9ja3NbaV0gPCAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lT3ZlciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmxvY2tJbmRleCsrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9maWxsZWQuY2hlY2tGb3JDbGVhcnMoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmN1ciA9IHRoaXMubmV4dFNoYXBlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZW5kZXJDaGFuZ2VkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gU3RvcCBob2xkaW5nIGRyb3AgKGFuZCBhbnkgb3RoZXIgYnV0dG9ucykuIEp1c3QgaW4gY2FzZSB0aGUgY29udHJvbHMgZ2V0IHN0aWNreS5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhvbGRpbmcubGVmdCA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ob2xkaW5nLnJpZ2h0ID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhvbGRpbmcuZHJvcCA9IG51bGw7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUub3B0aW9ucy5vblBsYWNlZC5jYWxsKGdhbWUuZWxlbWVudCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIERyb3BcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZHJvcCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtb3ZlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3VyLnkrKztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChkcm9wIHx8IG1vdmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVuZGVyQ2hhbmdlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoZ2FtZU92ZXIpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2FtZW92ZXIgPSB0cnVlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5nYW1lb3ZlcigpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGdhbWUub3B0aW9ucy5hdXRvcGxheSAmJiBnYW1lLm9wdGlvbnMuYXV0b3BsYXlSZXN0YXJ0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBPbiBhdXRvcGxheSwgcmVzdGFydCB0aGUgZ2FtZSBhdXRvbWF0aWNhbGx5XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLnJlc3RhcnQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlbmRlckNoYW5nZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMubGV2ZWxPdmVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChnYW1lLm9wdGlvbnMuYXV0b3BsYXkgJiYgZ2FtZS5vcHRpb25zLmF1dG9wbGF5UmVzdGFydCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gT24gYXV0b3BsYXksIHJlc3RhcnQgdGhlIGdhbWUgYXV0b21hdGljYWxseVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5yZXN0YXJ0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZW5kZXJDaGFuZ2VkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFVwZGF0ZSB0aGUgc3BlZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hbmltYXRlRGVsYXkgPSAxMDAwIC8gZ2FtZS5vcHRpb25zLnNwZWVkO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hbmltYXRlVGltZW91dElkID0gd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5hbmltYXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIHRoaXMuYW5pbWF0ZURlbGF5KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICAgICAgYW5pbWF0ZU9ic3RhY2xlOiBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuYW5pbWF0ZVRpbWVvdXRJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5hbmltYXRlVGltZW91dElkKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHZhciBjdXIgPSB0aGlzLmN1cixcclxuICAgICAgICAgICAgICAgICAgICAgICAgeCA9IGN1ci54LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB5ID0gY3VyLnksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJsb2NrcyA9IGN1ci5nZXRCbG9ja3MoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGJsb2NrSW5kZXggPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY3VyLmJsb2Nrc0xlbjsgaSArPSAyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2ZpbGxlZC5hZGQoeCArIGJsb2Nrc1tpXSwgeSArIGJsb2Nrc1tpICsgMV0sIGN1ci5ibG9ja1R5cGUsIGN1ci5ibG9ja1ZhcmlhdGlvbiwgYmxvY2tJbmRleCwgY3VyLm9yaWVudGF0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHkgKyBibG9ja3NbaV0gPCAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lT3ZlciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYmxvY2tJbmRleCsrO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBnYW1lLl9maWxsZWQuY2hlY2tGb3JDbGVhcnMoKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJlbmRlckNoYW5nZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBTdG9wIGhvbGRpbmcgZHJvcCAoYW5kIGFueSBvdGhlciBidXR0b25zKS4gSnVzdCBpbiBjYXNlIHRoZSBjb250cm9scyBnZXQgc3RpY2t5LlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaG9sZGluZy5sZWZ0ID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmhvbGRpbmcucmlnaHQgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaG9sZGluZy5kcm9wID0gbnVsbDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy9nYW1lLm9wdGlvbnMub25QbGFjZWQuY2FsbChnYW1lLmVsZW1lbnQpO1xyXG5cclxuICAgICAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICAgICAgY3JlYXRlUmFuZG9tQm9hcmQ6IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB2YXIgc3RhcnQgPSBbXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmxvY2tUeXBlcyA9IFtdLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpLCBpbGVuLCBqLCBqbGVuLCBibG9ja1R5cGU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIERyYXcgYSByYW5kb20gYmxvY2tyYWluIHNjcmVlblxyXG4gICAgICAgICAgICAgICAgICAgIGJsb2NrVHlwZXMgPSBPYmplY3Qua2V5cyhnYW1lLl9ub3JtYWxTaGFwZUZhY3RvcnkpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGkgPSAwLCBpbGVuID0gZ2FtZS5fQkxPQ0tfV0lEVEg7IGkgPCBpbGVuOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yIChqID0gMCwgamxlbiA9IGdhbWUuX3JhbmRDaG9pY2UoW2dhbWUuX3JhbmRJbnQoMCwgOCksIGdhbWUuX3JhbmRJbnQoNSwgOSldKTsgaiA8IGpsZW47IGorKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFibG9ja1R5cGUgfHwgIWdhbWUuX3JhbmRJbnQoMCwgMykpIGJsb2NrVHlwZSA9IGdhbWUuX3JhbmRDaG9pY2UoYmxvY2tUeXBlcyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gVXNlIGEgcmFuZG9tIHBpZWNlIGFuZCBvcmllbnRhdGlvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gVG9kbzogVXNlIGFuIGFjdHVhbCByYW5kb20gdmFyaWF0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9maWxsZWQuYWRkKGksIGdhbWUuX0JMT0NLX0hFSUdIVCAtIGosIGJsb2NrVHlwZSwgZ2FtZS5fcmFuZEludCgwLCAzKSwgbnVsbCwgZ2FtZS5fcmFuZEludCgwLCAzKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChpPTAsIGlsZW49V0lEVEg7IGk8aWxlbjsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICBmb3IgKGo9MCwgamxlbj1yYW5kQ2hvaWNlKFtyYW5kSW50KDAsIDgpLCByYW5kSW50KDUsIDkpXSk7IGo8amxlbjsgaisrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghYmxvY2tUeXBlIHx8ICFyYW5kSW50KDAsIDMpKSBibG9ja1R5cGUgPSByYW5kQ2hvaWNlKGJsb2NrVHlwZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdGFydC5wdXNoKFtpLCBIRUlHSFQgLSBqLCBibG9ja1R5cGVdKTtcclxuICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmKCBvcHRpb25zLnNob3dGaWVsZE9uU3RhcnQgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICBkcmF3QmFja2dyb3VuZCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgZm9yIChpPTAsIGlsZW49c3RhcnQubGVuZ3RoOyBpPGlsZW47IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkcmF3QmxvY2suYXBwbHkoZHJhd0Jsb2NrLCBzdGFydFtpXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICovXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLnJlbmRlcih0cnVlKTtcclxuXHJcbiAgICAgICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgICAgIHJlbmRlcjogZnVuY3Rpb24oZm9yY2VSZW5kZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5yZW5kZXJDaGFuZ2VkIHx8IGZvcmNlUmVuZGVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVuZGVyQ2hhbmdlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguY2xlYXJSZWN0KDAsIDAsIGdhbWUuX1BJWEVMX1dJRFRILCBnYW1lLl9QSVhFTF9IRUlHSFQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9kcmF3QmFja2dyb3VuZCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9maWxsZWQuZHJhdygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmN1ci5kcmF3KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgICAgICByZW5kZXJPYnN0YWNsZTogZnVuY3Rpb24oZm9yY2VSZW5kZXIsIHgsIHkpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5yZW5kZXJDaGFuZ2VkIHx8IGZvcmNlUmVuZGVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVuZGVyQ2hhbmdlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguY2xlYXJSZWN0KDAsIDAsIGdhbWUuX1BJWEVMX1dJRFRILCBnYW1lLl9QSVhFTF9IRUlHSFQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9kcmF3QmFja2dyb3VuZCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9maWxsZWQuZHJhdygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmN1ci5kcmF3T2JzdGFjbGUoeCwgeSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgICAgICAgICAgLyoqXHJcbiAgICAgICAgICAgICAgICAgKiBEcmF3cyBvbmUgYmxvY2sgKEVhY2ggcGllY2UgaXMgbWFkZSBvZiA0IGJsb2NrcylcclxuICAgICAgICAgICAgICAgICAqIFRoZSBibG9ja1R5cGUgaXMgdXNlZCB0byBkcmF3IGFueSBibG9jay4gXHJcbiAgICAgICAgICAgICAgICAgKiBUaGUgZmFsbGluZyBhdHRyaWJ1dGUgaXMgbmVlZGVkIHRvIGFwcGx5IGRpZmZlcmVudCBzdHlsZXMgZm9yIGZhbGxpbmcgYW5kIHBsYWNlZCBibG9ja3MuXHJcbiAgICAgICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgICAgIGRyYXdCbG9jazogZnVuY3Rpb24oeCwgeSwgYmxvY2tUeXBlLCBibG9ja1ZhcmlhdGlvbiwgYmxvY2tJbmRleCwgYmxvY2tSb3RhdGlvbiwgZmFsbGluZykge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBjb252ZXJ0IHggYW5kIHkgdG8gcGl4ZWxcclxuICAgICAgICAgICAgICAgICAgICB4ID0geCAqIGdhbWUuX2Jsb2NrX3NpemU7XHJcbiAgICAgICAgICAgICAgICAgICAgeSA9IHkgKiBnYW1lLl9ibG9ja19zaXplO1xyXG5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZmFsbGluZyA9IHR5cGVvZiBmYWxsaW5nID09PSAnYm9vbGVhbicgPyBmYWxsaW5nIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGJvcmRlcldpZHRoID0gZ2FtZS5fdGhlbWUuc3Ryb2tlV2lkdGg7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGJvcmRlckRpc3RhbmNlID0gTWF0aC5yb3VuZChnYW1lLl9ibG9ja19zaXplICogMC4yMyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIHNxdWFyZURpc3RhbmNlID0gTWF0aC5yb3VuZChnYW1lLl9ibG9ja19zaXplICogMC4zMCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHZhciBjb2xvciA9IHRoaXMuZ2V0QmxvY2tDb2xvcihibG9ja1R5cGUsIGJsb2NrVmFyaWF0aW9uLCBibG9ja0luZGV4LCBmYWxsaW5nKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gRHJhdyB0aGUgbWFpbiBzcXVhcmVcclxuICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguZ2xvYmFsQWxwaGEgPSAxLjA7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIElmIGl0J3MgYW4gaW1hZ2UsIHRoZSBibG9jayBoYXMgYSBzcGVjaWZpYyB0ZXh0dXJlLiBVc2UgdGhhdC5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoY29sb3IgaW5zdGFuY2VvZiBJbWFnZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguZ2xvYmFsQWxwaGEgPSAxLjA7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBOb3QgbG9hZGVkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjb2xvci53aWR0aCA9PT0gMCB8fCBjb2xvci5oZWlnaHQgPT09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQSBzcXVhcmUgaXMgdGhlIHNhbWUgc3R5bGUgZm9yIGFsbCBibG9ja3NcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBnYW1lLl90aGVtZS5ibG9ja3MgIT09ICd1bmRlZmluZWQnICYmIGdhbWUuX3RoZW1lLmJsb2NrcyAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fY3R4LmRyYXdJbWFnZShjb2xvciwgMCwgMCwgY29sb3Iud2lkdGgsIGNvbG9yLmhlaWdodCwgeCwgeSwgZ2FtZS5fYmxvY2tfc2l6ZSwgZ2FtZS5fYmxvY2tfc2l6ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQSBjdXN0b20gdGV4dHVyZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbHNlIGlmICh0eXBlb2YgZ2FtZS5fdGhlbWUuY29tcGxleEJsb2NrcyAhPT0gJ3VuZGVmaW5lZCcgJiYgZ2FtZS5fdGhlbWUuY29tcGxleEJsb2NrcyAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBibG9ja0luZGV4ID09PSAndW5kZWZpbmVkJyB8fCBibG9ja0luZGV4ID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmxvY2tJbmRleCA9IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGdldEN1c3RvbUJsb2NrSW1hZ2VDb29yZGluYXRlcyA9IGZ1bmN0aW9uKGltYWdlLCBibG9ja1R5cGUsIGJsb2NrSW5kZXgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBUaGUgaW1hZ2UgaXMgYmFzZWQgb24gdGhlIGZpcnN0IChcInVwcmlnaHRcIikgb3JpZW50YXRpb25cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcG9zaXRpb25zID0gZ2FtZS5fc2hhcGVzW2Jsb2NrVHlwZV1bMF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gRmluZCB0aGUgbnVtYmVyIG9mIHRpbGVzIGl0IHNob3VsZCBoYXZlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG1pblggPSBNYXRoLm1pbihwb3NpdGlvbnNbMF0sIHBvc2l0aW9uc1syXSwgcG9zaXRpb25zWzRdLCBwb3NpdGlvbnNbNl0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBtYXhYID0gTWF0aC5tYXgocG9zaXRpb25zWzBdLCBwb3NpdGlvbnNbMl0sIHBvc2l0aW9uc1s0XSwgcG9zaXRpb25zWzZdKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgbWluWSA9IE1hdGgubWluKHBvc2l0aW9uc1sxXSwgcG9zaXRpb25zWzNdLCBwb3NpdGlvbnNbNV0sIHBvc2l0aW9uc1s3XSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG1heFkgPSBNYXRoLm1heChwb3NpdGlvbnNbMV0sIHBvc2l0aW9uc1szXSwgcG9zaXRpb25zWzVdLCBwb3NpdGlvbnNbN10pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciByYW5nZVggPSBtYXhYIC0gbWluWCArIDE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHJhbmdlWSA9IG1heFkgLSBtaW5ZICsgMTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gWCBhbmQgWSBzaXplcyBzaG91bGQgbWF0Y2guIFNob3VsZC5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdGlsZVNpemVYID0gaW1hZ2Uud2lkdGggLyByYW5nZVg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRpbGVTaXplWSA9IGltYWdlLmhlaWdodCAvIHJhbmdlWTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeDogdGlsZVNpemVYICogKHBvc2l0aW9uc1tibG9ja0luZGV4ICogMl0gLSBtaW5YKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeTogdGlsZVNpemVZICogTWF0aC5hYnMobWluWSAtIHBvc2l0aW9uc1tibG9ja0luZGV4ICogMiArIDFdKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdzogdGlsZVNpemVYLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoOiB0aWxlU2l6ZVlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgY29vcmRzID0gZ2V0Q3VzdG9tQmxvY2tJbWFnZUNvb3JkaW5hdGVzKGNvbG9yLCBibG9ja1R5cGUsIGJsb2NrSW5kZXgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2N0eC5zYXZlKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fY3R4LnRyYW5zbGF0ZSh4LCB5KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2N0eC50cmFuc2xhdGUoZ2FtZS5fYmxvY2tfc2l6ZSAvIDIsIGdhbWUuX2Jsb2NrX3NpemUgLyAyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2N0eC5yb3RhdGUoLU1hdGguUEkgLyAyICogYmxvY2tSb3RhdGlvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguZHJhd0ltYWdlKGNvbG9yLCBjb29yZHMueCwgY29vcmRzLnksIGNvb3Jkcy53LCBjb29yZHMuaCwgLWdhbWUuX2Jsb2NrX3NpemUgLyAyLCAtZ2FtZS5fYmxvY2tfc2l6ZSAvIDIsIGdhbWUuX2Jsb2NrX3NpemUsIGdhbWUuX2Jsb2NrX3NpemUpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2N0eC5yZXN0b3JlKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gRVJST1JcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2N0eC5maWxsU3R5bGUgPSAnI2ZmMDAwMCc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguZmlsbFJlY3QoeCwgeSwgZ2FtZS5fYmxvY2tfc2l6ZSwgZ2FtZS5fYmxvY2tfc2l6ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiBjb2xvciA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fY3R4LmZpbGxTdHlsZSA9IGNvbG9yO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguZmlsbFJlY3QoeCwgeSwgZ2FtZS5fYmxvY2tfc2l6ZSwgZ2FtZS5fYmxvY2tfc2l6ZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBJbm5lciBTaGFkb3dcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBnYW1lLl90aGVtZS5pbm5lclNoYWRvdyA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2N0eC5nbG9iYWxBbHBoYSA9IDEuMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2N0eC5zdHJva2VTdHlsZSA9IGdhbWUuX3RoZW1lLmlubmVyU2hhZG93O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fY3R4LmxpbmVXaWR0aCA9IDEuMDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBEcmF3IHRoZSBib3JkZXJzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguc3Ryb2tlUmVjdCh4ICsgMSwgeSArIDEsIGdhbWUuX2Jsb2NrX3NpemUgLSAyLCBnYW1lLl9ibG9ja19zaXplIC0gMik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIERlY29yYXRpb24gKGJvcmRlcnMpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgZ2FtZS5fdGhlbWUuc3Ryb2tlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fY3R4Lmdsb2JhbEFscGhhID0gMS4wO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZS5fY3R4LmZpbGxTdHlsZSA9IGdhbWUuX3RoZW1lLnN0cm9rZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2N0eC5zdHJva2VTdHlsZSA9IGdhbWUuX3RoZW1lLnN0cm9rZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2N0eC5saW5lV2lkdGggPSBib3JkZXJXaWR0aDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBEcmF3IHRoZSBib3JkZXJzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguc3Ryb2tlUmVjdCh4LCB5LCBnYW1lLl9ibG9ja19zaXplLCBnYW1lLl9ibG9ja19zaXplKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGdhbWUuX3RoZW1lLmlubmVyU3Ryb2tlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gRHJhdyB0aGUgaW5uZXIgZGFzaGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguZmlsbFN0eWxlID0gZ2FtZS5fdGhlbWUuaW5uZXJTdHJva2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguZmlsbFJlY3QoeCArIGJvcmRlckRpc3RhbmNlLCB5ICsgYm9yZGVyRGlzdGFuY2UsIGdhbWUuX2Jsb2NrX3NpemUgLSBib3JkZXJEaXN0YW5jZSAqIDIsIGJvcmRlcldpZHRoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIFRoZSByZWN0cyBzaG91bGRuJ3Qgb3ZlcmxhcCwgdG8gcHJldmVudCBpc3N1ZXMgd2l0aCB0cmFuc3BhcmVuY3lcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2N0eC5maWxsUmVjdCh4ICsgYm9yZGVyRGlzdGFuY2UsIHkgKyBib3JkZXJEaXN0YW5jZSArIGJvcmRlcldpZHRoLCBib3JkZXJXaWR0aCwgZ2FtZS5fYmxvY2tfc2l6ZSAtIGJvcmRlckRpc3RhbmNlICogMiAtIGJvcmRlcldpZHRoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGdhbWUuX3RoZW1lLmlubmVyU3F1YXJlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gRHJhdyB0aGUgaW5uZXIgc3F1YXJlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguZmlsbFN0eWxlID0gZ2FtZS5fdGhlbWUuaW5uZXJTcXVhcmU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguZ2xvYmFsQWxwaGEgPSAwLjI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9jdHguZmlsbFJlY3QoeCArIHNxdWFyZURpc3RhbmNlLCB5ICsgc3F1YXJlRGlzdGFuY2UsIGdhbWUuX2Jsb2NrX3NpemUgLSBzcXVhcmVEaXN0YW5jZSAqIDIsIGdhbWUuX2Jsb2NrX3NpemUgLSBzcXVhcmVEaXN0YW5jZSAqIDIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBSZXR1cm4gdGhlIGFscGhhIGJhY2sgdG8gMS4wIHNvIHdlIGRvbid0IGNyZWF0ZSBhbnkgaXNzdWVzIHdpdGggb3RoZXIgZHJhd2luZ3MuXHJcbiAgICAgICAgICAgICAgICAgICAgZ2FtZS5fY3R4Lmdsb2JhbEFscGhhID0gMS4wO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgICAgICAgICAgZ2V0QmxvY2tDb2xvcjogZnVuY3Rpb24oYmxvY2tUeXBlLCBibG9ja1ZhcmlhdGlvbiwgYmxvY2tJbmRleCwgZmFsbGluZykge1xyXG4gICAgICAgICAgICAgICAgICAgIC8qKlxyXG4gICAgICAgICAgICAgICAgICAgICAqIFRoZSB0aGVtZSBhbGxvd3MgdXMgdG8gZG8gbWFueSB0aGluZ3M6XHJcbiAgICAgICAgICAgICAgICAgICAgICogLSBVc2UgYSBzcGVjaWZpYyBjb2xvciBmb3IgdGhlIGZhbGxpbmcgYmxvY2sgKHByaW1hcnkpLCByZWdhcmRsZXNzIG9mIHRoZSBwcm9wZXIgY29sb3IuXHJcbiAgICAgICAgICAgICAgICAgICAgICogLSBVc2UgYW5vdGhlciBjb2xvciBmb3IgdGhlIHBsYWNlZCBibG9ja3MgKHNlY29uZGFyeSkuXHJcbiAgICAgICAgICAgICAgICAgICAgICogLSBEZWZhdWx0IHRvIHRoZSBcIm9yaWdpbmFsXCIgYmxvY2sgY29sb3IgaW4gYW55IG9mIHRob3NlIGNhc2VzIGJ5IHNldHRpbmcgcHJpbWFyeSBhbmQvb3Igc2Vjb25kYXJ5IHRvIG51bGwuXHJcbiAgICAgICAgICAgICAgICAgICAgICogLSBXaXRoIHByaW1hcnkgYW5kIHNlY29uZGFyeSBhcyBudWxsLCBhbGwgYmxvY2tzIGtlZXAgdGhlaXIgb3JpZ2luYWwgY29sb3JzLlxyXG4gICAgICAgICAgICAgICAgICAgICAqL1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB2YXIgZ2V0QmxvY2tWYXJpYXRpb24gPSBmdW5jdGlvbihibG9ja1RoZW1lLCBibG9ja1ZhcmlhdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoJC5pc0FycmF5KGJsb2NrVGhlbWUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYmxvY2tWYXJpYXRpb24gIT09IG51bGwgJiYgdHlwZW9mIGJsb2NrVGhlbWVbYmxvY2tWYXJpYXRpb25dICE9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBibG9ja1RoZW1lW2Jsb2NrVmFyaWF0aW9uXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoYmxvY2tUaGVtZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGJsb2NrVGhlbWVbMF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGJsb2NrVGhlbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgZmFsbGluZyAhPT0gJ2Jvb2xlYW4nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZhbGxpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAoZmFsbGluZykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGdhbWUuX3RoZW1lLnByaW1hcnkgPT09ICdzdHJpbmcnICYmIGdhbWUuX3RoZW1lLnByaW1hcnkgIT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZ2FtZS5fdGhlbWUucHJpbWFyeTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0eXBlb2YgZ2FtZS5fdGhlbWUuYmxvY2tzICE9PSAndW5kZWZpbmVkJyAmJiBnYW1lLl90aGVtZS5ibG9ja3MgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBnZXRCbG9ja1ZhcmlhdGlvbihnYW1lLl90aGVtZS5ibG9ja3NbYmxvY2tUeXBlXSwgYmxvY2tWYXJpYXRpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGdldEJsb2NrVmFyaWF0aW9uKGdhbWUuX3RoZW1lLmNvbXBsZXhCbG9ja3NbYmxvY2tUeXBlXSwgYmxvY2tWYXJpYXRpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBnYW1lLl90aGVtZS5zZWNvbmRhcnkgPT09ICdzdHJpbmcnICYmIGdhbWUuX3RoZW1lLnNlY29uZGFyeSAhPT0gJycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBnYW1lLl90aGVtZS5zZWNvbmRhcnk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGdhbWUuX3RoZW1lLmJsb2NrcyAhPT0gJ3VuZGVmaW5lZCcgJiYgZ2FtZS5fdGhlbWUuYmxvY2tzICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZ2V0QmxvY2tWYXJpYXRpb24oZ2FtZS5fdGhlbWUuYmxvY2tzW2Jsb2NrVHlwZV0sIGJsb2NrVmFyaWF0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBnZXRCbG9ja1ZhcmlhdGlvbihnYW1lLl90aGVtZS5jb21wbGV4QmxvY2tzW2Jsb2NrVHlwZV0sIGJsb2NrVmFyaWF0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBnYW1lLl9uaWNlU2hhcGVzID0gZ2FtZS5fZ2V0TmljZVNoYXBlcygpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIC8vIFV0aWxpdHkgRnVuY3Rpb25zXHJcbiAgICAgICAgX3JhbmRJbnQ6IGZ1bmN0aW9uKGEsIGIpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGEgKyBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAoMSArIGIgLSBhKSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBfcmFuZFNpZ246IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fcmFuZEludCgwLCAxKSAqIDIgLSAxO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgX3JhbmRDaG9pY2U6IGZ1bmN0aW9uKGNob2ljZXMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGNob2ljZXNbdGhpcy5fcmFuZEludCgwLCBjaG9pY2VzLmxlbmd0aCAtIDEpXTtcclxuICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogRmluZCBiYXNlNjQgZW5jb2RlZCBpbWFnZXMgYW5kIGxvYWQgdGhlbSBhcyBpbWFnZSBvYmplY3RzLCB3aGljaCBjYW4gYmUgdXNlZCBieSB0aGUgY2FudmFzIHJlbmRlcmVyXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgX3ByZWxvYWRUaGVtZUFzc2V0czogZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICAgICB2YXIgZ2FtZSA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICB2YXIgaGV4Q29sb3JjaGVjayA9IG5ldyBSZWdFeHAoJ14jW0EtRjAtOStdezMsNn0nLCAnaScpO1xyXG4gICAgICAgICAgICB2YXIgYmFzZTY0Y2hlY2sgPSBuZXcgUmVnRXhwKCdeZGF0YTppbWFnZS8ocG5nfGdpZnxqcGcpO2Jhc2U2NCwnLCAnaScpO1xyXG5cclxuICAgICAgICAgICAgdmFyIGhhbmRsZUFzc2V0TG9hZCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgLy8gUmVyZW5kZXIgdGhlIGJvYXJkIGFzIHNvb24gYXMgYW4gYXNzZXQgbG9hZHNcclxuICAgICAgICAgICAgICAgIGlmIChnYW1lLl9ib2FyZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLnJlbmRlcih0cnVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIHZhciBsb2FkQXNzZXQgPSBmdW5jdGlvbihzcmMpIHtcclxuICAgICAgICAgICAgICAgIHZhciBwbGFpblNyYyA9IHNyYztcclxuICAgICAgICAgICAgICAgIGlmICghaGV4Q29sb3JjaGVjay50ZXN0KHBsYWluU3JjKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIEl0J3MgYW4gaW1hZ2VcclxuICAgICAgICAgICAgICAgICAgICBzcmMgPSBuZXcgSW1hZ2UoKTtcclxuICAgICAgICAgICAgICAgICAgICBzcmMuc3JjID0gcGxhaW5TcmM7XHJcbiAgICAgICAgICAgICAgICAgICAgc3JjLm9ubG9hZCA9IGhhbmRsZUFzc2V0TG9hZDtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gSXQncyBhIGNvbG9yXHJcbiAgICAgICAgICAgICAgICAgICAgc3JjID0gcGxhaW5TcmM7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gc3JjO1xyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgdmFyIHN0YXJ0QXNzZXRMb2FkID0gZnVuY3Rpb24oYmxvY2spIHtcclxuICAgICAgICAgICAgICAgIC8vIEFzc2V0cyBjYW4gYmUgYW4gYXJyYXkgb2YgdmFyaWF0aW9uIHNvIHRoZXkgY2FuIGNoYW5nZSBjb2xvci9kZXNpZ24gcmFuZG9tbHlcclxuICAgICAgICAgICAgICAgIGlmICgkLmlzQXJyYXkoYmxvY2spICYmIGJsb2NrLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGJsb2NrLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJsb2NrW2ldID0gbG9hZEFzc2V0KGJsb2NrW2ldKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiBibG9jayA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICAgICAgICBibG9jayA9IGxvYWRBc3NldChibG9jayk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYmxvY2s7XHJcbiAgICAgICAgICAgIH07XHJcblxyXG5cclxuICAgICAgICAgICAgaWYgKHR5cGVvZiB0aGlzLl90aGVtZS5jb21wbGV4QmxvY2tzICE9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICAgICAgdmFyIGtleXMgPSBPYmplY3Qua2V5cyh0aGlzLl90aGVtZS5jb21wbGV4QmxvY2tzKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBMb2FkIHRoZSBjb21wbGV4QmxvY2tzXHJcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGtleXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl90aGVtZS5jb21wbGV4QmxvY2tzW2tleXNbaV1dID0gc3RhcnRBc3NldExvYWQodGhpcy5fdGhlbWUuY29tcGxleEJsb2Nrc1trZXlzW2ldXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHRoaXMuX3RoZW1lLmJsb2NrcyAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgICAgIHZhciBrZXlzID0gT2JqZWN0LmtleXModGhpcy5fdGhlbWUuYmxvY2tzKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBMb2FkIHRoZSBibG9ja3NcclxuICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwga2V5cy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3RoZW1lLmJsb2Nrc1trZXlzW2ldXSA9IHN0YXJ0QXNzZXRMb2FkKHRoaXMuX3RoZW1lLmJsb2Nrc1trZXlzW2ldXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIExvYWQgdGhlIGJnXHJcbiAgICAgICAgICAgIGlmICh0eXBlb2YgdGhpcy5fdGhlbWUuYmFja2dyb3VuZEdyaWQgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHRoaXMuX3RoZW1lLmJhY2tncm91bmRHcmlkID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghaGV4Q29sb3JjaGVjay50ZXN0KHRoaXMuX3RoZW1lLmJhY2tncm91bmRHcmlkKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgc3JjID0gdGhpcy5fdGhlbWUuYmFja2dyb3VuZEdyaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3RoZW1lLmJhY2tncm91bmRHcmlkID0gbmV3IEltYWdlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3RoZW1lLmJhY2tncm91bmRHcmlkLnNyYyA9IHNyYztcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fdGhlbWUuYmFja2dyb3VuZEdyaWQub25sb2FkID0gaGFuZGxlQXNzZXRMb2FkO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgX2NyZWF0ZUhvbGRlcjogZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICAgICAvLyBDcmVhdGUgdGhlIG1haW4gaG9sZGVyIChpdCBob2xkcyBhbGwgdGhlIHVpIGVsZW1lbnRzLCB0aGUgb3JpZ2luYWwgZWxlbWVudCBpcyBqdXN0IHRoZSB3cmFwcGVyKVxyXG4gICAgICAgICAgICB0aGlzLl8kZ2FtZWhvbGRlciA9ICQoJzxkaXYgY2xhc3M9XCJibG9ja3JhaW4tZ2FtZS1ob2xkZXJcIj48L2Rpdj4nKTtcclxuICAgICAgICAgICAgdGhpcy5fJGdhbWVob2xkZXIuY3NzKCdwb3NpdGlvbicsICdyZWxhdGl2ZScpLmNzcygnd2lkdGgnLCAnMTAwJScpLmNzcygnaGVpZ2h0JywgJzEwMCUnKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5odG1sKCcnKS5hcHBlbmQodGhpcy5fJGdhbWVob2xkZXIpO1xyXG5cclxuICAgICAgICAgICAgLy8gQ3JlYXRlIHRoZSBnYW1lIGNhbnZhcyBhbmQgY29udGV4dFxyXG4gICAgICAgICAgICB0aGlzLl8kY2FudmFzID0gJCgnPGNhbnZhcyBzdHlsZT1cImRpc3BsYXk6YmxvY2s7IHdpZHRoOjEwMCU7IGhlaWdodDoxMDAlOyBwYWRkaW5nOjA7IG1hcmdpbjowOyBib3JkZXI6bm9uZTtcIiAvPicpO1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIHRoaXMuX3RoZW1lLmJhY2tncm91bmQgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl8kY2FudmFzLmNzcygnYmFja2dyb3VuZC1jb2xvcicsIHRoaXMuX3RoZW1lLmJhY2tncm91bmQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuXyRnYW1laG9sZGVyLmFwcGVuZCh0aGlzLl8kY2FudmFzKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX2NhbnZhcyA9IHRoaXMuXyRjYW52YXMuZ2V0KDApO1xyXG4gICAgICAgICAgICB0aGlzLl9jdHggPSB0aGlzLl9jYW52YXMuZ2V0Q29udGV4dCgnMmQnKTtcclxuXHJcbiAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgIF9jcmVhdGVVSTogZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICAgICB2YXIgZ2FtZSA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICAvLyBTY29yZVxyXG4gICAgICAgICAgICBnYW1lLl8kc2NvcmUgPSAkKFxyXG4gICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJibG9ja3JhaW4tc2NvcmUtaG9sZGVyXCIgc3R5bGU9XCJwb3NpdGlvbjphYnNvbHV0ZTtcIj4nICtcclxuICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwiYmxvY2tyYWluLXNjb3JlXCI+JyArXHJcbiAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cImJsb2NrcmFpbi1zY29yZS1tc2dcIj4nICsgdGhpcy5vcHRpb25zLnNjb3JlVGV4dCArICc8L2Rpdj4nICtcclxuICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwiYmxvY2tyYWluLXNjb3JlLW51bVwiPjA8L2Rpdj4nICtcclxuICAgICAgICAgICAgICAgICc8L2Rpdj4nICtcclxuICAgICAgICAgICAgICAgICc8L2Rpdj4nKS5oaWRlKCk7XHJcbiAgICAgICAgICAgIGdhbWUuXyRzY29yZVRleHQgPSBnYW1lLl8kc2NvcmUuZmluZCgnLmJsb2NrcmFpbi1zY29yZS1udW0nKTtcclxuICAgICAgICAgICAgZ2FtZS5fJGdhbWVob2xkZXIuYXBwZW5kKGdhbWUuXyRzY29yZSk7XHJcblxyXG4gICAgICAgICAgICAvLyBDcmVhdGUgdGhlIHN0YXJ0IG1lbnVcclxuICAgICAgICAgICAgZ2FtZS5fJHN0YXJ0ID0gJChcclxuICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwiYmxvY2tyYWluLXN0YXJ0LWhvbGRlclwiIHN0eWxlPVwicG9zaXRpb246YWJzb2x1dGU7XCI+JyArXHJcbiAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cImJsb2NrcmFpbi1zdGFydFwiPicgK1xyXG4gICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJibG9ja3JhaW4tc3RhcnQtbXNnXCI+JyArIHRoaXMub3B0aW9ucy5wbGF5VGV4dCArICc8L2Rpdj4nICtcclxuICAgICAgICAgICAgICAgICc8YSBjbGFzcz1cImJsb2NrcmFpbi1idG4gYmxvY2tyYWluLXN0YXJ0LWJ0blwiPicgKyB0aGlzLm9wdGlvbnMucGxheUJ1dHRvblRleHQgKyAnPC9hPicgK1xyXG4gICAgICAgICAgICAgICAgJzwvZGl2PicgK1xyXG4gICAgICAgICAgICAgICAgJzwvZGl2PicpLmhpZGUoKTtcclxuICAgICAgICAgICAgZ2FtZS5fJGdhbWVob2xkZXIuYXBwZW5kKGdhbWUuXyRzdGFydCk7XHJcblxyXG4gICAgICAgICAgICBnYW1lLl8kc3RhcnQuZmluZCgnLmJsb2NrcmFpbi1zdGFydC1idG4nKS5jbGljayhmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgICAgIGdhbWUuc3RhcnQoKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG5cclxuICAgICAgICAgICAgZ2FtZS5fJHBhdXNlID0gJCgnPGEgaWQ9XCJwYXVzZUJ1dHRvblwiIGNsYXNzPVwiYmxvY2tyYWluLXBhdXNlLWJ0blwiPjwvYT4nKS5oaWRlKCk7XHJcbiAgICAgICAgICAgIGdhbWUuXyRnYW1laG9sZGVyLmFwcGVuZChnYW1lLl8kcGF1c2UpO1xyXG5cclxuICAgICAgICAgICAgZ2FtZS5fJHBhdXNlLmNsaWNrKGZ1bmN0aW9uKGV2ZW50KSB7XHJcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgZ2FtZS5wYXVzZVJlc3VtZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcblxyXG4gICAgICAgICAgICAvLyBDcmVhdGUgdGhlIGdhbWUgb3ZlciBtZW51XHJcbiAgICAgICAgICAgIGdhbWUuXyRnYW1lb3ZlciA9ICQoXHJcbiAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cImJsb2NrcmFpbi1nYW1lLW92ZXItaG9sZGVyXCIgc3R5bGU9XCJwb3NpdGlvbjphYnNvbHV0ZTtcIj4nICtcclxuICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwiYmxvY2tyYWluLWdhbWUtb3ZlclwiPicgK1xyXG4gICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJibG9ja3JhaW4tZ2FtZS1vdmVyLW1zZ1wiPicgKyB0aGlzLm9wdGlvbnMuZ2FtZU92ZXJUZXh0ICsgJzwvZGl2PicgK1xyXG4gICAgICAgICAgICAgICAgJzxhIGNsYXNzPVwiYmxvY2tyYWluLWJ0biBibG9ja3JhaW4tZ2FtZS1vdmVyLWJ0blwiPicgKyB0aGlzLm9wdGlvbnMucmVzdGFydEJ1dHRvblRleHQgKyAnPC9hPicgK1xyXG4gICAgICAgICAgICAgICAgJzwvZGl2PicgK1xyXG4gICAgICAgICAgICAgICAgJzwvZGl2PicpLmhpZGUoKTtcclxuICAgICAgICAgICAgZ2FtZS5fJGdhbWVvdmVyLmZpbmQoJy5ibG9ja3JhaW4tZ2FtZS1vdmVyLWJ0bicpLmNsaWNrKGZ1bmN0aW9uKGV2ZW50KSB7XHJcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgZ2FtZS5yZXN0YXJ0KCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBnYW1lLl8kZ2FtZWhvbGRlci5hcHBlbmQoZ2FtZS5fJGdhbWVvdmVyKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX2NyZWF0ZUNvbnRyb2xzKCk7XHJcbiAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgIF9jcmVhdGVDb250cm9sczogZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICAgICB2YXIgZ2FtZSA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICBnYW1lLl8kdG91Y2hMZWZ0ID0gJCgnPGEgY2xhc3M9XCJibG9ja3JhaW4tdG91Y2ggYmxvY2tyYWluLXRvdWNoLWxlZnRcIiAvPicpLmFwcGVuZFRvKGdhbWUuXyRnYW1laG9sZGVyKTtcclxuICAgICAgICAgICAgZ2FtZS5fJHRvdWNoUmlnaHQgPSAkKCc8YSBjbGFzcz1cImJsb2NrcmFpbi10b3VjaCBibG9ja3JhaW4tdG91Y2gtcmlnaHRcIiAvPicpLmFwcGVuZFRvKGdhbWUuXyRnYW1laG9sZGVyKTtcclxuICAgICAgICAgICAgZ2FtZS5fJHRvdWNoUm90YXRlUmlnaHQgPSAkKCc8YSBjbGFzcz1cImJsb2NrcmFpbi10b3VjaCBibG9ja3JhaW4tdG91Y2gtcm90YXRlLXJpZ2h0XCIgLz4nKS5hcHBlbmRUbyhnYW1lLl8kZ2FtZWhvbGRlcik7XHJcbiAgICAgICAgICAgIGdhbWUuXyR0b3VjaFJvdGF0ZUxlZnQgPSAkKCc8YSBjbGFzcz1cImJsb2NrcmFpbi10b3VjaCBibG9ja3JhaW4tdG91Y2gtcm90YXRlLWxlZnRcIiAvPicpLmFwcGVuZFRvKGdhbWUuXyRnYW1laG9sZGVyKTtcclxuICAgICAgICAgICAgZ2FtZS5fJHRvdWNoRHJvcCA9ICQoJzxhIGNsYXNzPVwiYmxvY2tyYWluLXRvdWNoIGJsb2NrcmFpbi10b3VjaC1kcm9wXCIgLz4nKS5hcHBlbmRUbyhnYW1lLl8kZ2FtZWhvbGRlcik7XHJcbiAgICAgICAgICAgIGdhbWUuXyR0b3VjaERyb3BTcGFjZSA9ICQoJzxhIGNsYXNzPVwiYmxvY2tyYWluLXRvdWNoIGJsb2NrcmFpbi10b3VjaC1kcm9wLXNwYWNlXCIgLz4nKS5hcHBlbmRUbyhnYW1lLl8kZ2FtZWhvbGRlcik7XHJcbiAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgIF9yZWZyZXNoQmxvY2tTaXplczogZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmF1dG9CbG9ja1dpZHRoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMuYmxvY2tXaWR0aCA9IE1hdGguY2VpbCh0aGlzLmVsZW1lbnQud2lkdGgoKSAvIHRoaXMub3B0aW9ucy5hdXRvQmxvY2tTaXplKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgX2dldE5pY2VTaGFwZXM6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICAgKiBUaGluZ3MgSSBuZWVkIGZvciB0aGlzIHRvIHdvcmsuLi5cclxuICAgICAgICAgICAgICogIC0gYWJpbGl0eSB0byB0ZXN0IGVhY2ggc2hhcGUgd2l0aCB0aGlzLl9maWxsZWQgZGF0YVxyXG4gICAgICAgICAgICAgKiAgLSBtYXliZSBnaXZlIGVtcHR5IHNwb3RzIHNjb3Jlcz8gYW5kIHRyeSB0byBtYXhpbWl6ZSB0aGUgc2NvcmU/XHJcbiAgICAgICAgICAgICAqL1xyXG5cclxuICAgICAgICAgICAgdmFyIGdhbWUgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgdmFyIHNoYXBlcyA9IHt9LFxyXG4gICAgICAgICAgICAgICAgYXR0cjtcclxuXHJcbiAgICAgICAgICAgIGZvciAodmFyIGF0dHIgaW4gdGhpcy5fc2hhcGVGYWN0b3J5KSB7XHJcbiAgICAgICAgICAgICAgICBzaGFwZXNbYXR0cl0gPSB0aGlzLl9zaGFwZUZhY3RvcnlbYXR0cl0oKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gc2NvcmVCbG9ja3MocG9zc2libGVzLCBibG9ja3MsIHgsIHksIGZpbGxlZCwgd2lkdGgsIGhlaWdodCkge1xyXG4gICAgICAgICAgICAgICAgdmFyIGksIGxlbiA9IGJsb2Nrcy5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICAgICAgc2NvcmUgPSAwLFxyXG4gICAgICAgICAgICAgICAgICAgIGJvdHRvbXMgPSB7fSxcclxuICAgICAgICAgICAgICAgICAgICB0eCwgdHksIG92ZXJsYXBzO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIGJhc2Ugc2NvcmVcclxuICAgICAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBsZW47IGkgKz0gMikge1xyXG4gICAgICAgICAgICAgICAgICAgIHNjb3JlICs9IHBvc3NpYmxlc1tnYW1lLl9maWxsZWQuYXNJbmRleCh4ICsgYmxvY2tzW2ldLCB5ICsgYmxvY2tzW2kgKyAxXSldIHx8IDA7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gb3ZlcmxhcCBzY29yZSAtLSAvL1RPRE8gLSBkb24ndCBjb3VudCBvdmVybGFwcyBpZiBjbGVhcmVkP1xyXG4gICAgICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IGxlbjsgaSArPSAyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdHggPSBibG9ja3NbaV07XHJcbiAgICAgICAgICAgICAgICAgICAgdHkgPSBibG9ja3NbaSArIDFdO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChib3R0b21zW3R4XSA9PT0gdW5kZWZpbmVkIHx8IGJvdHRvbXNbdHhdIDwgdHkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm90dG9tc1t0eF0gPSB0eTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBvdmVybGFwcyA9IDA7XHJcbiAgICAgICAgICAgICAgICBmb3IgKHR4IGluIGJvdHRvbXMpIHtcclxuICAgICAgICAgICAgICAgICAgICB0eCA9IHBhcnNlSW50KHR4KTtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKHR5ID0gYm90dG9tc1t0eF0gKyAxLCBpID0gMDsgeSArIHR5IDwgaGVpZ2h0OyB0eSsrLCBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFnYW1lLl9maWxsZWQuY2hlY2soeCArIHR4LCB5ICsgdHkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdmVybGFwcyArPSBpID09IDAgPyAyIDogMTsgLy9UT0RPLXNjb3JlIGJldHRlclxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9pZiAoaSA9PSAwKSBvdmVybGFwcyArPSAxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgc2NvcmUgPSBzY29yZSAtIG92ZXJsYXBzO1xyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiBzY29yZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gcmVzZXRTaGFwZXMoKSB7XHJcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBhdHRyIGluIHNoYXBlcykge1xyXG4gICAgICAgICAgICAgICAgICAgIHNoYXBlc1thdHRyXS54ID0gMDtcclxuICAgICAgICAgICAgICAgICAgICBzaGFwZXNbYXR0cl0ueSA9IC0xO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvL1RPRE8gLS0gZXZpbCBtb2RlIG5lZWRzIHRvIHJlYWxpemUgdGhhdCBvdmVybGFwIGlzIGJhZC4uLlxyXG4gICAgICAgICAgICB2YXIgZnVuYyA9IGZ1bmN0aW9uKGZpbGxlZCwgY2hlY2tDb2xsaXNpb25zLCB3aWR0aCwgaGVpZ2h0LCBtb2RlLCBfb25lX3NoYXBlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIV9vbmVfc2hhcGUpIHJlc2V0U2hhcGVzKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgdmFyIHBvc3NpYmxlcyA9IG5ldyBBcnJheSh3aWR0aCAqIGhlaWdodCksXHJcbiAgICAgICAgICAgICAgICAgICAgZXZpbCA9IG1vZGUgPT0gJ2V2aWwnLFxyXG4gICAgICAgICAgICAgICAgICAgIHgsIHksIHB5LFxyXG4gICAgICAgICAgICAgICAgICAgIGF0dHIsIHNoYXBlLCBpLCBibG9ja3MsIGJvdW5kcyxcclxuICAgICAgICAgICAgICAgICAgICBzY29yZSwgYmVzdF9zaGFwZSwgYmVzdF9zY29yZSA9IChldmlsID8gMSA6IC0xKSAqIDk5OSxcclxuICAgICAgICAgICAgICAgICAgICBiZXN0X29yaWVudGF0aW9uLCBiZXN0X3gsXHJcbiAgICAgICAgICAgICAgICAgICAgYmVzdF9zY29yZV9mb3Jfc2hhcGUsIGJlc3Rfb3JpZW50YXRpb25fZm9yX3NoYXBlLCBiZXN0X3hfZm9yX3NoYXBlO1xyXG5cclxuICAgICAgICAgICAgICAgIGZvciAoeCA9IDA7IHggPCB3aWR0aDsgeCsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yICh5ID0gMDsgeSA8PSBoZWlnaHQ7IHkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoeSA9PSBoZWlnaHQgfHwgZmlsbGVkLmNoZWNrKHgsIHkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgKHB5ID0geSAtIDQ7IHB5IDwgeTsgcHkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvc3NpYmxlc1tmaWxsZWQuYXNJbmRleCh4LCBweSldID0gcHk7IC8vVE9ETyAtIGZpZ3VyZSBvdXQgYmV0dGVyIHNjb3Jpbmc/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyBmb3IgZWFjaCBzaGFwZS4uLlxyXG4gICAgICAgICAgICAgICAgdmFyIG9wdHMgPSBfb25lX3NoYXBlID09PSB1bmRlZmluZWQgPyBzaGFwZXMgOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY3VyOiBfb25lX3NoYXBlXHJcbiAgICAgICAgICAgICAgICB9OyAvL0JPT1xyXG4gICAgICAgICAgICAgICAgZm9yIChhdHRyIGluIG9wdHMpIHsgLy9UT0RPIC0gY2hlY2sgaW4gcmFuZG9tIG9yZGVyIHRvIHByZXZlbnQgbGF0ZXIgc2hhcGVzIGZyb20gd2lubmluZ1xyXG4gICAgICAgICAgICAgICAgICAgIHNoYXBlID0gb3B0c1thdHRyXTtcclxuICAgICAgICAgICAgICAgICAgICBiZXN0X3Njb3JlX2Zvcl9zaGFwZSA9IC05OTk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGZvciBlYWNoIG9yaWVudGF0aW9uLi4uXHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IChzaGFwZS5zeW1tZXRyaWNhbCA/IDIgOiA0KTsgaSsrKSB7IC8vVE9ETyAtIG9ubHkgbG9vayBhdCB1bmlxdWUgb3JpZW50YXRpb25zXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJsb2NrcyA9IHNoYXBlLmdldEJsb2NrcyhpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm91bmRzID0gc2hhcGUuZ2V0Qm91bmRzKGJsb2Nrcyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB0cnkgZWFjaCBwb3NzaWJsZSBwb3NpdGlvbi4uLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKHggPSAtYm91bmRzLmxlZnQ7IHggPCB3aWR0aCAtIGJvdW5kcy53aWR0aDsgeCsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgKHkgPSAtMTsgeSA8IGhlaWdodCAtIGJvdW5kcy5ib3R0b207IHkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChnYW1lLl9jaGVja0NvbGxpc2lvbnMoeCwgeSArIDEsIGJsb2NrcywgdHJ1ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29sbGlzaW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3JlID0gc2NvcmVCbG9ja3MocG9zc2libGVzLCBibG9ja3MsIHgsIHksIGZpbGxlZCwgd2lkdGgsIGhlaWdodCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzY29yZSA+IGJlc3Rfc2NvcmVfZm9yX3NoYXBlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBiZXN0X3Njb3JlX2Zvcl9zaGFwZSA9IHNjb3JlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmVzdF9vcmllbnRhdGlvbl9mb3Jfc2hhcGUgPSBpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmVzdF94X2Zvcl9zaGFwZSA9IHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoKGV2aWwgJiYgYmVzdF9zY29yZV9mb3Jfc2hhcGUgPCBiZXN0X3Njb3JlKSB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAoIWV2aWwgJiYgYmVzdF9zY29yZV9mb3Jfc2hhcGUgPiBiZXN0X3Njb3JlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBiZXN0X3NoYXBlID0gc2hhcGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJlc3Rfc2NvcmUgPSBiZXN0X3Njb3JlX2Zvcl9zaGFwZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmVzdF9vcmllbnRhdGlvbiA9IGJlc3Rfb3JpZW50YXRpb25fZm9yX3NoYXBlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBiZXN0X3ggPSBiZXN0X3hfZm9yX3NoYXBlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBiZXN0X3NoYXBlLmJlc3Rfb3JpZW50YXRpb24gPSBiZXN0X29yaWVudGF0aW9uO1xyXG4gICAgICAgICAgICAgICAgYmVzdF9zaGFwZS5iZXN0X3ggPSBiZXN0X3g7XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGJlc3Rfc2hhcGU7XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBmdW5jLm5vX3ByZXZpZXcgPSB0cnVlO1xyXG4gICAgICAgICAgICByZXR1cm4gZnVuYztcclxuICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgX3JhbmRvbVNoYXBlczogZnVuY3Rpb24oc2hhcGVGYWN0b3J5KSB7XHJcbiAgICAgICAgICAgIC8vIFRvZG86IFRoZSBzaGFwZWZ1bmNzIHNob3VsZCBiZSBjYWNoZWQuXHJcbiAgICAgICAgICAgIHZhciBzaGFwZUZ1bmNzID0gW107XHJcbiAgICAgICAgICAgICQuZWFjaChzaGFwZUZhY3RvcnksIGZ1bmN0aW9uKGssIHYpIHtcclxuICAgICAgICAgICAgICAgIHNoYXBlRnVuY3MucHVzaCh2KTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fcmFuZENob2ljZShzaGFwZUZ1bmNzKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBfcmFuZG9tT2JzdGFjbGVzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgLy8gVG9kbzogVGhlIHNoYXBlZnVuY3Mgc2hvdWxkIGJlIGNhY2hlZC5cclxuICAgICAgICAgICAgdmFyIG9ic3RhY2xlU2hhcGVGdW5jcyA9IFtdO1xyXG4gICAgICAgICAgICAkLmVhY2godGhpcy5fb2JzdGFjbGVTaGFwZUZhY3RvcnksIGZ1bmN0aW9uKGssIHYpIHtcclxuICAgICAgICAgICAgICAgIG9ic3RhY2xlU2hhcGVGdW5jcy5wdXNoKHYpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9yYW5kQ2hvaWNlKG9ic3RhY2xlU2hhcGVGdW5jcyk7XHJcbiAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIENvbnRyb2xzXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgX3NldHVwQ29udHJvbHM6IGZ1bmN0aW9uKGVuYWJsZSkge1xyXG5cclxuICAgICAgICAgICAgdmFyIGdhbWUgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgdmFyIG1vdmVMZWZ0ID0gZnVuY3Rpb24oc3RhcnQpIHtcclxuICAgICAgICAgICAgICAgIGlmICghc3RhcnQpIHtcclxuICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5ob2xkaW5nLmxlZnQgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICghZ2FtZS5fYm9hcmQuaG9sZGluZy5sZWZ0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuY3VyLm1vdmVMZWZ0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuaG9sZGluZy5sZWZ0ID0gRGF0ZS5ub3coKTtcclxuICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5ob2xkaW5nLnJpZ2h0ID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB2YXIgbW92ZVJpZ2h0ID0gZnVuY3Rpb24oc3RhcnQpIHtcclxuICAgICAgICAgICAgICAgIGlmICghc3RhcnQpIHtcclxuICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5ob2xkaW5nLnJpZ2h0ID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoIWdhbWUuX2JvYXJkLmhvbGRpbmcucmlnaHQpIHtcclxuICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5jdXIubW92ZVJpZ2h0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuaG9sZGluZy5yaWdodCA9IERhdGUubm93KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuaG9sZGluZy5sZWZ0ID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB2YXIgZHJvcCA9IGZ1bmN0aW9uKHN0YXJ0KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXN0YXJ0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuaG9sZGluZy5kcm9wID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoIWdhbWUuX2JvYXJkLmhvbGRpbmcuZHJvcCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChnYW1lLl9ib2FyZC5kcm9wRmlyc3RQcmVzcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5jdXIuZHJvcCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5ob2xkaW5nLmRyb3AgPSBEYXRlLm5vdygpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChnYW1lLm9wdGlvbnMucXVpY2tEcm9wKSBnYW1lLl9ib2FyZC5jdXIucXVpY2tEcm9wKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmRyb3BGaXJzdFByZXNzID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBnYW1lLm9wdGlvbnMuZG91YmxlUHJlc3NUaW1lKTtcclxuICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5kcm9wRmlyc3RQcmVzcyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB2YXIgcXVpY2tEcm9wID0gZnVuY3Rpb24oc3RhcnQpIHtcclxuICAgICAgICAgICAgICAgIGlmICghc3RhcnQpIHtcclxuICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5ob2xkaW5nLmRyb3AgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICghZ2FtZS5fYm9hcmQuaG9sZGluZy5kcm9wKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGdhbWUub3B0aW9ucy5xdWlja0Ryb3ApIGdhbWUuX2JvYXJkLmN1ci5xdWlja0Ryb3AoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdmFyIHJvdGF0ZUxlZnQgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmN1ci5yb3RhdGUoJ2xlZnQnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB2YXIgcm90YXRlUmlnaHQgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmN1ci5yb3RhdGUoJ3JpZ2h0Jyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIEhhbmRsZXJzOiBUaGVzZSBhcmUgdXNlZCB0byBiZSBhYmxlIHRvIGJpbmQvdW5iaW5kIGNvbnRyb2xzXHJcbiAgICAgICAgICAgIHZhciBoYW5kbGVLZXlEb3duID0gZnVuY3Rpb24oZXZ0KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWdhbWUuX2JvYXJkLmN1cikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdmFyIGNhdWdodCA9IGZhbHNlO1xyXG5cclxuICAgICAgICAgICAgICAgIGNhdWdodCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBpZiAoZ2FtZS5vcHRpb25zLmFzZHdLZXlzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3dpdGNoIChldnQua2V5Q29kZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlIDY1OlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLyphKi9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vdmVMZWZ0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgNjg6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvKmQqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbW92ZVJpZ2h0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgODM6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvKnMqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZHJvcCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlIDg3OlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLyp3Ki9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmN1ci5yb3RhdGUoJ3JpZ2h0Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKGV2dC5rZXlDb2RlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAzMjpcclxuICAgICAgICAgICAgICAgICAgICAgICAgLypzcGFjZWJhciovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1aWNrRHJvcCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAzNzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgLypsZWZ0Ki9cclxuICAgICAgICAgICAgICAgICAgICAgICAgbW92ZUxlZnQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMzk6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qcmlnaHQqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtb3ZlUmlnaHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgNDA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qZG93biovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRyb3AodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMzg6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qdXAqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5jdXIucm90YXRlKCdyaWdodCcpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIDgwOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvKnBhdXNlIFAqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLnBhdXNlUmVzdW1lKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgODg6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qeCovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmN1ci5yb3RhdGUoJ3JpZ2h0Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgOTA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qeiovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmN1ci5yb3RhdGUoJ2xlZnQnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2F1Z2h0ID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoY2F1Z2h0KSBldnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiAhY2F1Z2h0O1xyXG4gICAgICAgICAgICB9O1xyXG5cclxuXHJcbiAgICAgICAgICAgIHZhciBoYW5kbGVLZXlVcCA9IGZ1bmN0aW9uKGV2dCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFnYW1lLl9ib2FyZC5jdXIpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHZhciBjYXVnaHQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgICAgICBjYXVnaHQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgaWYgKGdhbWUub3B0aW9ucy5hc2R3S2V5cykge1xyXG4gICAgICAgICAgICAgICAgICAgIHN3aXRjaCAoZXZ0LmtleUNvZGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSA2NTpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qYSovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb3ZlTGVmdChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSA2ODpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qZCovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb3ZlUmlnaHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgODM6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvKnMqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZHJvcChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKGV2dC5rZXlDb2RlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAzMjpcclxuICAgICAgICAgICAgICAgICAgICAgICAgLypzcGFjZWJhciovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1aWNrRHJvcChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMzc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qbGVmdCovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1vdmVMZWZ0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAzOTpcclxuICAgICAgICAgICAgICAgICAgICAgICAgLypyaWdodCovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1vdmVSaWdodChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgNDA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qZG93biovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRyb3AoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXVnaHQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChjYXVnaHQpIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICFjYXVnaHQ7XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBpc1N0b3BLZXkoZXZ0KSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgY2ZnID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHN0b3BLZXlzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDMyOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAzNzogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgMzg6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDM5OiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA0MDogMVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgdmFyIGlzU3RvcCA9IChjZmcuc3RvcEtleXNbZXZ0LmtleUNvZGVdIHx8IChjZmcubW9yZVN0b3BLZXlzICYmIGNmZy5tb3JlU3RvcEtleXNbZXZ0LmtleUNvZGVdKSk7XHJcbiAgICAgICAgICAgICAgICBpZiAoaXNTdG9wKSBldnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBpc1N0b3A7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIGdldEtleShldnQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnc2FmZWtleXByZXNzLicgKyBldnQua2V5Q29kZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24ga2V5ZG93bihldnQpIHtcclxuICAgICAgICAgICAgICAgIHZhciBrZXkgPSBnZXRLZXkoZXZ0KTtcclxuICAgICAgICAgICAgICAgICQuZGF0YSh0aGlzLCBrZXksICgkLmRhdGEodGhpcywga2V5KSB8fCAwKSAtIDEpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGhhbmRsZUtleURvd24uY2FsbCh0aGlzLCBldnQpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBrZXl1cChldnQpIHtcclxuICAgICAgICAgICAgICAgICQuZGF0YSh0aGlzLCBnZXRLZXkoZXZ0KSwgMCk7XHJcbiAgICAgICAgICAgICAgICBoYW5kbGVLZXlVcC5jYWxsKHRoaXMsIGV2dCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gaXNTdG9wS2V5KGV2dCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIFVuYmluZCBldmVyeXRoaW5nIGJ5IGRlZmF1bHRcclxuICAgICAgICAgICAgLy8gVXNlIGV2ZW50IG5hbWVzcGFjaW5nIHNvIHdlIGRvbid0IHJ1aW4gb3RoZXIga2V5cHJlc3MgZXZlbnRzXHJcbiAgICAgICAgICAgICQoZG9jdW1lbnQpLnVuYmluZCgna2V5ZG93bi5ibG9ja3JhaW4nKVxyXG4gICAgICAgICAgICAgICAgLnVuYmluZCgna2V5dXAuYmxvY2tyYWluJyk7XHJcblxyXG4gICAgICAgICAgICBpZiAoIWdhbWUub3B0aW9ucy5hdXRvcGxheSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVuYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICQoZG9jdW1lbnQpLmJpbmQoJ2tleWRvd24uYmxvY2tyYWluJywga2V5ZG93bilcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmJpbmQoJ2tleXVwLmJsb2NrcmFpbicsIGtleXVwKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuX2d5cm9Db250cm9scygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgIF9zZXR1cFRvdWNoQ29udHJvbHM6IGZ1bmN0aW9uKGVuYWJsZSkge1xyXG5cclxuICAgICAgICAgICAgdmFyIGdhbWUgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgLy8gTW92ZW1lbnRzIGNhbiBiZSBoZWxkIGZvciBmYXN0ZXIgbW92ZW1lbnRcclxuICAgICAgICAgICAgdmFyIG1vdmVMZWZ0ID0gZnVuY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5jdXIubW92ZUxlZnQoKTtcclxuICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmhvbGRpbmcubGVmdCA9IERhdGUubm93KCk7XHJcbiAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5ob2xkaW5nLnJpZ2h0ID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmhvbGRpbmcuZHJvcCA9IG51bGw7XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHZhciBtb3ZlUmlnaHQgPSBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmN1ci5tb3ZlUmlnaHQoKTtcclxuICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmhvbGRpbmcucmlnaHQgPSBEYXRlLm5vdygpO1xyXG4gICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuaG9sZGluZy5sZWZ0ID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmhvbGRpbmcuZHJvcCA9IG51bGw7XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHZhciBkcm9wID0gZnVuY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5jdXIuZHJvcCgpO1xyXG4gICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuaG9sZGluZy5kcm9wID0gRGF0ZS5ub3coKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdmFyIHF1aWNrRHJvcCA9IGZ1bmN0aW9uKGV2ZW50KSB7XHJcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuY3VyLnF1aWNrRHJvcCgpO1xyXG4gICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuaG9sZGluZy5kcm9wID0gRGF0ZS5ub3coKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdmFyIGVuZE1vdmVMZWZ0ID0gZnVuY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5ob2xkaW5nLmxlZnQgPSBudWxsO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB2YXIgZW5kTW92ZVJpZ2h0ID0gZnVuY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5ob2xkaW5nLnJpZ2h0ID0gbnVsbDtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdmFyIGVuZERyb3AgPSBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmhvbGRpbmcuZHJvcCA9IG51bGw7XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHZhciBlbmRRdWlja0Ryb3AgPSBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmhvbGRpbmcuZHJvcCA9IG51bGw7XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAvLyBSb3RhdGlvbnMgY2FuJ3QgYmUgaGVsZFxyXG4gICAgICAgICAgICB2YXIgcm90YXRlTGVmdCA9IGZ1bmN0aW9uKGV2ZW50KSB7XHJcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgZ2FtZS5fYm9hcmQuY3VyLnJvdGF0ZSgnbGVmdCcpO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB2YXIgcm90YXRlUmlnaHQgPSBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmN1ci5yb3RhdGUoJ3JpZ2h0Jyk7XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAvLyBVbmJpbmQgZXZlcnl0aGluZyBieSBkZWZhdWx0XHJcbiAgICAgICAgICAgIGdhbWUuXyR0b3VjaExlZnQudW5iaW5kKCd0b3VjaHN0YXJ0IHRvdWNoZW5kIGNsaWNrJyk7XHJcbiAgICAgICAgICAgIGdhbWUuXyR0b3VjaFJpZ2h0LnVuYmluZCgndG91Y2hzdGFydCB0b3VjaGVuZCBjbGljaycpO1xyXG4gICAgICAgICAgICBnYW1lLl8kdG91Y2hSb3RhdGVMZWZ0LnVuYmluZCgndG91Y2hzdGFydCB0b3VjaGVuZCBjbGljaycpO1xyXG4gICAgICAgICAgICBnYW1lLl8kdG91Y2hSb3RhdGVSaWdodC51bmJpbmQoJ3RvdWNoc3RhcnQgdG91Y2hlbmQgY2xpY2snKTtcclxuICAgICAgICAgICAgZ2FtZS5fJHRvdWNoRHJvcC51bmJpbmQoJ3RvdWNoc3RhcnQgdG91Y2hlbmQgY2xpY2snKTtcclxuICAgICAgICAgICAgZ2FtZS5fJHRvdWNoRHJvcFNwYWNlLnVuYmluZCgndG91Y2hzdGFydCB0b3VjaGVuZCBjbGljaycpO1xyXG5cclxuICAgICAgICAgICAgaWYgKCFnYW1lLm9wdGlvbnMuYXV0b3BsYXkgJiYgZW5hYmxlKSB7XHJcbiAgICAgICAgICAgICAgICBnYW1lLl8kdG91Y2hMZWZ0LnNob3coKS5iaW5kKCd0b3VjaHN0YXJ0IGNsaWNrJywgbW92ZUxlZnQpLmJpbmQoJ3RvdWNoZW5kJywgZW5kTW92ZUxlZnQpO1xyXG4gICAgICAgICAgICAgICAgZ2FtZS5fJHRvdWNoUmlnaHQuc2hvdygpLmJpbmQoJ3RvdWNoc3RhcnQgY2xpY2snLCBtb3ZlUmlnaHQpLmJpbmQoJ3RvdWNoZW5kJywgZW5kTW92ZVJpZ2h0KTtcclxuICAgICAgICAgICAgICAgIGdhbWUuXyR0b3VjaERyb3Auc2hvdygpLmJpbmQoJ3RvdWNoc3RhcnQgY2xpY2snLCBkcm9wKS5iaW5kKCd0b3VjaGVuZCcsIGVuZERyb3ApO1xyXG4gICAgICAgICAgICAgICAgZ2FtZS5fJHRvdWNoRHJvcFNwYWNlLnNob3coKS5iaW5kKCd0b3VjaHN0YXJ0IGNsaWNrJywgcXVpY2tEcm9wKS5iaW5kKCd0b3VjaGVuZCcsIGVuZFF1aWNrRHJvcCk7XHJcbiAgICAgICAgICAgICAgICBnYW1lLl8kdG91Y2hSb3RhdGVMZWZ0LnNob3coKS5iaW5kKCd0b3VjaHN0YXJ0IGNsaWNrJywgcm90YXRlTGVmdCk7XHJcbiAgICAgICAgICAgICAgICBnYW1lLl8kdG91Y2hSb3RhdGVSaWdodC5zaG93KCkuYmluZCgndG91Y2hzdGFydCBjbGljaycsIHJvdGF0ZVJpZ2h0KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGdhbWUuXyR0b3VjaExlZnQuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgZ2FtZS5fJHRvdWNoUmlnaHQuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgZ2FtZS5fJHRvdWNoUm90YXRlTGVmdC5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICBnYW1lLl8kdG91Y2hSb3RhdGVSaWdodC5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICBnYW1lLl8kdG91Y2hEcm9wLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgIGdhbWUuXyR0b3VjaERyb3BTcGFjZS5oaWRlKCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgaXNNb2JpbGVEZXZpY2U6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gKHR5cGVvZiB3aW5kb3cub3JpZW50YXRpb24gIT09IFwidW5kZWZpbmVkXCIpIHx8IChuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoJ0lFTW9iaWxlJykgIT09IC0xKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBfZ3lyb0NvbnRyb2xzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgaWYgKHdpbmRvdy5EZXZpY2VNb3Rpb25FdmVudCAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICB3aW5kb3cub25kZXZpY2Vtb3Rpb24gPSBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBhY2NYID0gTWF0aC5yb3VuZChldmVudC5hY2NlbGVyYXRpb25JbmNsdWRpbmdHcmF2aXR5LnggKiAxMCkgLyAxMDtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgYWNjWiA9IE1hdGgucm91bmQoZXZlbnQuYWNjZWxlcmF0aW9uSW5jbHVkaW5nR3Jhdml0eS56ICogMTApIC8gMTA7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKE1hdGgucm91bmQoYWNjWikgPiA0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdhbWUuX2JvYXJkLmN1ci5kcm9wKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChNYXRoLnJvdW5kKGFjY1gpID4gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5jdXIubW92ZUxlZnQoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKE1hdGgucm91bmQoYWNjWCkgPCAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnYW1lLl9ib2FyZC5jdXIubW92ZVJpZ2h0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG5cclxuXHJcblxyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxufSkoalF1ZXJ5KSk7IiwiLyoqXHJcbiAqIFRoZW1lcy4gWW91IGNhbiBhZGQgbW9yZSBjdXN0b20gdGhlbWVzIHRvIHRoaXMgb2JqZWN0LlxyXG4gKi9cclxud2luZG93LkJsb2NrcmFpblRoZW1lcyA9IHtcclxuICAgICdjdXN0b20nOiB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogJyMwNDAzMDQnLFxyXG4gICAgICAgIGJhY2tncm91bmRHcmlkOiAnZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFFQUFBQUJBQ0FJQUFBQWxDK2FKQUFBSEhFbEVRVlI0WHNWWmdaYmtxZ3FrVE83Ny8vK2RidXZ0Z1E3UWNHd25PM1BQWlRQRzFkaFdBU0l4d1A4T0VjcVRZaEozeXBzQXVMcXNCN0tTTnJRMTR1TW9YQVhzbndOaWhvVURJbktLYkNkRGYyWWpQdUwrS0RSU3lPcEUxUTVrNkpCSlY3SUpTZm52SlV6ZjhSaHlBT2g5QURxTjN2dHorYW0reklYV0hJSzlsMUQ1SVN1U1RidjNhVUFKWktmdm1NWVlCbjNPNlkzVy9sdDJJRm1tSUhtYlFEY0NnT000RENqSnFlS3NOZ1FBSWU5YWcxM0k0Tk5Ib1VXaG9tTW40Qm9pdWJYQXFuMjdxQW9ObTlITHdoTUFmUTEwbGdZeGM1Z3F2Z3hjZnV3OHNkaE1IS3REOTlJckdmQ3BrWFpqQkc5eDlyOFNpekovSkhGOFl3dzNoWXN6TkRuejV1YXdESDNXc1RFU0laQmNzNk81cjM2U1ZuNGdtY0ZZSlZtZ1NZWk9NcW1FZGpmOHZ4VjhyaUE0dEcwWm81MXFlZURRdFF4aHVQNmhVbWdZWS9VL3l1OEpLWUJWbUdkWkd6bldocUJab0FlZlRUaTdHWU9ZL2pLSEVQTDU3bG9PYkJVOHpoTDR6L1A4VXhiZE4wMnNVek9TcUttbHltWm5DTGNrdDJ0ZHE0MUFPSThLeVU0QVFHZkNyTkVPa3IwRFBqeEQ3NjdWQlVsczNxSE5FZmpkaGRwV3hhNysremt6Vm1NQiswUFhjbmR5OXlNb2djd3NkNWZKQUZ6b3RjY2ZnS0JmQXJtdWtQS1FROGRDT3ZyR0FYa054QlBla3ZNYWh5TmJNWmJmRkZjRExjVlBmZ1Y4TW9KT2NnbzJRY1dEUVppTk5oM2xKOUlkYU5Sc2tDazBGTVVaRkpKaGdUbnBzcHhGM2w1Uy8zVWh1WGdwcTFFb3B4eFF5WDdWM3BkQjhuZHhYbzRhdWttYXBEUWFKQWxTR0daekF1OGJJZElEci9MYjZCblhUdGdrL3dMSm5vQ1ViTFNQUitQTlRiQU1tdDNIQ0RQb25uTi9jMEJyTVU3TWF3QUFtQVFnZ09Jd2V1OW9HRVVtaUhMUUJQeFMrdjJXU2dESXdUZ21qd3JibGdrMWtCYnRWSWQxcC80NTNCQVBSKzVmSnlLdVFHUTQ5S0xEV3ZuTFNOUUpzZThlK1NpdW5JL1VjQVE1YVRCbzZuY2orSE1MbUdCSDA0V09xVmttK3FQblFrd1lCS1IxR0VwWGNYT2ZwTlZBT25TUW1KUzhldWxvcXhkMWZXTFpVaTJJNEpDa3Z5U1dOL3BzTWQ4SERKaHp5RC9EZFc1ZkJBRnZJenZxS0xzRXJPd2NSa0tVWFQ4RDVDSmRwa0N2RUc3U3p6MHI2cVZGRTZxMGZhQ1N4dVYwNWtPOC9HVUJkT2xOa0wwd1N0Z2QvcmVSU2dDRTBGV1Bob1hmaVM1RWc0N1A2Q0g4VEJsU2MrUlNQMzFSQ2dqd3l0UjVKMHJpVmpzeWg2MEFIM3VWZ0tGUGlwa2lRL0NCQXlvVU5zVnZoRTFIa0wrU002R2M2a1cwUUpyblNIRU5EYThKOWppWWFsMDdORDN1Yzc1R0FFa2w0R1dCa3VmYzhobXNIWVFlb1VzM3ZiMjZUWWZlb3hCRTZOQkh4Y3RiS3dGVjJlRnZzZGNVLzJGZEdzdi9VU1gzbmQwMUlmd2VXSHg3aStxbTZWbVE0VUxCVEFvK0pyS2pnSExYdjM4Nmd2ZW9pUElvMXBFTjVkNHp5TFZIbllZWllWa3lqQkFnbUxVWnpWM1hQU0hvNklNb2U0cDBVOFo2ZC9SN1ZSSW9Td3NJTmw1VnpWU0VYZmRjTDhQK2dZUEpEL0N1RXVBcXVzL0ZhUVc3MFZsZC80N0VPaUNhd1pSQWlTQnJaK3lvb0Z5NytWRzB5SGNYNGw4ZVRYTHBRbjBvSUFEeElVTUJlb0R0cnNIVzg3RWRzdnR2YnhnUVNSZXNGSUhqUkZadGo2S0VYK3VjZ1owRDkraUw4OWF2QkNMdkJNUTVSQ1VVM3BPd3ZtVlN3S3dQTU5XRm9IdlNUclhvQ2VucWk4RndaTU43cllFT0VONGJKbkZCUmNLNGdpMjFuQ2xLRk9ZWjdaSkxZeEt3RFJZRWVYSnMxdGw5MmZ2OXRxL25Ra2d1U1ZnRjlGUG9ucXV3Qmkxc3NkYnhBcFFjZ2t2SUFIYnBkQURLSHNMdy9DNDMwMzMyeEo4SllTSjZaMmVtVUhnNmVoQkN3QjBKc1FVMUVOZ21LejJXb3VYbVdDVWpLTjRDWUdPQnFuNElXTGxteFBUWnVZVU9oL0txZzZoblkvY2xEcmJzaDBqVHNNZS9sZjBvZmxiUmpZQWxJaVRYWVJ5M0ltZmJFTjc2eEcrUVQ4YzVLWlBFVkJLaktSZ0ZZOXZmNEtUcGtMMkYxSWE2ZksrMnhUcnZYNWJtbk8xTHZkNm5rbm84bnhwNmprRUJrT01Od2kxR25TNU1vcFdzN2M2ZjltTW9LbWxNNHNEY3RUNVZIby9IaTRES2dURjhMbkxxUFFiSExNTmFobjg1OWZLQ0VTdW9McXRvQlpDMnpmajVMdEhzdW44K24xOWZYMy9LT1ZYaHlRTGt5emtuSnlsVGNCdzRqNkdvSFlDQkxpL2xOUktHQzYxZlFaSEE4eUplN0FhZnpWMy9vWkplaTVHakVDOGFrNFE4WHNvYkhGckoyeDlJWVh0empRQUZwaWJDK2ttVUUzZjZ0SjRQMExHV1UvYy9XaS9vZllyemRSOUc0ZUlxVTU0UGhYb0E0Mm9YUmk0OUJDTlkyVkNVUElneGlCNDdBWUNDN0hCOHZnekJwQXdnRVZDaFNuMmhpYXlmY1pGOHppa1BPVVhHSWFCTURRQnpVdEVmQTBZZzFNcCtZcVUrZVZWSVJXOEdpTzhwSWxOQ0dQZndud2c3UldpTCtKK0JFWTNGSzN3VlRjN0h3OVlQWGFHa2tES1p4QU8wVlRuMW9qRGFxYVUxK2xPcUh1b1ZmZmtEZHVjQTllNFRoMXNBcG5zd291SUVCeWhENWlSQmUwVEFNU3pqODVQOElBVzNSanAvcHJZTDdFNENRdTBJQTAzM3MxQy9sVUlPNVFNQkVRUU9sSE9obm9neGNpQysxMmszbDNEZmZxeVh4MDFKUDhwOENlbXNRLzl5R2N3QkZmay9XcXo2VDFVVS8zY0FBQUFBU1VWT1JLNUNZSUk9JyxcclxuICAgICAgICBjb21wbGV4QmxvY2tzOiB7XHJcbiAgICAgICAgICAgIGxpbmU6IFsnYXNzZXRzL2Jsb2Nrcy9jdXN0b20vbGluZS5wbmcnLCAnYXNzZXRzL2Jsb2Nrcy9jdXN0b20vbGluZS5wbmcnXSxcclxuICAgICAgICAgICAgc3F1YXJlOiBbJ2Fzc2V0cy9ibG9ja3MvY3VzdG9tL3NxdWFyZS5wbmcnXSxcclxuICAgICAgICAgICAgYXJyb3c6ICdhc3NldHMvYmxvY2tzL2N1c3RvbS9hcnJvdy5wbmcnLFxyXG4gICAgICAgICAgICByaWdodEhvb2s6IFsnYXNzZXRzL2Jsb2Nrcy9jdXN0b20vcmlnaHRIb29rLnBuZyddLFxyXG4gICAgICAgICAgICBsZWZ0SG9vazogJ2Fzc2V0cy9ibG9ja3MvY3VzdG9tL2xlZnRIb29rLnBuZycsXHJcbiAgICAgICAgICAgIHJpZ2h0WmFnOiBbJ2Fzc2V0cy9ibG9ja3MvY3VzdG9tL3JpZ2h0WmFnLnBuZyddLFxyXG4gICAgICAgICAgICBsZWZ0WmFnOiAnYXNzZXRzL2Jsb2Nrcy9jdXN0b20vbGVmdFphZy5wbmcnXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgICdjYW5keSc6IHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAnIzA0MDMwNCcsXHJcbiAgICAgICAgYmFja2dyb3VuZEdyaWQ6ICdkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQUVBQUFBQkFDQUlBQUFBbEMrYUpBQUFISEVsRVFWUjRYc1ZaZ1pia3FncWtUTzc3Ly8rZGJ1dnRnUTdRY0d3bk8zUFBaVFBHMWRoV0FTSXh3UDhPRWNxVFloSjN5cHNBdUxxc0I3S1NOclExNHVNb1hBWHNud05paG9VREluS0tiQ2REZjJZalB1TCtLRFJTeU9wRTFRNWs2SkJKVjdJSlNmbnZKVXpmOFJoeUFPaDlBRHFOM3Z0eithbSt6SVhXSElLOWwxRDVJU3VTVGJ2M2FVQUpaS2Z2bU1ZWUJuM082WTNXL2x0MklGbW1JSG1iUURjQ2dPTTREQ2pKcWVLc05nUUFJZTlhZzEzSTROTkhvVVdob21NbjRCb2l1YlhBcW4yN3FBb05tOUhMd2hNQWZRMTBsZ1l4YzVncXZneGNmdXc4c2RoTUhLdEQ5OUlyR2ZDcGtYWmpCRzl4OXI4U2l6Si9KSEY4WXd3M2hZc3pORG56NXVhd0RIM1dzVEVTSVpCY3M2TzVyMzZTVm40Z21jRllKVm1nU1laT01xbUVkamY4dnhWOHJpQTR0RzBabzUxcWVlRFF0UXhodVA2aFVtZ1lZL1UveXU4SktZQlZtR2RaR3puV2hxQlpvQWVmVFRpN0dZT1kvaktIRVBMNTdsb09iQlU4emhMNHovUDhVeGJkTjAyc1V6T1NxS21seW1abkNMY2t0MnRkcTQxQU9JOEt5VTRBUUdmQ3JORU9rcjBEUGp4RDc2N1ZCVWxzM3FITkVmamRoZHBXeGE3Kyt6a3pWbU1CKzBQWGNuZHk5eU1vZ2N3c2Q1ZkpBRnpvdGNjZmdLQmZBcm11a1BLUVE4ZENPdnJHQVhrTnhCUGVrdk1haHlOYk1aYmZGRmNETGNWUGZnVjhNb0pPY2dvMlFjV0RRWmlOTmgzbEo5SWRhTlJza0NrMEZNVVpGSkpoZ1RucHNweEYzbDVTLzNVaHVYZ3BxMUVvcHh4UXlYN1YzcGRCOG5keFhvNGF1a21hcERRYUpBbFNHR1p6QXU4YklkSURyL0xiNkJuWFR0Z2svd0xKbm9DVWJMU1BSK1BOVGJBTW10M0hDRFBvbm5OL2MwQnJNVTdNYXdBQW1BUWdnT0l3ZXU5b0dFVW1pSExRQlB4Uyt2MldTZ0RJd1RnbWp3cmJsZ2sxa0JidFZJZDFwLzQ1M0JBUFIrNWZKeUt1UUdRNDlLTERXdm5MU05RSnNlOGUrU2l1bkkvVWNBUTVhVEJvNm5jaitITUxtR0JIMDRXT3FWa20rcVBuUWt3WUJLUjFHRXBYY1hPZnBOVkFPblNRbUpTOGV1bG9xeGQxZldMWlVpMkk0SkNrdnlTV04vcHNNZDhIREpoenlEL0RkVzVmQkFGdkl6dnFLTHNFck93Y1JrS1VYVDhENUNKZHBrQ3ZFRzdTenowcjZxVkZFNnEwZmFDU3h1VjA1a084L0dVQmRPbE5rTDB3U3RnZC9yZVJTZ0NFMEZXUGhvWGZpUzVFZzQ3UDZDSDhUQmxTYytSU1AzMVJDZ2p3eXRSNUowcmlWanN5aDYwQUgzdVZnS0ZQaXBraVEvQ0JBeW9VTnNWdmhFMUhrTCtTTTZHYzZrVzBRSnJuU0hFTkRhOEo5amlZYWwwN05EM3VjNzVHQUVrbDRHV0JrdWZjOGhtc0hZUWVvVXMzdmIyNlRZZmVveEJFNk5CSHhjdGJLd0ZWMmVGdnNkY1UvMkZkR3N2L1VTWDNuZDAxSWZ3ZVdIeDdpK3FtNlZtUTRVTEJUQW8rSnJLamdITFh2Mzg2Z3Zlb2lQSW8xcEVONWQ0enlMVkhuWVlaWVZreWpCQWdtTFVaelYzWFBTSG82SU1vZTRwMFU4WjZkL1I3VlJJb1N3c0lObDVWelZTRVhmZGNMOFArZ1lQSkQvQ3VFdUFxdXMvRmFRVzcwVmxkLzQ3RU9pQ2F3WlJBaVNCcloreW9vRnk3K1ZHMHlIY1g0bDhlVFhMcFFuMG9JQUR4SVVNQmVvRHRyc0hXODdFZHN2dHZieGdRU1Jlc0ZJSGpSRlp0ajZLRVgrdWNnWjBEOStpTDg5YXZCQ0x2Qk1RNVJDVVUzcE93dm1WU3dLd1BNTldGb0h2U1RyWG9DZW5xaThGd1pNTjdyWUVPRU40YkpuRkJSY0s0Z2kyMW5DbEtGT1laN1pKTFl4S3dEUllFZVhKczF0bDkyZnY5dHEvblFrZ3VTVmdGOUZQb25xdXdCaTFzc2RieEFwUWNna3ZJQUhicGRBREtIc0x3L0M0MzAzMzJ4SjhKWVNKNloyZW1VSGc2ZWhCQ3dCMEpzUVUxRU5nbUt6MldvdVhtV0NVaktONENZR09CcW40SVdMbG14UFRadVlVT2gvS3FnNmhuWS9jbERyYnNoMGpUc01lL2xmMG9mbGJSallBbElpVFhZUnkzSW1mYkVONzZ4RytRVDhjNUtaUEVWQktqS1JnRlk5dmY0S1Rwa0wyRjFJYTZmSysyeFRydlg1Ym1uTzFMdmQ2bmtubzhueHA2amtFQmtPTU53aTFHblM1TW9wV3M3YzZmOW1Nb0ttbE00c0RjdFQ1VkhvL0hpNERLZ1RGOExuTHFQUWJITE1OYWhuODU5ZktDRVN1b0xxdG9CWkMyemZqNUx0SHN1bjgrbjE5ZlgzL0tPVlhoeVFMa3l6a25KeWxUY0J3NGo2R29IWUNCTGkvbE5SS0dDNjFmUVpIQTh5SmU3QWFmelYzL29aSmVpNUdqRUM4YWs0UThYc29iSEZySjJ4OUlZWHR6alFBRnBpYkMra21VRTNmNnRKNFAwTEdXVS9jL1dpL29mWXJ6ZFI5RzRlSXFVNTRQaFhvQTQyb1hSaTQ5QkNOWTJWQ1VQSWd4aUI0N0FZQ0M3SEI4dmd6QnBBd2dFVkNoU24yaGlheWZjWkY4emlrUE9VWEdJYUJNRFFCelV0RWZBMFlnMU1wK1lxVStlVlZJUlc4R2lPOHBJbE5DR1Bmd253ZzdSV2lMK0orQkVZM0ZLM3dWVGM3SHc5WVBYYUdra0RLWnhBTzBWVG4xb2pEYXFhVTErbE9xSHVvVmZma0RkdWNBOWU0VGgxc0FwbnN3b3VJRUJ5aEQ1aVJCZTBUQU1Temo4NVA4SUFXM1JqcC9wcllMN0U0Q1F1MElBMDMzczFDL2xVSU81UU1CRVFRT2xIT2hub2d4Y2lDKzEyazNsM0RmZnF5WHgwMUpQOHA4Q2Vtc1EvOXlHY3dCRmZrL1dxejZUMVVVLzNjQUFBQUFTVVZPUks1Q1lJST0nLFxyXG4gICAgICAgIGJsb2Nrczoge1xyXG4gICAgICAgICAgICBsaW5lOiAnZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFCSUFBQUFTQ0FJQUFBRFpyQmtBQUFBQ3VrbEVRVlI0WGkyTXpXNGNSUlNGei8ycDZwNGVUMnpqakUyTUhCUVd2QUFMTmp3RFM4U09GYy9GSXlDeFFnaDJMRkhrQlFnSnlRby93WEtDRVo3eDlFOVZWOTNMQlBOSjUxNmR4WGZvL1U4K0hUWjl1enJaeW1wVGpMbzREYnREaUJwWFNCR01vY0k1bGthTWhLdjBmeitMVlg5UDZadXZ2eHFCYnkrM0wzZUp1Nlp0MjdETDRxZ0lXVzBLc3hQQzNLanh6NWZQVDN6NDhjc3Z0SFNIZDBDLzEzNzRSZGRQWC85eEs2SmRjakV1dE5mcUZKTUJzUWF0M0J5K2kvN1Z4cUo2Yy96ZDVhdnZuMStON2RsMlY5QWQ5Mk0yVmlJMmtzeVdTSjJRWFFYYzUvSDI1alljSEhLbDFZWWViY0pxdy9yVzAzT096ZnI4OU5IYlI4dkgzV3E5T2xvZnJkZG5pK1hoeWNYSmhCU2tLRkx4V2VIQlpGRkRWeUQzNDNSemMvMFhlWVNYZVhaSUphYW0yVjlwTHNCT3RSSUtZQW9yWHF2QWc4Z2lhS3NDS3cxekpBR0ZiS2h3dHhyZEc5RTNCUXhBSVcxeHlZVUtVSXdOeE1Bd0RFd3VTZ0E1aUF3Mmw1SVRPZUUvR0xSTWloSU9zblFabktxT2xXWFJVV2lkeVlrRnBDeHRDRjFzOEQrc2VCZ2dZVllSaENoa3Fxam1sZDNOS2hodTFYS3VLVWY0ZzZlZ0VpdGt2cWM1ZXdvKzNaT1ZaSk80UTRPU0VER1JSeUFRa1lIOFFmTVVDUjA3S1M4RElzTzh4aGpjblVYblN2NEdNUllTaFNVRzJNRkNlYnkvYXdPSkZ5dkYzVFUyMmNrazNFM0ZRamNpK21KVmdnd3oxMnJLVkV2V09rM2R3V3BJcjdlenJVNFhaKzljaUJBeFdDVmxoRGIybzVjWlRnQlIxN1IzdzQ1aGdnOC9mdStEajY0M2FkWlZYekNtZWJzYnA1VDZmaHIyTDlXOHo1eExOc3lEcGwyYnRyc1hQeW5TOU52VlZjMjh4ek1MUERDVVliVlVNeStUTWdkaW51cENFYXBkdlB2MGxxR3dqVzlmSHNYakYzOWVJelJwbkZiTGRpeTkxOHFDV21jbG5xM21jV2hWbnp3NXYrODNYWXowK0xQUC84bEw3MDROQjNCQmRiQ0RLbENoQXE4UWdSdm1HU0ZvdXlvM3Z6N0xyLzhGTUhxaWUzVkNwTlFBQUFBQVNVVk9SSzVDWUlJPScsXHJcbiAgICAgICAgICAgIHNxdWFyZTogJ2RhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBQklBQUFBU0NBSUFBQURackJrQUFBQUNmVWxFUVZSNFhoM0x2WTVjUlJCQTRWTlYzZmZlK2ZFYVpBUmdXZkJFeEVoRVpFZzhBNjlBZ2tDOEF6RWlJQ01nSkNkQXhnZ3NHUnVXM1ozZG1lbmJYVldza1Q2ZDdNaVhuejNxY2x1ODdlc20xcVczTnRlUnVxYUNJQW1RQUJ3UHZQVTJOeWVhVWE2UDE1OS85UVYzbC9IcWxXcU40MUVsMFBhYWRFSkFvWkJLcVM5K2V5YkwvdHZ2dmk4UHJIQTRrdTMzcHo5djVyYWZLbWMwSTdRaGJpR2FKU2lreWVDaDFUN09HNmZNbHVPdlAvNSs4Y3NiKy9SeGhVdGxFWEE1S1VNcGlFcFd3WktVM0Q3OTlabDFDdHBFanNvaCsrSFJ4VU1CVmhncnk0eVVYTHZzdG5FOHFXaS9IWm5kVEV3cHBwajVYTElnN2ZaNCtmSmxiOTFqelNsR2hqYzJPN1JNajk5NW5BcFJJUlcwQkhSbDlVWExvbmE2UFlpR0dLcGFhNTBxUHRROTYvNUJDZ2twQktnaHBJN1ZKWks1Ym1ZanU1SXhRa1pPTnF0YnUrdHhkNVpFUUZJMEthUlJKeEVaWTB3amgvZHFLcWJaL3o5dHFtSklxcFFTbmFEOFQ5c1lrR3FXbXVUSVpIZ29XWXVwS2g0U3FTUWltcXJKZmVWMWdURUNGNE5LbVVId0RGUnFOU3VKak1UeEZRQU5OS0dVeVRCeFlxU1g2SjZVU2RjMUJxdVlGaEVNRkVRQ1V2U2VDNXJoZUwvWTc0a0F2WC9PUFhSWm9qSWtxSGthTHBNd1dkWUptMWZIbFpJSm90ZFhON3RGdGN6dnZmdUI1TUl5Ulp3UTExcG9yYTJCVEtPdnZhM3pQS3RTdHBzS0xOTm1tY3I1M3h0Rk10TGFHTFNSNjFJS0thUEhlbk8xTDIrcVRocXJuNUZ2UHVHamp6OHM0eCtOSTZQdGR2dHhja3l3bzBjanN0WlpaZlpoTmhTV051b1BQLzVVZGhlYmM4K3RicDQvL3pQZDZkY2FHdEp0V3RVaVZraFRXU0prVG4zeTVQMTVNMTllcnZMMXA5VVpGelg5ekhZbU8xUGgzS0ZRSytya0lCTVNjYlk3RGljT3puK1FSb1c1aWFtcWhnQUFBQUJKUlU1RXJrSmdnZz09JyxcclxuICAgICAgICAgICAgYXJyb3c6ICdkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQUJJQUFBQVNDQUlBQUFEWnJCa0FBQUFDc2tsRVFWUjRYaFdQdTI1YlZ4UkVaei9PdVE5UlZLaFhqQlN1OGdYNWgrUUQwdWREOGlsQnlqUnBVcVp4YTZSeFk4RHVZbGh3SGhBYzJxUklpZUs5dk9mc3ZVTU5Cb1BWckdMb2g2KysrN3k3bWJGZlROSVZkU2hDeENHdUFnRTh0U2lsaE9WV1U2bHJ0UGpQSzMzZmZ2dmI2eGQ0S0c5LytmMWtlbklZb0FnT0ljK0FGOXU1UmR0ZXV2dWJ0eTlMVTE2OGZxVzl6N0FGYW5yMzZ1UFpTT0lnV0hCMUlKQUJLRTBrZVNyM1FkTG9sWGl0ancxbjcxNysrc2ZQUC83VWxhNnBmYlltZVU3ZWFCeWJuemc2ZXd3cFQ4RFcvdnRoMmFjRmt6ZjlQaTBPT1UxKy9lVmxNMi9tMTR2WitSZno4OHQrTnArZm5aOHRuczFPTDY2dkxzczBGSXVnaGdwcER1bUx6SXBtWUJySHordVZzWlZTc3Jhb3JKb01RVTVkT3MzSzNqYWlTWEdjaU5Zb0czY1FjYkpqd2lLQzNOeTlsamhFRVVsR0JuaDRJSndSK3FRNnE3RmJ1TVBCTEVRQ1ZqYVlBMG1Zd2N5SVk2cDVxWUZRUXFPZXhSTWhIT3FnNnNWOG10aXFRa2k1MWdTSFY2WUFNWU1NeGdJQzJJT2NCY0lzUWlwQkJDSWpOcUVRQldBUmpqQU9IQXRTaDFlcWs5YnFMcEJLUVVTWkU0Y2NJcXBKRm5XUWlSU0t5cGpVQTFEZ0VEdzVqYW5Oa2dBNHpLbUdRdHBnc05xaE1vUkNtUm5pNEFnS0poNkRCODQyVGZmRU5XV3FVK200NDhlWWplMTg2R1pUMzFuWG81RXFQaFVHSXFvR3dkMWd0V0Z0UkJlTHhjWEYxVHlkWWgrdGQxRTVsTWRwT094R3I5YlBPemRydEZHV2sybEFnODRlOS9mWWJHbXN5bmMweEQ3bWZrSVRnVU1iSmFLYzI2R09WbjJzQjYxR2Y5NThPRTk5Mndzb1FqU1NqQkhVd0VtbElTVHN4akVzT0lzeFAzdisvTTN0ZXkzd3JSVmgyOXd1QTJWbzNYb2UzS1A2RW5kaHpoM3Y5Mk5qS2VmY1haekVUS3hSK1NhK3Z0MnMzLzN6MTI3WXJZZk4rbUd6Mm00K3JUK3ROdXU3N2QzeWZ2bjMrdmJoOGVINGJiM2RMcmVyOTdjM3E3TDZIMTVndk9ES0I1dTRBQUFBQUVsRlRrU3VRbUNDJyxcclxuICAgICAgICAgICAgcmlnaHRIb29rOiAnZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFCSUFBQUFTQ0FJQUFBRFpyQmtBQUFBQ3FFbEVRVlI0WGgzUE8ydGxaUlNIOGY5YTczWHZQVGtURXhsMWRMQUlLRnBOWTJGbDRVY1Q3RzNGd21JYU8zc2JFZlFqV0F4REFrSU9SREhKeVhGZjNzdGF5ekMvOXFrZSt1YnJGMHU3enF3ZnNKTkZYRUt0Q0FKcmlCR0JvQjBnTkFaNVdJZWtjQzBmK2JyOCsrM1ByMUFQNVkvZjZYZ2Z4NFJxb0Iya0kyN1Fna1lRdzVCQTJMOTVNK3ZUbjM3OXkwYzJISStZNU9iNnozQi9NL1ptbFRpZVY3SVNqOXJYWjJuMzM4TmNpRU1lZEdzaEphY2JaOVgxNnZLM0g3NnZoLzFwYUNPV2lXdm9zNU0xVTlsbExJZWJLZXF6MHpHNFBtWi9kZmw2WFdjLzlwNE94L2ZNd25JY1hyd0w2b2hQZzU0T1poWVRVVWMrdzFZUTgzejNrTWFkRUtkcDhFbUU1NGNuS2lmVEJMT3IxN2NxdHdJZjRnQXVLaFVkOExoNCtUS1Bvd2p5T0sxbDhjeEFtOW5LMm5XbnNRZUF5TnRnbGRuQ2xDTG50a3JSclRCY0Z5UGpZUmc4QUVBWTRKRGhraHA3dU9CSVJieXFybUsrS2NCbTFxVTMwVWJTeEhjR25JR3BLWWs2M2RReG5DMnNQY1dvMnNsWlVDQ0dZWFBra0JuVXFqY0NTQUdGc2FPVVFrNWtvcVVxUEZWNUcyc0RhaSt0RzN2djZaRjNCb2pCekZydnBlaWo0S3REWlRqUFhUV0dCQTkxdVdKVG8xV2txVEFab0FicFQzZWpkMFlPQlcxem9BSEUvdEc4RlRobTUyS09MbkR2MWRBOERBQ1R3YlJVNlR4U2FYMVdaSUkvMXFCZ3dMdld0Z042aWVRYzJoQzlod0xHVEZTMzVlVEo3dU9MNXh4VGtaSTQ0bDdCREdwd0JrZThGRkNBOVBqMkVBaXBIYmQ4UXJlM2Z4T01EV3lrR3J5eU0yVnY2N0p3bUNKN2NRUlJNNkpYWCtDckx6OTcvdUhKNGJnSEZVZEtCaThCNWdFQ0xMQXlzNGdyQWtyNVZ0NzU4WmRMVHdodVBOdmZIUDdaUHpqckJBSFlMQmxDaHdQUStqcnRwcTJVSnYzaTAvY2xuaS9ZMDNlZll6eFB2WmRCa0F6ZUFLQVR1a0VOQW9UQTg2eW5aM0dyVlFsM0V1L3lKLzhEMzNtbWVLUjNDejhBQUFBQVNVVk9SSzVDWUlJPScsXHJcbiAgICAgICAgICAgIGxlZnRIb29rOiAnZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFCSUFBQUFTQ0FJQUFBRFpyQmtBQUFBQ3EwbEVRVlI0WGczSE8yNG1WUkJIOFgvVnJmdjR1dDAyQmpTOEdTQkFRdXlDbUUwZ3dTTElDRmdDQkxBU0FoTGlpUWpRSUNGQk1BTVNGbjYzdis3N3FDcDg5RXNPNVUrL3J1cy8welFQUFc4dGhYUlFBQkxBanRIQUFaMmd6R2sySFJDRzFZTVB3Z2RmL2ZuWGoxZlgrTzc3MzJzL0Ewa3pid0VVQ1c1RmdoOEhHVmRGVE9uNUg3OU5pWi85OHJQQVRCVlM4T3pYNXhUZWhZc3hkM0tGd1hjR0VsalZPR1ZKOGE3VzRRNFlUOHNiMzN6NzB4ZGYvb0I4SHVmemhqSTBtMmEzSkx5a3VBeVY0Ukd4ckxWTjUrZi8zZHhTS1hKYzU5cWZVazc3ZGpoZFhoR3gwL21NQWcvcnBtMzBiWHE5N0ZwM3N1MW1yTWUxOWU0NkJMTHc5TmIxdzBXYVpvTytmUG1DTmJUVzNEMUZjamNpNzdDbm4zd01oQ0JwV1U3dnV3cW8zZXZnYWFyNlFIUnFmUlhLaFFpRUxGRzFPNkgzTVpVeUxwVEl4MjRVTWtOQVNicVp3WmlOM0FRc2dLaFo2L1ZZUjlPY0R3L2JsZzV4bUtxUG5JUkJMUVVadzRpSVdRS3lkUmNFY3ZMT09aMkE4ME52OFNSdjFpaUhRV1BYZXdienRtM01iT2JNWWdwR0NNd2l3Z3dBSWhKQ0FHQm1qTUQrS0RKSlRoSnpUSThUS1ptNUU5UzdVM2MwczZQcmtWdWp2WWJtMFFyMWljZUorR1o5NzcyT01hd05ETkFnVm04Y0FEY0FRU2d6Q1NpQ2haajhNUkx3WENUUHVmUUVDOFNsVktQV05USko0QlREY2RzNTh0Q2FNc1ZvSVE4TnEyQWdTOTd1VmcyaDd2MjlEejg2Vzg2TUdxQjkyMkYrZGpqc2RiV0UwZXUyMzNmYndGMG9wZDU3S1lVNDNsNWV3UTQzTjdkMTdDd1VISm5sc2w0UW1Vd2tKSktaNXVYYUxkQnJuemw2RUdiVGJWMm5WSWlNdU1VNDRFTXdNam43NkdNSGVXQUtoS3VMdnlYb0MrRjNJdlhyZnk5em1PL1dLeVVNNmMxcmlXVVk3YnRLaUErOVVreXZQbms3S0ZNUHRMei91Uzl2cmpWRkxOcVRVU0VKTkxONWhRTDdnSVhwc0d5dE8wZWtDZHVSL2ZnL2hWK29sVnFTbTNZQUFBQUFTVVZPUks1Q1lJST0nLFxyXG4gICAgICAgICAgICByaWdodFphZzogJ2RhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBQklBQUFBU0NBSUFBQURackJrQUFBQUN0MGxFUVZSNFhnM0xPVzZlVlJRRzRQY005MzdEUDloWWxrZ1VRNlJRc0FpRVdBQzdZQ1dVdEd5QUZxaHBFSFJJMENDQklqcEhKQlN4RWh6a3hQNkgrOTNwSFB6MEQzMzU4SXNidXlZL1hsQ3M3L1lVNXpqRlpnZEdwMDdCRURxNEtYd0EwUHZ1cUlmREdiVDB1NjkvK2hadnIvRHJIMWdjMVdFTndVQUdNM1JEY1hRR1JiQTkvZTNuSlBYM0Y1ZHFkWS9qYTh6dHgrKy8rWWhYNVM2UDQ3UkVPRFcyUHJhK3FzYk9SNVZGV3dqSFZQS0pKRjFGZXZQbjAxOSsrTzdqZVlvM2g1Vkc4VWE1T0xuQ2gyNVQ3K1J3WWZaYXZWejk5M3E1T2VoY2NmSW1uVjNucWRMakQ1NGdUaURBRjNjbkVKckJBYlpOU1MvL3ZUS1AxRmNUazBwcHNmWXprcEV0NThQbHM4dmIyOXZUYWJEV1laSnpqc3FuSit2SGp4OXRWdmYzcUczUVZwWEowWTVMMnhXbVliUHQ3VzVXZUs3UktVcUlDaWM3cG9RWTZxNEZ3MkFVRFN5a1VHVWxRMGNVeTNYVXFDeG04T2JlNFN5cFZvUndPQzdrTEU1c3pJb0JIc21sTzZOSk8zUXY1R0VvekkzSWlFMjVLMkVlTEFpWUFEaUJPd2k1MU5MZENSckdjUTV4VExWVWNnMUJWZUZzelh0YVdpbmtJQWNBTGlnUUR3SVNieTBWcTNkcFI4SUEycjFjS05mWlJUcTlGMmMxaUx1WXFRY0hnMkRtdmJhbTA4UVVHMVYwN3pBU3FEQ0xvdVNTRTB0b2pDYkdhYmxGb01na3BNeGh0OVF1a3ZJQ0FTSVh0Tkx5N3JERGFrVkRlR2VwenBJVXVwMW1sSVpzNURibzlPa25ud0VFTEdnWkpnQmhISERZSXgvdjgvcjk4K2QvUDJjVlZTT2tpdFJQTjl1WGZ6MGoyclJTaDFDWkRLUm1Gb21nbm5zNjJXeXZycStuY1R5MkpKL3JSWHY3NnZ4MDR5bXI4M3BjaTlnWVJZSVNTNE4zY2dSS3ZZYjE1RUhPSHp5OGZQYVBndE1lOHpIRzV5OHVINnkzd2E3TFBiTk9JSWtRUnBDT1p0N0t6YXRIRngrV3cwS2k5Tlg4WkhXeTdTbHRjaHR6MmFqMzJuaWNqWmtwTHIxbTc2MHZwNXROMmU4anRIWDBZZjRmM2RhKzFMNG9FRVFBQUFBQVNVVk9SSzVDWUlJPScsXHJcbiAgICAgICAgICAgIGxlZnRaYWc6ICdkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQUJJQUFBQVNDQUlBQUFEWnJCa0FBQUFDdEVsRVFWUjRYZzJMdTI1Y1pSaEY5M2Y1TDNPT1BlT1FtSnVEaEVJSERhL0NDL0I0U0hTSUNxUzBDRVJERTJNSkFZS0VtSVRFY2V3WnovbnZIN08wdGFxMTZlVEwrK1A2Nm5oQ0RlZ09BRWhnaEZZaERETTBoU2dvd1FrS2tESXdKaTBwUC83Mkc5ajI0dklpOFM3bk4zNGxZRzFWeU9LZzB1VktGRklpWS9YTCtSOHM5NzcrNm52dHRCTWtvdlREejkvTnAzRzNmVGx2WWtvRkpzd3pheHR5YTJoYVEwblVkR2EyUExZNkdELzkrdmpKK1krYkQrSzJ2Rnh0ckxTM2NRcTl0MXJMd1VaN0lwdU9BbVFROHU5L1BnRkRXYkc1NzA4L2pLL1RpOU96RXhFcmhZVlUxVE1yQ1pxVjdmWTJPcjIrdVE1SDd6Z1BNTlFGNUhMNyt1cFplRmVITFUvL2V0cnJhTTJZZWRRQkFYc25vUFhIbjh5cmNIVnpFMExJQ1dvRUNUUWYrU0cxOWRScUszdE1VeFNSWnRrSVp0cWJzYmxXK3ZGOC9QenkxWG9OdmR0aGpEYXM3SGU3c3dkbm8rSmtNMjF2OTk3cjZBMU1JS05Cd2lDaVhzc3FoTnNiY0F3UW9zUFRlZWsxTXpCYTk0N05tbmV3WWNHUjhPaDFzWkc4czVMdlFvQUNhSzBZZWdpKzk2NU1OV1hueElBeDBBZHlXbnFETWp1aDFLb0lrWUdOVVBxaDB6SGFZY3B3b2luMVppQkZqRkJWRVdMbTFwcHpUa1FBc0FRWVFkd2hUYXArZEJDUlh6a1c2ZzJsSXBkcVJrWUUxdjErQVpnRXVpUm84RzJZYUNCbzc1WnlsU0NkaU1ub1lQVmVmTzFrOEFZbTdybEFqVURpNy9aTjE4SElmZmJwNXpGTzIyVmYwYnpYZnNCUVV1NERJSjZQMS90L1hvd0I5UkdsNFhqOTRNNjJ6eSt2b3Uyc0RVeVN5aEpqVERYcEFlTDlZRktYYXhZbnVvTDJCUmZuenhnaGo3WUttNGxpSGtzekdrT1VqdHhJWWpLTTluY3BUblByNCtIRGo4aitVNS9CNVI1ay92ZnYzNDRtNnJmWEhiM05TcUt2ZHJzNXhLVVdlTjk1blVkNjlPaTl2cFFJMFB0ZlRBbjdlRS9lTEYwWTkyZVVqcmNESVNCbXJCamJna3JJRFBGWUNmb0NMUGdmbFhPanVJRUZnTVlBQUFBQVNVVk9SSzVDWUlJPSdcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgJ21vZGVybic6IHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAnIzAwMDAwMCcsXHJcbiAgICAgICAgYmFja2dyb3VuZEdyaWQ6ICdkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQUI0QUFBQWVDQUlBQUFDMFVqbjFBQUFBR1hSRldIUlRiMlowZDJGeVpRQkJaRzlpWlNCSmJXRm5aVkpsWVdSNWNjbGxQQUFBQTNScFZGaDBXRTFNT21OdmJTNWhaRzlpWlM1NGJYQUFBQUFBQUR3L2VIQmhZMnRsZENCaVpXZHBiajBpNzd1L0lpQnBaRDBpVnpWTk1FMXdRMlZvYVVoNmNtVlRlazVVWTNwcll6bGtJajgrSUR4NE9uaHRjRzFsZEdFZ2VHMXNibk02ZUQwaVlXUnZZbVU2Ym5NNmJXVjBZUzhpSUhnNmVHMXdkR3M5SWtGa2IySmxJRmhOVUNCRGIzSmxJRFV1TlMxak1ERTBJRGM1TGpFMU1UUTRNU3dnTWpBeE15OHdNeTh4TXkweE1qb3dPVG94TlNBZ0lDQWdJQ0FnSWo0Z1BISmtaanBTUkVZZ2VHMXNibk02Y21SbVBTSm9kSFJ3T2k4dmQzZDNMbmN6TG05eVp5OHhPVGs1THpBeUx6SXlMWEprWmkxemVXNTBZWGd0Ym5NaklqNGdQSEprWmpwRVpYTmpjbWx3ZEdsdmJpQnlaR1k2WVdKdmRYUTlJaUlnZUcxc2JuTTZlRzF3VFUwOUltaDBkSEE2THk5dWN5NWhaRzlpWlM1amIyMHZlR0Z3THpFdU1DOXRiUzhpSUhodGJHNXpPbk4wVW1WbVBTSm9kSFJ3T2k4dmJuTXVZV1J2WW1VdVkyOXRMM2hoY0M4eExqQXZjMVI1Y0dVdlVtVnpiM1Z5WTJWU1pXWWpJaUI0Yld4dWN6cDRiWEE5SW1oMGRIQTZMeTl1Y3k1aFpHOWlaUzVqYjIwdmVHRndMekV1TUM4aUlIaHRjRTFOT2s5eWFXZHBibUZzUkc5amRXMWxiblJKUkQwaWVHMXdMbVJwWkRwalpUZzBOelU0TUMwME9EazNMVFJrTmpBdE9XTmhZaTFtWlRrMU56UTVOemhpTmpraUlIaHRjRTFOT2tSdlkzVnRaVzUwU1VROUluaHRjQzVrYVdRNk1URXpPRVF3TURjNU1EUXlNVEZGTkRsQk16bEZOelk0UmpCQ05rTkVOek1pSUhodGNFMU5Pa2x1YzNSaGJtTmxTVVE5SW5odGNDNXBhV1E2TVRFek9FUXdNRFk1TURReU1URkZORGxCTXpsRk56WTRSakJDTmtORU56TWlJSGh0Y0RwRGNtVmhkRzl5Vkc5dmJEMGlRV1J2WW1VZ1VHaHZkRzl6YUc5d0lFTkRJREl3TVRRZ0tFMWhZMmx1ZEc5emFDa2lQaUE4ZUcxd1RVMDZSR1Z5YVhabFpFWnliMjBnYzNSU1pXWTZhVzV6ZEdGdVkyVkpSRDBpZUcxd0xtbHBaRHBsTkRSak9XWmlOQzB5TnpFNUxUUTNORFl0WW1SbU1pMHdNbVkyWlRBNFpqQXhNbVVpSUhOMFVtVm1PbVJ2WTNWdFpXNTBTVVE5SW5odGNDNWthV1E2TXpNd05UTkVPVGs1TURNMU1URkZORGxCTXpsRk56WTRSakJDTmtORU56TWlMejRnUEM5eVpHWTZSR1Z6WTNKcGNIUnBiMjQrSUR3dmNtUm1PbEpFUmo0Z1BDOTRPbmh0Y0cxbGRHRStJRHcvZUhCaFkydGxkQ0JsYm1ROUluSWlQejdZMDErekFBQUFNa2xFUVZSNDJtSmdHQVdqWUJTTWdrRUpHSWxVZCtqL1dqamJqakdZR0MxTXRIUDEwRFI2Rkl5Q1VUQUtCaWtBQ0RBQTBOb0RDTEdHakg4QUFBQUFTVVZPUks1Q1lJST0nLFxyXG4gICAgICAgIHByaW1hcnk6IG51bGwsXHJcbiAgICAgICAgc2Vjb25kYXJ5OiBudWxsLFxyXG4gICAgICAgIHN0cm9rZTogbnVsbCxcclxuICAgICAgICBibG9ja3M6IHtcclxuICAgICAgICAgICAgbGluZTogJyNmYTFlMWUnLFxyXG4gICAgICAgICAgICBzcXVhcmU6ICcjZjFmYTFlJyxcclxuICAgICAgICAgICAgYXJyb3c6ICcjZDgzOGNiJyxcclxuICAgICAgICAgICAgcmlnaHRIb29rOiAnI2Y1ODIxZicsXHJcbiAgICAgICAgICAgIGxlZnRIb29rOiAnIzQyYzZmMCcsXHJcbiAgICAgICAgICAgIHJpZ2h0WmFnOiAnIzRiZDgzOCcsXHJcbiAgICAgICAgICAgIGxlZnRaYWc6ICcjZmExZTFlJ1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICAncmV0cm8nOiB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogJyMwMDAwMDAnLFxyXG4gICAgICAgIGJhY2tncm91bmRHcmlkOiAnZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFCNEFBQUFlQ0FJQUFBQzBVam4xQUFBQUdYUkZXSFJUYjJaMGQyRnlaUUJCWkc5aVpTQkpiV0ZuWlZKbFlXUjVjY2xsUEFBQUEzUnBWRmgwV0UxTU9tTnZiUzVoWkc5aVpTNTRiWEFBQUFBQUFEdy9lSEJoWTJ0bGRDQmlaV2RwYmowaTc3dS9JaUJwWkQwaVZ6Vk5NRTF3UTJWb2FVaDZjbVZUZWs1VVkzcHJZemxrSWo4K0lEeDRPbmh0Y0cxbGRHRWdlRzFzYm5NNmVEMGlZV1J2WW1VNmJuTTZiV1YwWVM4aUlIZzZlRzF3ZEdzOUlrRmtiMkpsSUZoTlVDQkRiM0psSURVdU5TMWpNREUwSURjNUxqRTFNVFE0TVN3Z01qQXhNeTh3TXk4eE15MHhNam93T1RveE5TQWdJQ0FnSUNBZ0lqNGdQSEprWmpwU1JFWWdlRzFzYm5NNmNtUm1QU0pvZEhSd09pOHZkM2QzTG5jekxtOXlaeTh4T1RrNUx6QXlMekl5TFhKa1ppMXplVzUwWVhndGJuTWpJajRnUEhKa1pqcEVaWE5qY21sd2RHbHZiaUJ5WkdZNllXSnZkWFE5SWlJZ2VHMXNibk02ZUcxd1RVMDlJbWgwZEhBNkx5OXVjeTVoWkc5aVpTNWpiMjB2ZUdGd0x6RXVNQzl0YlM4aUlIaHRiRzV6T25OMFVtVm1QU0pvZEhSd09pOHZibk11WVdSdlltVXVZMjl0TDNoaGNDOHhMakF2YzFSNWNHVXZVbVZ6YjNWeVkyVlNaV1lqSWlCNGJXeHVjenA0YlhBOUltaDBkSEE2THk5dWN5NWhaRzlpWlM1amIyMHZlR0Z3THpFdU1DOGlJSGh0Y0UxTk9rOXlhV2RwYm1Gc1JHOWpkVzFsYm5SSlJEMGllRzF3TG1ScFpEcGpaVGcwTnpVNE1DMDBPRGszTFRSa05qQXRPV05oWWkxbVpUazFOelE1TnpoaU5qa2lJSGh0Y0UxTk9rUnZZM1Z0Wlc1MFNVUTlJbmh0Y0M1a2FXUTZNVEV6T0VRd01EYzVNRFF5TVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUlIaHRjRTFOT2tsdWMzUmhibU5sU1VROUluaHRjQzVwYVdRNk1URXpPRVF3TURZNU1EUXlNVEZGTkRsQk16bEZOelk0UmpCQ05rTkVOek1pSUhodGNEcERjbVZoZEc5eVZHOXZiRDBpUVdSdlltVWdVR2h2ZEc5emFHOXdJRU5ESURJd01UUWdLRTFoWTJsdWRHOXphQ2tpUGlBOGVHMXdUVTA2UkdWeWFYWmxaRVp5YjIwZ2MzUlNaV1k2YVc1emRHRnVZMlZKUkQwaWVHMXdMbWxwWkRwbE5EUmpPV1ppTkMweU56RTVMVFEzTkRZdFltUm1NaTB3TW1ZMlpUQTRaakF4TW1VaUlITjBVbVZtT21SdlkzVnRaVzUwU1VROUluaHRjQzVrYVdRNk16TXdOVE5FT1RrNU1ETTFNVEZGTkRsQk16bEZOelk0UmpCQ05rTkVOek1pTHo0Z1BDOXlaR1k2UkdWelkzSnBjSFJwYjI0K0lEd3ZjbVJtT2xKRVJqNGdQQzk0T25odGNHMWxkR0UrSUR3L2VIQmhZMnRsZENCbGJtUTlJbklpUHo3WTAxK3pBQUFBTWtsRVFWUjQybUpnR0FXallCU01na0VKR0lsVWQrai9XampiampHWUdDMU10SFAxMERSNkZJeUNVVEFLQmlrQUNEQUEwTm9EQ0xHR2pIOEFBQUFBU1VWT1JLNUNZSUk9JyxcclxuICAgICAgICBwcmltYXJ5OiBudWxsLFxyXG4gICAgICAgIHNlY29uZGFyeTogbnVsbCxcclxuICAgICAgICBzdHJva2U6ICcjMDAwMDAwJyxcclxuICAgICAgICBpbm5lclN0cm9rZTogJyMwMDAwMDAnLFxyXG4gICAgICAgIGJsb2Nrczoge1xyXG4gICAgICAgICAgICBsaW5lOiAnI2ZhMWUxZScsXHJcbiAgICAgICAgICAgIHNxdWFyZTogJyNmMWZhMWUnLFxyXG4gICAgICAgICAgICBhcnJvdzogJyNkODM4Y2InLFxyXG4gICAgICAgICAgICByaWdodEhvb2s6ICcjZjU4MjFmJyxcclxuICAgICAgICAgICAgbGVmdEhvb2s6ICcjNDJjNmYwJyxcclxuICAgICAgICAgICAgcmlnaHRaYWc6ICcjNGJkODM4JyxcclxuICAgICAgICAgICAgbGVmdFphZzogJyNmYTFlMWUnXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgICdtb25vY2hyb21lJzoge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICcjMDAwMDAwJyxcclxuICAgICAgICBiYWNrZ3JvdW5kR3JpZDogJ2RhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBQjRBQUFBZUNBSUFBQUMwVWpuMUFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBM1JwVkZoMFdFMU1PbU52YlM1aFpHOWlaUzU0YlhBQUFBQUFBRHcvZUhCaFkydGxkQ0JpWldkcGJqMGk3N3UvSWlCcFpEMGlWelZOTUUxd1EyVm9hVWg2Y21WVGVrNVVZM3ByWXpsa0lqOCtJRHg0T25odGNHMWxkR0VnZUcxc2JuTTZlRDBpWVdSdlltVTZibk02YldWMFlTOGlJSGc2ZUcxd2RHczlJa0ZrYjJKbElGaE5VQ0JEYjNKbElEVXVOUzFqTURFMElEYzVMakUxTVRRNE1Td2dNakF4TXk4d015OHhNeTB4TWpvd09Ub3hOU0FnSUNBZ0lDQWdJajRnUEhKa1pqcFNSRVlnZUcxc2JuTTZjbVJtUFNKb2RIUndPaTh2ZDNkM0xuY3pMbTl5Wnk4eE9UazVMekF5THpJeUxYSmtaaTF6ZVc1MFlYZ3Ribk1qSWo0Z1BISmtaanBFWlhOamNtbHdkR2x2YmlCeVpHWTZZV0p2ZFhROUlpSWdlRzFzYm5NNmVHMXdUVTA5SW1oMGRIQTZMeTl1Y3k1aFpHOWlaUzVqYjIwdmVHRndMekV1TUM5dGJTOGlJSGh0Ykc1ek9uTjBVbVZtUFNKb2RIUndPaTh2Ym5NdVlXUnZZbVV1WTI5dEwzaGhjQzh4TGpBdmMxUjVjR1V2VW1WemIzVnlZMlZTWldZaklpQjRiV3h1Y3pwNGJYQTlJbWgwZEhBNkx5OXVjeTVoWkc5aVpTNWpiMjB2ZUdGd0x6RXVNQzhpSUhodGNFMU5Pazl5YVdkcGJtRnNSRzlqZFcxbGJuUkpSRDBpZUcxd0xtUnBaRHBqWlRnME56VTRNQzAwT0RrM0xUUmtOakF0T1dOaFlpMW1aVGsxTnpRNU56aGlOamtpSUhodGNFMU5Pa1J2WTNWdFpXNTBTVVE5SW5odGNDNWthV1E2TVRFek9FUXdNRGM1TURReU1URkZORGxCTXpsRk56WTRSakJDTmtORU56TWlJSGh0Y0UxTk9rbHVjM1JoYm1ObFNVUTlJbmh0Y0M1cGFXUTZNVEV6T0VRd01EWTVNRFF5TVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUlIaHRjRHBEY21WaGRHOXlWRzl2YkQwaVFXUnZZbVVnVUdodmRHOXphRzl3SUVORElESXdNVFFnS0UxaFkybHVkRzl6YUNraVBpQThlRzF3VFUwNlJHVnlhWFpsWkVaeWIyMGdjM1JTWldZNmFXNXpkR0Z1WTJWSlJEMGllRzF3TG1scFpEcGxORFJqT1daaU5DMHlOekU1TFRRM05EWXRZbVJtTWkwd01tWTJaVEE0WmpBeE1tVWlJSE4wVW1WbU9tUnZZM1Z0Wlc1MFNVUTlJbmh0Y0M1a2FXUTZNek13TlRORU9UazVNRE0xTVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUx6NGdQQzl5WkdZNlJHVnpZM0pwY0hScGIyNCtJRHd2Y21SbU9sSkVSajRnUEM5NE9uaHRjRzFsZEdFK0lEdy9lSEJoWTJ0bGRDQmxibVE5SW5JaVB6N1kwMSt6QUFBQU1rbEVRVlI0Mm1KZ0dBV2pZQlNNZ2tFSkdJbFVkK2ovV2pqYmpqR1lHQzFNdEhQMTBEUjZGSXlDVVRBS0Jpa0FDREFBME5vRENMR0dqSDhBQUFBQVNVVk9SSzVDWUlJPScsXHJcbiAgICAgICAgcHJpbWFyeTogJyNmZmZmZmYnLFxyXG4gICAgICAgIHNlY29uZGFyeTogJyNmZmZmZmYnLFxyXG4gICAgICAgIHN0cm9rZTogJyMwMDAwMDAnLFxyXG4gICAgICAgIGlubmVyU3Ryb2tlOiAnIzAwMDAwMCdcclxuICAgIH0sXHJcbiAgICAnYWVyb2xhYic6IHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAnI2ZmZmZmZicsXHJcbiAgICAgICAgcHJpbWFyeTogJyNmZjdiMDAnLFxyXG4gICAgICAgIHNlY29uZGFyeTogJyMwMDAwMDAnXHJcbiAgICB9LFxyXG4gICAgJ2dhbWVib3knOiB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogJyNDNENGQTEnLFxyXG4gICAgICAgIHByaW1hcnk6IG51bGwsXHJcbiAgICAgICAgc2Vjb25kYXJ5OiBudWxsLFxyXG4gICAgICAgIHN0cm9rZTogJyM0MTQxNDEnLFxyXG4gICAgICAgIGlubmVyU3Ryb2tlOiAnIzQxNDE0MScsXHJcbiAgICAgICAgaW5uZXJTcXVhcmU6ICcjMDAwMDAwJyxcclxuICAgICAgICBibG9ja3M6IHtcclxuICAgICAgICAgICAgbGluZTogJyM4ODkyNkEnLFxyXG4gICAgICAgICAgICBzcXVhcmU6ICcjNTg1RTQ0JyxcclxuICAgICAgICAgICAgYXJyb3c6ICcjQTRBQzhDJyxcclxuICAgICAgICAgICAgcmlnaHRIb29rOiAnIzZCNzM1MycsXHJcbiAgICAgICAgICAgIGxlZnRIb29rOiAnIzZCNzM1MycsXHJcbiAgICAgICAgICAgIHJpZ2h0WmFnOiAnIzU5NUY0NScsXHJcbiAgICAgICAgICAgIGxlZnRaYWc6ICcjNTk1RjQ1J1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICAndmltJzoge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICcjMDAwMDAwJyxcclxuICAgICAgICBiYWNrZ3JvdW5kR3JpZDogJ2RhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBQjRBQUFBZUNBSUFBQUMwVWpuMUFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBM1JwVkZoMFdFMU1PbU52YlM1aFpHOWlaUzU0YlhBQUFBQUFBRHcvZUhCaFkydGxkQ0JpWldkcGJqMGk3N3UvSWlCcFpEMGlWelZOTUUxd1EyVm9hVWg2Y21WVGVrNVVZM3ByWXpsa0lqOCtJRHg0T25odGNHMWxkR0VnZUcxc2JuTTZlRDBpWVdSdlltVTZibk02YldWMFlTOGlJSGc2ZUcxd2RHczlJa0ZrYjJKbElGaE5VQ0JEYjNKbElEVXVOUzFqTURFMElEYzVMakUxTVRRNE1Td2dNakF4TXk4d015OHhNeTB4TWpvd09Ub3hOU0FnSUNBZ0lDQWdJajRnUEhKa1pqcFNSRVlnZUcxc2JuTTZjbVJtUFNKb2RIUndPaTh2ZDNkM0xuY3pMbTl5Wnk4eE9UazVMekF5THpJeUxYSmtaaTF6ZVc1MFlYZ3Ribk1qSWo0Z1BISmtaanBFWlhOamNtbHdkR2x2YmlCeVpHWTZZV0p2ZFhROUlpSWdlRzFzYm5NNmVHMXdUVTA5SW1oMGRIQTZMeTl1Y3k1aFpHOWlaUzVqYjIwdmVHRndMekV1TUM5dGJTOGlJSGh0Ykc1ek9uTjBVbVZtUFNKb2RIUndPaTh2Ym5NdVlXUnZZbVV1WTI5dEwzaGhjQzh4TGpBdmMxUjVjR1V2VW1WemIzVnlZMlZTWldZaklpQjRiV3h1Y3pwNGJYQTlJbWgwZEhBNkx5OXVjeTVoWkc5aVpTNWpiMjB2ZUdGd0x6RXVNQzhpSUhodGNFMU5Pazl5YVdkcGJtRnNSRzlqZFcxbGJuUkpSRDBpZUcxd0xtUnBaRHBqWlRnME56VTRNQzAwT0RrM0xUUmtOakF0T1dOaFlpMW1aVGsxTnpRNU56aGlOamtpSUhodGNFMU5Pa1J2WTNWdFpXNTBTVVE5SW5odGNDNWthV1E2TVRFek9FUXdNRGM1TURReU1URkZORGxCTXpsRk56WTRSakJDTmtORU56TWlJSGh0Y0UxTk9rbHVjM1JoYm1ObFNVUTlJbmh0Y0M1cGFXUTZNVEV6T0VRd01EWTVNRFF5TVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUlIaHRjRHBEY21WaGRHOXlWRzl2YkQwaVFXUnZZbVVnVUdodmRHOXphRzl3SUVORElESXdNVFFnS0UxaFkybHVkRzl6YUNraVBpQThlRzF3VFUwNlJHVnlhWFpsWkVaeWIyMGdjM1JTWldZNmFXNXpkR0Z1WTJWSlJEMGllRzF3TG1scFpEcGxORFJqT1daaU5DMHlOekU1TFRRM05EWXRZbVJtTWkwd01tWTJaVEE0WmpBeE1tVWlJSE4wVW1WbU9tUnZZM1Z0Wlc1MFNVUTlJbmh0Y0M1a2FXUTZNek13TlRORU9UazVNRE0xTVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUx6NGdQQzl5WkdZNlJHVnpZM0pwY0hScGIyNCtJRHd2Y21SbU9sSkVSajRnUEM5NE9uaHRjRzFsZEdFK0lEdy9lSEJoWTJ0bGRDQmxibVE5SW5JaVB6N1kwMSt6QUFBQU1rbEVRVlI0Mm1KZ0dBV2pZQlNNZ2tFSkdJbFVkK2ovV2pqYmpqR1lHQzFNdEhQMTBEUjZGSXlDVVRBS0Jpa0FDREFBME5vRENMR0dqSDhBQUFBQVNVVk9SSzVDWUlJPScsXHJcbiAgICAgICAgcHJpbWFyeTogJyNDMkZGQUUnLFxyXG4gICAgICAgIHNlY29uZGFyeTogJyNDMkZGQUUnLFxyXG4gICAgICAgIHN0cm9rZTogJyMwMDAwMDAnLFxyXG4gICAgICAgIHN0cm9rZVdpZHRoOiAzLFxyXG4gICAgICAgIGlubmVyU3Ryb2tlOiBudWxsXHJcbiAgICB9LFxyXG4gICAgJ3dhdGVyJzoge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICcjMDAwMDAwJyxcclxuICAgICAgICBiYWNrZ3JvdW5kR3JpZDogJ2RhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBQjRBQUFBZUNBSUFBQUMwVWpuMUFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBM1JwVkZoMFdFMU1PbU52YlM1aFpHOWlaUzU0YlhBQUFBQUFBRHcvZUhCaFkydGxkQ0JpWldkcGJqMGk3N3UvSWlCcFpEMGlWelZOTUUxd1EyVm9hVWg2Y21WVGVrNVVZM3ByWXpsa0lqOCtJRHg0T25odGNHMWxkR0VnZUcxc2JuTTZlRDBpWVdSdlltVTZibk02YldWMFlTOGlJSGc2ZUcxd2RHczlJa0ZrYjJKbElGaE5VQ0JEYjNKbElEVXVOUzFqTURFMElEYzVMakUxTVRRNE1Td2dNakF4TXk4d015OHhNeTB4TWpvd09Ub3hOU0FnSUNBZ0lDQWdJajRnUEhKa1pqcFNSRVlnZUcxc2JuTTZjbVJtUFNKb2RIUndPaTh2ZDNkM0xuY3pMbTl5Wnk4eE9UazVMekF5THpJeUxYSmtaaTF6ZVc1MFlYZ3Ribk1qSWo0Z1BISmtaanBFWlhOamNtbHdkR2x2YmlCeVpHWTZZV0p2ZFhROUlpSWdlRzFzYm5NNmVHMXdUVTA5SW1oMGRIQTZMeTl1Y3k1aFpHOWlaUzVqYjIwdmVHRndMekV1TUM5dGJTOGlJSGh0Ykc1ek9uTjBVbVZtUFNKb2RIUndPaTh2Ym5NdVlXUnZZbVV1WTI5dEwzaGhjQzh4TGpBdmMxUjVjR1V2VW1WemIzVnlZMlZTWldZaklpQjRiV3h1Y3pwNGJYQTlJbWgwZEhBNkx5OXVjeTVoWkc5aVpTNWpiMjB2ZUdGd0x6RXVNQzhpSUhodGNFMU5Pazl5YVdkcGJtRnNSRzlqZFcxbGJuUkpSRDBpZUcxd0xtUnBaRHBqWlRnME56VTRNQzAwT0RrM0xUUmtOakF0T1dOaFlpMW1aVGsxTnpRNU56aGlOamtpSUhodGNFMU5Pa1J2WTNWdFpXNTBTVVE5SW5odGNDNWthV1E2TVRFek9FUXdNRGM1TURReU1URkZORGxCTXpsRk56WTRSakJDTmtORU56TWlJSGh0Y0UxTk9rbHVjM1JoYm1ObFNVUTlJbmh0Y0M1cGFXUTZNVEV6T0VRd01EWTVNRFF5TVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUlIaHRjRHBEY21WaGRHOXlWRzl2YkQwaVFXUnZZbVVnVUdodmRHOXphRzl3SUVORElESXdNVFFnS0UxaFkybHVkRzl6YUNraVBpQThlRzF3VFUwNlJHVnlhWFpsWkVaeWIyMGdjM1JTWldZNmFXNXpkR0Z1WTJWSlJEMGllRzF3TG1scFpEcGxORFJqT1daaU5DMHlOekU1TFRRM05EWXRZbVJtTWkwd01tWTJaVEE0WmpBeE1tVWlJSE4wVW1WbU9tUnZZM1Z0Wlc1MFNVUTlJbmh0Y0M1a2FXUTZNek13TlRORU9UazVNRE0xTVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUx6NGdQQzl5WkdZNlJHVnpZM0pwY0hScGIyNCtJRHd2Y21SbU9sSkVSajRnUEM5NE9uaHRjRzFsZEdFK0lEdy9lSEJoWTJ0bGRDQmxibVE5SW5JaVB6N1kwMSt6QUFBQU1rbEVRVlI0Mm1KZ0dBV2pZQlNNZ2tFSkdJbFVkK2ovV2pqYmpqR1lHQzFNdEhQMTBEUjZGSXlDVVRBS0Jpa0FDREFBME5vRENMR0dqSDhBQUFBQVNVVk9SSzVDWUlJPScsXHJcbiAgICAgICAgcHJpbWFyeTogJyMzMzk5ZmYnLFxyXG4gICAgICAgIHNlY29uZGFyeTogJyMzMzk5ZmYnLFxyXG4gICAgICAgIHN0cm9rZTogJyMwMDAwMDAnLFxyXG4gICAgICAgIGlubmVyU3Ryb2tlOiAnIzAwMDAwMCdcclxuICAgIH0sXHJcbiAgICAnYWxpbWVudGF0aW9uJzoge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICcjMDAwMDAwJyxcclxuICAgICAgICBiYWNrZ3JvdW5kR3JpZDogJ2RhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBQjRBQUFBZUNBSUFBQUMwVWpuMUFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBM1JwVkZoMFdFMU1PbU52YlM1aFpHOWlaUzU0YlhBQUFBQUFBRHcvZUhCaFkydGxkQ0JpWldkcGJqMGk3N3UvSWlCcFpEMGlWelZOTUUxd1EyVm9hVWg2Y21WVGVrNVVZM3ByWXpsa0lqOCtJRHg0T25odGNHMWxkR0VnZUcxc2JuTTZlRDBpWVdSdlltVTZibk02YldWMFlTOGlJSGc2ZUcxd2RHczlJa0ZrYjJKbElGaE5VQ0JEYjNKbElEVXVOUzFqTURFMElEYzVMakUxTVRRNE1Td2dNakF4TXk4d015OHhNeTB4TWpvd09Ub3hOU0FnSUNBZ0lDQWdJajRnUEhKa1pqcFNSRVlnZUcxc2JuTTZjbVJtUFNKb2RIUndPaTh2ZDNkM0xuY3pMbTl5Wnk4eE9UazVMekF5THpJeUxYSmtaaTF6ZVc1MFlYZ3Ribk1qSWo0Z1BISmtaanBFWlhOamNtbHdkR2x2YmlCeVpHWTZZV0p2ZFhROUlpSWdlRzFzYm5NNmVHMXdUVTA5SW1oMGRIQTZMeTl1Y3k1aFpHOWlaUzVqYjIwdmVHRndMekV1TUM5dGJTOGlJSGh0Ykc1ek9uTjBVbVZtUFNKb2RIUndPaTh2Ym5NdVlXUnZZbVV1WTI5dEwzaGhjQzh4TGpBdmMxUjVjR1V2VW1WemIzVnlZMlZTWldZaklpQjRiV3h1Y3pwNGJYQTlJbWgwZEhBNkx5OXVjeTVoWkc5aVpTNWpiMjB2ZUdGd0x6RXVNQzhpSUhodGNFMU5Pazl5YVdkcGJtRnNSRzlqZFcxbGJuUkpSRDBpZUcxd0xtUnBaRHBqWlRnME56VTRNQzAwT0RrM0xUUmtOakF0T1dOaFlpMW1aVGsxTnpRNU56aGlOamtpSUhodGNFMU5Pa1J2WTNWdFpXNTBTVVE5SW5odGNDNWthV1E2TVRFek9FUXdNRGM1TURReU1URkZORGxCTXpsRk56WTRSakJDTmtORU56TWlJSGh0Y0UxTk9rbHVjM1JoYm1ObFNVUTlJbmh0Y0M1cGFXUTZNVEV6T0VRd01EWTVNRFF5TVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUlIaHRjRHBEY21WaGRHOXlWRzl2YkQwaVFXUnZZbVVnVUdodmRHOXphRzl3SUVORElESXdNVFFnS0UxaFkybHVkRzl6YUNraVBpQThlRzF3VFUwNlJHVnlhWFpsWkVaeWIyMGdjM1JTWldZNmFXNXpkR0Z1WTJWSlJEMGllRzF3TG1scFpEcGxORFJqT1daaU5DMHlOekU1TFRRM05EWXRZbVJtTWkwd01tWTJaVEE0WmpBeE1tVWlJSE4wVW1WbU9tUnZZM1Z0Wlc1MFNVUTlJbmh0Y0M1a2FXUTZNek13TlRORU9UazVNRE0xTVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUx6NGdQQzl5WkdZNlJHVnpZM0pwY0hScGIyNCtJRHd2Y21SbU9sSkVSajRnUEM5NE9uaHRjRzFsZEdFK0lEdy9lSEJoWTJ0bGRDQmxibVE5SW5JaVB6N1kwMSt6QUFBQU1rbEVRVlI0Mm1KZ0dBV2pZQlNNZ2tFSkdJbFVkK2ovV2pqYmpqR1lHQzFNdEhQMTBEUjZGSXlDVVRBS0Jpa0FDREFBME5vRENMR0dqSDhBQUFBQVNVVk9SSzVDWUlJPScsXHJcbiAgICAgICAgcHJpbWFyeTogJyMwMDk5MzMnLFxyXG4gICAgICAgIHNlY29uZGFyeTogJyMwMDk5MzMnLFxyXG4gICAgICAgIHN0cm9rZTogJyMwMDAwMDAnLFxyXG4gICAgICAgIGlubmVyU3Ryb2tlOiAnIzAwMDAwMCdcclxuICAgIH0sXHJcbiAgICAnZW5lcmd5Jzoge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICcjMDAwMDAwJyxcclxuICAgICAgICBiYWNrZ3JvdW5kR3JpZDogJ2RhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBQjRBQUFBZUNBSUFBQUMwVWpuMUFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBM1JwVkZoMFdFMU1PbU52YlM1aFpHOWlaUzU0YlhBQUFBQUFBRHcvZUhCaFkydGxkQ0JpWldkcGJqMGk3N3UvSWlCcFpEMGlWelZOTUUxd1EyVm9hVWg2Y21WVGVrNVVZM3ByWXpsa0lqOCtJRHg0T25odGNHMWxkR0VnZUcxc2JuTTZlRDBpWVdSdlltVTZibk02YldWMFlTOGlJSGc2ZUcxd2RHczlJa0ZrYjJKbElGaE5VQ0JEYjNKbElEVXVOUzFqTURFMElEYzVMakUxTVRRNE1Td2dNakF4TXk4d015OHhNeTB4TWpvd09Ub3hOU0FnSUNBZ0lDQWdJajRnUEhKa1pqcFNSRVlnZUcxc2JuTTZjbVJtUFNKb2RIUndPaTh2ZDNkM0xuY3pMbTl5Wnk4eE9UazVMekF5THpJeUxYSmtaaTF6ZVc1MFlYZ3Ribk1qSWo0Z1BISmtaanBFWlhOamNtbHdkR2x2YmlCeVpHWTZZV0p2ZFhROUlpSWdlRzFzYm5NNmVHMXdUVTA5SW1oMGRIQTZMeTl1Y3k1aFpHOWlaUzVqYjIwdmVHRndMekV1TUM5dGJTOGlJSGh0Ykc1ek9uTjBVbVZtUFNKb2RIUndPaTh2Ym5NdVlXUnZZbVV1WTI5dEwzaGhjQzh4TGpBdmMxUjVjR1V2VW1WemIzVnlZMlZTWldZaklpQjRiV3h1Y3pwNGJYQTlJbWgwZEhBNkx5OXVjeTVoWkc5aVpTNWpiMjB2ZUdGd0x6RXVNQzhpSUhodGNFMU5Pazl5YVdkcGJtRnNSRzlqZFcxbGJuUkpSRDBpZUcxd0xtUnBaRHBqWlRnME56VTRNQzAwT0RrM0xUUmtOakF0T1dOaFlpMW1aVGsxTnpRNU56aGlOamtpSUhodGNFMU5Pa1J2WTNWdFpXNTBTVVE5SW5odGNDNWthV1E2TVRFek9FUXdNRGM1TURReU1URkZORGxCTXpsRk56WTRSakJDTmtORU56TWlJSGh0Y0UxTk9rbHVjM1JoYm1ObFNVUTlJbmh0Y0M1cGFXUTZNVEV6T0VRd01EWTVNRFF5TVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUlIaHRjRHBEY21WaGRHOXlWRzl2YkQwaVFXUnZZbVVnVUdodmRHOXphRzl3SUVORElESXdNVFFnS0UxaFkybHVkRzl6YUNraVBpQThlRzF3VFUwNlJHVnlhWFpsWkVaeWIyMGdjM1JTWldZNmFXNXpkR0Z1WTJWSlJEMGllRzF3TG1scFpEcGxORFJqT1daaU5DMHlOekU1TFRRM05EWXRZbVJtTWkwd01tWTJaVEE0WmpBeE1tVWlJSE4wVW1WbU9tUnZZM1Z0Wlc1MFNVUTlJbmh0Y0M1a2FXUTZNek13TlRORU9UazVNRE0xTVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUx6NGdQQzl5WkdZNlJHVnpZM0pwY0hScGIyNCtJRHd2Y21SbU9sSkVSajRnUEM5NE9uaHRjRzFsZEdFK0lEdy9lSEJoWTJ0bGRDQmxibVE5SW5JaVB6N1kwMSt6QUFBQU1rbEVRVlI0Mm1KZ0dBV2pZQlNNZ2tFSkdJbFVkK2ovV2pqYmpqR1lHQzFNdEhQMTBEUjZGSXlDVVRBS0Jpa0FDREFBME5vRENMR0dqSDhBQUFBQVNVVk9SSzVDWUlJPScsXHJcbiAgICAgICAgcHJpbWFyeTogJyNmZmZmMWEnLFxyXG4gICAgICAgIHNlY29uZGFyeTogJyNmZmZmMWEnLFxyXG4gICAgICAgIHN0cm9rZTogJyMwMDAwMDAnLFxyXG4gICAgICAgIGlubmVyU3Ryb2tlOiAnIzAwMDAwMCdcclxuICAgIH0sXHJcbiAgICAnaWN0Jzoge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICcjMDAwMDAwJyxcclxuICAgICAgICBiYWNrZ3JvdW5kR3JpZDogJ2RhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBQjRBQUFBZUNBSUFBQUMwVWpuMUFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBM1JwVkZoMFdFMU1PbU52YlM1aFpHOWlaUzU0YlhBQUFBQUFBRHcvZUhCaFkydGxkQ0JpWldkcGJqMGk3N3UvSWlCcFpEMGlWelZOTUUxd1EyVm9hVWg2Y21WVGVrNVVZM3ByWXpsa0lqOCtJRHg0T25odGNHMWxkR0VnZUcxc2JuTTZlRDBpWVdSdlltVTZibk02YldWMFlTOGlJSGc2ZUcxd2RHczlJa0ZrYjJKbElGaE5VQ0JEYjNKbElEVXVOUzFqTURFMElEYzVMakUxTVRRNE1Td2dNakF4TXk4d015OHhNeTB4TWpvd09Ub3hOU0FnSUNBZ0lDQWdJajRnUEhKa1pqcFNSRVlnZUcxc2JuTTZjbVJtUFNKb2RIUndPaTh2ZDNkM0xuY3pMbTl5Wnk4eE9UazVMekF5THpJeUxYSmtaaTF6ZVc1MFlYZ3Ribk1qSWo0Z1BISmtaanBFWlhOamNtbHdkR2x2YmlCeVpHWTZZV0p2ZFhROUlpSWdlRzFzYm5NNmVHMXdUVTA5SW1oMGRIQTZMeTl1Y3k1aFpHOWlaUzVqYjIwdmVHRndMekV1TUM5dGJTOGlJSGh0Ykc1ek9uTjBVbVZtUFNKb2RIUndPaTh2Ym5NdVlXUnZZbVV1WTI5dEwzaGhjQzh4TGpBdmMxUjVjR1V2VW1WemIzVnlZMlZTWldZaklpQjRiV3h1Y3pwNGJYQTlJbWgwZEhBNkx5OXVjeTVoWkc5aVpTNWpiMjB2ZUdGd0x6RXVNQzhpSUhodGNFMU5Pazl5YVdkcGJtRnNSRzlqZFcxbGJuUkpSRDBpZUcxd0xtUnBaRHBqWlRnME56VTRNQzAwT0RrM0xUUmtOakF0T1dOaFlpMW1aVGsxTnpRNU56aGlOamtpSUhodGNFMU5Pa1J2WTNWdFpXNTBTVVE5SW5odGNDNWthV1E2TVRFek9FUXdNRGM1TURReU1URkZORGxCTXpsRk56WTRSakJDTmtORU56TWlJSGh0Y0UxTk9rbHVjM1JoYm1ObFNVUTlJbmh0Y0M1cGFXUTZNVEV6T0VRd01EWTVNRFF5TVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUlIaHRjRHBEY21WaGRHOXlWRzl2YkQwaVFXUnZZbVVnVUdodmRHOXphRzl3SUVORElESXdNVFFnS0UxaFkybHVkRzl6YUNraVBpQThlRzF3VFUwNlJHVnlhWFpsWkVaeWIyMGdjM1JTWldZNmFXNXpkR0Z1WTJWSlJEMGllRzF3TG1scFpEcGxORFJqT1daaU5DMHlOekU1TFRRM05EWXRZbVJtTWkwd01tWTJaVEE0WmpBeE1tVWlJSE4wVW1WbU9tUnZZM1Z0Wlc1MFNVUTlJbmh0Y0M1a2FXUTZNek13TlRORU9UazVNRE0xTVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUx6NGdQQzl5WkdZNlJHVnpZM0pwY0hScGIyNCtJRHd2Y21SbU9sSkVSajRnUEM5NE9uaHRjRzFsZEdFK0lEdy9lSEJoWTJ0bGRDQmxibVE5SW5JaVB6N1kwMSt6QUFBQU1rbEVRVlI0Mm1KZ0dBV2pZQlNNZ2tFSkdJbFVkK2ovV2pqYmpqR1lHQzFNdEhQMTBEUjZGSXlDVVRBS0Jpa0FDREFBME5vRENMR0dqSDhBQUFBQVNVVk9SSzVDWUlJPScsXHJcbiAgICAgICAgcHJpbWFyeTogJyNmZjc1MWEnLFxyXG4gICAgICAgIHNlY29uZGFyeTogJyNmZjc1MWEnLFxyXG4gICAgICAgIHN0cm9rZTogJyMwMDAwMDAnLFxyXG4gICAgICAgIGlubmVyU3Ryb2tlOiAnIzAwMDAwMCdcclxuICAgIH0sXHJcbiAgICAnbWl4Jzoge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICcjMDAwMDAwJyxcclxuICAgICAgICBiYWNrZ3JvdW5kR3JpZDogJ2RhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBQjRBQUFBZUNBSUFBQUMwVWpuMUFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBM1JwVkZoMFdFMU1PbU52YlM1aFpHOWlaUzU0YlhBQUFBQUFBRHcvZUhCaFkydGxkQ0JpWldkcGJqMGk3N3UvSWlCcFpEMGlWelZOTUUxd1EyVm9hVWg2Y21WVGVrNVVZM3ByWXpsa0lqOCtJRHg0T25odGNHMWxkR0VnZUcxc2JuTTZlRDBpWVdSdlltVTZibk02YldWMFlTOGlJSGc2ZUcxd2RHczlJa0ZrYjJKbElGaE5VQ0JEYjNKbElEVXVOUzFqTURFMElEYzVMakUxTVRRNE1Td2dNakF4TXk4d015OHhNeTB4TWpvd09Ub3hOU0FnSUNBZ0lDQWdJajRnUEhKa1pqcFNSRVlnZUcxc2JuTTZjbVJtUFNKb2RIUndPaTh2ZDNkM0xuY3pMbTl5Wnk4eE9UazVMekF5THpJeUxYSmtaaTF6ZVc1MFlYZ3Ribk1qSWo0Z1BISmtaanBFWlhOamNtbHdkR2x2YmlCeVpHWTZZV0p2ZFhROUlpSWdlRzFzYm5NNmVHMXdUVTA5SW1oMGRIQTZMeTl1Y3k1aFpHOWlaUzVqYjIwdmVHRndMekV1TUM5dGJTOGlJSGh0Ykc1ek9uTjBVbVZtUFNKb2RIUndPaTh2Ym5NdVlXUnZZbVV1WTI5dEwzaGhjQzh4TGpBdmMxUjVjR1V2VW1WemIzVnlZMlZTWldZaklpQjRiV3h1Y3pwNGJYQTlJbWgwZEhBNkx5OXVjeTVoWkc5aVpTNWpiMjB2ZUdGd0x6RXVNQzhpSUhodGNFMU5Pazl5YVdkcGJtRnNSRzlqZFcxbGJuUkpSRDBpZUcxd0xtUnBaRHBqWlRnME56VTRNQzAwT0RrM0xUUmtOakF0T1dOaFlpMW1aVGsxTnpRNU56aGlOamtpSUhodGNFMU5Pa1J2WTNWdFpXNTBTVVE5SW5odGNDNWthV1E2TVRFek9FUXdNRGM1TURReU1URkZORGxCTXpsRk56WTRSakJDTmtORU56TWlJSGh0Y0UxTk9rbHVjM1JoYm1ObFNVUTlJbmh0Y0M1cGFXUTZNVEV6T0VRd01EWTVNRFF5TVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUlIaHRjRHBEY21WaGRHOXlWRzl2YkQwaVFXUnZZbVVnVUdodmRHOXphRzl3SUVORElESXdNVFFnS0UxaFkybHVkRzl6YUNraVBpQThlRzF3VFUwNlJHVnlhWFpsWkVaeWIyMGdjM1JTWldZNmFXNXpkR0Z1WTJWSlJEMGllRzF3TG1scFpEcGxORFJqT1daaU5DMHlOekU1TFRRM05EWXRZbVJtTWkwd01tWTJaVEE0WmpBeE1tVWlJSE4wVW1WbU9tUnZZM1Z0Wlc1MFNVUTlJbmh0Y0M1a2FXUTZNek13TlRORU9UazVNRE0xTVRGRk5EbEJNemxGTnpZNFJqQkNOa05FTnpNaUx6NGdQQzl5WkdZNlJHVnpZM0pwY0hScGIyNCtJRHd2Y21SbU9sSkVSajRnUEM5NE9uaHRjRzFsZEdFK0lEdy9lSEJoWTJ0bGRDQmxibVE5SW5JaVB6N1kwMSt6QUFBQU1rbEVRVlI0Mm1KZ0dBV2pZQlNNZ2tFSkdJbFVkK2ovV2pqYmpqR1lHQzFNdEhQMTBEUjZGSXlDVVRBS0Jpa0FDREFBME5vRENMR0dqSDhBQUFBQVNVVk9SSzVDWUlJPScsXHJcbiAgICAgICAgcHJpbWFyeTogJyNlNmU2ZTYnLFxyXG4gICAgICAgIHNlY29uZGFyeTogJyNlNmU2ZTYnLFxyXG4gICAgICAgIHN0cm9rZTogJyMwMDAwMDAnLFxyXG4gICAgICAgIGlubmVyU3Ryb2tlOiAnIzAwMDAwMCdcclxuICAgIH0sXHJcbn07IiwidmFyIGpzb25Mb2FkZXIgPSByZXF1aXJlKFwiLi9qc29uTG9hZGVyLmpzXCIpO1xyXG52YXIgbGluZXNTdWJMZXZlbCA9IHJlcXVpcmUoXCIuL2xldmVscy9saW5lc1N1YkxldmVsLmpzXCIpO1xyXG52YXIgcG9pbnRzU3ViTGV2ZWwgPSByZXF1aXJlKFwiLi9sZXZlbHMvcG9pbnRzU3ViTGV2ZWwuanNcIik7XHJcbnZhciBzY29yZVJlZ2lzdGVyID0gcmVxdWlyZShcIi4vc2NvcmVSZWdpc3RlclwiKTtcclxuXHJcbnZhciBzY29yZVJlZ2lzdGVySW5zdGFuY2UgPSBuZXcgc2NvcmVSZWdpc3Rlci5zY29yZVJlZ2lzdGVyKCk7XHJcbnZhciBsZXZlbERhdGE7XHJcbnZhciBnbG9iYWxDb25maWcgPSBqc29uTG9hZGVyLmxvYWRKc29uRmlsZShcImNvbmYvZ2xvYmFsX2NvbmZpZy5qc29uXCIpO1xyXG5cclxuZnVuY3Rpb24gbGlnaHRlbkRhcmtlbkNvbG9yKGNvbCwgYW10KSB7XHJcblxyXG4gICAgdmFyIHVzZVBvdW5kID0gZmFsc2U7XHJcblxyXG4gICAgaWYgKGNvbFswXSA9PSBcIiNcIikge1xyXG4gICAgICAgIGNvbCA9IGNvbC5zbGljZSgxKTtcclxuICAgICAgICB1c2VQb3VuZCA9IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIG51bSA9IHBhcnNlSW50KGNvbCwgMTYpO1xyXG5cclxuICAgIHZhciByID0gKG51bSA+PiAxNikgKyBhbXQ7XHJcblxyXG4gICAgaWYgKHIgPiAyNTUpIHIgPSAyNTU7XHJcbiAgICBlbHNlIGlmIChyIDwgMCkgciA9IDA7XHJcblxyXG4gICAgdmFyIGIgPSAoKG51bSA+PiA4KSAmIDB4MDBGRikgKyBhbXQ7XHJcblxyXG4gICAgaWYgKGIgPiAyNTUpIGIgPSAyNTU7XHJcbiAgICBlbHNlIGlmIChiIDwgMCkgYiA9IDA7XHJcblxyXG4gICAgdmFyIGcgPSAobnVtICYgMHgwMDAwRkYpICsgYW10O1xyXG5cclxuICAgIGlmIChnID4gMjU1KSBnID0gMjU1O1xyXG4gICAgZWxzZSBpZiAoZyA8IDApIGcgPSAwO1xyXG5cclxuICAgIHJldHVybiAodXNlUG91bmQgPyBcIiNcIiA6IFwiXCIpICsgKGcgfCAoYiA8PCA4KSB8IChyIDw8IDE2KSkudG9TdHJpbmcoMTYpO1xyXG5cclxufVxyXG5cclxuZnVuY3Rpb24gc2V0TGV2ZWxOYW1lKGN1cnJlbnRMZXZlbCkge1xyXG4gICAgJChcIiNsZXZlbC1uYW1lXCIpLnRleHQoJC5pMThuKFwibGV2ZWxfXCIuY29uY2F0KGN1cnJlbnRMZXZlbCkpKTtcclxufVxyXG5cclxuZnVuY3Rpb24gc2V0QmxvY2tUZXh0Q29sb3IoYmxvY2tUZXh0Q29sb3IpIHtcclxuICAgIHZhciBibG9ja1RleHQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImJsb2NrLXRleHRcIik7XHJcbiAgICB2YXIgbGlnaHRCbG9ja1RleHRDb2xvciA9IGxpZ2h0ZW5EYXJrZW5Db2xvcihibG9ja1RleHRDb2xvciwgNzApOyAvLzMwJSBicmlnaHRlclxyXG4gICAgYmxvY2tUZXh0LnN0eWxlLmNvbG9yID0gbGlnaHRCbG9ja1RleHRDb2xvcjtcclxufVxyXG5cclxuZnVuY3Rpb24gc2V0QmxvY2tUZXh0KHNoYXBlVHlwZSwgY3VycmVudExldmVsKSB7XHJcbiAgICB2YXIgYmxvY2tUZXh0ID0gJC5pMThuKHNoYXBlVHlwZS5jb25jYXQoXCJfbGV2ZWxfXCIpLmNvbmNhdChjdXJyZW50TGV2ZWwpKTtcclxuICAgIHZhciBibG9ja1RleHRQb3M7XHJcbiAgICB2YXIgYXJyID0gYmxvY2tUZXh0LnNwbGl0KFwiflwiKTtcclxuXHJcbiAgICBibG9ja1RleHRQb3MgPSBnZXRSYW5kb21JbnQoMCwgYXJyLmxlbmd0aCk7XHJcblxyXG4gICAgaWYgKHR5cGVvZiBhcnJbYmxvY2tUZXh0UG9zXSA9PT0gXCJ1bmRlZmluZWRcIikge1xyXG4gICAgICAgIGJsb2NrVGV4dFBvcyA9IDA7XHJcbiAgICB9XHJcblxyXG4gICAgJChcIiNibG9jay10ZXh0XCIpLnRleHQoYXJyW2Jsb2NrVGV4dFBvc10pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBzZXRTcGVjaWFsQmxvY2tUZXh0KHByZWZpeCwgY3VycmVudExldmVsKSB7XHJcbiAgICB2YXIgYmxvY2tUZXh0cyA9ICQuaTE4bihwcmVmaXguY29uY2F0KFwiX2xldmVsX1wiKS5jb25jYXQoY3VycmVudExldmVsKSk7XHJcbiAgICB2YXIgc3BlY2lhbEJsb2NrVGV4dFBvcztcclxuICAgIHZhciBhcnIgPSBibG9ja1RleHRzLnNwbGl0KFwiflwiKTtcclxuXHJcbiAgICBzcGVjaWFsQmxvY2tUZXh0UG9zID0gZ2V0UmFuZG9tSW50KDAsIGFyci5sZW5ndGgpO1xyXG5cclxuICAgIGlmICh0eXBlb2YgYXJyW3NwZWNpYWxCbG9ja1RleHRQb3NdID09PSBcInVuZGVmaW5lZFwiKSB7XHJcbiAgICAgICAgc3BlY2lhbEJsb2NrVGV4dFBvcyA9IDA7XHJcbiAgICB9XHJcblxyXG4gICAgJChcIiNibG9jay10ZXh0XCIpLnRleHQoYXJyW3NwZWNpYWxCbG9ja1RleHRQb3NdKTtcclxufVxyXG5cclxuZnVuY3Rpb24gaGlkZUJsb2NrdGV4dCgpIHtcclxuICAgICQoXCIjYmxvY2stdGV4dFwiKS5oaWRlKCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHNob3dCbG9ja3RleHQoKSB7XHJcbiAgICAkKFwiI2Jsb2NrLXRleHRcIikudG9nZ2xlKCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdldFN1YkxldmVsVGV4dChsZXZlbCkge1xyXG4gICAgdmFyIGxldmVsVGV4dCA9ICQuaTE4bihcImRlc2NyaXB0aW9uX2xldmVsX1wiLmNvbmNhdChsZXZlbCkpO1xyXG4gICAgdmFyIHRvcmV0ID0gXCJcIjtcclxuICAgIHZhciBhcnIgPSBsZXZlbFRleHQuc3BsaXQoXCJ+XCIpO1xyXG4gICAgZm9yIChpbmRleCA9IDA7IGluZGV4IDwgYXJyLmxlbmd0aDsgKytpbmRleCkge1xyXG4gICAgICAgIHZhciBzdHIgPSBhcnJbaW5kZXhdLnJlcGxhY2UoL14vLCBcIjxsaT5cIikuY29uY2F0KFwiPC9saT5cIik7XHJcbiAgICAgICAgdG9yZXQgKz0gc3RyO1xyXG4gICAgfVxyXG4gICAgdG9yZXQucmVwbGFjZSgvXi8sIFwiPHVsPlwiKS5jb25jYXQoXCI8L3VsPlwiKTtcclxuICAgIHJldHVybiB0b3JldDtcclxufVxyXG5cclxuZnVuY3Rpb24gc2hvd1N1YkxldmVsRGVzY3JpcHRpb24oY3VycmVudExldmVsLCBsZXZlbFRleHQpIHtcclxuICAgIGhpZGVCbG9ja3RleHQoKTtcclxuICAgIHN3YWwoe1xyXG4gICAgICAgIHRpdGxlOiBcIk5pdmVsIFwiICsgY3VycmVudExldmVsICsgXCI6IFwiICsgJC5pMThuKFwibGV2ZWxfXCIuY29uY2F0KGN1cnJlbnRMZXZlbCkpLFxyXG4gICAgICAgIGh0bWw6IGxldmVsVGV4dCxcclxuICAgICAgICBhbGxvd091dHNpZGVDbGljazogZmFsc2UsXHJcbiAgICAgICAgYWxsb3dFc2NhcGVLZXk6IGZhbHNlLFxyXG4gICAgICAgIHRpbWVyOiA2MDAwLFxyXG5cclxuICAgICAgICBvbk9wZW46IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICB2YXIgYiA9IHN3YWwuZ2V0Q29uZmlybUJ1dHRvbigpO1xyXG4gICAgICAgICAgICBiLmhpZGRlbiA9IHRydWU7XHJcbiAgICAgICAgICAgIGIuZGlzYWJsZWQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH0pLnRoZW4oZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgc3dhbCh7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIk5pdmVsIFwiICsgY3VycmVudExldmVsICsgXCI6IFwiICsgJC5pMThuKFwibGV2ZWxfXCIuY29uY2F0KGN1cnJlbnRMZXZlbCkpLFxyXG4gICAgICAgICAgICBodG1sOiBsZXZlbFRleHQsXHJcbiAgICAgICAgICAgIGFsbG93T3V0c2lkZUNsaWNrOiBmYWxzZSxcclxuICAgICAgICAgICAgYWxsb3dFc2NhcGVLZXk6IGZhbHNlLFxyXG4gICAgICAgICAgICBhbmltYXRpb246IGZhbHNlXHJcbiAgICAgICAgfSkudGhlbih2YWx1ZSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgJGdhbWUuYmxvY2tyYWluKFwicmVzdW1lXCIpO1xyXG4gICAgICAgICAgICAgICAgc2hvd0Jsb2NrdGV4dCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFJldHVybnMgYSByYW5kb20gaW50ZWdlciBiZXR3ZWVuIG1pbiAoaW5jbHVzaXZlKSBhbmQgbWF4IChpbmNsdXNpdmUpXHJcbiAqIFVzaW5nIE1hdGgucm91bmQoKSB3aWxsIGdpdmUgeW91IGEgbm9uLXVuaWZvcm0gZGlzdHJpYnV0aW9uIVxyXG4gKi9cclxuZnVuY3Rpb24gZ2V0UmFuZG9tSW50KG1pbiwgbWF4KSB7XHJcbiAgICByZXR1cm4gKE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIChtYXggLSBtaW4gKyAxKSkgKyBtaW4pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRSYW5kb21EZWNpbWFsKG1pbiwgbWF4KSB7XHJcbiAgICByZXR1cm4gKE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIChtYXggLSBtaW4gKyAxKSkgKyBtaW4pIC8gMTA7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdlblJhbmQobWluLCBtYXgsIGRlY2ltYWxQbGFjZXMpIHtcclxuICAgIHZhciByYW5kID0gTWF0aC5yYW5kb20oKSAqIChtYXggLSBtaW4pICsgbWluO1xyXG4gICAgdmFyIHBvd2VyID0gTWF0aC5wb3coMTAsIGRlY2ltYWxQbGFjZXMpO1xyXG4gICAgcmV0dXJuIE1hdGguZmxvb3IocmFuZCAqIHBvd2VyKSAvIHBvd2VyO1xyXG59XHJcblxyXG5mdW5jdGlvbiBzYXZlU3ViTGV2ZWxTY29yZShsZXZlbCwgc3ViTGV2ZWwsIHNjb3JlKSB7XHJcbiAgICB2YXIgc3ViTGV2ZWxOdW0gPSBzdWJMZXZlbC5zcGxpdChcIl9cIilbMV0gLSAxO1xyXG4gICAgc2NvcmVSZWdpc3Rlckluc3RhbmNlLnNldFN1YkxldmVsU2NvcmUobGV2ZWwsIHN1YkxldmVsTnVtLCBzY29yZSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHNhdmVTdWJMZXZlbExpbmVzKGxldmVsLCBzdWJMZXZlbCwgbnVtTGluZXMpIHtcclxuICAgIHZhciBzdWJMZXZlbE51bSA9IHN1YkxldmVsLnNwbGl0KFwiX1wiKVsxXSAtIDE7XHJcbiAgICBzY29yZVJlZ2lzdGVySW5zdGFuY2Uuc2V0U3ViTGV2ZWxMaW5lcyhsZXZlbCwgc3ViTGV2ZWxOdW0sIG51bUxpbmVzKTtcclxufVxyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XHJcbiAgICB2YXIgbGV2ZWxTY3JpcHQgPSBzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFwibGV2ZWxGaWxlXCIpO1xyXG5cclxuICAgIHZhciBsZXZlbHNEZWZpbnRpb25EaXIgPSBnbG9iYWxDb25maWcubGV2ZWxzX2RlZmluaXRpb25fZGlyO1xyXG5cclxuICAgIHZhciBsZXZlbFBvc0xvYWRlZCA9IDA7XHJcblxyXG4gICAgbGV2ZWxTY3JpcHQgPSBsZXZlbHNEZWZpbnRpb25EaXIgKyBsZXZlbFNjcmlwdDtcclxuXHJcbiAgICBsZXZlbERhdGEgPSBqc29uTG9hZGVyLmxvYWRKc29uRmlsZShsZXZlbFNjcmlwdCk7XHJcblxyXG4gICAgdmFyIGxldmVsc1BhcmFtcyA9IGxldmVsRGF0YS5sZXZlbHNfcGFyYW1zO1xyXG5cclxuICAgIHZhciBwYXJhbXMgPSBsZXZlbHNQYXJhbXNbbGV2ZWxQb3NMb2FkZWRdO1xyXG5cclxuICAgIHZhciBzdWJMZXZlbCA9IG5ldyBsaW5lc1N1YkxldmVsLkxpbmVzU3ViTGV2ZWwocGFyYW1zKTtcclxuXHJcbiAgICB2YXIgbnVtTGluZXNMZXZlbCA9IDA7XHJcblxyXG4gICAgdmFyIHJhbmROdW07XHJcblxyXG4gICAgdmFyIHNwZWNpYWxCbG9ja1RleHRTaG93biA9IGZhbHNlO1xyXG5cclxuICAgICRnYW1lID0gJChcIiN0ZXRyaXMtZGRoaFwiKS5ibG9ja3JhaW4oe1xyXG4gICAgICAgIHNwZWVkOiBzdWJMZXZlbC5nZXRTcGVlZCgpLFxyXG4gICAgICAgIGRpZmZpY3VsdHk6IHN1YkxldmVsLmdldERpZmZpY3VsdHkoKSxcclxuICAgICAgICB0aGVtZTogc3ViTGV2ZWwuZ2V0VGhlbWUoKSxcclxuICAgICAgICBwbGF5VGV4dDogJC5pMThuKGdsb2JhbENvbmZpZy5wbGF5X3RleHQpLFxyXG4gICAgICAgIHBsYXlCdXR0b25UZXh0OiAkLmkxOG4oZ2xvYmFsQ29uZmlnLnBsYXlfYnV0dG9uX3RleHQpLFxyXG4gICAgICAgIGdhbWVPdmVyVGV4dDogJC5pMThuKGdsb2JhbENvbmZpZy5nYW1lX292ZXJfdGV4dCksXHJcbiAgICAgICAgcmVzdGFydEJ1dHRvblRleHQ6ICQuaTE4bihnbG9iYWxDb25maWcucmVzdGFydF9idXR0b25fdGV4dCksXHJcbiAgICAgICAgc2NvcmVUZXh0OiAkLmkxOG4oZ2xvYmFsQ29uZmlnLnNjb3JlX3RleHQpLFxyXG4gICAgICAgIGJsb2NrV2lkdGg6IGdsb2JhbENvbmZpZy5kZWZhdWx0X2Jsb2NrX3dpZHRoLFxyXG4gICAgICAgIG9ic3RhY2xlczogc3ViTGV2ZWwuZ2V0T2JzdGFjbGVzKCksXHJcbiAgICAgICAgbnVtT2JzdGFjbGVzOiBzdWJMZXZlbC5nZXROdW1PYnN0YWNsZXMoKSxcclxuICAgICAgICBwYXVzZUJ1dHRvbjogc3ViTGV2ZWwuZ2V0U2hvd1BhdXNlQnV0dG9uKCksXHJcbiAgICAgICAgc2hhcGVUeXBlUHJvYnM6IHN1YkxldmVsLmdldFNoYXBlVHlwZVByb2JhYmlsaXRpZXMoKSxcclxuXHJcbiAgICAgICAgb25TdGFydDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICRnYW1lLmJsb2NrcmFpbihcInRvdWNoQ29udHJvbHNcIiwgdHJ1ZSk7XHJcbiAgICAgICAgICAgICRnYW1lLmJsb2NrcmFpbihcInBhdXNlXCIpO1xyXG4gICAgICAgICAgICBzZXRMZXZlbE5hbWUoc3ViTGV2ZWwuZ2V0U3ViTGV2ZWwoKSk7XHJcbiAgICAgICAgICAgIHZhciBsZXZlbFRleHQgPSBnZXRTdWJMZXZlbFRleHQoc3ViTGV2ZWwuZ2V0U3ViTGV2ZWwoKSk7XHJcbiAgICAgICAgICAgIHNob3dTdWJMZXZlbERlc2NyaXB0aW9uKHN1YkxldmVsLmdldFN1YkxldmVsKCksIGxldmVsVGV4dCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgb25SZXN0YXJ0OiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgJGdhbWUuYmxvY2tyYWluKFwidG91Y2hDb250cm9sc1wiLCB0cnVlKTtcclxuICAgICAgICAgICAgc2V0TGV2ZWxOYW1lKHN1YkxldmVsLmdldFN1YkxldmVsKCkpO1xyXG4gICAgICAgICAgICAkZ2FtZS5ibG9ja3JhaW4oXCJyZXN1bWVcIik7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgb25MaW5lOiBmdW5jdGlvbihsaW5lcywgc2NvcmVJbmNyZW1lbnQsIHNjb3JlKSB7XHJcbiAgICAgICAgICAgIG51bUxpbmVzTGV2ZWwgKz0gbGluZXM7XHJcbiAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICAgIHN1YkxldmVsLmlzTGV2ZWxDb21wbGV0ZWQobnVtTGluZXNMZXZlbCkgJiZcclxuICAgICAgICAgICAgICAgICFzdWJMZXZlbC5pc0ZpbmFsTGV2ZWwoKVxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgICRnYW1lLmJsb2NrcmFpbihcInBhdXNlXCIpO1xyXG4gICAgICAgICAgICAgICAgJGdhbWUuYmxvY2tyYWluKFwibGV2ZWxPdmVyXCIpO1xyXG4gICAgICAgICAgICAgICAgaGlkZUJsb2NrdGV4dCgpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJsZXZlbCBwYXNzZWRcIik7XHJcbiAgICAgICAgICAgICAgICBzYXZlU3ViTGV2ZWxTY29yZShsZXZlbERhdGEubGV2ZWwsIHN1YkxldmVsLmdldFN1YkxldmVsKCksIHNjb3JlKTtcclxuICAgICAgICAgICAgICAgIHNhdmVTdWJMZXZlbExpbmVzKGxldmVsRGF0YS5sZXZlbCwgc3ViTGV2ZWwuZ2V0U3ViTGV2ZWwoKSwgbnVtTGluZXNMZXZlbCk7XHJcblxyXG4gICAgICAgICAgICAgICAgbnVtTGluZXNMZXZlbCA9IDA7XHJcbiAgICAgICAgICAgICAgICBsZXZlbFBvc0xvYWRlZCsrO1xyXG5cclxuICAgICAgICAgICAgICAgIHBhcmFtcyA9IGxldmVsc1BhcmFtc1tsZXZlbFBvc0xvYWRlZF07XHJcblxyXG4gICAgICAgICAgICAgICAgc3ViTGV2ZWwgPSBuZXcgbGluZXNTdWJMZXZlbC5MaW5lc1N1YkxldmVsKHBhcmFtcyk7XHJcblxyXG4gICAgICAgICAgICAgICAgc3dhbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICQuaTE4bihcImxldmVsX3dvblwiKSxcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBcInN1Y2Nlc3NcIixcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0OiAkLmkxOG4oXCJuZXh0X2xldmVsXCIpXHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKHZhbHVlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gUmVzdGFydCB0aGUgZ2FtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAkZ2FtZS5ibG9ja3JhaW4oXCJzcGVlZFwiLCBzdWJMZXZlbC5nZXRTcGVlZCgpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJGdhbWUuYmxvY2tyYWluKFwiZGlmZmljdWx0eVwiLCBzdWJMZXZlbC5nZXREaWZmaWN1bHR5KCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkZ2FtZS5ibG9ja3JhaW4oXCJvYnN0YWNsZXNcIiwgc3ViTGV2ZWwuZ2V0T2JzdGFjbGVzKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkZ2FtZS5ibG9ja3JhaW4oXCJudW1PYnN0YWNsZXNcIiwgc3ViTGV2ZWwuZ2V0TnVtT2JzdGFjbGVzKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkZ2FtZS5ibG9ja3JhaW4oXCJwYXVzZUJ1dHRvblwiLCBzdWJMZXZlbC5nZXRTaG93UGF1c2VCdXR0b24oKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICRnYW1lLmJsb2NrcmFpbihcInRoZW1lXCIsIHN1YkxldmVsLmdldFRoZW1lKCkpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgJGdhbWUuYmxvY2tyYWluKFwic3RhcnRcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoXHJcbiAgICAgICAgICAgICAgICBzdWJMZXZlbC5pc0xldmVsQ29tcGxldGVkKG51bUxpbmVzTGV2ZWwpICYmXHJcbiAgICAgICAgICAgICAgICBzdWJMZXZlbC5pc0ZpbmFsTGV2ZWwoKVxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgICRnYW1lLmJsb2NrcmFpbihcInBhdXNlXCIpO1xyXG4gICAgICAgICAgICAgICAgJGdhbWUuYmxvY2tyYWluKFwibGV2ZWxPdmVyXCIpO1xyXG4gICAgICAgICAgICAgICAgaGlkZUJsb2NrdGV4dCgpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJmaW5hbCBsZXZlbCBwYXNzZWRcIik7XHJcbiAgICAgICAgICAgICAgICBzYXZlU3ViTGV2ZWxTY29yZShsZXZlbERhdGEubGV2ZWwsIHN1YkxldmVsLmdldFN1YkxldmVsKCksIHNjb3JlKTtcclxuICAgICAgICAgICAgICAgIHNhdmVTdWJMZXZlbExpbmVzKGxldmVsRGF0YS5sZXZlbCwgc3ViTGV2ZWwuZ2V0U3ViTGV2ZWwoKSwgbnVtTGluZXNMZXZlbCk7XHJcblxyXG4gICAgICAgICAgICAgICAgc3dhbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJzdWNjZXNzXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dDogJC5pMThuKFwiZ2FtZV93b25fXCIuY29uY2F0KGxldmVsRGF0YS5sZXZlbCkpXHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKHZhbHVlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGxldmVsRGF0YS5uZXh0X2xldmVsX2ZpbGUgIT0gXCJcIikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGxldmVsc1VubG9ja2VkID0gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFwibGV2ZWxzVW5sb2NrZWRcIikpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGxldmVsVW5sb2NrZWQgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogbGV2ZWxEYXRhLm5leHRfbGV2ZWxfY29kZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG93bjogZmFsc2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXZlbHNVbmxvY2tlZC5wdXNoKGxldmVsVW5sb2NrZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShcImxldmVsc1VubG9ja2VkXCIsIEpTT04uc3RyaW5naWZ5KGxldmVsc1VubG9ja2VkKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGxldmVsRmlsZXMgPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJsZXZlbEZpbGVzXCIpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBuZXh0TGV2ZWxDb2RlID0gbGV2ZWxEYXRhLm5leHRfbGV2ZWxfY29kZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldmVsRmlsZXNbbmV4dExldmVsQ29kZV0gPSBsZXZlbERhdGEubmV4dF9sZXZlbF9maWxlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShcImxldmVsRmlsZXNcIiwgSlNPTi5zdHJpbmdpZnkobGV2ZWxGaWxlcykpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjb2xvcnMgPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJjb2xvcnNcIikpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3JzW25leHRMZXZlbENvZGVdID0gZ2xvYmFsQ29uZmlnLnVubG9ja2VkX2xldmVsX2NvbG9yO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShcImNvbG9yc1wiLCBKU09OLnN0cmluZ2lmeShjb2xvcnMpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgbG9jYXRpb24ucmVwbGFjZShcImluZGV4Lmh0bWxcIik7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgb25OZXh0OiBmdW5jdGlvbihuZXh0KSB7XHJcbiAgICAgICAgICAgIGlmICh0eXBlb2YgbmV4dCAhPSBcInVuZGVmaW5lZFwiICYmIHR5cGVvZiAkZ2FtZSAhPSBcInVuZGVmaW5lZFwiKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHNwZWNpYWxCbG9ja1RleHRTaG93bikge1xyXG4gICAgICAgICAgICAgICAgICAgICRnYW1lLmJsb2NrcmFpbihcInRoZW1lXCIsIHN1YkxldmVsLmdldFRoZW1lKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNldEJsb2NrVGV4dENvbG9yKHN1YkxldmVsLmdldEJsb2NrVGV4dENvbG9yKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNwZWNpYWxCbG9ja1RleHRTaG93biA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHJhbmROdW0gPSBnZW5SYW5kKDAsIDEsIDIpO1xyXG5cclxuICAgICAgICAgICAgICAgIHZhciBzcGVjaWFsUGllY2VzID0gc3ViTGV2ZWwuZ2V0U3BlY2lhbFBpZWNlcygpO1xyXG4gICAgICAgICAgICAgICAgdmFyIGFjY3VtdWxhdGVkUHJvYiA9IDA7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBzcGVjaWFsUGllY2VzICE9IFwidW5kZWZpbmVkXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHNwZWNpYWxQaWVjZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWNjdW11bGF0ZWRQcm9iICs9IHNwZWNpYWxQaWVjZXNbaV0ucGllY2VfcHJvYmFiaWxpdHk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyYW5kTnVtIDw9IGFjY3VtdWxhdGVkUHJvYikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGdhbWUuYmxvY2tyYWluKCd0aGVtZScsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAnIzAwMDAwMCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZEdyaWQ6ICdkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQUI0QUFBQWVDQUlBQUFDMFVqbjFBQUFBR1hSRldIUlRiMlowZDJGeVpRQkJaRzlpWlNCSmJXRm5aVkpsWVdSNWNjbGxQQUFBQTNScFZGaDBXRTFNT21OdmJTNWhaRzlpWlM1NGJYQUFBQUFBQUR3L2VIQmhZMnRsZENCaVpXZHBiajBpNzd1L0lpQnBaRDBpVnpWTk1FMXdRMlZvYVVoNmNtVlRlazVVWTNwcll6bGtJajgrSUR4NE9uaHRjRzFsZEdFZ2VHMXNibk02ZUQwaVlXUnZZbVU2Ym5NNmJXVjBZUzhpSUhnNmVHMXdkR3M5SWtGa2IySmxJRmhOVUNCRGIzSmxJRFV1TlMxak1ERTBJRGM1TGpFMU1UUTRNU3dnTWpBeE15OHdNeTh4TXkweE1qb3dPVG94TlNBZ0lDQWdJQ0FnSWo0Z1BISmtaanBTUkVZZ2VHMXNibk02Y21SbVBTSm9kSFJ3T2k4dmQzZDNMbmN6TG05eVp5OHhPVGs1THpBeUx6SXlMWEprWmkxemVXNTBZWGd0Ym5NaklqNGdQSEprWmpwRVpYTmpjbWx3ZEdsdmJpQnlaR1k2WVdKdmRYUTlJaUlnZUcxc2JuTTZlRzF3VFUwOUltaDBkSEE2THk5dWN5NWhaRzlpWlM1amIyMHZlR0Z3THpFdU1DOXRiUzhpSUhodGJHNXpPbk4wVW1WbVBTSm9kSFJ3T2k4dmJuTXVZV1J2WW1VdVkyOXRMM2hoY0M4eExqQXZjMVI1Y0dVdlVtVnpiM1Z5WTJWU1pXWWpJaUI0Yld4dWN6cDRiWEE5SW1oMGRIQTZMeTl1Y3k1aFpHOWlaUzVqYjIwdmVHRndMekV1TUM4aUlIaHRjRTFOT2s5eWFXZHBibUZzUkc5amRXMWxiblJKUkQwaWVHMXdMbVJwWkRwalpUZzBOelU0TUMwME9EazNMVFJrTmpBdE9XTmhZaTFtWlRrMU56UTVOemhpTmpraUlIaHRjRTFOT2tSdlkzVnRaVzUwU1VROUluaHRjQzVrYVdRNk1URXpPRVF3TURjNU1EUXlNVEZGTkRsQk16bEZOelk0UmpCQ05rTkVOek1pSUhodGNFMU5Pa2x1YzNSaGJtTmxTVVE5SW5odGNDNXBhV1E2TVRFek9FUXdNRFk1TURReU1URkZORGxCTXpsRk56WTRSakJDTmtORU56TWlJSGh0Y0RwRGNtVmhkRzl5Vkc5dmJEMGlRV1J2WW1VZ1VHaHZkRzl6YUc5d0lFTkRJREl3TVRRZ0tFMWhZMmx1ZEc5emFDa2lQaUE4ZUcxd1RVMDZSR1Z5YVhabFpFWnliMjBnYzNSU1pXWTZhVzV6ZEdGdVkyVkpSRDBpZUcxd0xtbHBaRHBsTkRSak9XWmlOQzB5TnpFNUxUUTNORFl0WW1SbU1pMHdNbVkyWlRBNFpqQXhNbVVpSUhOMFVtVm1PbVJ2WTNWdFpXNTBTVVE5SW5odGNDNWthV1E2TXpNd05UTkVPVGs1TURNMU1URkZORGxCTXpsRk56WTRSakJDTmtORU56TWlMejRnUEM5eVpHWTZSR1Z6WTNKcGNIUnBiMjQrSUR3dmNtUm1PbEpFUmo0Z1BDOTRPbmh0Y0cxbGRHRStJRHcvZUhCaFkydGxkQ0JsYm1ROUluSWlQejdZMDErekFBQUFNa2xFUVZSNDJtSmdHQVdqWUJTTWdrRUpHSWxVZCtqL1dqamJqakdZR0MxTXRIUDEwRFI2Rkl5Q1VUQUtCaWtBQ0RBQTBOb0RDTEdHakg4QUFBQUFTVVZPUks1Q1lJST0nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaW1hcnk6IHNwZWNpYWxQaWVjZXNbaV0uY29sb3IsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2Vjb25kYXJ5OiBzdWJMZXZlbC5nZXRCbG9ja1RleHRDb2xvcigpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0cm9rZTogJyMwMDAwMDAnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlubmVyU3Ryb2tlOiAnIzAwMDAwMCdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0QmxvY2tUZXh0Q29sb3Ioc3BlY2lhbFBpZWNlc1tpXS5jb2xvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHNwZWNpYWxQaWVjZXNbaV0ucGllY2VfdHlwZSA9PT0gXCJudW1iZXJcIikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBsZXYgPSBsZXZlbERhdGEubGV2ZWwgKyBcIl9cIiArIHNwZWNpYWxQaWVjZXNbaV0ucGllY2VfdHlwZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRCbG9ja1RleHQobmV4dC5zaGFwZVR5cGUsIGxldik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldFNwZWNpYWxCbG9ja1RleHQoc3BlY2lhbFBpZWNlc1tpXS5waWVjZV90eXBlLCBzdWJMZXZlbC5nZXRTdWJMZXZlbCgpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNwZWNpYWxCbG9ja1RleHRTaG93biA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICghc3BlY2lhbEJsb2NrVGV4dFNob3duKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0QmxvY2tUZXh0Q29sb3Ioc3ViTGV2ZWwuZ2V0QmxvY2tUZXh0Q29sb3IoKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0QmxvY2tUZXh0KG5leHQuc2hhcGVUeXBlLCBzdWJMZXZlbC5nZXRTdWJMZXZlbCgpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIG1vZHVsZS5leHBvcnRzLnVwZGF0ZVBhZ2VFbGVtZW50cyA9IGZ1bmN0aW9uIHVwZGF0ZVBhZ2VFbGVtZW50cygpIHtcclxuICAgICAgICAkKFwiI3RldHJpcy1oZWFkZXJcIikudGV4dCgkLmkxOG4oXCJ0ZXRyaXNfaGVhZGVyXCIpKTtcclxuICAgICAgICAkKFwiI2dhbWUtaW5zdHJ1Y3Rpb25zXCIpLnRleHQoJC5pMThuKFwiZ2FtZV9pbnN0cnVjdGlvbnNcIikpO1xyXG4gICAgICAgICQoXCIuYmxvY2tyYWluLXN0YXJ0LW1zZ1wiKS50ZXh0KCQuaTE4bihnbG9iYWxDb25maWcucGxheV90ZXh0KSk7XHJcbiAgICAgICAgJChcIi5ibG9ja3JhaW4tc3RhcnQtYnRuXCIpLnRleHQoJC5pMThuKGdsb2JhbENvbmZpZy5wbGF5X2J1dHRvbl90ZXh0KSk7XHJcbiAgICAgICAgJChcIi5ibG9ja3JhaW4tc2NvcmUtbXNnXCIpLnRleHQoJC5pMThuKGdsb2JhbENvbmZpZy5zY29yZV90ZXh0KSk7XHJcbiAgICAgICAgJChcIi5ibG9ja3JhaW4tZ2FtZS1vdmVyLW1zZ1wiKS50ZXh0KCQuaTE4bihnbG9iYWxDb25maWcuZ2FtZV9vdmVyX3RleHQpKTtcclxuICAgICAgICAkKFwiLmJsb2NrcmFpbi1nYW1lLW92ZXItYnRuXCIpLnRleHQoXHJcbiAgICAgICAgICAgICQuaTE4bihnbG9iYWxDb25maWcucmVzdGFydF9idXR0b25fdGV4dClcclxuICAgICAgICApO1xyXG4gICAgICAgIGlmICghJGdhbWUuYmxvY2tyYWluKFwiaXNHYW1lUGF1c2VkXCIpKSB7XHJcbiAgICAgICAgICAgIHNldExldmVsTmFtZShzdWJMZXZlbC5nZXRTdWJMZXZlbCgpKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG59KTsiLCJmdW5jdGlvbiBsb2FkVGV4dEZpbGVBamF4U3luYyhmaWxlKSB7XHJcbiAgdmFyIHhvYmogPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuICB4b2JqLm92ZXJyaWRlTWltZVR5cGUoXCJhcHBsaWNhdGlvbi9qc29uXCIpO1xyXG4gIHhvYmoub3BlbihcIkdFVFwiLCBmaWxlLCBmYWxzZSk7IC8vIFJlcGxhY2UgZmlsZSB3aXRoIHRoZSBwYXRoIHRvIHlvdXIgZmlsZVxyXG4gIHhvYmouc2VuZCgpO1xyXG4gIGlmICh4b2JqLnN0YXR1cyA9PSAyMDApIHtcclxuICAgIHJldHVybiB4b2JqLnJlc3BvbnNlVGV4dDtcclxuICB9IGVsc2Uge1xyXG4gICAgLy8gVE9ETyBUaHJvdyBleGNlcHRpb25cclxuICAgIHJldHVybiBudWxsO1xyXG4gIH1cclxufVxyXG5cclxubW9kdWxlLmV4cG9ydHMubG9hZEpzb25GaWxlID0gZnVuY3Rpb24gbG9hZEpzb24oZmlsZVBhdGgpIHtcclxuICAvLyBMb2FkIGpzb24gZmlsZTtcclxuICB2YXIganNvbiA9IGxvYWRUZXh0RmlsZUFqYXhTeW5jKGZpbGVQYXRoKTtcclxuICAvLyBQYXJzZSBqc29uXHJcbiAgcmV0dXJuIEpTT04ucGFyc2UoanNvbik7XHJcbn07XHJcblxyXG4iLCJ2YXIgcGFnZUVsZW1lbnRzVXBkYXRlID0gcmVxdWlyZShcIi4vZ2FtZVByZXNlbnRlci5qc1wiKTtcclxuXHJcbnZhciBsYW5nSW1hZ2VzID0ge1xyXG4gICAgZ2w6IFwiYXNzZXRzL2ltYWdlcy9nbC5wbmdcIixcclxuICAgIGVzOiBcImFzc2V0cy9pbWFnZXMvZXMucG5nXCJcclxufTtcclxuXHJcbnZhciBsYW5nVGV4dHMgPSB7XHJcbiAgICBnbDogXCJHYWxlZ29cIixcclxuICAgIGVzOiBcIkNhc3RlbGxhbm9cIlxyXG59O1xyXG5cclxuLy8gRW5hYmxlIGRlYnVnXHJcbiQuaTE4bi5kZWJ1ZyA9IHRydWU7XHJcblxyXG52YXIgY3VycmVudExvY2FsZSA9IHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50TG9jYWxlXCIpO1xyXG5cclxuZnVuY3Rpb24gc2V0X2xvY2FsZV90byhsb2NhbGUpIHtcclxuICAgIGlmIChsb2NhbGUpICQuaTE4bigpLmxvY2FsZSA9IGxvY2FsZTtcclxufVxyXG5cclxualF1ZXJ5KGZ1bmN0aW9uIHVwZGF0ZUxvY2FsZSgkKSB7XHJcbiAgICBcInVzZSBzdHJpY3RcIjtcclxuICAgIHZhciBpMThuID0gJC5pMThuKCk7XHJcblxyXG4gICAgaTE4bi5sb2NhbGUgPSBjdXJyZW50TG9jYWxlO1xyXG5cclxuICAgIGkxOG5cclxuICAgICAgICAubG9hZCh7XHJcbiAgICAgICAgICAgIGVzOiBcIi4vc3JjL2kxOG4vZXMuanNvblwiLFxyXG4gICAgICAgICAgICBnbDogXCIuL3NyYy9pMThuL2dsLmpzb25cIlxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmRvbmUoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiSW50ZXJuYXRpb25hbGl6YXRpb24gZmlsZXMgbG9hZGVkIGNvcnJlY3RseVwiKTtcclxuXHJcbiAgICAgICAgICAgICQoXCIjdGl0bGVpbWFnZVwiKS5hdHRyKFxyXG4gICAgICAgICAgICAgICAgXCJzcmNcIiwgbGFuZ0ltYWdlc1tpMThuLmxvY2FsZV0pO1xyXG5cclxuICAgICAgICAgICAgJChcIiN0aXRsZXRleHRcIikudGV4dChsYW5nVGV4dHNbaTE4bi5sb2NhbGVdKTtcclxuXHJcbiAgICAgICAgICAgIEhpc3RvcnkuQWRhcHRlci5iaW5kKHdpbmRvdywgXCJzdGF0ZWNoYW5nZVwiLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIHNldF9sb2NhbGVfdG8odXJsKFwiP2xvY2FsZVwiKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgSGlzdG9yeS5wdXNoU3RhdGUobnVsbCwgbnVsbCwgXCI/bG9jYWxlPVwiICsgaTE4bi5sb2NhbGUpO1xyXG4gICAgICAgICAgICBwYWdlRWxlbWVudHNVcGRhdGUudXBkYXRlUGFnZUVsZW1lbnRzKCk7XHJcblxyXG4gICAgICAgICAgICAkKFwiLnN3aXRjaC1sb2NhbGVcIikub24oXCJjbGlja1wiLCBcImFcIiwgZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJsb2NhbGUgY2hhbmdlZFwiKTtcclxuICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgICAgIEhpc3RvcnkucHVzaFN0YXRlKG51bGwsIG51bGwsIFwiP2xvY2FsZT1cIiArICQodGhpcykuZGF0YShcImxvY2FsZVwiKSk7XHJcbiAgICAgICAgICAgICAgICAkLmkxOG4oKS5sb2NhbGUgPSAkKHRoaXMpLmRhdGEoXCJsb2NhbGVcIik7XHJcbiAgICAgICAgICAgICAgICBwYWdlRWxlbWVudHNVcGRhdGUudXBkYXRlUGFnZUVsZW1lbnRzKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgJChcIi5sYW5ndWFnZXMgPiBsaVwiKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICQoXCIjdGl0bGVpbWFnZVwiKS5hdHRyKFxyXG4gICAgICAgICAgICAgICAgICAgIFwic3JjXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgJCh0aGlzKVxyXG4gICAgICAgICAgICAgICAgICAgIC5jaGlsZHJlbigpXHJcbiAgICAgICAgICAgICAgICAgICAgLmNoaWxkcmVuKFwiaW1nXCIpXHJcbiAgICAgICAgICAgICAgICAgICAgLmF0dHIoXCJzcmNcIilcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAkKFwiI3RpdGxldGV4dFwiKS50ZXh0KCQodGhpcykudGV4dCgpKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbn0pOyIsIi8qKlxyXG4gQGNvbnN0cnVjdG9yXHJcbiBAYWJzdHJhY3RcclxuICovXHJcbm1vZHVsZS5leHBvcnRzLkxldmVsID0gZnVuY3Rpb24gbGV2ZWwobGV2ZWxEYXRhKSB7XHJcbiAgICB2YXIgdGhhdCA9IHt9O1xyXG5cclxuICAgIGlmICh0aGlzLmNvbnN0cnVjdG9yID09PSBsZXZlbCkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIkNhbid0IGluc3RhbnRpYXRlIGFic3RyYWN0IGNsYXNzIVwiKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGF0LmdldFNwZWVkID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIGxldmVsRGF0YS5zcGVlZDtcclxuICAgIH07XHJcblxyXG4gICAgdGhhdC5nZXREaWZmaWN1bHR5ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIGxldmVsRGF0YS5kaWZmaWN1bHR5O1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGF0LmdldFRoZW1lID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIGxldmVsRGF0YS50aGVtZTtcclxuICAgIH07XHJcblxyXG4gICAgdGhhdC5nZXRCbG9ja1RleHRDb2xvciA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiBsZXZlbERhdGEuYmxvY2tfdGV4dF9jb2xvcjtcclxuICAgIH1cclxuXHJcbiAgICB0aGF0LmdldFN1YkxldmVsID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIGxldmVsRGF0YS5zdWJfbGV2ZWw7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoYXQuZ2V0TnVtTGluZXMgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gbGV2ZWxEYXRhLm51bV9saW5lcztcclxuICAgIH07XHJcblxyXG4gICAgLyoqXHJcbiBAYWJzdHJhY3RcclxuICovXHJcbiAgICB0aGF0LmlzTGV2ZWxDb21wbGV0ZWQgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJBYnN0cmFjdCBtZXRob2QhXCIpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGF0LmlzRmluYWxMZXZlbCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiBsZXZlbERhdGEuZmluYWxfbGV2ZWw7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoYXQuZ2V0U3BlY2lhbFBpZWNlcyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiBsZXZlbERhdGEuc3BlY2lhbF9waWVjZXM7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoYXQuZ2V0T2JzdGFjbGVzID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIGxldmVsRGF0YS5vYnN0YWNsZXM7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoYXQuZ2V0TnVtT2JzdGFjbGVzID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIGxldmVsRGF0YS5udW1fb2JzdGFjbGVzO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGF0LmdldFNob3dQYXVzZUJ1dHRvbiA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiBsZXZlbERhdGEuc2hvd19wYXVzZV9idXR0b247XHJcbiAgICB9O1xyXG5cclxuICAgIHRoYXQuZ2V0U2hhcGVUeXBlUHJvYmFiaWxpdGllcyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiBsZXZlbERhdGEuc2hhcGVUeXBlUHJvYmFiaWxpdGllcztcclxuICAgIH07XHJcblxyXG4gICAgcmV0dXJuIHRoYXQ7XHJcbn07IiwidmFyIGxldmVsID0gcmVxdWlyZShcIi4vbGV2ZWwuanNcIik7XHJcblxyXG5tb2R1bGUuZXhwb3J0cy5MaW5lc1N1YkxldmVsID0gZnVuY3Rpb24gbGluZXNTdWJMZXZlbChsZXZlbERhdGEpIHtcclxuICB2YXIgdGhhdCA9IGxldmVsLkxldmVsKGxldmVsRGF0YSk7XHJcblxyXG4gIHRoYXQuaXNMZXZlbENvbXBsZXRlZCA9IGZ1bmN0aW9uKGN1cnJlbnROdW1MaW5lcykge1xyXG4gICAgaWYgKGN1cnJlbnROdW1MaW5lcyA+PSB0aGF0LmdldE51bUxpbmVzKCkpIHtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfTtcclxuXHJcbiAgcmV0dXJuIHRoYXQ7XHJcbn07XHJcbiIsInZhciBsZXZlbCA9IHJlcXVpcmUoXCIuL2xldmVsLmpzXCIpO1xyXG5cclxubW9kdWxlLmV4cG9ydHMuUG9pbnRzU3ViTGV2ZWwgPSBmdW5jdGlvbiBwb2ludHNTdWJMZXZlbChsZXZlbERhdGEpIHtcclxuICB2YXIgdGhhdCA9IGxldmVsLkxldmVsKGxldmVsRGF0YSk7XHJcblxyXG4gIHRoYXQuaXNMZXZlbENvbXBsZXRlZCA9IGZ1bmN0aW9uKGN1cnJlbnRQb2ludHMpIHtcclxuICAgIGlmIChjdXJyZW50UG9pbnRzID49IDIwMDApIHtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfTtcclxuXHJcbiAgcmV0dXJuIHRoYXQ7XHJcbn07XHJcbiIsIi8qKlxyXG4gKiBjbGRycGx1cmFscGFyc2VyLmpzXHJcbiAqIEEgcGFyc2VyIGVuZ2luZSBmb3IgQ0xEUiBwbHVyYWwgcnVsZXMuXHJcbiAqXHJcbiAqIENvcHlyaWdodCAyMDEyLTIwMTQgU2FudGhvc2ggVGhvdHRpbmdhbCBhbmQgb3RoZXIgY29udHJpYnV0b3JzXHJcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxyXG4gKiBodHRwOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUXHJcbiAqXHJcbiAqIEBzb3VyY2UgaHR0cHM6Ly9naXRodWIuY29tL3NhbnRob3NodHIvQ0xEUlBsdXJhbFJ1bGVQYXJzZXJcclxuICogQGF1dGhvciBTYW50aG9zaCBUaG90dGluZ2FsIDxzYW50aG9zaC50aG90dGluZ2FsQGdtYWlsLmNvbT5cclxuICogQGF1dGhvciBUaW1vIFRpamhvZlxyXG4gKiBAYXV0aG9yIEFtaXIgQWhhcm9uaVxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiBFdmFsdWF0ZXMgYSBwbHVyYWwgcnVsZSBpbiBDTERSIHN5bnRheCBmb3IgYSBudW1iZXJcclxuICogQHBhcmFtIHtzdHJpbmd9IHJ1bGVcclxuICogQHBhcmFtIHtpbnRlZ2VyfSBudW1iZXJcclxuICogQHJldHVybiB7Ym9vbGVhbn0gdHJ1ZSBpZiBldmFsdWF0aW9uIHBhc3NlZCwgZmFsc2UgaWYgZXZhbHVhdGlvbiBmYWlsZWQuXHJcbiAqL1xyXG5cclxuLy8gVU1EIHJldHVybkV4cG9ydHMgaHR0cHM6Ly9naXRodWIuY29tL3VtZGpzL3VtZC9ibG9iL21hc3Rlci9yZXR1cm5FeHBvcnRzLmpzXHJcbihmdW5jdGlvbihyb290LCBmYWN0b3J5KSB7XHJcblx0aWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xyXG5cdFx0Ly8gQU1ELiBSZWdpc3RlciBhcyBhbiBhbm9ueW1vdXMgbW9kdWxlLlxyXG5cdFx0ZGVmaW5lKGZhY3RvcnkpO1xyXG5cdH0gZWxzZSBpZiAodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKSB7XHJcblx0XHQvLyBOb2RlLiBEb2VzIG5vdCB3b3JrIHdpdGggc3RyaWN0IENvbW1vbkpTLCBidXRcclxuXHRcdC8vIG9ubHkgQ29tbW9uSlMtbGlrZSBlbnZpcm9ubWVudHMgdGhhdCBzdXBwb3J0IG1vZHVsZS5leHBvcnRzLFxyXG5cdFx0Ly8gbGlrZSBOb2RlLlxyXG5cdFx0bW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KCk7XHJcblx0fSBlbHNlIHtcclxuXHRcdC8vIEJyb3dzZXIgZ2xvYmFscyAocm9vdCBpcyB3aW5kb3cpXHJcblx0XHRyb290LnBsdXJhbFJ1bGVQYXJzZXIgPSBmYWN0b3J5KCk7XHJcblx0fVxyXG59KHRoaXMsIGZ1bmN0aW9uKCkge1xyXG5cclxuZnVuY3Rpb24gcGx1cmFsUnVsZVBhcnNlcihydWxlLCBudW1iZXIpIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblxyXG5cdC8qXHJcblx0U3ludGF4OiBzZWUgaHR0cDovL3VuaWNvZGUub3JnL3JlcG9ydHMvdHIzNS8jTGFuZ3VhZ2VfUGx1cmFsX1J1bGVzXHJcblx0LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRjb25kaXRpb24gICAgID0gYW5kX2NvbmRpdGlvbiAoJ29yJyBhbmRfY29uZGl0aW9uKSpcclxuXHRcdCgnQGludGVnZXInIHNhbXBsZXMpP1xyXG5cdFx0KCdAZGVjaW1hbCcgc2FtcGxlcyk/XHJcblx0YW5kX2NvbmRpdGlvbiA9IHJlbGF0aW9uICgnYW5kJyByZWxhdGlvbikqXHJcblx0cmVsYXRpb24gICAgICA9IGlzX3JlbGF0aW9uIHwgaW5fcmVsYXRpb24gfCB3aXRoaW5fcmVsYXRpb25cclxuXHRpc19yZWxhdGlvbiAgID0gZXhwciAnaXMnICgnbm90Jyk/IHZhbHVlXHJcblx0aW5fcmVsYXRpb24gICA9IGV4cHIgKCgnbm90Jyk/ICdpbicgfCAnPScgfCAnIT0nKSByYW5nZV9saXN0XHJcblx0d2l0aGluX3JlbGF0aW9uID0gZXhwciAoJ25vdCcpPyAnd2l0aGluJyByYW5nZV9saXN0XHJcblx0ZXhwciAgICAgICAgICA9IG9wZXJhbmQgKCgnbW9kJyB8ICclJykgdmFsdWUpP1xyXG5cdG9wZXJhbmQgICAgICAgPSAnbicgfCAnaScgfCAnZicgfCAndCcgfCAndicgfCAndydcclxuXHRyYW5nZV9saXN0ICAgID0gKHJhbmdlIHwgdmFsdWUpICgnLCcgcmFuZ2VfbGlzdCkqXHJcblx0dmFsdWUgICAgICAgICA9IGRpZ2l0K1xyXG5cdGRpZ2l0ICAgICAgICAgPSAwfDF8MnwzfDR8NXw2fDd8OHw5XHJcblx0cmFuZ2UgICAgICAgICA9IHZhbHVlJy4uJ3ZhbHVlXHJcblx0c2FtcGxlcyAgICAgICA9IHNhbXBsZVJhbmdlICgnLCcgc2FtcGxlUmFuZ2UpKiAoJywnICgn4oCmJ3wnLi4uJykpP1xyXG5cdHNhbXBsZVJhbmdlICAgPSBkZWNpbWFsVmFsdWUgJ34nIGRlY2ltYWxWYWx1ZVxyXG5cdGRlY2ltYWxWYWx1ZSAgPSB2YWx1ZSAoJy4nIHZhbHVlKT9cclxuXHQqL1xyXG5cclxuXHQvLyBXZSBkb24ndCBldmFsdWF0ZSB0aGUgc2FtcGxlcyBzZWN0aW9uIG9mIHRoZSBydWxlLiBJZ25vcmUgaXQuXHJcblx0cnVsZSA9IHJ1bGUuc3BsaXQoJ0AnKVswXS5yZXBsYWNlKC9eXFxzKi8sICcnKS5yZXBsYWNlKC9cXHMqJC8sICcnKTtcclxuXHJcblx0aWYgKCFydWxlLmxlbmd0aCkge1xyXG5cdFx0Ly8gRW1wdHkgcnVsZSBvciAnb3RoZXInIHJ1bGUuXHJcblx0XHRyZXR1cm4gdHJ1ZTtcclxuXHR9XHJcblxyXG5cdC8vIEluZGljYXRlcyB0aGUgY3VycmVudCBwb3NpdGlvbiBpbiB0aGUgcnVsZSBhcyB3ZSBwYXJzZSB0aHJvdWdoIGl0LlxyXG5cdC8vIFNoYXJlZCBhbW9uZyBhbGwgcGFyc2luZyBmdW5jdGlvbnMgYmVsb3cuXHJcblx0dmFyIHBvcyA9IDAsXHJcblx0XHRvcGVyYW5kLFxyXG5cdFx0ZXhwcmVzc2lvbixcclxuXHRcdHJlbGF0aW9uLFxyXG5cdFx0cmVzdWx0LFxyXG5cdFx0d2hpdGVzcGFjZSA9IG1ha2VSZWdleFBhcnNlcigvXlxccysvKSxcclxuXHRcdHZhbHVlID0gbWFrZVJlZ2V4UGFyc2VyKC9eXFxkKy8pLFxyXG5cdFx0X25fID0gbWFrZVN0cmluZ1BhcnNlcignbicpLFxyXG5cdFx0X2lfID0gbWFrZVN0cmluZ1BhcnNlcignaScpLFxyXG5cdFx0X2ZfID0gbWFrZVN0cmluZ1BhcnNlcignZicpLFxyXG5cdFx0X3RfID0gbWFrZVN0cmluZ1BhcnNlcigndCcpLFxyXG5cdFx0X3ZfID0gbWFrZVN0cmluZ1BhcnNlcigndicpLFxyXG5cdFx0X3dfID0gbWFrZVN0cmluZ1BhcnNlcigndycpLFxyXG5cdFx0X2lzXyA9IG1ha2VTdHJpbmdQYXJzZXIoJ2lzJyksXHJcblx0XHRfaXNub3RfID0gbWFrZVN0cmluZ1BhcnNlcignaXMgbm90JyksXHJcblx0XHRfaXNub3Rfc2lnbl8gPSBtYWtlU3RyaW5nUGFyc2VyKCchPScpLFxyXG5cdFx0X2VxdWFsXyA9IG1ha2VTdHJpbmdQYXJzZXIoJz0nKSxcclxuXHRcdF9tb2RfID0gbWFrZVN0cmluZ1BhcnNlcignbW9kJyksXHJcblx0XHRfcGVyY2VudF8gPSBtYWtlU3RyaW5nUGFyc2VyKCclJyksXHJcblx0XHRfbm90XyA9IG1ha2VTdHJpbmdQYXJzZXIoJ25vdCcpLFxyXG5cdFx0X2luXyA9IG1ha2VTdHJpbmdQYXJzZXIoJ2luJyksXHJcblx0XHRfd2l0aGluXyA9IG1ha2VTdHJpbmdQYXJzZXIoJ3dpdGhpbicpLFxyXG5cdFx0X3JhbmdlXyA9IG1ha2VTdHJpbmdQYXJzZXIoJy4uJyksXHJcblx0XHRfY29tbWFfID0gbWFrZVN0cmluZ1BhcnNlcignLCcpLFxyXG5cdFx0X29yXyA9IG1ha2VTdHJpbmdQYXJzZXIoJ29yJyksXHJcblx0XHRfYW5kXyA9IG1ha2VTdHJpbmdQYXJzZXIoJ2FuZCcpO1xyXG5cclxuXHRmdW5jdGlvbiBkZWJ1ZygpIHtcclxuXHRcdC8vIGNvbnNvbGUubG9nLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XHJcblx0fVxyXG5cclxuXHRkZWJ1ZygncGx1cmFsUnVsZVBhcnNlcicsIHJ1bGUsIG51bWJlcik7XHJcblxyXG5cdC8vIFRyeSBwYXJzZXJzIHVudGlsIG9uZSB3b3JrcywgaWYgbm9uZSB3b3JrIHJldHVybiBudWxsXHJcblx0ZnVuY3Rpb24gY2hvaWNlKHBhcnNlclN5bnRheCkge1xyXG5cdFx0cmV0dXJuIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgaSwgcmVzdWx0O1xyXG5cclxuXHRcdFx0Zm9yIChpID0gMDsgaSA8IHBhcnNlclN5bnRheC5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRcdHJlc3VsdCA9IHBhcnNlclN5bnRheFtpXSgpO1xyXG5cclxuXHRcdFx0XHRpZiAocmVzdWx0ICE9PSBudWxsKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIG51bGw7XHJcblx0XHR9O1xyXG5cdH1cclxuXHJcblx0Ly8gVHJ5IHNldmVyYWwgcGFyc2VyU3ludGF4LWVzIGluIGEgcm93LlxyXG5cdC8vIEFsbCBtdXN0IHN1Y2NlZWQ7IG90aGVyd2lzZSwgcmV0dXJuIG51bGwuXHJcblx0Ly8gVGhpcyBpcyB0aGUgb25seSBlYWdlciBvbmUuXHJcblx0ZnVuY3Rpb24gc2VxdWVuY2UocGFyc2VyU3ludGF4KSB7XHJcblx0XHR2YXIgaSwgcGFyc2VyUmVzLFxyXG5cdFx0XHRvcmlnaW5hbFBvcyA9IHBvcyxcclxuXHRcdFx0cmVzdWx0ID0gW107XHJcblxyXG5cdFx0Zm9yIChpID0gMDsgaSA8IHBhcnNlclN5bnRheC5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRwYXJzZXJSZXMgPSBwYXJzZXJTeW50YXhbaV0oKTtcclxuXHJcblx0XHRcdGlmIChwYXJzZXJSZXMgPT09IG51bGwpIHtcclxuXHRcdFx0XHRwb3MgPSBvcmlnaW5hbFBvcztcclxuXHJcblx0XHRcdFx0cmV0dXJuIG51bGw7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJlc3VsdC5wdXNoKHBhcnNlclJlcyk7XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIHJlc3VsdDtcclxuXHR9XHJcblxyXG5cdC8vIFJ1biB0aGUgc2FtZSBwYXJzZXIgb3ZlciBhbmQgb3ZlciB1bnRpbCBpdCBmYWlscy5cclxuXHQvLyBNdXN0IHN1Y2NlZWQgYSBtaW5pbXVtIG9mIG4gdGltZXM7IG90aGVyd2lzZSwgcmV0dXJuIG51bGwuXHJcblx0ZnVuY3Rpb24gbk9yTW9yZShuLCBwKSB7XHJcblx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBvcmlnaW5hbFBvcyA9IHBvcyxcclxuXHRcdFx0XHRyZXN1bHQgPSBbXSxcclxuXHRcdFx0XHRwYXJzZWQgPSBwKCk7XHJcblxyXG5cdFx0XHR3aGlsZSAocGFyc2VkICE9PSBudWxsKSB7XHJcblx0XHRcdFx0cmVzdWx0LnB1c2gocGFyc2VkKTtcclxuXHRcdFx0XHRwYXJzZWQgPSBwKCk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmIChyZXN1bHQubGVuZ3RoIDwgbikge1xyXG5cdFx0XHRcdHBvcyA9IG9yaWdpbmFsUG9zO1xyXG5cclxuXHRcdFx0XHRyZXR1cm4gbnVsbDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdH07XHJcblx0fVxyXG5cclxuXHQvLyBIZWxwZXJzIC0ganVzdCBtYWtlIHBhcnNlclN5bnRheCBvdXQgb2Ygc2ltcGxlciBKUyBidWlsdGluIHR5cGVzXHJcblx0ZnVuY3Rpb24gbWFrZVN0cmluZ1BhcnNlcihzKSB7XHJcblx0XHR2YXIgbGVuID0gcy5sZW5ndGg7XHJcblxyXG5cdFx0cmV0dXJuIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgcmVzdWx0ID0gbnVsbDtcclxuXHJcblx0XHRcdGlmIChydWxlLnN1YnN0cihwb3MsIGxlbikgPT09IHMpIHtcclxuXHRcdFx0XHRyZXN1bHQgPSBzO1xyXG5cdFx0XHRcdHBvcyArPSBsZW47XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJldHVybiByZXN1bHQ7XHJcblx0XHR9O1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gbWFrZVJlZ2V4UGFyc2VyKHJlZ2V4KSB7XHJcblx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBtYXRjaGVzID0gcnVsZS5zdWJzdHIocG9zKS5tYXRjaChyZWdleCk7XHJcblxyXG5cdFx0XHRpZiAobWF0Y2hlcyA9PT0gbnVsbCkge1xyXG5cdFx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRwb3MgKz0gbWF0Y2hlc1swXS5sZW5ndGg7XHJcblxyXG5cdFx0XHRyZXR1cm4gbWF0Y2hlc1swXTtcclxuXHRcdH07XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBJbnRlZ2VyIGRpZ2l0cyBvZiBuLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIGkoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gX2lfKCk7XHJcblxyXG5cdFx0aWYgKHJlc3VsdCA9PT0gbnVsbCkge1xyXG5cdFx0XHRkZWJ1ZygnIC0tIGZhaWxlZCBpJywgcGFyc2VJbnQobnVtYmVyLCAxMCkpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdH1cclxuXHJcblx0XHRyZXN1bHQgPSBwYXJzZUludChudW1iZXIsIDEwKTtcclxuXHRcdGRlYnVnKCcgLS0gcGFzc2VkIGkgJywgcmVzdWx0KTtcclxuXHJcblx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogQWJzb2x1dGUgdmFsdWUgb2YgdGhlIHNvdXJjZSBudW1iZXIgKGludGVnZXIgYW5kIGRlY2ltYWxzKS5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBuKCkge1xyXG5cdFx0dmFyIHJlc3VsdCA9IF9uXygpO1xyXG5cclxuXHRcdGlmIChyZXN1bHQgPT09IG51bGwpIHtcclxuXHRcdFx0ZGVidWcoJyAtLSBmYWlsZWQgbiAnLCBudW1iZXIpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdH1cclxuXHJcblx0XHRyZXN1bHQgPSBwYXJzZUZsb2F0KG51bWJlciwgMTApO1xyXG5cdFx0ZGVidWcoJyAtLSBwYXNzZWQgbiAnLCByZXN1bHQpO1xyXG5cclxuXHRcdHJldHVybiByZXN1bHQ7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBWaXNpYmxlIGZyYWN0aW9uYWwgZGlnaXRzIGluIG4sIHdpdGggdHJhaWxpbmcgemVyb3MuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gZigpIHtcclxuXHRcdHZhciByZXN1bHQgPSBfZl8oKTtcclxuXHJcblx0XHRpZiAocmVzdWx0ID09PSBudWxsKSB7XHJcblx0XHRcdGRlYnVnKCcgLS0gZmFpbGVkIGYgJywgbnVtYmVyKTtcclxuXHJcblx0XHRcdHJldHVybiByZXN1bHQ7XHJcblx0XHR9XHJcblxyXG5cdFx0cmVzdWx0ID0gKG51bWJlciArICcuJykuc3BsaXQoJy4nKVsxXSB8fCAwO1xyXG5cdFx0ZGVidWcoJyAtLSBwYXNzZWQgZiAnLCByZXN1bHQpO1xyXG5cclxuXHRcdHJldHVybiByZXN1bHQ7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBWaXNpYmxlIGZyYWN0aW9uYWwgZGlnaXRzIGluIG4sIHdpdGhvdXQgdHJhaWxpbmcgemVyb3MuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gdCgpIHtcclxuXHRcdHZhciByZXN1bHQgPSBfdF8oKTtcclxuXHJcblx0XHRpZiAocmVzdWx0ID09PSBudWxsKSB7XHJcblx0XHRcdGRlYnVnKCcgLS0gZmFpbGVkIHQgJywgbnVtYmVyKTtcclxuXHJcblx0XHRcdHJldHVybiByZXN1bHQ7XHJcblx0XHR9XHJcblxyXG5cdFx0cmVzdWx0ID0gKG51bWJlciArICcuJykuc3BsaXQoJy4nKVsxXS5yZXBsYWNlKC8wJC8sICcnKSB8fCAwO1xyXG5cdFx0ZGVidWcoJyAtLSBwYXNzZWQgdCAnLCByZXN1bHQpO1xyXG5cclxuXHRcdHJldHVybiByZXN1bHQ7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBOdW1iZXIgb2YgdmlzaWJsZSBmcmFjdGlvbiBkaWdpdHMgaW4gbiwgd2l0aCB0cmFpbGluZyB6ZXJvcy5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiB2KCkge1xyXG5cdFx0dmFyIHJlc3VsdCA9IF92XygpO1xyXG5cclxuXHRcdGlmIChyZXN1bHQgPT09IG51bGwpIHtcclxuXHRcdFx0ZGVidWcoJyAtLSBmYWlsZWQgdiAnLCBudW1iZXIpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdH1cclxuXHJcblx0XHRyZXN1bHQgPSAobnVtYmVyICsgJy4nKS5zcGxpdCgnLicpWzFdLmxlbmd0aCB8fCAwO1xyXG5cdFx0ZGVidWcoJyAtLSBwYXNzZWQgdiAnLCByZXN1bHQpO1xyXG5cclxuXHRcdHJldHVybiByZXN1bHQ7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBOdW1iZXIgb2YgdmlzaWJsZSBmcmFjdGlvbiBkaWdpdHMgaW4gbiwgd2l0aG91dCB0cmFpbGluZyB6ZXJvcy5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiB3KCkge1xyXG5cdFx0dmFyIHJlc3VsdCA9IF93XygpO1xyXG5cclxuXHRcdGlmIChyZXN1bHQgPT09IG51bGwpIHtcclxuXHRcdFx0ZGVidWcoJyAtLSBmYWlsZWQgdyAnLCBudW1iZXIpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdH1cclxuXHJcblx0XHRyZXN1bHQgPSAobnVtYmVyICsgJy4nKS5zcGxpdCgnLicpWzFdLnJlcGxhY2UoLzAkLywgJycpLmxlbmd0aCB8fCAwO1xyXG5cdFx0ZGVidWcoJyAtLSBwYXNzZWQgdyAnLCByZXN1bHQpO1xyXG5cclxuXHRcdHJldHVybiByZXN1bHQ7XHJcblx0fVxyXG5cclxuXHQvLyBvcGVyYW5kICAgICAgID0gJ24nIHwgJ2knIHwgJ2YnIHwgJ3QnIHwgJ3YnIHwgJ3cnXHJcblx0b3BlcmFuZCA9IGNob2ljZShbbiwgaSwgZiwgdCwgdiwgd10pO1xyXG5cclxuXHQvLyBleHByICAgICAgICAgID0gb3BlcmFuZCAoKCdtb2QnIHwgJyUnKSB2YWx1ZSk/XHJcblx0ZXhwcmVzc2lvbiA9IGNob2ljZShbbW9kLCBvcGVyYW5kXSk7XHJcblxyXG5cdGZ1bmN0aW9uIG1vZCgpIHtcclxuXHRcdHZhciByZXN1bHQgPSBzZXF1ZW5jZShcclxuXHRcdFx0W29wZXJhbmQsIHdoaXRlc3BhY2UsIGNob2ljZShbX21vZF8sIF9wZXJjZW50X10pLCB3aGl0ZXNwYWNlLCB2YWx1ZV1cclxuXHRcdCk7XHJcblxyXG5cdFx0aWYgKHJlc3VsdCA9PT0gbnVsbCkge1xyXG5cdFx0XHRkZWJ1ZygnIC0tIGZhaWxlZCBtb2QnKTtcclxuXHJcblx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0fVxyXG5cclxuXHRcdGRlYnVnKCcgLS0gcGFzc2VkICcgKyBwYXJzZUludChyZXN1bHRbMF0sIDEwKSArICcgJyArIHJlc3VsdFsyXSArICcgJyArIHBhcnNlSW50KHJlc3VsdFs0XSwgMTApKTtcclxuXHJcblx0XHRyZXR1cm4gcGFyc2VGbG9hdChyZXN1bHRbMF0pICUgcGFyc2VJbnQocmVzdWx0WzRdLCAxMCk7XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBub3QoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gc2VxdWVuY2UoW3doaXRlc3BhY2UsIF9ub3RfXSk7XHJcblxyXG5cdFx0aWYgKHJlc3VsdCA9PT0gbnVsbCkge1xyXG5cdFx0XHRkZWJ1ZygnIC0tIGZhaWxlZCBub3QnKTtcclxuXHJcblx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybiByZXN1bHRbMV07XHJcblx0fVxyXG5cclxuXHQvLyBpc19yZWxhdGlvbiAgID0gZXhwciAnaXMnICgnbm90Jyk/IHZhbHVlXHJcblx0ZnVuY3Rpb24gaXMoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gc2VxdWVuY2UoW2V4cHJlc3Npb24sIHdoaXRlc3BhY2UsIGNob2ljZShbX2lzX10pLCB3aGl0ZXNwYWNlLCB2YWx1ZV0pO1xyXG5cclxuXHRcdGlmIChyZXN1bHQgIT09IG51bGwpIHtcclxuXHRcdFx0ZGVidWcoJyAtLSBwYXNzZWQgaXMgOiAnICsgcmVzdWx0WzBdICsgJyA9PSAnICsgcGFyc2VJbnQocmVzdWx0WzRdLCAxMCkpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdFswXSA9PT0gcGFyc2VJbnQocmVzdWx0WzRdLCAxMCk7XHJcblx0XHR9XHJcblxyXG5cdFx0ZGVidWcoJyAtLSBmYWlsZWQgaXMnKTtcclxuXHJcblx0XHRyZXR1cm4gbnVsbDtcclxuXHR9XHJcblxyXG5cdC8vIGlzX3JlbGF0aW9uICAgPSBleHByICdpcycgKCdub3QnKT8gdmFsdWVcclxuXHRmdW5jdGlvbiBpc25vdCgpIHtcclxuXHRcdHZhciByZXN1bHQgPSBzZXF1ZW5jZShcclxuXHRcdFx0W2V4cHJlc3Npb24sIHdoaXRlc3BhY2UsIGNob2ljZShbX2lzbm90XywgX2lzbm90X3NpZ25fXSksIHdoaXRlc3BhY2UsIHZhbHVlXVxyXG5cdFx0KTtcclxuXHJcblx0XHRpZiAocmVzdWx0ICE9PSBudWxsKSB7XHJcblx0XHRcdGRlYnVnKCcgLS0gcGFzc2VkIGlzbm90OiAnICsgcmVzdWx0WzBdICsgJyAhPSAnICsgcGFyc2VJbnQocmVzdWx0WzRdLCAxMCkpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdFswXSAhPT0gcGFyc2VJbnQocmVzdWx0WzRdLCAxMCk7XHJcblx0XHR9XHJcblxyXG5cdFx0ZGVidWcoJyAtLSBmYWlsZWQgaXNub3QnKTtcclxuXHJcblx0XHRyZXR1cm4gbnVsbDtcclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIG5vdF9pbigpIHtcclxuXHRcdHZhciBpLCByYW5nZV9saXN0LFxyXG5cdFx0XHRyZXN1bHQgPSBzZXF1ZW5jZShbZXhwcmVzc2lvbiwgd2hpdGVzcGFjZSwgX2lzbm90X3NpZ25fLCB3aGl0ZXNwYWNlLCByYW5nZUxpc3RdKTtcclxuXHJcblx0XHRpZiAocmVzdWx0ICE9PSBudWxsKSB7XHJcblx0XHRcdGRlYnVnKCcgLS0gcGFzc2VkIG5vdF9pbjogJyArIHJlc3VsdFswXSArICcgIT0gJyArIHJlc3VsdFs0XSk7XHJcblx0XHRcdHJhbmdlX2xpc3QgPSByZXN1bHRbNF07XHJcblxyXG5cdFx0XHRmb3IgKGkgPSAwOyBpIDwgcmFuZ2VfbGlzdC5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRcdGlmIChwYXJzZUludChyYW5nZV9saXN0W2ldLCAxMCkgPT09IHBhcnNlSW50KHJlc3VsdFswXSwgMTApKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdH1cclxuXHJcblx0XHRkZWJ1ZygnIC0tIGZhaWxlZCBub3RfaW4nKTtcclxuXHJcblx0XHRyZXR1cm4gbnVsbDtcclxuXHR9XHJcblxyXG5cdC8vIHJhbmdlX2xpc3QgICAgPSAocmFuZ2UgfCB2YWx1ZSkgKCcsJyByYW5nZV9saXN0KSpcclxuXHRmdW5jdGlvbiByYW5nZUxpc3QoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gc2VxdWVuY2UoW2Nob2ljZShbcmFuZ2UsIHZhbHVlXSksIG5Pck1vcmUoMCwgcmFuZ2VUYWlsKV0pLFxyXG5cdFx0XHRyZXN1bHRMaXN0ID0gW107XHJcblxyXG5cdFx0aWYgKHJlc3VsdCAhPT0gbnVsbCkge1xyXG5cdFx0XHRyZXN1bHRMaXN0ID0gcmVzdWx0TGlzdC5jb25jYXQocmVzdWx0WzBdKTtcclxuXHJcblx0XHRcdGlmIChyZXN1bHRbMV1bMF0pIHtcclxuXHRcdFx0XHRyZXN1bHRMaXN0ID0gcmVzdWx0TGlzdC5jb25jYXQocmVzdWx0WzFdWzBdKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdExpc3Q7XHJcblx0XHR9XHJcblxyXG5cdFx0ZGVidWcoJyAtLSBmYWlsZWQgcmFuZ2VMaXN0Jyk7XHJcblxyXG5cdFx0cmV0dXJuIG51bGw7XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiByYW5nZVRhaWwoKSB7XHJcblx0XHQvLyAnLCcgcmFuZ2VfbGlzdFxyXG5cdFx0dmFyIHJlc3VsdCA9IHNlcXVlbmNlKFtfY29tbWFfLCByYW5nZUxpc3RdKTtcclxuXHJcblx0XHRpZiAocmVzdWx0ICE9PSBudWxsKSB7XHJcblx0XHRcdHJldHVybiByZXN1bHRbMV07XHJcblx0XHR9XHJcblxyXG5cdFx0ZGVidWcoJyAtLSBmYWlsZWQgcmFuZ2VUYWlsJyk7XHJcblxyXG5cdFx0cmV0dXJuIG51bGw7XHJcblx0fVxyXG5cclxuXHQvLyByYW5nZSAgICAgICAgID0gdmFsdWUnLi4ndmFsdWVcclxuXHRmdW5jdGlvbiByYW5nZSgpIHtcclxuXHRcdHZhciBpLCBhcnJheSwgbGVmdCwgcmlnaHQsXHJcblx0XHRcdHJlc3VsdCA9IHNlcXVlbmNlKFt2YWx1ZSwgX3JhbmdlXywgdmFsdWVdKTtcclxuXHJcblx0XHRpZiAocmVzdWx0ICE9PSBudWxsKSB7XHJcblx0XHRcdGRlYnVnKCcgLS0gcGFzc2VkIHJhbmdlJyk7XHJcblxyXG5cdFx0XHRhcnJheSA9IFtdO1xyXG5cdFx0XHRsZWZ0ID0gcGFyc2VJbnQocmVzdWx0WzBdLCAxMCk7XHJcblx0XHRcdHJpZ2h0ID0gcGFyc2VJbnQocmVzdWx0WzJdLCAxMCk7XHJcblxyXG5cdFx0XHRmb3IgKGkgPSBsZWZ0OyBpIDw9IHJpZ2h0OyBpKyspIHtcclxuXHRcdFx0XHRhcnJheS5wdXNoKGkpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRyZXR1cm4gYXJyYXk7XHJcblx0XHR9XHJcblxyXG5cdFx0ZGVidWcoJyAtLSBmYWlsZWQgcmFuZ2UnKTtcclxuXHJcblx0XHRyZXR1cm4gbnVsbDtcclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIF9pbigpIHtcclxuXHRcdHZhciByZXN1bHQsIHJhbmdlX2xpc3QsIGk7XHJcblxyXG5cdFx0Ly8gaW5fcmVsYXRpb24gICA9IGV4cHIgKCdub3QnKT8gJ2luJyByYW5nZV9saXN0XHJcblx0XHRyZXN1bHQgPSBzZXF1ZW5jZShcclxuXHRcdFx0W2V4cHJlc3Npb24sIG5Pck1vcmUoMCwgbm90KSwgd2hpdGVzcGFjZSwgY2hvaWNlKFtfaW5fLCBfZXF1YWxfXSksIHdoaXRlc3BhY2UsIHJhbmdlTGlzdF1cclxuXHRcdCk7XHJcblxyXG5cdFx0aWYgKHJlc3VsdCAhPT0gbnVsbCkge1xyXG5cdFx0XHRkZWJ1ZygnIC0tIHBhc3NlZCBfaW46JyArIHJlc3VsdCk7XHJcblxyXG5cdFx0XHRyYW5nZV9saXN0ID0gcmVzdWx0WzVdO1xyXG5cclxuXHRcdFx0Zm9yIChpID0gMDsgaSA8IHJhbmdlX2xpc3QubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRpZiAocGFyc2VJbnQocmFuZ2VfbGlzdFtpXSwgMTApID09PSBwYXJzZUZsb2F0KHJlc3VsdFswXSkpIHtcclxuXHRcdFx0XHRcdHJldHVybiAocmVzdWx0WzFdWzBdICE9PSAnbm90Jyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRyZXR1cm4gKHJlc3VsdFsxXVswXSA9PT0gJ25vdCcpO1xyXG5cdFx0fVxyXG5cclxuXHRcdGRlYnVnKCcgLS0gZmFpbGVkIF9pbiAnKTtcclxuXHJcblx0XHRyZXR1cm4gbnVsbDtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIFRoZSBkaWZmZXJlbmNlIGJldHdlZW4gXCJpblwiIGFuZCBcIndpdGhpblwiIGlzIHRoYXRcclxuXHQgKiBcImluXCIgb25seSBpbmNsdWRlcyBpbnRlZ2VycyBpbiB0aGUgc3BlY2lmaWVkIHJhbmdlLFxyXG5cdCAqIHdoaWxlIFwid2l0aGluXCIgaW5jbHVkZXMgYWxsIHZhbHVlcy5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiB3aXRoaW4oKSB7XHJcblx0XHR2YXIgcmFuZ2VfbGlzdCwgcmVzdWx0O1xyXG5cclxuXHRcdC8vIHdpdGhpbl9yZWxhdGlvbiA9IGV4cHIgKCdub3QnKT8gJ3dpdGhpbicgcmFuZ2VfbGlzdFxyXG5cdFx0cmVzdWx0ID0gc2VxdWVuY2UoXHJcblx0XHRcdFtleHByZXNzaW9uLCBuT3JNb3JlKDAsIG5vdCksIHdoaXRlc3BhY2UsIF93aXRoaW5fLCB3aGl0ZXNwYWNlLCByYW5nZUxpc3RdXHJcblx0XHQpO1xyXG5cclxuXHRcdGlmIChyZXN1bHQgIT09IG51bGwpIHtcclxuXHRcdFx0ZGVidWcoJyAtLSBwYXNzZWQgd2l0aGluJyk7XHJcblxyXG5cdFx0XHRyYW5nZV9saXN0ID0gcmVzdWx0WzVdO1xyXG5cclxuXHRcdFx0aWYgKChyZXN1bHRbMF0gPj0gcGFyc2VJbnQocmFuZ2VfbGlzdFswXSwgMTApKSAmJlxyXG5cdFx0XHRcdChyZXN1bHRbMF0gPCBwYXJzZUludChyYW5nZV9saXN0W3JhbmdlX2xpc3QubGVuZ3RoIC0gMV0sIDEwKSkpIHtcclxuXHJcblx0XHRcdFx0cmV0dXJuIChyZXN1bHRbMV1bMF0gIT09ICdub3QnKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIChyZXN1bHRbMV1bMF0gPT09ICdub3QnKTtcclxuXHRcdH1cclxuXHJcblx0XHRkZWJ1ZygnIC0tIGZhaWxlZCB3aXRoaW4gJyk7XHJcblxyXG5cdFx0cmV0dXJuIG51bGw7XHJcblx0fVxyXG5cclxuXHQvLyByZWxhdGlvbiAgICAgID0gaXNfcmVsYXRpb24gfCBpbl9yZWxhdGlvbiB8IHdpdGhpbl9yZWxhdGlvblxyXG5cdHJlbGF0aW9uID0gY2hvaWNlKFtpcywgbm90X2luLCBpc25vdCwgX2luLCB3aXRoaW5dKTtcclxuXHJcblx0Ly8gYW5kX2NvbmRpdGlvbiA9IHJlbGF0aW9uICgnYW5kJyByZWxhdGlvbikqXHJcblx0ZnVuY3Rpb24gYW5kKCkge1xyXG5cdFx0dmFyIGksXHJcblx0XHRcdHJlc3VsdCA9IHNlcXVlbmNlKFtyZWxhdGlvbiwgbk9yTW9yZSgwLCBhbmRUYWlsKV0pO1xyXG5cclxuXHRcdGlmIChyZXN1bHQpIHtcclxuXHRcdFx0aWYgKCFyZXN1bHRbMF0pIHtcclxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGZvciAoaSA9IDA7IGkgPCByZXN1bHRbMV0ubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRpZiAoIXJlc3VsdFsxXVtpXSkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHR9XHJcblxyXG5cdFx0ZGVidWcoJyAtLSBmYWlsZWQgYW5kJyk7XHJcblxyXG5cdFx0cmV0dXJuIG51bGw7XHJcblx0fVxyXG5cclxuXHQvLyAoJ2FuZCcgcmVsYXRpb24pKlxyXG5cdGZ1bmN0aW9uIGFuZFRhaWwoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gc2VxdWVuY2UoW3doaXRlc3BhY2UsIF9hbmRfLCB3aGl0ZXNwYWNlLCByZWxhdGlvbl0pO1xyXG5cclxuXHRcdGlmIChyZXN1bHQgIT09IG51bGwpIHtcclxuXHRcdFx0ZGVidWcoJyAtLSBwYXNzZWQgYW5kVGFpbCcgKyByZXN1bHQpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdFszXTtcclxuXHRcdH1cclxuXHJcblx0XHRkZWJ1ZygnIC0tIGZhaWxlZCBhbmRUYWlsJyk7XHJcblxyXG5cdFx0cmV0dXJuIG51bGw7XHJcblxyXG5cdH1cclxuXHQvLyAgKCdvcicgYW5kX2NvbmRpdGlvbikqXHJcblx0ZnVuY3Rpb24gb3JUYWlsKCkge1xyXG5cdFx0dmFyIHJlc3VsdCA9IHNlcXVlbmNlKFt3aGl0ZXNwYWNlLCBfb3JfLCB3aGl0ZXNwYWNlLCBhbmRdKTtcclxuXHJcblx0XHRpZiAocmVzdWx0ICE9PSBudWxsKSB7XHJcblx0XHRcdGRlYnVnKCcgLS0gcGFzc2VkIG9yVGFpbDogJyArIHJlc3VsdFszXSk7XHJcblxyXG5cdFx0XHRyZXR1cm4gcmVzdWx0WzNdO1xyXG5cdFx0fVxyXG5cclxuXHRcdGRlYnVnKCcgLS0gZmFpbGVkIG9yVGFpbCcpO1xyXG5cclxuXHRcdHJldHVybiBudWxsO1xyXG5cdH1cclxuXHJcblx0Ly8gY29uZGl0aW9uICAgICA9IGFuZF9jb25kaXRpb24gKCdvcicgYW5kX2NvbmRpdGlvbikqXHJcblx0ZnVuY3Rpb24gY29uZGl0aW9uKCkge1xyXG5cdFx0dmFyIGksXHJcblx0XHRcdHJlc3VsdCA9IHNlcXVlbmNlKFthbmQsIG5Pck1vcmUoMCwgb3JUYWlsKV0pO1xyXG5cclxuXHRcdGlmIChyZXN1bHQpIHtcclxuXHRcdFx0Zm9yIChpID0gMDsgaSA8IHJlc3VsdFsxXS5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRcdGlmIChyZXN1bHRbMV1baV0pIHtcclxuXHRcdFx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdFswXTtcclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG5cclxuXHRyZXN1bHQgPSBjb25kaXRpb24oKTtcclxuXHJcblx0LyoqXHJcblx0ICogRm9yIHN1Y2Nlc3MsIHRoZSBwb3MgbXVzdCBoYXZlIGdvdHRlbiB0byB0aGUgZW5kIG9mIHRoZSBydWxlXHJcblx0ICogYW5kIHJldHVybmVkIGEgbm9uLW51bGwuXHJcblx0ICogbi5iLiBUaGlzIGlzIHBhcnQgb2YgbGFuZ3VhZ2UgaW5mcmFzdHJ1Y3R1cmUsXHJcblx0ICogc28gd2UgZG8gbm90IHRocm93IGFuIGludGVybmF0aW9uYWxpemFibGUgbWVzc2FnZS5cclxuXHQgKi9cclxuXHRpZiAocmVzdWx0ID09PSBudWxsKSB7XHJcblx0XHR0aHJvdyBuZXcgRXJyb3IoJ1BhcnNlIGVycm9yIGF0IHBvc2l0aW9uICcgKyBwb3MudG9TdHJpbmcoKSArICcgZm9yIHJ1bGU6ICcgKyBydWxlKTtcclxuXHR9XHJcblxyXG5cdGlmIChwb3MgIT09IHJ1bGUubGVuZ3RoKSB7XHJcblx0XHRkZWJ1ZygnV2FybmluZzogUnVsZSBub3QgcGFyc2VkIGNvbXBsZXRlbHkuIFBhcnNlciBzdG9wcGVkIGF0ICcgKyBydWxlLnN1YnN0cigwLCBwb3MpICsgJyBmb3IgcnVsZTogJyArIHJ1bGUpO1xyXG5cdH1cclxuXHJcblx0cmV0dXJuIHJlc3VsdDtcclxufVxyXG5cclxucmV0dXJuIHBsdXJhbFJ1bGVQYXJzZXI7XHJcblxyXG59KSk7XHJcbiIsInR5cGVvZiBKU09OIT1cIm9iamVjdFwiJiYoSlNPTj17fSksZnVuY3Rpb24oKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBmKGUpe3JldHVybiBlPDEwP1wiMFwiK2U6ZX1mdW5jdGlvbiBxdW90ZShlKXtyZXR1cm4gZXNjYXBhYmxlLmxhc3RJbmRleD0wLGVzY2FwYWJsZS50ZXN0KGUpPydcIicrZS5yZXBsYWNlKGVzY2FwYWJsZSxmdW5jdGlvbihlKXt2YXIgdD1tZXRhW2VdO3JldHVybiB0eXBlb2YgdD09XCJzdHJpbmdcIj90OlwiXFxcXHVcIisoXCIwMDAwXCIrZS5jaGFyQ29kZUF0KDApLnRvU3RyaW5nKDE2KSkuc2xpY2UoLTQpfSkrJ1wiJzonXCInK2UrJ1wiJ31mdW5jdGlvbiBzdHIoZSx0KXt2YXIgbixyLGkscyxvPWdhcCx1LGE9dFtlXTthJiZ0eXBlb2YgYT09XCJvYmplY3RcIiYmdHlwZW9mIGEudG9KU09OPT1cImZ1bmN0aW9uXCImJihhPWEudG9KU09OKGUpKSx0eXBlb2YgcmVwPT1cImZ1bmN0aW9uXCImJihhPXJlcC5jYWxsKHQsZSxhKSk7c3dpdGNoKHR5cGVvZiBhKXtjYXNlXCJzdHJpbmdcIjpyZXR1cm4gcXVvdGUoYSk7Y2FzZVwibnVtYmVyXCI6cmV0dXJuIGlzRmluaXRlKGEpP1N0cmluZyhhKTpcIm51bGxcIjtjYXNlXCJib29sZWFuXCI6Y2FzZVwibnVsbFwiOnJldHVybiBTdHJpbmcoYSk7Y2FzZVwib2JqZWN0XCI6aWYoIWEpcmV0dXJuXCJudWxsXCI7Z2FwKz1pbmRlbnQsdT1bXTtpZihPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmFwcGx5KGEpPT09XCJbb2JqZWN0IEFycmF5XVwiKXtzPWEubGVuZ3RoO2ZvcihuPTA7bjxzO24rPTEpdVtuXT1zdHIobixhKXx8XCJudWxsXCI7cmV0dXJuIGk9dS5sZW5ndGg9PT0wP1wiW11cIjpnYXA/XCJbXFxuXCIrZ2FwK3Uuam9pbihcIixcXG5cIitnYXApK1wiXFxuXCIrbytcIl1cIjpcIltcIit1LmpvaW4oXCIsXCIpK1wiXVwiLGdhcD1vLGl9aWYocmVwJiZ0eXBlb2YgcmVwPT1cIm9iamVjdFwiKXtzPXJlcC5sZW5ndGg7Zm9yKG49MDtuPHM7bis9MSl0eXBlb2YgcmVwW25dPT1cInN0cmluZ1wiJiYocj1yZXBbbl0saT1zdHIocixhKSxpJiZ1LnB1c2gocXVvdGUocikrKGdhcD9cIjogXCI6XCI6XCIpK2kpKX1lbHNlIGZvcihyIGluIGEpT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKGEscikmJihpPXN0cihyLGEpLGkmJnUucHVzaChxdW90ZShyKSsoZ2FwP1wiOiBcIjpcIjpcIikraSkpO3JldHVybiBpPXUubGVuZ3RoPT09MD9cInt9XCI6Z2FwP1wie1xcblwiK2dhcCt1LmpvaW4oXCIsXFxuXCIrZ2FwKStcIlxcblwiK28rXCJ9XCI6XCJ7XCIrdS5qb2luKFwiLFwiKStcIn1cIixnYXA9byxpfX10eXBlb2YgRGF0ZS5wcm90b3R5cGUudG9KU09OIT1cImZ1bmN0aW9uXCImJihEYXRlLnByb3RvdHlwZS50b0pTT049ZnVuY3Rpb24oZSl7cmV0dXJuIGlzRmluaXRlKHRoaXMudmFsdWVPZigpKT90aGlzLmdldFVUQ0Z1bGxZZWFyKCkrXCItXCIrZih0aGlzLmdldFVUQ01vbnRoKCkrMSkrXCItXCIrZih0aGlzLmdldFVUQ0RhdGUoKSkrXCJUXCIrZih0aGlzLmdldFVUQ0hvdXJzKCkpK1wiOlwiK2YodGhpcy5nZXRVVENNaW51dGVzKCkpK1wiOlwiK2YodGhpcy5nZXRVVENTZWNvbmRzKCkpK1wiWlwiOm51bGx9LFN0cmluZy5wcm90b3R5cGUudG9KU09OPU51bWJlci5wcm90b3R5cGUudG9KU09OPUJvb2xlYW4ucHJvdG90eXBlLnRvSlNPTj1mdW5jdGlvbihlKXtyZXR1cm4gdGhpcy52YWx1ZU9mKCl9KTt2YXIgY3g9L1tcXHUwMDAwXFx1MDBhZFxcdTA2MDAtXFx1MDYwNFxcdTA3MGZcXHUxN2I0XFx1MTdiNVxcdTIwMGMtXFx1MjAwZlxcdTIwMjgtXFx1MjAyZlxcdTIwNjAtXFx1MjA2ZlxcdWZlZmZcXHVmZmYwLVxcdWZmZmZdL2csZXNjYXBhYmxlPS9bXFxcXFxcXCJcXHgwMC1cXHgxZlxceDdmLVxceDlmXFx1MDBhZFxcdTA2MDAtXFx1MDYwNFxcdTA3MGZcXHUxN2I0XFx1MTdiNVxcdTIwMGMtXFx1MjAwZlxcdTIwMjgtXFx1MjAyZlxcdTIwNjAtXFx1MjA2ZlxcdWZlZmZcXHVmZmYwLVxcdWZmZmZdL2csZ2FwLGluZGVudCxtZXRhPXtcIlxcYlwiOlwiXFxcXGJcIixcIiBcIjpcIlxcXFx0XCIsXCJcXG5cIjpcIlxcXFxuXCIsXCJcXGZcIjpcIlxcXFxmXCIsXCJcXHJcIjpcIlxcXFxyXCIsJ1wiJzonXFxcXFwiJyxcIlxcXFxcIjpcIlxcXFxcXFxcXCJ9LHJlcDt0eXBlb2YgSlNPTi5zdHJpbmdpZnkhPVwiZnVuY3Rpb25cIiYmKEpTT04uc3RyaW5naWZ5PWZ1bmN0aW9uKGUsdCxuKXt2YXIgcjtnYXA9XCJcIixpbmRlbnQ9XCJcIjtpZih0eXBlb2Ygbj09XCJudW1iZXJcIilmb3Iocj0wO3I8bjtyKz0xKWluZGVudCs9XCIgXCI7ZWxzZSB0eXBlb2Ygbj09XCJzdHJpbmdcIiYmKGluZGVudD1uKTtyZXA9dDtpZighdHx8dHlwZW9mIHQ9PVwiZnVuY3Rpb25cInx8dHlwZW9mIHQ9PVwib2JqZWN0XCImJnR5cGVvZiB0Lmxlbmd0aD09XCJudW1iZXJcIilyZXR1cm4gc3RyKFwiXCIse1wiXCI6ZX0pO3Rocm93IG5ldyBFcnJvcihcIkpTT04uc3RyaW5naWZ5XCIpfSksdHlwZW9mIEpTT04ucGFyc2UhPVwiZnVuY3Rpb25cIiYmKEpTT04ucGFyc2U9ZnVuY3Rpb24odGV4dCxyZXZpdmVyKXtmdW5jdGlvbiB3YWxrKGUsdCl7dmFyIG4scixpPWVbdF07aWYoaSYmdHlwZW9mIGk9PVwib2JqZWN0XCIpZm9yKG4gaW4gaSlPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoaSxuKSYmKHI9d2FsayhpLG4pLHIhPT11bmRlZmluZWQ/aVtuXT1yOmRlbGV0ZSBpW25dKTtyZXR1cm4gcmV2aXZlci5jYWxsKGUsdCxpKX12YXIgajt0ZXh0PVN0cmluZyh0ZXh0KSxjeC5sYXN0SW5kZXg9MCxjeC50ZXN0KHRleHQpJiYodGV4dD10ZXh0LnJlcGxhY2UoY3gsZnVuY3Rpb24oZSl7cmV0dXJuXCJcXFxcdVwiKyhcIjAwMDBcIitlLmNoYXJDb2RlQXQoMCkudG9TdHJpbmcoMTYpKS5zbGljZSgtNCl9KSk7aWYoL15bXFxdLDp7fVxcc10qJC8udGVzdCh0ZXh0LnJlcGxhY2UoL1xcXFwoPzpbXCJcXFxcXFwvYmZucnRdfHVbMC05YS1mQS1GXXs0fSkvZyxcIkBcIikucmVwbGFjZSgvXCJbXlwiXFxcXFxcblxccl0qXCJ8dHJ1ZXxmYWxzZXxudWxsfC0/XFxkKyg/OlxcLlxcZCopPyg/OltlRV1bK1xcLV0/XFxkKyk/L2csXCJdXCIpLnJlcGxhY2UoLyg/Ol58OnwsKSg/OlxccypcXFspKy9nLFwiXCIpKSlyZXR1cm4gaj1ldmFsKFwiKFwiK3RleHQrXCIpXCIpLHR5cGVvZiByZXZpdmVyPT1cImZ1bmN0aW9uXCI/d2Fsayh7XCJcIjpqfSxcIlwiKTpqO3Rocm93IG5ldyBTeW50YXhFcnJvcihcIkpTT04ucGFyc2VcIil9KX0oKSxmdW5jdGlvbihlLHQpe1widXNlIHN0cmljdFwiO3ZhciBuPWUuSGlzdG9yeT1lLkhpc3Rvcnl8fHt9LHI9ZS5qUXVlcnk7aWYodHlwZW9mIG4uQWRhcHRlciE9XCJ1bmRlZmluZWRcIil0aHJvdyBuZXcgRXJyb3IoXCJIaXN0b3J5LmpzIEFkYXB0ZXIgaGFzIGFscmVhZHkgYmVlbiBsb2FkZWQuLi5cIik7bi5BZGFwdGVyPXtiaW5kOmZ1bmN0aW9uKGUsdCxuKXtyKGUpLmJpbmQodCxuKX0sdHJpZ2dlcjpmdW5jdGlvbihlLHQsbil7cihlKS50cmlnZ2VyKHQsbil9LGV4dHJhY3RFdmVudERhdGE6ZnVuY3Rpb24oZSxuLHIpe3ZhciBpPW4mJm4ub3JpZ2luYWxFdmVudCYmbi5vcmlnaW5hbEV2ZW50W2VdfHxyJiZyW2VdfHx0O3JldHVybiBpfSxvbkRvbUxvYWQ6ZnVuY3Rpb24oZSl7cihlKX19LHR5cGVvZiBuLmluaXQhPVwidW5kZWZpbmVkXCImJm4uaW5pdCgpfSh3aW5kb3cpLGZ1bmN0aW9uKGUsdCl7XCJ1c2Ugc3RyaWN0XCI7dmFyIG49ZS5kb2N1bWVudCxyPWUuc2V0VGltZW91dHx8cixpPWUuY2xlYXJUaW1lb3V0fHxpLHM9ZS5zZXRJbnRlcnZhbHx8cyxvPWUuSGlzdG9yeT1lLkhpc3Rvcnl8fHt9O2lmKHR5cGVvZiBvLmluaXRIdG1sNCE9XCJ1bmRlZmluZWRcIil0aHJvdyBuZXcgRXJyb3IoXCJIaXN0b3J5LmpzIEhUTUw0IFN1cHBvcnQgaGFzIGFscmVhZHkgYmVlbiBsb2FkZWQuLi5cIik7by5pbml0SHRtbDQ9ZnVuY3Rpb24oKXtpZih0eXBlb2Ygby5pbml0SHRtbDQuaW5pdGlhbGl6ZWQhPVwidW5kZWZpbmVkXCIpcmV0dXJuITE7by5pbml0SHRtbDQuaW5pdGlhbGl6ZWQ9ITAsby5lbmFibGVkPSEwLG8uc2F2ZWRIYXNoZXM9W10sby5pc0xhc3RIYXNoPWZ1bmN0aW9uKGUpe3ZhciB0PW8uZ2V0SGFzaEJ5SW5kZXgoKSxuO3JldHVybiBuPWU9PT10LG59LG8uaXNIYXNoRXF1YWw9ZnVuY3Rpb24oZSx0KXtyZXR1cm4gZT1lbmNvZGVVUklDb21wb25lbnQoZSkucmVwbGFjZSgvJTI1L2csXCIlXCIpLHQ9ZW5jb2RlVVJJQ29tcG9uZW50KHQpLnJlcGxhY2UoLyUyNS9nLFwiJVwiKSxlPT09dH0sby5zYXZlSGFzaD1mdW5jdGlvbihlKXtyZXR1cm4gby5pc0xhc3RIYXNoKGUpPyExOihvLnNhdmVkSGFzaGVzLnB1c2goZSksITApfSxvLmdldEhhc2hCeUluZGV4PWZ1bmN0aW9uKGUpe3ZhciB0PW51bGw7cmV0dXJuIHR5cGVvZiBlPT1cInVuZGVmaW5lZFwiP3Q9by5zYXZlZEhhc2hlc1tvLnNhdmVkSGFzaGVzLmxlbmd0aC0xXTplPDA/dD1vLnNhdmVkSGFzaGVzW28uc2F2ZWRIYXNoZXMubGVuZ3RoK2VdOnQ9by5zYXZlZEhhc2hlc1tlXSx0fSxvLmRpc2NhcmRlZEhhc2hlcz17fSxvLmRpc2NhcmRlZFN0YXRlcz17fSxvLmRpc2NhcmRTdGF0ZT1mdW5jdGlvbihlLHQsbil7dmFyIHI9by5nZXRIYXNoQnlTdGF0ZShlKSxpO3JldHVybiBpPXtkaXNjYXJkZWRTdGF0ZTplLGJhY2tTdGF0ZTpuLGZvcndhcmRTdGF0ZTp0fSxvLmRpc2NhcmRlZFN0YXRlc1tyXT1pLCEwfSxvLmRpc2NhcmRIYXNoPWZ1bmN0aW9uKGUsdCxuKXt2YXIgcj17ZGlzY2FyZGVkSGFzaDplLGJhY2tTdGF0ZTpuLGZvcndhcmRTdGF0ZTp0fTtyZXR1cm4gby5kaXNjYXJkZWRIYXNoZXNbZV09ciwhMH0sby5kaXNjYXJkZWRTdGF0ZT1mdW5jdGlvbihlKXt2YXIgdD1vLmdldEhhc2hCeVN0YXRlKGUpLG47cmV0dXJuIG49by5kaXNjYXJkZWRTdGF0ZXNbdF18fCExLG59LG8uZGlzY2FyZGVkSGFzaD1mdW5jdGlvbihlKXt2YXIgdD1vLmRpc2NhcmRlZEhhc2hlc1tlXXx8ITE7cmV0dXJuIHR9LG8ucmVjeWNsZVN0YXRlPWZ1bmN0aW9uKGUpe3ZhciB0PW8uZ2V0SGFzaEJ5U3RhdGUoZSk7cmV0dXJuIG8uZGlzY2FyZGVkU3RhdGUoZSkmJmRlbGV0ZSBvLmRpc2NhcmRlZFN0YXRlc1t0XSwhMH0sby5lbXVsYXRlZC5oYXNoQ2hhbmdlJiYoby5oYXNoQ2hhbmdlSW5pdD1mdW5jdGlvbigpe28uY2hlY2tlckZ1bmN0aW9uPW51bGw7dmFyIHQ9XCJcIixyLGksdSxhLGY9Qm9vbGVhbihvLmdldEhhc2goKSk7cmV0dXJuIG8uaXNJbnRlcm5ldEV4cGxvcmVyKCk/KHI9XCJoaXN0b3J5anMtaWZyYW1lXCIsaT1uLmNyZWF0ZUVsZW1lbnQoXCJpZnJhbWVcIiksaS5zZXRBdHRyaWJ1dGUoXCJpZFwiLHIpLGkuc2V0QXR0cmlidXRlKFwic3JjXCIsXCIjXCIpLGkuc3R5bGUuZGlzcGxheT1cIm5vbmVcIixuLmJvZHkuYXBwZW5kQ2hpbGQoaSksaS5jb250ZW50V2luZG93LmRvY3VtZW50Lm9wZW4oKSxpLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQuY2xvc2UoKSx1PVwiXCIsYT0hMSxvLmNoZWNrZXJGdW5jdGlvbj1mdW5jdGlvbigpe2lmKGEpcmV0dXJuITE7YT0hMDt2YXIgbj1vLmdldEhhc2goKSxyPW8uZ2V0SGFzaChpLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQpO3JldHVybiBuIT09dD8odD1uLHIhPT1uJiYodT1yPW4saS5jb250ZW50V2luZG93LmRvY3VtZW50Lm9wZW4oKSxpLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQuY2xvc2UoKSxpLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQubG9jYXRpb24uaGFzaD1vLmVzY2FwZUhhc2gobikpLG8uQWRhcHRlci50cmlnZ2VyKGUsXCJoYXNoY2hhbmdlXCIpKTpyIT09dSYmKHU9cixmJiZyPT09XCJcIj9vLmJhY2soKTpvLnNldEhhc2gociwhMSkpLGE9ITEsITB9KTpvLmNoZWNrZXJGdW5jdGlvbj1mdW5jdGlvbigpe3ZhciBuPW8uZ2V0SGFzaCgpfHxcIlwiO3JldHVybiBuIT09dCYmKHQ9bixvLkFkYXB0ZXIudHJpZ2dlcihlLFwiaGFzaGNoYW5nZVwiKSksITB9LG8uaW50ZXJ2YWxMaXN0LnB1c2gocyhvLmNoZWNrZXJGdW5jdGlvbixvLm9wdGlvbnMuaGFzaENoYW5nZUludGVydmFsKSksITB9LG8uQWRhcHRlci5vbkRvbUxvYWQoby5oYXNoQ2hhbmdlSW5pdCkpLG8uZW11bGF0ZWQucHVzaFN0YXRlJiYoby5vbkhhc2hDaGFuZ2U9ZnVuY3Rpb24odCl7dmFyIG49dCYmdC5uZXdVUkx8fG8uZ2V0TG9jYXRpb25IcmVmKCkscj1vLmdldEhhc2hCeVVybChuKSxpPW51bGwscz1udWxsLHU9bnVsbCxhO3JldHVybiBvLmlzTGFzdEhhc2gocik/KG8uYnVzeSghMSksITEpOihvLmRvdWJsZUNoZWNrQ29tcGxldGUoKSxvLnNhdmVIYXNoKHIpLHImJm8uaXNUcmFkaXRpb25hbEFuY2hvcihyKT8oby5BZGFwdGVyLnRyaWdnZXIoZSxcImFuY2hvcmNoYW5nZVwiKSxvLmJ1c3koITEpLCExKTooaT1vLmV4dHJhY3RTdGF0ZShvLmdldEZ1bGxVcmwocnx8by5nZXRMb2NhdGlvbkhyZWYoKSksITApLG8uaXNMYXN0U2F2ZWRTdGF0ZShpKT8oby5idXN5KCExKSwhMSk6KHM9by5nZXRIYXNoQnlTdGF0ZShpKSxhPW8uZGlzY2FyZGVkU3RhdGUoaSksYT8oby5nZXRIYXNoQnlJbmRleCgtMik9PT1vLmdldEhhc2hCeVN0YXRlKGEuZm9yd2FyZFN0YXRlKT9vLmJhY2soITEpOm8uZm9yd2FyZCghMSksITEpOihvLnB1c2hTdGF0ZShpLmRhdGEsaS50aXRsZSxlbmNvZGVVUkkoaS51cmwpLCExKSwhMCkpKSl9LG8uQWRhcHRlci5iaW5kKGUsXCJoYXNoY2hhbmdlXCIsby5vbkhhc2hDaGFuZ2UpLG8ucHVzaFN0YXRlPWZ1bmN0aW9uKHQsbixyLGkpe3I9ZW5jb2RlVVJJKHIpLnJlcGxhY2UoLyUyNS9nLFwiJVwiKTtpZihvLmdldEhhc2hCeVVybChyKSl0aHJvdyBuZXcgRXJyb3IoXCJIaXN0b3J5LmpzIGRvZXMgbm90IHN1cHBvcnQgc3RhdGVzIHdpdGggZnJhZ21lbnQtaWRlbnRpZmllcnMgKGhhc2hlcy9hbmNob3JzKS5cIik7aWYoaSE9PSExJiZvLmJ1c3koKSlyZXR1cm4gby5wdXNoUXVldWUoe3Njb3BlOm8sY2FsbGJhY2s6by5wdXNoU3RhdGUsYXJnczphcmd1bWVudHMscXVldWU6aX0pLCExO28uYnVzeSghMCk7dmFyIHM9by5jcmVhdGVTdGF0ZU9iamVjdCh0LG4sciksdT1vLmdldEhhc2hCeVN0YXRlKHMpLGE9by5nZXRTdGF0ZSghMSksZj1vLmdldEhhc2hCeVN0YXRlKGEpLGw9by5nZXRIYXNoKCksYz1vLmV4cGVjdGVkU3RhdGVJZD09cy5pZDtyZXR1cm4gby5zdG9yZVN0YXRlKHMpLG8uZXhwZWN0ZWRTdGF0ZUlkPXMuaWQsby5yZWN5Y2xlU3RhdGUocyksby5zZXRUaXRsZShzKSx1PT09Zj8oby5idXN5KCExKSwhMSk6KG8uc2F2ZVN0YXRlKHMpLGN8fG8uQWRhcHRlci50cmlnZ2VyKGUsXCJzdGF0ZWNoYW5nZVwiKSwhby5pc0hhc2hFcXVhbCh1LGwpJiYhby5pc0hhc2hFcXVhbCh1LG8uZ2V0U2hvcnRVcmwoby5nZXRMb2NhdGlvbkhyZWYoKSkpJiZvLnNldEhhc2godSwhMSksby5idXN5KCExKSwhMCl9LG8ucmVwbGFjZVN0YXRlPWZ1bmN0aW9uKHQsbixyLGkpe3I9ZW5jb2RlVVJJKHIpLnJlcGxhY2UoLyUyNS9nLFwiJVwiKTtpZihvLmdldEhhc2hCeVVybChyKSl0aHJvdyBuZXcgRXJyb3IoXCJIaXN0b3J5LmpzIGRvZXMgbm90IHN1cHBvcnQgc3RhdGVzIHdpdGggZnJhZ21lbnQtaWRlbnRpZmllcnMgKGhhc2hlcy9hbmNob3JzKS5cIik7aWYoaSE9PSExJiZvLmJ1c3koKSlyZXR1cm4gby5wdXNoUXVldWUoe3Njb3BlOm8sY2FsbGJhY2s6by5yZXBsYWNlU3RhdGUsYXJnczphcmd1bWVudHMscXVldWU6aX0pLCExO28uYnVzeSghMCk7dmFyIHM9by5jcmVhdGVTdGF0ZU9iamVjdCh0LG4sciksdT1vLmdldEhhc2hCeVN0YXRlKHMpLGE9by5nZXRTdGF0ZSghMSksZj1vLmdldEhhc2hCeVN0YXRlKGEpLGw9by5nZXRTdGF0ZUJ5SW5kZXgoLTIpO3JldHVybiBvLmRpc2NhcmRTdGF0ZShhLHMsbCksdT09PWY/KG8uc3RvcmVTdGF0ZShzKSxvLmV4cGVjdGVkU3RhdGVJZD1zLmlkLG8ucmVjeWNsZVN0YXRlKHMpLG8uc2V0VGl0bGUocyksby5zYXZlU3RhdGUocyksby5BZGFwdGVyLnRyaWdnZXIoZSxcInN0YXRlY2hhbmdlXCIpLG8uYnVzeSghMSkpOm8ucHVzaFN0YXRlKHMuZGF0YSxzLnRpdGxlLHMudXJsLCExKSwhMH0pLG8uZW11bGF0ZWQucHVzaFN0YXRlJiZvLmdldEhhc2goKSYmIW8uZW11bGF0ZWQuaGFzaENoYW5nZSYmby5BZGFwdGVyLm9uRG9tTG9hZChmdW5jdGlvbigpe28uQWRhcHRlci50cmlnZ2VyKGUsXCJoYXNoY2hhbmdlXCIpfSl9LHR5cGVvZiBvLmluaXQhPVwidW5kZWZpbmVkXCImJm8uaW5pdCgpfSh3aW5kb3cpLGZ1bmN0aW9uKGUsdCl7XCJ1c2Ugc3RyaWN0XCI7dmFyIG49ZS5jb25zb2xlfHx0LHI9ZS5kb2N1bWVudCxpPWUubmF2aWdhdG9yLHM9ITEsbz1lLnNldFRpbWVvdXQsdT1lLmNsZWFyVGltZW91dCxhPWUuc2V0SW50ZXJ2YWwsZj1lLmNsZWFySW50ZXJ2YWwsbD1lLkpTT04sYz1lLmFsZXJ0LGg9ZS5IaXN0b3J5PWUuSGlzdG9yeXx8e30scD1lLmhpc3Rvcnk7dHJ5e3M9ZS5zZXNzaW9uU3RvcmFnZSxzLnNldEl0ZW0oXCJURVNUXCIsXCIxXCIpLHMucmVtb3ZlSXRlbShcIlRFU1RcIil9Y2F0Y2goZCl7cz0hMX1sLnN0cmluZ2lmeT1sLnN0cmluZ2lmeXx8bC5lbmNvZGUsbC5wYXJzZT1sLnBhcnNlfHxsLmRlY29kZTtpZih0eXBlb2YgaC5pbml0IT1cInVuZGVmaW5lZFwiKXRocm93IG5ldyBFcnJvcihcIkhpc3RvcnkuanMgQ29yZSBoYXMgYWxyZWFkeSBiZWVuIGxvYWRlZC4uLlwiKTtoLmluaXQ9ZnVuY3Rpb24oZSl7cmV0dXJuIHR5cGVvZiBoLkFkYXB0ZXI9PVwidW5kZWZpbmVkXCI/ITE6KHR5cGVvZiBoLmluaXRDb3JlIT1cInVuZGVmaW5lZFwiJiZoLmluaXRDb3JlKCksdHlwZW9mIGguaW5pdEh0bWw0IT1cInVuZGVmaW5lZFwiJiZoLmluaXRIdG1sNCgpLCEwKX0saC5pbml0Q29yZT1mdW5jdGlvbihkKXtpZih0eXBlb2YgaC5pbml0Q29yZS5pbml0aWFsaXplZCE9XCJ1bmRlZmluZWRcIilyZXR1cm4hMTtoLmluaXRDb3JlLmluaXRpYWxpemVkPSEwLGgub3B0aW9ucz1oLm9wdGlvbnN8fHt9LGgub3B0aW9ucy5oYXNoQ2hhbmdlSW50ZXJ2YWw9aC5vcHRpb25zLmhhc2hDaGFuZ2VJbnRlcnZhbHx8MTAwLGgub3B0aW9ucy5zYWZhcmlQb2xsSW50ZXJ2YWw9aC5vcHRpb25zLnNhZmFyaVBvbGxJbnRlcnZhbHx8NTAwLGgub3B0aW9ucy5kb3VibGVDaGVja0ludGVydmFsPWgub3B0aW9ucy5kb3VibGVDaGVja0ludGVydmFsfHw1MDAsaC5vcHRpb25zLmRpc2FibGVTdWlkPWgub3B0aW9ucy5kaXNhYmxlU3VpZHx8ITEsaC5vcHRpb25zLnN0b3JlSW50ZXJ2YWw9aC5vcHRpb25zLnN0b3JlSW50ZXJ2YWx8fDFlMyxoLm9wdGlvbnMuYnVzeURlbGF5PWgub3B0aW9ucy5idXN5RGVsYXl8fDI1MCxoLm9wdGlvbnMuZGVidWc9aC5vcHRpb25zLmRlYnVnfHwhMSxoLm9wdGlvbnMuaW5pdGlhbFRpdGxlPWgub3B0aW9ucy5pbml0aWFsVGl0bGV8fHIudGl0bGUsaC5vcHRpb25zLmh0bWw0TW9kZT1oLm9wdGlvbnMuaHRtbDRNb2RlfHwhMSxoLm9wdGlvbnMuZGVsYXlJbml0PWgub3B0aW9ucy5kZWxheUluaXR8fCExLGguaW50ZXJ2YWxMaXN0PVtdLGguY2xlYXJBbGxJbnRlcnZhbHM9ZnVuY3Rpb24oKXt2YXIgZSx0PWguaW50ZXJ2YWxMaXN0O2lmKHR5cGVvZiB0IT1cInVuZGVmaW5lZFwiJiZ0IT09bnVsbCl7Zm9yKGU9MDtlPHQubGVuZ3RoO2UrKylmKHRbZV0pO2guaW50ZXJ2YWxMaXN0PW51bGx9fSxoLmRlYnVnPWZ1bmN0aW9uKCl7KGgub3B0aW9ucy5kZWJ1Z3x8ITEpJiZoLmxvZy5hcHBseShoLGFyZ3VtZW50cyl9LGgubG9nPWZ1bmN0aW9uKCl7dmFyIGU9dHlwZW9mIG4hPVwidW5kZWZpbmVkXCImJnR5cGVvZiBuLmxvZyE9XCJ1bmRlZmluZWRcIiYmdHlwZW9mIG4ubG9nLmFwcGx5IT1cInVuZGVmaW5lZFwiLHQ9ci5nZXRFbGVtZW50QnlJZChcImxvZ1wiKSxpLHMsbyx1LGE7ZT8odT1BcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpLGk9dS5zaGlmdCgpLHR5cGVvZiBuLmRlYnVnIT1cInVuZGVmaW5lZFwiP24uZGVidWcuYXBwbHkobixbaSx1XSk6bi5sb2cuYXBwbHkobixbaSx1XSkpOmk9XCJcXG5cIithcmd1bWVudHNbMF0rXCJcXG5cIjtmb3Iocz0xLG89YXJndW1lbnRzLmxlbmd0aDtzPG87KytzKXthPWFyZ3VtZW50c1tzXTtpZih0eXBlb2YgYT09XCJvYmplY3RcIiYmdHlwZW9mIGwhPVwidW5kZWZpbmVkXCIpdHJ5e2E9bC5zdHJpbmdpZnkoYSl9Y2F0Y2goZil7fWkrPVwiXFxuXCIrYStcIlxcblwifXJldHVybiB0Pyh0LnZhbHVlKz1pK1wiXFxuLS0tLS1cXG5cIix0LnNjcm9sbFRvcD10LnNjcm9sbEhlaWdodC10LmNsaWVudEhlaWdodCk6ZXx8YyhpKSwhMH0saC5nZXRJbnRlcm5ldEV4cGxvcmVyTWFqb3JWZXJzaW9uPWZ1bmN0aW9uKCl7dmFyIGU9aC5nZXRJbnRlcm5ldEV4cGxvcmVyTWFqb3JWZXJzaW9uLmNhY2hlZD10eXBlb2YgaC5nZXRJbnRlcm5ldEV4cGxvcmVyTWFqb3JWZXJzaW9uLmNhY2hlZCE9XCJ1bmRlZmluZWRcIj9oLmdldEludGVybmV0RXhwbG9yZXJNYWpvclZlcnNpb24uY2FjaGVkOmZ1bmN0aW9uKCl7dmFyIGU9Myx0PXIuY3JlYXRlRWxlbWVudChcImRpdlwiKSxuPXQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJpXCIpO3doaWxlKCh0LmlubmVySFRNTD1cIjwhLS1baWYgZ3QgSUUgXCIrICsrZStcIl0+PGk+PC9pPjwhW2VuZGlmXS0tPlwiKSYmblswXSk7cmV0dXJuIGU+ND9lOiExfSgpO3JldHVybiBlfSxoLmlzSW50ZXJuZXRFeHBsb3Jlcj1mdW5jdGlvbigpe3ZhciBlPWguaXNJbnRlcm5ldEV4cGxvcmVyLmNhY2hlZD10eXBlb2YgaC5pc0ludGVybmV0RXhwbG9yZXIuY2FjaGVkIT1cInVuZGVmaW5lZFwiP2guaXNJbnRlcm5ldEV4cGxvcmVyLmNhY2hlZDpCb29sZWFuKGguZ2V0SW50ZXJuZXRFeHBsb3Jlck1ham9yVmVyc2lvbigpKTtyZXR1cm4gZX0saC5vcHRpb25zLmh0bWw0TW9kZT9oLmVtdWxhdGVkPXtwdXNoU3RhdGU6ITAsaGFzaENoYW5nZTohMH06aC5lbXVsYXRlZD17cHVzaFN0YXRlOiFCb29sZWFuKGUuaGlzdG9yeSYmZS5oaXN0b3J5LnB1c2hTdGF0ZSYmZS5oaXN0b3J5LnJlcGxhY2VTdGF0ZSYmIS8gTW9iaWxlXFwvKFsxLTddW2Etel18KDgoW2FiY2RlXXxmKDFbMC04XSkpKSkvaS50ZXN0KGkudXNlckFnZW50KSYmIS9BcHBsZVdlYktpdFxcLzUoWzAtMl18M1swLTJdKS9pLnRlc3QoaS51c2VyQWdlbnQpKSxoYXNoQ2hhbmdlOkJvb2xlYW4oIShcIm9uaGFzaGNoYW5nZVwiaW4gZXx8XCJvbmhhc2hjaGFuZ2VcImluIHIpfHxoLmlzSW50ZXJuZXRFeHBsb3JlcigpJiZoLmdldEludGVybmV0RXhwbG9yZXJNYWpvclZlcnNpb24oKTw4KX0saC5lbmFibGVkPSFoLmVtdWxhdGVkLnB1c2hTdGF0ZSxoLmJ1Z3M9e3NldEhhc2g6Qm9vbGVhbighaC5lbXVsYXRlZC5wdXNoU3RhdGUmJmkudmVuZG9yPT09XCJBcHBsZSBDb21wdXRlciwgSW5jLlwiJiYvQXBwbGVXZWJLaXRcXC81KFswLTJdfDNbMC0zXSkvLnRlc3QoaS51c2VyQWdlbnQpKSxzYWZhcmlQb2xsOkJvb2xlYW4oIWguZW11bGF0ZWQucHVzaFN0YXRlJiZpLnZlbmRvcj09PVwiQXBwbGUgQ29tcHV0ZXIsIEluYy5cIiYmL0FwcGxlV2ViS2l0XFwvNShbMC0yXXwzWzAtM10pLy50ZXN0KGkudXNlckFnZW50KSksaWVEb3VibGVDaGVjazpCb29sZWFuKGguaXNJbnRlcm5ldEV4cGxvcmVyKCkmJmguZ2V0SW50ZXJuZXRFeHBsb3Jlck1ham9yVmVyc2lvbigpPDgpLGhhc2hFc2NhcGU6Qm9vbGVhbihoLmlzSW50ZXJuZXRFeHBsb3JlcigpJiZoLmdldEludGVybmV0RXhwbG9yZXJNYWpvclZlcnNpb24oKTw3KX0saC5pc0VtcHR5T2JqZWN0PWZ1bmN0aW9uKGUpe2Zvcih2YXIgdCBpbiBlKWlmKGUuaGFzT3duUHJvcGVydHkodCkpcmV0dXJuITE7cmV0dXJuITB9LGguY2xvbmVPYmplY3Q9ZnVuY3Rpb24oZSl7dmFyIHQsbjtyZXR1cm4gZT8odD1sLnN0cmluZ2lmeShlKSxuPWwucGFyc2UodCkpOm49e30sbn0saC5nZXRSb290VXJsPWZ1bmN0aW9uKCl7dmFyIGU9ci5sb2NhdGlvbi5wcm90b2NvbCtcIi8vXCIrKHIubG9jYXRpb24uaG9zdG5hbWV8fHIubG9jYXRpb24uaG9zdCk7aWYoci5sb2NhdGlvbi5wb3J0fHwhMSllKz1cIjpcIityLmxvY2F0aW9uLnBvcnQ7cmV0dXJuIGUrPVwiL1wiLGV9LGguZ2V0QmFzZUhyZWY9ZnVuY3Rpb24oKXt2YXIgZT1yLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiYmFzZVwiKSx0PW51bGwsbj1cIlwiO3JldHVybiBlLmxlbmd0aD09PTEmJih0PWVbMF0sbj10LmhyZWYucmVwbGFjZSgvW15cXC9dKyQvLFwiXCIpKSxuPW4ucmVwbGFjZSgvXFwvKyQvLFwiXCIpLG4mJihuKz1cIi9cIiksbn0saC5nZXRCYXNlVXJsPWZ1bmN0aW9uKCl7dmFyIGU9aC5nZXRCYXNlSHJlZigpfHxoLmdldEJhc2VQYWdlVXJsKCl8fGguZ2V0Um9vdFVybCgpO3JldHVybiBlfSxoLmdldFBhZ2VVcmw9ZnVuY3Rpb24oKXt2YXIgZT1oLmdldFN0YXRlKCExLCExKSx0PShlfHx7fSkudXJsfHxoLmdldExvY2F0aW9uSHJlZigpLG47cmV0dXJuIG49dC5yZXBsYWNlKC9cXC8rJC8sXCJcIikucmVwbGFjZSgvW15cXC9dKyQvLGZ1bmN0aW9uKGUsdCxuKXtyZXR1cm4vXFwuLy50ZXN0KGUpP2U6ZStcIi9cIn0pLG59LGguZ2V0QmFzZVBhZ2VVcmw9ZnVuY3Rpb24oKXt2YXIgZT1oLmdldExvY2F0aW9uSHJlZigpLnJlcGxhY2UoL1sjXFw/XS4qLyxcIlwiKS5yZXBsYWNlKC9bXlxcL10rJC8sZnVuY3Rpb24oZSx0LG4pe3JldHVybi9bXlxcL10kLy50ZXN0KGUpP1wiXCI6ZX0pLnJlcGxhY2UoL1xcLyskLyxcIlwiKStcIi9cIjtyZXR1cm4gZX0saC5nZXRGdWxsVXJsPWZ1bmN0aW9uKGUsdCl7dmFyIG49ZSxyPWUuc3Vic3RyaW5nKDAsMSk7cmV0dXJuIHQ9dHlwZW9mIHQ9PVwidW5kZWZpbmVkXCI/ITA6dCwvW2Etel0rXFw6XFwvXFwvLy50ZXN0KGUpfHwocj09PVwiL1wiP249aC5nZXRSb290VXJsKCkrZS5yZXBsYWNlKC9eXFwvKy8sXCJcIik6cj09PVwiI1wiP249aC5nZXRQYWdlVXJsKCkucmVwbGFjZSgvIy4qLyxcIlwiKStlOnI9PT1cIj9cIj9uPWguZ2V0UGFnZVVybCgpLnJlcGxhY2UoL1tcXD8jXS4qLyxcIlwiKStlOnQ/bj1oLmdldEJhc2VVcmwoKStlLnJlcGxhY2UoL14oXFwuXFwvKSsvLFwiXCIpOm49aC5nZXRCYXNlUGFnZVVybCgpK2UucmVwbGFjZSgvXihcXC5cXC8pKy8sXCJcIikpLG4ucmVwbGFjZSgvXFwjJC8sXCJcIil9LGguZ2V0U2hvcnRVcmw9ZnVuY3Rpb24oZSl7dmFyIHQ9ZSxuPWguZ2V0QmFzZVVybCgpLHI9aC5nZXRSb290VXJsKCk7cmV0dXJuIGguZW11bGF0ZWQucHVzaFN0YXRlJiYodD10LnJlcGxhY2UobixcIlwiKSksdD10LnJlcGxhY2UocixcIi9cIiksaC5pc1RyYWRpdGlvbmFsQW5jaG9yKHQpJiYodD1cIi4vXCIrdCksdD10LnJlcGxhY2UoL14oXFwuXFwvKSsvZyxcIi4vXCIpLnJlcGxhY2UoL1xcIyQvLFwiXCIpLHR9LGguZ2V0TG9jYXRpb25IcmVmPWZ1bmN0aW9uKGUpe3JldHVybiBlPWV8fHIsZS5VUkw9PT1lLmxvY2F0aW9uLmhyZWY/ZS5sb2NhdGlvbi5ocmVmOmUubG9jYXRpb24uaHJlZj09PWRlY29kZVVSSUNvbXBvbmVudChlLlVSTCk/ZS5VUkw6ZS5sb2NhdGlvbi5oYXNoJiZkZWNvZGVVUklDb21wb25lbnQoZS5sb2NhdGlvbi5ocmVmLnJlcGxhY2UoL15bXiNdKy8sXCJcIikpPT09ZS5sb2NhdGlvbi5oYXNoP2UubG9jYXRpb24uaHJlZjplLlVSTC5pbmRleE9mKFwiI1wiKT09LTEmJmUubG9jYXRpb24uaHJlZi5pbmRleE9mKFwiI1wiKSE9LTE/ZS5sb2NhdGlvbi5ocmVmOmUuVVJMfHxlLmxvY2F0aW9uLmhyZWZ9LGguc3RvcmU9e30saC5pZFRvU3RhdGU9aC5pZFRvU3RhdGV8fHt9LGguc3RhdGVUb0lkPWguc3RhdGVUb0lkfHx7fSxoLnVybFRvSWQ9aC51cmxUb0lkfHx7fSxoLnN0b3JlZFN0YXRlcz1oLnN0b3JlZFN0YXRlc3x8W10saC5zYXZlZFN0YXRlcz1oLnNhdmVkU3RhdGVzfHxbXSxoLm5vcm1hbGl6ZVN0b3JlPWZ1bmN0aW9uKCl7aC5zdG9yZS5pZFRvU3RhdGU9aC5zdG9yZS5pZFRvU3RhdGV8fHt9LGguc3RvcmUudXJsVG9JZD1oLnN0b3JlLnVybFRvSWR8fHt9LGguc3RvcmUuc3RhdGVUb0lkPWguc3RvcmUuc3RhdGVUb0lkfHx7fX0saC5nZXRTdGF0ZT1mdW5jdGlvbihlLHQpe3R5cGVvZiBlPT1cInVuZGVmaW5lZFwiJiYoZT0hMCksdHlwZW9mIHQ9PVwidW5kZWZpbmVkXCImJih0PSEwKTt2YXIgbj1oLmdldExhc3RTYXZlZFN0YXRlKCk7cmV0dXJuIW4mJnQmJihuPWguY3JlYXRlU3RhdGVPYmplY3QoKSksZSYmKG49aC5jbG9uZU9iamVjdChuKSxuLnVybD1uLmNsZWFuVXJsfHxuLnVybCksbn0saC5nZXRJZEJ5U3RhdGU9ZnVuY3Rpb24oZSl7dmFyIHQ9aC5leHRyYWN0SWQoZS51cmwpLG47aWYoIXQpe249aC5nZXRTdGF0ZVN0cmluZyhlKTtpZih0eXBlb2YgaC5zdGF0ZVRvSWRbbl0hPVwidW5kZWZpbmVkXCIpdD1oLnN0YXRlVG9JZFtuXTtlbHNlIGlmKHR5cGVvZiBoLnN0b3JlLnN0YXRlVG9JZFtuXSE9XCJ1bmRlZmluZWRcIil0PWguc3RvcmUuc3RhdGVUb0lkW25dO2Vsc2V7Zm9yKDs7KXt0PShuZXcgRGF0ZSkuZ2V0VGltZSgpK1N0cmluZyhNYXRoLnJhbmRvbSgpKS5yZXBsYWNlKC9cXEQvZyxcIlwiKTtpZih0eXBlb2YgaC5pZFRvU3RhdGVbdF09PVwidW5kZWZpbmVkXCImJnR5cGVvZiBoLnN0b3JlLmlkVG9TdGF0ZVt0XT09XCJ1bmRlZmluZWRcIilicmVha31oLnN0YXRlVG9JZFtuXT10LGguaWRUb1N0YXRlW3RdPWV9fXJldHVybiB0fSxoLm5vcm1hbGl6ZVN0YXRlPWZ1bmN0aW9uKGUpe3ZhciB0LG47aWYoIWV8fHR5cGVvZiBlIT1cIm9iamVjdFwiKWU9e307aWYodHlwZW9mIGUubm9ybWFsaXplZCE9XCJ1bmRlZmluZWRcIilyZXR1cm4gZTtpZighZS5kYXRhfHx0eXBlb2YgZS5kYXRhIT1cIm9iamVjdFwiKWUuZGF0YT17fTtyZXR1cm4gdD17fSx0Lm5vcm1hbGl6ZWQ9ITAsdC50aXRsZT1lLnRpdGxlfHxcIlwiLHQudXJsPWguZ2V0RnVsbFVybChlLnVybD9lLnVybDpoLmdldExvY2F0aW9uSHJlZigpKSx0Lmhhc2g9aC5nZXRTaG9ydFVybCh0LnVybCksdC5kYXRhPWguY2xvbmVPYmplY3QoZS5kYXRhKSx0LmlkPWguZ2V0SWRCeVN0YXRlKHQpLHQuY2xlYW5Vcmw9dC51cmwucmVwbGFjZSgvXFw/P1xcJl9zdWlkLiovLFwiXCIpLHQudXJsPXQuY2xlYW5Vcmwsbj0haC5pc0VtcHR5T2JqZWN0KHQuZGF0YSksKHQudGl0bGV8fG4pJiZoLm9wdGlvbnMuZGlzYWJsZVN1aWQhPT0hMCYmKHQuaGFzaD1oLmdldFNob3J0VXJsKHQudXJsKS5yZXBsYWNlKC9cXD8/XFwmX3N1aWQuKi8sXCJcIiksL1xcPy8udGVzdCh0Lmhhc2gpfHwodC5oYXNoKz1cIj9cIiksdC5oYXNoKz1cIiZfc3VpZD1cIit0LmlkKSx0Lmhhc2hlZFVybD1oLmdldEZ1bGxVcmwodC5oYXNoKSwoaC5lbXVsYXRlZC5wdXNoU3RhdGV8fGguYnVncy5zYWZhcmlQb2xsKSYmaC5oYXNVcmxEdXBsaWNhdGUodCkmJih0LnVybD10Lmhhc2hlZFVybCksdH0saC5jcmVhdGVTdGF0ZU9iamVjdD1mdW5jdGlvbihlLHQsbil7dmFyIHI9e2RhdGE6ZSx0aXRsZTp0LHVybDpufTtyZXR1cm4gcj1oLm5vcm1hbGl6ZVN0YXRlKHIpLHJ9LGguZ2V0U3RhdGVCeUlkPWZ1bmN0aW9uKGUpe2U9U3RyaW5nKGUpO3ZhciBuPWguaWRUb1N0YXRlW2VdfHxoLnN0b3JlLmlkVG9TdGF0ZVtlXXx8dDtyZXR1cm4gbn0saC5nZXRTdGF0ZVN0cmluZz1mdW5jdGlvbihlKXt2YXIgdCxuLHI7cmV0dXJuIHQ9aC5ub3JtYWxpemVTdGF0ZShlKSxuPXtkYXRhOnQuZGF0YSx0aXRsZTplLnRpdGxlLHVybDplLnVybH0scj1sLnN0cmluZ2lmeShuKSxyfSxoLmdldFN0YXRlSWQ9ZnVuY3Rpb24oZSl7dmFyIHQsbjtyZXR1cm4gdD1oLm5vcm1hbGl6ZVN0YXRlKGUpLG49dC5pZCxufSxoLmdldEhhc2hCeVN0YXRlPWZ1bmN0aW9uKGUpe3ZhciB0LG47cmV0dXJuIHQ9aC5ub3JtYWxpemVTdGF0ZShlKSxuPXQuaGFzaCxufSxoLmV4dHJhY3RJZD1mdW5jdGlvbihlKXt2YXIgdCxuLHIsaTtyZXR1cm4gZS5pbmRleE9mKFwiI1wiKSE9LTE/aT1lLnNwbGl0KFwiI1wiKVswXTppPWUsbj0vKC4qKVxcJl9zdWlkPShbMC05XSspJC8uZXhlYyhpKSxyPW4/blsxXXx8ZTplLHQ9bj9TdHJpbmcoblsyXXx8XCJcIik6XCJcIix0fHwhMX0saC5pc1RyYWRpdGlvbmFsQW5jaG9yPWZ1bmN0aW9uKGUpe3ZhciB0PSEvW1xcL1xcP1xcLl0vLnRlc3QoZSk7cmV0dXJuIHR9LGguZXh0cmFjdFN0YXRlPWZ1bmN0aW9uKGUsdCl7dmFyIG49bnVsbCxyLGk7cmV0dXJuIHQ9dHx8ITEscj1oLmV4dHJhY3RJZChlKSxyJiYobj1oLmdldFN0YXRlQnlJZChyKSksbnx8KGk9aC5nZXRGdWxsVXJsKGUpLHI9aC5nZXRJZEJ5VXJsKGkpfHwhMSxyJiYobj1oLmdldFN0YXRlQnlJZChyKSksIW4mJnQmJiFoLmlzVHJhZGl0aW9uYWxBbmNob3IoZSkmJihuPWguY3JlYXRlU3RhdGVPYmplY3QobnVsbCxudWxsLGkpKSksbn0saC5nZXRJZEJ5VXJsPWZ1bmN0aW9uKGUpe3ZhciBuPWgudXJsVG9JZFtlXXx8aC5zdG9yZS51cmxUb0lkW2VdfHx0O3JldHVybiBufSxoLmdldExhc3RTYXZlZFN0YXRlPWZ1bmN0aW9uKCl7cmV0dXJuIGguc2F2ZWRTdGF0ZXNbaC5zYXZlZFN0YXRlcy5sZW5ndGgtMV18fHR9LGguZ2V0TGFzdFN0b3JlZFN0YXRlPWZ1bmN0aW9uKCl7cmV0dXJuIGguc3RvcmVkU3RhdGVzW2guc3RvcmVkU3RhdGVzLmxlbmd0aC0xXXx8dH0saC5oYXNVcmxEdXBsaWNhdGU9ZnVuY3Rpb24oZSl7dmFyIHQ9ITEsbjtyZXR1cm4gbj1oLmV4dHJhY3RTdGF0ZShlLnVybCksdD1uJiZuLmlkIT09ZS5pZCx0fSxoLnN0b3JlU3RhdGU9ZnVuY3Rpb24oZSl7cmV0dXJuIGgudXJsVG9JZFtlLnVybF09ZS5pZCxoLnN0b3JlZFN0YXRlcy5wdXNoKGguY2xvbmVPYmplY3QoZSkpLGV9LGguaXNMYXN0U2F2ZWRTdGF0ZT1mdW5jdGlvbihlKXt2YXIgdD0hMSxuLHIsaTtyZXR1cm4gaC5zYXZlZFN0YXRlcy5sZW5ndGgmJihuPWUuaWQscj1oLmdldExhc3RTYXZlZFN0YXRlKCksaT1yLmlkLHQ9bj09PWkpLHR9LGguc2F2ZVN0YXRlPWZ1bmN0aW9uKGUpe3JldHVybiBoLmlzTGFzdFNhdmVkU3RhdGUoZSk/ITE6KGguc2F2ZWRTdGF0ZXMucHVzaChoLmNsb25lT2JqZWN0KGUpKSwhMCl9LGguZ2V0U3RhdGVCeUluZGV4PWZ1bmN0aW9uKGUpe3ZhciB0PW51bGw7cmV0dXJuIHR5cGVvZiBlPT1cInVuZGVmaW5lZFwiP3Q9aC5zYXZlZFN0YXRlc1toLnNhdmVkU3RhdGVzLmxlbmd0aC0xXTplPDA/dD1oLnNhdmVkU3RhdGVzW2guc2F2ZWRTdGF0ZXMubGVuZ3RoK2VdOnQ9aC5zYXZlZFN0YXRlc1tlXSx0fSxoLmdldEN1cnJlbnRJbmRleD1mdW5jdGlvbigpe3ZhciBlPW51bGw7cmV0dXJuIGguc2F2ZWRTdGF0ZXMubGVuZ3RoPDE/ZT0wOmU9aC5zYXZlZFN0YXRlcy5sZW5ndGgtMSxlfSxoLmdldEhhc2g9ZnVuY3Rpb24oZSl7dmFyIHQ9aC5nZXRMb2NhdGlvbkhyZWYoZSksbjtyZXR1cm4gbj1oLmdldEhhc2hCeVVybCh0KSxufSxoLnVuZXNjYXBlSGFzaD1mdW5jdGlvbihlKXt2YXIgdD1oLm5vcm1hbGl6ZUhhc2goZSk7cmV0dXJuIHQ9ZGVjb2RlVVJJQ29tcG9uZW50KHQpLHR9LGgubm9ybWFsaXplSGFzaD1mdW5jdGlvbihlKXt2YXIgdD1lLnJlcGxhY2UoL1teI10qIy8sXCJcIikucmVwbGFjZSgvIy4qLyxcIlwiKTtyZXR1cm4gdH0saC5zZXRIYXNoPWZ1bmN0aW9uKGUsdCl7dmFyIG4saTtyZXR1cm4gdCE9PSExJiZoLmJ1c3koKT8oaC5wdXNoUXVldWUoe3Njb3BlOmgsY2FsbGJhY2s6aC5zZXRIYXNoLGFyZ3M6YXJndW1lbnRzLHF1ZXVlOnR9KSwhMSk6KGguYnVzeSghMCksbj1oLmV4dHJhY3RTdGF0ZShlLCEwKSxuJiYhaC5lbXVsYXRlZC5wdXNoU3RhdGU/aC5wdXNoU3RhdGUobi5kYXRhLG4udGl0bGUsbi51cmwsITEpOmguZ2V0SGFzaCgpIT09ZSYmKGguYnVncy5zZXRIYXNoPyhpPWguZ2V0UGFnZVVybCgpLGgucHVzaFN0YXRlKG51bGwsbnVsbCxpK1wiI1wiK2UsITEpKTpyLmxvY2F0aW9uLmhhc2g9ZSksaCl9LGguZXNjYXBlSGFzaD1mdW5jdGlvbih0KXt2YXIgbj1oLm5vcm1hbGl6ZUhhc2godCk7cmV0dXJuIG49ZS5lbmNvZGVVUklDb21wb25lbnQobiksaC5idWdzLmhhc2hFc2NhcGV8fChuPW4ucmVwbGFjZSgvXFwlMjEvZyxcIiFcIikucmVwbGFjZSgvXFwlMjYvZyxcIiZcIikucmVwbGFjZSgvXFwlM0QvZyxcIj1cIikucmVwbGFjZSgvXFwlM0YvZyxcIj9cIikpLG59LGguZ2V0SGFzaEJ5VXJsPWZ1bmN0aW9uKGUpe3ZhciB0PVN0cmluZyhlKS5yZXBsYWNlKC8oW14jXSopIz8oW14jXSopIz8oLiopLyxcIiQyXCIpO3JldHVybiB0PWgudW5lc2NhcGVIYXNoKHQpLHR9LGguc2V0VGl0bGU9ZnVuY3Rpb24oZSl7dmFyIHQ9ZS50aXRsZSxuO3R8fChuPWguZ2V0U3RhdGVCeUluZGV4KDApLG4mJm4udXJsPT09ZS51cmwmJih0PW4udGl0bGV8fGgub3B0aW9ucy5pbml0aWFsVGl0bGUpKTt0cnl7ci5nZXRFbGVtZW50c0J5VGFnTmFtZShcInRpdGxlXCIpWzBdLmlubmVySFRNTD10LnJlcGxhY2UoXCI8XCIsXCImbHQ7XCIpLnJlcGxhY2UoXCI+XCIsXCImZ3Q7XCIpLnJlcGxhY2UoXCIgJiBcIixcIiAmYW1wOyBcIil9Y2F0Y2goaSl7fXJldHVybiByLnRpdGxlPXQsaH0saC5xdWV1ZXM9W10saC5idXN5PWZ1bmN0aW9uKGUpe3R5cGVvZiBlIT1cInVuZGVmaW5lZFwiP2guYnVzeS5mbGFnPWU6dHlwZW9mIGguYnVzeS5mbGFnPT1cInVuZGVmaW5lZFwiJiYoaC5idXN5LmZsYWc9ITEpO2lmKCFoLmJ1c3kuZmxhZyl7dShoLmJ1c3kudGltZW91dCk7dmFyIHQ9ZnVuY3Rpb24oKXt2YXIgZSxuLHI7aWYoaC5idXN5LmZsYWcpcmV0dXJuO2ZvcihlPWgucXVldWVzLmxlbmd0aC0xO2U+PTA7LS1lKXtuPWgucXVldWVzW2VdO2lmKG4ubGVuZ3RoPT09MCljb250aW51ZTtyPW4uc2hpZnQoKSxoLmZpcmVRdWV1ZUl0ZW0ociksaC5idXN5LnRpbWVvdXQ9byh0LGgub3B0aW9ucy5idXN5RGVsYXkpfX07aC5idXN5LnRpbWVvdXQ9byh0LGgub3B0aW9ucy5idXN5RGVsYXkpfXJldHVybiBoLmJ1c3kuZmxhZ30saC5idXN5LmZsYWc9ITEsaC5maXJlUXVldWVJdGVtPWZ1bmN0aW9uKGUpe3JldHVybiBlLmNhbGxiYWNrLmFwcGx5KGUuc2NvcGV8fGgsZS5hcmdzfHxbXSl9LGgucHVzaFF1ZXVlPWZ1bmN0aW9uKGUpe3JldHVybiBoLnF1ZXVlc1tlLnF1ZXVlfHwwXT1oLnF1ZXVlc1tlLnF1ZXVlfHwwXXx8W10saC5xdWV1ZXNbZS5xdWV1ZXx8MF0ucHVzaChlKSxofSxoLnF1ZXVlPWZ1bmN0aW9uKGUsdCl7cmV0dXJuIHR5cGVvZiBlPT1cImZ1bmN0aW9uXCImJihlPXtjYWxsYmFjazplfSksdHlwZW9mIHQhPVwidW5kZWZpbmVkXCImJihlLnF1ZXVlPXQpLGguYnVzeSgpP2gucHVzaFF1ZXVlKGUpOmguZmlyZVF1ZXVlSXRlbShlKSxofSxoLmNsZWFyUXVldWU9ZnVuY3Rpb24oKXtyZXR1cm4gaC5idXN5LmZsYWc9ITEsaC5xdWV1ZXM9W10saH0saC5zdGF0ZUNoYW5nZWQ9ITEsaC5kb3VibGVDaGVja2VyPSExLGguZG91YmxlQ2hlY2tDb21wbGV0ZT1mdW5jdGlvbigpe3JldHVybiBoLnN0YXRlQ2hhbmdlZD0hMCxoLmRvdWJsZUNoZWNrQ2xlYXIoKSxofSxoLmRvdWJsZUNoZWNrQ2xlYXI9ZnVuY3Rpb24oKXtyZXR1cm4gaC5kb3VibGVDaGVja2VyJiYodShoLmRvdWJsZUNoZWNrZXIpLGguZG91YmxlQ2hlY2tlcj0hMSksaH0saC5kb3VibGVDaGVjaz1mdW5jdGlvbihlKXtyZXR1cm4gaC5zdGF0ZUNoYW5nZWQ9ITEsaC5kb3VibGVDaGVja0NsZWFyKCksaC5idWdzLmllRG91YmxlQ2hlY2smJihoLmRvdWJsZUNoZWNrZXI9byhmdW5jdGlvbigpe3JldHVybiBoLmRvdWJsZUNoZWNrQ2xlYXIoKSxoLnN0YXRlQ2hhbmdlZHx8ZSgpLCEwfSxoLm9wdGlvbnMuZG91YmxlQ2hlY2tJbnRlcnZhbCkpLGh9LGguc2FmYXJpU3RhdGVQb2xsPWZ1bmN0aW9uKCl7dmFyIHQ9aC5leHRyYWN0U3RhdGUoaC5nZXRMb2NhdGlvbkhyZWYoKSksbjtpZighaC5pc0xhc3RTYXZlZFN0YXRlKHQpKXJldHVybiBuPXQsbnx8KG49aC5jcmVhdGVTdGF0ZU9iamVjdCgpKSxoLkFkYXB0ZXIudHJpZ2dlcihlLFwicG9wc3RhdGVcIiksaDtyZXR1cm59LGguYmFjaz1mdW5jdGlvbihlKXtyZXR1cm4gZSE9PSExJiZoLmJ1c3koKT8oaC5wdXNoUXVldWUoe3Njb3BlOmgsY2FsbGJhY2s6aC5iYWNrLGFyZ3M6YXJndW1lbnRzLHF1ZXVlOmV9KSwhMSk6KGguYnVzeSghMCksaC5kb3VibGVDaGVjayhmdW5jdGlvbigpe2guYmFjayghMSl9KSxwLmdvKC0xKSwhMCl9LGguZm9yd2FyZD1mdW5jdGlvbihlKXtyZXR1cm4gZSE9PSExJiZoLmJ1c3koKT8oaC5wdXNoUXVldWUoe3Njb3BlOmgsY2FsbGJhY2s6aC5mb3J3YXJkLGFyZ3M6YXJndW1lbnRzLHF1ZXVlOmV9KSwhMSk6KGguYnVzeSghMCksaC5kb3VibGVDaGVjayhmdW5jdGlvbigpe2guZm9yd2FyZCghMSl9KSxwLmdvKDEpLCEwKX0saC5nbz1mdW5jdGlvbihlLHQpe3ZhciBuO2lmKGU+MClmb3Iobj0xO248PWU7KytuKWguZm9yd2FyZCh0KTtlbHNle2lmKCEoZTwwKSl0aHJvdyBuZXcgRXJyb3IoXCJIaXN0b3J5LmdvOiBIaXN0b3J5LmdvIHJlcXVpcmVzIGEgcG9zaXRpdmUgb3IgbmVnYXRpdmUgaW50ZWdlciBwYXNzZWQuXCIpO2ZvcihuPS0xO24+PWU7LS1uKWguYmFjayh0KX1yZXR1cm4gaH07aWYoaC5lbXVsYXRlZC5wdXNoU3RhdGUpe3ZhciB2PWZ1bmN0aW9uKCl7fTtoLnB1c2hTdGF0ZT1oLnB1c2hTdGF0ZXx8dixoLnJlcGxhY2VTdGF0ZT1oLnJlcGxhY2VTdGF0ZXx8dn1lbHNlIGgub25Qb3BTdGF0ZT1mdW5jdGlvbih0LG4pe3ZhciByPSExLGk9ITEscyxvO3JldHVybiBoLmRvdWJsZUNoZWNrQ29tcGxldGUoKSxzPWguZ2V0SGFzaCgpLHM/KG89aC5leHRyYWN0U3RhdGUoc3x8aC5nZXRMb2NhdGlvbkhyZWYoKSwhMCksbz9oLnJlcGxhY2VTdGF0ZShvLmRhdGEsby50aXRsZSxvLnVybCwhMSk6KGguQWRhcHRlci50cmlnZ2VyKGUsXCJhbmNob3JjaGFuZ2VcIiksaC5idXN5KCExKSksaC5leHBlY3RlZFN0YXRlSWQ9ITEsITEpOihyPWguQWRhcHRlci5leHRyYWN0RXZlbnREYXRhKFwic3RhdGVcIix0LG4pfHwhMSxyP2k9aC5nZXRTdGF0ZUJ5SWQocik6aC5leHBlY3RlZFN0YXRlSWQ/aT1oLmdldFN0YXRlQnlJZChoLmV4cGVjdGVkU3RhdGVJZCk6aT1oLmV4dHJhY3RTdGF0ZShoLmdldExvY2F0aW9uSHJlZigpKSxpfHwoaT1oLmNyZWF0ZVN0YXRlT2JqZWN0KG51bGwsbnVsbCxoLmdldExvY2F0aW9uSHJlZigpKSksaC5leHBlY3RlZFN0YXRlSWQ9ITEsaC5pc0xhc3RTYXZlZFN0YXRlKGkpPyhoLmJ1c3koITEpLCExKTooaC5zdG9yZVN0YXRlKGkpLGguc2F2ZVN0YXRlKGkpLGguc2V0VGl0bGUoaSksaC5BZGFwdGVyLnRyaWdnZXIoZSxcInN0YXRlY2hhbmdlXCIpLGguYnVzeSghMSksITApKX0saC5BZGFwdGVyLmJpbmQoZSxcInBvcHN0YXRlXCIsaC5vblBvcFN0YXRlKSxoLnB1c2hTdGF0ZT1mdW5jdGlvbih0LG4scixpKXtpZihoLmdldEhhc2hCeVVybChyKSYmaC5lbXVsYXRlZC5wdXNoU3RhdGUpdGhyb3cgbmV3IEVycm9yKFwiSGlzdG9yeS5qcyBkb2VzIG5vdCBzdXBwb3J0IHN0YXRlcyB3aXRoIGZyYWdlbWVudC1pZGVudGlmaWVycyAoaGFzaGVzL2FuY2hvcnMpLlwiKTtpZihpIT09ITEmJmguYnVzeSgpKXJldHVybiBoLnB1c2hRdWV1ZSh7c2NvcGU6aCxjYWxsYmFjazpoLnB1c2hTdGF0ZSxhcmdzOmFyZ3VtZW50cyxxdWV1ZTppfSksITE7aC5idXN5KCEwKTt2YXIgcz1oLmNyZWF0ZVN0YXRlT2JqZWN0KHQsbixyKTtyZXR1cm4gaC5pc0xhc3RTYXZlZFN0YXRlKHMpP2guYnVzeSghMSk6KGguc3RvcmVTdGF0ZShzKSxoLmV4cGVjdGVkU3RhdGVJZD1zLmlkLHAucHVzaFN0YXRlKHMuaWQscy50aXRsZSxzLnVybCksaC5BZGFwdGVyLnRyaWdnZXIoZSxcInBvcHN0YXRlXCIpKSwhMH0saC5yZXBsYWNlU3RhdGU9ZnVuY3Rpb24odCxuLHIsaSl7aWYoaC5nZXRIYXNoQnlVcmwocikmJmguZW11bGF0ZWQucHVzaFN0YXRlKXRocm93IG5ldyBFcnJvcihcIkhpc3RvcnkuanMgZG9lcyBub3Qgc3VwcG9ydCBzdGF0ZXMgd2l0aCBmcmFnZW1lbnQtaWRlbnRpZmllcnMgKGhhc2hlcy9hbmNob3JzKS5cIik7aWYoaSE9PSExJiZoLmJ1c3koKSlyZXR1cm4gaC5wdXNoUXVldWUoe3Njb3BlOmgsY2FsbGJhY2s6aC5yZXBsYWNlU3RhdGUsYXJnczphcmd1bWVudHMscXVldWU6aX0pLCExO2guYnVzeSghMCk7dmFyIHM9aC5jcmVhdGVTdGF0ZU9iamVjdCh0LG4scik7cmV0dXJuIGguaXNMYXN0U2F2ZWRTdGF0ZShzKT9oLmJ1c3koITEpOihoLnN0b3JlU3RhdGUocyksaC5leHBlY3RlZFN0YXRlSWQ9cy5pZCxwLnJlcGxhY2VTdGF0ZShzLmlkLHMudGl0bGUscy51cmwpLGguQWRhcHRlci50cmlnZ2VyKGUsXCJwb3BzdGF0ZVwiKSksITB9O2lmKHMpe3RyeXtoLnN0b3JlPWwucGFyc2Uocy5nZXRJdGVtKFwiSGlzdG9yeS5zdG9yZVwiKSl8fHt9fWNhdGNoKG0pe2guc3RvcmU9e319aC5ub3JtYWxpemVTdG9yZSgpfWVsc2UgaC5zdG9yZT17fSxoLm5vcm1hbGl6ZVN0b3JlKCk7aC5BZGFwdGVyLmJpbmQoZSxcInVubG9hZFwiLGguY2xlYXJBbGxJbnRlcnZhbHMpLGguc2F2ZVN0YXRlKGguc3RvcmVTdGF0ZShoLmV4dHJhY3RTdGF0ZShoLmdldExvY2F0aW9uSHJlZigpLCEwKSkpLHMmJihoLm9uVW5sb2FkPWZ1bmN0aW9uKCl7dmFyIGUsdCxuO3RyeXtlPWwucGFyc2Uocy5nZXRJdGVtKFwiSGlzdG9yeS5zdG9yZVwiKSl8fHt9fWNhdGNoKHIpe2U9e319ZS5pZFRvU3RhdGU9ZS5pZFRvU3RhdGV8fHt9LGUudXJsVG9JZD1lLnVybFRvSWR8fHt9LGUuc3RhdGVUb0lkPWUuc3RhdGVUb0lkfHx7fTtmb3IodCBpbiBoLmlkVG9TdGF0ZSl7aWYoIWguaWRUb1N0YXRlLmhhc093blByb3BlcnR5KHQpKWNvbnRpbnVlO2UuaWRUb1N0YXRlW3RdPWguaWRUb1N0YXRlW3RdfWZvcih0IGluIGgudXJsVG9JZCl7aWYoIWgudXJsVG9JZC5oYXNPd25Qcm9wZXJ0eSh0KSljb250aW51ZTtlLnVybFRvSWRbdF09aC51cmxUb0lkW3RdfWZvcih0IGluIGguc3RhdGVUb0lkKXtpZighaC5zdGF0ZVRvSWQuaGFzT3duUHJvcGVydHkodCkpY29udGludWU7ZS5zdGF0ZVRvSWRbdF09aC5zdGF0ZVRvSWRbdF19aC5zdG9yZT1lLGgubm9ybWFsaXplU3RvcmUoKSxuPWwuc3RyaW5naWZ5KGUpO3RyeXtzLnNldEl0ZW0oXCJIaXN0b3J5LnN0b3JlXCIsbil9Y2F0Y2goaSl7aWYoaS5jb2RlIT09RE9NRXhjZXB0aW9uLlFVT1RBX0VYQ0VFREVEX0VSUil0aHJvdyBpO3MubGVuZ3RoJiYocy5yZW1vdmVJdGVtKFwiSGlzdG9yeS5zdG9yZVwiKSxzLnNldEl0ZW0oXCJIaXN0b3J5LnN0b3JlXCIsbikpfX0saC5pbnRlcnZhbExpc3QucHVzaChhKGgub25VbmxvYWQsaC5vcHRpb25zLnN0b3JlSW50ZXJ2YWwpKSxoLkFkYXB0ZXIuYmluZChlLFwiYmVmb3JldW5sb2FkXCIsaC5vblVubG9hZCksaC5BZGFwdGVyLmJpbmQoZSxcInVubG9hZFwiLGgub25VbmxvYWQpKTtpZighaC5lbXVsYXRlZC5wdXNoU3RhdGUpe2guYnVncy5zYWZhcmlQb2xsJiZoLmludGVydmFsTGlzdC5wdXNoKGEoaC5zYWZhcmlTdGF0ZVBvbGwsaC5vcHRpb25zLnNhZmFyaVBvbGxJbnRlcnZhbCkpO2lmKGkudmVuZG9yPT09XCJBcHBsZSBDb21wdXRlciwgSW5jLlwifHwoaS5hcHBDb2RlTmFtZXx8XCJcIik9PT1cIk1vemlsbGFcIiloLkFkYXB0ZXIuYmluZChlLFwiaGFzaGNoYW5nZVwiLGZ1bmN0aW9uKCl7aC5BZGFwdGVyLnRyaWdnZXIoZSxcInBvcHN0YXRlXCIpfSksaC5nZXRIYXNoKCkmJmguQWRhcHRlci5vbkRvbUxvYWQoZnVuY3Rpb24oKXtoLkFkYXB0ZXIudHJpZ2dlcihlLFwiaGFzaGNoYW5nZVwiKX0pfX0sKCFoLm9wdGlvbnN8fCFoLm9wdGlvbnMuZGVsYXlJbml0KSYmaC5pbml0KCl9KHdpbmRvdylcclxuIiwiLyohXHJcbiAqIEJJREkgZW1iZWRkaW5nIHN1cHBvcnQgZm9yIGpRdWVyeS5pMThuXHJcbiAqXHJcbiAqIENvcHlyaWdodCAoQykgMjAxNSwgRGF2aWQgQ2hhblxyXG4gKlxyXG4gKiBUaGlzIGNvZGUgaXMgZHVhbCBsaWNlbnNlZCBHUEx2MiBvciBsYXRlciBhbmQgTUlULiBZb3UgZG9uJ3QgaGF2ZSB0byBkb1xyXG4gKiBhbnl0aGluZyBzcGVjaWFsIHRvIGNob29zZSBvbmUgbGljZW5zZSBvciB0aGUgb3RoZXIgYW5kIHlvdSBkb24ndCBoYXZlIHRvXHJcbiAqIG5vdGlmeSBhbnlvbmUgd2hpY2ggbGljZW5zZSB5b3UgYXJlIHVzaW5nLiBZb3UgYXJlIGZyZWUgdG8gdXNlIHRoaXMgY29kZVxyXG4gKiBpbiBjb21tZXJjaWFsIHByb2plY3RzIGFzIGxvbmcgYXMgdGhlIGNvcHlyaWdodCBoZWFkZXIgaXMgbGVmdCBpbnRhY3QuXHJcbiAqIFNlZSBmaWxlcyBHUEwtTElDRU5TRSBhbmQgTUlULUxJQ0VOU0UgZm9yIGRldGFpbHMuXHJcbiAqXHJcbiAqIEBsaWNlbmNlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbmNlIDIuMCBvciBsYXRlclxyXG4gKiBAbGljZW5jZSBNSVQgTGljZW5zZVxyXG4gKi9cclxuXHJcbiggZnVuY3Rpb24gKCAkICkge1xyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHR2YXIgc3Ryb25nRGlyUmVnRXhwO1xyXG5cclxuXHQvKipcclxuXHQgKiBNYXRjaGVzIHRoZSBmaXJzdCBzdHJvbmcgZGlyZWN0aW9uYWxpdHkgY29kZXBvaW50OlxyXG5cdCAqIC0gaW4gZ3JvdXAgMSBpZiBpdCBpcyBMVFJcclxuXHQgKiAtIGluIGdyb3VwIDIgaWYgaXQgaXMgUlRMXHJcblx0ICogRG9lcyBub3QgbWF0Y2ggaWYgdGhlcmUgaXMgbm8gc3Ryb25nIGRpcmVjdGlvbmFsaXR5IGNvZGVwb2ludC5cclxuXHQgKlxyXG5cdCAqIEdlbmVyYXRlZCBieSBVbmljb2RlSlMgKHNlZSB0b29scy9zdHJvbmdEaXIpIGZyb20gdGhlIFVDRDsgc2VlXHJcblx0ICogaHR0cHM6Ly9waGFicmljYXRvci53aWtpbWVkaWEub3JnL2RpZmZ1c2lvbi9HVUpTLyAuXHJcblx0ICovXHJcblx0c3Ryb25nRGlyUmVnRXhwID0gbmV3IFJlZ0V4cChcclxuXHRcdCcoPzonICtcclxuXHRcdFx0JygnICtcclxuXHRcdFx0XHQnW1xcdTAwNDEtXFx1MDA1YVxcdTAwNjEtXFx1MDA3YVxcdTAwYWFcXHUwMGI1XFx1MDBiYVxcdTAwYzAtXFx1MDBkNlxcdTAwZDgtXFx1MDBmNlxcdTAwZjgtXFx1MDJiOFxcdTAyYmItXFx1MDJjMVxcdTAyZDBcXHUwMmQxXFx1MDJlMC1cXHUwMmU0XFx1MDJlZVxcdTAzNzAtXFx1MDM3M1xcdTAzNzZcXHUwMzc3XFx1MDM3YS1cXHUwMzdkXFx1MDM3ZlxcdTAzODZcXHUwMzg4LVxcdTAzOGFcXHUwMzhjXFx1MDM4ZS1cXHUwM2ExXFx1MDNhMy1cXHUwM2Y1XFx1MDNmNy1cXHUwNDgyXFx1MDQ4YS1cXHUwNTJmXFx1MDUzMS1cXHUwNTU2XFx1MDU1OS1cXHUwNTVmXFx1MDU2MS1cXHUwNTg3XFx1MDU4OVxcdTA5MDMtXFx1MDkzOVxcdTA5M2JcXHUwOTNkLVxcdTA5NDBcXHUwOTQ5LVxcdTA5NGNcXHUwOTRlLVxcdTA5NTBcXHUwOTU4LVxcdTA5NjFcXHUwOTY0LVxcdTA5ODBcXHUwOTgyXFx1MDk4M1xcdTA5ODUtXFx1MDk4Y1xcdTA5OGZcXHUwOTkwXFx1MDk5My1cXHUwOWE4XFx1MDlhYS1cXHUwOWIwXFx1MDliMlxcdTA5YjYtXFx1MDliOVxcdTA5YmQtXFx1MDljMFxcdTA5YzdcXHUwOWM4XFx1MDljYlxcdTA5Y2NcXHUwOWNlXFx1MDlkN1xcdTA5ZGNcXHUwOWRkXFx1MDlkZi1cXHUwOWUxXFx1MDllNi1cXHUwOWYxXFx1MDlmNC1cXHUwOWZhXFx1MGEwM1xcdTBhMDUtXFx1MGEwYVxcdTBhMGZcXHUwYTEwXFx1MGExMy1cXHUwYTI4XFx1MGEyYS1cXHUwYTMwXFx1MGEzMlxcdTBhMzNcXHUwYTM1XFx1MGEzNlxcdTBhMzhcXHUwYTM5XFx1MGEzZS1cXHUwYTQwXFx1MGE1OS1cXHUwYTVjXFx1MGE1ZVxcdTBhNjYtXFx1MGE2ZlxcdTBhNzItXFx1MGE3NFxcdTBhODNcXHUwYTg1LVxcdTBhOGRcXHUwYThmLVxcdTBhOTFcXHUwYTkzLVxcdTBhYThcXHUwYWFhLVxcdTBhYjBcXHUwYWIyXFx1MGFiM1xcdTBhYjUtXFx1MGFiOVxcdTBhYmQtXFx1MGFjMFxcdTBhYzlcXHUwYWNiXFx1MGFjY1xcdTBhZDBcXHUwYWUwXFx1MGFlMVxcdTBhZTYtXFx1MGFmMFxcdTBhZjlcXHUwYjAyXFx1MGIwM1xcdTBiMDUtXFx1MGIwY1xcdTBiMGZcXHUwYjEwXFx1MGIxMy1cXHUwYjI4XFx1MGIyYS1cXHUwYjMwXFx1MGIzMlxcdTBiMzNcXHUwYjM1LVxcdTBiMzlcXHUwYjNkXFx1MGIzZVxcdTBiNDBcXHUwYjQ3XFx1MGI0OFxcdTBiNGJcXHUwYjRjXFx1MGI1N1xcdTBiNWNcXHUwYjVkXFx1MGI1Zi1cXHUwYjYxXFx1MGI2Ni1cXHUwYjc3XFx1MGI4M1xcdTBiODUtXFx1MGI4YVxcdTBiOGUtXFx1MGI5MFxcdTBiOTItXFx1MGI5NVxcdTBiOTlcXHUwYjlhXFx1MGI5Y1xcdTBiOWVcXHUwYjlmXFx1MGJhM1xcdTBiYTRcXHUwYmE4LVxcdTBiYWFcXHUwYmFlLVxcdTBiYjlcXHUwYmJlXFx1MGJiZlxcdTBiYzFcXHUwYmMyXFx1MGJjNi1cXHUwYmM4XFx1MGJjYS1cXHUwYmNjXFx1MGJkMFxcdTBiZDdcXHUwYmU2LVxcdTBiZjJcXHUwYzAxLVxcdTBjMDNcXHUwYzA1LVxcdTBjMGNcXHUwYzBlLVxcdTBjMTBcXHUwYzEyLVxcdTBjMjhcXHUwYzJhLVxcdTBjMzlcXHUwYzNkXFx1MGM0MS1cXHUwYzQ0XFx1MGM1OC1cXHUwYzVhXFx1MGM2MFxcdTBjNjFcXHUwYzY2LVxcdTBjNmZcXHUwYzdmXFx1MGM4MlxcdTBjODNcXHUwYzg1LVxcdTBjOGNcXHUwYzhlLVxcdTBjOTBcXHUwYzkyLVxcdTBjYThcXHUwY2FhLVxcdTBjYjNcXHUwY2I1LVxcdTBjYjlcXHUwY2JkLVxcdTBjYzRcXHUwY2M2LVxcdTBjYzhcXHUwY2NhXFx1MGNjYlxcdTBjZDVcXHUwY2Q2XFx1MGNkZVxcdTBjZTBcXHUwY2UxXFx1MGNlNi1cXHUwY2VmXFx1MGNmMVxcdTBjZjJcXHUwZDAyXFx1MGQwM1xcdTBkMDUtXFx1MGQwY1xcdTBkMGUtXFx1MGQxMFxcdTBkMTItXFx1MGQzYVxcdTBkM2QtXFx1MGQ0MFxcdTBkNDYtXFx1MGQ0OFxcdTBkNGEtXFx1MGQ0Y1xcdTBkNGVcXHUwZDU3XFx1MGQ1Zi1cXHUwZDYxXFx1MGQ2Ni1cXHUwZDc1XFx1MGQ3OS1cXHUwZDdmXFx1MGQ4MlxcdTBkODNcXHUwZDg1LVxcdTBkOTZcXHUwZDlhLVxcdTBkYjFcXHUwZGIzLVxcdTBkYmJcXHUwZGJkXFx1MGRjMC1cXHUwZGM2XFx1MGRjZi1cXHUwZGQxXFx1MGRkOC1cXHUwZGRmXFx1MGRlNi1cXHUwZGVmXFx1MGRmMi1cXHUwZGY0XFx1MGUwMS1cXHUwZTMwXFx1MGUzMlxcdTBlMzNcXHUwZTQwLVxcdTBlNDZcXHUwZTRmLVxcdTBlNWJcXHUwZTgxXFx1MGU4MlxcdTBlODRcXHUwZTg3XFx1MGU4OFxcdTBlOGFcXHUwZThkXFx1MGU5NC1cXHUwZTk3XFx1MGU5OS1cXHUwZTlmXFx1MGVhMS1cXHUwZWEzXFx1MGVhNVxcdTBlYTdcXHUwZWFhXFx1MGVhYlxcdTBlYWQtXFx1MGViMFxcdTBlYjJcXHUwZWIzXFx1MGViZFxcdTBlYzAtXFx1MGVjNFxcdTBlYzZcXHUwZWQwLVxcdTBlZDlcXHUwZWRjLVxcdTBlZGZcXHUwZjAwLVxcdTBmMTdcXHUwZjFhLVxcdTBmMzRcXHUwZjM2XFx1MGYzOFxcdTBmM2UtXFx1MGY0N1xcdTBmNDktXFx1MGY2Y1xcdTBmN2ZcXHUwZjg1XFx1MGY4OC1cXHUwZjhjXFx1MGZiZS1cXHUwZmM1XFx1MGZjNy1cXHUwZmNjXFx1MGZjZS1cXHUwZmRhXFx1MTAwMC1cXHUxMDJjXFx1MTAzMVxcdTEwMzhcXHUxMDNiXFx1MTAzY1xcdTEwM2YtXFx1MTA1N1xcdTEwNWEtXFx1MTA1ZFxcdTEwNjEtXFx1MTA3MFxcdTEwNzUtXFx1MTA4MVxcdTEwODNcXHUxMDg0XFx1MTA4Ny1cXHUxMDhjXFx1MTA4ZS1cXHUxMDljXFx1MTA5ZS1cXHUxMGM1XFx1MTBjN1xcdTEwY2RcXHUxMGQwLVxcdTEyNDhcXHUxMjRhLVxcdTEyNGRcXHUxMjUwLVxcdTEyNTZcXHUxMjU4XFx1MTI1YS1cXHUxMjVkXFx1MTI2MC1cXHUxMjg4XFx1MTI4YS1cXHUxMjhkXFx1MTI5MC1cXHUxMmIwXFx1MTJiMi1cXHUxMmI1XFx1MTJiOC1cXHUxMmJlXFx1MTJjMFxcdTEyYzItXFx1MTJjNVxcdTEyYzgtXFx1MTJkNlxcdTEyZDgtXFx1MTMxMFxcdTEzMTItXFx1MTMxNVxcdTEzMTgtXFx1MTM1YVxcdTEzNjAtXFx1MTM3Y1xcdTEzODAtXFx1MTM4ZlxcdTEzYTAtXFx1MTNmNVxcdTEzZjgtXFx1MTNmZFxcdTE0MDEtXFx1MTY3ZlxcdTE2ODEtXFx1MTY5YVxcdTE2YTAtXFx1MTZmOFxcdTE3MDAtXFx1MTcwY1xcdTE3MGUtXFx1MTcxMVxcdTE3MjAtXFx1MTczMVxcdTE3MzVcXHUxNzM2XFx1MTc0MC1cXHUxNzUxXFx1MTc2MC1cXHUxNzZjXFx1MTc2ZS1cXHUxNzcwXFx1MTc4MC1cXHUxN2IzXFx1MTdiNlxcdTE3YmUtXFx1MTdjNVxcdTE3YzdcXHUxN2M4XFx1MTdkNC1cXHUxN2RhXFx1MTdkY1xcdTE3ZTAtXFx1MTdlOVxcdTE4MTAtXFx1MTgxOVxcdTE4MjAtXFx1MTg3N1xcdTE4ODAtXFx1MThhOFxcdTE4YWFcXHUxOGIwLVxcdTE4ZjVcXHUxOTAwLVxcdTE5MWVcXHUxOTIzLVxcdTE5MjZcXHUxOTI5LVxcdTE5MmJcXHUxOTMwXFx1MTkzMVxcdTE5MzMtXFx1MTkzOFxcdTE5NDYtXFx1MTk2ZFxcdTE5NzAtXFx1MTk3NFxcdTE5ODAtXFx1MTlhYlxcdTE5YjAtXFx1MTljOVxcdTE5ZDAtXFx1MTlkYVxcdTFhMDAtXFx1MWExNlxcdTFhMTlcXHUxYTFhXFx1MWExZS1cXHUxYTU1XFx1MWE1N1xcdTFhNjFcXHUxYTYzXFx1MWE2NFxcdTFhNmQtXFx1MWE3MlxcdTFhODAtXFx1MWE4OVxcdTFhOTAtXFx1MWE5OVxcdTFhYTAtXFx1MWFhZFxcdTFiMDQtXFx1MWIzM1xcdTFiMzVcXHUxYjNiXFx1MWIzZC1cXHUxYjQxXFx1MWI0My1cXHUxYjRiXFx1MWI1MC1cXHUxYjZhXFx1MWI3NC1cXHUxYjdjXFx1MWI4Mi1cXHUxYmExXFx1MWJhNlxcdTFiYTdcXHUxYmFhXFx1MWJhZS1cXHUxYmU1XFx1MWJlN1xcdTFiZWEtXFx1MWJlY1xcdTFiZWVcXHUxYmYyXFx1MWJmM1xcdTFiZmMtXFx1MWMyYlxcdTFjMzRcXHUxYzM1XFx1MWMzYi1cXHUxYzQ5XFx1MWM0ZC1cXHUxYzdmXFx1MWNjMC1cXHUxY2M3XFx1MWNkM1xcdTFjZTFcXHUxY2U5LVxcdTFjZWNcXHUxY2VlLVxcdTFjZjNcXHUxY2Y1XFx1MWNmNlxcdTFkMDAtXFx1MWRiZlxcdTFlMDAtXFx1MWYxNVxcdTFmMTgtXFx1MWYxZFxcdTFmMjAtXFx1MWY0NVxcdTFmNDgtXFx1MWY0ZFxcdTFmNTAtXFx1MWY1N1xcdTFmNTlcXHUxZjViXFx1MWY1ZFxcdTFmNWYtXFx1MWY3ZFxcdTFmODAtXFx1MWZiNFxcdTFmYjYtXFx1MWZiY1xcdTFmYmVcXHUxZmMyLVxcdTFmYzRcXHUxZmM2LVxcdTFmY2NcXHUxZmQwLVxcdTFmZDNcXHUxZmQ2LVxcdTFmZGJcXHUxZmUwLVxcdTFmZWNcXHUxZmYyLVxcdTFmZjRcXHUxZmY2LVxcdTFmZmNcXHUyMDBlXFx1MjA3MVxcdTIwN2ZcXHUyMDkwLVxcdTIwOWNcXHUyMTAyXFx1MjEwN1xcdTIxMGEtXFx1MjExM1xcdTIxMTVcXHUyMTE5LVxcdTIxMWRcXHUyMTI0XFx1MjEyNlxcdTIxMjhcXHUyMTJhLVxcdTIxMmRcXHUyMTJmLVxcdTIxMzlcXHUyMTNjLVxcdTIxM2ZcXHUyMTQ1LVxcdTIxNDlcXHUyMTRlXFx1MjE0ZlxcdTIxNjAtXFx1MjE4OFxcdTIzMzYtXFx1MjM3YVxcdTIzOTVcXHUyNDljLVxcdTI0ZTlcXHUyNmFjXFx1MjgwMC1cXHUyOGZmXFx1MmMwMC1cXHUyYzJlXFx1MmMzMC1cXHUyYzVlXFx1MmM2MC1cXHUyY2U0XFx1MmNlYi1cXHUyY2VlXFx1MmNmMlxcdTJjZjNcXHUyZDAwLVxcdTJkMjVcXHUyZDI3XFx1MmQyZFxcdTJkMzAtXFx1MmQ2N1xcdTJkNmZcXHUyZDcwXFx1MmQ4MC1cXHUyZDk2XFx1MmRhMC1cXHUyZGE2XFx1MmRhOC1cXHUyZGFlXFx1MmRiMC1cXHUyZGI2XFx1MmRiOC1cXHUyZGJlXFx1MmRjMC1cXHUyZGM2XFx1MmRjOC1cXHUyZGNlXFx1MmRkMC1cXHUyZGQ2XFx1MmRkOC1cXHUyZGRlXFx1MzAwNS1cXHUzMDA3XFx1MzAyMS1cXHUzMDI5XFx1MzAyZVxcdTMwMmZcXHUzMDMxLVxcdTMwMzVcXHUzMDM4LVxcdTMwM2NcXHUzMDQxLVxcdTMwOTZcXHUzMDlkLVxcdTMwOWZcXHUzMGExLVxcdTMwZmFcXHUzMGZjLVxcdTMwZmZcXHUzMTA1LVxcdTMxMmRcXHUzMTMxLVxcdTMxOGVcXHUzMTkwLVxcdTMxYmFcXHUzMWYwLVxcdTMyMWNcXHUzMjIwLVxcdTMyNGZcXHUzMjYwLVxcdTMyN2JcXHUzMjdmLVxcdTMyYjBcXHUzMmMwLVxcdTMyY2JcXHUzMmQwLVxcdTMyZmVcXHUzMzAwLVxcdTMzNzZcXHUzMzdiLVxcdTMzZGRcXHUzM2UwLVxcdTMzZmVcXHUzNDAwLVxcdTRkYjVcXHU0ZTAwLVxcdTlmZDVcXHVhMDAwLVxcdWE0OGNcXHVhNGQwLVxcdWE2MGNcXHVhNjEwLVxcdWE2MmJcXHVhNjQwLVxcdWE2NmVcXHVhNjgwLVxcdWE2OWRcXHVhNmEwLVxcdWE2ZWZcXHVhNmYyLVxcdWE2ZjdcXHVhNzIyLVxcdWE3ODdcXHVhNzg5LVxcdWE3YWRcXHVhN2IwLVxcdWE3YjdcXHVhN2Y3LVxcdWE4MDFcXHVhODAzLVxcdWE4MDVcXHVhODA3LVxcdWE4MGFcXHVhODBjLVxcdWE4MjRcXHVhODI3XFx1YTgzMC1cXHVhODM3XFx1YTg0MC1cXHVhODczXFx1YTg4MC1cXHVhOGMzXFx1YThjZS1cXHVhOGQ5XFx1YThmMi1cXHVhOGZkXFx1YTkwMC1cXHVhOTI1XFx1YTkyZS1cXHVhOTQ2XFx1YTk1MlxcdWE5NTNcXHVhOTVmLVxcdWE5N2NcXHVhOTgzLVxcdWE5YjJcXHVhOWI0XFx1YTliNVxcdWE5YmFcXHVhOWJiXFx1YTliZC1cXHVhOWNkXFx1YTljZi1cXHVhOWQ5XFx1YTlkZS1cXHVhOWU0XFx1YTllNi1cXHVhOWZlXFx1YWEwMC1cXHVhYTI4XFx1YWEyZlxcdWFhMzBcXHVhYTMzXFx1YWEzNFxcdWFhNDAtXFx1YWE0MlxcdWFhNDQtXFx1YWE0YlxcdWFhNGRcXHVhYTUwLVxcdWFhNTlcXHVhYTVjLVxcdWFhN2JcXHVhYTdkLVxcdWFhYWZcXHVhYWIxXFx1YWFiNVxcdWFhYjZcXHVhYWI5LVxcdWFhYmRcXHVhYWMwXFx1YWFjMlxcdWFhZGItXFx1YWFlYlxcdWFhZWUtXFx1YWFmNVxcdWFiMDEtXFx1YWIwNlxcdWFiMDktXFx1YWIwZVxcdWFiMTEtXFx1YWIxNlxcdWFiMjAtXFx1YWIyNlxcdWFiMjgtXFx1YWIyZVxcdWFiMzAtXFx1YWI2NVxcdWFiNzAtXFx1YWJlNFxcdWFiZTZcXHVhYmU3XFx1YWJlOS1cXHVhYmVjXFx1YWJmMC1cXHVhYmY5XFx1YWMwMC1cXHVkN2EzXFx1ZDdiMC1cXHVkN2M2XFx1ZDdjYi1cXHVkN2ZiXFx1ZTAwMC1cXHVmYTZkXFx1ZmE3MC1cXHVmYWQ5XFx1ZmIwMC1cXHVmYjA2XFx1ZmIxMy1cXHVmYjE3XFx1ZmYyMS1cXHVmZjNhXFx1ZmY0MS1cXHVmZjVhXFx1ZmY2Ni1cXHVmZmJlXFx1ZmZjMi1cXHVmZmM3XFx1ZmZjYS1cXHVmZmNmXFx1ZmZkMi1cXHVmZmQ3XFx1ZmZkYS1cXHVmZmRjXXxcXHVkODAwW1xcdWRjMDAtXFx1ZGMwYl18XFx1ZDgwMFtcXHVkYzBkLVxcdWRjMjZdfFxcdWQ4MDBbXFx1ZGMyOC1cXHVkYzNhXXxcXHVkODAwXFx1ZGMzY3xcXHVkODAwXFx1ZGMzZHxcXHVkODAwW1xcdWRjM2YtXFx1ZGM0ZF18XFx1ZDgwMFtcXHVkYzUwLVxcdWRjNWRdfFxcdWQ4MDBbXFx1ZGM4MC1cXHVkY2ZhXXxcXHVkODAwXFx1ZGQwMHxcXHVkODAwXFx1ZGQwMnxcXHVkODAwW1xcdWRkMDctXFx1ZGQzM118XFx1ZDgwMFtcXHVkZDM3LVxcdWRkM2ZdfFxcdWQ4MDBbXFx1ZGRkMC1cXHVkZGZjXXxcXHVkODAwW1xcdWRlODAtXFx1ZGU5Y118XFx1ZDgwMFtcXHVkZWEwLVxcdWRlZDBdfFxcdWQ4MDBbXFx1ZGYwMC1cXHVkZjIzXXxcXHVkODAwW1xcdWRmMzAtXFx1ZGY0YV18XFx1ZDgwMFtcXHVkZjUwLVxcdWRmNzVdfFxcdWQ4MDBbXFx1ZGY4MC1cXHVkZjlkXXxcXHVkODAwW1xcdWRmOWYtXFx1ZGZjM118XFx1ZDgwMFtcXHVkZmM4LVxcdWRmZDVdfFxcdWQ4MDFbXFx1ZGMwMC1cXHVkYzlkXXxcXHVkODAxW1xcdWRjYTAtXFx1ZGNhOV18XFx1ZDgwMVtcXHVkZDAwLVxcdWRkMjddfFxcdWQ4MDFbXFx1ZGQzMC1cXHVkZDYzXXxcXHVkODAxXFx1ZGQ2ZnxcXHVkODAxW1xcdWRlMDAtXFx1ZGYzNl18XFx1ZDgwMVtcXHVkZjQwLVxcdWRmNTVdfFxcdWQ4MDFbXFx1ZGY2MC1cXHVkZjY3XXxcXHVkODA0XFx1ZGMwMHxcXHVkODA0W1xcdWRjMDItXFx1ZGMzN118XFx1ZDgwNFtcXHVkYzQ3LVxcdWRjNGRdfFxcdWQ4MDRbXFx1ZGM2Ni1cXHVkYzZmXXxcXHVkODA0W1xcdWRjODItXFx1ZGNiMl18XFx1ZDgwNFxcdWRjYjd8XFx1ZDgwNFxcdWRjYjh8XFx1ZDgwNFtcXHVkY2JiLVxcdWRjYzFdfFxcdWQ4MDRbXFx1ZGNkMC1cXHVkY2U4XXxcXHVkODA0W1xcdWRjZjAtXFx1ZGNmOV18XFx1ZDgwNFtcXHVkZDAzLVxcdWRkMjZdfFxcdWQ4MDRcXHVkZDJjfFxcdWQ4MDRbXFx1ZGQzNi1cXHVkZDQzXXxcXHVkODA0W1xcdWRkNTAtXFx1ZGQ3Ml18XFx1ZDgwNFtcXHVkZDc0LVxcdWRkNzZdfFxcdWQ4MDRbXFx1ZGQ4Mi1cXHVkZGI1XXxcXHVkODA0W1xcdWRkYmYtXFx1ZGRjOV18XFx1ZDgwNFxcdWRkY2R8XFx1ZDgwNFtcXHVkZGQwLVxcdWRkZGZdfFxcdWQ4MDRbXFx1ZGRlMS1cXHVkZGY0XXxcXHVkODA0W1xcdWRlMDAtXFx1ZGUxMV18XFx1ZDgwNFtcXHVkZTEzLVxcdWRlMmVdfFxcdWQ4MDRcXHVkZTMyfFxcdWQ4MDRcXHVkZTMzfFxcdWQ4MDRcXHVkZTM1fFxcdWQ4MDRbXFx1ZGUzOC1cXHVkZTNkXXxcXHVkODA0W1xcdWRlODAtXFx1ZGU4Nl18XFx1ZDgwNFxcdWRlODh8XFx1ZDgwNFtcXHVkZThhLVxcdWRlOGRdfFxcdWQ4MDRbXFx1ZGU4Zi1cXHVkZTlkXXxcXHVkODA0W1xcdWRlOWYtXFx1ZGVhOV18XFx1ZDgwNFtcXHVkZWIwLVxcdWRlZGVdfFxcdWQ4MDRbXFx1ZGVlMC1cXHVkZWUyXXxcXHVkODA0W1xcdWRlZjAtXFx1ZGVmOV18XFx1ZDgwNFxcdWRmMDJ8XFx1ZDgwNFxcdWRmMDN8XFx1ZDgwNFtcXHVkZjA1LVxcdWRmMGNdfFxcdWQ4MDRcXHVkZjBmfFxcdWQ4MDRcXHVkZjEwfFxcdWQ4MDRbXFx1ZGYxMy1cXHVkZjI4XXxcXHVkODA0W1xcdWRmMmEtXFx1ZGYzMF18XFx1ZDgwNFxcdWRmMzJ8XFx1ZDgwNFxcdWRmMzN8XFx1ZDgwNFtcXHVkZjM1LVxcdWRmMzldfFxcdWQ4MDRbXFx1ZGYzZC1cXHVkZjNmXXxcXHVkODA0W1xcdWRmNDEtXFx1ZGY0NF18XFx1ZDgwNFxcdWRmNDd8XFx1ZDgwNFxcdWRmNDh8XFx1ZDgwNFtcXHVkZjRiLVxcdWRmNGRdfFxcdWQ4MDRcXHVkZjUwfFxcdWQ4MDRcXHVkZjU3fFxcdWQ4MDRbXFx1ZGY1ZC1cXHVkZjYzXXxcXHVkODA1W1xcdWRjODAtXFx1ZGNiMl18XFx1ZDgwNVxcdWRjYjl8XFx1ZDgwNVtcXHVkY2JiLVxcdWRjYmVdfFxcdWQ4MDVcXHVkY2MxfFxcdWQ4MDVbXFx1ZGNjNC1cXHVkY2M3XXxcXHVkODA1W1xcdWRjZDAtXFx1ZGNkOV18XFx1ZDgwNVtcXHVkZDgwLVxcdWRkYjFdfFxcdWQ4MDVbXFx1ZGRiOC1cXHVkZGJiXXxcXHVkODA1XFx1ZGRiZXxcXHVkODA1W1xcdWRkYzEtXFx1ZGRkYl18XFx1ZDgwNVtcXHVkZTAwLVxcdWRlMzJdfFxcdWQ4MDVcXHVkZTNifFxcdWQ4MDVcXHVkZTNjfFxcdWQ4MDVcXHVkZTNlfFxcdWQ4MDVbXFx1ZGU0MS1cXHVkZTQ0XXxcXHVkODA1W1xcdWRlNTAtXFx1ZGU1OV18XFx1ZDgwNVtcXHVkZTgwLVxcdWRlYWFdfFxcdWQ4MDVcXHVkZWFjfFxcdWQ4MDVcXHVkZWFlfFxcdWQ4MDVcXHVkZWFmfFxcdWQ4MDVcXHVkZWI2fFxcdWQ4MDVbXFx1ZGVjMC1cXHVkZWM5XXxcXHVkODA1W1xcdWRmMDAtXFx1ZGYxOV18XFx1ZDgwNVxcdWRmMjB8XFx1ZDgwNVxcdWRmMjF8XFx1ZDgwNVxcdWRmMjZ8XFx1ZDgwNVtcXHVkZjMwLVxcdWRmM2ZdfFxcdWQ4MDZbXFx1ZGNhMC1cXHVkY2YyXXxcXHVkODA2XFx1ZGNmZnxcXHVkODA2W1xcdWRlYzAtXFx1ZGVmOF18XFx1ZDgwOFtcXHVkYzAwLVxcdWRmOTldfFxcdWQ4MDlbXFx1ZGMwMC1cXHVkYzZlXXxcXHVkODA5W1xcdWRjNzAtXFx1ZGM3NF18XFx1ZDgwOVtcXHVkYzgwLVxcdWRkNDNdfFxcdWQ4MGNbXFx1ZGMwMC1cXHVkZmZmXXxcXHVkODBkW1xcdWRjMDAtXFx1ZGMyZV18XFx1ZDgxMVtcXHVkYzAwLVxcdWRlNDZdfFxcdWQ4MWFbXFx1ZGMwMC1cXHVkZTM4XXxcXHVkODFhW1xcdWRlNDAtXFx1ZGU1ZV18XFx1ZDgxYVtcXHVkZTYwLVxcdWRlNjldfFxcdWQ4MWFcXHVkZTZlfFxcdWQ4MWFcXHVkZTZmfFxcdWQ4MWFbXFx1ZGVkMC1cXHVkZWVkXXxcXHVkODFhXFx1ZGVmNXxcXHVkODFhW1xcdWRmMDAtXFx1ZGYyZl18XFx1ZDgxYVtcXHVkZjM3LVxcdWRmNDVdfFxcdWQ4MWFbXFx1ZGY1MC1cXHVkZjU5XXxcXHVkODFhW1xcdWRmNWItXFx1ZGY2MV18XFx1ZDgxYVtcXHVkZjYzLVxcdWRmNzddfFxcdWQ4MWFbXFx1ZGY3ZC1cXHVkZjhmXXxcXHVkODFiW1xcdWRmMDAtXFx1ZGY0NF18XFx1ZDgxYltcXHVkZjUwLVxcdWRmN2VdfFxcdWQ4MWJbXFx1ZGY5My1cXHVkZjlmXXxcXHVkODJjXFx1ZGMwMHxcXHVkODJjXFx1ZGMwMXxcXHVkODJmW1xcdWRjMDAtXFx1ZGM2YV18XFx1ZDgyZltcXHVkYzcwLVxcdWRjN2NdfFxcdWQ4MmZbXFx1ZGM4MC1cXHVkYzg4XXxcXHVkODJmW1xcdWRjOTAtXFx1ZGM5OV18XFx1ZDgyZlxcdWRjOWN8XFx1ZDgyZlxcdWRjOWZ8XFx1ZDgzNFtcXHVkYzAwLVxcdWRjZjVdfFxcdWQ4MzRbXFx1ZGQwMC1cXHVkZDI2XXxcXHVkODM0W1xcdWRkMjktXFx1ZGQ2Nl18XFx1ZDgzNFtcXHVkZDZhLVxcdWRkNzJdfFxcdWQ4MzRcXHVkZDgzfFxcdWQ4MzRcXHVkZDg0fFxcdWQ4MzRbXFx1ZGQ4Yy1cXHVkZGE5XXxcXHVkODM0W1xcdWRkYWUtXFx1ZGRlOF18XFx1ZDgzNFtcXHVkZjYwLVxcdWRmNzFdfFxcdWQ4MzVbXFx1ZGMwMC1cXHVkYzU0XXxcXHVkODM1W1xcdWRjNTYtXFx1ZGM5Y118XFx1ZDgzNVxcdWRjOWV8XFx1ZDgzNVxcdWRjOWZ8XFx1ZDgzNVxcdWRjYTJ8XFx1ZDgzNVxcdWRjYTV8XFx1ZDgzNVxcdWRjYTZ8XFx1ZDgzNVtcXHVkY2E5LVxcdWRjYWNdfFxcdWQ4MzVbXFx1ZGNhZS1cXHVkY2I5XXxcXHVkODM1XFx1ZGNiYnxcXHVkODM1W1xcdWRjYmQtXFx1ZGNjM118XFx1ZDgzNVtcXHVkY2M1LVxcdWRkMDVdfFxcdWQ4MzVbXFx1ZGQwNy1cXHVkZDBhXXxcXHVkODM1W1xcdWRkMGQtXFx1ZGQxNF18XFx1ZDgzNVtcXHVkZDE2LVxcdWRkMWNdfFxcdWQ4MzVbXFx1ZGQxZS1cXHVkZDM5XXxcXHVkODM1W1xcdWRkM2ItXFx1ZGQzZV18XFx1ZDgzNVtcXHVkZDQwLVxcdWRkNDRdfFxcdWQ4MzVcXHVkZDQ2fFxcdWQ4MzVbXFx1ZGQ0YS1cXHVkZDUwXXxcXHVkODM1W1xcdWRkNTItXFx1ZGVhNV18XFx1ZDgzNVtcXHVkZWE4LVxcdWRlZGFdfFxcdWQ4MzVbXFx1ZGVkYy1cXHVkZjE0XXxcXHVkODM1W1xcdWRmMTYtXFx1ZGY0ZV18XFx1ZDgzNVtcXHVkZjUwLVxcdWRmODhdfFxcdWQ4MzVbXFx1ZGY4YS1cXHVkZmMyXXxcXHVkODM1W1xcdWRmYzQtXFx1ZGZjYl18XFx1ZDgzNltcXHVkYzAwLVxcdWRkZmZdfFxcdWQ4MzZbXFx1ZGUzNy1cXHVkZTNhXXxcXHVkODM2W1xcdWRlNmQtXFx1ZGU3NF18XFx1ZDgzNltcXHVkZTc2LVxcdWRlODNdfFxcdWQ4MzZbXFx1ZGU4NS1cXHVkZThiXXxcXHVkODNjW1xcdWRkMTAtXFx1ZGQyZV18XFx1ZDgzY1tcXHVkZDMwLVxcdWRkNjldfFxcdWQ4M2NbXFx1ZGQ3MC1cXHVkZDlhXXxcXHVkODNjW1xcdWRkZTYtXFx1ZGUwMl18XFx1ZDgzY1tcXHVkZTEwLVxcdWRlM2FdfFxcdWQ4M2NbXFx1ZGU0MC1cXHVkZTQ4XXxcXHVkODNjXFx1ZGU1MHxcXHVkODNjXFx1ZGU1MXxbXFx1ZDg0MC1cXHVkODY4XVtcXHVkYzAwLVxcdWRmZmZdfFxcdWQ4NjlbXFx1ZGMwMC1cXHVkZWQ2XXxcXHVkODY5W1xcdWRmMDAtXFx1ZGZmZl18W1xcdWQ4NmEtXFx1ZDg2Y11bXFx1ZGMwMC1cXHVkZmZmXXxcXHVkODZkW1xcdWRjMDAtXFx1ZGYzNF18XFx1ZDg2ZFtcXHVkZjQwLVxcdWRmZmZdfFxcdWQ4NmVbXFx1ZGMwMC1cXHVkYzFkXXxcXHVkODZlW1xcdWRjMjAtXFx1ZGZmZl18W1xcdWQ4NmYtXFx1ZDg3Ml1bXFx1ZGMwMC1cXHVkZmZmXXxcXHVkODczW1xcdWRjMDAtXFx1ZGVhMV18XFx1ZDg3ZVtcXHVkYzAwLVxcdWRlMWRdfFtcXHVkYjgwLVxcdWRiYmVdW1xcdWRjMDAtXFx1ZGZmZl18XFx1ZGJiZltcXHVkYzAwLVxcdWRmZmRdfFtcXHVkYmMwLVxcdWRiZmVdW1xcdWRjMDAtXFx1ZGZmZl18XFx1ZGJmZltcXHVkYzAwLVxcdWRmZmRdJyArXHJcblx0XHRcdCcpfCgnICtcclxuXHRcdFx0XHQnW1xcdTA1OTBcXHUwNWJlXFx1MDVjMFxcdTA1YzNcXHUwNWM2XFx1MDVjOC1cXHUwNWZmXFx1MDdjMC1cXHUwN2VhXFx1MDdmNFxcdTA3ZjVcXHUwN2ZhLVxcdTA4MTVcXHUwODFhXFx1MDgyNFxcdTA4MjhcXHUwODJlLVxcdTA4NThcXHUwODVjLVxcdTA4OWZcXHUyMDBmXFx1ZmIxZFxcdWZiMWYtXFx1ZmIyOFxcdWZiMmEtXFx1ZmI0ZlxcdTA2MDhcXHUwNjBiXFx1MDYwZFxcdTA2MWItXFx1MDY0YVxcdTA2NmQtXFx1MDY2ZlxcdTA2NzEtXFx1MDZkNVxcdTA2ZTVcXHUwNmU2XFx1MDZlZVxcdTA2ZWZcXHUwNmZhLVxcdTA3MTBcXHUwNzEyLVxcdTA3MmZcXHUwNzRiLVxcdTA3YTVcXHUwN2IxLVxcdTA3YmZcXHUwOGEwLVxcdTA4ZTJcXHVmYjUwLVxcdWZkM2RcXHVmZDQwLVxcdWZkY2ZcXHVmZGYwLVxcdWZkZmNcXHVmZGZlXFx1ZmRmZlxcdWZlNzAtXFx1ZmVmZV18XFx1ZDgwMltcXHVkYzAwLVxcdWRkMWVdfFxcdWQ4MDJbXFx1ZGQyMC1cXHVkZTAwXXxcXHVkODAyXFx1ZGUwNHxcXHVkODAyW1xcdWRlMDctXFx1ZGUwYl18XFx1ZDgwMltcXHVkZTEwLVxcdWRlMzddfFxcdWQ4MDJbXFx1ZGUzYi1cXHVkZTNlXXxcXHVkODAyW1xcdWRlNDAtXFx1ZGVlNF18XFx1ZDgwMltcXHVkZWU3LVxcdWRmMzhdfFxcdWQ4MDJbXFx1ZGY0MC1cXHVkZmZmXXxcXHVkODAzW1xcdWRjMDAtXFx1ZGU1Zl18XFx1ZDgwM1tcXHVkZTdmLVxcdWRmZmZdfFxcdWQ4M2FbXFx1ZGMwMC1cXHVkY2NmXXxcXHVkODNhW1xcdWRjZDctXFx1ZGZmZl18XFx1ZDgzYltcXHVkYzAwLVxcdWRkZmZdfFxcdWQ4M2JbXFx1ZGYwMC1cXHVkZmZmXXxcXHVkODNiW1xcdWRmMDAtXFx1ZGZmZl18XFx1ZDgzYltcXHVkZjAwLVxcdWRmZmZdfFxcdWQ4M2JbXFx1ZGYwMC1cXHVkZmZmXXxcXHVkODNiW1xcdWRmMDAtXFx1ZGZmZl18XFx1ZDgzYltcXHVkZjAwLVxcdWRmZmZdfFxcdWQ4M2JbXFx1ZGYwMC1cXHVkZmZmXXxcXHVkODNiW1xcdWRmMDAtXFx1ZGZmZl18XFx1ZDgzYltcXHVkZjAwLVxcdWRmZmZdfFxcdWQ4M2JbXFx1ZGYwMC1cXHVkZmZmXXxcXHVkODNiW1xcdWRmMDAtXFx1ZGZmZl18XFx1ZDgzYltcXHVkZjAwLVxcdWRmZmZdfFxcdWQ4M2JbXFx1ZGYwMC1cXHVkZmZmXXxcXHVkODNiW1xcdWRlMDAtXFx1ZGVlZl18XFx1ZDgzYltcXHVkZWYyLVxcdWRlZmZdJyArXHJcblx0XHRcdCcpJyArXHJcblx0XHQnKSdcclxuXHQpO1xyXG5cclxuXHQvKipcclxuXHQgKiBHZXRzIGRpcmVjdGlvbmFsaXR5IG9mIHRoZSBmaXJzdCBzdHJvbmdseSBkaXJlY3Rpb25hbCBjb2RlcG9pbnRcclxuXHQgKlxyXG5cdCAqIFRoaXMgaXMgdGhlIHJ1bGUgdGhlIEJJREkgYWxnb3JpdGhtIHVzZXMgdG8gZGV0ZXJtaW5lIHRoZSBkaXJlY3Rpb25hbGl0eSBvZlxyXG5cdCAqIHBhcmFncmFwaHMgKCBodHRwOi8vdW5pY29kZS5vcmcvcmVwb3J0cy90cjkvI1RoZV9QYXJhZ3JhcGhfTGV2ZWwgKSBhbmRcclxuXHQgKiBGU0kgaXNvbGF0ZXMgKCBodHRwOi8vdW5pY29kZS5vcmcvcmVwb3J0cy90cjkvI0V4cGxpY2l0X0RpcmVjdGlvbmFsX0lzb2xhdGVzICkuXHJcblx0ICpcclxuXHQgKiBUT0RPOiBEb2VzIG5vdCBoYW5kbGUgQklESSBjb250cm9sIGNoYXJhY3RlcnMgaW5zaWRlIHRoZSB0ZXh0LlxyXG5cdCAqIFRPRE86IERvZXMgbm90IGhhbmRsZSB1bmFsbG9jYXRlZCBjaGFyYWN0ZXJzLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtzdHJpbmd9IHRleHQgVGhlIHRleHQgZnJvbSB3aGljaCB0byBleHRyYWN0IGluaXRpYWwgZGlyZWN0aW9uYWxpdHkuXHJcblx0ICogQHJldHVybiB7c3RyaW5nfSBEaXJlY3Rpb25hbGl0eSAoZWl0aGVyICdsdHInIG9yICdydGwnKVxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIHN0cm9uZ0RpckZyb21Db250ZW50KCB0ZXh0ICkge1xyXG5cdFx0dmFyIG0gPSB0ZXh0Lm1hdGNoKCBzdHJvbmdEaXJSZWdFeHAgKTtcclxuXHRcdGlmICggIW0gKSB7XHJcblx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0fVxyXG5cdFx0aWYgKCBtWyAyIF0gPT09IHVuZGVmaW5lZCApIHtcclxuXHRcdFx0cmV0dXJuICdsdHInO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuICdydGwnO1xyXG5cdH1cclxuXHJcblx0JC5leHRlbmQoICQuaTE4bi5wYXJzZXIuZW1pdHRlciwge1xyXG5cdFx0LyoqXHJcblx0XHQgKiBXcmFwcyBhcmd1bWVudCB3aXRoIHVuaWNvZGUgY29udHJvbCBjaGFyYWN0ZXJzIGZvciBkaXJlY3Rpb25hbGl0eSBzYWZldHlcclxuXHRcdCAqXHJcblx0XHQgKiBUaGlzIHNvbHZlcyB0aGUgcHJvYmxlbSB3aGVyZSBkaXJlY3Rpb25hbGl0eS1uZXV0cmFsIGNoYXJhY3RlcnMgYXQgdGhlIGVkZ2Ugb2ZcclxuXHRcdCAqIHRoZSBhcmd1bWVudCBzdHJpbmcgZ2V0IGludGVycHJldGVkIHdpdGggdGhlIHdyb25nIGRpcmVjdGlvbmFsaXR5IGZyb20gdGhlXHJcblx0XHQgKiBlbmNsb3NpbmcgY29udGV4dCwgZ2l2aW5nIHJlbmRlcmluZ3MgdGhhdCBsb29rIGNvcnJ1cHRlZCBsaWtlIFwiKEJlbl8oV01GXCIuXHJcblx0XHQgKlxyXG5cdFx0ICogVGhlIHdyYXBwaW5nIGlzIExSRS4uLlBERiBvciBSTEUuLi5QREYsIGRlcGVuZGluZyBvbiB0aGUgZGV0ZWN0ZWRcclxuXHRcdCAqIGRpcmVjdGlvbmFsaXR5IG9mIHRoZSBhcmd1bWVudCBzdHJpbmcsIHVzaW5nIHRoZSBCSURJIGFsZ29yaXRobSdzIG93biBcIkZpcnN0XHJcblx0XHQgKiBzdHJvbmcgZGlyZWN0aW9uYWwgY29kZXBvaW50XCIgcnVsZS4gRXNzZW50aWFsbHksIHRoaXMgd29ya3Mgcm91bmQgdGhlIGZhY3QgdGhhdFxyXG5cdFx0ICogdGhlcmUgaXMgbm8gZW1iZWRkaW5nIGVxdWl2YWxlbnQgb2YgVSsyMDY4IEZTSSAoaXNvbGF0aW9uIHdpdGggaGV1cmlzdGljXHJcblx0XHQgKiBkaXJlY3Rpb24gaW5mZXJlbmNlKS4gVGhlIGxhdHRlciBpcyBjbGVhbmVyIGJ1dCBzdGlsbCBub3Qgd2lkZWx5IHN1cHBvcnRlZC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge3N0cmluZ1tdfSBub2RlcyBUaGUgdGV4dCBub2RlcyBmcm9tIHdoaWNoIHRvIHRha2UgdGhlIGZpcnN0IGl0ZW0uXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9IFdyYXBwZWQgU3RyaW5nIG9mIGNvbnRlbnQgYXMgbmVlZGVkLlxyXG5cdFx0ICovXHJcblx0XHRiaWRpOiBmdW5jdGlvbiAoIG5vZGVzICkge1xyXG5cdFx0XHR2YXIgZGlyID0gc3Ryb25nRGlyRnJvbUNvbnRlbnQoIG5vZGVzWyAwIF0gKTtcclxuXHRcdFx0aWYgKCBkaXIgPT09ICdsdHInICkge1xyXG5cdFx0XHRcdC8vIFdyYXAgaW4gTEVGVC1UTy1SSUdIVCBFTUJFRERJTkcgLi4uIFBPUCBESVJFQ1RJT05BTCBGT1JNQVRUSU5HXHJcblx0XHRcdFx0cmV0dXJuICdcXHUyMDJBJyArIG5vZGVzWyAwIF0gKyAnXFx1MjAyQyc7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKCBkaXIgPT09ICdydGwnICkge1xyXG5cdFx0XHRcdC8vIFdyYXAgaW4gUklHSFQtVE8tTEVGVCBFTUJFRERJTkcgLi4uIFBPUCBESVJFQ1RJT05BTCBGT1JNQVRUSU5HXHJcblx0XHRcdFx0cmV0dXJuICdcXHUyMDJCJyArIG5vZGVzWyAwIF0gKyAnXFx1MjAyQyc7XHJcblx0XHRcdH1cclxuXHRcdFx0Ly8gTm8gc3Ryb25nIGRpcmVjdGlvbmFsaXR5OiBkbyBub3Qgd3JhcFxyXG5cdFx0XHRyZXR1cm4gbm9kZXNbIDAgXTtcclxuXHRcdH1cclxuXHR9ICk7XHJcbn0oIGpRdWVyeSApICk7XHJcbiIsIi8qIVxyXG4gKiBqUXVlcnkgSW50ZXJuYXRpb25hbGl6YXRpb24gbGlicmFyeVxyXG4gKlxyXG4gKiBDb3B5cmlnaHQgKEMpIDIwMTEtMjAxMyBTYW50aG9zaCBUaG90dGluZ2FsLCBOZWlsIEthbmRhbGdhb25rYXJcclxuICpcclxuICoganF1ZXJ5LmkxOG4gaXMgZHVhbCBsaWNlbnNlZCBHUEx2MiBvciBsYXRlciBhbmQgTUlULiBZb3UgZG9uJ3QgaGF2ZSB0byBkb1xyXG4gKiBhbnl0aGluZyBzcGVjaWFsIHRvIGNob29zZSBvbmUgbGljZW5zZSBvciB0aGUgb3RoZXIgYW5kIHlvdSBkb24ndCBoYXZlIHRvXHJcbiAqIG5vdGlmeSBhbnlvbmUgd2hpY2ggbGljZW5zZSB5b3UgYXJlIHVzaW5nLiBZb3UgYXJlIGZyZWUgdG8gdXNlXHJcbiAqIFVuaXZlcnNhbExhbmd1YWdlU2VsZWN0b3IgaW4gY29tbWVyY2lhbCBwcm9qZWN0cyBhcyBsb25nIGFzIHRoZSBjb3B5cmlnaHRcclxuICogaGVhZGVyIGlzIGxlZnQgaW50YWN0LiBTZWUgZmlsZXMgR1BMLUxJQ0VOU0UgYW5kIE1JVC1MSUNFTlNFIGZvciBkZXRhaWxzLlxyXG4gKlxyXG4gKiBAbGljZW5jZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5jZSAyLjAgb3IgbGF0ZXJcclxuICogQGxpY2VuY2UgTUlUIExpY2Vuc2VcclxuICovXHJcblxyXG4oIGZ1bmN0aW9uICggJCApIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblxyXG5cdHZhciBNZXNzYWdlUGFyc2VyRW1pdHRlciA9IGZ1bmN0aW9uICgpIHtcclxuXHRcdHRoaXMubGFuZ3VhZ2UgPSAkLmkxOG4ubGFuZ3VhZ2VzWyBTdHJpbmcubG9jYWxlIF0gfHwgJC5pMThuLmxhbmd1YWdlc1sgJ2RlZmF1bHQnIF07XHJcblx0fTtcclxuXHJcblx0TWVzc2FnZVBhcnNlckVtaXR0ZXIucHJvdG90eXBlID0ge1xyXG5cdFx0Y29uc3RydWN0b3I6IE1lc3NhZ2VQYXJzZXJFbWl0dGVyLFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogKFdlIHB1dCB0aGlzIG1ldGhvZCBkZWZpbml0aW9uIGhlcmUsIGFuZCBub3QgaW4gcHJvdG90eXBlLCB0byBtYWtlXHJcblx0XHQgKiBzdXJlIGl0J3Mgbm90IG92ZXJ3cml0dGVuIGJ5IGFueSBtYWdpYy4pIFdhbGsgZW50aXJlIG5vZGUgc3RydWN0dXJlLFxyXG5cdFx0ICogYXBwbHlpbmcgcmVwbGFjZW1lbnRzIGFuZCB0ZW1wbGF0ZSBmdW5jdGlvbnMgd2hlbiBhcHByb3ByaWF0ZVxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7TWl4ZWR9IG5vZGUgYWJzdHJhY3Qgc3ludGF4IHRyZWUgKHRvcCBub2RlIG9yIHN1Ym5vZGUpXHJcblx0XHQgKiBAcGFyYW0ge0FycmF5fSByZXBsYWNlbWVudHMgZm9yICQxLCAkMiwgLi4uICRuXHJcblx0XHQgKiBAcmV0dXJuIHtNaXhlZH0gc2luZ2xlLXN0cmluZyBub2RlIG9yIGFycmF5IG9mIG5vZGVzIHN1aXRhYmxlIGZvclxyXG5cdFx0ICogIGpRdWVyeSBhcHBlbmRpbmcuXHJcblx0XHQgKi9cclxuXHRcdGVtaXQ6IGZ1bmN0aW9uICggbm9kZSwgcmVwbGFjZW1lbnRzICkge1xyXG5cdFx0XHR2YXIgcmV0LCBzdWJub2Rlcywgb3BlcmF0aW9uLFxyXG5cdFx0XHRcdG1lc3NhZ2VQYXJzZXJFbWl0dGVyID0gdGhpcztcclxuXHJcblx0XHRcdHN3aXRjaCAoIHR5cGVvZiBub2RlICkge1xyXG5cdFx0XHRcdGNhc2UgJ3N0cmluZyc6XHJcblx0XHRcdFx0Y2FzZSAnbnVtYmVyJzpcclxuXHRcdFx0XHRcdHJldCA9IG5vZGU7XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRjYXNlICdvYmplY3QnOlxyXG5cdFx0XHRcdC8vIG5vZGUgaXMgYW4gYXJyYXkgb2Ygbm9kZXNcclxuXHRcdFx0XHRcdHN1Ym5vZGVzID0gJC5tYXAoIG5vZGUuc2xpY2UoIDEgKSwgZnVuY3Rpb24gKCBuICkge1xyXG5cdFx0XHRcdFx0XHRyZXR1cm4gbWVzc2FnZVBhcnNlckVtaXR0ZXIuZW1pdCggbiwgcmVwbGFjZW1lbnRzICk7XHJcblx0XHRcdFx0XHR9ICk7XHJcblxyXG5cdFx0XHRcdFx0b3BlcmF0aW9uID0gbm9kZVsgMCBdLnRvTG93ZXJDYXNlKCk7XHJcblxyXG5cdFx0XHRcdFx0aWYgKCB0eXBlb2YgbWVzc2FnZVBhcnNlckVtaXR0ZXJbIG9wZXJhdGlvbiBdID09PSAnZnVuY3Rpb24nICkge1xyXG5cdFx0XHRcdFx0XHRyZXQgPSBtZXNzYWdlUGFyc2VyRW1pdHRlclsgb3BlcmF0aW9uIF0oIHN1Ym5vZGVzLCByZXBsYWNlbWVudHMgKTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdHRocm93IG5ldyBFcnJvciggJ3Vua25vd24gb3BlcmF0aW9uIFwiJyArIG9wZXJhdGlvbiArICdcIicgKTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRjYXNlICd1bmRlZmluZWQnOlxyXG5cdFx0XHRcdC8vIFBhcnNpbmcgdGhlIGVtcHR5IHN0cmluZyAoYXMgYW4gZW50aXJlIGV4cHJlc3Npb24sIG9yIGFzIGFcclxuXHRcdFx0XHQvLyBwYXJhbUV4cHJlc3Npb24gaW4gYSB0ZW1wbGF0ZSkgcmVzdWx0cyBpbiB1bmRlZmluZWRcclxuXHRcdFx0XHQvLyBQZXJoYXBzIGEgbW9yZSBjbGV2ZXIgcGFyc2VyIGNhbiBkZXRlY3QgdGhpcywgYW5kIHJldHVybiB0aGVcclxuXHRcdFx0XHQvLyBlbXB0eSBzdHJpbmc/IE9yIGlzIHRoYXQgdXNlZnVsIGluZm9ybWF0aW9uP1xyXG5cdFx0XHRcdC8vIFRoZSBsb2dpY2FsIHRoaW5nIGlzIHByb2JhYmx5IHRvIHJldHVybiB0aGUgZW1wdHkgc3RyaW5nIGhlcmVcclxuXHRcdFx0XHQvLyB3aGVuIHdlIGVuY291bnRlciB1bmRlZmluZWQuXHJcblx0XHRcdFx0XHRyZXQgPSAnJztcclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdGRlZmF1bHQ6XHJcblx0XHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoICd1bmV4cGVjdGVkIHR5cGUgaW4gQVNUOiAnICsgdHlwZW9mIG5vZGUgKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHJldDtcclxuXHRcdH0sXHJcblxyXG5cdFx0LyoqXHJcblx0XHQgKiBQYXJzaW5nIGhhcyBiZWVuIGFwcGxpZWQgZGVwdGgtZmlyc3Qgd2UgY2FuIGFzc3VtZSB0aGF0IGFsbCBub2Rlc1xyXG5cdFx0ICogaGVyZSBhcmUgc2luZ2xlIG5vZGVzIE11c3QgcmV0dXJuIGEgc2luZ2xlIG5vZGUgdG8gcGFyZW50cyAtLSBhXHJcblx0XHQgKiBqUXVlcnkgd2l0aCBzeW50aGV0aWMgc3BhbiBIb3dldmVyLCB1bndyYXAgYW55IG90aGVyIHN5bnRoZXRpYyBzcGFuc1xyXG5cdFx0ICogaW4gb3VyIGNoaWxkcmVuIGFuZCBwYXNzIHRoZW0gdXB3YXJkc1xyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7QXJyYXl9IG5vZGVzIE1peGVkLCBzb21lIHNpbmdsZSBub2Rlcywgc29tZSBhcnJheXMgb2Ygbm9kZXMuXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9XHJcblx0XHQgKi9cclxuXHRcdGNvbmNhdDogZnVuY3Rpb24gKCBub2RlcyApIHtcclxuXHRcdFx0dmFyIHJlc3VsdCA9ICcnO1xyXG5cclxuXHRcdFx0JC5lYWNoKCBub2RlcywgZnVuY3Rpb24gKCBpLCBub2RlICkge1xyXG5cdFx0XHRcdC8vIHN0cmluZ3MsIGludGVnZXJzLCBhbnl0aGluZyBlbHNlXHJcblx0XHRcdFx0cmVzdWx0ICs9IG5vZGU7XHJcblx0XHRcdH0gKTtcclxuXHJcblx0XHRcdHJldHVybiByZXN1bHQ7XHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogUmV0dXJuIGVzY2FwZWQgcmVwbGFjZW1lbnQgb2YgY29ycmVjdCBpbmRleCwgb3Igc3RyaW5nIGlmXHJcblx0XHQgKiB1bmF2YWlsYWJsZS4gTm90ZSB0aGF0IHdlIGV4cGVjdCB0aGUgcGFyc2VkIHBhcmFtZXRlciB0byBiZVxyXG5cdFx0ICogemVyby1iYXNlZC4gaS5lLiAkMSBzaG91bGQgaGF2ZSBiZWNvbWUgWyAwIF0uIGlmIHRoZSBzcGVjaWZpZWRcclxuXHRcdCAqIHBhcmFtZXRlciBpcyBub3QgZm91bmQgcmV0dXJuIHRoZSBzYW1lIHN0cmluZyAoZS5nLiBcIiQ5OVwiIC0+XHJcblx0XHQgKiBwYXJhbWV0ZXIgOTggLT4gbm90IGZvdW5kIC0+IHJldHVybiBcIiQ5OVwiICkgVE9ETyB0aHJvdyBlcnJvciBpZlxyXG5cdFx0ICogbm9kZXMubGVuZ3RoID4gMSA/XHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtBcnJheX0gbm9kZXMgT25lIGVsZW1lbnQsIGludGVnZXIsIG4gPj0gMFxyXG5cdFx0ICogQHBhcmFtIHtBcnJheX0gcmVwbGFjZW1lbnRzIGZvciAkMSwgJDIsIC4uLiAkblxyXG5cdFx0ICogQHJldHVybiB7c3RyaW5nfSByZXBsYWNlbWVudFxyXG5cdFx0ICovXHJcblx0XHRyZXBsYWNlOiBmdW5jdGlvbiAoIG5vZGVzLCByZXBsYWNlbWVudHMgKSB7XHJcblx0XHRcdHZhciBpbmRleCA9IHBhcnNlSW50KCBub2Rlc1sgMCBdLCAxMCApO1xyXG5cclxuXHRcdFx0aWYgKCBpbmRleCA8IHJlcGxhY2VtZW50cy5sZW5ndGggKSB7XHJcblx0XHRcdFx0Ly8gcmVwbGFjZW1lbnQgaXMgbm90IGEgc3RyaW5nLCBkb24ndCB0b3VjaCFcclxuXHRcdFx0XHRyZXR1cm4gcmVwbGFjZW1lbnRzWyBpbmRleCBdO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdC8vIGluZGV4IG5vdCBmb3VuZCwgZmFsbGJhY2sgdG8gZGlzcGxheWluZyB2YXJpYWJsZVxyXG5cdFx0XHRcdHJldHVybiAnJCcgKyAoIGluZGV4ICsgMSApO1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogVHJhbnNmb3JtIHBhcnNlZCBzdHJ1Y3R1cmUgaW50byBwbHVyYWxpemF0aW9uIG4uYi4gVGhlIGZpcnN0IG5vZGUgbWF5XHJcblx0XHQgKiBiZSBhIG5vbi1pbnRlZ2VyIChmb3IgaW5zdGFuY2UsIGEgc3RyaW5nIHJlcHJlc2VudGluZyBhbiBBcmFiaWNcclxuXHRcdCAqIG51bWJlcikuIFNvIGNvbnZlcnQgaXQgYmFjayB3aXRoIHRoZSBjdXJyZW50IGxhbmd1YWdlJ3NcclxuXHRcdCAqIGNvbnZlcnROdW1iZXIuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtBcnJheX0gbm9kZXMgTGlzdCBbIHtTdHJpbmd8TnVtYmVyfSwge1N0cmluZ30sIHtTdHJpbmd9IC4uLiBdXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9IHNlbGVjdGVkIHBsdXJhbGl6ZWQgZm9ybSBhY2NvcmRpbmcgdG8gY3VycmVudFxyXG5cdFx0ICogIGxhbmd1YWdlLlxyXG5cdFx0ICovXHJcblx0XHRwbHVyYWw6IGZ1bmN0aW9uICggbm9kZXMgKSB7XHJcblx0XHRcdHZhciBjb3VudCA9IHBhcnNlRmxvYXQoIHRoaXMubGFuZ3VhZ2UuY29udmVydE51bWJlciggbm9kZXNbIDAgXSwgMTAgKSApLFxyXG5cdFx0XHRcdGZvcm1zID0gbm9kZXMuc2xpY2UoIDEgKTtcclxuXHJcblx0XHRcdHJldHVybiBmb3Jtcy5sZW5ndGggPyB0aGlzLmxhbmd1YWdlLmNvbnZlcnRQbHVyYWwoIGNvdW50LCBmb3JtcyApIDogJyc7XHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogVHJhbnNmb3JtIHBhcnNlZCBzdHJ1Y3R1cmUgaW50byBnZW5kZXIgVXNhZ2VcclxuXHRcdCAqIHt7Z2VuZGVyOmdlbmRlcnxtYXNjdWxpbmV8ZmVtaW5pbmV8bmV1dHJhbH19LlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7QXJyYXl9IG5vZGVzIExpc3QgWyB7U3RyaW5nfSwge1N0cmluZ30sIHtTdHJpbmd9ICwge1N0cmluZ30gXVxyXG5cdFx0ICogQHJldHVybiB7c3RyaW5nfSBzZWxlY3RlZCBnZW5kZXIgZm9ybSBhY2NvcmRpbmcgdG8gY3VycmVudCBsYW5ndWFnZVxyXG5cdFx0ICovXHJcblx0XHRnZW5kZXI6IGZ1bmN0aW9uICggbm9kZXMgKSB7XHJcblx0XHRcdHZhciBnZW5kZXIgPSBub2Rlc1sgMCBdLFxyXG5cdFx0XHRcdGZvcm1zID0gbm9kZXMuc2xpY2UoIDEgKTtcclxuXHJcblx0XHRcdHJldHVybiB0aGlzLmxhbmd1YWdlLmdlbmRlciggZ2VuZGVyLCBmb3JtcyApO1xyXG5cdFx0fSxcclxuXHJcblx0XHQvKipcclxuXHRcdCAqIFRyYW5zZm9ybSBwYXJzZWQgc3RydWN0dXJlIGludG8gZ3JhbW1hciBjb252ZXJzaW9uLiBJbnZva2VkIGJ5XHJcblx0XHQgKiBwdXR0aW5nIHt7Z3JhbW1hcjpmb3JtfHdvcmR9fSBpbiBhIG1lc3NhZ2VcclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge0FycmF5fSBub2RlcyBMaXN0IFt7R3JhbW1hciBjYXNlIGVnOiBnZW5pdGl2ZX0sIHtTdHJpbmcgd29yZH1dXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9IHNlbGVjdGVkIGdyYW1tYXRpY2FsIGZvcm0gYWNjb3JkaW5nIHRvIGN1cnJlbnRcclxuXHRcdCAqICBsYW5ndWFnZS5cclxuXHRcdCAqL1xyXG5cdFx0Z3JhbW1hcjogZnVuY3Rpb24gKCBub2RlcyApIHtcclxuXHRcdFx0dmFyIGZvcm0gPSBub2Rlc1sgMCBdLFxyXG5cdFx0XHRcdHdvcmQgPSBub2Rlc1sgMSBdO1xyXG5cclxuXHRcdFx0cmV0dXJuIHdvcmQgJiYgZm9ybSAmJiB0aGlzLmxhbmd1YWdlLmNvbnZlcnRHcmFtbWFyKCB3b3JkLCBmb3JtICk7XHJcblx0XHR9XHJcblx0fTtcclxuXHJcblx0JC5leHRlbmQoICQuaTE4bi5wYXJzZXIuZW1pdHRlciwgbmV3IE1lc3NhZ2VQYXJzZXJFbWl0dGVyKCkgKTtcclxufSggalF1ZXJ5ICkgKTtcclxuIiwiLyohXHJcbiAqIGpRdWVyeSBJbnRlcm5hdGlvbmFsaXphdGlvbiBsaWJyYXJ5XHJcbiAqXHJcbiAqIENvcHlyaWdodCAoQykgMjAxMiBTYW50aG9zaCBUaG90dGluZ2FsXHJcbiAqXHJcbiAqIGpxdWVyeS5pMThuIGlzIGR1YWwgbGljZW5zZWQgR1BMdjIgb3IgbGF0ZXIgYW5kIE1JVC4gWW91IGRvbid0IGhhdmUgdG8gZG8gYW55dGhpbmcgc3BlY2lhbCB0b1xyXG4gKiBjaG9vc2Ugb25lIGxpY2Vuc2Ugb3IgdGhlIG90aGVyIGFuZCB5b3UgZG9uJ3QgaGF2ZSB0byBub3RpZnkgYW55b25lIHdoaWNoIGxpY2Vuc2UgeW91IGFyZSB1c2luZy5cclxuICogWW91IGFyZSBmcmVlIHRvIHVzZSBVbml2ZXJzYWxMYW5ndWFnZVNlbGVjdG9yIGluIGNvbW1lcmNpYWwgcHJvamVjdHMgYXMgbG9uZyBhcyB0aGUgY29weXJpZ2h0XHJcbiAqIGhlYWRlciBpcyBsZWZ0IGludGFjdC4gU2VlIGZpbGVzIEdQTC1MSUNFTlNFIGFuZCBNSVQtTElDRU5TRSBmb3IgZGV0YWlscy5cclxuICpcclxuICogQGxpY2VuY2UgR05VIEdlbmVyYWwgUHVibGljIExpY2VuY2UgMi4wIG9yIGxhdGVyXHJcbiAqIEBsaWNlbmNlIE1JVCBMaWNlbnNlXHJcbiAqL1xyXG4oIGZ1bmN0aW9uICggJCApIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblxyXG5cdCQuaTE4biA9ICQuaTE4biB8fCB7fTtcclxuXHQkLmV4dGVuZCggJC5pMThuLmZhbGxiYWNrcywge1xyXG5cdFx0YWI6IFsgJ3J1JyBdLFxyXG5cdFx0YWNlOiBbICdpZCcgXSxcclxuXHRcdGFsbjogWyAnc3EnIF0sXHJcblx0XHQvLyBOb3Qgc28gc3RhbmRhcmQgLSBhbHMgaXMgc3VwcG9zZWQgdG8gYmUgVG9zayBBbGJhbmlhbixcclxuXHRcdC8vIGJ1dCBpbiBXaWtpcGVkaWEgaXQncyB1c2VkIGZvciBhIEdlcm1hbmljIGxhbmd1YWdlLlxyXG5cdFx0YWxzOiBbICdnc3cnLCAnZGUnIF0sXHJcblx0XHRhbjogWyAnZXMnIF0sXHJcblx0XHRhbnA6IFsgJ2hpJyBdLFxyXG5cdFx0YXJuOiBbICdlcycgXSxcclxuXHRcdGFyejogWyAnYXInIF0sXHJcblx0XHRhdjogWyAncnUnIF0sXHJcblx0XHRheTogWyAnZXMnIF0sXHJcblx0XHRiYTogWyAncnUnIF0sXHJcblx0XHRiYXI6IFsgJ2RlJyBdLFxyXG5cdFx0J2JhdC1zbWcnOiBbICdzZ3MnLCAnbHQnIF0sXHJcblx0XHRiY2M6IFsgJ2ZhJyBdLFxyXG5cdFx0J2JlLXgtb2xkJzogWyAnYmUtdGFyYXNrJyBdLFxyXG5cdFx0Ymg6IFsgJ2JobycgXSxcclxuXHRcdGJqbjogWyAnaWQnIF0sXHJcblx0XHRibTogWyAnZnInIF0sXHJcblx0XHRicHk6IFsgJ2JuJyBdLFxyXG5cdFx0YnFpOiBbICdmYScgXSxcclxuXHRcdGJ1ZzogWyAnaWQnIF0sXHJcblx0XHQnY2JrLXphbSc6IFsgJ2VzJyBdLFxyXG5cdFx0Y2U6IFsgJ3J1JyBdLFxyXG5cdFx0Y3JoOiBbICdjcmgtbGF0bicgXSxcclxuXHRcdCdjcmgtY3lybCc6IFsgJ3J1JyBdLFxyXG5cdFx0Y3NiOiBbICdwbCcgXSxcclxuXHRcdGN2OiBbICdydScgXSxcclxuXHRcdCdkZS1hdCc6IFsgJ2RlJyBdLFxyXG5cdFx0J2RlLWNoJzogWyAnZGUnIF0sXHJcblx0XHQnZGUtZm9ybWFsJzogWyAnZGUnIF0sXHJcblx0XHRkc2I6IFsgJ2RlJyBdLFxyXG5cdFx0ZHRwOiBbICdtcycgXSxcclxuXHRcdGVnbDogWyAnaXQnIF0sXHJcblx0XHRlbWw6IFsgJ2l0JyBdLFxyXG5cdFx0ZmY6IFsgJ2ZyJyBdLFxyXG5cdFx0Zml0OiBbICdmaScgXSxcclxuXHRcdCdmaXUtdnJvJzogWyAndnJvJywgJ2V0JyBdLFxyXG5cdFx0ZnJjOiBbICdmcicgXSxcclxuXHRcdGZycDogWyAnZnInIF0sXHJcblx0XHRmcnI6IFsgJ2RlJyBdLFxyXG5cdFx0ZnVyOiBbICdpdCcgXSxcclxuXHRcdGdhZzogWyAndHInIF0sXHJcblx0XHRnYW46IFsgJ2dhbi1oYW50JywgJ3poLWhhbnQnLCAnemgtaGFucycgXSxcclxuXHRcdCdnYW4taGFucyc6IFsgJ3poLWhhbnMnIF0sXHJcblx0XHQnZ2FuLWhhbnQnOiBbICd6aC1oYW50JywgJ3poLWhhbnMnIF0sXHJcblx0XHRnbDogWyAncHQnIF0sXHJcblx0XHRnbGs6IFsgJ2ZhJyBdLFxyXG5cdFx0Z246IFsgJ2VzJyBdLFxyXG5cdFx0Z3N3OiBbICdkZScgXSxcclxuXHRcdGhpZjogWyAnaGlmLWxhdG4nIF0sXHJcblx0XHRoc2I6IFsgJ2RlJyBdLFxyXG5cdFx0aHQ6IFsgJ2ZyJyBdLFxyXG5cdFx0aWk6IFsgJ3poLWNuJywgJ3poLWhhbnMnIF0sXHJcblx0XHRpbmg6IFsgJ3J1JyBdLFxyXG5cdFx0aXU6IFsgJ2lrZS1jYW5zJyBdLFxyXG5cdFx0anV0OiBbICdkYScgXSxcclxuXHRcdGp2OiBbICdpZCcgXSxcclxuXHRcdGthYTogWyAna2stbGF0bicsICdray1jeXJsJyBdLFxyXG5cdFx0a2JkOiBbICdrYmQtY3lybCcgXSxcclxuXHRcdGtodzogWyAndXInIF0sXHJcblx0XHRraXU6IFsgJ3RyJyBdLFxyXG5cdFx0a2s6IFsgJ2trLWN5cmwnIF0sXHJcblx0XHQna2stYXJhYic6IFsgJ2trLWN5cmwnIF0sXHJcblx0XHQna2stbGF0bic6IFsgJ2trLWN5cmwnIF0sXHJcblx0XHQna2stY24nOiBbICdray1hcmFiJywgJ2trLWN5cmwnIF0sXHJcblx0XHQna2sta3onOiBbICdray1jeXJsJyBdLFxyXG5cdFx0J2trLXRyJzogWyAna2stbGF0bicsICdray1jeXJsJyBdLFxyXG5cdFx0a2w6IFsgJ2RhJyBdLFxyXG5cdFx0J2tvLWtwJzogWyAna28nIF0sXHJcblx0XHRrb2k6IFsgJ3J1JyBdLFxyXG5cdFx0a3JjOiBbICdydScgXSxcclxuXHRcdGtzOiBbICdrcy1hcmFiJyBdLFxyXG5cdFx0a3NoOiBbICdkZScgXSxcclxuXHRcdGt1OiBbICdrdS1sYXRuJyBdLFxyXG5cdFx0J2t1LWFyYWInOiBbICdja2InIF0sXHJcblx0XHRrdjogWyAncnUnIF0sXHJcblx0XHRsYWQ6IFsgJ2VzJyBdLFxyXG5cdFx0bGI6IFsgJ2RlJyBdLFxyXG5cdFx0bGJlOiBbICdydScgXSxcclxuXHRcdGxlejogWyAncnUnIF0sXHJcblx0XHRsaTogWyAnbmwnIF0sXHJcblx0XHRsaWo6IFsgJ2l0JyBdLFxyXG5cdFx0bGl2OiBbICdldCcgXSxcclxuXHRcdGxtbzogWyAnaXQnIF0sXHJcblx0XHRsbjogWyAnZnInIF0sXHJcblx0XHRsdGc6IFsgJ2x2JyBdLFxyXG5cdFx0bHp6OiBbICd0cicgXSxcclxuXHRcdG1haTogWyAnaGknIF0sXHJcblx0XHQnbWFwLWJtcyc6IFsgJ2p2JywgJ2lkJyBdLFxyXG5cdFx0bWc6IFsgJ2ZyJyBdLFxyXG5cdFx0bWhyOiBbICdydScgXSxcclxuXHRcdG1pbjogWyAnaWQnIF0sXHJcblx0XHRtbzogWyAncm8nIF0sXHJcblx0XHRtcmo6IFsgJ3J1JyBdLFxyXG5cdFx0bXdsOiBbICdwdCcgXSxcclxuXHRcdG15djogWyAncnUnIF0sXHJcblx0XHRtem46IFsgJ2ZhJyBdLFxyXG5cdFx0bmFoOiBbICdlcycgXSxcclxuXHRcdG5hcDogWyAnaXQnIF0sXHJcblx0XHRuZHM6IFsgJ2RlJyBdLFxyXG5cdFx0J25kcy1ubCc6IFsgJ25sJyBdLFxyXG5cdFx0J25sLWluZm9ybWFsJzogWyAnbmwnIF0sXHJcblx0XHRubzogWyAnbmInIF0sXHJcblx0XHRvczogWyAncnUnIF0sXHJcblx0XHRwY2Q6IFsgJ2ZyJyBdLFxyXG5cdFx0cGRjOiBbICdkZScgXSxcclxuXHRcdHBkdDogWyAnZGUnIF0sXHJcblx0XHRwZmw6IFsgJ2RlJyBdLFxyXG5cdFx0cG1zOiBbICdpdCcgXSxcclxuXHRcdHB0OiBbICdwdC1icicgXSxcclxuXHRcdCdwdC1icic6IFsgJ3B0JyBdLFxyXG5cdFx0cXU6IFsgJ2VzJyBdLFxyXG5cdFx0cXVnOiBbICdxdScsICdlcycgXSxcclxuXHRcdHJnbjogWyAnaXQnIF0sXHJcblx0XHRybXk6IFsgJ3JvJyBdLFxyXG5cdFx0J3JvYS1ydXAnOiBbICdydXAnIF0sXHJcblx0XHRydWU6IFsgJ3VrJywgJ3J1JyBdLFxyXG5cdFx0cnVxOiBbICdydXEtbGF0bicsICdybycgXSxcclxuXHRcdCdydXEtY3lybCc6IFsgJ21rJyBdLFxyXG5cdFx0J3J1cS1sYXRuJzogWyAncm8nIF0sXHJcblx0XHRzYTogWyAnaGknIF0sXHJcblx0XHRzYWg6IFsgJ3J1JyBdLFxyXG5cdFx0c2NuOiBbICdpdCcgXSxcclxuXHRcdHNnOiBbICdmcicgXSxcclxuXHRcdHNnczogWyAnbHQnIF0sXHJcblx0XHRzbGk6IFsgJ2RlJyBdLFxyXG5cdFx0c3I6IFsgJ3NyLWVjJyBdLFxyXG5cdFx0c3JuOiBbICdubCcgXSxcclxuXHRcdHN0cTogWyAnZGUnIF0sXHJcblx0XHRzdTogWyAnaWQnIF0sXHJcblx0XHRzemw6IFsgJ3BsJyBdLFxyXG5cdFx0dGN5OiBbICdrbicgXSxcclxuXHRcdHRnOiBbICd0Zy1jeXJsJyBdLFxyXG5cdFx0dHQ6IFsgJ3R0LWN5cmwnLCAncnUnIF0sXHJcblx0XHQndHQtY3lybCc6IFsgJ3J1JyBdLFxyXG5cdFx0dHk6IFsgJ2ZyJyBdLFxyXG5cdFx0dWRtOiBbICdydScgXSxcclxuXHRcdHVnOiBbICd1Zy1hcmFiJyBdLFxyXG5cdFx0dWs6IFsgJ3J1JyBdLFxyXG5cdFx0dmVjOiBbICdpdCcgXSxcclxuXHRcdHZlcDogWyAnZXQnIF0sXHJcblx0XHR2bHM6IFsgJ25sJyBdLFxyXG5cdFx0dm1mOiBbICdkZScgXSxcclxuXHRcdHZvdDogWyAnZmknIF0sXHJcblx0XHR2cm86IFsgJ2V0JyBdLFxyXG5cdFx0d2E6IFsgJ2ZyJyBdLFxyXG5cdFx0d286IFsgJ2ZyJyBdLFxyXG5cdFx0d3V1OiBbICd6aC1oYW5zJyBdLFxyXG5cdFx0eGFsOiBbICdydScgXSxcclxuXHRcdHhtZjogWyAna2EnIF0sXHJcblx0XHR5aTogWyAnaGUnIF0sXHJcblx0XHR6YTogWyAnemgtaGFucycgXSxcclxuXHRcdHplYTogWyAnbmwnIF0sXHJcblx0XHR6aDogWyAnemgtaGFucycgXSxcclxuXHRcdCd6aC1jbGFzc2ljYWwnOiBbICdsemgnIF0sXHJcblx0XHQnemgtY24nOiBbICd6aC1oYW5zJyBdLFxyXG5cdFx0J3poLWhhbnQnOiBbICd6aC1oYW5zJyBdLFxyXG5cdFx0J3poLWhrJzogWyAnemgtaGFudCcsICd6aC1oYW5zJyBdLFxyXG5cdFx0J3poLW1pbi1uYW4nOiBbICduYW4nIF0sXHJcblx0XHQnemgtbW8nOiBbICd6aC1oaycsICd6aC1oYW50JywgJ3poLWhhbnMnIF0sXHJcblx0XHQnemgtbXknOiBbICd6aC1zZycsICd6aC1oYW5zJyBdLFxyXG5cdFx0J3poLXNnJzogWyAnemgtaGFucycgXSxcclxuXHRcdCd6aC10dyc6IFsgJ3poLWhhbnQnLCAnemgtaGFucycgXSxcclxuXHRcdCd6aC15dWUnOiBbICd5dWUnIF1cclxuXHR9ICk7XHJcbn0oIGpRdWVyeSApICk7XHJcbiIsIi8qIVxyXG4gKiBqUXVlcnkgSW50ZXJuYXRpb25hbGl6YXRpb24gbGlicmFyeVxyXG4gKlxyXG4gKiBDb3B5cmlnaHQgKEMpIDIwMTIgU2FudGhvc2ggVGhvdHRpbmdhbFxyXG4gKlxyXG4gKiBqcXVlcnkuaTE4biBpcyBkdWFsIGxpY2Vuc2VkIEdQTHYyIG9yIGxhdGVyIGFuZCBNSVQuIFlvdSBkb24ndCBoYXZlIHRvIGRvXHJcbiAqIGFueXRoaW5nIHNwZWNpYWwgdG8gY2hvb3NlIG9uZSBsaWNlbnNlIG9yIHRoZSBvdGhlciBhbmQgeW91IGRvbid0IGhhdmUgdG9cclxuICogbm90aWZ5IGFueW9uZSB3aGljaCBsaWNlbnNlIHlvdSBhcmUgdXNpbmcuIFlvdSBhcmUgZnJlZSB0byB1c2VcclxuICogVW5pdmVyc2FsTGFuZ3VhZ2VTZWxlY3RvciBpbiBjb21tZXJjaWFsIHByb2plY3RzIGFzIGxvbmcgYXMgdGhlIGNvcHlyaWdodFxyXG4gKiBoZWFkZXIgaXMgbGVmdCBpbnRhY3QuIFNlZSBmaWxlcyBHUEwtTElDRU5TRSBhbmQgTUlULUxJQ0VOU0UgZm9yIGRldGFpbHMuXHJcbiAqXHJcbiAqIEBsaWNlbmNlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbmNlIDIuMCBvciBsYXRlclxyXG4gKiBAbGljZW5jZSBNSVQgTGljZW5zZVxyXG4gKi9cclxuXHJcbiggZnVuY3Rpb24gKCAkICkge1xyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHJcblx0dmFyIG5hdiwgSTE4TixcclxuXHRcdHNsaWNlID0gQXJyYXkucHJvdG90eXBlLnNsaWNlO1xyXG5cdC8qKlxyXG5cdCAqIEBjb25zdHJ1Y3RvclxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXHJcblx0ICovXHJcblx0STE4TiA9IGZ1bmN0aW9uICggb3B0aW9ucyApIHtcclxuXHRcdC8vIExvYWQgZGVmYXVsdHNcclxuXHRcdHRoaXMub3B0aW9ucyA9ICQuZXh0ZW5kKCB7fSwgSTE4Ti5kZWZhdWx0cywgb3B0aW9ucyApO1xyXG5cclxuXHRcdHRoaXMucGFyc2VyID0gdGhpcy5vcHRpb25zLnBhcnNlcjtcclxuXHRcdHRoaXMubG9jYWxlID0gdGhpcy5vcHRpb25zLmxvY2FsZTtcclxuXHRcdHRoaXMubWVzc2FnZVN0b3JlID0gdGhpcy5vcHRpb25zLm1lc3NhZ2VTdG9yZTtcclxuXHRcdHRoaXMubGFuZ3VhZ2VzID0ge307XHJcblxyXG5cdFx0dGhpcy5pbml0KCk7XHJcblx0fTtcclxuXHJcblx0STE4Ti5wcm90b3R5cGUgPSB7XHJcblx0XHQvKipcclxuXHRcdCAqIEluaXRpYWxpemUgYnkgbG9hZGluZyBsb2NhbGVzIGFuZCBzZXR0aW5nIHVwXHJcblx0XHQgKiBTdHJpbmcucHJvdG90eXBlLnRvTG9jYWxlU3RyaW5nIGFuZCBTdHJpbmcubG9jYWxlLlxyXG5cdFx0ICovXHJcblx0XHRpbml0OiBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdHZhciBpMThuID0gdGhpcztcclxuXHJcblx0XHRcdC8vIFNldCBsb2NhbGUgb2YgU3RyaW5nIGVudmlyb25tZW50XHJcblx0XHRcdFN0cmluZy5sb2NhbGUgPSBpMThuLmxvY2FsZTtcclxuXHJcblx0XHRcdC8vIE92ZXJyaWRlIFN0cmluZy5sb2NhbGVTdHJpbmcgbWV0aG9kXHJcblx0XHRcdFN0cmluZy5wcm90b3R5cGUudG9Mb2NhbGVTdHJpbmcgPSBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0dmFyIGxvY2FsZVBhcnRzLCBsb2NhbGVQYXJ0SW5kZXgsIHZhbHVlLCBsb2NhbGUsIGZhbGxiYWNrSW5kZXgsXHJcblx0XHRcdFx0XHR0cnlpbmdMb2NhbGUsIG1lc3NhZ2U7XHJcblxyXG5cdFx0XHRcdHZhbHVlID0gdGhpcy52YWx1ZU9mKCk7XHJcblx0XHRcdFx0bG9jYWxlID0gaTE4bi5sb2NhbGU7XHJcblx0XHRcdFx0ZmFsbGJhY2tJbmRleCA9IDA7XHJcblxyXG5cdFx0XHRcdHdoaWxlICggbG9jYWxlICkge1xyXG5cdFx0XHRcdFx0Ly8gSXRlcmF0ZSB0aHJvdWdoIGxvY2FsZXMgc3RhcnRpbmcgYXQgbW9zdC1zcGVjaWZpYyB1bnRpbFxyXG5cdFx0XHRcdFx0Ly8gbG9jYWxpemF0aW9uIGlzIGZvdW5kLiBBcyBpbiBmaS1MYXRuLUZJLCBmaS1MYXRuIGFuZCBmaS5cclxuXHRcdFx0XHRcdGxvY2FsZVBhcnRzID0gbG9jYWxlLnNwbGl0KCAnLScgKTtcclxuXHRcdFx0XHRcdGxvY2FsZVBhcnRJbmRleCA9IGxvY2FsZVBhcnRzLmxlbmd0aDtcclxuXHJcblx0XHRcdFx0XHRkbyB7XHJcblx0XHRcdFx0XHRcdHRyeWluZ0xvY2FsZSA9IGxvY2FsZVBhcnRzLnNsaWNlKCAwLCBsb2NhbGVQYXJ0SW5kZXggKS5qb2luKCAnLScgKTtcclxuXHRcdFx0XHRcdFx0bWVzc2FnZSA9IGkxOG4ubWVzc2FnZVN0b3JlLmdldCggdHJ5aW5nTG9jYWxlLCB2YWx1ZSApO1xyXG5cclxuXHRcdFx0XHRcdFx0aWYgKCBtZXNzYWdlICkge1xyXG5cdFx0XHRcdFx0XHRcdHJldHVybiBtZXNzYWdlO1xyXG5cdFx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0XHRsb2NhbGVQYXJ0SW5kZXgtLTtcclxuXHRcdFx0XHRcdH0gd2hpbGUgKCBsb2NhbGVQYXJ0SW5kZXggKTtcclxuXHJcblx0XHRcdFx0XHRpZiAoIGxvY2FsZSA9PT0gJ2VuJyApIHtcclxuXHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0bG9jYWxlID0gKCAkLmkxOG4uZmFsbGJhY2tzWyBpMThuLmxvY2FsZSBdICYmICQuaTE4bi5mYWxsYmFja3NbIGkxOG4ubG9jYWxlIF1bIGZhbGxiYWNrSW5kZXggXSApIHx8XHJcblx0XHRcdFx0XHRcdGkxOG4ub3B0aW9ucy5mYWxsYmFja0xvY2FsZTtcclxuXHRcdFx0XHRcdCQuaTE4bi5sb2coICdUcnlpbmcgZmFsbGJhY2sgbG9jYWxlIGZvciAnICsgaTE4bi5sb2NhbGUgKyAnOiAnICsgbG9jYWxlICsgJyAoJyArIHZhbHVlICsgJyknICk7XHJcblxyXG5cdFx0XHRcdFx0ZmFsbGJhY2tJbmRleCsrO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0Ly8ga2V5IG5vdCBmb3VuZFxyXG5cdFx0XHRcdHJldHVybiAnJztcclxuXHRcdFx0fTtcclxuXHRcdH0sXHJcblxyXG5cdFx0LypcclxuXHRcdCAqIERlc3Ryb3kgdGhlIGkxOG4gaW5zdGFuY2UuXHJcblx0XHQgKi9cclxuXHRcdGRlc3Ryb3k6IGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0JC5yZW1vdmVEYXRhKCBkb2N1bWVudCwgJ2kxOG4nICk7XHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogR2VuZXJhbCBtZXNzYWdlIGxvYWRpbmcgQVBJIFRoaXMgY2FuIHRha2UgYSBVUkwgc3RyaW5nIGZvclxyXG5cdFx0ICogdGhlIGpzb24gZm9ybWF0dGVkIG1lc3NhZ2VzLiBFeGFtcGxlOlxyXG5cdFx0ICogPGNvZGU+bG9hZCgncGF0aC90by9hbGxfbG9jYWxpemF0aW9ucy5qc29uJyk7PC9jb2RlPlxyXG5cdFx0ICpcclxuXHRcdCAqIFRvIGxvYWQgYSBsb2NhbGl6YXRpb24gZmlsZSBmb3IgYSBsb2NhbGU6XHJcblx0XHQgKiA8Y29kZT5cclxuXHRcdCAqIGxvYWQoJ3BhdGgvdG8vZGUtbWVzc2FnZXMuanNvbicsICdkZScgKTtcclxuXHRcdCAqIDwvY29kZT5cclxuXHRcdCAqXHJcblx0XHQgKiBUbyBsb2FkIGEgbG9jYWxpemF0aW9uIGZpbGUgZnJvbSBhIGRpcmVjdG9yeTpcclxuXHRcdCAqIDxjb2RlPlxyXG5cdFx0ICogbG9hZCgncGF0aC90by9pMThuL2RpcmVjdG9yeScsICdkZScgKTtcclxuXHRcdCAqIDwvY29kZT5cclxuXHRcdCAqIFRoZSBhYm92ZSBtZXRob2QgaGFzIHRoZSBhZHZhbnRhZ2Ugb2YgZmFsbGJhY2sgcmVzb2x1dGlvbi5cclxuXHRcdCAqIGllLCBpdCB3aWxsIGF1dG9tYXRpY2FsbHkgbG9hZCB0aGUgZmFsbGJhY2sgbG9jYWxlcyBmb3IgZGUuXHJcblx0XHQgKiBGb3IgbW9zdCB1c2VjYXNlcywgdGhpcyBpcyB0aGUgcmVjb21tZW5kZWQgbWV0aG9kLlxyXG5cdFx0ICogSXQgaXMgb3B0aW9uYWwgdG8gaGF2ZSB0cmFpbGluZyBzbGFzaCBhdCBlbmQuXHJcblx0XHQgKlxyXG5cdFx0ICogQSBkYXRhIG9iamVjdCBjb250YWluaW5nIG1lc3NhZ2Uga2V5LSBtZXNzYWdlIHRyYW5zbGF0aW9uIG1hcHBpbmdzXHJcblx0XHQgKiBjYW4gYWxzbyBiZSBwYXNzZWQuIEV4YW1wbGU6XHJcblx0XHQgKiA8Y29kZT5cclxuXHRcdCAqIGxvYWQoIHsgJ2hlbGxvJyA6ICdIZWxsbycgfSwgb3B0aW9uYWxMb2NhbGUgKTtcclxuXHRcdCAqIDwvY29kZT5cclxuXHRcdCAqXHJcblx0XHQgKiBBIHNvdXJjZSBtYXAgY29udGFpbmluZyBrZXktdmFsdWUgcGFpciBvZiBsYW5ndWFnZW5hbWUgYW5kIGxvY2F0aW9uc1xyXG5cdFx0ICogY2FuIGFsc28gYmUgcGFzc2VkLiBFeGFtcGxlOlxyXG5cdFx0ICogPGNvZGU+XHJcblx0XHQgKiBsb2FkKCB7XHJcblx0XHQgKiBibjogJ2kxOG4vYm4uanNvbicsXHJcblx0XHQgKiBoZTogJ2kxOG4vaGUuanNvbicsXHJcblx0XHQgKiBlbjogJ2kxOG4vZW4uanNvbidcclxuXHRcdCAqIH0gKVxyXG5cdFx0ICogPC9jb2RlPlxyXG5cdFx0ICpcclxuXHRcdCAqIElmIHRoZSBkYXRhIGFyZ3VtZW50IGlzIG51bGwvdW5kZWZpbmVkL2ZhbHNlLFxyXG5cdFx0ICogYWxsIGNhY2hlZCBtZXNzYWdlcyBmb3IgdGhlIGkxOG4gaW5zdGFuY2Ugd2lsbCBnZXQgcmVzZXQuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd8T2JqZWN0fSBzb3VyY2VcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBsb2NhbGUgTGFuZ3VhZ2UgdGFnXHJcblx0XHQgKiBAcmV0dXJuIHtqUXVlcnkuUHJvbWlzZX1cclxuXHRcdCAqL1xyXG5cdFx0bG9hZDogZnVuY3Rpb24gKCBzb3VyY2UsIGxvY2FsZSApIHtcclxuXHRcdFx0dmFyIGZhbGxiYWNrTG9jYWxlcywgbG9jSW5kZXgsIGZhbGxiYWNrTG9jYWxlLCBzb3VyY2VNYXAgPSB7fTtcclxuXHRcdFx0aWYgKCAhc291cmNlICYmICFsb2NhbGUgKSB7XHJcblx0XHRcdFx0c291cmNlID0gJ2kxOG4vJyArICQuaTE4bigpLmxvY2FsZSArICcuanNvbic7XHJcblx0XHRcdFx0bG9jYWxlID0gJC5pMThuKCkubG9jYWxlO1xyXG5cdFx0XHR9XHJcblx0XHRcdGlmICggdHlwZW9mIHNvdXJjZSA9PT0gJ3N0cmluZycgJiZcclxuXHRcdFx0XHQvLyBzb3VyY2UgZXh0ZW5zaW9uIHNob3VsZCBiZSBqc29uLCBidXQgY2FuIGhhdmUgcXVlcnkgcGFyYW1zIGFmdGVyIHRoYXQuXHJcblx0XHRcdFx0c291cmNlLnNwbGl0KCAnPycgKVsgMCBdLnNwbGl0KCAnLicgKS5wb3AoKSAhPT0gJ2pzb24nXHJcblx0XHRcdCkge1xyXG5cdFx0XHRcdC8vIExvYWQgc3BlY2lmaWVkIGxvY2FsZSB0aGVuIGNoZWNrIGZvciBmYWxsYmFja3Mgd2hlbiBkaXJlY3RvcnkgaXMgc3BlY2lmaWVkIGluIGxvYWQoKVxyXG5cdFx0XHRcdHNvdXJjZU1hcFsgbG9jYWxlIF0gPSBzb3VyY2UgKyAnLycgKyBsb2NhbGUgKyAnLmpzb24nO1xyXG5cdFx0XHRcdGZhbGxiYWNrTG9jYWxlcyA9ICggJC5pMThuLmZhbGxiYWNrc1sgbG9jYWxlIF0gfHwgW10gKVxyXG5cdFx0XHRcdFx0LmNvbmNhdCggdGhpcy5vcHRpb25zLmZhbGxiYWNrTG9jYWxlICk7XHJcblx0XHRcdFx0Zm9yICggbG9jSW5kZXggPSAwOyBsb2NJbmRleCA8IGZhbGxiYWNrTG9jYWxlcy5sZW5ndGg7IGxvY0luZGV4KysgKSB7XHJcblx0XHRcdFx0XHRmYWxsYmFja0xvY2FsZSA9IGZhbGxiYWNrTG9jYWxlc1sgbG9jSW5kZXggXTtcclxuXHRcdFx0XHRcdHNvdXJjZU1hcFsgZmFsbGJhY2tMb2NhbGUgXSA9IHNvdXJjZSArICcvJyArIGZhbGxiYWNrTG9jYWxlICsgJy5qc29uJztcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuIHRoaXMubG9hZCggc291cmNlTWFwICk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0cmV0dXJuIHRoaXMubWVzc2FnZVN0b3JlLmxvYWQoIHNvdXJjZSwgbG9jYWxlICk7XHJcblx0XHRcdH1cclxuXHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogRG9lcyBwYXJhbWV0ZXIgYW5kIG1hZ2ljIHdvcmQgc3Vic3RpdHV0aW9uLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgTWVzc2FnZSBrZXlcclxuXHRcdCAqIEBwYXJhbSB7QXJyYXl9IHBhcmFtZXRlcnMgTWVzc2FnZSBwYXJhbWV0ZXJzXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9XHJcblx0XHQgKi9cclxuXHRcdHBhcnNlOiBmdW5jdGlvbiAoIGtleSwgcGFyYW1ldGVycyApIHtcclxuXHRcdFx0dmFyIG1lc3NhZ2UgPSBrZXkudG9Mb2NhbGVTdHJpbmcoKTtcclxuXHRcdFx0Ly8gRklYTUU6IFRoaXMgY2hhbmdlcyB0aGUgc3RhdGUgb2YgdGhlIEkxOE4gb2JqZWN0LFxyXG5cdFx0XHQvLyBzaG91bGQgcHJvYmFibHkgbm90IGNoYW5nZSB0aGUgJ3RoaXMucGFyc2VyJyBidXQganVzdFxyXG5cdFx0XHQvLyBwYXNzIGl0IHRvIHRoZSBwYXJzZXIuXHJcblx0XHRcdHRoaXMucGFyc2VyLmxhbmd1YWdlID0gJC5pMThuLmxhbmd1YWdlc1sgJC5pMThuKCkubG9jYWxlIF0gfHwgJC5pMThuLmxhbmd1YWdlc1sgJ2RlZmF1bHQnIF07XHJcblx0XHRcdGlmICggbWVzc2FnZSA9PT0gJycgKSB7XHJcblx0XHRcdFx0bWVzc2FnZSA9IGtleTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdGhpcy5wYXJzZXIucGFyc2UoIG1lc3NhZ2UsIHBhcmFtZXRlcnMgKTtcclxuXHRcdH1cclxuXHR9O1xyXG5cclxuXHQvKipcclxuXHQgKiBQcm9jZXNzIGEgbWVzc2FnZSBmcm9tIHRoZSAkLkkxOE4gaW5zdGFuY2VcclxuXHQgKiBmb3IgdGhlIGN1cnJlbnQgZG9jdW1lbnQsIHN0b3JlZCBpbiBqUXVlcnkuZGF0YShkb2N1bWVudCkuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge3N0cmluZ30ga2V5IEtleSBvZiB0aGUgbWVzc2FnZS5cclxuXHQgKiBAcGFyYW0ge3N0cmluZ30gcGFyYW0xIFtwYXJhbS4uLl0gVmFyaWFkaWMgbGlzdCBvZiBwYXJhbWV0ZXJzIGZvciB7a2V5fS5cclxuXHQgKiBAcmV0dXJuIHtzdHJpbmd8JC5JMThOfSBQYXJzZWQgbWVzc2FnZSwgb3IgaWYgbm8ga2V5IHdhcyBnaXZlblxyXG5cdCAqIHRoZSBpbnN0YW5jZSBvZiAkLkkxOE4gaXMgcmV0dXJuZWQuXHJcblx0ICovXHJcblx0JC5pMThuID0gZnVuY3Rpb24gKCBrZXksIHBhcmFtMSApIHtcclxuXHRcdHZhciBwYXJhbWV0ZXJzLFxyXG5cdFx0XHRpMThuID0gJC5kYXRhKCBkb2N1bWVudCwgJ2kxOG4nICksXHJcblx0XHRcdG9wdGlvbnMgPSB0eXBlb2Yga2V5ID09PSAnb2JqZWN0JyAmJiBrZXk7XHJcblxyXG5cdFx0Ly8gSWYgdGhlIGxvY2FsZSBvcHRpb24gZm9yIHRoaXMgY2FsbCBpcyBkaWZmZXJlbnQgdGhlbiB0aGUgc2V0dXAgc28gZmFyLFxyXG5cdFx0Ly8gdXBkYXRlIGl0IGF1dG9tYXRpY2FsbHkuIFRoaXMgZG9lc24ndCBqdXN0IGNoYW5nZSB0aGUgY29udGV4dCBmb3IgdGhpc1xyXG5cdFx0Ly8gY2FsbCBidXQgZm9yIGFsbCBmdXR1cmUgY2FsbCBhcyB3ZWxsLlxyXG5cdFx0Ly8gSWYgdGhlcmUgaXMgbm8gaTE4biBzZXR1cCB5ZXQsIGRvbid0IGRvIHRoaXMuIEl0IHdpbGwgYmUgdGFrZW4gY2FyZSBvZlxyXG5cdFx0Ly8gYnkgdGhlIGBuZXcgSTE4TmAgY29uc3RydWN0aW9uIGJlbG93LlxyXG5cdFx0Ly8gTk9URTogSXQgc2hvdWxkIG9ubHkgY2hhbmdlIGxhbmd1YWdlIGZvciB0aGlzIG9uZSBjYWxsLlxyXG5cdFx0Ly8gVGhlbiBjYWNoZSBpbnN0YW5jZXMgb2YgSTE4TiBzb21ld2hlcmUuXHJcblx0XHRpZiAoIG9wdGlvbnMgJiYgb3B0aW9ucy5sb2NhbGUgJiYgaTE4biAmJiBpMThuLmxvY2FsZSAhPT0gb3B0aW9ucy5sb2NhbGUgKSB7XHJcblx0XHRcdFN0cmluZy5sb2NhbGUgPSBpMThuLmxvY2FsZSA9IG9wdGlvbnMubG9jYWxlO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmICggIWkxOG4gKSB7XHJcblx0XHRcdGkxOG4gPSBuZXcgSTE4Tiggb3B0aW9ucyApO1xyXG5cdFx0XHQkLmRhdGEoIGRvY3VtZW50LCAnaTE4bicsIGkxOG4gKTtcclxuXHRcdH1cclxuXHJcblx0XHRpZiAoIHR5cGVvZiBrZXkgPT09ICdzdHJpbmcnICkge1xyXG5cdFx0XHRpZiAoIHBhcmFtMSAhPT0gdW5kZWZpbmVkICkge1xyXG5cdFx0XHRcdHBhcmFtZXRlcnMgPSBzbGljZS5jYWxsKCBhcmd1bWVudHMsIDEgKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRwYXJhbWV0ZXJzID0gW107XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJldHVybiBpMThuLnBhcnNlKCBrZXksIHBhcmFtZXRlcnMgKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdC8vIEZJWE1FOiByZW1vdmUgdGhpcyBmZWF0dXJlL2J1Zy5cclxuXHRcdFx0cmV0dXJuIGkxOG47XHJcblx0XHR9XHJcblx0fTtcclxuXHJcblx0JC5mbi5pMThuID0gZnVuY3Rpb24gKCkge1xyXG5cdFx0dmFyIGkxOG4gPSAkLmRhdGEoIGRvY3VtZW50LCAnaTE4bicgKTtcclxuXHJcblx0XHRpZiAoICFpMThuICkge1xyXG5cdFx0XHRpMThuID0gbmV3IEkxOE4oKTtcclxuXHRcdFx0JC5kYXRhKCBkb2N1bWVudCwgJ2kxOG4nLCBpMThuICk7XHJcblx0XHR9XHJcblx0XHRTdHJpbmcubG9jYWxlID0gaTE4bi5sb2NhbGU7XHJcblx0XHRyZXR1cm4gdGhpcy5lYWNoKCBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdHZhciAkdGhpcyA9ICQoIHRoaXMgKSxcclxuXHRcdFx0XHRtZXNzYWdlS2V5ID0gJHRoaXMuZGF0YSggJ2kxOG4nICksXHJcblx0XHRcdFx0bEJyYWNrZXQsIHJCcmFja2V0LCB0eXBlLCBrZXk7XHJcblxyXG5cdFx0XHRpZiAoIG1lc3NhZ2VLZXkgKSB7XHJcblx0XHRcdFx0bEJyYWNrZXQgPSBtZXNzYWdlS2V5LmluZGV4T2YoICdbJyApO1xyXG5cdFx0XHRcdHJCcmFja2V0ID0gbWVzc2FnZUtleS5pbmRleE9mKCAnXScgKTtcclxuXHRcdFx0XHRpZiAoIGxCcmFja2V0ICE9PSAtMSAmJiByQnJhY2tldCAhPT0gLTEgJiYgbEJyYWNrZXQgPCByQnJhY2tldCApIHtcclxuXHRcdFx0XHRcdHR5cGUgPSBtZXNzYWdlS2V5LnNsaWNlKCBsQnJhY2tldCArIDEsIHJCcmFja2V0ICk7XHJcblx0XHRcdFx0XHRrZXkgPSBtZXNzYWdlS2V5LnNsaWNlKCByQnJhY2tldCArIDEgKTtcclxuXHRcdFx0XHRcdGlmICggdHlwZSA9PT0gJ2h0bWwnICkge1xyXG5cdFx0XHRcdFx0XHQkdGhpcy5odG1sKCBpMThuLnBhcnNlKCBrZXkgKSApO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0JHRoaXMuYXR0ciggdHlwZSwgaTE4bi5wYXJzZSgga2V5ICkgKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0JHRoaXMudGV4dCggaTE4bi5wYXJzZSggbWVzc2FnZUtleSApICk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdCR0aGlzLmZpbmQoICdbZGF0YS1pMThuXScgKS5pMThuKCk7XHJcblx0XHRcdH1cclxuXHRcdH0gKTtcclxuXHR9O1xyXG5cclxuXHRTdHJpbmcubG9jYWxlID0gU3RyaW5nLmxvY2FsZSB8fCAkKCAnaHRtbCcgKS5hdHRyKCAnbGFuZycgKTtcclxuXHJcblx0aWYgKCAhU3RyaW5nLmxvY2FsZSApIHtcclxuXHRcdGlmICggdHlwZW9mIHdpbmRvdy5uYXZpZ2F0b3IgIT09IHVuZGVmaW5lZCApIHtcclxuXHRcdFx0bmF2ID0gd2luZG93Lm5hdmlnYXRvcjtcclxuXHRcdFx0U3RyaW5nLmxvY2FsZSA9IG5hdi5sYW5ndWFnZSB8fCBuYXYudXNlckxhbmd1YWdlIHx8ICcnO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0U3RyaW5nLmxvY2FsZSA9ICcnO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0JC5pMThuLmxhbmd1YWdlcyA9IHt9O1xyXG5cdCQuaTE4bi5tZXNzYWdlU3RvcmUgPSAkLmkxOG4ubWVzc2FnZVN0b3JlIHx8IHt9O1xyXG5cdCQuaTE4bi5wYXJzZXIgPSB7XHJcblx0XHQvLyBUaGUgZGVmYXVsdCBwYXJzZXIgb25seSBoYW5kbGVzIHZhcmlhYmxlIHN1YnN0aXR1dGlvblxyXG5cdFx0cGFyc2U6IGZ1bmN0aW9uICggbWVzc2FnZSwgcGFyYW1ldGVycyApIHtcclxuXHRcdFx0cmV0dXJuIG1lc3NhZ2UucmVwbGFjZSggL1xcJChcXGQrKS9nLCBmdW5jdGlvbiAoIHN0ciwgbWF0Y2ggKSB7XHJcblx0XHRcdFx0dmFyIGluZGV4ID0gcGFyc2VJbnQoIG1hdGNoLCAxMCApIC0gMTtcclxuXHRcdFx0XHRyZXR1cm4gcGFyYW1ldGVyc1sgaW5kZXggXSAhPT0gdW5kZWZpbmVkID8gcGFyYW1ldGVyc1sgaW5kZXggXSA6ICckJyArIG1hdGNoO1xyXG5cdFx0XHR9ICk7XHJcblx0XHR9LFxyXG5cdFx0ZW1pdHRlcjoge31cclxuXHR9O1xyXG5cdCQuaTE4bi5mYWxsYmFja3MgPSB7fTtcclxuXHQkLmkxOG4uZGVidWcgPSBmYWxzZTtcclxuXHQkLmkxOG4ubG9nID0gZnVuY3Rpb24gKCAvKiBhcmd1bWVudHMgKi8gKSB7XHJcblx0XHRpZiAoIHdpbmRvdy5jb25zb2xlICYmICQuaTE4bi5kZWJ1ZyApIHtcclxuXHRcdFx0d2luZG93LmNvbnNvbGUubG9nLmFwcGx5KCB3aW5kb3cuY29uc29sZSwgYXJndW1lbnRzICk7XHJcblx0XHR9XHJcblx0fTtcclxuXHQvKiBTdGF0aWMgbWVtYmVycyAqL1xyXG5cdEkxOE4uZGVmYXVsdHMgPSB7XHJcblx0XHRsb2NhbGU6IFN0cmluZy5sb2NhbGUsXHJcblx0XHRmYWxsYmFja0xvY2FsZTogJ2VuJyxcclxuXHRcdHBhcnNlcjogJC5pMThuLnBhcnNlcixcclxuXHRcdG1lc3NhZ2VTdG9yZTogJC5pMThuLm1lc3NhZ2VTdG9yZVxyXG5cdH07XHJcblxyXG5cdC8vIEV4cG9zZSBjb25zdHJ1Y3RvclxyXG5cdCQuaTE4bi5jb25zdHJ1Y3RvciA9IEkxOE47XHJcbn0oIGpRdWVyeSApICk7XHJcbiIsIi8qIGdsb2JhbCBwbHVyYWxSdWxlUGFyc2VyICovXHJcbiggZnVuY3Rpb24gKCAkICkge1xyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHJcblx0Ly8ganNjczpkaXNhYmxlXHJcblx0dmFyIGxhbmd1YWdlID0ge1xyXG5cdFx0Ly8gQ0xEUiBwbHVyYWwgcnVsZXMgZ2VuZXJhdGVkIHVzaW5nXHJcblx0XHQvLyBsaWJzL0NMRFJQbHVyYWxSdWxlUGFyc2VyL3Rvb2xzL1BsdXJhbFhNTDJKU09OLmh0bWxcclxuXHRcdHBsdXJhbFJ1bGVzOiB7XHJcblx0XHRcdGFrOiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDAuLjEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGFtOiB7XHJcblx0XHRcdFx0b25lOiAnaSA9IDAgb3IgbiA9IDEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGFyOiB7XHJcblx0XHRcdFx0emVybzogJ24gPSAwJyxcclxuXHRcdFx0XHRvbmU6ICduID0gMScsXHJcblx0XHRcdFx0dHdvOiAnbiA9IDInLFxyXG5cdFx0XHRcdGZldzogJ24gJSAxMDAgPSAzLi4xMCcsXHJcblx0XHRcdFx0bWFueTogJ24gJSAxMDAgPSAxMS4uOTknXHJcblx0XHRcdH0sXHJcblx0XHRcdGFyczoge1xyXG5cdFx0XHRcdHplcm86ICduID0gMCcsXHJcblx0XHRcdFx0b25lOiAnbiA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ24gPSAyJyxcclxuXHRcdFx0XHRmZXc6ICduICUgMTAwID0gMy4uMTAnLFxyXG5cdFx0XHRcdG1hbnk6ICduICUgMTAwID0gMTEuLjk5J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRhczoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAwIG9yIG4gPSAxJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRiZToge1xyXG5cdFx0XHRcdG9uZTogJ24gJSAxMCA9IDEgYW5kIG4gJSAxMDAgIT0gMTEnLFxyXG5cdFx0XHRcdGZldzogJ24gJSAxMCA9IDIuLjQgYW5kIG4gJSAxMDAgIT0gMTIuLjE0JyxcclxuXHRcdFx0XHRtYW55OiAnbiAlIDEwID0gMCBvciBuICUgMTAgPSA1Li45IG9yIG4gJSAxMDAgPSAxMS4uMTQnXHJcblx0XHRcdH0sXHJcblx0XHRcdGJoOiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDAuLjEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGJuOiB7XHJcblx0XHRcdFx0b25lOiAnaSA9IDAgb3IgbiA9IDEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGJyOiB7XHJcblx0XHRcdFx0b25lOiAnbiAlIDEwID0gMSBhbmQgbiAlIDEwMCAhPSAxMSw3MSw5MScsXHJcblx0XHRcdFx0dHdvOiAnbiAlIDEwID0gMiBhbmQgbiAlIDEwMCAhPSAxMiw3Miw5MicsXHJcblx0XHRcdFx0ZmV3OiAnbiAlIDEwID0gMy4uNCw5IGFuZCBuICUgMTAwICE9IDEwLi4xOSw3MC4uNzksOTAuLjk5JyxcclxuXHRcdFx0XHRtYW55OiAnbiAhPSAwIGFuZCBuICUgMTAwMDAwMCA9IDAnXHJcblx0XHRcdH0sXHJcblx0XHRcdGJzOiB7XHJcblx0XHRcdFx0b25lOiAndiA9IDAgYW5kIGkgJSAxMCA9IDEgYW5kIGkgJSAxMDAgIT0gMTEgb3IgZiAlIDEwID0gMSBhbmQgZiAlIDEwMCAhPSAxMScsXHJcblx0XHRcdFx0ZmV3OiAndiA9IDAgYW5kIGkgJSAxMCA9IDIuLjQgYW5kIGkgJSAxMDAgIT0gMTIuLjE0IG9yIGYgJSAxMCA9IDIuLjQgYW5kIGYgJSAxMDAgIT0gMTIuLjE0J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRjczoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAxIGFuZCB2ID0gMCcsXHJcblx0XHRcdFx0ZmV3OiAnaSA9IDIuLjQgYW5kIHYgPSAwJyxcclxuXHRcdFx0XHRtYW55OiAndiAhPSAwJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRjeToge1xyXG5cdFx0XHRcdHplcm86ICduID0gMCcsXHJcblx0XHRcdFx0b25lOiAnbiA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ24gPSAyJyxcclxuXHRcdFx0XHRmZXc6ICduID0gMycsXHJcblx0XHRcdFx0bWFueTogJ24gPSA2J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRkYToge1xyXG5cdFx0XHRcdG9uZTogJ24gPSAxIG9yIHQgIT0gMCBhbmQgaSA9IDAsMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0ZHNiOiB7XHJcblx0XHRcdFx0b25lOiAndiA9IDAgYW5kIGkgJSAxMDAgPSAxIG9yIGYgJSAxMDAgPSAxJyxcclxuXHRcdFx0XHR0d286ICd2ID0gMCBhbmQgaSAlIDEwMCA9IDIgb3IgZiAlIDEwMCA9IDInLFxyXG5cdFx0XHRcdGZldzogJ3YgPSAwIGFuZCBpICUgMTAwID0gMy4uNCBvciBmICUgMTAwID0gMy4uNCdcclxuXHRcdFx0fSxcclxuXHRcdFx0ZmE6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCBvciBuID0gMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0ZmY6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCwxJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRmaWw6IHtcclxuXHRcdFx0XHRvbmU6ICd2ID0gMCBhbmQgaSA9IDEsMiwzIG9yIHYgPSAwIGFuZCBpICUgMTAgIT0gNCw2LDkgb3IgdiAhPSAwIGFuZCBmICUgMTAgIT0gNCw2LDknXHJcblx0XHRcdH0sXHJcblx0XHRcdGZyOiB7XHJcblx0XHRcdFx0b25lOiAnaSA9IDAsMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0Z2E6IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMScsXHJcblx0XHRcdFx0dHdvOiAnbiA9IDInLFxyXG5cdFx0XHRcdGZldzogJ24gPSAzLi42JyxcclxuXHRcdFx0XHRtYW55OiAnbiA9IDcuLjEwJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRnZDoge1xyXG5cdFx0XHRcdG9uZTogJ24gPSAxLDExJyxcclxuXHRcdFx0XHR0d286ICduID0gMiwxMicsXHJcblx0XHRcdFx0ZmV3OiAnbiA9IDMuLjEwLDEzLi4xOSdcclxuXHRcdFx0fSxcclxuXHRcdFx0Z3U6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCBvciBuID0gMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0Z3V3OiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDAuLjEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGd2OiB7XHJcblx0XHRcdFx0b25lOiAndiA9IDAgYW5kIGkgJSAxMCA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ3YgPSAwIGFuZCBpICUgMTAgPSAyJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwMCA9IDAsMjAsNDAsNjAsODAnLFxyXG5cdFx0XHRcdG1hbnk6ICd2ICE9IDAnXHJcblx0XHRcdH0sXHJcblx0XHRcdGhlOiB7XHJcblx0XHRcdFx0b25lOiAnaSA9IDEgYW5kIHYgPSAwJyxcclxuXHRcdFx0XHR0d286ICdpID0gMiBhbmQgdiA9IDAnLFxyXG5cdFx0XHRcdG1hbnk6ICd2ID0gMCBhbmQgbiAhPSAwLi4xMCBhbmQgbiAlIDEwID0gMCdcclxuXHRcdFx0fSxcclxuXHRcdFx0aGk6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCBvciBuID0gMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0aHI6IHtcclxuXHRcdFx0XHRvbmU6ICd2ID0gMCBhbmQgaSAlIDEwID0gMSBhbmQgaSAlIDEwMCAhPSAxMSBvciBmICUgMTAgPSAxIGFuZCBmICUgMTAwICE9IDExJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwID0gMi4uNCBhbmQgaSAlIDEwMCAhPSAxMi4uMTQgb3IgZiAlIDEwID0gMi4uNCBhbmQgZiAlIDEwMCAhPSAxMi4uMTQnXHJcblx0XHRcdH0sXHJcblx0XHRcdGhzYjoge1xyXG5cdFx0XHRcdG9uZTogJ3YgPSAwIGFuZCBpICUgMTAwID0gMSBvciBmICUgMTAwID0gMScsXHJcblx0XHRcdFx0dHdvOiAndiA9IDAgYW5kIGkgJSAxMDAgPSAyIG9yIGYgJSAxMDAgPSAyJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwMCA9IDMuLjQgb3IgZiAlIDEwMCA9IDMuLjQnXHJcblx0XHRcdH0sXHJcblx0XHRcdGh5OiB7XHJcblx0XHRcdFx0b25lOiAnaSA9IDAsMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0aXM6IHtcclxuXHRcdFx0XHRvbmU6ICd0ID0gMCBhbmQgaSAlIDEwID0gMSBhbmQgaSAlIDEwMCAhPSAxMSBvciB0ICE9IDAnXHJcblx0XHRcdH0sXHJcblx0XHRcdGl1OiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ24gPSAyJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRpdzoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAxIGFuZCB2ID0gMCcsXHJcblx0XHRcdFx0dHdvOiAnaSA9IDIgYW5kIHYgPSAwJyxcclxuXHRcdFx0XHRtYW55OiAndiA9IDAgYW5kIG4gIT0gMC4uMTAgYW5kIG4gJSAxMCA9IDAnXHJcblx0XHRcdH0sXHJcblx0XHRcdGthYjoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAwLDEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGtuOiB7XHJcblx0XHRcdFx0b25lOiAnaSA9IDAgb3IgbiA9IDEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGt3OiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ24gPSAyJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRsYWc6IHtcclxuXHRcdFx0XHR6ZXJvOiAnbiA9IDAnLFxyXG5cdFx0XHRcdG9uZTogJ2kgPSAwLDEgYW5kIG4gIT0gMCdcclxuXHRcdFx0fSxcclxuXHRcdFx0bG46IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMC4uMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0bHQ6IHtcclxuXHRcdFx0XHRvbmU6ICduICUgMTAgPSAxIGFuZCBuICUgMTAwICE9IDExLi4xOScsXHJcblx0XHRcdFx0ZmV3OiAnbiAlIDEwID0gMi4uOSBhbmQgbiAlIDEwMCAhPSAxMS4uMTknLFxyXG5cdFx0XHRcdG1hbnk6ICdmICE9IDAnXHJcblx0XHRcdH0sXHJcblx0XHRcdGx2OiB7XHJcblx0XHRcdFx0emVybzogJ24gJSAxMCA9IDAgb3IgbiAlIDEwMCA9IDExLi4xOSBvciB2ID0gMiBhbmQgZiAlIDEwMCA9IDExLi4xOScsXHJcblx0XHRcdFx0b25lOiAnbiAlIDEwID0gMSBhbmQgbiAlIDEwMCAhPSAxMSBvciB2ID0gMiBhbmQgZiAlIDEwID0gMSBhbmQgZiAlIDEwMCAhPSAxMSBvciB2ICE9IDIgYW5kIGYgJSAxMCA9IDEnXHJcblx0XHRcdH0sXHJcblx0XHRcdG1nOiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDAuLjEnXHJcblx0XHRcdH0sXHJcblx0XHRcdG1rOiB7XHJcblx0XHRcdFx0b25lOiAndiA9IDAgYW5kIGkgJSAxMCA9IDEgb3IgZiAlIDEwID0gMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0bW86IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMSBhbmQgdiA9IDAnLFxyXG5cdFx0XHRcdGZldzogJ3YgIT0gMCBvciBuID0gMCBvciBuICE9IDEgYW5kIG4gJSAxMDAgPSAxLi4xOSdcclxuXHRcdFx0fSxcclxuXHRcdFx0bXI6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCBvciBuID0gMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0bXQ6IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMScsXHJcblx0XHRcdFx0ZmV3OiAnbiA9IDAgb3IgbiAlIDEwMCA9IDIuLjEwJyxcclxuXHRcdFx0XHRtYW55OiAnbiAlIDEwMCA9IDExLi4xOSdcclxuXHRcdFx0fSxcclxuXHRcdFx0bmFxOiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ24gPSAyJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRuc286IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMC4uMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0cGE6IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMC4uMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0cGw6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMSBhbmQgdiA9IDAnLFxyXG5cdFx0XHRcdGZldzogJ3YgPSAwIGFuZCBpICUgMTAgPSAyLi40IGFuZCBpICUgMTAwICE9IDEyLi4xNCcsXHJcblx0XHRcdFx0bWFueTogJ3YgPSAwIGFuZCBpICE9IDEgYW5kIGkgJSAxMCA9IDAuLjEgb3IgdiA9IDAgYW5kIGkgJSAxMCA9IDUuLjkgb3IgdiA9IDAgYW5kIGkgJSAxMDAgPSAxMi4uMTQnXHJcblx0XHRcdH0sXHJcblx0XHRcdHByZzoge1xyXG5cdFx0XHRcdHplcm86ICduICUgMTAgPSAwIG9yIG4gJSAxMDAgPSAxMS4uMTkgb3IgdiA9IDIgYW5kIGYgJSAxMDAgPSAxMS4uMTknLFxyXG5cdFx0XHRcdG9uZTogJ24gJSAxMCA9IDEgYW5kIG4gJSAxMDAgIT0gMTEgb3IgdiA9IDIgYW5kIGYgJSAxMCA9IDEgYW5kIGYgJSAxMDAgIT0gMTEgb3IgdiAhPSAyIGFuZCBmICUgMTAgPSAxJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRwdDoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAwLi4xJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRybzoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAxIGFuZCB2ID0gMCcsXHJcblx0XHRcdFx0ZmV3OiAndiAhPSAwIG9yIG4gPSAwIG9yIG4gIT0gMSBhbmQgbiAlIDEwMCA9IDEuLjE5J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRydToge1xyXG5cdFx0XHRcdG9uZTogJ3YgPSAwIGFuZCBpICUgMTAgPSAxIGFuZCBpICUgMTAwICE9IDExJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwID0gMi4uNCBhbmQgaSAlIDEwMCAhPSAxMi4uMTQnLFxyXG5cdFx0XHRcdG1hbnk6ICd2ID0gMCBhbmQgaSAlIDEwID0gMCBvciB2ID0gMCBhbmQgaSAlIDEwID0gNS4uOSBvciB2ID0gMCBhbmQgaSAlIDEwMCA9IDExLi4xNCdcclxuXHRcdFx0fSxcclxuXHRcdFx0c2U6IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMScsXHJcblx0XHRcdFx0dHdvOiAnbiA9IDInXHJcblx0XHRcdH0sXHJcblx0XHRcdHNoOiB7XHJcblx0XHRcdFx0b25lOiAndiA9IDAgYW5kIGkgJSAxMCA9IDEgYW5kIGkgJSAxMDAgIT0gMTEgb3IgZiAlIDEwID0gMSBhbmQgZiAlIDEwMCAhPSAxMScsXHJcblx0XHRcdFx0ZmV3OiAndiA9IDAgYW5kIGkgJSAxMCA9IDIuLjQgYW5kIGkgJSAxMDAgIT0gMTIuLjE0IG9yIGYgJSAxMCA9IDIuLjQgYW5kIGYgJSAxMDAgIT0gMTIuLjE0J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRzaGk6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCBvciBuID0gMScsXHJcblx0XHRcdFx0ZmV3OiAnbiA9IDIuLjEwJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRzaToge1xyXG5cdFx0XHRcdG9uZTogJ24gPSAwLDEgb3IgaSA9IDAgYW5kIGYgPSAxJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRzazoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAxIGFuZCB2ID0gMCcsXHJcblx0XHRcdFx0ZmV3OiAnaSA9IDIuLjQgYW5kIHYgPSAwJyxcclxuXHRcdFx0XHRtYW55OiAndiAhPSAwJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRzbDoge1xyXG5cdFx0XHRcdG9uZTogJ3YgPSAwIGFuZCBpICUgMTAwID0gMScsXHJcblx0XHRcdFx0dHdvOiAndiA9IDAgYW5kIGkgJSAxMDAgPSAyJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwMCA9IDMuLjQgb3IgdiAhPSAwJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRzbWE6IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMScsXHJcblx0XHRcdFx0dHdvOiAnbiA9IDInXHJcblx0XHRcdH0sXHJcblx0XHRcdHNtaToge1xyXG5cdFx0XHRcdG9uZTogJ24gPSAxJyxcclxuXHRcdFx0XHR0d286ICduID0gMidcclxuXHRcdFx0fSxcclxuXHRcdFx0c21qOiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ24gPSAyJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRzbW46IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMScsXHJcblx0XHRcdFx0dHdvOiAnbiA9IDInXHJcblx0XHRcdH0sXHJcblx0XHRcdHNtczoge1xyXG5cdFx0XHRcdG9uZTogJ24gPSAxJyxcclxuXHRcdFx0XHR0d286ICduID0gMidcclxuXHRcdFx0fSxcclxuXHRcdFx0c3I6IHtcclxuXHRcdFx0XHRvbmU6ICd2ID0gMCBhbmQgaSAlIDEwID0gMSBhbmQgaSAlIDEwMCAhPSAxMSBvciBmICUgMTAgPSAxIGFuZCBmICUgMTAwICE9IDExJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwID0gMi4uNCBhbmQgaSAlIDEwMCAhPSAxMi4uMTQgb3IgZiAlIDEwID0gMi4uNCBhbmQgZiAlIDEwMCAhPSAxMi4uMTQnXHJcblx0XHRcdH0sXHJcblx0XHRcdHRpOiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDAuLjEnXHJcblx0XHRcdH0sXHJcblx0XHRcdHRsOiB7XHJcblx0XHRcdFx0b25lOiAndiA9IDAgYW5kIGkgPSAxLDIsMyBvciB2ID0gMCBhbmQgaSAlIDEwICE9IDQsNiw5IG9yIHYgIT0gMCBhbmQgZiAlIDEwICE9IDQsNiw5J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHR0em06IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMC4uMSBvciBuID0gMTEuLjk5J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHR1azoge1xyXG5cdFx0XHRcdG9uZTogJ3YgPSAwIGFuZCBpICUgMTAgPSAxIGFuZCBpICUgMTAwICE9IDExJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwID0gMi4uNCBhbmQgaSAlIDEwMCAhPSAxMi4uMTQnLFxyXG5cdFx0XHRcdG1hbnk6ICd2ID0gMCBhbmQgaSAlIDEwID0gMCBvciB2ID0gMCBhbmQgaSAlIDEwID0gNS4uOSBvciB2ID0gMCBhbmQgaSAlIDEwMCA9IDExLi4xNCdcclxuXHRcdFx0fSxcclxuXHRcdFx0d2E6IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMC4uMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0enU6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCBvciBuID0gMSdcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcdC8vIGpzY3M6ZW5hYmxlXHJcblxyXG5cdFx0LyoqXHJcblx0XHQgKiBQbHVyYWwgZm9ybSB0cmFuc2Zvcm1hdGlvbnMsIG5lZWRlZCBmb3Igc29tZSBsYW5ndWFnZXMuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtpbnRlZ2VyfSBjb3VudFxyXG5cdFx0ICogICAgICAgICAgICBOb24tbG9jYWxpemVkIHF1YW50aWZpZXJcclxuXHRcdCAqIEBwYXJhbSB7QXJyYXl9IGZvcm1zXHJcblx0XHQgKiAgICAgICAgICAgIExpc3Qgb2YgcGx1cmFsIGZvcm1zXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9IENvcnJlY3QgZm9ybSBmb3IgcXVhbnRpZmllciBpbiB0aGlzIGxhbmd1YWdlXHJcblx0XHQgKi9cclxuXHRcdGNvbnZlcnRQbHVyYWw6IGZ1bmN0aW9uICggY291bnQsIGZvcm1zICkge1xyXG5cdFx0XHR2YXIgcGx1cmFsUnVsZXMsXHJcblx0XHRcdFx0cGx1cmFsRm9ybUluZGV4LFxyXG5cdFx0XHRcdGluZGV4LFxyXG5cdFx0XHRcdGV4cGxpY2l0UGx1cmFsUGF0dGVybiA9IG5ldyBSZWdFeHAoICdcXFxcZCs9JywgJ2knICksXHJcblx0XHRcdFx0Zm9ybUNvdW50LFxyXG5cdFx0XHRcdGZvcm07XHJcblxyXG5cdFx0XHRpZiAoICFmb3JtcyB8fCBmb3Jtcy5sZW5ndGggPT09IDAgKSB7XHJcblx0XHRcdFx0cmV0dXJuICcnO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQvLyBIYW5kbGUgZm9yIEV4cGxpY2l0IDA9ICYgMT0gdmFsdWVzXHJcblx0XHRcdGZvciAoIGluZGV4ID0gMDsgaW5kZXggPCBmb3Jtcy5sZW5ndGg7IGluZGV4KysgKSB7XHJcblx0XHRcdFx0Zm9ybSA9IGZvcm1zWyBpbmRleCBdO1xyXG5cdFx0XHRcdGlmICggZXhwbGljaXRQbHVyYWxQYXR0ZXJuLnRlc3QoIGZvcm0gKSApIHtcclxuXHRcdFx0XHRcdGZvcm1Db3VudCA9IHBhcnNlSW50KCBmb3JtLnNsaWNlKCAwLCBmb3JtLmluZGV4T2YoICc9JyApICksIDEwICk7XHJcblx0XHRcdFx0XHRpZiAoIGZvcm1Db3VudCA9PT0gY291bnQgKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybiAoIGZvcm0uc2xpY2UoIGZvcm0uaW5kZXhPZiggJz0nICkgKyAxICkgKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGZvcm1zWyBpbmRleCBdID0gdW5kZWZpbmVkO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0Zm9ybXMgPSAkLm1hcCggZm9ybXMsIGZ1bmN0aW9uICggZm9ybSApIHtcclxuXHRcdFx0XHRpZiAoIGZvcm0gIT09IHVuZGVmaW5lZCApIHtcclxuXHRcdFx0XHRcdHJldHVybiBmb3JtO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSApO1xyXG5cclxuXHRcdFx0cGx1cmFsUnVsZXMgPSB0aGlzLnBsdXJhbFJ1bGVzWyAkLmkxOG4oKS5sb2NhbGUgXTtcclxuXHJcblx0XHRcdGlmICggIXBsdXJhbFJ1bGVzICkge1xyXG5cdFx0XHRcdC8vIGRlZmF1bHQgZmFsbGJhY2suXHJcblx0XHRcdFx0cmV0dXJuICggY291bnQgPT09IDEgKSA/IGZvcm1zWyAwIF0gOiBmb3Jtc1sgMSBdO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRwbHVyYWxGb3JtSW5kZXggPSB0aGlzLmdldFBsdXJhbEZvcm0oIGNvdW50LCBwbHVyYWxSdWxlcyApO1xyXG5cdFx0XHRwbHVyYWxGb3JtSW5kZXggPSBNYXRoLm1pbiggcGx1cmFsRm9ybUluZGV4LCBmb3Jtcy5sZW5ndGggLSAxICk7XHJcblxyXG5cdFx0XHRyZXR1cm4gZm9ybXNbIHBsdXJhbEZvcm1JbmRleCBdO1xyXG5cdFx0fSxcclxuXHJcblx0XHQvKipcclxuXHRcdCAqIEZvciB0aGUgbnVtYmVyLCBnZXQgdGhlIHBsdXJhbCBmb3IgaW5kZXhcclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2ludGVnZXJ9IG51bWJlclxyXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHBsdXJhbFJ1bGVzXHJcblx0XHQgKiBAcmV0dXJuIHtpbnRlZ2VyfSBwbHVyYWwgZm9ybSBpbmRleFxyXG5cdFx0ICovXHJcblx0XHRnZXRQbHVyYWxGb3JtOiBmdW5jdGlvbiAoIG51bWJlciwgcGx1cmFsUnVsZXMgKSB7XHJcblx0XHRcdHZhciBpLFxyXG5cdFx0XHRcdHBsdXJhbEZvcm1zID0gWyAnemVybycsICdvbmUnLCAndHdvJywgJ2ZldycsICdtYW55JywgJ290aGVyJyBdLFxyXG5cdFx0XHRcdHBsdXJhbEZvcm1JbmRleCA9IDA7XHJcblxyXG5cdFx0XHRmb3IgKCBpID0gMDsgaSA8IHBsdXJhbEZvcm1zLmxlbmd0aDsgaSsrICkge1xyXG5cdFx0XHRcdGlmICggcGx1cmFsUnVsZXNbIHBsdXJhbEZvcm1zWyBpIF0gXSApIHtcclxuXHRcdFx0XHRcdGlmICggcGx1cmFsUnVsZVBhcnNlciggcGx1cmFsUnVsZXNbIHBsdXJhbEZvcm1zWyBpIF0gXSwgbnVtYmVyICkgKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybiBwbHVyYWxGb3JtSW5kZXg7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0cGx1cmFsRm9ybUluZGV4Kys7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRyZXR1cm4gcGx1cmFsRm9ybUluZGV4O1xyXG5cdFx0fSxcclxuXHJcblx0XHQvKipcclxuXHRcdCAqIENvbnZlcnRzIGEgbnVtYmVyIHVzaW5nIGRpZ2l0VHJhbnNmb3JtVGFibGUuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtudW1iZXJ9IG51bSBWYWx1ZSB0byBiZSBjb252ZXJ0ZWRcclxuXHRcdCAqIEBwYXJhbSB7Ym9vbGVhbn0gaW50ZWdlciBDb252ZXJ0IHRoZSByZXR1cm4gdmFsdWUgdG8gYW4gaW50ZWdlclxyXG5cdFx0ICogQHJldHVybiB7c3RyaW5nfSBUaGUgbnVtYmVyIGNvbnZlcnRlZCBpbnRvIGEgU3RyaW5nLlxyXG5cdFx0ICovXHJcblx0XHRjb252ZXJ0TnVtYmVyOiBmdW5jdGlvbiAoIG51bSwgaW50ZWdlciApIHtcclxuXHRcdFx0dmFyIHRtcCwgaXRlbSwgaSxcclxuXHRcdFx0XHR0cmFuc2Zvcm1UYWJsZSwgbnVtYmVyU3RyaW5nLCBjb252ZXJ0ZWROdW1iZXI7XHJcblxyXG5cdFx0XHQvLyBTZXQgdGhlIHRhcmdldCBUcmFuc2Zvcm0gdGFibGU6XHJcblx0XHRcdHRyYW5zZm9ybVRhYmxlID0gdGhpcy5kaWdpdFRyYW5zZm9ybVRhYmxlKCAkLmkxOG4oKS5sb2NhbGUgKTtcclxuXHRcdFx0bnVtYmVyU3RyaW5nID0gU3RyaW5nKCBudW0gKTtcclxuXHRcdFx0Y29udmVydGVkTnVtYmVyID0gJyc7XHJcblxyXG5cdFx0XHRpZiAoICF0cmFuc2Zvcm1UYWJsZSApIHtcclxuXHRcdFx0XHRyZXR1cm4gbnVtO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQvLyBDaGVjayBpZiB0aGUgcmVzdG9yZSB0byBMYXRpbiBudW1iZXIgZmxhZyBpcyBzZXQ6XHJcblx0XHRcdGlmICggaW50ZWdlciApIHtcclxuXHRcdFx0XHRpZiAoIHBhcnNlRmxvYXQoIG51bSwgMTAgKSA9PT0gbnVtICkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIG51bTtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdHRtcCA9IFtdO1xyXG5cclxuXHRcdFx0XHRmb3IgKCBpdGVtIGluIHRyYW5zZm9ybVRhYmxlICkge1xyXG5cdFx0XHRcdFx0dG1wWyB0cmFuc2Zvcm1UYWJsZVsgaXRlbSBdIF0gPSBpdGVtO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0dHJhbnNmb3JtVGFibGUgPSB0bXA7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGZvciAoIGkgPSAwOyBpIDwgbnVtYmVyU3RyaW5nLmxlbmd0aDsgaSsrICkge1xyXG5cdFx0XHRcdGlmICggdHJhbnNmb3JtVGFibGVbIG51bWJlclN0cmluZ1sgaSBdIF0gKSB7XHJcblx0XHRcdFx0XHRjb252ZXJ0ZWROdW1iZXIgKz0gdHJhbnNmb3JtVGFibGVbIG51bWJlclN0cmluZ1sgaSBdIF07XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdGNvbnZlcnRlZE51bWJlciArPSBudW1iZXJTdHJpbmdbIGkgXTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJldHVybiBpbnRlZ2VyID8gcGFyc2VGbG9hdCggY29udmVydGVkTnVtYmVyLCAxMCApIDogY29udmVydGVkTnVtYmVyO1xyXG5cdFx0fSxcclxuXHJcblx0XHQvKipcclxuXHRcdCAqIEdyYW1tYXRpY2FsIHRyYW5zZm9ybWF0aW9ucywgbmVlZGVkIGZvciBpbmZsZWN0ZWQgbGFuZ3VhZ2VzLlxyXG5cdFx0ICogSW52b2tlZCBieSBwdXR0aW5nIHt7Z3JhbW1hcjpmb3JtfHdvcmR9fSBpbiBhIG1lc3NhZ2UuXHJcblx0XHQgKiBPdmVycmlkZSB0aGlzIG1ldGhvZCBmb3IgbGFuZ3VhZ2VzIHRoYXQgbmVlZCBzcGVjaWFsIGdyYW1tYXIgcnVsZXNcclxuXHRcdCAqIGFwcGxpZWQgZHluYW1pY2FsbHkuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IHdvcmRcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBmb3JtXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9XHJcblx0XHQgKi9cclxuXHRcdC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby11bnVzZWQtdmFyc1xyXG5cdFx0Y29udmVydEdyYW1tYXI6IGZ1bmN0aW9uICggd29yZCwgZm9ybSApIHtcclxuXHRcdFx0cmV0dXJuIHdvcmQ7XHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogUHJvdmlkZXMgYW4gYWx0ZXJuYXRpdmUgdGV4dCBkZXBlbmRpbmcgb24gc3BlY2lmaWVkIGdlbmRlci4gVXNhZ2VcclxuXHRcdCAqIHt7Z2VuZGVyOltnZW5kZXJ8dXNlciBvYmplY3RdfG1hc2N1bGluZXxmZW1pbmluZXxuZXV0cmFsfX0uIElmIHNlY29uZFxyXG5cdFx0ICogb3IgdGhpcmQgcGFyYW1ldGVyIGFyZSBub3Qgc3BlY2lmaWVkLCBtYXNjdWxpbmUgaXMgdXNlZC5cclxuXHRcdCAqXHJcblx0XHQgKiBUaGVzZSBkZXRhaWxzIG1heSBiZSBvdmVycmlkZW4gcGVyIGxhbmd1YWdlLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBnZW5kZXJcclxuXHRcdCAqICAgICAgbWFsZSwgZmVtYWxlLCBvciBhbnl0aGluZyBlbHNlIGZvciBuZXV0cmFsLlxyXG5cdFx0ICogQHBhcmFtIHtBcnJheX0gZm9ybXNcclxuXHRcdCAqICAgICAgTGlzdCBvZiBnZW5kZXIgZm9ybXNcclxuXHRcdCAqXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9XHJcblx0XHQgKi9cclxuXHRcdGdlbmRlcjogZnVuY3Rpb24gKCBnZW5kZXIsIGZvcm1zICkge1xyXG5cdFx0XHRpZiAoICFmb3JtcyB8fCBmb3Jtcy5sZW5ndGggPT09IDAgKSB7XHJcblx0XHRcdFx0cmV0dXJuICcnO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHR3aGlsZSAoIGZvcm1zLmxlbmd0aCA8IDIgKSB7XHJcblx0XHRcdFx0Zm9ybXMucHVzaCggZm9ybXNbIGZvcm1zLmxlbmd0aCAtIDEgXSApO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAoIGdlbmRlciA9PT0gJ21hbGUnICkge1xyXG5cdFx0XHRcdHJldHVybiBmb3Jtc1sgMCBdO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAoIGdlbmRlciA9PT0gJ2ZlbWFsZScgKSB7XHJcblx0XHRcdFx0cmV0dXJuIGZvcm1zWyAxIF07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJldHVybiAoIGZvcm1zLmxlbmd0aCA9PT0gMyApID8gZm9ybXNbIDIgXSA6IGZvcm1zWyAwIF07XHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogR2V0IHRoZSBkaWdpdCB0cmFuc2Zvcm0gdGFibGUgZm9yIHRoZSBnaXZlbiBsYW5ndWFnZVxyXG5cdFx0ICogU2VlIGh0dHA6Ly9jbGRyLnVuaWNvZGUub3JnL3RyYW5zbGF0aW9uL251bWJlcmluZy1zeXN0ZW1zXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IGxhbmd1YWdlXHJcblx0XHQgKiBAcmV0dXJuIHtBcnJheXxib29sZWFufSBMaXN0IG9mIGRpZ2l0cyBpbiB0aGUgcGFzc2VkIGxhbmd1YWdlIG9yIGZhbHNlXHJcblx0XHQgKiByZXByZXNlbnRhdGlvbiwgb3IgYm9vbGVhbiBmYWxzZSBpZiB0aGVyZSBpcyBubyBpbmZvcm1hdGlvbi5cclxuXHRcdCAqL1xyXG5cdFx0ZGlnaXRUcmFuc2Zvcm1UYWJsZTogZnVuY3Rpb24gKCBsYW5ndWFnZSApIHtcclxuXHRcdFx0dmFyIHRhYmxlcyA9IHtcclxuXHRcdFx0XHRhcjogJ9mg2aHZotmj2aTZpdmm2afZqNmpJyxcclxuXHRcdFx0XHRmYTogJ9uw27Hbstuz27Tbtdu227fbuNu5JyxcclxuXHRcdFx0XHRtbDogJ+C1puC1p+C1qOC1qeC1quC1q+C1rOC1reC1ruC1rycsXHJcblx0XHRcdFx0a246ICfgs6bgs6fgs6jgs6ngs6rgs6vgs6zgs63gs67gs68nLFxyXG5cdFx0XHRcdGxvOiAn4LuQ4LuR4LuS4LuT4LuU4LuV4LuW4LuX4LuY4LuZJyxcclxuXHRcdFx0XHRvcjogJ+CtpuCtp+CtqOCtqeCtquCtq+CtrOCtreCtruCtrycsXHJcblx0XHRcdFx0a2g6ICfhn6Dhn6Hhn6Lhn6Phn6Thn6Xhn6bhn6fhn6jhn6knLFxyXG5cdFx0XHRcdHBhOiAn4Kmm4Kmn4Kmo4Kmp4Kmq4Kmr4Kms4Kmt4Kmu4KmvJyxcclxuXHRcdFx0XHRndTogJ+CrpuCrp+CrqOCrqeCrquCrq+CrrOCrreCrruCrrycsXHJcblx0XHRcdFx0aGk6ICfgpabgpafgpajgpangpargpavgpazgpa3gpa7gpa8nLFxyXG5cdFx0XHRcdG15OiAn4YGA4YGB4YGC4YGD4YGE4YGF4YGG4YGH4YGI4YGJJyxcclxuXHRcdFx0XHR0YTogJ+CvpuCvp+CvqOCvqeCvquCvq+CvrOCvreCvruCvrycsXHJcblx0XHRcdFx0dGU6ICfgsabgsafgsajgsangsargsavgsazgsa3gsa7gsa8nLFxyXG5cdFx0XHRcdHRoOiAn4LmQ4LmR4LmS4LmT4LmU4LmV4LmW4LmX4LmY4LmZJywgLy8gRklYTUUgdXNlIGlzbyA2MzkgY29kZXNcclxuXHRcdFx0XHRibzogJ+C8oOC8oeC8ouC8o+C8pOC8peC8puC8p+C8qOC8qScgLy8gRklYTUUgdXNlIGlzbyA2MzkgY29kZXNcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdGlmICggIXRhYmxlc1sgbGFuZ3VhZ2UgXSApIHtcclxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJldHVybiB0YWJsZXNbIGxhbmd1YWdlIF0uc3BsaXQoICcnICk7XHJcblx0XHR9XHJcblx0fTtcclxuXHJcblx0JC5leHRlbmQoICQuaTE4bi5sYW5ndWFnZXMsIHtcclxuXHRcdCdkZWZhdWx0JzogbGFuZ3VhZ2VcclxuXHR9ICk7XHJcbn0oIGpRdWVyeSApICk7XHJcbiIsIi8qIVxyXG4gKiBqUXVlcnkgSW50ZXJuYXRpb25hbGl6YXRpb24gbGlicmFyeSAtIE1lc3NhZ2UgU3RvcmVcclxuICpcclxuICogQ29weXJpZ2h0IChDKSAyMDEyIFNhbnRob3NoIFRob3R0aW5nYWxcclxuICpcclxuICoganF1ZXJ5LmkxOG4gaXMgZHVhbCBsaWNlbnNlZCBHUEx2MiBvciBsYXRlciBhbmQgTUlULiBZb3UgZG9uJ3QgaGF2ZSB0byBkbyBhbnl0aGluZyBzcGVjaWFsIHRvXHJcbiAqIGNob29zZSBvbmUgbGljZW5zZSBvciB0aGUgb3RoZXIgYW5kIHlvdSBkb24ndCBoYXZlIHRvIG5vdGlmeSBhbnlvbmUgd2hpY2ggbGljZW5zZSB5b3UgYXJlIHVzaW5nLlxyXG4gKiBZb3UgYXJlIGZyZWUgdG8gdXNlIFVuaXZlcnNhbExhbmd1YWdlU2VsZWN0b3IgaW4gY29tbWVyY2lhbCBwcm9qZWN0cyBhcyBsb25nIGFzIHRoZSBjb3B5cmlnaHRcclxuICogaGVhZGVyIGlzIGxlZnQgaW50YWN0LiBTZWUgZmlsZXMgR1BMLUxJQ0VOU0UgYW5kIE1JVC1MSUNFTlNFIGZvciBkZXRhaWxzLlxyXG4gKlxyXG4gKiBAbGljZW5jZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5jZSAyLjAgb3IgbGF0ZXJcclxuICogQGxpY2VuY2UgTUlUIExpY2Vuc2VcclxuICovXHJcblxyXG4oIGZ1bmN0aW9uICggJCApIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblxyXG5cdHZhciBNZXNzYWdlU3RvcmUgPSBmdW5jdGlvbiAoKSB7XHJcblx0XHR0aGlzLm1lc3NhZ2VzID0ge307XHJcblx0XHR0aGlzLnNvdXJjZXMgPSB7fTtcclxuXHR9O1xyXG5cclxuXHRmdW5jdGlvbiBqc29uTWVzc2FnZUxvYWRlciggdXJsICkge1xyXG5cdFx0dmFyIGRlZmVycmVkID0gJC5EZWZlcnJlZCgpO1xyXG5cclxuXHRcdCQuZ2V0SlNPTiggdXJsIClcclxuXHRcdFx0LmRvbmUoIGRlZmVycmVkLnJlc29sdmUgKVxyXG5cdFx0XHQuZmFpbCggZnVuY3Rpb24gKCBqcXhociwgc2V0dGluZ3MsIGV4Y2VwdGlvbiApIHtcclxuXHRcdFx0XHQkLmkxOG4ubG9nKCAnRXJyb3IgaW4gbG9hZGluZyBtZXNzYWdlcyBmcm9tICcgKyB1cmwgKyAnIEV4Y2VwdGlvbjogJyArIGV4Y2VwdGlvbiApO1xyXG5cdFx0XHRcdC8vIElnbm9yZSA0MDQgZXhjZXB0aW9uLCBiZWNhdXNlIHdlIGFyZSBoYW5kbGluZyBmYWxsYWJhY2tzIGV4cGxpY2l0bHlcclxuXHRcdFx0XHRkZWZlcnJlZC5yZXNvbHZlKCk7XHJcblx0XHRcdH0gKTtcclxuXHJcblx0XHRyZXR1cm4gZGVmZXJyZWQucHJvbWlzZSgpO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS93aWtpbWVkaWEvanF1ZXJ5LmkxOG4vd2lraS9TcGVjaWZpY2F0aW9uI3dpa2ktTWVzc2FnZV9GaWxlX0xvYWRpbmdcclxuXHQgKi9cclxuXHRNZXNzYWdlU3RvcmUucHJvdG90eXBlID0ge1xyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogR2VuZXJhbCBtZXNzYWdlIGxvYWRpbmcgQVBJIFRoaXMgY2FuIHRha2UgYSBVUkwgc3RyaW5nIGZvclxyXG5cdFx0ICogdGhlIGpzb24gZm9ybWF0dGVkIG1lc3NhZ2VzLlxyXG5cdFx0ICogPGNvZGU+bG9hZCgncGF0aC90by9hbGxfbG9jYWxpemF0aW9ucy5qc29uJyk7PC9jb2RlPlxyXG5cdFx0ICpcclxuXHRcdCAqIFRoaXMgY2FuIGFsc28gbG9hZCBhIGxvY2FsaXphdGlvbiBmaWxlIGZvciBhIGxvY2FsZSA8Y29kZT5cclxuXHRcdCAqIGxvYWQoICdwYXRoL3RvL2RlLW1lc3NhZ2VzLmpzb24nLCAnZGUnICk7XHJcblx0XHQgKiA8L2NvZGU+XHJcblx0XHQgKiBBIGRhdGEgb2JqZWN0IGNvbnRhaW5pbmcgbWVzc2FnZSBrZXktIG1lc3NhZ2UgdHJhbnNsYXRpb24gbWFwcGluZ3NcclxuXHRcdCAqIGNhbiBhbHNvIGJlIHBhc3NlZCBFZzpcclxuXHRcdCAqIDxjb2RlPlxyXG5cdFx0ICogbG9hZCggeyAnaGVsbG8nIDogJ0hlbGxvJyB9LCBvcHRpb25hbExvY2FsZSApO1xyXG5cdFx0ICogPC9jb2RlPiBJZiB0aGUgZGF0YSBhcmd1bWVudCBpc1xyXG5cdFx0ICogbnVsbC91bmRlZmluZWQvZmFsc2UsXHJcblx0XHQgKiBhbGwgY2FjaGVkIG1lc3NhZ2VzIGZvciB0aGUgaTE4biBpbnN0YW5jZSB3aWxsIGdldCByZXNldC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge3N0cmluZ3xPYmplY3R9IHNvdXJjZVxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IGxvY2FsZSBMYW5ndWFnZSB0YWdcclxuXHRcdCAqIEByZXR1cm4ge2pRdWVyeS5Qcm9taXNlfVxyXG5cdFx0ICovXHJcblx0XHRsb2FkOiBmdW5jdGlvbiAoIHNvdXJjZSwgbG9jYWxlICkge1xyXG5cdFx0XHR2YXIga2V5ID0gbnVsbCxcclxuXHRcdFx0XHRkZWZlcnJlZCA9IG51bGwsXHJcblx0XHRcdFx0ZGVmZXJyZWRzID0gW10sXHJcblx0XHRcdFx0bWVzc2FnZVN0b3JlID0gdGhpcztcclxuXHJcblx0XHRcdGlmICggdHlwZW9mIHNvdXJjZSA9PT0gJ3N0cmluZycgKSB7XHJcblx0XHRcdFx0Ly8gVGhpcyBpcyBhIFVSTCB0byB0aGUgbWVzc2FnZXMgZmlsZS5cclxuXHRcdFx0XHQkLmkxOG4ubG9nKCAnTG9hZGluZyBtZXNzYWdlcyBmcm9tOiAnICsgc291cmNlICk7XHJcblx0XHRcdFx0ZGVmZXJyZWQgPSBqc29uTWVzc2FnZUxvYWRlciggc291cmNlIClcclxuXHRcdFx0XHRcdC5kb25lKCBmdW5jdGlvbiAoIGxvY2FsaXphdGlvbiApIHtcclxuXHRcdFx0XHRcdFx0bWVzc2FnZVN0b3JlLnNldCggbG9jYWxlLCBsb2NhbGl6YXRpb24gKTtcclxuXHRcdFx0XHRcdH0gKTtcclxuXHJcblx0XHRcdFx0cmV0dXJuIGRlZmVycmVkLnByb21pc2UoKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYgKCBsb2NhbGUgKSB7XHJcblx0XHRcdFx0Ly8gc291cmNlIGlzIGFuIGtleS12YWx1ZSBwYWlyIG9mIG1lc3NhZ2VzIGZvciBnaXZlbiBsb2NhbGVcclxuXHRcdFx0XHRtZXNzYWdlU3RvcmUuc2V0KCBsb2NhbGUsIHNvdXJjZSApO1xyXG5cclxuXHRcdFx0XHRyZXR1cm4gJC5EZWZlcnJlZCgpLnJlc29sdmUoKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHQvLyBzb3VyY2UgaXMgYSBrZXktdmFsdWUgcGFpciBvZiBsb2NhbGVzIGFuZCB0aGVpciBzb3VyY2VcclxuXHRcdFx0XHRmb3IgKCBrZXkgaW4gc291cmNlICkge1xyXG5cdFx0XHRcdFx0aWYgKCBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoIHNvdXJjZSwga2V5ICkgKSB7XHJcblx0XHRcdFx0XHRcdGxvY2FsZSA9IGtleTtcclxuXHRcdFx0XHRcdFx0Ly8gTm8ge2xvY2FsZX0gZ2l2ZW4sIGFzc3VtZSBkYXRhIGlzIGEgZ3JvdXAgb2YgbGFuZ3VhZ2VzLFxyXG5cdFx0XHRcdFx0XHQvLyBjYWxsIHRoaXMgZnVuY3Rpb24gYWdhaW4gZm9yIGVhY2ggbGFuZ3VhZ2UuXHJcblx0XHRcdFx0XHRcdGRlZmVycmVkcy5wdXNoKCBtZXNzYWdlU3RvcmUubG9hZCggc291cmNlWyBrZXkgXSwgbG9jYWxlICkgKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuICQud2hlbi5hcHBseSggJCwgZGVmZXJyZWRzICk7XHJcblx0XHRcdH1cclxuXHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogU2V0IG1lc3NhZ2VzIHRvIHRoZSBnaXZlbiBsb2NhbGUuXHJcblx0XHQgKiBJZiBsb2NhbGUgZXhpc3RzLCBhZGQgbWVzc2FnZXMgdG8gdGhlIGxvY2FsZS5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gbG9jYWxlXHJcblx0XHQgKiBAcGFyYW0ge09iamVjdH0gbWVzc2FnZXNcclxuXHRcdCAqL1xyXG5cdFx0c2V0OiBmdW5jdGlvbiAoIGxvY2FsZSwgbWVzc2FnZXMgKSB7XHJcblx0XHRcdGlmICggIXRoaXMubWVzc2FnZXNbIGxvY2FsZSBdICkge1xyXG5cdFx0XHRcdHRoaXMubWVzc2FnZXNbIGxvY2FsZSBdID0gbWVzc2FnZXM7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0dGhpcy5tZXNzYWdlc1sgbG9jYWxlIF0gPSAkLmV4dGVuZCggdGhpcy5tZXNzYWdlc1sgbG9jYWxlIF0sIG1lc3NhZ2VzICk7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblxyXG5cdFx0LyoqXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IGxvY2FsZVxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IG1lc3NhZ2VLZXlcclxuXHRcdCAqIEByZXR1cm4ge2Jvb2xlYW59XHJcblx0XHQgKi9cclxuXHRcdGdldDogZnVuY3Rpb24gKCBsb2NhbGUsIG1lc3NhZ2VLZXkgKSB7XHJcblx0XHRcdHJldHVybiB0aGlzLm1lc3NhZ2VzWyBsb2NhbGUgXSAmJiB0aGlzLm1lc3NhZ2VzWyBsb2NhbGUgXVsgbWVzc2FnZUtleSBdO1xyXG5cdFx0fVxyXG5cdH07XHJcblxyXG5cdCQuZXh0ZW5kKCAkLmkxOG4ubWVzc2FnZVN0b3JlLCBuZXcgTWVzc2FnZVN0b3JlKCkgKTtcclxufSggalF1ZXJ5ICkgKTtcclxuIiwiLyohXHJcbiAqIGpRdWVyeSBJbnRlcm5hdGlvbmFsaXphdGlvbiBsaWJyYXJ5XHJcbiAqXHJcbiAqIENvcHlyaWdodCAoQykgMjAxMS0yMDEzIFNhbnRob3NoIFRob3R0aW5nYWwsIE5laWwgS2FuZGFsZ2FvbmthclxyXG4gKlxyXG4gKiBqcXVlcnkuaTE4biBpcyBkdWFsIGxpY2Vuc2VkIEdQTHYyIG9yIGxhdGVyIGFuZCBNSVQuIFlvdSBkb24ndCBoYXZlIHRvIGRvXHJcbiAqIGFueXRoaW5nIHNwZWNpYWwgdG8gY2hvb3NlIG9uZSBsaWNlbnNlIG9yIHRoZSBvdGhlciBhbmQgeW91IGRvbid0IGhhdmUgdG9cclxuICogbm90aWZ5IGFueW9uZSB3aGljaCBsaWNlbnNlIHlvdSBhcmUgdXNpbmcuIFlvdSBhcmUgZnJlZSB0byB1c2VcclxuICogVW5pdmVyc2FsTGFuZ3VhZ2VTZWxlY3RvciBpbiBjb21tZXJjaWFsIHByb2plY3RzIGFzIGxvbmcgYXMgdGhlIGNvcHlyaWdodFxyXG4gKiBoZWFkZXIgaXMgbGVmdCBpbnRhY3QuIFNlZSBmaWxlcyBHUEwtTElDRU5TRSBhbmQgTUlULUxJQ0VOU0UgZm9yIGRldGFpbHMuXHJcbiAqXHJcbiAqIEBsaWNlbmNlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbmNlIDIuMCBvciBsYXRlclxyXG4gKiBAbGljZW5jZSBNSVQgTGljZW5zZVxyXG4gKi9cclxuXHJcbiggZnVuY3Rpb24gKCAkICkge1xyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHJcblx0dmFyIE1lc3NhZ2VQYXJzZXIgPSBmdW5jdGlvbiAoIG9wdGlvbnMgKSB7XHJcblx0XHR0aGlzLm9wdGlvbnMgPSAkLmV4dGVuZCgge30sICQuaTE4bi5wYXJzZXIuZGVmYXVsdHMsIG9wdGlvbnMgKTtcclxuXHRcdHRoaXMubGFuZ3VhZ2UgPSAkLmkxOG4ubGFuZ3VhZ2VzWyBTdHJpbmcubG9jYWxlIF0gfHwgJC5pMThuLmxhbmd1YWdlc1sgJ2RlZmF1bHQnIF07XHJcblx0XHR0aGlzLmVtaXR0ZXIgPSAkLmkxOG4ucGFyc2VyLmVtaXR0ZXI7XHJcblx0fTtcclxuXHJcblx0TWVzc2FnZVBhcnNlci5wcm90b3R5cGUgPSB7XHJcblxyXG5cdFx0Y29uc3RydWN0b3I6IE1lc3NhZ2VQYXJzZXIsXHJcblxyXG5cdFx0c2ltcGxlUGFyc2U6IGZ1bmN0aW9uICggbWVzc2FnZSwgcGFyYW1ldGVycyApIHtcclxuXHRcdFx0cmV0dXJuIG1lc3NhZ2UucmVwbGFjZSggL1xcJChcXGQrKS9nLCBmdW5jdGlvbiAoIHN0ciwgbWF0Y2ggKSB7XHJcblx0XHRcdFx0dmFyIGluZGV4ID0gcGFyc2VJbnQoIG1hdGNoLCAxMCApIC0gMTtcclxuXHJcblx0XHRcdFx0cmV0dXJuIHBhcmFtZXRlcnNbIGluZGV4IF0gIT09IHVuZGVmaW5lZCA/IHBhcmFtZXRlcnNbIGluZGV4IF0gOiAnJCcgKyBtYXRjaDtcclxuXHRcdFx0fSApO1xyXG5cdFx0fSxcclxuXHJcblx0XHRwYXJzZTogZnVuY3Rpb24gKCBtZXNzYWdlLCByZXBsYWNlbWVudHMgKSB7XHJcblx0XHRcdGlmICggbWVzc2FnZS5pbmRleE9mKCAne3snICkgPCAwICkge1xyXG5cdFx0XHRcdHJldHVybiB0aGlzLnNpbXBsZVBhcnNlKCBtZXNzYWdlLCByZXBsYWNlbWVudHMgKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0dGhpcy5lbWl0dGVyLmxhbmd1YWdlID0gJC5pMThuLmxhbmd1YWdlc1sgJC5pMThuKCkubG9jYWxlIF0gfHxcclxuXHRcdFx0XHQkLmkxOG4ubGFuZ3VhZ2VzWyAnZGVmYXVsdCcgXTtcclxuXHJcblx0XHRcdHJldHVybiB0aGlzLmVtaXR0ZXIuZW1pdCggdGhpcy5hc3QoIG1lc3NhZ2UgKSwgcmVwbGFjZW1lbnRzICk7XHJcblx0XHR9LFxyXG5cclxuXHRcdGFzdDogZnVuY3Rpb24gKCBtZXNzYWdlICkge1xyXG5cdFx0XHR2YXIgcGlwZSwgY29sb24sIGJhY2tzbGFzaCwgYW55Q2hhcmFjdGVyLCBkb2xsYXIsIGRpZ2l0cywgcmVndWxhckxpdGVyYWwsXHJcblx0XHRcdFx0cmVndWxhckxpdGVyYWxXaXRob3V0QmFyLCByZWd1bGFyTGl0ZXJhbFdpdGhvdXRTcGFjZSwgZXNjYXBlZE9yTGl0ZXJhbFdpdGhvdXRCYXIsXHJcblx0XHRcdFx0ZXNjYXBlZE9yUmVndWxhckxpdGVyYWwsIHRlbXBsYXRlQ29udGVudHMsIHRlbXBsYXRlTmFtZSwgb3BlblRlbXBsYXRlLFxyXG5cdFx0XHRcdGNsb3NlVGVtcGxhdGUsIGV4cHJlc3Npb24sIHBhcmFtRXhwcmVzc2lvbiwgcmVzdWx0LFxyXG5cdFx0XHRcdHBvcyA9IDA7XHJcblxyXG5cdFx0XHQvLyBUcnkgcGFyc2VycyB1bnRpbCBvbmUgd29ya3MsIGlmIG5vbmUgd29yayByZXR1cm4gbnVsbFxyXG5cdFx0XHRmdW5jdGlvbiBjaG9pY2UoIHBhcnNlclN5bnRheCApIHtcclxuXHRcdFx0XHRyZXR1cm4gZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0dmFyIGksIHJlc3VsdDtcclxuXHJcblx0XHRcdFx0XHRmb3IgKCBpID0gMDsgaSA8IHBhcnNlclN5bnRheC5sZW5ndGg7IGkrKyApIHtcclxuXHRcdFx0XHRcdFx0cmVzdWx0ID0gcGFyc2VyU3ludGF4WyBpIF0oKTtcclxuXHJcblx0XHRcdFx0XHRcdGlmICggcmVzdWx0ICE9PSBudWxsICkge1xyXG5cdFx0XHRcdFx0XHRcdHJldHVybiByZXN1bHQ7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRyZXR1cm4gbnVsbDtcclxuXHRcdFx0XHR9O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQvLyBUcnkgc2V2ZXJhbCBwYXJzZXJTeW50YXgtZXMgaW4gYSByb3cuXHJcblx0XHRcdC8vIEFsbCBtdXN0IHN1Y2NlZWQ7IG90aGVyd2lzZSwgcmV0dXJuIG51bGwuXHJcblx0XHRcdC8vIFRoaXMgaXMgdGhlIG9ubHkgZWFnZXIgb25lLlxyXG5cdFx0XHRmdW5jdGlvbiBzZXF1ZW5jZSggcGFyc2VyU3ludGF4ICkge1xyXG5cdFx0XHRcdHZhciBpLCByZXMsXHJcblx0XHRcdFx0XHRvcmlnaW5hbFBvcyA9IHBvcyxcclxuXHRcdFx0XHRcdHJlc3VsdCA9IFtdO1xyXG5cclxuXHRcdFx0XHRmb3IgKCBpID0gMDsgaSA8IHBhcnNlclN5bnRheC5sZW5ndGg7IGkrKyApIHtcclxuXHRcdFx0XHRcdHJlcyA9IHBhcnNlclN5bnRheFsgaSBdKCk7XHJcblxyXG5cdFx0XHRcdFx0aWYgKCByZXMgPT09IG51bGwgKSB7XHJcblx0XHRcdFx0XHRcdHBvcyA9IG9yaWdpbmFsUG9zO1xyXG5cclxuXHRcdFx0XHRcdFx0cmV0dXJuIG51bGw7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0cmVzdWx0LnB1c2goIHJlcyApO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0Ly8gUnVuIHRoZSBzYW1lIHBhcnNlciBvdmVyIGFuZCBvdmVyIHVudGlsIGl0IGZhaWxzLlxyXG5cdFx0XHQvLyBNdXN0IHN1Y2NlZWQgYSBtaW5pbXVtIG9mIG4gdGltZXM7IG90aGVyd2lzZSwgcmV0dXJuIG51bGwuXHJcblx0XHRcdGZ1bmN0aW9uIG5Pck1vcmUoIG4sIHAgKSB7XHJcblx0XHRcdFx0cmV0dXJuIGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRcdHZhciBvcmlnaW5hbFBvcyA9IHBvcyxcclxuXHRcdFx0XHRcdFx0cmVzdWx0ID0gW10sXHJcblx0XHRcdFx0XHRcdHBhcnNlZCA9IHAoKTtcclxuXHJcblx0XHRcdFx0XHR3aGlsZSAoIHBhcnNlZCAhPT0gbnVsbCApIHtcclxuXHRcdFx0XHRcdFx0cmVzdWx0LnB1c2goIHBhcnNlZCApO1xyXG5cdFx0XHRcdFx0XHRwYXJzZWQgPSBwKCk7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0aWYgKCByZXN1bHQubGVuZ3RoIDwgbiApIHtcclxuXHRcdFx0XHRcdFx0cG9zID0gb3JpZ2luYWxQb3M7XHJcblxyXG5cdFx0XHRcdFx0XHRyZXR1cm4gbnVsbDtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdFx0XHRcdH07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdC8vIEhlbHBlcnMgLS0ganVzdCBtYWtlIHBhcnNlclN5bnRheCBvdXQgb2Ygc2ltcGxlciBKUyBidWlsdGluIHR5cGVzXHJcblxyXG5cdFx0XHRmdW5jdGlvbiBtYWtlU3RyaW5nUGFyc2VyKCBzICkge1xyXG5cdFx0XHRcdHZhciBsZW4gPSBzLmxlbmd0aDtcclxuXHJcblx0XHRcdFx0cmV0dXJuIGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRcdHZhciByZXN1bHQgPSBudWxsO1xyXG5cclxuXHRcdFx0XHRcdGlmICggbWVzc2FnZS5zbGljZSggcG9zLCBwb3MgKyBsZW4gKSA9PT0gcyApIHtcclxuXHRcdFx0XHRcdFx0cmVzdWx0ID0gcztcclxuXHRcdFx0XHRcdFx0cG9zICs9IGxlbjtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdFx0XHRcdH07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGZ1bmN0aW9uIG1ha2VSZWdleFBhcnNlciggcmVnZXggKSB7XHJcblx0XHRcdFx0cmV0dXJuIGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRcdHZhciBtYXRjaGVzID0gbWVzc2FnZS5zbGljZSggcG9zICkubWF0Y2goIHJlZ2V4ICk7XHJcblxyXG5cdFx0XHRcdFx0aWYgKCBtYXRjaGVzID09PSBudWxsICkge1xyXG5cdFx0XHRcdFx0XHRyZXR1cm4gbnVsbDtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRwb3MgKz0gbWF0Y2hlc1sgMCBdLmxlbmd0aDtcclxuXHJcblx0XHRcdFx0XHRyZXR1cm4gbWF0Y2hlc1sgMCBdO1xyXG5cdFx0XHRcdH07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHBpcGUgPSBtYWtlU3RyaW5nUGFyc2VyKCAnfCcgKTtcclxuXHRcdFx0Y29sb24gPSBtYWtlU3RyaW5nUGFyc2VyKCAnOicgKTtcclxuXHRcdFx0YmFja3NsYXNoID0gbWFrZVN0cmluZ1BhcnNlciggJ1xcXFwnICk7XHJcblx0XHRcdGFueUNoYXJhY3RlciA9IG1ha2VSZWdleFBhcnNlciggL14uLyApO1xyXG5cdFx0XHRkb2xsYXIgPSBtYWtlU3RyaW5nUGFyc2VyKCAnJCcgKTtcclxuXHRcdFx0ZGlnaXRzID0gbWFrZVJlZ2V4UGFyc2VyKCAvXlxcZCsvICk7XHJcblx0XHRcdHJlZ3VsYXJMaXRlcmFsID0gbWFrZVJlZ2V4UGFyc2VyKCAvXltee31cXFtcXF0kXFxcXF0vICk7XHJcblx0XHRcdHJlZ3VsYXJMaXRlcmFsV2l0aG91dEJhciA9IG1ha2VSZWdleFBhcnNlciggL15bXnt9XFxbXFxdJFxcXFx8XS8gKTtcclxuXHRcdFx0cmVndWxhckxpdGVyYWxXaXRob3V0U3BhY2UgPSBtYWtlUmVnZXhQYXJzZXIoIC9eW157fVxcW1xcXSRcXHNdLyApO1xyXG5cclxuXHRcdFx0Ly8gVGhlcmUgaXMgYSBnZW5lcmFsIHBhdHRlcm46XHJcblx0XHRcdC8vIHBhcnNlIGEgdGhpbmc7XHJcblx0XHRcdC8vIGlmIGl0IHdvcmtlZCwgYXBwbHkgdHJhbnNmb3JtLFxyXG5cdFx0XHQvLyBvdGhlcndpc2UgcmV0dXJuIG51bGwuXHJcblx0XHRcdC8vIEJ1dCB1c2luZyB0aGlzIGFzIGEgY29tYmluYXRvciBzZWVtcyB0byBjYXVzZSBwcm9ibGVtc1xyXG5cdFx0XHQvLyB3aGVuIGNvbWJpbmVkIHdpdGggbk9yTW9yZSgpLlxyXG5cdFx0XHQvLyBNYXkgYmUgc29tZSBzY29waW5nIGlzc3VlLlxyXG5cdFx0XHRmdW5jdGlvbiB0cmFuc2Zvcm0oIHAsIGZuICkge1xyXG5cdFx0XHRcdHJldHVybiBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHR2YXIgcmVzdWx0ID0gcCgpO1xyXG5cclxuXHRcdFx0XHRcdHJldHVybiByZXN1bHQgPT09IG51bGwgPyBudWxsIDogZm4oIHJlc3VsdCApO1xyXG5cdFx0XHRcdH07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdC8vIFVzZWQgdG8gZGVmaW5lIFwibGl0ZXJhbHNcIiB3aXRoaW4gdGVtcGxhdGUgcGFyYW1ldGVycy4gVGhlIHBpcGVcclxuXHRcdFx0Ly8gY2hhcmFjdGVyIGlzIHRoZSBwYXJhbWV0ZXIgZGVsaW1ldGVyLCBzbyBieSBkZWZhdWx0XHJcblx0XHRcdC8vIGl0IGlzIG5vdCBhIGxpdGVyYWwgaW4gdGhlIHBhcmFtZXRlclxyXG5cdFx0XHRmdW5jdGlvbiBsaXRlcmFsV2l0aG91dEJhcigpIHtcclxuXHRcdFx0XHR2YXIgcmVzdWx0ID0gbk9yTW9yZSggMSwgZXNjYXBlZE9yTGl0ZXJhbFdpdGhvdXRCYXIgKSgpO1xyXG5cclxuXHRcdFx0XHRyZXR1cm4gcmVzdWx0ID09PSBudWxsID8gbnVsbCA6IHJlc3VsdC5qb2luKCAnJyApO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRmdW5jdGlvbiBsaXRlcmFsKCkge1xyXG5cdFx0XHRcdHZhciByZXN1bHQgPSBuT3JNb3JlKCAxLCBlc2NhcGVkT3JSZWd1bGFyTGl0ZXJhbCApKCk7XHJcblxyXG5cdFx0XHRcdHJldHVybiByZXN1bHQgPT09IG51bGwgPyBudWxsIDogcmVzdWx0LmpvaW4oICcnICk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGZ1bmN0aW9uIGVzY2FwZWRMaXRlcmFsKCkge1xyXG5cdFx0XHRcdHZhciByZXN1bHQgPSBzZXF1ZW5jZSggWyBiYWNrc2xhc2gsIGFueUNoYXJhY3RlciBdICk7XHJcblxyXG5cdFx0XHRcdHJldHVybiByZXN1bHQgPT09IG51bGwgPyBudWxsIDogcmVzdWx0WyAxIF07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGNob2ljZSggWyBlc2NhcGVkTGl0ZXJhbCwgcmVndWxhckxpdGVyYWxXaXRob3V0U3BhY2UgXSApO1xyXG5cdFx0XHRlc2NhcGVkT3JMaXRlcmFsV2l0aG91dEJhciA9IGNob2ljZSggWyBlc2NhcGVkTGl0ZXJhbCwgcmVndWxhckxpdGVyYWxXaXRob3V0QmFyIF0gKTtcclxuXHRcdFx0ZXNjYXBlZE9yUmVndWxhckxpdGVyYWwgPSBjaG9pY2UoIFsgZXNjYXBlZExpdGVyYWwsIHJlZ3VsYXJMaXRlcmFsIF0gKTtcclxuXHJcblx0XHRcdGZ1bmN0aW9uIHJlcGxhY2VtZW50KCkge1xyXG5cdFx0XHRcdHZhciByZXN1bHQgPSBzZXF1ZW5jZSggWyBkb2xsYXIsIGRpZ2l0cyBdICk7XHJcblxyXG5cdFx0XHRcdGlmICggcmVzdWx0ID09PSBudWxsICkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIG51bGw7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRyZXR1cm4gWyAnUkVQTEFDRScsIHBhcnNlSW50KCByZXN1bHRbIDEgXSwgMTAgKSAtIDEgXTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0dGVtcGxhdGVOYW1lID0gdHJhbnNmb3JtKFxyXG5cdFx0XHRcdC8vIHNlZSAkd2dMZWdhbFRpdGxlQ2hhcnNcclxuXHRcdFx0XHQvLyBub3QgYWxsb3dpbmcgOiBkdWUgdG8gdGhlIG5lZWQgdG8gY2F0Y2ggXCJQTFVSQUw6JDFcIlxyXG5cdFx0XHRcdG1ha2VSZWdleFBhcnNlciggL15bICFcIiQmJygpKiwuXFwvMC05Oz0/QEEtWlxcXl9gYS16flxceDgwLVxceEZGK1xcLV0rLyApLFxyXG5cclxuXHRcdFx0XHRmdW5jdGlvbiAoIHJlc3VsdCApIHtcclxuXHRcdFx0XHRcdHJldHVybiByZXN1bHQudG9TdHJpbmcoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdCk7XHJcblxyXG5cdFx0XHRmdW5jdGlvbiB0ZW1wbGF0ZVBhcmFtKCkge1xyXG5cdFx0XHRcdHZhciBleHByLFxyXG5cdFx0XHRcdFx0cmVzdWx0ID0gc2VxdWVuY2UoIFsgcGlwZSwgbk9yTW9yZSggMCwgcGFyYW1FeHByZXNzaW9uICkgXSApO1xyXG5cclxuXHRcdFx0XHRpZiAoIHJlc3VsdCA9PT0gbnVsbCApIHtcclxuXHRcdFx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0ZXhwciA9IHJlc3VsdFsgMSBdO1xyXG5cclxuXHRcdFx0XHQvLyB1c2UgYSBcIkNPTkNBVFwiIG9wZXJhdG9yIGlmIHRoZXJlIGFyZSBtdWx0aXBsZSBub2RlcyxcclxuXHRcdFx0XHQvLyBvdGhlcndpc2UgcmV0dXJuIHRoZSBmaXJzdCBub2RlLCByYXcuXHJcblx0XHRcdFx0cmV0dXJuIGV4cHIubGVuZ3RoID4gMSA/IFsgJ0NPTkNBVCcgXS5jb25jYXQoIGV4cHIgKSA6IGV4cHJbIDAgXTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0ZnVuY3Rpb24gdGVtcGxhdGVXaXRoUmVwbGFjZW1lbnQoKSB7XHJcblx0XHRcdFx0dmFyIHJlc3VsdCA9IHNlcXVlbmNlKCBbIHRlbXBsYXRlTmFtZSwgY29sb24sIHJlcGxhY2VtZW50IF0gKTtcclxuXHJcblx0XHRcdFx0cmV0dXJuIHJlc3VsdCA9PT0gbnVsbCA/IG51bGwgOiBbIHJlc3VsdFsgMCBdLCByZXN1bHRbIDIgXSBdO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRmdW5jdGlvbiB0ZW1wbGF0ZVdpdGhPdXRSZXBsYWNlbWVudCgpIHtcclxuXHRcdFx0XHR2YXIgcmVzdWx0ID0gc2VxdWVuY2UoIFsgdGVtcGxhdGVOYW1lLCBjb2xvbiwgcGFyYW1FeHByZXNzaW9uIF0gKTtcclxuXHJcblx0XHRcdFx0cmV0dXJuIHJlc3VsdCA9PT0gbnVsbCA/IG51bGwgOiBbIHJlc3VsdFsgMCBdLCByZXN1bHRbIDIgXSBdO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHR0ZW1wbGF0ZUNvbnRlbnRzID0gY2hvaWNlKCBbXHJcblx0XHRcdFx0ZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0dmFyIHJlcyA9IHNlcXVlbmNlKCBbXHJcblx0XHRcdFx0XHRcdC8vIHRlbXBsYXRlcyBjYW4gaGF2ZSBwbGFjZWhvbGRlcnMgZm9yIGR5bmFtaWNcclxuXHRcdFx0XHRcdFx0Ly8gcmVwbGFjZW1lbnQgZWc6IHt7UExVUkFMOiQxfG9uZSBjYXJ8JDEgY2Fyc319XHJcblx0XHRcdFx0XHRcdC8vIG9yIG5vIHBsYWNlaG9sZGVycyBlZzpcclxuXHRcdFx0XHRcdFx0Ly8ge3tHUkFNTUFSOmdlbml0aXZlfHt7U0lURU5BTUV9fX1cclxuXHRcdFx0XHRcdFx0Y2hvaWNlKCBbIHRlbXBsYXRlV2l0aFJlcGxhY2VtZW50LCB0ZW1wbGF0ZVdpdGhPdXRSZXBsYWNlbWVudCBdICksXHJcblx0XHRcdFx0XHRcdG5Pck1vcmUoIDAsIHRlbXBsYXRlUGFyYW0gKVxyXG5cdFx0XHRcdFx0XSApO1xyXG5cclxuXHRcdFx0XHRcdHJldHVybiByZXMgPT09IG51bGwgPyBudWxsIDogcmVzWyAwIF0uY29uY2F0KCByZXNbIDEgXSApO1xyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0ZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0dmFyIHJlcyA9IHNlcXVlbmNlKCBbIHRlbXBsYXRlTmFtZSwgbk9yTW9yZSggMCwgdGVtcGxhdGVQYXJhbSApIF0gKTtcclxuXHJcblx0XHRcdFx0XHRpZiAoIHJlcyA9PT0gbnVsbCApIHtcclxuXHRcdFx0XHRcdFx0cmV0dXJuIG51bGw7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0cmV0dXJuIFsgcmVzWyAwIF0gXS5jb25jYXQoIHJlc1sgMSBdICk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRdICk7XHJcblxyXG5cdFx0XHRvcGVuVGVtcGxhdGUgPSBtYWtlU3RyaW5nUGFyc2VyKCAne3snICk7XHJcblx0XHRcdGNsb3NlVGVtcGxhdGUgPSBtYWtlU3RyaW5nUGFyc2VyKCAnfX0nICk7XHJcblxyXG5cdFx0XHRmdW5jdGlvbiB0ZW1wbGF0ZSgpIHtcclxuXHRcdFx0XHR2YXIgcmVzdWx0ID0gc2VxdWVuY2UoIFsgb3BlblRlbXBsYXRlLCB0ZW1wbGF0ZUNvbnRlbnRzLCBjbG9zZVRlbXBsYXRlIF0gKTtcclxuXHJcblx0XHRcdFx0cmV0dXJuIHJlc3VsdCA9PT0gbnVsbCA/IG51bGwgOiByZXN1bHRbIDEgXTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0ZXhwcmVzc2lvbiA9IGNob2ljZSggWyB0ZW1wbGF0ZSwgcmVwbGFjZW1lbnQsIGxpdGVyYWwgXSApO1xyXG5cdFx0XHRwYXJhbUV4cHJlc3Npb24gPSBjaG9pY2UoIFsgdGVtcGxhdGUsIHJlcGxhY2VtZW50LCBsaXRlcmFsV2l0aG91dEJhciBdICk7XHJcblxyXG5cdFx0XHRmdW5jdGlvbiBzdGFydCgpIHtcclxuXHRcdFx0XHR2YXIgcmVzdWx0ID0gbk9yTW9yZSggMCwgZXhwcmVzc2lvbiApKCk7XHJcblxyXG5cdFx0XHRcdGlmICggcmVzdWx0ID09PSBudWxsICkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIG51bGw7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRyZXR1cm4gWyAnQ09OQ0FUJyBdLmNvbmNhdCggcmVzdWx0ICk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJlc3VsdCA9IHN0YXJ0KCk7XHJcblxyXG5cdFx0XHQvKlxyXG5cdFx0XHQgKiBGb3Igc3VjY2VzcywgdGhlIHBvcyBtdXN0IGhhdmUgZ290dGVuIHRvIHRoZSBlbmQgb2YgdGhlIGlucHV0XHJcblx0XHRcdCAqIGFuZCByZXR1cm5lZCBhIG5vbi1udWxsLlxyXG5cdFx0XHQgKiBuLmIuIFRoaXMgaXMgcGFydCBvZiBsYW5ndWFnZSBpbmZyYXN0cnVjdHVyZSwgc28gd2UgZG8gbm90IHRocm93IGFuIGludGVybmF0aW9uYWxpemFibGUgbWVzc2FnZS5cclxuXHRcdFx0ICovXHJcblx0XHRcdGlmICggcmVzdWx0ID09PSBudWxsIHx8IHBvcyAhPT0gbWVzc2FnZS5sZW5ndGggKSB7XHJcblx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCAnUGFyc2UgZXJyb3IgYXQgcG9zaXRpb24gJyArIHBvcy50b1N0cmluZygpICsgJyBpbiBpbnB1dDogJyArIG1lc3NhZ2UgKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdH1cclxuXHJcblx0fTtcclxuXHJcblx0JC5leHRlbmQoICQuaTE4bi5wYXJzZXIsIG5ldyBNZXNzYWdlUGFyc2VyKCkgKTtcclxufSggalF1ZXJ5ICkgKTtcclxuIiwiLyohIGpzLXVybCAtIHYyLjUuMiAtIDIwMTctMDgtMzAgKi8hZnVuY3Rpb24oKXt2YXIgYT1mdW5jdGlvbigpe2Z1bmN0aW9uIGEoKXt9ZnVuY3Rpb24gYihhKXtyZXR1cm4gZGVjb2RlVVJJQ29tcG9uZW50KGEucmVwbGFjZSgvXFwrL2csXCIgXCIpKX1mdW5jdGlvbiBjKGEsYil7dmFyIGM9YS5jaGFyQXQoMCksZD1iLnNwbGl0KGMpO3JldHVybiBjPT09YT9kOihhPXBhcnNlSW50KGEuc3Vic3RyaW5nKDEpLDEwKSxkW2E8MD9kLmxlbmd0aCthOmEtMV0pfWZ1bmN0aW9uIGQoYSxjKXtmb3IodmFyIGQ9YS5jaGFyQXQoMCksZT1jLnNwbGl0KFwiJlwiKSxmPVtdLGc9e30saD1bXSxpPWEuc3Vic3RyaW5nKDEpLGo9MCxrPWUubGVuZ3RoO2o8aztqKyspaWYoZj1lW2pdLm1hdGNoKC8oLio/KT0oLiopLyksZnx8KGY9W2Vbal0sZVtqXSxcIlwiXSksXCJcIiE9PWZbMV0ucmVwbGFjZSgvXFxzL2csXCJcIikpe2lmKGZbMl09YihmWzJdfHxcIlwiKSxpPT09ZlsxXSlyZXR1cm4gZlsyXTtoPWZbMV0ubWF0Y2goLyguKilcXFsoWzAtOV0rKVxcXS8pLGg/KGdbaFsxXV09Z1toWzFdXXx8W10sZ1toWzFdXVtoWzJdXT1mWzJdKTpnW2ZbMV1dPWZbMl19cmV0dXJuIGQ9PT1hP2c6Z1tpXX1yZXR1cm4gZnVuY3Rpb24oYixlKXt2YXIgZixnPXt9O2lmKFwidGxkP1wiPT09YilyZXR1cm4gYSgpO2lmKGU9ZXx8d2luZG93LmxvY2F0aW9uLnRvU3RyaW5nKCksIWIpcmV0dXJuIGU7aWYoYj1iLnRvU3RyaW5nKCksZj1lLm1hdGNoKC9ebWFpbHRvOihbXlxcL10uKykvKSlnLnByb3RvY29sPVwibWFpbHRvXCIsZy5lbWFpbD1mWzFdO2Vsc2V7aWYoKGY9ZS5tYXRjaCgvKC4qPylcXC8jXFwhKC4qKS8pKSYmKGU9ZlsxXStmWzJdKSwoZj1lLm1hdGNoKC8oLio/KSMoLiopLykpJiYoZy5oYXNoPWZbMl0sZT1mWzFdKSxnLmhhc2gmJmIubWF0Y2goL14jLykpcmV0dXJuIGQoYixnLmhhc2gpO2lmKChmPWUubWF0Y2goLyguKj8pXFw/KC4qKS8pKSYmKGcucXVlcnk9ZlsyXSxlPWZbMV0pLGcucXVlcnkmJmIubWF0Y2goL15cXD8vKSlyZXR1cm4gZChiLGcucXVlcnkpO2lmKChmPWUubWF0Y2goLyguKj8pXFw6P1xcL1xcLyguKikvKSkmJihnLnByb3RvY29sPWZbMV0udG9Mb3dlckNhc2UoKSxlPWZbMl0pLChmPWUubWF0Y2goLyguKj8pKFxcLy4qKS8pKSYmKGcucGF0aD1mWzJdLGU9ZlsxXSksZy5wYXRoPShnLnBhdGh8fFwiXCIpLnJlcGxhY2UoL14oW15cXC9dKS8sXCIvJDFcIiksYi5tYXRjaCgvXltcXC0wLTldKyQvKSYmKGI9Yi5yZXBsYWNlKC9eKFteXFwvXSkvLFwiLyQxXCIpKSxiLm1hdGNoKC9eXFwvLykpcmV0dXJuIGMoYixnLnBhdGguc3Vic3RyaW5nKDEpKTtpZihmPWMoXCIvLTFcIixnLnBhdGguc3Vic3RyaW5nKDEpKSxmJiYoZj1mLm1hdGNoKC8oLio/KVxcLiguKikvKSkmJihnLmZpbGU9ZlswXSxnLmZpbGVuYW1lPWZbMV0sZy5maWxlZXh0PWZbMl0pLChmPWUubWF0Y2goLyguKilcXDooWzAtOV0rKSQvKSkmJihnLnBvcnQ9ZlsyXSxlPWZbMV0pLChmPWUubWF0Y2goLyguKj8pQCguKikvKSkmJihnLmF1dGg9ZlsxXSxlPWZbMl0pLGcuYXV0aCYmKGY9Zy5hdXRoLm1hdGNoKC8oLiopXFw6KC4qKS8pLGcudXNlcj1mP2ZbMV06Zy5hdXRoLGcucGFzcz1mP2ZbMl06dm9pZCAwKSxnLmhvc3RuYW1lPWUudG9Mb3dlckNhc2UoKSxcIi5cIj09PWIuY2hhckF0KDApKXJldHVybiBjKGIsZy5ob3N0bmFtZSk7YSgpJiYoZj1nLmhvc3RuYW1lLm1hdGNoKGEoKSksZiYmKGcudGxkPWZbM10sZy5kb21haW49ZlsyXT9mWzJdK1wiLlwiK2ZbM106dm9pZCAwLGcuc3ViPWZbMV18fHZvaWQgMCkpLGcucG9ydD1nLnBvcnR8fChcImh0dHBzXCI9PT1nLnByb3RvY29sP1wiNDQzXCI6XCI4MFwiKSxnLnByb3RvY29sPWcucHJvdG9jb2x8fChcIjQ0M1wiPT09Zy5wb3J0P1wiaHR0cHNcIjpcImh0dHBcIil9cmV0dXJuIGIgaW4gZz9nW2JdOlwie31cIj09PWI/Zzp2b2lkIDB9fSgpO1wiZnVuY3Rpb25cIj09dHlwZW9mIHdpbmRvdy5kZWZpbmUmJndpbmRvdy5kZWZpbmUuYW1kP3dpbmRvdy5kZWZpbmUoXCJqcy11cmxcIixbXSxmdW5jdGlvbigpe3JldHVybiBhfSk6KFwidW5kZWZpbmVkXCIhPXR5cGVvZiB3aW5kb3cualF1ZXJ5JiZ3aW5kb3cualF1ZXJ5LmV4dGVuZCh7dXJsOmZ1bmN0aW9uKGEsYil7cmV0dXJuIHdpbmRvdy51cmwoYSxiKX19KSx3aW5kb3cudXJsPWEpfSgpO1xyXG4iLCJtb2R1bGUuZXhwb3J0cy5zY29yZVJlZ2lzdGVyID0gZnVuY3Rpb24gU2NvcmVSZWdpc3RlcigpIHtcclxuICAgIHZhciB0aGF0ID0ge307XHJcblxyXG4gICAgdGhhdC5zZXRTdWJMZXZlbFNjb3JlID0gZnVuY3Rpb24obGV2ZWwsIHN1YkxldmVsLCBzdWJMZXZlbFNjb3JlKSB7XHJcbiAgICAgICAgdmFyIHNjb3JlcywgbGV2ZWxTY29yZXM7XHJcbiAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlW1wic2NvcmVzXCJdID09IG51bGwpIHtcclxuICAgICAgICAgICAgbGV2ZWxTY29yZXMgPSBbc3ViTGV2ZWxTY29yZV07XHJcbiAgICAgICAgICAgIHNjb3JlcyA9IHt9XHJcbiAgICAgICAgICAgIHNjb3Jlc1tsZXZlbF0gPSBsZXZlbFNjb3JlcztcclxuICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShcInNjb3Jlc1wiLCBKU09OLnN0cmluZ2lmeShzY29yZXMpKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzY29yZXMgPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJzY29yZXNcIikpO1xyXG4gICAgICAgICAgICBpZiAoc2NvcmVzW2xldmVsXSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICBsZXZlbFNjb3JlcyA9IFtzdWJMZXZlbFNjb3JlXTtcclxuICAgICAgICAgICAgICAgIHNjb3Jlc1tsZXZlbF0gPSBsZXZlbFNjb3JlcztcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHNjb3Jlc1tsZXZlbF1bc3ViTGV2ZWxdID0gc3ViTGV2ZWxTY29yZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFwic2NvcmVzXCIsIEpTT04uc3RyaW5naWZ5KHNjb3JlcykpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgdGhhdC5zZXRTdWJMZXZlbExpbmVzID0gZnVuY3Rpb24obGV2ZWwsIHN1YkxldmVsLCBzdWJMZXZlbExpbmVzKSB7XHJcbiAgICAgICAgdmFyIGxpbmVzLCBsZXZlbExpbmVzO1xyXG4gICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZVtcImxpbmVzXCJdID09IG51bGwpIHtcclxuICAgICAgICAgICAgbGV2ZWxMaW5lcyA9IFtzdWJMZXZlbExpbmVzXTtcclxuICAgICAgICAgICAgbGluZXMgPSB7fVxyXG4gICAgICAgICAgICBsaW5lc1tsZXZlbF0gPSBsZXZlbExpbmVzO1xyXG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFwibGluZXNcIiwgSlNPTi5zdHJpbmdpZnkobGluZXMpKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBsaW5lcyA9IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShcImxpbmVzXCIpKTtcclxuICAgICAgICAgICAgaWYgKGxpbmVzW2xldmVsXSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICBsZXZlbExpbmVzID0gW3N1YkxldmVsTGluZXNdO1xyXG4gICAgICAgICAgICAgICAgbGluZXNbbGV2ZWxdID0gbGV2ZWxMaW5lcztcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGxpbmVzW2xldmVsXVtzdWJMZXZlbF0gPSBzdWJMZXZlbExpbmVzO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oXCJsaW5lc1wiLCBKU09OLnN0cmluZ2lmeShsaW5lcykpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgdGhhdC5nZXRMZXZlbFNjb3JlID0gZnVuY3Rpb24obGV2ZWwpIHtcclxuICAgICAgICB2YXIgc2NvcmVzID0gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFwic2NvcmVzXCIpKTtcclxuICAgICAgICB2YXIgbGV2ZWxTY29yZSA9IDA7XHJcbiAgICAgICAgZm9yICh2YXIgc3ViTGV2ZWxTY29yZSBpbiBzY29yZXNbbGV2ZWxdKSB7XHJcbiAgICAgICAgICAgIGxldmVsU2NvcmUgKz0gc2NvcmVzW2xldmVsXVtzdWJMZXZlbFNjb3JlXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGxldmVsU2NvcmU7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoYXQuZ2V0TGV2ZWxMaW5lcyA9IGZ1bmN0aW9uKGxldmVsKSB7XHJcbiAgICAgICAgdmFyIGxpbmVzID0gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFwibGluZXNcIikpO1xyXG4gICAgICAgIHZhciBsZXZlbExpbmVzID0gMDtcclxuICAgICAgICBmb3IgKHZhciBzdWJMZXZlbExpbmVzIGluIGxpbmVzW2xldmVsXSkge1xyXG4gICAgICAgICAgICBsZXZlbExpbmVzICs9IGxpbmVzW2xldmVsXVtzdWJMZXZlbExpbmVzXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGxldmVsTGluZXM7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoYXQuZ2V0VG90YWxTY29yZSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHZhciBzY29yZXMgPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJzY29yZXNcIikpO1xyXG4gICAgICAgIHZhciBzY29yZXNBcnIgPSBPYmplY3QudmFsdWVzKHNjb3Jlcyk7XHJcbiAgICAgICAgdmFyIHRvdGFsU2NvcmUgPSAwO1xyXG4gICAgICAgIGZvciAodmFyIGxldmVsU2NvcmVzIGluIHNjb3Jlc0Fycikge1xyXG4gICAgICAgICAgICBmb3IgKHZhciBzdWJMZXZlbFNjb3JlIGluIHNjb3Jlc0FycltsZXZlbFNjb3Jlc10pIHtcclxuICAgICAgICAgICAgICAgIHRvdGFsU2NvcmUgKz0gc2NvcmVzQXJyW2xldmVsU2NvcmVzXVtzdWJMZXZlbFNjb3JlXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdG90YWxTY29yZTtcclxuICAgIH07XHJcblxyXG4gICAgdGhhdC5nZXRUb3RhbExpbmVzID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyIGxpbmVzID0gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFwibGluZXNcIikpO1xyXG4gICAgICAgIHZhciBsaW5lc0FyciA9IE9iamVjdC52YWx1ZXMobGluZXMpO1xyXG4gICAgICAgIHZhciB0b3RhbExpbmVzID0gMDtcclxuICAgICAgICBmb3IgKHZhciBsZXZlbExpbmVzIGluIGxpbmVzQXJyKSB7XHJcbiAgICAgICAgICAgIGZvciAodmFyIHN1YkxldmVsTGluZXMgaW4gbGluZXNBcnJbbGV2ZWxMaW5lc10pIHtcclxuICAgICAgICAgICAgICAgIHRvdGFsTGluZXMgKz0gbGluZXNBcnJbbGV2ZWxMaW5lc11bc3ViTGV2ZWxMaW5lc107XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRvdGFsTGluZXM7XHJcbiAgICB9O1xyXG5cclxuICAgIHJldHVybiB0aGF0O1xyXG5cclxufTsiXX0=
