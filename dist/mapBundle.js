(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
//Map JS code
var mapPresenter = require('./src/mapPresenter.js');

var jvmap = require('./src/map/jquery.vmap.js');

var worldMap = require('./src/map/maps/jquery.vmap.world.js');

//Internationalizations libs
var cldrp = require('./src/lib/CLDRPluralRuleParser/CLDRPluralRuleParser.js');

var jqueryi18n = require('./src/lib/jquery.i18n/jquery.i18n.js');

var messagestore = require('./src/lib/jquery.i18n/jquery.i18n.messagestore.js');

var fallbacks = require('./src/lib/jquery.i18n/jquery.i18n.fallbacks.js');

var language = require('./src/lib/jquery.i18n/jquery.i18n.language.js');

var parser = require('./src/lib/jquery.i18n/jquery.i18n.parser.js');

var emitter = require('./src/lib/jquery.i18n/jquery.i18n.emitter.js');

var emitterbidi = require('./src/lib/jquery.i18n/jquery.i18n.emitter.bidi.js');

var history = require('./src/lib/history/jquery.history.js');

var url = require('./src/lib/url/url.min.js');

var mapLanguagePicker = require('./src/mapLanguagePicker.js');

var jsonLoader = require('./src/jsonLoader.js');
},{"./src/jsonLoader.js":2,"./src/lib/CLDRPluralRuleParser/CLDRPluralRuleParser.js":3,"./src/lib/history/jquery.history.js":4,"./src/lib/jquery.i18n/jquery.i18n.emitter.bidi.js":5,"./src/lib/jquery.i18n/jquery.i18n.emitter.js":6,"./src/lib/jquery.i18n/jquery.i18n.fallbacks.js":7,"./src/lib/jquery.i18n/jquery.i18n.js":8,"./src/lib/jquery.i18n/jquery.i18n.language.js":9,"./src/lib/jquery.i18n/jquery.i18n.messagestore.js":10,"./src/lib/jquery.i18n/jquery.i18n.parser.js":11,"./src/lib/url/url.min.js":12,"./src/map/jquery.vmap.js":15,"./src/map/maps/jquery.vmap.world.js":16,"./src/mapLanguagePicker.js":13,"./src/mapPresenter.js":14}],2:[function(require,module,exports){
function loadTextFileAjaxSync(file) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", file, false); // Replace file with the path to your file
  xobj.send();
  if (xobj.status == 200) {
    return xobj.responseText;
  } else {
    // TODO Throw exception
    return null;
  }
}

module.exports.loadJsonFile = function loadJson(filePath) {
  // Load json file;
  var json = loadTextFileAjaxSync(filePath);
  // Parse json
  return JSON.parse(json);
};


},{}],3:[function(require,module,exports){
/**
 * cldrpluralparser.js
 * A parser engine for CLDR plural rules.
 *
 * Copyright 2012-2014 Santhosh Thottingal and other contributors
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 * @source https://github.com/santhoshtr/CLDRPluralRuleParser
 * @author Santhosh Thottingal <santhosh.thottingal@gmail.com>
 * @author Timo Tijhof
 * @author Amir Aharoni
 */

/**
 * Evaluates a plural rule in CLDR syntax for a number
 * @param {string} rule
 * @param {integer} number
 * @return {boolean} true if evaluation passed, false if evaluation failed.
 */

// UMD returnExports https://github.com/umdjs/umd/blob/master/returnExports.js
(function(root, factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module.
		define(factory);
	} else if (typeof exports === 'object') {
		// Node. Does not work with strict CommonJS, but
		// only CommonJS-like environments that support module.exports,
		// like Node.
		module.exports = factory();
	} else {
		// Browser globals (root is window)
		root.pluralRuleParser = factory();
	}
}(this, function() {

function pluralRuleParser(rule, number) {
	'use strict';

	/*
	Syntax: see http://unicode.org/reports/tr35/#Language_Plural_Rules
	-----------------------------------------------------------------
	condition     = and_condition ('or' and_condition)*
		('@integer' samples)?
		('@decimal' samples)?
	and_condition = relation ('and' relation)*
	relation      = is_relation | in_relation | within_relation
	is_relation   = expr 'is' ('not')? value
	in_relation   = expr (('not')? 'in' | '=' | '!=') range_list
	within_relation = expr ('not')? 'within' range_list
	expr          = operand (('mod' | '%') value)?
	operand       = 'n' | 'i' | 'f' | 't' | 'v' | 'w'
	range_list    = (range | value) (',' range_list)*
	value         = digit+
	digit         = 0|1|2|3|4|5|6|7|8|9
	range         = value'..'value
	samples       = sampleRange (',' sampleRange)* (',' ('…'|'...'))?
	sampleRange   = decimalValue '~' decimalValue
	decimalValue  = value ('.' value)?
	*/

	// We don't evaluate the samples section of the rule. Ignore it.
	rule = rule.split('@')[0].replace(/^\s*/, '').replace(/\s*$/, '');

	if (!rule.length) {
		// Empty rule or 'other' rule.
		return true;
	}

	// Indicates the current position in the rule as we parse through it.
	// Shared among all parsing functions below.
	var pos = 0,
		operand,
		expression,
		relation,
		result,
		whitespace = makeRegexParser(/^\s+/),
		value = makeRegexParser(/^\d+/),
		_n_ = makeStringParser('n'),
		_i_ = makeStringParser('i'),
		_f_ = makeStringParser('f'),
		_t_ = makeStringParser('t'),
		_v_ = makeStringParser('v'),
		_w_ = makeStringParser('w'),
		_is_ = makeStringParser('is'),
		_isnot_ = makeStringParser('is not'),
		_isnot_sign_ = makeStringParser('!='),
		_equal_ = makeStringParser('='),
		_mod_ = makeStringParser('mod'),
		_percent_ = makeStringParser('%'),
		_not_ = makeStringParser('not'),
		_in_ = makeStringParser('in'),
		_within_ = makeStringParser('within'),
		_range_ = makeStringParser('..'),
		_comma_ = makeStringParser(','),
		_or_ = makeStringParser('or'),
		_and_ = makeStringParser('and');

	function debug() {
		// console.log.apply(console, arguments);
	}

	debug('pluralRuleParser', rule, number);

	// Try parsers until one works, if none work return null
	function choice(parserSyntax) {
		return function() {
			var i, result;

			for (i = 0; i < parserSyntax.length; i++) {
				result = parserSyntax[i]();

				if (result !== null) {
					return result;
				}
			}

			return null;
		};
	}

	// Try several parserSyntax-es in a row.
	// All must succeed; otherwise, return null.
	// This is the only eager one.
	function sequence(parserSyntax) {
		var i, parserRes,
			originalPos = pos,
			result = [];

		for (i = 0; i < parserSyntax.length; i++) {
			parserRes = parserSyntax[i]();

			if (parserRes === null) {
				pos = originalPos;

				return null;
			}

			result.push(parserRes);
		}

		return result;
	}

	// Run the same parser over and over until it fails.
	// Must succeed a minimum of n times; otherwise, return null.
	function nOrMore(n, p) {
		return function() {
			var originalPos = pos,
				result = [],
				parsed = p();

			while (parsed !== null) {
				result.push(parsed);
				parsed = p();
			}

			if (result.length < n) {
				pos = originalPos;

				return null;
			}

			return result;
		};
	}

	// Helpers - just make parserSyntax out of simpler JS builtin types
	function makeStringParser(s) {
		var len = s.length;

		return function() {
			var result = null;

			if (rule.substr(pos, len) === s) {
				result = s;
				pos += len;
			}

			return result;
		};
	}

	function makeRegexParser(regex) {
		return function() {
			var matches = rule.substr(pos).match(regex);

			if (matches === null) {
				return null;
			}

			pos += matches[0].length;

			return matches[0];
		};
	}

	/**
	 * Integer digits of n.
	 */
	function i() {
		var result = _i_();

		if (result === null) {
			debug(' -- failed i', parseInt(number, 10));

			return result;
		}

		result = parseInt(number, 10);
		debug(' -- passed i ', result);

		return result;
	}

	/**
	 * Absolute value of the source number (integer and decimals).
	 */
	function n() {
		var result = _n_();

		if (result === null) {
			debug(' -- failed n ', number);

			return result;
		}

		result = parseFloat(number, 10);
		debug(' -- passed n ', result);

		return result;
	}

	/**
	 * Visible fractional digits in n, with trailing zeros.
	 */
	function f() {
		var result = _f_();

		if (result === null) {
			debug(' -- failed f ', number);

			return result;
		}

		result = (number + '.').split('.')[1] || 0;
		debug(' -- passed f ', result);

		return result;
	}

	/**
	 * Visible fractional digits in n, without trailing zeros.
	 */
	function t() {
		var result = _t_();

		if (result === null) {
			debug(' -- failed t ', number);

			return result;
		}

		result = (number + '.').split('.')[1].replace(/0$/, '') || 0;
		debug(' -- passed t ', result);

		return result;
	}

	/**
	 * Number of visible fraction digits in n, with trailing zeros.
	 */
	function v() {
		var result = _v_();

		if (result === null) {
			debug(' -- failed v ', number);

			return result;
		}

		result = (number + '.').split('.')[1].length || 0;
		debug(' -- passed v ', result);

		return result;
	}

	/**
	 * Number of visible fraction digits in n, without trailing zeros.
	 */
	function w() {
		var result = _w_();

		if (result === null) {
			debug(' -- failed w ', number);

			return result;
		}

		result = (number + '.').split('.')[1].replace(/0$/, '').length || 0;
		debug(' -- passed w ', result);

		return result;
	}

	// operand       = 'n' | 'i' | 'f' | 't' | 'v' | 'w'
	operand = choice([n, i, f, t, v, w]);

	// expr          = operand (('mod' | '%') value)?
	expression = choice([mod, operand]);

	function mod() {
		var result = sequence(
			[operand, whitespace, choice([_mod_, _percent_]), whitespace, value]
		);

		if (result === null) {
			debug(' -- failed mod');

			return null;
		}

		debug(' -- passed ' + parseInt(result[0], 10) + ' ' + result[2] + ' ' + parseInt(result[4], 10));

		return parseFloat(result[0]) % parseInt(result[4], 10);
	}

	function not() {
		var result = sequence([whitespace, _not_]);

		if (result === null) {
			debug(' -- failed not');

			return null;
		}

		return result[1];
	}

	// is_relation   = expr 'is' ('not')? value
	function is() {
		var result = sequence([expression, whitespace, choice([_is_]), whitespace, value]);

		if (result !== null) {
			debug(' -- passed is : ' + result[0] + ' == ' + parseInt(result[4], 10));

			return result[0] === parseInt(result[4], 10);
		}

		debug(' -- failed is');

		return null;
	}

	// is_relation   = expr 'is' ('not')? value
	function isnot() {
		var result = sequence(
			[expression, whitespace, choice([_isnot_, _isnot_sign_]), whitespace, value]
		);

		if (result !== null) {
			debug(' -- passed isnot: ' + result[0] + ' != ' + parseInt(result[4], 10));

			return result[0] !== parseInt(result[4], 10);
		}

		debug(' -- failed isnot');

		return null;
	}

	function not_in() {
		var i, range_list,
			result = sequence([expression, whitespace, _isnot_sign_, whitespace, rangeList]);

		if (result !== null) {
			debug(' -- passed not_in: ' + result[0] + ' != ' + result[4]);
			range_list = result[4];

			for (i = 0; i < range_list.length; i++) {
				if (parseInt(range_list[i], 10) === parseInt(result[0], 10)) {
					return false;
				}
			}

			return true;
		}

		debug(' -- failed not_in');

		return null;
	}

	// range_list    = (range | value) (',' range_list)*
	function rangeList() {
		var result = sequence([choice([range, value]), nOrMore(0, rangeTail)]),
			resultList = [];

		if (result !== null) {
			resultList = resultList.concat(result[0]);

			if (result[1][0]) {
				resultList = resultList.concat(result[1][0]);
			}

			return resultList;
		}

		debug(' -- failed rangeList');

		return null;
	}

	function rangeTail() {
		// ',' range_list
		var result = sequence([_comma_, rangeList]);

		if (result !== null) {
			return result[1];
		}

		debug(' -- failed rangeTail');

		return null;
	}

	// range         = value'..'value
	function range() {
		var i, array, left, right,
			result = sequence([value, _range_, value]);

		if (result !== null) {
			debug(' -- passed range');

			array = [];
			left = parseInt(result[0], 10);
			right = parseInt(result[2], 10);

			for (i = left; i <= right; i++) {
				array.push(i);
			}

			return array;
		}

		debug(' -- failed range');

		return null;
	}

	function _in() {
		var result, range_list, i;

		// in_relation   = expr ('not')? 'in' range_list
		result = sequence(
			[expression, nOrMore(0, not), whitespace, choice([_in_, _equal_]), whitespace, rangeList]
		);

		if (result !== null) {
			debug(' -- passed _in:' + result);

			range_list = result[5];

			for (i = 0; i < range_list.length; i++) {
				if (parseInt(range_list[i], 10) === parseFloat(result[0])) {
					return (result[1][0] !== 'not');
				}
			}

			return (result[1][0] === 'not');
		}

		debug(' -- failed _in ');

		return null;
	}

	/**
	 * The difference between "in" and "within" is that
	 * "in" only includes integers in the specified range,
	 * while "within" includes all values.
	 */
	function within() {
		var range_list, result;

		// within_relation = expr ('not')? 'within' range_list
		result = sequence(
			[expression, nOrMore(0, not), whitespace, _within_, whitespace, rangeList]
		);

		if (result !== null) {
			debug(' -- passed within');

			range_list = result[5];

			if ((result[0] >= parseInt(range_list[0], 10)) &&
				(result[0] < parseInt(range_list[range_list.length - 1], 10))) {

				return (result[1][0] !== 'not');
			}

			return (result[1][0] === 'not');
		}

		debug(' -- failed within ');

		return null;
	}

	// relation      = is_relation | in_relation | within_relation
	relation = choice([is, not_in, isnot, _in, within]);

	// and_condition = relation ('and' relation)*
	function and() {
		var i,
			result = sequence([relation, nOrMore(0, andTail)]);

		if (result) {
			if (!result[0]) {
				return false;
			}

			for (i = 0; i < result[1].length; i++) {
				if (!result[1][i]) {
					return false;
				}
			}

			return true;
		}

		debug(' -- failed and');

		return null;
	}

	// ('and' relation)*
	function andTail() {
		var result = sequence([whitespace, _and_, whitespace, relation]);

		if (result !== null) {
			debug(' -- passed andTail' + result);

			return result[3];
		}

		debug(' -- failed andTail');

		return null;

	}
	//  ('or' and_condition)*
	function orTail() {
		var result = sequence([whitespace, _or_, whitespace, and]);

		if (result !== null) {
			debug(' -- passed orTail: ' + result[3]);

			return result[3];
		}

		debug(' -- failed orTail');

		return null;
	}

	// condition     = and_condition ('or' and_condition)*
	function condition() {
		var i,
			result = sequence([and, nOrMore(0, orTail)]);

		if (result) {
			for (i = 0; i < result[1].length; i++) {
				if (result[1][i]) {
					return true;
				}
			}

			return result[0];
		}

		return false;
	}

	result = condition();

	/**
	 * For success, the pos must have gotten to the end of the rule
	 * and returned a non-null.
	 * n.b. This is part of language infrastructure,
	 * so we do not throw an internationalizable message.
	 */
	if (result === null) {
		throw new Error('Parse error at position ' + pos.toString() + ' for rule: ' + rule);
	}

	if (pos !== rule.length) {
		debug('Warning: Rule not parsed completely. Parser stopped at ' + rule.substr(0, pos) + ' for rule: ' + rule);
	}

	return result;
}

return pluralRuleParser;

}));

},{}],4:[function(require,module,exports){
typeof JSON!="object"&&(JSON={}),function(){"use strict";function f(e){return e<10?"0"+e:e}function quote(e){return escapable.lastIndex=0,escapable.test(e)?'"'+e.replace(escapable,function(e){var t=meta[e];return typeof t=="string"?t:"\\u"+("0000"+e.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+e+'"'}function str(e,t){var n,r,i,s,o=gap,u,a=t[e];a&&typeof a=="object"&&typeof a.toJSON=="function"&&(a=a.toJSON(e)),typeof rep=="function"&&(a=rep.call(t,e,a));switch(typeof a){case"string":return quote(a);case"number":return isFinite(a)?String(a):"null";case"boolean":case"null":return String(a);case"object":if(!a)return"null";gap+=indent,u=[];if(Object.prototype.toString.apply(a)==="[object Array]"){s=a.length;for(n=0;n<s;n+=1)u[n]=str(n,a)||"null";return i=u.length===0?"[]":gap?"[\n"+gap+u.join(",\n"+gap)+"\n"+o+"]":"["+u.join(",")+"]",gap=o,i}if(rep&&typeof rep=="object"){s=rep.length;for(n=0;n<s;n+=1)typeof rep[n]=="string"&&(r=rep[n],i=str(r,a),i&&u.push(quote(r)+(gap?": ":":")+i))}else for(r in a)Object.prototype.hasOwnProperty.call(a,r)&&(i=str(r,a),i&&u.push(quote(r)+(gap?": ":":")+i));return i=u.length===0?"{}":gap?"{\n"+gap+u.join(",\n"+gap)+"\n"+o+"}":"{"+u.join(",")+"}",gap=o,i}}typeof Date.prototype.toJSON!="function"&&(Date.prototype.toJSON=function(e){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null},String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(e){return this.valueOf()});var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={"\b":"\\b"," ":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},rep;typeof JSON.stringify!="function"&&(JSON.stringify=function(e,t,n){var r;gap="",indent="";if(typeof n=="number")for(r=0;r<n;r+=1)indent+=" ";else typeof n=="string"&&(indent=n);rep=t;if(!t||typeof t=="function"||typeof t=="object"&&typeof t.length=="number")return str("",{"":e});throw new Error("JSON.stringify")}),typeof JSON.parse!="function"&&(JSON.parse=function(text,reviver){function walk(e,t){var n,r,i=e[t];if(i&&typeof i=="object")for(n in i)Object.prototype.hasOwnProperty.call(i,n)&&(r=walk(i,n),r!==undefined?i[n]=r:delete i[n]);return reviver.call(e,t,i)}var j;text=String(text),cx.lastIndex=0,cx.test(text)&&(text=text.replace(cx,function(e){return"\\u"+("0000"+e.charCodeAt(0).toString(16)).slice(-4)}));if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return j=eval("("+text+")"),typeof reviver=="function"?walk({"":j},""):j;throw new SyntaxError("JSON.parse")})}(),function(e,t){"use strict";var n=e.History=e.History||{},r=e.jQuery;if(typeof n.Adapter!="undefined")throw new Error("History.js Adapter has already been loaded...");n.Adapter={bind:function(e,t,n){r(e).bind(t,n)},trigger:function(e,t,n){r(e).trigger(t,n)},extractEventData:function(e,n,r){var i=n&&n.originalEvent&&n.originalEvent[e]||r&&r[e]||t;return i},onDomLoad:function(e){r(e)}},typeof n.init!="undefined"&&n.init()}(window),function(e,t){"use strict";var n=e.document,r=e.setTimeout||r,i=e.clearTimeout||i,s=e.setInterval||s,o=e.History=e.History||{};if(typeof o.initHtml4!="undefined")throw new Error("History.js HTML4 Support has already been loaded...");o.initHtml4=function(){if(typeof o.initHtml4.initialized!="undefined")return!1;o.initHtml4.initialized=!0,o.enabled=!0,o.savedHashes=[],o.isLastHash=function(e){var t=o.getHashByIndex(),n;return n=e===t,n},o.isHashEqual=function(e,t){return e=encodeURIComponent(e).replace(/%25/g,"%"),t=encodeURIComponent(t).replace(/%25/g,"%"),e===t},o.saveHash=function(e){return o.isLastHash(e)?!1:(o.savedHashes.push(e),!0)},o.getHashByIndex=function(e){var t=null;return typeof e=="undefined"?t=o.savedHashes[o.savedHashes.length-1]:e<0?t=o.savedHashes[o.savedHashes.length+e]:t=o.savedHashes[e],t},o.discardedHashes={},o.discardedStates={},o.discardState=function(e,t,n){var r=o.getHashByState(e),i;return i={discardedState:e,backState:n,forwardState:t},o.discardedStates[r]=i,!0},o.discardHash=function(e,t,n){var r={discardedHash:e,backState:n,forwardState:t};return o.discardedHashes[e]=r,!0},o.discardedState=function(e){var t=o.getHashByState(e),n;return n=o.discardedStates[t]||!1,n},o.discardedHash=function(e){var t=o.discardedHashes[e]||!1;return t},o.recycleState=function(e){var t=o.getHashByState(e);return o.discardedState(e)&&delete o.discardedStates[t],!0},o.emulated.hashChange&&(o.hashChangeInit=function(){o.checkerFunction=null;var t="",r,i,u,a,f=Boolean(o.getHash());return o.isInternetExplorer()?(r="historyjs-iframe",i=n.createElement("iframe"),i.setAttribute("id",r),i.setAttribute("src","#"),i.style.display="none",n.body.appendChild(i),i.contentWindow.document.open(),i.contentWindow.document.close(),u="",a=!1,o.checkerFunction=function(){if(a)return!1;a=!0;var n=o.getHash(),r=o.getHash(i.contentWindow.document);return n!==t?(t=n,r!==n&&(u=r=n,i.contentWindow.document.open(),i.contentWindow.document.close(),i.contentWindow.document.location.hash=o.escapeHash(n)),o.Adapter.trigger(e,"hashchange")):r!==u&&(u=r,f&&r===""?o.back():o.setHash(r,!1)),a=!1,!0}):o.checkerFunction=function(){var n=o.getHash()||"";return n!==t&&(t=n,o.Adapter.trigger(e,"hashchange")),!0},o.intervalList.push(s(o.checkerFunction,o.options.hashChangeInterval)),!0},o.Adapter.onDomLoad(o.hashChangeInit)),o.emulated.pushState&&(o.onHashChange=function(t){var n=t&&t.newURL||o.getLocationHref(),r=o.getHashByUrl(n),i=null,s=null,u=null,a;return o.isLastHash(r)?(o.busy(!1),!1):(o.doubleCheckComplete(),o.saveHash(r),r&&o.isTraditionalAnchor(r)?(o.Adapter.trigger(e,"anchorchange"),o.busy(!1),!1):(i=o.extractState(o.getFullUrl(r||o.getLocationHref()),!0),o.isLastSavedState(i)?(o.busy(!1),!1):(s=o.getHashByState(i),a=o.discardedState(i),a?(o.getHashByIndex(-2)===o.getHashByState(a.forwardState)?o.back(!1):o.forward(!1),!1):(o.pushState(i.data,i.title,encodeURI(i.url),!1),!0))))},o.Adapter.bind(e,"hashchange",o.onHashChange),o.pushState=function(t,n,r,i){r=encodeURI(r).replace(/%25/g,"%");if(o.getHashByUrl(r))throw new Error("History.js does not support states with fragment-identifiers (hashes/anchors).");if(i!==!1&&o.busy())return o.pushQueue({scope:o,callback:o.pushState,args:arguments,queue:i}),!1;o.busy(!0);var s=o.createStateObject(t,n,r),u=o.getHashByState(s),a=o.getState(!1),f=o.getHashByState(a),l=o.getHash(),c=o.expectedStateId==s.id;return o.storeState(s),o.expectedStateId=s.id,o.recycleState(s),o.setTitle(s),u===f?(o.busy(!1),!1):(o.saveState(s),c||o.Adapter.trigger(e,"statechange"),!o.isHashEqual(u,l)&&!o.isHashEqual(u,o.getShortUrl(o.getLocationHref()))&&o.setHash(u,!1),o.busy(!1),!0)},o.replaceState=function(t,n,r,i){r=encodeURI(r).replace(/%25/g,"%");if(o.getHashByUrl(r))throw new Error("History.js does not support states with fragment-identifiers (hashes/anchors).");if(i!==!1&&o.busy())return o.pushQueue({scope:o,callback:o.replaceState,args:arguments,queue:i}),!1;o.busy(!0);var s=o.createStateObject(t,n,r),u=o.getHashByState(s),a=o.getState(!1),f=o.getHashByState(a),l=o.getStateByIndex(-2);return o.discardState(a,s,l),u===f?(o.storeState(s),o.expectedStateId=s.id,o.recycleState(s),o.setTitle(s),o.saveState(s),o.Adapter.trigger(e,"statechange"),o.busy(!1)):o.pushState(s.data,s.title,s.url,!1),!0}),o.emulated.pushState&&o.getHash()&&!o.emulated.hashChange&&o.Adapter.onDomLoad(function(){o.Adapter.trigger(e,"hashchange")})},typeof o.init!="undefined"&&o.init()}(window),function(e,t){"use strict";var n=e.console||t,r=e.document,i=e.navigator,s=!1,o=e.setTimeout,u=e.clearTimeout,a=e.setInterval,f=e.clearInterval,l=e.JSON,c=e.alert,h=e.History=e.History||{},p=e.history;try{s=e.sessionStorage,s.setItem("TEST","1"),s.removeItem("TEST")}catch(d){s=!1}l.stringify=l.stringify||l.encode,l.parse=l.parse||l.decode;if(typeof h.init!="undefined")throw new Error("History.js Core has already been loaded...");h.init=function(e){return typeof h.Adapter=="undefined"?!1:(typeof h.initCore!="undefined"&&h.initCore(),typeof h.initHtml4!="undefined"&&h.initHtml4(),!0)},h.initCore=function(d){if(typeof h.initCore.initialized!="undefined")return!1;h.initCore.initialized=!0,h.options=h.options||{},h.options.hashChangeInterval=h.options.hashChangeInterval||100,h.options.safariPollInterval=h.options.safariPollInterval||500,h.options.doubleCheckInterval=h.options.doubleCheckInterval||500,h.options.disableSuid=h.options.disableSuid||!1,h.options.storeInterval=h.options.storeInterval||1e3,h.options.busyDelay=h.options.busyDelay||250,h.options.debug=h.options.debug||!1,h.options.initialTitle=h.options.initialTitle||r.title,h.options.html4Mode=h.options.html4Mode||!1,h.options.delayInit=h.options.delayInit||!1,h.intervalList=[],h.clearAllIntervals=function(){var e,t=h.intervalList;if(typeof t!="undefined"&&t!==null){for(e=0;e<t.length;e++)f(t[e]);h.intervalList=null}},h.debug=function(){(h.options.debug||!1)&&h.log.apply(h,arguments)},h.log=function(){var e=typeof n!="undefined"&&typeof n.log!="undefined"&&typeof n.log.apply!="undefined",t=r.getElementById("log"),i,s,o,u,a;e?(u=Array.prototype.slice.call(arguments),i=u.shift(),typeof n.debug!="undefined"?n.debug.apply(n,[i,u]):n.log.apply(n,[i,u])):i="\n"+arguments[0]+"\n";for(s=1,o=arguments.length;s<o;++s){a=arguments[s];if(typeof a=="object"&&typeof l!="undefined")try{a=l.stringify(a)}catch(f){}i+="\n"+a+"\n"}return t?(t.value+=i+"\n-----\n",t.scrollTop=t.scrollHeight-t.clientHeight):e||c(i),!0},h.getInternetExplorerMajorVersion=function(){var e=h.getInternetExplorerMajorVersion.cached=typeof h.getInternetExplorerMajorVersion.cached!="undefined"?h.getInternetExplorerMajorVersion.cached:function(){var e=3,t=r.createElement("div"),n=t.getElementsByTagName("i");while((t.innerHTML="<!--[if gt IE "+ ++e+"]><i></i><![endif]-->")&&n[0]);return e>4?e:!1}();return e},h.isInternetExplorer=function(){var e=h.isInternetExplorer.cached=typeof h.isInternetExplorer.cached!="undefined"?h.isInternetExplorer.cached:Boolean(h.getInternetExplorerMajorVersion());return e},h.options.html4Mode?h.emulated={pushState:!0,hashChange:!0}:h.emulated={pushState:!Boolean(e.history&&e.history.pushState&&e.history.replaceState&&!/ Mobile\/([1-7][a-z]|(8([abcde]|f(1[0-8]))))/i.test(i.userAgent)&&!/AppleWebKit\/5([0-2]|3[0-2])/i.test(i.userAgent)),hashChange:Boolean(!("onhashchange"in e||"onhashchange"in r)||h.isInternetExplorer()&&h.getInternetExplorerMajorVersion()<8)},h.enabled=!h.emulated.pushState,h.bugs={setHash:Boolean(!h.emulated.pushState&&i.vendor==="Apple Computer, Inc."&&/AppleWebKit\/5([0-2]|3[0-3])/.test(i.userAgent)),safariPoll:Boolean(!h.emulated.pushState&&i.vendor==="Apple Computer, Inc."&&/AppleWebKit\/5([0-2]|3[0-3])/.test(i.userAgent)),ieDoubleCheck:Boolean(h.isInternetExplorer()&&h.getInternetExplorerMajorVersion()<8),hashEscape:Boolean(h.isInternetExplorer()&&h.getInternetExplorerMajorVersion()<7)},h.isEmptyObject=function(e){for(var t in e)if(e.hasOwnProperty(t))return!1;return!0},h.cloneObject=function(e){var t,n;return e?(t=l.stringify(e),n=l.parse(t)):n={},n},h.getRootUrl=function(){var e=r.location.protocol+"//"+(r.location.hostname||r.location.host);if(r.location.port||!1)e+=":"+r.location.port;return e+="/",e},h.getBaseHref=function(){var e=r.getElementsByTagName("base"),t=null,n="";return e.length===1&&(t=e[0],n=t.href.replace(/[^\/]+$/,"")),n=n.replace(/\/+$/,""),n&&(n+="/"),n},h.getBaseUrl=function(){var e=h.getBaseHref()||h.getBasePageUrl()||h.getRootUrl();return e},h.getPageUrl=function(){var e=h.getState(!1,!1),t=(e||{}).url||h.getLocationHref(),n;return n=t.replace(/\/+$/,"").replace(/[^\/]+$/,function(e,t,n){return/\./.test(e)?e:e+"/"}),n},h.getBasePageUrl=function(){var e=h.getLocationHref().replace(/[#\?].*/,"").replace(/[^\/]+$/,function(e,t,n){return/[^\/]$/.test(e)?"":e}).replace(/\/+$/,"")+"/";return e},h.getFullUrl=function(e,t){var n=e,r=e.substring(0,1);return t=typeof t=="undefined"?!0:t,/[a-z]+\:\/\//.test(e)||(r==="/"?n=h.getRootUrl()+e.replace(/^\/+/,""):r==="#"?n=h.getPageUrl().replace(/#.*/,"")+e:r==="?"?n=h.getPageUrl().replace(/[\?#].*/,"")+e:t?n=h.getBaseUrl()+e.replace(/^(\.\/)+/,""):n=h.getBasePageUrl()+e.replace(/^(\.\/)+/,"")),n.replace(/\#$/,"")},h.getShortUrl=function(e){var t=e,n=h.getBaseUrl(),r=h.getRootUrl();return h.emulated.pushState&&(t=t.replace(n,"")),t=t.replace(r,"/"),h.isTraditionalAnchor(t)&&(t="./"+t),t=t.replace(/^(\.\/)+/g,"./").replace(/\#$/,""),t},h.getLocationHref=function(e){return e=e||r,e.URL===e.location.href?e.location.href:e.location.href===decodeURIComponent(e.URL)?e.URL:e.location.hash&&decodeURIComponent(e.location.href.replace(/^[^#]+/,""))===e.location.hash?e.location.href:e.URL.indexOf("#")==-1&&e.location.href.indexOf("#")!=-1?e.location.href:e.URL||e.location.href},h.store={},h.idToState=h.idToState||{},h.stateToId=h.stateToId||{},h.urlToId=h.urlToId||{},h.storedStates=h.storedStates||[],h.savedStates=h.savedStates||[],h.normalizeStore=function(){h.store.idToState=h.store.idToState||{},h.store.urlToId=h.store.urlToId||{},h.store.stateToId=h.store.stateToId||{}},h.getState=function(e,t){typeof e=="undefined"&&(e=!0),typeof t=="undefined"&&(t=!0);var n=h.getLastSavedState();return!n&&t&&(n=h.createStateObject()),e&&(n=h.cloneObject(n),n.url=n.cleanUrl||n.url),n},h.getIdByState=function(e){var t=h.extractId(e.url),n;if(!t){n=h.getStateString(e);if(typeof h.stateToId[n]!="undefined")t=h.stateToId[n];else if(typeof h.store.stateToId[n]!="undefined")t=h.store.stateToId[n];else{for(;;){t=(new Date).getTime()+String(Math.random()).replace(/\D/g,"");if(typeof h.idToState[t]=="undefined"&&typeof h.store.idToState[t]=="undefined")break}h.stateToId[n]=t,h.idToState[t]=e}}return t},h.normalizeState=function(e){var t,n;if(!e||typeof e!="object")e={};if(typeof e.normalized!="undefined")return e;if(!e.data||typeof e.data!="object")e.data={};return t={},t.normalized=!0,t.title=e.title||"",t.url=h.getFullUrl(e.url?e.url:h.getLocationHref()),t.hash=h.getShortUrl(t.url),t.data=h.cloneObject(e.data),t.id=h.getIdByState(t),t.cleanUrl=t.url.replace(/\??\&_suid.*/,""),t.url=t.cleanUrl,n=!h.isEmptyObject(t.data),(t.title||n)&&h.options.disableSuid!==!0&&(t.hash=h.getShortUrl(t.url).replace(/\??\&_suid.*/,""),/\?/.test(t.hash)||(t.hash+="?"),t.hash+="&_suid="+t.id),t.hashedUrl=h.getFullUrl(t.hash),(h.emulated.pushState||h.bugs.safariPoll)&&h.hasUrlDuplicate(t)&&(t.url=t.hashedUrl),t},h.createStateObject=function(e,t,n){var r={data:e,title:t,url:n};return r=h.normalizeState(r),r},h.getStateById=function(e){e=String(e);var n=h.idToState[e]||h.store.idToState[e]||t;return n},h.getStateString=function(e){var t,n,r;return t=h.normalizeState(e),n={data:t.data,title:e.title,url:e.url},r=l.stringify(n),r},h.getStateId=function(e){var t,n;return t=h.normalizeState(e),n=t.id,n},h.getHashByState=function(e){var t,n;return t=h.normalizeState(e),n=t.hash,n},h.extractId=function(e){var t,n,r,i;return e.indexOf("#")!=-1?i=e.split("#")[0]:i=e,n=/(.*)\&_suid=([0-9]+)$/.exec(i),r=n?n[1]||e:e,t=n?String(n[2]||""):"",t||!1},h.isTraditionalAnchor=function(e){var t=!/[\/\?\.]/.test(e);return t},h.extractState=function(e,t){var n=null,r,i;return t=t||!1,r=h.extractId(e),r&&(n=h.getStateById(r)),n||(i=h.getFullUrl(e),r=h.getIdByUrl(i)||!1,r&&(n=h.getStateById(r)),!n&&t&&!h.isTraditionalAnchor(e)&&(n=h.createStateObject(null,null,i))),n},h.getIdByUrl=function(e){var n=h.urlToId[e]||h.store.urlToId[e]||t;return n},h.getLastSavedState=function(){return h.savedStates[h.savedStates.length-1]||t},h.getLastStoredState=function(){return h.storedStates[h.storedStates.length-1]||t},h.hasUrlDuplicate=function(e){var t=!1,n;return n=h.extractState(e.url),t=n&&n.id!==e.id,t},h.storeState=function(e){return h.urlToId[e.url]=e.id,h.storedStates.push(h.cloneObject(e)),e},h.isLastSavedState=function(e){var t=!1,n,r,i;return h.savedStates.length&&(n=e.id,r=h.getLastSavedState(),i=r.id,t=n===i),t},h.saveState=function(e){return h.isLastSavedState(e)?!1:(h.savedStates.push(h.cloneObject(e)),!0)},h.getStateByIndex=function(e){var t=null;return typeof e=="undefined"?t=h.savedStates[h.savedStates.length-1]:e<0?t=h.savedStates[h.savedStates.length+e]:t=h.savedStates[e],t},h.getCurrentIndex=function(){var e=null;return h.savedStates.length<1?e=0:e=h.savedStates.length-1,e},h.getHash=function(e){var t=h.getLocationHref(e),n;return n=h.getHashByUrl(t),n},h.unescapeHash=function(e){var t=h.normalizeHash(e);return t=decodeURIComponent(t),t},h.normalizeHash=function(e){var t=e.replace(/[^#]*#/,"").replace(/#.*/,"");return t},h.setHash=function(e,t){var n,i;return t!==!1&&h.busy()?(h.pushQueue({scope:h,callback:h.setHash,args:arguments,queue:t}),!1):(h.busy(!0),n=h.extractState(e,!0),n&&!h.emulated.pushState?h.pushState(n.data,n.title,n.url,!1):h.getHash()!==e&&(h.bugs.setHash?(i=h.getPageUrl(),h.pushState(null,null,i+"#"+e,!1)):r.location.hash=e),h)},h.escapeHash=function(t){var n=h.normalizeHash(t);return n=e.encodeURIComponent(n),h.bugs.hashEscape||(n=n.replace(/\%21/g,"!").replace(/\%26/g,"&").replace(/\%3D/g,"=").replace(/\%3F/g,"?")),n},h.getHashByUrl=function(e){var t=String(e).replace(/([^#]*)#?([^#]*)#?(.*)/,"$2");return t=h.unescapeHash(t),t},h.setTitle=function(e){var t=e.title,n;t||(n=h.getStateByIndex(0),n&&n.url===e.url&&(t=n.title||h.options.initialTitle));try{r.getElementsByTagName("title")[0].innerHTML=t.replace("<","&lt;").replace(">","&gt;").replace(" & "," &amp; ")}catch(i){}return r.title=t,h},h.queues=[],h.busy=function(e){typeof e!="undefined"?h.busy.flag=e:typeof h.busy.flag=="undefined"&&(h.busy.flag=!1);if(!h.busy.flag){u(h.busy.timeout);var t=function(){var e,n,r;if(h.busy.flag)return;for(e=h.queues.length-1;e>=0;--e){n=h.queues[e];if(n.length===0)continue;r=n.shift(),h.fireQueueItem(r),h.busy.timeout=o(t,h.options.busyDelay)}};h.busy.timeout=o(t,h.options.busyDelay)}return h.busy.flag},h.busy.flag=!1,h.fireQueueItem=function(e){return e.callback.apply(e.scope||h,e.args||[])},h.pushQueue=function(e){return h.queues[e.queue||0]=h.queues[e.queue||0]||[],h.queues[e.queue||0].push(e),h},h.queue=function(e,t){return typeof e=="function"&&(e={callback:e}),typeof t!="undefined"&&(e.queue=t),h.busy()?h.pushQueue(e):h.fireQueueItem(e),h},h.clearQueue=function(){return h.busy.flag=!1,h.queues=[],h},h.stateChanged=!1,h.doubleChecker=!1,h.doubleCheckComplete=function(){return h.stateChanged=!0,h.doubleCheckClear(),h},h.doubleCheckClear=function(){return h.doubleChecker&&(u(h.doubleChecker),h.doubleChecker=!1),h},h.doubleCheck=function(e){return h.stateChanged=!1,h.doubleCheckClear(),h.bugs.ieDoubleCheck&&(h.doubleChecker=o(function(){return h.doubleCheckClear(),h.stateChanged||e(),!0},h.options.doubleCheckInterval)),h},h.safariStatePoll=function(){var t=h.extractState(h.getLocationHref()),n;if(!h.isLastSavedState(t))return n=t,n||(n=h.createStateObject()),h.Adapter.trigger(e,"popstate"),h;return},h.back=function(e){return e!==!1&&h.busy()?(h.pushQueue({scope:h,callback:h.back,args:arguments,queue:e}),!1):(h.busy(!0),h.doubleCheck(function(){h.back(!1)}),p.go(-1),!0)},h.forward=function(e){return e!==!1&&h.busy()?(h.pushQueue({scope:h,callback:h.forward,args:arguments,queue:e}),!1):(h.busy(!0),h.doubleCheck(function(){h.forward(!1)}),p.go(1),!0)},h.go=function(e,t){var n;if(e>0)for(n=1;n<=e;++n)h.forward(t);else{if(!(e<0))throw new Error("History.go: History.go requires a positive or negative integer passed.");for(n=-1;n>=e;--n)h.back(t)}return h};if(h.emulated.pushState){var v=function(){};h.pushState=h.pushState||v,h.replaceState=h.replaceState||v}else h.onPopState=function(t,n){var r=!1,i=!1,s,o;return h.doubleCheckComplete(),s=h.getHash(),s?(o=h.extractState(s||h.getLocationHref(),!0),o?h.replaceState(o.data,o.title,o.url,!1):(h.Adapter.trigger(e,"anchorchange"),h.busy(!1)),h.expectedStateId=!1,!1):(r=h.Adapter.extractEventData("state",t,n)||!1,r?i=h.getStateById(r):h.expectedStateId?i=h.getStateById(h.expectedStateId):i=h.extractState(h.getLocationHref()),i||(i=h.createStateObject(null,null,h.getLocationHref())),h.expectedStateId=!1,h.isLastSavedState(i)?(h.busy(!1),!1):(h.storeState(i),h.saveState(i),h.setTitle(i),h.Adapter.trigger(e,"statechange"),h.busy(!1),!0))},h.Adapter.bind(e,"popstate",h.onPopState),h.pushState=function(t,n,r,i){if(h.getHashByUrl(r)&&h.emulated.pushState)throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");if(i!==!1&&h.busy())return h.pushQueue({scope:h,callback:h.pushState,args:arguments,queue:i}),!1;h.busy(!0);var s=h.createStateObject(t,n,r);return h.isLastSavedState(s)?h.busy(!1):(h.storeState(s),h.expectedStateId=s.id,p.pushState(s.id,s.title,s.url),h.Adapter.trigger(e,"popstate")),!0},h.replaceState=function(t,n,r,i){if(h.getHashByUrl(r)&&h.emulated.pushState)throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");if(i!==!1&&h.busy())return h.pushQueue({scope:h,callback:h.replaceState,args:arguments,queue:i}),!1;h.busy(!0);var s=h.createStateObject(t,n,r);return h.isLastSavedState(s)?h.busy(!1):(h.storeState(s),h.expectedStateId=s.id,p.replaceState(s.id,s.title,s.url),h.Adapter.trigger(e,"popstate")),!0};if(s){try{h.store=l.parse(s.getItem("History.store"))||{}}catch(m){h.store={}}h.normalizeStore()}else h.store={},h.normalizeStore();h.Adapter.bind(e,"unload",h.clearAllIntervals),h.saveState(h.storeState(h.extractState(h.getLocationHref(),!0))),s&&(h.onUnload=function(){var e,t,n;try{e=l.parse(s.getItem("History.store"))||{}}catch(r){e={}}e.idToState=e.idToState||{},e.urlToId=e.urlToId||{},e.stateToId=e.stateToId||{};for(t in h.idToState){if(!h.idToState.hasOwnProperty(t))continue;e.idToState[t]=h.idToState[t]}for(t in h.urlToId){if(!h.urlToId.hasOwnProperty(t))continue;e.urlToId[t]=h.urlToId[t]}for(t in h.stateToId){if(!h.stateToId.hasOwnProperty(t))continue;e.stateToId[t]=h.stateToId[t]}h.store=e,h.normalizeStore(),n=l.stringify(e);try{s.setItem("History.store",n)}catch(i){if(i.code!==DOMException.QUOTA_EXCEEDED_ERR)throw i;s.length&&(s.removeItem("History.store"),s.setItem("History.store",n))}},h.intervalList.push(a(h.onUnload,h.options.storeInterval)),h.Adapter.bind(e,"beforeunload",h.onUnload),h.Adapter.bind(e,"unload",h.onUnload));if(!h.emulated.pushState){h.bugs.safariPoll&&h.intervalList.push(a(h.safariStatePoll,h.options.safariPollInterval));if(i.vendor==="Apple Computer, Inc."||(i.appCodeName||"")==="Mozilla")h.Adapter.bind(e,"hashchange",function(){h.Adapter.trigger(e,"popstate")}),h.getHash()&&h.Adapter.onDomLoad(function(){h.Adapter.trigger(e,"hashchange")})}},(!h.options||!h.options.delayInit)&&h.init()}(window)

},{}],5:[function(require,module,exports){
/*!
 * BIDI embedding support for jQuery.i18n
 *
 * Copyright (C) 2015, David Chan
 *
 * This code is dual licensed GPLv2 or later and MIT. You don't have to do
 * anything special to choose one license or the other and you don't have to
 * notify anyone which license you are using. You are free to use this code
 * in commercial projects as long as the copyright header is left intact.
 * See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */

( function ( $ ) {
	'use strict';
	var strongDirRegExp;

	/**
	 * Matches the first strong directionality codepoint:
	 * - in group 1 if it is LTR
	 * - in group 2 if it is RTL
	 * Does not match if there is no strong directionality codepoint.
	 *
	 * Generated by UnicodeJS (see tools/strongDir) from the UCD; see
	 * https://phabricator.wikimedia.org/diffusion/GUJS/ .
	 */
	strongDirRegExp = new RegExp(
		'(?:' +
			'(' +
				'[\u0041-\u005a\u0061-\u007a\u00aa\u00b5\u00ba\u00c0-\u00d6\u00d8-\u00f6\u00f8-\u02b8\u02bb-\u02c1\u02d0\u02d1\u02e0-\u02e4\u02ee\u0370-\u0373\u0376\u0377\u037a-\u037d\u037f\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0482\u048a-\u052f\u0531-\u0556\u0559-\u055f\u0561-\u0587\u0589\u0903-\u0939\u093b\u093d-\u0940\u0949-\u094c\u094e-\u0950\u0958-\u0961\u0964-\u0980\u0982\u0983\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd-\u09c0\u09c7\u09c8\u09cb\u09cc\u09ce\u09d7\u09dc\u09dd\u09df-\u09e1\u09e6-\u09f1\u09f4-\u09fa\u0a03\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a3e-\u0a40\u0a59-\u0a5c\u0a5e\u0a66-\u0a6f\u0a72-\u0a74\u0a83\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd-\u0ac0\u0ac9\u0acb\u0acc\u0ad0\u0ae0\u0ae1\u0ae6-\u0af0\u0af9\u0b02\u0b03\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b3e\u0b40\u0b47\u0b48\u0b4b\u0b4c\u0b57\u0b5c\u0b5d\u0b5f-\u0b61\u0b66-\u0b77\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bbe\u0bbf\u0bc1\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcc\u0bd0\u0bd7\u0be6-\u0bf2\u0c01-\u0c03\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c39\u0c3d\u0c41-\u0c44\u0c58-\u0c5a\u0c60\u0c61\u0c66-\u0c6f\u0c7f\u0c82\u0c83\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd-\u0cc4\u0cc6-\u0cc8\u0cca\u0ccb\u0cd5\u0cd6\u0cde\u0ce0\u0ce1\u0ce6-\u0cef\u0cf1\u0cf2\u0d02\u0d03\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d-\u0d40\u0d46-\u0d48\u0d4a-\u0d4c\u0d4e\u0d57\u0d5f-\u0d61\u0d66-\u0d75\u0d79-\u0d7f\u0d82\u0d83\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0dcf-\u0dd1\u0dd8-\u0ddf\u0de6-\u0def\u0df2-\u0df4\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e4f-\u0e5b\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0ed0-\u0ed9\u0edc-\u0edf\u0f00-\u0f17\u0f1a-\u0f34\u0f36\u0f38\u0f3e-\u0f47\u0f49-\u0f6c\u0f7f\u0f85\u0f88-\u0f8c\u0fbe-\u0fc5\u0fc7-\u0fcc\u0fce-\u0fda\u1000-\u102c\u1031\u1038\u103b\u103c\u103f-\u1057\u105a-\u105d\u1061-\u1070\u1075-\u1081\u1083\u1084\u1087-\u108c\u108e-\u109c\u109e-\u10c5\u10c7\u10cd\u10d0-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1360-\u137c\u1380-\u138f\u13a0-\u13f5\u13f8-\u13fd\u1401-\u167f\u1681-\u169a\u16a0-\u16f8\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1735\u1736\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17b6\u17be-\u17c5\u17c7\u17c8\u17d4-\u17da\u17dc\u17e0-\u17e9\u1810-\u1819\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191e\u1923-\u1926\u1929-\u192b\u1930\u1931\u1933-\u1938\u1946-\u196d\u1970-\u1974\u1980-\u19ab\u19b0-\u19c9\u19d0-\u19da\u1a00-\u1a16\u1a19\u1a1a\u1a1e-\u1a55\u1a57\u1a61\u1a63\u1a64\u1a6d-\u1a72\u1a80-\u1a89\u1a90-\u1a99\u1aa0-\u1aad\u1b04-\u1b33\u1b35\u1b3b\u1b3d-\u1b41\u1b43-\u1b4b\u1b50-\u1b6a\u1b74-\u1b7c\u1b82-\u1ba1\u1ba6\u1ba7\u1baa\u1bae-\u1be5\u1be7\u1bea-\u1bec\u1bee\u1bf2\u1bf3\u1bfc-\u1c2b\u1c34\u1c35\u1c3b-\u1c49\u1c4d-\u1c7f\u1cc0-\u1cc7\u1cd3\u1ce1\u1ce9-\u1cec\u1cee-\u1cf3\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u200e\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u214f\u2160-\u2188\u2336-\u237a\u2395\u249c-\u24e9\u26ac\u2800-\u28ff\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d70\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u3005-\u3007\u3021-\u3029\u302e\u302f\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u3190-\u31ba\u31f0-\u321c\u3220-\u324f\u3260-\u327b\u327f-\u32b0\u32c0-\u32cb\u32d0-\u32fe\u3300-\u3376\u337b-\u33dd\u33e0-\u33fe\u3400-\u4db5\u4e00-\u9fd5\ua000-\ua48c\ua4d0-\ua60c\ua610-\ua62b\ua640-\ua66e\ua680-\ua69d\ua6a0-\ua6ef\ua6f2-\ua6f7\ua722-\ua787\ua789-\ua7ad\ua7b0-\ua7b7\ua7f7-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua824\ua827\ua830-\ua837\ua840-\ua873\ua880-\ua8c3\ua8ce-\ua8d9\ua8f2-\ua8fd\ua900-\ua925\ua92e-\ua946\ua952\ua953\ua95f-\ua97c\ua983-\ua9b2\ua9b4\ua9b5\ua9ba\ua9bb\ua9bd-\ua9cd\ua9cf-\ua9d9\ua9de-\ua9e4\ua9e6-\ua9fe\uaa00-\uaa28\uaa2f\uaa30\uaa33\uaa34\uaa40-\uaa42\uaa44-\uaa4b\uaa4d\uaa50-\uaa59\uaa5c-\uaa7b\uaa7d-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaaeb\uaaee-\uaaf5\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uab30-\uab65\uab70-\uabe4\uabe6\uabe7\uabe9-\uabec\uabf0-\uabf9\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\ue000-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc]|\ud800[\udc00-\udc0b]|\ud800[\udc0d-\udc26]|\ud800[\udc28-\udc3a]|\ud800\udc3c|\ud800\udc3d|\ud800[\udc3f-\udc4d]|\ud800[\udc50-\udc5d]|\ud800[\udc80-\udcfa]|\ud800\udd00|\ud800\udd02|\ud800[\udd07-\udd33]|\ud800[\udd37-\udd3f]|\ud800[\uddd0-\uddfc]|\ud800[\ude80-\ude9c]|\ud800[\udea0-\uded0]|\ud800[\udf00-\udf23]|\ud800[\udf30-\udf4a]|\ud800[\udf50-\udf75]|\ud800[\udf80-\udf9d]|\ud800[\udf9f-\udfc3]|\ud800[\udfc8-\udfd5]|\ud801[\udc00-\udc9d]|\ud801[\udca0-\udca9]|\ud801[\udd00-\udd27]|\ud801[\udd30-\udd63]|\ud801\udd6f|\ud801[\ude00-\udf36]|\ud801[\udf40-\udf55]|\ud801[\udf60-\udf67]|\ud804\udc00|\ud804[\udc02-\udc37]|\ud804[\udc47-\udc4d]|\ud804[\udc66-\udc6f]|\ud804[\udc82-\udcb2]|\ud804\udcb7|\ud804\udcb8|\ud804[\udcbb-\udcc1]|\ud804[\udcd0-\udce8]|\ud804[\udcf0-\udcf9]|\ud804[\udd03-\udd26]|\ud804\udd2c|\ud804[\udd36-\udd43]|\ud804[\udd50-\udd72]|\ud804[\udd74-\udd76]|\ud804[\udd82-\uddb5]|\ud804[\uddbf-\uddc9]|\ud804\uddcd|\ud804[\uddd0-\udddf]|\ud804[\udde1-\uddf4]|\ud804[\ude00-\ude11]|\ud804[\ude13-\ude2e]|\ud804\ude32|\ud804\ude33|\ud804\ude35|\ud804[\ude38-\ude3d]|\ud804[\ude80-\ude86]|\ud804\ude88|\ud804[\ude8a-\ude8d]|\ud804[\ude8f-\ude9d]|\ud804[\ude9f-\udea9]|\ud804[\udeb0-\udede]|\ud804[\udee0-\udee2]|\ud804[\udef0-\udef9]|\ud804\udf02|\ud804\udf03|\ud804[\udf05-\udf0c]|\ud804\udf0f|\ud804\udf10|\ud804[\udf13-\udf28]|\ud804[\udf2a-\udf30]|\ud804\udf32|\ud804\udf33|\ud804[\udf35-\udf39]|\ud804[\udf3d-\udf3f]|\ud804[\udf41-\udf44]|\ud804\udf47|\ud804\udf48|\ud804[\udf4b-\udf4d]|\ud804\udf50|\ud804\udf57|\ud804[\udf5d-\udf63]|\ud805[\udc80-\udcb2]|\ud805\udcb9|\ud805[\udcbb-\udcbe]|\ud805\udcc1|\ud805[\udcc4-\udcc7]|\ud805[\udcd0-\udcd9]|\ud805[\udd80-\uddb1]|\ud805[\uddb8-\uddbb]|\ud805\uddbe|\ud805[\uddc1-\udddb]|\ud805[\ude00-\ude32]|\ud805\ude3b|\ud805\ude3c|\ud805\ude3e|\ud805[\ude41-\ude44]|\ud805[\ude50-\ude59]|\ud805[\ude80-\udeaa]|\ud805\udeac|\ud805\udeae|\ud805\udeaf|\ud805\udeb6|\ud805[\udec0-\udec9]|\ud805[\udf00-\udf19]|\ud805\udf20|\ud805\udf21|\ud805\udf26|\ud805[\udf30-\udf3f]|\ud806[\udca0-\udcf2]|\ud806\udcff|\ud806[\udec0-\udef8]|\ud808[\udc00-\udf99]|\ud809[\udc00-\udc6e]|\ud809[\udc70-\udc74]|\ud809[\udc80-\udd43]|\ud80c[\udc00-\udfff]|\ud80d[\udc00-\udc2e]|\ud811[\udc00-\ude46]|\ud81a[\udc00-\ude38]|\ud81a[\ude40-\ude5e]|\ud81a[\ude60-\ude69]|\ud81a\ude6e|\ud81a\ude6f|\ud81a[\uded0-\udeed]|\ud81a\udef5|\ud81a[\udf00-\udf2f]|\ud81a[\udf37-\udf45]|\ud81a[\udf50-\udf59]|\ud81a[\udf5b-\udf61]|\ud81a[\udf63-\udf77]|\ud81a[\udf7d-\udf8f]|\ud81b[\udf00-\udf44]|\ud81b[\udf50-\udf7e]|\ud81b[\udf93-\udf9f]|\ud82c\udc00|\ud82c\udc01|\ud82f[\udc00-\udc6a]|\ud82f[\udc70-\udc7c]|\ud82f[\udc80-\udc88]|\ud82f[\udc90-\udc99]|\ud82f\udc9c|\ud82f\udc9f|\ud834[\udc00-\udcf5]|\ud834[\udd00-\udd26]|\ud834[\udd29-\udd66]|\ud834[\udd6a-\udd72]|\ud834\udd83|\ud834\udd84|\ud834[\udd8c-\udda9]|\ud834[\uddae-\udde8]|\ud834[\udf60-\udf71]|\ud835[\udc00-\udc54]|\ud835[\udc56-\udc9c]|\ud835\udc9e|\ud835\udc9f|\ud835\udca2|\ud835\udca5|\ud835\udca6|\ud835[\udca9-\udcac]|\ud835[\udcae-\udcb9]|\ud835\udcbb|\ud835[\udcbd-\udcc3]|\ud835[\udcc5-\udd05]|\ud835[\udd07-\udd0a]|\ud835[\udd0d-\udd14]|\ud835[\udd16-\udd1c]|\ud835[\udd1e-\udd39]|\ud835[\udd3b-\udd3e]|\ud835[\udd40-\udd44]|\ud835\udd46|\ud835[\udd4a-\udd50]|\ud835[\udd52-\udea5]|\ud835[\udea8-\udeda]|\ud835[\udedc-\udf14]|\ud835[\udf16-\udf4e]|\ud835[\udf50-\udf88]|\ud835[\udf8a-\udfc2]|\ud835[\udfc4-\udfcb]|\ud836[\udc00-\uddff]|\ud836[\ude37-\ude3a]|\ud836[\ude6d-\ude74]|\ud836[\ude76-\ude83]|\ud836[\ude85-\ude8b]|\ud83c[\udd10-\udd2e]|\ud83c[\udd30-\udd69]|\ud83c[\udd70-\udd9a]|\ud83c[\udde6-\ude02]|\ud83c[\ude10-\ude3a]|\ud83c[\ude40-\ude48]|\ud83c\ude50|\ud83c\ude51|[\ud840-\ud868][\udc00-\udfff]|\ud869[\udc00-\uded6]|\ud869[\udf00-\udfff]|[\ud86a-\ud86c][\udc00-\udfff]|\ud86d[\udc00-\udf34]|\ud86d[\udf40-\udfff]|\ud86e[\udc00-\udc1d]|\ud86e[\udc20-\udfff]|[\ud86f-\ud872][\udc00-\udfff]|\ud873[\udc00-\udea1]|\ud87e[\udc00-\ude1d]|[\udb80-\udbbe][\udc00-\udfff]|\udbbf[\udc00-\udffd]|[\udbc0-\udbfe][\udc00-\udfff]|\udbff[\udc00-\udffd]' +
			')|(' +
				'[\u0590\u05be\u05c0\u05c3\u05c6\u05c8-\u05ff\u07c0-\u07ea\u07f4\u07f5\u07fa-\u0815\u081a\u0824\u0828\u082e-\u0858\u085c-\u089f\u200f\ufb1d\ufb1f-\ufb28\ufb2a-\ufb4f\u0608\u060b\u060d\u061b-\u064a\u066d-\u066f\u0671-\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u0710\u0712-\u072f\u074b-\u07a5\u07b1-\u07bf\u08a0-\u08e2\ufb50-\ufd3d\ufd40-\ufdcf\ufdf0-\ufdfc\ufdfe\ufdff\ufe70-\ufefe]|\ud802[\udc00-\udd1e]|\ud802[\udd20-\ude00]|\ud802\ude04|\ud802[\ude07-\ude0b]|\ud802[\ude10-\ude37]|\ud802[\ude3b-\ude3e]|\ud802[\ude40-\udee4]|\ud802[\udee7-\udf38]|\ud802[\udf40-\udfff]|\ud803[\udc00-\ude5f]|\ud803[\ude7f-\udfff]|\ud83a[\udc00-\udccf]|\ud83a[\udcd7-\udfff]|\ud83b[\udc00-\uddff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\udf00-\udfff]|\ud83b[\ude00-\udeef]|\ud83b[\udef2-\udeff]' +
			')' +
		')'
	);

	/**
	 * Gets directionality of the first strongly directional codepoint
	 *
	 * This is the rule the BIDI algorithm uses to determine the directionality of
	 * paragraphs ( http://unicode.org/reports/tr9/#The_Paragraph_Level ) and
	 * FSI isolates ( http://unicode.org/reports/tr9/#Explicit_Directional_Isolates ).
	 *
	 * TODO: Does not handle BIDI control characters inside the text.
	 * TODO: Does not handle unallocated characters.
	 *
	 * @param {string} text The text from which to extract initial directionality.
	 * @return {string} Directionality (either 'ltr' or 'rtl')
	 */
	function strongDirFromContent( text ) {
		var m = text.match( strongDirRegExp );
		if ( !m ) {
			return null;
		}
		if ( m[ 2 ] === undefined ) {
			return 'ltr';
		}
		return 'rtl';
	}

	$.extend( $.i18n.parser.emitter, {
		/**
		 * Wraps argument with unicode control characters for directionality safety
		 *
		 * This solves the problem where directionality-neutral characters at the edge of
		 * the argument string get interpreted with the wrong directionality from the
		 * enclosing context, giving renderings that look corrupted like "(Ben_(WMF".
		 *
		 * The wrapping is LRE...PDF or RLE...PDF, depending on the detected
		 * directionality of the argument string, using the BIDI algorithm's own "First
		 * strong directional codepoint" rule. Essentially, this works round the fact that
		 * there is no embedding equivalent of U+2068 FSI (isolation with heuristic
		 * direction inference). The latter is cleaner but still not widely supported.
		 *
		 * @param {string[]} nodes The text nodes from which to take the first item.
		 * @return {string} Wrapped String of content as needed.
		 */
		bidi: function ( nodes ) {
			var dir = strongDirFromContent( nodes[ 0 ] );
			if ( dir === 'ltr' ) {
				// Wrap in LEFT-TO-RIGHT EMBEDDING ... POP DIRECTIONAL FORMATTING
				return '\u202A' + nodes[ 0 ] + '\u202C';
			}
			if ( dir === 'rtl' ) {
				// Wrap in RIGHT-TO-LEFT EMBEDDING ... POP DIRECTIONAL FORMATTING
				return '\u202B' + nodes[ 0 ] + '\u202C';
			}
			// No strong directionality: do not wrap
			return nodes[ 0 ];
		}
	} );
}( jQuery ) );

},{}],6:[function(require,module,exports){
/*!
 * jQuery Internationalization library
 *
 * Copyright (C) 2011-2013 Santhosh Thottingal, Neil Kandalgaonkar
 *
 * jquery.i18n is dual licensed GPLv2 or later and MIT. You don't have to do
 * anything special to choose one license or the other and you don't have to
 * notify anyone which license you are using. You are free to use
 * UniversalLanguageSelector in commercial projects as long as the copyright
 * header is left intact. See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */

( function ( $ ) {
	'use strict';

	var MessageParserEmitter = function () {
		this.language = $.i18n.languages[ String.locale ] || $.i18n.languages[ 'default' ];
	};

	MessageParserEmitter.prototype = {
		constructor: MessageParserEmitter,

		/**
		 * (We put this method definition here, and not in prototype, to make
		 * sure it's not overwritten by any magic.) Walk entire node structure,
		 * applying replacements and template functions when appropriate
		 *
		 * @param {Mixed} node abstract syntax tree (top node or subnode)
		 * @param {Array} replacements for $1, $2, ... $n
		 * @return {Mixed} single-string node or array of nodes suitable for
		 *  jQuery appending.
		 */
		emit: function ( node, replacements ) {
			var ret, subnodes, operation,
				messageParserEmitter = this;

			switch ( typeof node ) {
				case 'string':
				case 'number':
					ret = node;
					break;
				case 'object':
				// node is an array of nodes
					subnodes = $.map( node.slice( 1 ), function ( n ) {
						return messageParserEmitter.emit( n, replacements );
					} );

					operation = node[ 0 ].toLowerCase();

					if ( typeof messageParserEmitter[ operation ] === 'function' ) {
						ret = messageParserEmitter[ operation ]( subnodes, replacements );
					} else {
						throw new Error( 'unknown operation "' + operation + '"' );
					}

					break;
				case 'undefined':
				// Parsing the empty string (as an entire expression, or as a
				// paramExpression in a template) results in undefined
				// Perhaps a more clever parser can detect this, and return the
				// empty string? Or is that useful information?
				// The logical thing is probably to return the empty string here
				// when we encounter undefined.
					ret = '';
					break;
				default:
					throw new Error( 'unexpected type in AST: ' + typeof node );
			}

			return ret;
		},

		/**
		 * Parsing has been applied depth-first we can assume that all nodes
		 * here are single nodes Must return a single node to parents -- a
		 * jQuery with synthetic span However, unwrap any other synthetic spans
		 * in our children and pass them upwards
		 *
		 * @param {Array} nodes Mixed, some single nodes, some arrays of nodes.
		 * @return {string}
		 */
		concat: function ( nodes ) {
			var result = '';

			$.each( nodes, function ( i, node ) {
				// strings, integers, anything else
				result += node;
			} );

			return result;
		},

		/**
		 * Return escaped replacement of correct index, or string if
		 * unavailable. Note that we expect the parsed parameter to be
		 * zero-based. i.e. $1 should have become [ 0 ]. if the specified
		 * parameter is not found return the same string (e.g. "$99" ->
		 * parameter 98 -> not found -> return "$99" ) TODO throw error if
		 * nodes.length > 1 ?
		 *
		 * @param {Array} nodes One element, integer, n >= 0
		 * @param {Array} replacements for $1, $2, ... $n
		 * @return {string} replacement
		 */
		replace: function ( nodes, replacements ) {
			var index = parseInt( nodes[ 0 ], 10 );

			if ( index < replacements.length ) {
				// replacement is not a string, don't touch!
				return replacements[ index ];
			} else {
				// index not found, fallback to displaying variable
				return '$' + ( index + 1 );
			}
		},

		/**
		 * Transform parsed structure into pluralization n.b. The first node may
		 * be a non-integer (for instance, a string representing an Arabic
		 * number). So convert it back with the current language's
		 * convertNumber.
		 *
		 * @param {Array} nodes List [ {String|Number}, {String}, {String} ... ]
		 * @return {string} selected pluralized form according to current
		 *  language.
		 */
		plural: function ( nodes ) {
			var count = parseFloat( this.language.convertNumber( nodes[ 0 ], 10 ) ),
				forms = nodes.slice( 1 );

			return forms.length ? this.language.convertPlural( count, forms ) : '';
		},

		/**
		 * Transform parsed structure into gender Usage
		 * {{gender:gender|masculine|feminine|neutral}}.
		 *
		 * @param {Array} nodes List [ {String}, {String}, {String} , {String} ]
		 * @return {string} selected gender form according to current language
		 */
		gender: function ( nodes ) {
			var gender = nodes[ 0 ],
				forms = nodes.slice( 1 );

			return this.language.gender( gender, forms );
		},

		/**
		 * Transform parsed structure into grammar conversion. Invoked by
		 * putting {{grammar:form|word}} in a message
		 *
		 * @param {Array} nodes List [{Grammar case eg: genitive}, {String word}]
		 * @return {string} selected grammatical form according to current
		 *  language.
		 */
		grammar: function ( nodes ) {
			var form = nodes[ 0 ],
				word = nodes[ 1 ];

			return word && form && this.language.convertGrammar( word, form );
		}
	};

	$.extend( $.i18n.parser.emitter, new MessageParserEmitter() );
}( jQuery ) );

},{}],7:[function(require,module,exports){
/*!
 * jQuery Internationalization library
 *
 * Copyright (C) 2012 Santhosh Thottingal
 *
 * jquery.i18n is dual licensed GPLv2 or later and MIT. You don't have to do anything special to
 * choose one license or the other and you don't have to notify anyone which license you are using.
 * You are free to use UniversalLanguageSelector in commercial projects as long as the copyright
 * header is left intact. See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */
( function ( $ ) {
	'use strict';

	$.i18n = $.i18n || {};
	$.extend( $.i18n.fallbacks, {
		ab: [ 'ru' ],
		ace: [ 'id' ],
		aln: [ 'sq' ],
		// Not so standard - als is supposed to be Tosk Albanian,
		// but in Wikipedia it's used for a Germanic language.
		als: [ 'gsw', 'de' ],
		an: [ 'es' ],
		anp: [ 'hi' ],
		arn: [ 'es' ],
		arz: [ 'ar' ],
		av: [ 'ru' ],
		ay: [ 'es' ],
		ba: [ 'ru' ],
		bar: [ 'de' ],
		'bat-smg': [ 'sgs', 'lt' ],
		bcc: [ 'fa' ],
		'be-x-old': [ 'be-tarask' ],
		bh: [ 'bho' ],
		bjn: [ 'id' ],
		bm: [ 'fr' ],
		bpy: [ 'bn' ],
		bqi: [ 'fa' ],
		bug: [ 'id' ],
		'cbk-zam': [ 'es' ],
		ce: [ 'ru' ],
		crh: [ 'crh-latn' ],
		'crh-cyrl': [ 'ru' ],
		csb: [ 'pl' ],
		cv: [ 'ru' ],
		'de-at': [ 'de' ],
		'de-ch': [ 'de' ],
		'de-formal': [ 'de' ],
		dsb: [ 'de' ],
		dtp: [ 'ms' ],
		egl: [ 'it' ],
		eml: [ 'it' ],
		ff: [ 'fr' ],
		fit: [ 'fi' ],
		'fiu-vro': [ 'vro', 'et' ],
		frc: [ 'fr' ],
		frp: [ 'fr' ],
		frr: [ 'de' ],
		fur: [ 'it' ],
		gag: [ 'tr' ],
		gan: [ 'gan-hant', 'zh-hant', 'zh-hans' ],
		'gan-hans': [ 'zh-hans' ],
		'gan-hant': [ 'zh-hant', 'zh-hans' ],
		gl: [ 'pt' ],
		glk: [ 'fa' ],
		gn: [ 'es' ],
		gsw: [ 'de' ],
		hif: [ 'hif-latn' ],
		hsb: [ 'de' ],
		ht: [ 'fr' ],
		ii: [ 'zh-cn', 'zh-hans' ],
		inh: [ 'ru' ],
		iu: [ 'ike-cans' ],
		jut: [ 'da' ],
		jv: [ 'id' ],
		kaa: [ 'kk-latn', 'kk-cyrl' ],
		kbd: [ 'kbd-cyrl' ],
		khw: [ 'ur' ],
		kiu: [ 'tr' ],
		kk: [ 'kk-cyrl' ],
		'kk-arab': [ 'kk-cyrl' ],
		'kk-latn': [ 'kk-cyrl' ],
		'kk-cn': [ 'kk-arab', 'kk-cyrl' ],
		'kk-kz': [ 'kk-cyrl' ],
		'kk-tr': [ 'kk-latn', 'kk-cyrl' ],
		kl: [ 'da' ],
		'ko-kp': [ 'ko' ],
		koi: [ 'ru' ],
		krc: [ 'ru' ],
		ks: [ 'ks-arab' ],
		ksh: [ 'de' ],
		ku: [ 'ku-latn' ],
		'ku-arab': [ 'ckb' ],
		kv: [ 'ru' ],
		lad: [ 'es' ],
		lb: [ 'de' ],
		lbe: [ 'ru' ],
		lez: [ 'ru' ],
		li: [ 'nl' ],
		lij: [ 'it' ],
		liv: [ 'et' ],
		lmo: [ 'it' ],
		ln: [ 'fr' ],
		ltg: [ 'lv' ],
		lzz: [ 'tr' ],
		mai: [ 'hi' ],
		'map-bms': [ 'jv', 'id' ],
		mg: [ 'fr' ],
		mhr: [ 'ru' ],
		min: [ 'id' ],
		mo: [ 'ro' ],
		mrj: [ 'ru' ],
		mwl: [ 'pt' ],
		myv: [ 'ru' ],
		mzn: [ 'fa' ],
		nah: [ 'es' ],
		nap: [ 'it' ],
		nds: [ 'de' ],
		'nds-nl': [ 'nl' ],
		'nl-informal': [ 'nl' ],
		no: [ 'nb' ],
		os: [ 'ru' ],
		pcd: [ 'fr' ],
		pdc: [ 'de' ],
		pdt: [ 'de' ],
		pfl: [ 'de' ],
		pms: [ 'it' ],
		pt: [ 'pt-br' ],
		'pt-br': [ 'pt' ],
		qu: [ 'es' ],
		qug: [ 'qu', 'es' ],
		rgn: [ 'it' ],
		rmy: [ 'ro' ],
		'roa-rup': [ 'rup' ],
		rue: [ 'uk', 'ru' ],
		ruq: [ 'ruq-latn', 'ro' ],
		'ruq-cyrl': [ 'mk' ],
		'ruq-latn': [ 'ro' ],
		sa: [ 'hi' ],
		sah: [ 'ru' ],
		scn: [ 'it' ],
		sg: [ 'fr' ],
		sgs: [ 'lt' ],
		sli: [ 'de' ],
		sr: [ 'sr-ec' ],
		srn: [ 'nl' ],
		stq: [ 'de' ],
		su: [ 'id' ],
		szl: [ 'pl' ],
		tcy: [ 'kn' ],
		tg: [ 'tg-cyrl' ],
		tt: [ 'tt-cyrl', 'ru' ],
		'tt-cyrl': [ 'ru' ],
		ty: [ 'fr' ],
		udm: [ 'ru' ],
		ug: [ 'ug-arab' ],
		uk: [ 'ru' ],
		vec: [ 'it' ],
		vep: [ 'et' ],
		vls: [ 'nl' ],
		vmf: [ 'de' ],
		vot: [ 'fi' ],
		vro: [ 'et' ],
		wa: [ 'fr' ],
		wo: [ 'fr' ],
		wuu: [ 'zh-hans' ],
		xal: [ 'ru' ],
		xmf: [ 'ka' ],
		yi: [ 'he' ],
		za: [ 'zh-hans' ],
		zea: [ 'nl' ],
		zh: [ 'zh-hans' ],
		'zh-classical': [ 'lzh' ],
		'zh-cn': [ 'zh-hans' ],
		'zh-hant': [ 'zh-hans' ],
		'zh-hk': [ 'zh-hant', 'zh-hans' ],
		'zh-min-nan': [ 'nan' ],
		'zh-mo': [ 'zh-hk', 'zh-hant', 'zh-hans' ],
		'zh-my': [ 'zh-sg', 'zh-hans' ],
		'zh-sg': [ 'zh-hans' ],
		'zh-tw': [ 'zh-hant', 'zh-hans' ],
		'zh-yue': [ 'yue' ]
	} );
}( jQuery ) );

},{}],8:[function(require,module,exports){
/*!
 * jQuery Internationalization library
 *
 * Copyright (C) 2012 Santhosh Thottingal
 *
 * jquery.i18n is dual licensed GPLv2 or later and MIT. You don't have to do
 * anything special to choose one license or the other and you don't have to
 * notify anyone which license you are using. You are free to use
 * UniversalLanguageSelector in commercial projects as long as the copyright
 * header is left intact. See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */

( function ( $ ) {
	'use strict';

	var nav, I18N,
		slice = Array.prototype.slice;
	/**
	 * @constructor
	 * @param {Object} options
	 */
	I18N = function ( options ) {
		// Load defaults
		this.options = $.extend( {}, I18N.defaults, options );

		this.parser = this.options.parser;
		this.locale = this.options.locale;
		this.messageStore = this.options.messageStore;
		this.languages = {};

		this.init();
	};

	I18N.prototype = {
		/**
		 * Initialize by loading locales and setting up
		 * String.prototype.toLocaleString and String.locale.
		 */
		init: function () {
			var i18n = this;

			// Set locale of String environment
			String.locale = i18n.locale;

			// Override String.localeString method
			String.prototype.toLocaleString = function () {
				var localeParts, localePartIndex, value, locale, fallbackIndex,
					tryingLocale, message;

				value = this.valueOf();
				locale = i18n.locale;
				fallbackIndex = 0;

				while ( locale ) {
					// Iterate through locales starting at most-specific until
					// localization is found. As in fi-Latn-FI, fi-Latn and fi.
					localeParts = locale.split( '-' );
					localePartIndex = localeParts.length;

					do {
						tryingLocale = localeParts.slice( 0, localePartIndex ).join( '-' );
						message = i18n.messageStore.get( tryingLocale, value );

						if ( message ) {
							return message;
						}

						localePartIndex--;
					} while ( localePartIndex );

					if ( locale === 'en' ) {
						break;
					}

					locale = ( $.i18n.fallbacks[ i18n.locale ] && $.i18n.fallbacks[ i18n.locale ][ fallbackIndex ] ) ||
						i18n.options.fallbackLocale;
					$.i18n.log( 'Trying fallback locale for ' + i18n.locale + ': ' + locale + ' (' + value + ')' );

					fallbackIndex++;
				}

				// key not found
				return '';
			};
		},

		/*
		 * Destroy the i18n instance.
		 */
		destroy: function () {
			$.removeData( document, 'i18n' );
		},

		/**
		 * General message loading API This can take a URL string for
		 * the json formatted messages. Example:
		 * <code>load('path/to/all_localizations.json');</code>
		 *
		 * To load a localization file for a locale:
		 * <code>
		 * load('path/to/de-messages.json', 'de' );
		 * </code>
		 *
		 * To load a localization file from a directory:
		 * <code>
		 * load('path/to/i18n/directory', 'de' );
		 * </code>
		 * The above method has the advantage of fallback resolution.
		 * ie, it will automatically load the fallback locales for de.
		 * For most usecases, this is the recommended method.
		 * It is optional to have trailing slash at end.
		 *
		 * A data object containing message key- message translation mappings
		 * can also be passed. Example:
		 * <code>
		 * load( { 'hello' : 'Hello' }, optionalLocale );
		 * </code>
		 *
		 * A source map containing key-value pair of languagename and locations
		 * can also be passed. Example:
		 * <code>
		 * load( {
		 * bn: 'i18n/bn.json',
		 * he: 'i18n/he.json',
		 * en: 'i18n/en.json'
		 * } )
		 * </code>
		 *
		 * If the data argument is null/undefined/false,
		 * all cached messages for the i18n instance will get reset.
		 *
		 * @param {string|Object} source
		 * @param {string} locale Language tag
		 * @return {jQuery.Promise}
		 */
		load: function ( source, locale ) {
			var fallbackLocales, locIndex, fallbackLocale, sourceMap = {};
			if ( !source && !locale ) {
				source = 'i18n/' + $.i18n().locale + '.json';
				locale = $.i18n().locale;
			}
			if ( typeof source === 'string' &&
				// source extension should be json, but can have query params after that.
				source.split( '?' )[ 0 ].split( '.' ).pop() !== 'json'
			) {
				// Load specified locale then check for fallbacks when directory is specified in load()
				sourceMap[ locale ] = source + '/' + locale + '.json';
				fallbackLocales = ( $.i18n.fallbacks[ locale ] || [] )
					.concat( this.options.fallbackLocale );
				for ( locIndex = 0; locIndex < fallbackLocales.length; locIndex++ ) {
					fallbackLocale = fallbackLocales[ locIndex ];
					sourceMap[ fallbackLocale ] = source + '/' + fallbackLocale + '.json';
				}
				return this.load( sourceMap );
			} else {
				return this.messageStore.load( source, locale );
			}

		},

		/**
		 * Does parameter and magic word substitution.
		 *
		 * @param {string} key Message key
		 * @param {Array} parameters Message parameters
		 * @return {string}
		 */
		parse: function ( key, parameters ) {
			var message = key.toLocaleString();
			// FIXME: This changes the state of the I18N object,
			// should probably not change the 'this.parser' but just
			// pass it to the parser.
			this.parser.language = $.i18n.languages[ $.i18n().locale ] || $.i18n.languages[ 'default' ];
			if ( message === '' ) {
				message = key;
			}
			return this.parser.parse( message, parameters );
		}
	};

	/**
	 * Process a message from the $.I18N instance
	 * for the current document, stored in jQuery.data(document).
	 *
	 * @param {string} key Key of the message.
	 * @param {string} param1 [param...] Variadic list of parameters for {key}.
	 * @return {string|$.I18N} Parsed message, or if no key was given
	 * the instance of $.I18N is returned.
	 */
	$.i18n = function ( key, param1 ) {
		var parameters,
			i18n = $.data( document, 'i18n' ),
			options = typeof key === 'object' && key;

		// If the locale option for this call is different then the setup so far,
		// update it automatically. This doesn't just change the context for this
		// call but for all future call as well.
		// If there is no i18n setup yet, don't do this. It will be taken care of
		// by the `new I18N` construction below.
		// NOTE: It should only change language for this one call.
		// Then cache instances of I18N somewhere.
		if ( options && options.locale && i18n && i18n.locale !== options.locale ) {
			String.locale = i18n.locale = options.locale;
		}

		if ( !i18n ) {
			i18n = new I18N( options );
			$.data( document, 'i18n', i18n );
		}

		if ( typeof key === 'string' ) {
			if ( param1 !== undefined ) {
				parameters = slice.call( arguments, 1 );
			} else {
				parameters = [];
			}

			return i18n.parse( key, parameters );
		} else {
			// FIXME: remove this feature/bug.
			return i18n;
		}
	};

	$.fn.i18n = function () {
		var i18n = $.data( document, 'i18n' );

		if ( !i18n ) {
			i18n = new I18N();
			$.data( document, 'i18n', i18n );
		}
		String.locale = i18n.locale;
		return this.each( function () {
			var $this = $( this ),
				messageKey = $this.data( 'i18n' ),
				lBracket, rBracket, type, key;

			if ( messageKey ) {
				lBracket = messageKey.indexOf( '[' );
				rBracket = messageKey.indexOf( ']' );
				if ( lBracket !== -1 && rBracket !== -1 && lBracket < rBracket ) {
					type = messageKey.slice( lBracket + 1, rBracket );
					key = messageKey.slice( rBracket + 1 );
					if ( type === 'html' ) {
						$this.html( i18n.parse( key ) );
					} else {
						$this.attr( type, i18n.parse( key ) );
					}
				} else {
					$this.text( i18n.parse( messageKey ) );
				}
			} else {
				$this.find( '[data-i18n]' ).i18n();
			}
		} );
	};

	String.locale = String.locale || $( 'html' ).attr( 'lang' );

	if ( !String.locale ) {
		if ( typeof window.navigator !== undefined ) {
			nav = window.navigator;
			String.locale = nav.language || nav.userLanguage || '';
		} else {
			String.locale = '';
		}
	}

	$.i18n.languages = {};
	$.i18n.messageStore = $.i18n.messageStore || {};
	$.i18n.parser = {
		// The default parser only handles variable substitution
		parse: function ( message, parameters ) {
			return message.replace( /\$(\d+)/g, function ( str, match ) {
				var index = parseInt( match, 10 ) - 1;
				return parameters[ index ] !== undefined ? parameters[ index ] : '$' + match;
			} );
		},
		emitter: {}
	};
	$.i18n.fallbacks = {};
	$.i18n.debug = false;
	$.i18n.log = function ( /* arguments */ ) {
		if ( window.console && $.i18n.debug ) {
			window.console.log.apply( window.console, arguments );
		}
	};
	/* Static members */
	I18N.defaults = {
		locale: String.locale,
		fallbackLocale: 'en',
		parser: $.i18n.parser,
		messageStore: $.i18n.messageStore
	};

	// Expose constructor
	$.i18n.constructor = I18N;
}( jQuery ) );

},{}],9:[function(require,module,exports){
/* global pluralRuleParser */
( function ( $ ) {
	'use strict';

	// jscs:disable
	var language = {
		// CLDR plural rules generated using
		// libs/CLDRPluralRuleParser/tools/PluralXML2JSON.html
		pluralRules: {
			ak: {
				one: 'n = 0..1'
			},
			am: {
				one: 'i = 0 or n = 1'
			},
			ar: {
				zero: 'n = 0',
				one: 'n = 1',
				two: 'n = 2',
				few: 'n % 100 = 3..10',
				many: 'n % 100 = 11..99'
			},
			ars: {
				zero: 'n = 0',
				one: 'n = 1',
				two: 'n = 2',
				few: 'n % 100 = 3..10',
				many: 'n % 100 = 11..99'
			},
			as: {
				one: 'i = 0 or n = 1'
			},
			be: {
				one: 'n % 10 = 1 and n % 100 != 11',
				few: 'n % 10 = 2..4 and n % 100 != 12..14',
				many: 'n % 10 = 0 or n % 10 = 5..9 or n % 100 = 11..14'
			},
			bh: {
				one: 'n = 0..1'
			},
			bn: {
				one: 'i = 0 or n = 1'
			},
			br: {
				one: 'n % 10 = 1 and n % 100 != 11,71,91',
				two: 'n % 10 = 2 and n % 100 != 12,72,92',
				few: 'n % 10 = 3..4,9 and n % 100 != 10..19,70..79,90..99',
				many: 'n != 0 and n % 1000000 = 0'
			},
			bs: {
				one: 'v = 0 and i % 10 = 1 and i % 100 != 11 or f % 10 = 1 and f % 100 != 11',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14 or f % 10 = 2..4 and f % 100 != 12..14'
			},
			cs: {
				one: 'i = 1 and v = 0',
				few: 'i = 2..4 and v = 0',
				many: 'v != 0'
			},
			cy: {
				zero: 'n = 0',
				one: 'n = 1',
				two: 'n = 2',
				few: 'n = 3',
				many: 'n = 6'
			},
			da: {
				one: 'n = 1 or t != 0 and i = 0,1'
			},
			dsb: {
				one: 'v = 0 and i % 100 = 1 or f % 100 = 1',
				two: 'v = 0 and i % 100 = 2 or f % 100 = 2',
				few: 'v = 0 and i % 100 = 3..4 or f % 100 = 3..4'
			},
			fa: {
				one: 'i = 0 or n = 1'
			},
			ff: {
				one: 'i = 0,1'
			},
			fil: {
				one: 'v = 0 and i = 1,2,3 or v = 0 and i % 10 != 4,6,9 or v != 0 and f % 10 != 4,6,9'
			},
			fr: {
				one: 'i = 0,1'
			},
			ga: {
				one: 'n = 1',
				two: 'n = 2',
				few: 'n = 3..6',
				many: 'n = 7..10'
			},
			gd: {
				one: 'n = 1,11',
				two: 'n = 2,12',
				few: 'n = 3..10,13..19'
			},
			gu: {
				one: 'i = 0 or n = 1'
			},
			guw: {
				one: 'n = 0..1'
			},
			gv: {
				one: 'v = 0 and i % 10 = 1',
				two: 'v = 0 and i % 10 = 2',
				few: 'v = 0 and i % 100 = 0,20,40,60,80',
				many: 'v != 0'
			},
			he: {
				one: 'i = 1 and v = 0',
				two: 'i = 2 and v = 0',
				many: 'v = 0 and n != 0..10 and n % 10 = 0'
			},
			hi: {
				one: 'i = 0 or n = 1'
			},
			hr: {
				one: 'v = 0 and i % 10 = 1 and i % 100 != 11 or f % 10 = 1 and f % 100 != 11',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14 or f % 10 = 2..4 and f % 100 != 12..14'
			},
			hsb: {
				one: 'v = 0 and i % 100 = 1 or f % 100 = 1',
				two: 'v = 0 and i % 100 = 2 or f % 100 = 2',
				few: 'v = 0 and i % 100 = 3..4 or f % 100 = 3..4'
			},
			hy: {
				one: 'i = 0,1'
			},
			is: {
				one: 't = 0 and i % 10 = 1 and i % 100 != 11 or t != 0'
			},
			iu: {
				one: 'n = 1',
				two: 'n = 2'
			},
			iw: {
				one: 'i = 1 and v = 0',
				two: 'i = 2 and v = 0',
				many: 'v = 0 and n != 0..10 and n % 10 = 0'
			},
			kab: {
				one: 'i = 0,1'
			},
			kn: {
				one: 'i = 0 or n = 1'
			},
			kw: {
				one: 'n = 1',
				two: 'n = 2'
			},
			lag: {
				zero: 'n = 0',
				one: 'i = 0,1 and n != 0'
			},
			ln: {
				one: 'n = 0..1'
			},
			lt: {
				one: 'n % 10 = 1 and n % 100 != 11..19',
				few: 'n % 10 = 2..9 and n % 100 != 11..19',
				many: 'f != 0'
			},
			lv: {
				zero: 'n % 10 = 0 or n % 100 = 11..19 or v = 2 and f % 100 = 11..19',
				one: 'n % 10 = 1 and n % 100 != 11 or v = 2 and f % 10 = 1 and f % 100 != 11 or v != 2 and f % 10 = 1'
			},
			mg: {
				one: 'n = 0..1'
			},
			mk: {
				one: 'v = 0 and i % 10 = 1 or f % 10 = 1'
			},
			mo: {
				one: 'i = 1 and v = 0',
				few: 'v != 0 or n = 0 or n != 1 and n % 100 = 1..19'
			},
			mr: {
				one: 'i = 0 or n = 1'
			},
			mt: {
				one: 'n = 1',
				few: 'n = 0 or n % 100 = 2..10',
				many: 'n % 100 = 11..19'
			},
			naq: {
				one: 'n = 1',
				two: 'n = 2'
			},
			nso: {
				one: 'n = 0..1'
			},
			pa: {
				one: 'n = 0..1'
			},
			pl: {
				one: 'i = 1 and v = 0',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14',
				many: 'v = 0 and i != 1 and i % 10 = 0..1 or v = 0 and i % 10 = 5..9 or v = 0 and i % 100 = 12..14'
			},
			prg: {
				zero: 'n % 10 = 0 or n % 100 = 11..19 or v = 2 and f % 100 = 11..19',
				one: 'n % 10 = 1 and n % 100 != 11 or v = 2 and f % 10 = 1 and f % 100 != 11 or v != 2 and f % 10 = 1'
			},
			pt: {
				one: 'i = 0..1'
			},
			ro: {
				one: 'i = 1 and v = 0',
				few: 'v != 0 or n = 0 or n != 1 and n % 100 = 1..19'
			},
			ru: {
				one: 'v = 0 and i % 10 = 1 and i % 100 != 11',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14',
				many: 'v = 0 and i % 10 = 0 or v = 0 and i % 10 = 5..9 or v = 0 and i % 100 = 11..14'
			},
			se: {
				one: 'n = 1',
				two: 'n = 2'
			},
			sh: {
				one: 'v = 0 and i % 10 = 1 and i % 100 != 11 or f % 10 = 1 and f % 100 != 11',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14 or f % 10 = 2..4 and f % 100 != 12..14'
			},
			shi: {
				one: 'i = 0 or n = 1',
				few: 'n = 2..10'
			},
			si: {
				one: 'n = 0,1 or i = 0 and f = 1'
			},
			sk: {
				one: 'i = 1 and v = 0',
				few: 'i = 2..4 and v = 0',
				many: 'v != 0'
			},
			sl: {
				one: 'v = 0 and i % 100 = 1',
				two: 'v = 0 and i % 100 = 2',
				few: 'v = 0 and i % 100 = 3..4 or v != 0'
			},
			sma: {
				one: 'n = 1',
				two: 'n = 2'
			},
			smi: {
				one: 'n = 1',
				two: 'n = 2'
			},
			smj: {
				one: 'n = 1',
				two: 'n = 2'
			},
			smn: {
				one: 'n = 1',
				two: 'n = 2'
			},
			sms: {
				one: 'n = 1',
				two: 'n = 2'
			},
			sr: {
				one: 'v = 0 and i % 10 = 1 and i % 100 != 11 or f % 10 = 1 and f % 100 != 11',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14 or f % 10 = 2..4 and f % 100 != 12..14'
			},
			ti: {
				one: 'n = 0..1'
			},
			tl: {
				one: 'v = 0 and i = 1,2,3 or v = 0 and i % 10 != 4,6,9 or v != 0 and f % 10 != 4,6,9'
			},
			tzm: {
				one: 'n = 0..1 or n = 11..99'
			},
			uk: {
				one: 'v = 0 and i % 10 = 1 and i % 100 != 11',
				few: 'v = 0 and i % 10 = 2..4 and i % 100 != 12..14',
				many: 'v = 0 and i % 10 = 0 or v = 0 and i % 10 = 5..9 or v = 0 and i % 100 = 11..14'
			},
			wa: {
				one: 'n = 0..1'
			},
			zu: {
				one: 'i = 0 or n = 1'
			}
		},
		// jscs:enable

		/**
		 * Plural form transformations, needed for some languages.
		 *
		 * @param {integer} count
		 *            Non-localized quantifier
		 * @param {Array} forms
		 *            List of plural forms
		 * @return {string} Correct form for quantifier in this language
		 */
		convertPlural: function ( count, forms ) {
			var pluralRules,
				pluralFormIndex,
				index,
				explicitPluralPattern = new RegExp( '\\d+=', 'i' ),
				formCount,
				form;

			if ( !forms || forms.length === 0 ) {
				return '';
			}

			// Handle for Explicit 0= & 1= values
			for ( index = 0; index < forms.length; index++ ) {
				form = forms[ index ];
				if ( explicitPluralPattern.test( form ) ) {
					formCount = parseInt( form.slice( 0, form.indexOf( '=' ) ), 10 );
					if ( formCount === count ) {
						return ( form.slice( form.indexOf( '=' ) + 1 ) );
					}
					forms[ index ] = undefined;
				}
			}

			forms = $.map( forms, function ( form ) {
				if ( form !== undefined ) {
					return form;
				}
			} );

			pluralRules = this.pluralRules[ $.i18n().locale ];

			if ( !pluralRules ) {
				// default fallback.
				return ( count === 1 ) ? forms[ 0 ] : forms[ 1 ];
			}

			pluralFormIndex = this.getPluralForm( count, pluralRules );
			pluralFormIndex = Math.min( pluralFormIndex, forms.length - 1 );

			return forms[ pluralFormIndex ];
		},

		/**
		 * For the number, get the plural for index
		 *
		 * @param {integer} number
		 * @param {Object} pluralRules
		 * @return {integer} plural form index
		 */
		getPluralForm: function ( number, pluralRules ) {
			var i,
				pluralForms = [ 'zero', 'one', 'two', 'few', 'many', 'other' ],
				pluralFormIndex = 0;

			for ( i = 0; i < pluralForms.length; i++ ) {
				if ( pluralRules[ pluralForms[ i ] ] ) {
					if ( pluralRuleParser( pluralRules[ pluralForms[ i ] ], number ) ) {
						return pluralFormIndex;
					}

					pluralFormIndex++;
				}
			}

			return pluralFormIndex;
		},

		/**
		 * Converts a number using digitTransformTable.
		 *
		 * @param {number} num Value to be converted
		 * @param {boolean} integer Convert the return value to an integer
		 * @return {string} The number converted into a String.
		 */
		convertNumber: function ( num, integer ) {
			var tmp, item, i,
				transformTable, numberString, convertedNumber;

			// Set the target Transform table:
			transformTable = this.digitTransformTable( $.i18n().locale );
			numberString = String( num );
			convertedNumber = '';

			if ( !transformTable ) {
				return num;
			}

			// Check if the restore to Latin number flag is set:
			if ( integer ) {
				if ( parseFloat( num, 10 ) === num ) {
					return num;
				}

				tmp = [];

				for ( item in transformTable ) {
					tmp[ transformTable[ item ] ] = item;
				}

				transformTable = tmp;
			}

			for ( i = 0; i < numberString.length; i++ ) {
				if ( transformTable[ numberString[ i ] ] ) {
					convertedNumber += transformTable[ numberString[ i ] ];
				} else {
					convertedNumber += numberString[ i ];
				}
			}

			return integer ? parseFloat( convertedNumber, 10 ) : convertedNumber;
		},

		/**
		 * Grammatical transformations, needed for inflected languages.
		 * Invoked by putting {{grammar:form|word}} in a message.
		 * Override this method for languages that need special grammar rules
		 * applied dynamically.
		 *
		 * @param {string} word
		 * @param {string} form
		 * @return {string}
		 */
		// eslint-disable-next-line no-unused-vars
		convertGrammar: function ( word, form ) {
			return word;
		},

		/**
		 * Provides an alternative text depending on specified gender. Usage
		 * {{gender:[gender|user object]|masculine|feminine|neutral}}. If second
		 * or third parameter are not specified, masculine is used.
		 *
		 * These details may be overriden per language.
		 *
		 * @param {string} gender
		 *      male, female, or anything else for neutral.
		 * @param {Array} forms
		 *      List of gender forms
		 *
		 * @return {string}
		 */
		gender: function ( gender, forms ) {
			if ( !forms || forms.length === 0 ) {
				return '';
			}

			while ( forms.length < 2 ) {
				forms.push( forms[ forms.length - 1 ] );
			}

			if ( gender === 'male' ) {
				return forms[ 0 ];
			}

			if ( gender === 'female' ) {
				return forms[ 1 ];
			}

			return ( forms.length === 3 ) ? forms[ 2 ] : forms[ 0 ];
		},

		/**
		 * Get the digit transform table for the given language
		 * See http://cldr.unicode.org/translation/numbering-systems
		 *
		 * @param {string} language
		 * @return {Array|boolean} List of digits in the passed language or false
		 * representation, or boolean false if there is no information.
		 */
		digitTransformTable: function ( language ) {
			var tables = {
				ar: '٠١٢٣٤٥٦٧٨٩',
				fa: '۰۱۲۳۴۵۶۷۸۹',
				ml: '൦൧൨൩൪൫൬൭൮൯',
				kn: '೦೧೨೩೪೫೬೭೮೯',
				lo: '໐໑໒໓໔໕໖໗໘໙',
				or: '୦୧୨୩୪୫୬୭୮୯',
				kh: '០១២៣៤៥៦៧៨៩',
				pa: '੦੧੨੩੪੫੬੭੮੯',
				gu: '૦૧૨૩૪૫૬૭૮૯',
				hi: '०१२३४५६७८९',
				my: '၀၁၂၃၄၅၆၇၈၉',
				ta: '௦௧௨௩௪௫௬௭௮௯',
				te: '౦౧౨౩౪౫౬౭౮౯',
				th: '๐๑๒๓๔๕๖๗๘๙', // FIXME use iso 639 codes
				bo: '༠༡༢༣༤༥༦༧༨༩' // FIXME use iso 639 codes
			};

			if ( !tables[ language ] ) {
				return false;
			}

			return tables[ language ].split( '' );
		}
	};

	$.extend( $.i18n.languages, {
		'default': language
	} );
}( jQuery ) );

},{}],10:[function(require,module,exports){
/*!
 * jQuery Internationalization library - Message Store
 *
 * Copyright (C) 2012 Santhosh Thottingal
 *
 * jquery.i18n is dual licensed GPLv2 or later and MIT. You don't have to do anything special to
 * choose one license or the other and you don't have to notify anyone which license you are using.
 * You are free to use UniversalLanguageSelector in commercial projects as long as the copyright
 * header is left intact. See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */

( function ( $ ) {
	'use strict';

	var MessageStore = function () {
		this.messages = {};
		this.sources = {};
	};

	function jsonMessageLoader( url ) {
		var deferred = $.Deferred();

		$.getJSON( url )
			.done( deferred.resolve )
			.fail( function ( jqxhr, settings, exception ) {
				$.i18n.log( 'Error in loading messages from ' + url + ' Exception: ' + exception );
				// Ignore 404 exception, because we are handling fallabacks explicitly
				deferred.resolve();
			} );

		return deferred.promise();
	}

	/**
	 * See https://github.com/wikimedia/jquery.i18n/wiki/Specification#wiki-Message_File_Loading
	 */
	MessageStore.prototype = {

		/**
		 * General message loading API This can take a URL string for
		 * the json formatted messages.
		 * <code>load('path/to/all_localizations.json');</code>
		 *
		 * This can also load a localization file for a locale <code>
		 * load( 'path/to/de-messages.json', 'de' );
		 * </code>
		 * A data object containing message key- message translation mappings
		 * can also be passed Eg:
		 * <code>
		 * load( { 'hello' : 'Hello' }, optionalLocale );
		 * </code> If the data argument is
		 * null/undefined/false,
		 * all cached messages for the i18n instance will get reset.
		 *
		 * @param {string|Object} source
		 * @param {string} locale Language tag
		 * @return {jQuery.Promise}
		 */
		load: function ( source, locale ) {
			var key = null,
				deferred = null,
				deferreds = [],
				messageStore = this;

			if ( typeof source === 'string' ) {
				// This is a URL to the messages file.
				$.i18n.log( 'Loading messages from: ' + source );
				deferred = jsonMessageLoader( source )
					.done( function ( localization ) {
						messageStore.set( locale, localization );
					} );

				return deferred.promise();
			}

			if ( locale ) {
				// source is an key-value pair of messages for given locale
				messageStore.set( locale, source );

				return $.Deferred().resolve();
			} else {
				// source is a key-value pair of locales and their source
				for ( key in source ) {
					if ( Object.prototype.hasOwnProperty.call( source, key ) ) {
						locale = key;
						// No {locale} given, assume data is a group of languages,
						// call this function again for each language.
						deferreds.push( messageStore.load( source[ key ], locale ) );
					}
				}
				return $.when.apply( $, deferreds );
			}

		},

		/**
		 * Set messages to the given locale.
		 * If locale exists, add messages to the locale.
		 *
		 * @param {string} locale
		 * @param {Object} messages
		 */
		set: function ( locale, messages ) {
			if ( !this.messages[ locale ] ) {
				this.messages[ locale ] = messages;
			} else {
				this.messages[ locale ] = $.extend( this.messages[ locale ], messages );
			}
		},

		/**
		 *
		 * @param {string} locale
		 * @param {string} messageKey
		 * @return {boolean}
		 */
		get: function ( locale, messageKey ) {
			return this.messages[ locale ] && this.messages[ locale ][ messageKey ];
		}
	};

	$.extend( $.i18n.messageStore, new MessageStore() );
}( jQuery ) );

},{}],11:[function(require,module,exports){
/*!
 * jQuery Internationalization library
 *
 * Copyright (C) 2011-2013 Santhosh Thottingal, Neil Kandalgaonkar
 *
 * jquery.i18n is dual licensed GPLv2 or later and MIT. You don't have to do
 * anything special to choose one license or the other and you don't have to
 * notify anyone which license you are using. You are free to use
 * UniversalLanguageSelector in commercial projects as long as the copyright
 * header is left intact. See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */

( function ( $ ) {
	'use strict';

	var MessageParser = function ( options ) {
		this.options = $.extend( {}, $.i18n.parser.defaults, options );
		this.language = $.i18n.languages[ String.locale ] || $.i18n.languages[ 'default' ];
		this.emitter = $.i18n.parser.emitter;
	};

	MessageParser.prototype = {

		constructor: MessageParser,

		simpleParse: function ( message, parameters ) {
			return message.replace( /\$(\d+)/g, function ( str, match ) {
				var index = parseInt( match, 10 ) - 1;

				return parameters[ index ] !== undefined ? parameters[ index ] : '$' + match;
			} );
		},

		parse: function ( message, replacements ) {
			if ( message.indexOf( '{{' ) < 0 ) {
				return this.simpleParse( message, replacements );
			}

			this.emitter.language = $.i18n.languages[ $.i18n().locale ] ||
				$.i18n.languages[ 'default' ];

			return this.emitter.emit( this.ast( message ), replacements );
		},

		ast: function ( message ) {
			var pipe, colon, backslash, anyCharacter, dollar, digits, regularLiteral,
				regularLiteralWithoutBar, regularLiteralWithoutSpace, escapedOrLiteralWithoutBar,
				escapedOrRegularLiteral, templateContents, templateName, openTemplate,
				closeTemplate, expression, paramExpression, result,
				pos = 0;

			// Try parsers until one works, if none work return null
			function choice( parserSyntax ) {
				return function () {
					var i, result;

					for ( i = 0; i < parserSyntax.length; i++ ) {
						result = parserSyntax[ i ]();

						if ( result !== null ) {
							return result;
						}
					}

					return null;
				};
			}

			// Try several parserSyntax-es in a row.
			// All must succeed; otherwise, return null.
			// This is the only eager one.
			function sequence( parserSyntax ) {
				var i, res,
					originalPos = pos,
					result = [];

				for ( i = 0; i < parserSyntax.length; i++ ) {
					res = parserSyntax[ i ]();

					if ( res === null ) {
						pos = originalPos;

						return null;
					}

					result.push( res );
				}

				return result;
			}

			// Run the same parser over and over until it fails.
			// Must succeed a minimum of n times; otherwise, return null.
			function nOrMore( n, p ) {
				return function () {
					var originalPos = pos,
						result = [],
						parsed = p();

					while ( parsed !== null ) {
						result.push( parsed );
						parsed = p();
					}

					if ( result.length < n ) {
						pos = originalPos;

						return null;
					}

					return result;
				};
			}

			// Helpers -- just make parserSyntax out of simpler JS builtin types

			function makeStringParser( s ) {
				var len = s.length;

				return function () {
					var result = null;

					if ( message.slice( pos, pos + len ) === s ) {
						result = s;
						pos += len;
					}

					return result;
				};
			}

			function makeRegexParser( regex ) {
				return function () {
					var matches = message.slice( pos ).match( regex );

					if ( matches === null ) {
						return null;
					}

					pos += matches[ 0 ].length;

					return matches[ 0 ];
				};
			}

			pipe = makeStringParser( '|' );
			colon = makeStringParser( ':' );
			backslash = makeStringParser( '\\' );
			anyCharacter = makeRegexParser( /^./ );
			dollar = makeStringParser( '$' );
			digits = makeRegexParser( /^\d+/ );
			regularLiteral = makeRegexParser( /^[^{}\[\]$\\]/ );
			regularLiteralWithoutBar = makeRegexParser( /^[^{}\[\]$\\|]/ );
			regularLiteralWithoutSpace = makeRegexParser( /^[^{}\[\]$\s]/ );

			// There is a general pattern:
			// parse a thing;
			// if it worked, apply transform,
			// otherwise return null.
			// But using this as a combinator seems to cause problems
			// when combined with nOrMore().
			// May be some scoping issue.
			function transform( p, fn ) {
				return function () {
					var result = p();

					return result === null ? null : fn( result );
				};
			}

			// Used to define "literals" within template parameters. The pipe
			// character is the parameter delimeter, so by default
			// it is not a literal in the parameter
			function literalWithoutBar() {
				var result = nOrMore( 1, escapedOrLiteralWithoutBar )();

				return result === null ? null : result.join( '' );
			}

			function literal() {
				var result = nOrMore( 1, escapedOrRegularLiteral )();

				return result === null ? null : result.join( '' );
			}

			function escapedLiteral() {
				var result = sequence( [ backslash, anyCharacter ] );

				return result === null ? null : result[ 1 ];
			}

			choice( [ escapedLiteral, regularLiteralWithoutSpace ] );
			escapedOrLiteralWithoutBar = choice( [ escapedLiteral, regularLiteralWithoutBar ] );
			escapedOrRegularLiteral = choice( [ escapedLiteral, regularLiteral ] );

			function replacement() {
				var result = sequence( [ dollar, digits ] );

				if ( result === null ) {
					return null;
				}

				return [ 'REPLACE', parseInt( result[ 1 ], 10 ) - 1 ];
			}

			templateName = transform(
				// see $wgLegalTitleChars
				// not allowing : due to the need to catch "PLURAL:$1"
				makeRegexParser( /^[ !"$&'()*,.\/0-9;=?@A-Z\^_`a-z~\x80-\xFF+\-]+/ ),

				function ( result ) {
					return result.toString();
				}
			);

			function templateParam() {
				var expr,
					result = sequence( [ pipe, nOrMore( 0, paramExpression ) ] );

				if ( result === null ) {
					return null;
				}

				expr = result[ 1 ];

				// use a "CONCAT" operator if there are multiple nodes,
				// otherwise return the first node, raw.
				return expr.length > 1 ? [ 'CONCAT' ].concat( expr ) : expr[ 0 ];
			}

			function templateWithReplacement() {
				var result = sequence( [ templateName, colon, replacement ] );

				return result === null ? null : [ result[ 0 ], result[ 2 ] ];
			}

			function templateWithOutReplacement() {
				var result = sequence( [ templateName, colon, paramExpression ] );

				return result === null ? null : [ result[ 0 ], result[ 2 ] ];
			}

			templateContents = choice( [
				function () {
					var res = sequence( [
						// templates can have placeholders for dynamic
						// replacement eg: {{PLURAL:$1|one car|$1 cars}}
						// or no placeholders eg:
						// {{GRAMMAR:genitive|{{SITENAME}}}
						choice( [ templateWithReplacement, templateWithOutReplacement ] ),
						nOrMore( 0, templateParam )
					] );

					return res === null ? null : res[ 0 ].concat( res[ 1 ] );
				},
				function () {
					var res = sequence( [ templateName, nOrMore( 0, templateParam ) ] );

					if ( res === null ) {
						return null;
					}

					return [ res[ 0 ] ].concat( res[ 1 ] );
				}
			] );

			openTemplate = makeStringParser( '{{' );
			closeTemplate = makeStringParser( '}}' );

			function template() {
				var result = sequence( [ openTemplate, templateContents, closeTemplate ] );

				return result === null ? null : result[ 1 ];
			}

			expression = choice( [ template, replacement, literal ] );
			paramExpression = choice( [ template, replacement, literalWithoutBar ] );

			function start() {
				var result = nOrMore( 0, expression )();

				if ( result === null ) {
					return null;
				}

				return [ 'CONCAT' ].concat( result );
			}

			result = start();

			/*
			 * For success, the pos must have gotten to the end of the input
			 * and returned a non-null.
			 * n.b. This is part of language infrastructure, so we do not throw an internationalizable message.
			 */
			if ( result === null || pos !== message.length ) {
				throw new Error( 'Parse error at position ' + pos.toString() + ' in input: ' + message );
			}

			return result;
		}

	};

	$.extend( $.i18n.parser, new MessageParser() );
}( jQuery ) );

},{}],12:[function(require,module,exports){
/*! js-url - v2.5.2 - 2017-08-30 */!function(){var a=function(){function a(){}function b(a){return decodeURIComponent(a.replace(/\+/g," "))}function c(a,b){var c=a.charAt(0),d=b.split(c);return c===a?d:(a=parseInt(a.substring(1),10),d[a<0?d.length+a:a-1])}function d(a,c){for(var d=a.charAt(0),e=c.split("&"),f=[],g={},h=[],i=a.substring(1),j=0,k=e.length;j<k;j++)if(f=e[j].match(/(.*?)=(.*)/),f||(f=[e[j],e[j],""]),""!==f[1].replace(/\s/g,"")){if(f[2]=b(f[2]||""),i===f[1])return f[2];h=f[1].match(/(.*)\[([0-9]+)\]/),h?(g[h[1]]=g[h[1]]||[],g[h[1]][h[2]]=f[2]):g[f[1]]=f[2]}return d===a?g:g[i]}return function(b,e){var f,g={};if("tld?"===b)return a();if(e=e||window.location.toString(),!b)return e;if(b=b.toString(),f=e.match(/^mailto:([^\/].+)/))g.protocol="mailto",g.email=f[1];else{if((f=e.match(/(.*?)\/#\!(.*)/))&&(e=f[1]+f[2]),(f=e.match(/(.*?)#(.*)/))&&(g.hash=f[2],e=f[1]),g.hash&&b.match(/^#/))return d(b,g.hash);if((f=e.match(/(.*?)\?(.*)/))&&(g.query=f[2],e=f[1]),g.query&&b.match(/^\?/))return d(b,g.query);if((f=e.match(/(.*?)\:?\/\/(.*)/))&&(g.protocol=f[1].toLowerCase(),e=f[2]),(f=e.match(/(.*?)(\/.*)/))&&(g.path=f[2],e=f[1]),g.path=(g.path||"").replace(/^([^\/])/,"/$1"),b.match(/^[\-0-9]+$/)&&(b=b.replace(/^([^\/])/,"/$1")),b.match(/^\//))return c(b,g.path.substring(1));if(f=c("/-1",g.path.substring(1)),f&&(f=f.match(/(.*?)\.(.*)/))&&(g.file=f[0],g.filename=f[1],g.fileext=f[2]),(f=e.match(/(.*)\:([0-9]+)$/))&&(g.port=f[2],e=f[1]),(f=e.match(/(.*?)@(.*)/))&&(g.auth=f[1],e=f[2]),g.auth&&(f=g.auth.match(/(.*)\:(.*)/),g.user=f?f[1]:g.auth,g.pass=f?f[2]:void 0),g.hostname=e.toLowerCase(),"."===b.charAt(0))return c(b,g.hostname);a()&&(f=g.hostname.match(a()),f&&(g.tld=f[3],g.domain=f[2]?f[2]+"."+f[3]:void 0,g.sub=f[1]||void 0)),g.port=g.port||("https"===g.protocol?"443":"80"),g.protocol=g.protocol||("443"===g.port?"https":"http")}return b in g?g[b]:"{}"===b?g:void 0}}();"function"==typeof window.define&&window.define.amd?window.define("js-url",[],function(){return a}):("undefined"!=typeof window.jQuery&&window.jQuery.extend({url:function(a,b){return window.url(a,b)}}),window.url=a)}();

},{}],13:[function(require,module,exports){
var mapController = require("./mapPresenter.js");

var langImages = {
    gl: "assets/images/gl.png",
    es: "assets/images/es.png"
};

var langTexts = {
    gl: "Galego",
    es: "Castellano"
};

// Enable debug
$.i18n.debug = true;

var currentLocale = sessionStorage.getItem("currentLocale");

function set_locale_to(locale) {
    if (locale) $.i18n().locale = locale;
}

jQuery(function updateLocale($) {
    "use strict";
    var i18n = $.i18n();

    i18n.locale = currentLocale;

    i18n
        .load({
            es: "./src/i18n/map_es.json",
            gl: "./src/i18n/map_gl.json"
        })
        .done(function() {
            console.log("Internationalization files loaded correctly");

            $("#titleimage").attr(
                "src", langImages[i18n.locale]);

            $("#titletext").text(langTexts[i18n.locale]);

            History.Adapter.bind(window, "statechange", function() {
                set_locale_to(url("?locale"));
            });

            History.pushState(null, null, "?locale=" + i18n.locale);
            mapController.updatePageElements();
            mapController.showLevelUnlocked();

            $(".switch-locale").on("click", "a", function(e) {
                console.log("locale changed");
                e.preventDefault();
                History.pushState(null, null, "?locale=" + $(this).data("locale"));
                $.i18n().locale = $(this).data("locale");
                mapController.updatePageElements();
            });

            $(".languages > li").click(function() {
                $("#titleimage").attr(
                    "src",
                    $(this)
                    .children()
                    .children("img")
                    .attr("src")
                );
                $("#titletext").text($(this).text());
            });
        });
});
},{"./mapPresenter.js":14}],14:[function(require,module,exports){
var jsonLoader = require("./jsonLoader.js");

var globalConfig = jsonLoader.loadJsonFile("conf/global_config.json");

var levelsUnlocked = [globalConfig.first_level_unlocked];

var colors = {};

var globalLevels = globalConfig.levels;

var levelUnlocked;

var firstLevelCode = globalConfig.first_level_unlocked.code.replace(/["]+/g, '');

var levelFiles = {};

var _init = function () {
    if (sessionStorage["levelsUnlocked"] == null) {
        sessionStorage.setItem("levelsUnlocked", JSON.stringify(levelsUnlocked));
    } else {
        levelsUnlocked = JSON.parse(sessionStorage.getItem("levelsUnlocked"));
    }

    levelFiles[firstLevelCode] = globalConfig.first_level_file;

    if (sessionStorage["levelFiles"] == null) {
        sessionStorage.setItem("levelFiles", JSON.stringify(levelFiles));
    } else {
        levelFiles = JSON.parse(sessionStorage.getItem("levelFiles"));
    }

    if (sessionStorage["currentLocale"] == null) {
        sessionStorage.setItem("currentLocale", "gl");
    }

    for (var level in globalLevels) {
        levelUnlocked = false;
        for (var unlocked in levelsUnlocked) {
            if (levelsUnlocked[unlocked].code == globalLevels[level]) {
                colors[globalLevels[level]] = globalConfig.unlocked_level_color;
                levelUnlocked = true;
                break;
            }
        }
        if (!levelUnlocked) {
            colors[globalLevels[level]] = globalConfig.locked_level_color;
        }
    }

    if (sessionStorage["colors"] == null) {
        sessionStorage.setItem("colors", JSON.stringify(colors));
    } else {
        colors = JSON.parse(sessionStorage.getItem("colors"));
    }

}

function getLevelText(level) {
    var levelText = $.i18n("description_level_".concat(level));
    var toret = "";
    var arr = levelText.split("~");
    for (index = 0; index < arr.length; ++index) {
        var str = arr[index].replace(/^/, "<li>").concat("</li>");
        toret += str;
    }
    toret.replace(/^/, "<ul>").concat("</ul>");
    return toret;
}

function showLevelDescription(currentLevel, levelText) {
    swal({
        title: "Nivel " + currentLevel + ": " + $.i18n("level_".concat(currentLevel)),
        html: levelText,
        allowOutsideClick: false,
        allowEscapeKey: false,
        timer: 6000,

        onOpen: function () {
            var b = swal.getConfirmButton();
            b.hidden = true;
            b.disabled = true;
        }
    }).then(function () {
        swal({
            title: "Nivel " + currentLevel + ": " + $.i18n("level_".concat(currentLevel)),
            html: levelText,
            allowOutsideClick: true,
            allowEscapeKey: true,
            showCloseButton: true,
            showCancelButton: true,
            animation: false
        }).then((result) => {
            if (result.value) {
                if (sessionStorage) {
                    sessionStorage.setItem("currentLocale", $.i18n().locale);
                }
                location.replace("game.html");
            } else {
                result.dismiss === swal.DismissReason.cancel;
            }
        });
    });
}

_init();

$(document).ready(function () {
    var selectedRegions = [];

    for (var level in levelsUnlocked) {
        selectedRegions.push(levelsUnlocked[level].code);
    }

    jQuery("#vmap").vectorMap({
        map: "world_en",
        backgroundColor: "#a5bfdd",
        borderColor: "#818181",
        borderOpacity: 0.5,
        borderWidth: 1,
        colors: colors,
        enableZoom: true,
        hoverColor: "#33ff33",
        hoverOpacity: null,
        normalizeFunction: "linear",
        scaleColors: ["#b6d6ff", "#005ace"],
        selectedColor: "#00cc00",
        selectedRegions: selectedRegions,
        showTooltip: true,
        onRegionClick: function (element, code, region) {
            if (!selectedRegions.includes(code)) {
                event.preventDefault();
            } else {
                // if (supports_html5_storage()) {
                //     if (code == 'es') {
                //         localStorage.setItem("levelFile", "1.json");
                //     } else if (code == 'hn') {
                //         localStorage.setItem("levelFile", "2.json");
                //     }
                // }
                if (sessionStorage) {
                    sessionStorage.setItem("levelFile", levelFiles[code]);
                    var currentLevel = levelFiles[code].split(".")[0];
                    var levelText = getLevelText(currentLevel);
                    showLevelDescription(currentLevel, levelText);
                } else {
                    alert("Sorry, your browser do not support session storage.");
                }

            }
        },

        onLoad: function (event, map) { },
        onLabelShow: function (event, label, code) { },
        onRegionOver: function (event, code, region) {
            if (!selectedRegions.includes(code)) {
                event.preventDefault();
            }
        },
        onRegionOut: function (event, code, region) { },

        onResize: function (event, width, height) { }
    });
    module.exports.updatePageElements = function updatePageElements() {
        $("#tetris-header").text($.i18n("tetris_header"));
    };

    module.exports.showLevelUnlocked = function showLevelUnlocked() {
        for (var level in levelsUnlocked) {
            if (!levelsUnlocked[level].shown) {
                levelsUnlocked[level].shown = true;
                sessionStorage.setItem("levelsUnlocked", JSON.stringify(levelsUnlocked));
                swal({
                    title: $.i18n("level_unlocked_title"),
                    html: $.i18n("level_unlocked_text").concat($.i18n(levelsUnlocked[level].code)).concat("."),
                    allowOutsideClick: true,
                    allowEscapeKey: true,
                    showCloseButton: true,
                    animation: true
                }).then((result) => { });
            }
        }
    }
});

function supports_html5_storage() {
    try {
        return "localStorage" in window && window["localStorage"] !== null;
    } catch (e) {
        return false;
    }
}
},{"./jsonLoader.js":2}],15:[function(require,module,exports){
/*!
 * JQVMap: jQuery Vector Map Library
 * @author JQVMap <me@peterschmalfeldt.com>
 * @version 1.5.1
 * @link http://jqvmap.com
 * @license https://github.com/manifestinteractive/jqvmap/blob/master/LICENSE
 * @builddate 2016/06/02
 */

var VectorCanvas = function(width, height, params) {
    this.mode = window.SVGAngle ? 'svg' : 'vml';
    this.params = params;

    if (this.mode === 'svg') {
        this.createSvgNode = function(nodeName) {
            return document.createElementNS(this.svgns, nodeName);
        };
    } else {
        try {
            if (!document.namespaces.rvml) {
                document.namespaces.add('rvml', 'urn:schemas-microsoft-com:vml');
            }
            this.createVmlNode = function(tagName) {
                return document.createElement('<rvml:' + tagName + ' class="rvml">');
            };
        } catch (e) {
            this.createVmlNode = function(tagName) {
                return document.createElement('<' + tagName + ' xmlns="urn:schemas-microsoft.com:vml" class="rvml">');
            };
        }

        document.createStyleSheet().addRule('.rvml', 'behavior:url(#default#VML)');
    }

    if (this.mode === 'svg') {
        this.canvas = this.createSvgNode('svg');
    } else {
        this.canvas = this.createVmlNode('group');
        this.canvas.style.position = 'absolute';
    }

    this.setSize(width, height);
};

VectorCanvas.prototype = {
    svgns: 'http://www.w3.org/2000/svg',
    mode: 'svg',
    width: 0,
    height: 0,
    canvas: null
};

var ColorScale = function(colors, normalizeFunction, minValue, maxValue) {
    if (colors) {
        this.setColors(colors);
    }
    if (normalizeFunction) {
        this.setNormalizeFunction(normalizeFunction);
    }
    if (minValue) {
        this.setMin(minValue);
    }
    if (minValue) {
        this.setMax(maxValue);
    }
};

ColorScale.prototype = {
    colors: []
};

var JQVMap = function(params) {
    params = params || {};
    var map = this;
    var mapData = JQVMap.maps[params.map];
    var mapPins;

    if (!mapData) {
        throw new Error('Invalid "' + params.map + '" map parameter. Please make sure you have loaded this map file in your HTML.');
    }

    this.selectedRegions = [];
    this.multiSelectRegion = params.multiSelectRegion;

    this.container = params.container;

    this.defaultWidth = mapData.width;
    this.defaultHeight = mapData.height;

    this.color = params.color;
    this.selectedColor = params.selectedColor;
    this.hoverColor = params.hoverColor;
    this.hoverColors = params.hoverColors;
    this.hoverOpacity = params.hoverOpacity;
    this.setBackgroundColor(params.backgroundColor);

    this.width = params.container.width();
    this.height = params.container.height();

    this.resize();

    jQuery(window).resize(function() {
        var newWidth = params.container.width();
        var newHeight = params.container.height();

        if (newWidth && newHeight) {
            map.width = newWidth;
            map.height = newHeight;
            map.resize();
            map.canvas.setSize(map.width, map.height);
            map.applyTransform();

            var resizeEvent = jQuery.Event('resize.jqvmap');
            jQuery(params.container).trigger(resizeEvent, [newWidth, newHeight]);

            if (mapPins) {
                jQuery('.jqvmap-pin').remove();
                map.pinHandlers = false;
                map.placePins(mapPins.pins, mapPins.mode);
            }
        }
    });

    this.canvas = new VectorCanvas(this.width, this.height, params);
    params.container.append(this.canvas.canvas);

    this.makeDraggable();

    this.rootGroup = this.canvas.createGroup(true);

    this.index = JQVMap.mapIndex;
    this.label = jQuery('<div/>').addClass('jqvmap-label').appendTo(jQuery('body')).hide();

    if (params.enableZoom) {
        jQuery('<div/>').addClass('jqvmap-zoomin').text('+').appendTo(params.container);
        jQuery('<div/>').addClass('jqvmap-zoomout').html('&#x2212;').appendTo(params.container);
    }

    map.countries = [];

    for (var key in mapData.paths) {
        var path = this.canvas.createPath({
            path: mapData.paths[key].path
        });

        path.setFill(this.color);
        path.id = map.getCountryId(key);
        map.countries[key] = path;

        if (this.canvas.mode === 'svg') {
            path.setAttribute('class', 'jqvmap-region');
        } else {
            jQuery(path).addClass('jqvmap-region');
        }

        jQuery(this.rootGroup).append(path);
    }

    jQuery(params.container).delegate(this.canvas.mode === 'svg' ? 'path' : 'shape', 'mouseover mouseout', function(e) {
        var containerPath = e.target,
            code = e.target.id.split('_').pop(),
            labelShowEvent = jQuery.Event('labelShow.jqvmap'),
            regionMouseOverEvent = jQuery.Event('regionMouseOver.jqvmap');

        code = code.toLowerCase();

        if (e.type === 'mouseover') {
            jQuery(params.container).trigger(regionMouseOverEvent, [code, mapData.paths[code].name]);
            if (!regionMouseOverEvent.isDefaultPrevented()) {
                map.highlight(code, containerPath);
            }
            if (params.showTooltip) {
                map.label.text(mapData.paths[code].name);
                jQuery(params.container).trigger(labelShowEvent, [map.label, code]);

                if (!labelShowEvent.isDefaultPrevented()) {
                    map.label.show();
                    map.labelWidth = map.label.width();
                    map.labelHeight = map.label.height();
                }
            }
        } else {
            map.unhighlight(code, containerPath);

            map.label.hide();
            jQuery(params.container).trigger('regionMouseOut.jqvmap', [code, mapData.paths[code].name]);
        }
    });

    jQuery(params.container).delegate(this.canvas.mode === 'svg' ? 'path' : 'shape', 'click', function(regionClickEvent) {

        var targetPath = regionClickEvent.target;
        var code = regionClickEvent.target.id.split('_').pop();
        var mapClickEvent = jQuery.Event('regionClick.jqvmap');

        code = code.toLowerCase();

        jQuery(params.container).trigger(mapClickEvent, [code, mapData.paths[code].name]);

        if (!params.multiSelectRegion && !mapClickEvent.isDefaultPrevented()) {
            for (var keyPath in mapData.paths) {
                map.countries[keyPath].currentFillColor = map.countries[keyPath].getOriginalFill();
                map.countries[keyPath].setFill(map.countries[keyPath].getOriginalFill());
            }
        }

        if (!mapClickEvent.isDefaultPrevented()) {
            if (map.isSelected(code)) {
                map.deselect(code, targetPath);
            } else {
                map.select(code, targetPath);
            }
        }
    });

    if (params.showTooltip) {
        params.container.mousemove(function(e) {
            if (map.label.is(':visible')) {
                var left = e.pageX - 15 - map.labelWidth;
                var top = e.pageY - 15 - map.labelHeight;

                if (left < 0) {
                    left = e.pageX + 15;
                }
                if (top < 0) {
                    top = e.pageY + 15;
                }

                map.label.css({
                    left: left,
                    top: top
                });
            }
        });
    }

    this.setColors(params.colors);

    this.canvas.canvas.appendChild(this.rootGroup);

    this.applyTransform();

    this.colorScale = new ColorScale(params.scaleColors, params.normalizeFunction, params.valueMin, params.valueMax);

    if (params.values) {
        this.values = params.values;
        this.setValues(params.values);
    }

    if (params.selectedRegions) {
        if (params.selectedRegions instanceof Array) {
            for (var k in params.selectedRegions) {
                this.select(params.selectedRegions[k].toLowerCase());
            }
        } else {
            this.select(params.selectedRegions.toLowerCase());
        }
    }

    this.bindZoomButtons();

    if (params.pins) {
        mapPins = {
            pins: params.pins,
            mode: params.pinMode
        };

        this.pinHandlers = false;
        this.placePins(params.pins, params.pinMode);
    }

    if (params.showLabels) {
        this.pinHandlers = false;

        var pins = {};
        for (key in map.countries) {
            if (typeof map.countries[key] !== 'function') {
                if (!params.pins || !params.pins[key]) {
                    pins[key] = key.toUpperCase();
                }
            }
        }

        mapPins = {
            pins: pins,
            mode: 'content'
        };

        this.placePins(pins, 'content');
    }

    JQVMap.mapIndex++;
};

JQVMap.prototype = {
    transX: 0,
    transY: 0,
    scale: 1,
    baseTransX: 0,
    baseTransY: 0,
    baseScale: 1,
    width: 0,
    height: 0,
    countries: {},
    countriesColors: {},
    countriesData: {},
    zoomStep: 1.4,
    zoomMaxStep: 4,
    zoomCurStep: 1
};

JQVMap.xlink = 'http://www.w3.org/1999/xlink';
JQVMap.mapIndex = 1;
JQVMap.maps = {};

(function() {

    var apiParams = {
        colors: 1,
        values: 1,
        backgroundColor: 1,
        scaleColors: 1,
        normalizeFunction: 1,
        enableZoom: 1,
        showTooltip: 1,
        borderColor: 1,
        borderWidth: 1,
        borderOpacity: 1,
        selectedRegions: 1,
        multiSelectRegion: 1
    };

    var apiEvents = {
        onLabelShow: 'labelShow',
        onLoad: 'load',
        onRegionOver: 'regionMouseOver',
        onRegionOut: 'regionMouseOut',
        onRegionClick: 'regionClick',
        onRegionSelect: 'regionSelect',
        onRegionDeselect: 'regionDeselect',
        onResize: 'resize'
    };

    jQuery.fn.vectorMap = function(options) {

        var defaultParams = {
                map: 'world_en',
                backgroundColor: '#a5bfdd',
                color: '#f4f3f0',
                hoverColor: '#c9dfaf',
                hoverColors: {},
                selectedColor: '#c9dfaf',
                scaleColors: ['#b6d6ff', '#005ace'],
                normalizeFunction: 'linear',
                enableZoom: true,
                showTooltip: true,
                borderColor: '#818181',
                borderWidth: 1,
                borderOpacity: 0.25,
                selectedRegions: null,
                multiSelectRegion: false
            },
            map = this.data('mapObject');

        if (options === 'addMap') {
            JQVMap.maps[arguments[1]] = arguments[2];
        } else if (options === 'set' && apiParams[arguments[1]]) {
            map['set' + arguments[1].charAt(0).toUpperCase() + arguments[1].substr(1)].apply(map, Array.prototype.slice.call(arguments, 2));
        } else if (typeof options === 'string' &&
            typeof map[options] === 'function') {
            return map[options].apply(map, Array.prototype.slice.call(arguments, 1));
        } else {
            jQuery.extend(defaultParams, options);
            defaultParams.container = this;
            this.css({ position: 'relative', overflow: 'hidden' });

            map = new JQVMap(defaultParams);

            this.data('mapObject', map);

            this.unbind('.jqvmap');

            for (var e in apiEvents) {
                if (defaultParams[e]) {
                    this.bind(apiEvents[e] + '.jqvmap', defaultParams[e]);
                }
            }

            var loadEvent = jQuery.Event('load.jqvmap');
            jQuery(defaultParams.container).trigger(loadEvent, map);

            return map;
        }
    };

})(jQuery);

ColorScale.arrayToRgb = function(ar) {
    var rgb = '#';
    var d;
    for (var i = 0; i < ar.length; i++) {
        d = ar[i].toString(16);
        rgb += d.length === 1 ? '0' + d : d;
    }
    return rgb;
};

ColorScale.prototype.getColor = function(value) {
    if (typeof this.normalize === 'function') {
        value = this.normalize(value);
    }

    var lengthes = [];
    var fullLength = 0;
    var l;

    for (var i = 0; i < this.colors.length - 1; i++) {
        l = this.vectorLength(this.vectorSubtract(this.colors[i + 1], this.colors[i]));
        lengthes.push(l);
        fullLength += l;
    }

    var c = (this.maxValue - this.minValue) / fullLength;

    for (i = 0; i < lengthes.length; i++) {
        lengthes[i] *= c;
    }

    i = 0;
    value -= this.minValue;

    while (value - lengthes[i] >= 0) {
        value -= lengthes[i];
        i++;
    }

    var color;
    if (i === this.colors.length - 1) {
        color = this.vectorToNum(this.colors[i]).toString(16);
    } else {
        color = (this.vectorToNum(this.vectorAdd(this.colors[i], this.vectorMult(this.vectorSubtract(this.colors[i + 1], this.colors[i]), (value) / (lengthes[i]))))).toString(16);
    }

    while (color.length < 6) {
        color = '0' + color;
    }
    return '#' + color;
};

ColorScale.rgbToArray = function(rgb) {
    rgb = rgb.substr(1);
    return [parseInt(rgb.substr(0, 2), 16), parseInt(rgb.substr(2, 2), 16), parseInt(rgb.substr(4, 2), 16)];
};

ColorScale.prototype.setColors = function(colors) {
    for (var i = 0; i < colors.length; i++) {
        colors[i] = ColorScale.rgbToArray(colors[i]);
    }
    this.colors = colors;
};

ColorScale.prototype.setMax = function(max) {
    this.clearMaxValue = max;
    if (typeof this.normalize === 'function') {
        this.maxValue = this.normalize(max);
    } else {
        this.maxValue = max;
    }
};

ColorScale.prototype.setMin = function(min) {
    this.clearMinValue = min;

    if (typeof this.normalize === 'function') {
        this.minValue = this.normalize(min);
    } else {
        this.minValue = min;
    }
};

ColorScale.prototype.setNormalizeFunction = function(f) {
    if (f === 'polynomial') {
        this.normalize = function(value) {
            return Math.pow(value, 0.2);
        };
    } else if (f === 'linear') {
        delete this.normalize;
    } else {
        this.normalize = f;
    }
    this.setMin(this.clearMinValue);
    this.setMax(this.clearMaxValue);
};

ColorScale.prototype.vectorAdd = function(vector1, vector2) {
    var vector = [];
    for (var i = 0; i < vector1.length; i++) {
        vector[i] = vector1[i] + vector2[i];
    }
    return vector;
};

ColorScale.prototype.vectorLength = function(vector) {
    var result = 0;
    for (var i = 0; i < vector.length; i++) {
        result += vector[i] * vector[i];
    }
    return Math.sqrt(result);
};

ColorScale.prototype.vectorMult = function(vector, num) {
    var result = [];
    for (var i = 0; i < vector.length; i++) {
        result[i] = vector[i] * num;
    }
    return result;
};

ColorScale.prototype.vectorSubtract = function(vector1, vector2) {
    var vector = [];
    for (var i = 0; i < vector1.length; i++) {
        vector[i] = vector1[i] - vector2[i];
    }
    return vector;
};

ColorScale.prototype.vectorToNum = function(vector) {
    var num = 0;
    for (var i = 0; i < vector.length; i++) {
        num += Math.round(vector[i]) * Math.pow(256, vector.length - i - 1);
    }
    return num;
};

JQVMap.prototype.applyTransform = function() {
    var maxTransX, maxTransY, minTransX, minTransY;
    if (this.defaultWidth * this.scale <= this.width) {
        maxTransX = (this.width - this.defaultWidth * this.scale) / (2 * this.scale);
        minTransX = (this.width - this.defaultWidth * this.scale) / (2 * this.scale);
    } else {
        maxTransX = 0;
        minTransX = (this.width - this.defaultWidth * this.scale) / this.scale;
    }

    if (this.defaultHeight * this.scale <= this.height) {
        maxTransY = (this.height - this.defaultHeight * this.scale) / (2 * this.scale);
        minTransY = (this.height - this.defaultHeight * this.scale) / (2 * this.scale);
    } else {
        maxTransY = 0;
        minTransY = (this.height - this.defaultHeight * this.scale) / this.scale;
    }

    if (this.transY > maxTransY) {
        this.transY = maxTransY;
    } else if (this.transY < minTransY) {
        this.transY = minTransY;
    }
    if (this.transX > maxTransX) {
        this.transX = maxTransX;
    } else if (this.transX < minTransX) {
        this.transX = minTransX;
    }

    this.canvas.applyTransformParams(this.scale, this.transX, this.transY);
};

JQVMap.prototype.bindZoomButtons = function() {
    var map = this;
    this.container.find('.jqvmap-zoomin').click(function() {
        map.zoomIn();
    });
    this.container.find('.jqvmap-zoomout').click(function() {
        map.zoomOut();
    });
};

JQVMap.prototype.deselect = function(cc, path) {
    cc = cc.toLowerCase();
    path = path || jQuery('#' + this.getCountryId(cc))[0];

    if (this.isSelected(cc)) {
        this.selectedRegions.splice(this.selectIndex(cc), 1);

        jQuery(this.container).trigger('regionDeselect.jqvmap', [cc]);
        path.currentFillColor = path.getOriginalFill();
        path.setFill(path.getOriginalFill());
    } else {
        for (var key in this.countries) {
            this.selectedRegions.splice(this.selectedRegions.indexOf(key), 1);
            this.countries[key].currentFillColor = this.color;
            this.countries[key].setFill(this.color);
        }
    }
};

JQVMap.prototype.getCountryId = function(cc) {
    return 'jqvmap' + this.index + '_' + cc;
};

JQVMap.prototype.getPin = function(cc) {
    var pinObj = jQuery('#' + this.getPinId(cc));
    return pinObj.html();
};

JQVMap.prototype.getPinId = function(cc) {
    return this.getCountryId(cc) + '_pin';
};

JQVMap.prototype.getPins = function() {
    var pins = this.container.find('.jqvmap-pin');
    var ret = {};
    jQuery.each(pins, function(index, pinObj) {
        pinObj = jQuery(pinObj);
        var cc = pinObj.attr('for').toLowerCase();
        var pinContent = pinObj.html();
        ret[cc] = pinContent;
    });
    return JSON.stringify(ret);
};

JQVMap.prototype.highlight = function(cc, path) {
    path = path || jQuery('#' + this.getCountryId(cc))[0];
    if (this.hoverOpacity) {
        path.setOpacity(this.hoverOpacity);
    } else if (this.hoverColors && (cc in this.hoverColors)) {
        path.currentFillColor = path.getFill() + '';
        path.setFill(this.hoverColors[cc]);
    } else if (this.hoverColor) {
        path.currentFillColor = path.getFill() + '';
        path.setFill(this.hoverColor);
    }
};

JQVMap.prototype.isSelected = function(cc) {
    return this.selectIndex(cc) >= 0;
};

JQVMap.prototype.makeDraggable = function() {
    var mouseDown = false;
    var oldPageX, oldPageY;
    var self = this;

    self.isMoving = false;
    self.isMovingTimeout = false;

    var lastTouchCount;
    var touchCenterX;
    var touchCenterY;
    var touchStartDistance;
    var touchStartScale;
    var touchX;
    var touchY;

    this.container.mousemove(function(e) {

        if (mouseDown) {
            self.transX -= (oldPageX - e.pageX) / self.scale;
            self.transY -= (oldPageY - e.pageY) / self.scale;

            self.applyTransform();

            oldPageX = e.pageX;
            oldPageY = e.pageY;

            self.isMoving = true;
            if (self.isMovingTimeout) {
                clearTimeout(self.isMovingTimeout);
            }

            self.container.trigger('drag');
        }

        return false;

    }).mousedown(function(e) {

        mouseDown = true;
        oldPageX = e.pageX;
        oldPageY = e.pageY;

        return false;

    }).mouseup(function() {

        mouseDown = false;

        clearTimeout(self.isMovingTimeout);
        self.isMovingTimeout = setTimeout(function() {
            self.isMoving = false;
        }, 100);

        return false;

    }).mouseout(function() {

        if (mouseDown && self.isMoving) {

            clearTimeout(self.isMovingTimeout);
            self.isMovingTimeout = setTimeout(function() {
                mouseDown = false;
                self.isMoving = false;
            }, 100);

            return false;
        }
    });

    jQuery(this.container).bind('touchmove', function(e) {

        var offset;
        var scale;
        var touches = e.originalEvent.touches;
        var transformXOld;
        var transformYOld;

        if (touches.length === 1) {
            if (lastTouchCount === 1) {

                if (touchX === touches[0].pageX && touchY === touches[0].pageY) {
                    return;
                }

                transformXOld = self.transX;
                transformYOld = self.transY;

                self.transX -= (touchX - touches[0].pageX) / self.scale;
                self.transY -= (touchY - touches[0].pageY) / self.scale;

                self.applyTransform();

                if (transformXOld !== self.transX || transformYOld !== self.transY) {
                    e.preventDefault();
                }

                self.isMoving = true;
                if (self.isMovingTimeout) {
                    clearTimeout(self.isMovingTimeout);
                }
            }

            touchX = touches[0].pageX;
            touchY = touches[0].pageY;

        } else if (touches.length === 2) {

            if (lastTouchCount === 2) {
                scale = Math.sqrt(
                    Math.pow(touches[0].pageX - touches[1].pageX, 2) +
                    Math.pow(touches[0].pageY - touches[1].pageY, 2)
                ) / touchStartDistance;

                self.setScale(
                    touchStartScale * scale,
                    touchCenterX,
                    touchCenterY
                );

                e.preventDefault();

            } else {

                offset = jQuery(self.container).offset();
                if (touches[0].pageX > touches[1].pageX) {
                    touchCenterX = touches[1].pageX + (touches[0].pageX - touches[1].pageX) / 2;
                } else {
                    touchCenterX = touches[0].pageX + (touches[1].pageX - touches[0].pageX) / 2;
                }

                if (touches[0].pageY > touches[1].pageY) {
                    touchCenterY = touches[1].pageY + (touches[0].pageY - touches[1].pageY) / 2;
                } else {
                    touchCenterY = touches[0].pageY + (touches[1].pageY - touches[0].pageY) / 2;
                }

                touchCenterX -= offset.left;
                touchCenterY -= offset.top;
                touchStartScale = self.scale;

                touchStartDistance = Math.sqrt(
                    Math.pow(touches[0].pageX - touches[1].pageX, 2) +
                    Math.pow(touches[0].pageY - touches[1].pageY, 2)
                );
            }
        }

        lastTouchCount = touches.length;
    });

    jQuery(this.container).bind('touchstart', function() {
        lastTouchCount = 0;
    });

    jQuery(this.container).bind('touchend', function() {
        lastTouchCount = 0;
    });
};

JQVMap.prototype.placePins = function(pins, pinMode) {
    var map = this;

    if (!pinMode || (pinMode !== 'content' && pinMode !== 'id')) {
        pinMode = 'content';
    }

    if (pinMode === 'content') { //treat pin as content
        jQuery.each(pins, function(index, pin) {
            if (jQuery('#' + map.getCountryId(index)).length === 0) {
                return;
            }

            var pinIndex = map.getPinId(index);
            var $pin = jQuery('#' + pinIndex);
            if ($pin.length > 0) {
                $pin.remove();
            }
            map.container.append('<div id="' + pinIndex + '" for="' + index + '" class="jqvmap-pin" style="position:absolute">' + pin + '</div>');
        });
    } else { //treat pin as id of an html content
        jQuery.each(pins, function(index, pin) {
            if (jQuery('#' + map.getCountryId(index)).length === 0) {
                return;
            }
            var pinIndex = map.getPinId(index);
            var $pin = jQuery('#' + pinIndex);
            if ($pin.length > 0) {
                $pin.remove();
            }
            map.container.append('<div id="' + pinIndex + '" for="' + index + '" class="jqvmap-pin" style="position:absolute"></div>');
            $pin.append(jQuery('#' + pin));
        });
    }

    this.positionPins();
    if (!this.pinHandlers) {
        this.pinHandlers = true;
        var positionFix = function() {
            map.positionPins();
        };
        this.container.bind('zoomIn', positionFix)
            .bind('zoomOut', positionFix)
            .bind('drag', positionFix);
    }
};

JQVMap.prototype.positionPins = function() {
    var map = this;
    var pins = this.container.find('.jqvmap-pin');
    jQuery.each(pins, function(index, pinObj) {
        pinObj = jQuery(pinObj);
        var countryId = map.getCountryId(pinObj.attr('for').toLowerCase());
        var countryObj = jQuery('#' + countryId);
        var bbox = countryObj[0].getBBox();

        var scale = map.scale;
        var rootCoords = map.canvas.rootGroup.getBoundingClientRect();
        var mapCoords = map.container[0].getBoundingClientRect();
        var coords = {
            left: rootCoords.left - mapCoords.left,
            top: rootCoords.top - mapCoords.top
        };

        var middleX = (bbox.x * scale) + ((bbox.width * scale) / 2);
        var middleY = (bbox.y * scale) + ((bbox.height * scale) / 2);

        pinObj.css({
            left: coords.left + middleX - (pinObj.width() / 2),
            top: coords.top + middleY - (pinObj.height() / 2)
        });
    });
};

JQVMap.prototype.removePin = function(cc) {
    cc = cc.toLowerCase();
    jQuery('#' + this.getPinId(cc)).remove();
};

JQVMap.prototype.removePins = function() {
    this.container.find('.jqvmap-pin').remove();
};

JQVMap.prototype.reset = function() {
    for (var key in this.countries) {
        this.countries[key].setFill(this.color);
    }
    this.scale = this.baseScale;
    this.transX = this.baseTransX;
    this.transY = this.baseTransY;
    this.applyTransform();
    this.zoomCurStep = 1;
};

JQVMap.prototype.resize = function() {
    var curBaseScale = this.baseScale;
    if (this.width / this.height > this.defaultWidth / this.defaultHeight) {
        this.baseScale = this.height / this.defaultHeight;
        this.baseTransX = Math.abs(this.width - this.defaultWidth * this.baseScale) / (2 * this.baseScale);
    } else {
        this.baseScale = this.width / this.defaultWidth;
        this.baseTransY = Math.abs(this.height - this.defaultHeight * this.baseScale) / (2 * this.baseScale);
    }
    this.scale *= this.baseScale / curBaseScale;
    this.transX *= this.baseScale / curBaseScale;
    this.transY *= this.baseScale / curBaseScale;
};

JQVMap.prototype.select = function(cc, path) {
    cc = cc.toLowerCase();
    path = path || jQuery('#' + this.getCountryId(cc))[0];

    if (!this.isSelected(cc)) {
        if (this.multiSelectRegion) {
            this.selectedRegions.push(cc);
        } else {
            this.selectedRegions = [cc];
        }

        jQuery(this.container).trigger('regionSelect.jqvmap', [cc]);
        if (this.selectedColor && path) {
            path.currentFillColor = this.selectedColor;
            path.setFill(this.selectedColor);
        }
    }
};

JQVMap.prototype.selectIndex = function(cc) {
    cc = cc.toLowerCase();
    for (var i = 0; i < this.selectedRegions.length; i++) {
        if (cc === this.selectedRegions[i]) {
            return i;
        }
    }
    return -1;
};

JQVMap.prototype.setBackgroundColor = function(backgroundColor) {
    this.container.css('background-color', backgroundColor);
};

JQVMap.prototype.setColors = function(key, color) {
    if (typeof key === 'string') {
        this.countries[key].setFill(color);
        this.countries[key].setAttribute('original', color);
    } else {
        var colors = key;

        for (var code in colors) {
            if (this.countries[code]) {
                this.countries[code].setFill(colors[code]);
                this.countries[code].setAttribute('original', colors[code]);
            }
        }
    }
};

JQVMap.prototype.setNormalizeFunction = function(f) {
    this.colorScale.setNormalizeFunction(f);

    if (this.values) {
        this.setValues(this.values);
    }
};

JQVMap.prototype.setScale = function(scale) {
    this.scale = scale;
    this.applyTransform();
};

JQVMap.prototype.setScaleColors = function(colors) {
    this.colorScale.setColors(colors);

    if (this.values) {
        this.setValues(this.values);
    }
};

JQVMap.prototype.setValues = function(values) {
    var max = 0,
        min = Number.MAX_VALUE,
        val;

    for (var cc in values) {
        cc = cc.toLowerCase();
        val = parseFloat(values[cc]);

        if (isNaN(val)) {
            continue;
        }
        if (val > max) {
            max = values[cc];
        }
        if (val < min) {
            min = val;
        }
    }

    if (min === max) {
        max++;
    }

    this.colorScale.setMin(min);
    this.colorScale.setMax(max);

    var colors = {};
    for (cc in values) {
        cc = cc.toLowerCase();
        val = parseFloat(values[cc]);
        colors[cc] = isNaN(val) ? this.color : this.colorScale.getColor(val);
    }
    this.setColors(colors);
    this.values = values;
};

JQVMap.prototype.unhighlight = function(cc, path) {
    cc = cc.toLowerCase();
    path = path || jQuery('#' + this.getCountryId(cc))[0];
    path.setOpacity(1);
    if (path.currentFillColor) {
        path.setFill(path.currentFillColor);
    }
};

JQVMap.prototype.zoomIn = function() {
    var map = this;
    var sliderDelta = (jQuery('#zoom').innerHeight() - 6 * 2 - 15 * 2 - 3 * 2 - 7 - 6) / (this.zoomMaxStep - this.zoomCurStep);

    if (map.zoomCurStep < map.zoomMaxStep) {
        map.transX -= (map.width / map.scale - map.width / (map.scale * map.zoomStep)) / 2;
        map.transY -= (map.height / map.scale - map.height / (map.scale * map.zoomStep)) / 2;
        map.setScale(map.scale * map.zoomStep);
        map.zoomCurStep++;

        var $slider = jQuery('#zoomSlider');

        $slider.css('top', parseInt($slider.css('top'), 10) - sliderDelta);

        map.container.trigger('zoomIn');
    }
};

JQVMap.prototype.zoomOut = function() {
    var map = this;
    var sliderDelta = (jQuery('#zoom').innerHeight() - 6 * 2 - 15 * 2 - 3 * 2 - 7 - 6) / (this.zoomMaxStep - this.zoomCurStep);

    if (map.zoomCurStep > 1) {
        map.transX += (map.width / (map.scale / map.zoomStep) - map.width / map.scale) / 2;
        map.transY += (map.height / (map.scale / map.zoomStep) - map.height / map.scale) / 2;
        map.setScale(map.scale / map.zoomStep);
        map.zoomCurStep--;

        var $slider = jQuery('#zoomSlider');

        $slider.css('top', parseInt($slider.css('top'), 10) + sliderDelta);

        map.container.trigger('zoomOut');
    }
};

VectorCanvas.prototype.applyTransformParams = function(scale, transX, transY) {
    if (this.mode === 'svg') {
        this.rootGroup.setAttribute('transform', 'scale(' + scale + ') translate(' + transX + ', ' + transY + ')');
    } else {
        this.rootGroup.coordorigin = (this.width - transX) + ',' + (this.height - transY);
        this.rootGroup.coordsize = this.width / scale + ',' + this.height / scale;
    }
};

VectorCanvas.prototype.createGroup = function(isRoot) {
    var node;
    if (this.mode === 'svg') {
        node = this.createSvgNode('g');
    } else {
        node = this.createVmlNode('group');
        node.style.width = this.width + 'px';
        node.style.height = this.height + 'px';
        node.style.left = '0px';
        node.style.top = '0px';
        node.coordorigin = '0 0';
        node.coordsize = this.width + ' ' + this.height;
    }

    if (isRoot) {
        this.rootGroup = node;
    }
    return node;
};

VectorCanvas.prototype.createPath = function(config) {
    var node;
    if (this.mode === 'svg') {
        node = this.createSvgNode('path');
        node.setAttribute('d', config.path);

        if (this.params.borderColor !== null) {
            node.setAttribute('stroke', this.params.borderColor);
        }
        if (this.params.borderWidth > 0) {
            node.setAttribute('stroke-width', this.params.borderWidth);
            node.setAttribute('stroke-linecap', 'round');
            node.setAttribute('stroke-linejoin', 'round');
        }
        if (this.params.borderOpacity > 0) {
            node.setAttribute('stroke-opacity', this.params.borderOpacity);
        }

        node.setFill = function(color) {
            this.setAttribute('fill', color);
            if (this.getAttribute('original') === null) {
                this.setAttribute('original', color);
            }
        };

        node.getFill = function() {
            return this.getAttribute('fill');
        };

        node.getOriginalFill = function() {
            return this.getAttribute('original');
        };

        node.setOpacity = function(opacity) {
            this.setAttribute('fill-opacity', opacity);
        };
    } else {
        node = this.createVmlNode('shape');
        node.coordorigin = '0 0';
        node.coordsize = this.width + ' ' + this.height;
        node.style.width = this.width + 'px';
        node.style.height = this.height + 'px';
        node.fillcolor = JQVMap.defaultFillColor;
        node.stroked = false;
        node.path = VectorCanvas.pathSvgToVml(config.path);

        var scale = this.createVmlNode('skew');
        scale.on = true;
        scale.matrix = '0.01,0,0,0.01,0,0';
        scale.offset = '0,0';

        node.appendChild(scale);

        var fill = this.createVmlNode('fill');
        node.appendChild(fill);

        node.setFill = function(color) {
            this.getElementsByTagName('fill')[0].color = color;
            if (this.getAttribute('original') === null) {
                this.setAttribute('original', color);
            }
        };

        node.getFill = function() {
            return this.getElementsByTagName('fill')[0].color;
        };
        node.getOriginalFill = function() {
            return this.getAttribute('original');
        };
        node.setOpacity = function(opacity) {
            this.getElementsByTagName('fill')[0].opacity = parseInt(opacity * 100, 10) + '%';
        };
    }
    return node;
};

VectorCanvas.prototype.pathSvgToVml = function(path) {
    var result = '';
    var cx = 0,
        cy = 0,
        ctrlx, ctrly;

    return path.replace(/([MmLlHhVvCcSs])((?:-?(?:\d+)?(?:\.\d+)?,?\s?)+)/g, function(segment, letter, coords) {
        coords = coords.replace(/(\d)-/g, '$1,-').replace(/\s+/g, ',').split(',');
        if (!coords[0]) {
            coords.shift();
        }

        for (var i = 0, l = coords.length; i < l; i++) {
            coords[i] = Math.round(100 * coords[i]);
        }

        switch (letter) {
            case 'm':
                cx += coords[0];
                cy += coords[1];
                result = 't' + coords.join(',');
                break;

            case 'M':
                cx = coords[0];
                cy = coords[1];
                result = 'm' + coords.join(',');
                break;

            case 'l':
                cx += coords[0];
                cy += coords[1];
                result = 'r' + coords.join(',');
                break;

            case 'L':
                cx = coords[0];
                cy = coords[1];
                result = 'l' + coords.join(',');
                break;

            case 'h':
                cx += coords[0];
                result = 'r' + coords[0] + ',0';
                break;

            case 'H':
                cx = coords[0];
                result = 'l' + cx + ',' + cy;
                break;

            case 'v':
                cy += coords[0];
                result = 'r0,' + coords[0];
                break;

            case 'V':
                cy = coords[0];
                result = 'l' + cx + ',' + cy;
                break;

            case 'c':
                ctrlx = cx + coords[coords.length - 4];
                ctrly = cy + coords[coords.length - 3];
                cx += coords[coords.length - 2];
                cy += coords[coords.length - 1];
                result = 'v' + coords.join(',');
                break;

            case 'C':
                ctrlx = coords[coords.length - 4];
                ctrly = coords[coords.length - 3];
                cx = coords[coords.length - 2];
                cy = coords[coords.length - 1];
                result = 'c' + coords.join(',');
                break;

            case 's':
                coords.unshift(cy - ctrly);
                coords.unshift(cx - ctrlx);
                ctrlx = cx + coords[coords.length - 4];
                ctrly = cy + coords[coords.length - 3];
                cx += coords[coords.length - 2];
                cy += coords[coords.length - 1];
                result = 'v' + coords.join(',');
                break;

            case 'S':
                coords.unshift(cy + cy - ctrly);
                coords.unshift(cx + cx - ctrlx);
                ctrlx = coords[coords.length - 4];
                ctrly = coords[coords.length - 3];
                cx = coords[coords.length - 2];
                cy = coords[coords.length - 1];
                result = 'c' + coords.join(',');
                break;

            default:
                break;
        }

        return result;

    }).replace(/z/g, '');
};

VectorCanvas.prototype.setSize = function(width, height) {
    if (this.mode === 'svg') {
        this.canvas.setAttribute('width', width);
        this.canvas.setAttribute('height', height);
    } else {
        this.canvas.style.width = width + 'px';
        this.canvas.style.height = height + 'px';
        this.canvas.coordsize = width + ' ' + height;
        this.canvas.coordorigin = '0 0';
        if (this.rootGroup) {
            var paths = this.rootGroup.getElementsByTagName('shape');
            for (var i = 0, l = paths.length; i < l; i++) {
                paths[i].coordsize = width + ' ' + height;
                paths[i].style.width = width + 'px';
                paths[i].style.height = height + 'px';
            }
            this.rootGroup.coordsize = width + ' ' + height;
            this.rootGroup.style.width = width + 'px';
            this.rootGroup.style.height = height + 'px';
        }
    }
    this.width = width;
    this.height = height;
};
},{}],16:[function(require,module,exports){
/** Add World Map Data Points */
jQuery.fn.vectorMap('addMap', 'world_en', {"width":950,"height":550,"paths":{"id":{"path":"M781.68,324.4l-2.31,8.68l-12.53,4.23l-3.75-4.4l-1.82,0.5l3.4,13.12l5.09,0.57l6.79,2.57v2.57l3.11-0.57l4.53-6.27v-5.13l2.55-5.13l2.83,0.57l-3.4-7.13l-0.52-4.59L781.68,324.4L781.68,324.4M722.48,317.57l-0.28,2.28l6.79,11.41h1.98l14.15,23.67l5.66,0.57l2.83-8.27l-4.53-2.85l-0.85-4.56L722.48,317.57L722.48,317.57M789.53,349.11l2.26,2.77l-1.47,4.16v0.79h3.34l1.18-10.4l1.08,0.3l1.96,9.5l1.87,0.5l1.77-4.06l-1.77-6.14l-1.47-2.67l4.62-3.37l-1.08-1.49l-4.42,2.87h-1.18l-2.16-3.17l0.69-1.39l3.64-1.78l5.5,1.68l1.67-0.1l4.13-3.86l-1.67-1.68l-3.83,2.97h-2.46l-3.73-1.78l-2.65,0.1l-2.95,4.75l-1.87,8.22L789.53,349.11L789.53,349.11M814.19,330.5l-1.87,4.55l2.95,3.86h0.98l1.28-2.57l0.69-0.89l-1.28-1.39l-1.87-0.69L814.19,330.5L814.19,330.5M819.99,345.45l-4.03,0.89l-1.18,1.29l0.98,1.68l2.65-0.99l1.67-0.99l2.46,1.98l1.08-0.89l-1.96-2.38L819.99,345.45L819.99,345.45M753.17,358.32l-2.75,1.88l0.59,1.58l8.75,1.98l4.42,0.79l1.87,1.98l5.01,0.4l2.36,1.98l2.16-0.5l1.97-1.78l-3.64-1.68l-3.14-2.67l-8.16-1.98L753.17,358.32L753.17,358.32M781.77,366.93l-2.16,1.19l1.28,1.39l3.14-1.19L781.77,366.93L781.77,366.93M785.5,366.04l0.39,1.88l2.26,0.59l0.88-1.09l-0.98-1.49L785.5,366.04L785.5,366.04M790.91,370.99l-2.75,0.4l2.46,2.08h1.96L790.91,370.99L790.91,370.99M791.69,367.72l-0.59,1.19l4.42,0.69l3.44-1.98l-1.96-0.59l-3.14,0.89l-1.18-0.99L791.69,367.72L791.69,367.72M831.93,339.34l-4.17,0.47l-2.68,1.96l1.11,2.24l4.54,0.84v0.84l-2.87,2.33l1.39,4.85l1.39,0.09l1.2-4.76h2.22l0.93,4.66l10.83,8.96l0.28,7l3.7,4.01l1.67-0.09l0.37-24.72l-6.29-4.38l-5.93,4.01l-2.13,1.31l-3.52-2.24l-0.09-7.09L831.93,339.34L831.93,339.34z","name":"Indonesia"},"pg":{"path":"M852.76,348.29l-0.37,24.44l3.52-0.19l4.63-5.41l3.89,0.19l2.5,2.24l0.83,6.9l7.96,4.2l2.04-0.75v-2.52l-6.39-5.32l-3.15-7.28l2.5-1.21l-1.85-4.01l-3.7-0.09l-0.93-4.29l-9.81-6.62L852.76,348.29L852.76,348.29M880.48,349l-0.88,1.25l4.81,4.26l0.66,2.5l1.31-0.15l0.15-2.57l-1.46-1.32L880.48,349L880.48,349M882.89,355.03l-0.95,0.22l-0.58,2.57l-1.82,1.18l-5.47,0.96l0.22,2.06l5.76-0.29l3.65-2.28l-0.22-3.97L882.89,355.03L882.89,355.03M889.38,359.51l1.24,3.45l2.19,2.13l0.66-0.59l-0.22-2.28l-2.48-3.01L889.38,359.51L889.38,359.51z","name":"Papua New Guinea"},"mx":{"path":"M137.49,225.43l4.83,15.21l-2.25,1.26l0.25,3.02l4.25,3.27v6.05l5.25,5.04l-2.25-14.86l-3-9.83l0.75-6.8l2.5,0.25l1,2.27l-1,5.79l13,25.44v9.07l10.5,12.34l11.5,5.29l4.75-2.77l6.75,5.54l4-4.03l-1.75-4.54l5.75-1.76l1.75,1.01l1.75-1.76h2.75l5-8.82l-2.5-2.27l-9.75,2.27l-2.25,6.55l-5.75,1.01l-6.75-2.77l-3-9.57l2.27-12.07l-4.64-2.89l-2.21-11.59l-1.85-0.79l-3.38,3.43l-3.88-2.07l-1.52-7.73l-15.37-1.61l-7.94-5.97L137.49,225.43L137.49,225.43z","name":"Mexico"},"ee":{"path":"M517.77,143.66l-5.6-0.2l-3.55,2.17l-0.05,1.61l2.3,2.17l7.15,1.21L517.77,143.66L517.77,143.66M506.76,147.64l-1.55-0.05l-0.9,0.91l0.65,0.96l1.55,0.1l0.8-1.16L506.76,147.64L506.76,147.64z","name":"Estonia"},"dz":{"path":"M473.88,227.49l-4.08-1.37l-16.98,3.19l-3.7,2.81l2.26,11.67l-6.75,0.27l-4.06,6.53l-9.67,2.32l0.03,4.75l31.85,24.35l5.43,0.46l18.11-14.15l-1.81-2.28l-3.4-0.46l-2.04-3.42v-14.15l-1.36-1.37l0.23-3.65l-3.62-3.65l-0.45-3.88l1.58-1.14l-0.68-4.11L473.88,227.49L473.88,227.49z","name":"Algeria"},"ma":{"path":"M448.29,232.28h-11.55l-2.26,5.02l-5.21,2.51l-4.3,11.64l-8.38,5.02l-11.77,19.39l11.55-0.23l0.45-5.7h2.94v-7.76h10.19l0.23-10.04l9.74-2.28l4.08-6.62l6.34-0.23L448.29,232.28L448.29,232.28z","name":"Morocco"},"mr":{"path":"M404.9,276.66l2.18,2.85l-0.45,12.32l3.17-2.28l2.26-0.46l3.17,1.14l3.62,5.02l3.4-2.28l16.53-0.23l-4.08-27.61l4.38-0.02l-8.16-6.25l0.01,4.06l-10.33,0.01l-0.05,7.75l-2.97-0.01l-0.38,5.72L404.9,276.66L404.9,276.66z","name":"Mauritania"},"sn":{"path":"M412.03,289.84L410.12,290.31L406.18,293.18L405.28,294.78L405,296.37L406.43,297.40L411.28,297.34L414.40,296.5L414.75,298.03L414.46,300.06L414.53,300.09L406.78,300.21L408.03,303.21L408.71,301.37L418,302.15L418.06,302.21L419.03,302.25L422,302.37L422.12,300.62L418.53,296.31L414.53,290.87L412.03,289.84z","name":"Senegal"},"gm":{"path":"M406.89,298.34l-0.13,1.11l6.92-0.1l0.35-1.03l-0.15-1.04l-1.99,0.81L406.89,298.34L406.89,298.34z","name":"Gambia"},"gw":{"path":"M408.6,304.53l1.4,2.77l3.93-3.38l0.04-1.04l-4.63-0.67L408.6,304.53L408.6,304.53z","name":"Guinea-Bissau"},"gn":{"path":"M410.42,307.94l3.04,4.68l3.96-3.44l4.06-0.18l3.38,4.49l2.87,1.89l1.08-2.1l0.96-0.54l-0.07-4.62l-1.91-5.48l-5.86,0.65l-7.25-0.58l-0.04,1.86L410.42,307.94L410.42,307.94z","name":"Guinea"},"sl":{"path":"M413.93,313.13l5.65,5.46l4.03-4.89l-2.52-3.95l-3.47,0.35L413.93,313.13L413.93,313.13z","name":"Sierra Leone"},"lr":{"path":"M420.17,319.19l10.98,7.34l-0.26-5.56l-3.32-3.91l-3.24-2.87L420.17,319.19L420.17,319.19z","name":"Liberia"},"ci":{"path":"M432.07,326.75l4.28-3.03l5.32-0.93l5.43,1.17l-2.77-4.19l-0.81-2.56l0.81-7.57l-4.85,0.23l-2.2-2.1l-4.62,0.12l-2.2,0.35l0.23,5.12l-1.16,0.47l-1.39,2.56l3.58,4.19L432.07,326.75L432.07,326.75z","name":"Cote d'Ivoire"},"ml":{"path":"M419.46,295.84l3.08-2.11l17.12-0.1l-3.96-27.54l4.52-0.13l21.87,16.69l2.94,0.42l-1.11,9.28l-13.75,1.25l-10.61,7.92l-1.93,5.42l-7.37,0.31l-1.88-5.41l-5.65,0.4l0.22-1.77L419.46,295.84L419.46,295.84z","name":"Mali"},"bf":{"path":"M450.59,294.28l3.64-0.29l5.97,8.44l-5.54,4.18l-4.01-1.03l-5.39,0.07l-0.87,3.16l-4.52,0.22l-1.24-1.69l1.6-5.14L450.59,294.28L450.59,294.28z","name":"Burkina Faso"},"ne":{"path":"M460.89,302l2.55-0.06l2.3-3.45l3.86-0.69l4.11,2.51l8.77,0.25l6.78-2.76l2.55-2.19l0.19-2.88l4.73-4.77l1.25-10.53l-3.11-6.52l-7.96-1.94l-18.42,14.36l-2.61-0.25l-1.12,9.97l-9.4,0.94L460.89,302L460.89,302z","name":"Niger"},"gh":{"path":"M444.34,317.05l1.12,2.63l2.92,4.58l1.62-0.06l4.42-2.51l-0.31-14.29l-3.42-1l-4.79,0.13L444.34,317.05L444.34,317.05z","name":"Ghana"},"tg":{"path":"M455.22,321.25l2.68-1.57l-0.06-10.35l-1.74-2.82l-1.12,0.94L455.22,321.25L455.22,321.25z","name":"Togo"},"bj":{"path":"M458.71,319.49h2.12l0.12-6.02l2.68-3.89l-0.12-6.77l-2.43-0.06l-4.17,3.26l1.74,3.32L458.71,319.49L458.71,319.49z","name":"Benin"},"ng":{"path":"M461.57,319.37l3.92,0.19l4.73,5.27l2.3,0.63l1.8-0.88l2.74-0.38l0.93-3.82l3.73-2.45l4.04-0.19l7.4-13.61l-0.12-3.07l-3.42-2.63l-6.84,3.01l-9.15-0.13l-4.36-2.76l-3.11,0.69l-1.62,2.82l-0.12,7.96l-2.61,3.7L461.57,319.37L461.57,319.37z","name":"Nigeria"},"tn":{"path":"M474.91,227.33l5.53-2.23l1.82,1.18l0.07,1.44l-0.85,1.11l0.13,1.97l0.85,0.46v3.54l-0.98,1.64l0.13,1.05l3.71,1.31l-2.99,4.65l-1.17-0.07l-0.2,3.74l-1.3,0.2l-1.11-0.98l0.26-3.8l-3.64-3.54l-0.46-3.08l1.76-1.38L474.91,227.33L474.91,227.33z","name":"Tunisia"},"ly":{"path":"M480.05,248.03l1.56-0.26l0.46-3.6h0.78l3.19-5.24l7.87,2.29l2.15,3.34l7.74,3.54l4.03-1.7l-0.39-1.7l-1.76-1.7l0.2-1.18l2.86-2.42h5.66l2.15,2.88l4.55,0.66l0.59,36.89l-3.38-0.13l-20.42-10.62l-2.21,1.25l-8.39-2.1l-2.28-3.01l-3.32-0.46l-1.69-3.01L480.05,248.03L480.05,248.03z","name":"Libya"},"eg":{"path":"M521.93,243.06l2.67,0.07l5.2,1.44l2.47,0.07l3.06-2.56h1.43l2.6,1.44h3.29l0.59-0.04l2.08,5.98l0.59,1.93l0.55,2.89l-0.98,0.72l-1.69-0.85l-1.95-6.36l-1.76-0.13l-0.13,2.16l1.17,3.74l9.37,11.6l0.2,4.98l-2.73,3.15L522.32,273L521.93,243.06L521.93,243.06z","name":"Egypt"},"td":{"path":"M492.79,296l0.13-2.95l4.74-4.61l1.27-11.32l-3.16-6.04l2.21-1.13l21.4,11.15l-0.13,10.94l-3.77,3.21v5.64l2.47,4.78h-4.36l-7.22,7.14l-0.19,2.16l-5.33-0.07l-0.07,0.98l-3.04-0.4l-2.08-3.93l-1.56-0.77l0.2-1.2l1.96-1.5v-7.02l-2.71-0.42l-3.27-2.43L492.79,296L492.79,296L492.79,296z","name":"Chad"},"sd":{"path":"M520.15,292.43l0.18-11.83l2.46,0.07l-0.28-6.57l25.8,0.23l3.69-3.72l7.96,12.73l-4.36,5.14v7.85l-6.86,14.75l-2.36,1.04l0.75,4.11h2.94l3.99,5.79l-3.2,0.41l-0.82,1.49l-0.08,2.15l-9.6-0.17l-0.98-1.49l-6.71-0.38l-12.32-12.68l1.23-0.74l0.33-2.98l-2.95-1.74l-2.69-5.31l0.15-4.94L520.15,292.43L520.15,292.43z","name":"Sudan"},"cm":{"path":"M477.82,324.28l3.22,2.96l-0.23,4.58l17.66-0.41l1.44-1.62l-5.06-5.45l-0.75-1.97l3.22-6.03l-2.19-4l-1.84-0.99v-2.03l2.13-1.39l0.12-6.32l-1.69-0.19l-0.03,3.32l-7.42,13.85l-4.54,0.23l-3.11,2.14L477.82,324.28L477.82,324.28z","name":"Cameroon"},"er":{"path":"M556.71,294.7l-0.25-5.89l3.96-4.62l1.07,0.82l1.95,6.52l9.36,6.97l-1.7,2.09l-6.85-5.89H556.71L556.71,294.7z","name":"Eritrea"},"dj":{"path":"M571.48,301.54l-0.57,3.36l3.96-0.06l0.06-4.94l-1.45-0.89L571.48,301.54L571.48,301.54z","name":"Djibouti"},"et":{"path":"M549.49,311.76l7.28-16.2l7.23,0.04l6.41,5.57l-0.45,4.59h4.97l0.51,2.76l8.04,4.81l4.96,0.25l-9.43,10.13l-12.95,3.99h-3.21l-5.72-4.88l-2.26-0.95l-4.38-6.45l-2.89,0.04l-0.34-2.96L549.49,311.76L549.49,311.76z","name":"Ethiopia"},"so":{"path":"M575.74,305.04l4.08,2.78l1.21-0.06l10.13-3.48l1.15,3.71l-0.81,3.13l-2.19,1.74l-5.47-0.35l-7.83-4.81L575.74,305.04L575.74,305.04M591.97,304.05l4.37-1.68l1.55,0.93l-0.17,3.88l-4.03,11.48l-21.81,23.36l-2.53-1.74l-0.17-9.86l3.28-3.77l6.96-2.15l10.21-10.78l2.67-2.38l0.75-3.48L591.97,304.05L591.97,304.05z","name":"Somalia"},"ye":{"path":"M599.62,299.65l2.13,2.38l2.88-1.74l1.04-0.35l-1.32-1.28l-2.53,0.75L599.62,299.65L599.62,299.65M571.99,289.23l1.44,4.28v4.18l3.46,3.14l24.38-9.93l0.23-2.73l-3.91-7.02l-9.81,3.13l-5.63,5.54l-6.53-3.86L571.99,289.23L571.99,289.23z","name":"Yemen"},"cf":{"path":"M495.66,324.05l4.66,5.04l1.84-2.38l2.93,0.12l0.63-2.32l2.88-1.8l5.98,4.12l3.45-3.42l13.39,0.59L519,311.18l1.67-1.04l0.23-2.26l-2.82-1.33h-4.14l-6.67,6.61l-0.23,2.72l-5.29-0.17l-0.17,1.16l-3.45-0.35l-3.11,5.91L495.66,324.05L495.66,324.05z","name":"Central African Republic"},"st":{"path":"M470.74,337.15l1.15-0.58l0.86,0.7l-0.86,1.33l-1.04-0.41L470.74,337.15L470.74,337.15M473.05,333.5l1.73-0.29l0.58,1.1l-0.86,0.93l-0.86-0.12L473.05,333.5L473.05,333.5z","name":"Sao Tome and Principe"},"gq":{"path":"M476.84,327.41l-0.46,1.97l1.38,0.75l1.32-0.99l-0.46-2.03L476.84,327.41L476.84,327.41M480.99,332.69l-0.06,1.39l4.54,0.23l-0.06-1.57L480.99,332.69L480.99,332.69z","name":"Equatorial Guinea"},"ga":{"path":"M486.39,332.63l-0.12,2.49l-5.64-0.12l-3.45,6.67l8.11,8.87l2.01-1.68l-0.06-1.74l-1.38-0.64v-1.22l3.11-1.97l2.76,2.09l3.05,0.06l-0.06-10.49l-4.83-0.23l-0.06-2.2L486.39,332.63L486.39,332.63z","name":"Gabon"},"cg":{"path":"M491,332.52l-0.06,1.45l4.78,0.12l0.17,12.41l-4.37-0.12l-2.53-1.97l-1.96,1.1l-0.09,0.55l1.01,0.49l0.29,2.55l-2.7,2.32l0.58,1.22l2.99-2.32h1.44l0.46,1.39l1.9,0.81l6.1-5.16l-0.12-3.77l1.27-3.07l3.91-2.9l1.05-9.81l-2.78,0.01l-3.22,4.41L491,332.52L491,332.52z","name":"Congo"},"ao":{"path":"M486.55,353.23l1.74,2.26l2.25-2.13l-0.66-2.21l-0.56-0.04L486.55,353.23L486.55,353.23M488.62,356.71l3.41,12.73l-0.08,4.02l-4.99,5.36l-0.75,8.71l19.2,0.17l6.24,2.26l5.15-0.67l-3-3.76l0.01-10.74l5.9-0.25v-4.19l-4.79-0.2l-0.96-9.92l-2.02,0.03l-1.09-0.98l-1.19,0.06l-1.58,3.06H502l-1.41-1.42l0.42-2.01l-1.66-2.43L488.62,356.71L488.62,356.71z","name":"Angola"},"cd":{"path":"M489.38,355.71l10.31-0.18l2.09,2.97l-0.08,2.19l0.77,0.7h5.12l1.47-2.89h2.09l0.85,0.86l2.87-0.08l0.85,10.08l4.96,0.16v0.78l13.33,6.01l0.62,1.17h2.79l-0.31-4.22l-5.04-2.42l0.31-3.2l2.17-5.08l4.96-0.16l-4.26-14.14l0.08-6.01l6.74-10.54l0.08-1.48l-1.01-0.55l0.04-2.86l-1.23-0.11l-1.24-1.58l-20.35-0.92l-3.73,3.63l-6.11-4.02l-2.15,1.32l-1.56,13.13l-3.86,2.98l-1.16,2.64l0.21,3.91l-6.96,5.69l-1.85-0.84l0.25,1.09L489.38,355.71L489.38,355.71z","name":"Congo"},"rw":{"path":"M537.82,339.9l2.81,2.59l-0.12,2.77l-4.36,0.09v-3.06L537.82,339.9L537.82,339.9z","name":"Rwanda"},"bi":{"path":"M536.21,346.21l4.27-0.09l-1.11,3.74l-1.08,0.94h-1.32l-0.94-2.53L536.21,346.21L536.21,346.21z","name":"Burundi"},"ug":{"path":"M538.3,339.09l3.03,2.84l1.9-1.21l5.14-0.84l0.88,0.09l0.33-1.95l2.9-6.1l-2.44-5.08l-7.91,0.05l-0.05,2.09l1.06,1.02l-0.16,2.09L538.3,339.09L538.3,339.09z","name":"Uganda"},"ke":{"path":"M550.83,326.52l2.66,5.19l-3.19,6.69l-0.42,2.03l15.93,9.85l4.94-7.76l-2.5-2.03l-0.05-10.22l3.13-3.42l-4.99,1.66l-3.77,0.05l-5.9-4.98l-1.86-0.8l-3.45,0.32l-0.61,1.02L550.83,326.52L550.83,326.52z","name":"Kenya"},"tz":{"path":"M550.57,371.42l17.47-2.14l-3.93-7.6l-0.21-7.28l1.27-3.48l-16.62-10.44l-5.21,0.86l-1.81,1.34l-0.16,3.05l-1.17,4.23l-1.22,1.45l-1.75,0.16l3.35,11.61l5.47,2.57l3.77,0.11L550.57,371.42L550.57,371.42z","name":"Tanzania"},"zm":{"path":"M514.55,384.7l3.17,4.4l4.91,0.3l1.74,0.96l5.14,0.06l4.43-6.21l12.38-5.54l1.08-4.88l-1.44-6.99l-6.46-3.68l-4.31,0.3l-2.15,4.76l0.06,2.17l5.08,2.47l0.3,5.37l-4.37,0.24l-1.08-1.81l-12.14-5.18l-0.36,3.98l-5.74,0.18L514.55,384.7L514.55,384.7z","name":"Zambia"},"mw":{"path":"M547.16,379.4l3.11,3.25l-0.06,4.16l0.6,1.75l4.13-4.46l-0.48-5.67l-2.21-1.69l-1.97-9.95l-3.41-0.12l1.55,7.17L547.16,379.4L547.16,379.4z","name":"Malawi"},"mz":{"path":"M541.17,413.28l2.69,2.23l6.34-3.86l1.02-5.73v-9.46l10.17-8.32l1.74,0.06l6.16-5.91l-0.96-12.18L552,372.17l0.48,3.68l2.81,2.17l0.66,6.63l-5.5,5.37l-1.32-3.01l0.24-3.98l-3.17-3.44l-7.78,3.62l7.24,3.68l0.24,10.73l-4.79,7.11L541.17,413.28L541.17,413.28z","name":"Mozambique"},"zw":{"path":"M524.66,392.3l8.97,10.13l6.88,1.75l4.61-7.23l-0.36-9.58l-7.48-3.86l-2.81,1.27l-4.19,6.39l-5.8-0.06L524.66,392.3L524.66,392.3z","name":"Zimbabwe"},"na":{"path":"M496.55,421.96l3.35,0.24l1.97,1.99l4.67,0.06l1.14-13.26v-8.68l2.99-0.6l1.14-9.1l7.6-0.24l2.69-2.23l-4.55-0.18l-6.16,0.84l-6.64-2.41h-18.66l0.48,5.3l6.22,9.16l-1.08,4.7l0.06,2.47L496.55,421.96L496.55,421.96z","name":"Namibia"},"bw":{"path":"M508.51,411.23l2.15,0.66l-0.3,6.15l2.21,0.3l5.08-4.58l6.1,0.66l1.62-4.1l7.72-7.05l-9.27-10.67l-0.12-1.75l-1.02-0.3l-2.81,2.59l-7.3,0.18l-1.02,9.1l-2.87,0.66L508.51,411.23L508.51,411.23z","name":"Botswana"},"sz":{"path":"M540.87,414l-2.51,0.42l-1.08,2.95l1.92,1.75h2.33l1.97-2.83L540.87,414L540.87,414z","name":"Swaziland"},"ls":{"path":"M527.41,425.39l3.05-2.35l1.44,0.06l1.74,2.17l-0.18,2.17l-2.93,1.08v0.84l-3.23-0.18l-0.78-2.35L527.41,425.39L527.41,425.39z","name":"Lesotho"},"za":{"path":"M534.16,403.63l-7.9,7.3l-1.88,4.51l-6.26-0.78l-5.21,4.63l-3.46-0.34l0.28-6.4l-1.23-0.43l-0.86,13.09l-6.14-0.06l-1.85-2.18l-2.71-0.03l2.47,7.09l4.41,4.17l-3.15,3.67l2.04,4.6l4.72,1.8l3.76-3.2l10.77,0.06l0.77-0.96l4.78-0.84l16.17-16.1l-0.06-5.07l-1.73,2.24h-2.59l-3.15-2.64l1.6-3.98l2.75-0.56l-0.25-8.18L534.16,403.63L534.16,403.63z M530.37,422.13l1.51-0.06l2.45,2.66l-0.07,3.08l-2.87,1.45l-0.18,1.02l-4.38,0.05l-1.37-3.3l1.25-2.42L530.37,422.13L530.37,422.13z","name":"South Africa"},"gl":{"path":"M321.13,50.07l-1.36,2.17l2.45,2.45l-1.09,2.45l3.54,4.62l4.35-1.36l5.71-0.54l6.53,7.07l4.35,11.69l-3.53,7.34l4.89-0.82l2.72,1.63l0.27,3.54l-5.98,0.27l3.26,3.26l4.08,0.82l-8.97,11.96l-1.09,7.34l1.9,5.98l-1.36,3.54l2.45,7.61l4.62,5.17l1.36-0.27l2.99-0.82l0.27,4.35l1.9,2.72l3.53-0.27l2.72-10.06l8.16-10.06l12.24-4.89l7.61-9.52l3.53,1.63h7.34l5.98-5.98l7.34-2.99l0.82-4.62l-4.62-4.08l-4.08-1.36l-2.18-5.71l5.17-2.99l8.16,4.35l2.72-2.99l-4.35-2.45l9.25-12.51l-1.63-5.44l-4.35-0.27l1.63-4.89l5.44-2.45l11.15-9.79l-3.26-3.53l-12.51,1.09l-6.53,6.53l3.81-8.43l-4.35-1.09l-2.45,4.35l-3.53-2.99l-9.79,1.09l2.72-4.35l16.04-0.54l-4.08-5.44l-17.4-3.26l-7.07,1.09l0.27,3.54l-7.34-2.45l0.27-2.45l-5.17,1.09l-1.09,2.72l5.44,1.9l-5.71,4.08l-4.08-4.62l-5.71-1.63l-0.82,4.35h-5.71l-2.18-4.62l-8.97-1.36l-4.89,2.45l-0.27,3.26l-6.25-0.82l-3.81,1.63l0.27,3.81v1.9l-7.07,1.36l-3.26-2.17l-2.18,3.53l3.26,3.54l6.8-0.82l0.54,2.18l-5.17,2.45L321.13,50.07L321.13,50.07M342.89,92.49l1.63,2.45l-0.82,2.99h-1.63l-2.18-2.45l0.54-1.9L342.89,92.49L342.89,92.49M410.87,85.69l4.62,1.36l-0.27,3.81l-4.89-2.45l-1.09-1.36L410.87,85.69L410.87,85.69z","name":"Greenland"},"au":{"path":"M761.17,427.98l-0.35,25.38l-3.9,2.86l-0.35,2.5l5.32,3.57l13.13-2.5h6.74l2.48-3.58l14.9-2.86l10.64,3.22l-0.71,4.29l1.42,4.29l8.16-1.43l0.35,2.14l-5.32,3.93l1.77,1.43l3.9-1.43l-1.06,11.8l7.45,5.72l4.26-1.43l2.13,2.14l12.42-1.79l11.71-18.95l4.26-1.07l8.51-15.73l2.13-13.58l-5.32-6.79l2.13-1.43l-4.26-13.23l-4.61-3.22l0.71-17.87l-4.26-3.22l-1.06-10.01h-2.13l-7.1,23.59l-3.9,0.36l-8.87-8.94l4.97-13.23l-9.22-1.79l-10.29,2.86l-2.84,8.22l-4.61,1.07l-0.35-5.72l-18.8,11.44l0.35,4.29l-2.84,3.93h-7.1l-15.26,6.43L761.17,427.98L761.17,427.98M825.74,496.26l-1.77,7.15l0.35,5l5.32-0.36l6.03-9.29L825.74,496.26L825.74,496.26z","name":"Australia"},"nz":{"path":"M913.02,481.96l1.06,11.8l-1.42,5.36l-5.32,3.93l0.35,4.65v5l1.42,1.79l14.55-12.51v-2.86h-3.55l-4.97-16.8L913.02,481.96L913.02,481.96M902.38,507.7l2.84,5.36l-7.81,7.51l-0.71,3.93l-5.32,0.71l-8.87,8.22l-8.16-3.93l-0.71-2.86l14.9-6.43L902.38,507.7L902.38,507.7z","name":"New Zealand"},"nc":{"path":"M906.64,420.47l-0.35,1.79l4.61,6.43l2.48,1.07l0.35-2.5L906.64,420.47L906.64,420.47z","name":"New Caledonia"},"my":{"path":"M764.14,332.92l3.02,3.49l11.58-4.01l2.29-8.84l5.16-0.37l4.72-3.42l-6.12-4.46l-1.4-2.45l-3.02,5.57l1.11,3.2l-1.84,2.67l-3.47-0.89l-8.41,6.17l0.22,3.57L764.14,332.92L764.14,332.92M732.71,315.45l2.01,4.51l0.45,5.86l2.69,4.17l6.49,3.94l2.46,0.23l-0.45-4.06l-2.13-5.18l-3.12-6.63l-0.26,1.16l-3.76-0.17l-2.7-3.88L732.71,315.45L732.71,315.45z","name":"Malaysia"},"bn":{"path":"M779.77,319.25l-2.88,3.49l2.36,0.74l1.33-1.86L779.77,319.25L779.77,319.25z","name":"Brunei Darussalam"},"tl":{"path":"M806.14,368.42l-5.11,4.26l0.49,1.09l2.16-0.4l2.55-2.38l5.01-0.69l-0.98-1.68L806.14,368.42L806.14,368.42z","name":"Timor-Leste"},"sb":{"path":"M895.43,364.65l0.15,2.28l1.39,1.32l1.31-0.81l-1.17-2.43L895.43,364.65L895.43,364.65M897.18,370.31l-1.17,1.25l1.24,2.28l1.46,0.44l-0.07-1.54L897.18,370.31L897.18,370.31M900.03,368.99l1.02,2.5l1.97,2.35l1.09-1.76l-1.46-2.5L900.03,368.99L900.03,368.99M905.14,372.74l0.58,3.09l1.39,1.91l1.17-2.42L905.14,372.74L905.14,372.74M906.74,379.65l-0.51,0.88l1.68,2.21l1.17,0.07l-0.73-2.87L906.74,379.65L906.74,379.65M903.02,384.05l-1.75,0.81l1.53,2.13l1.31-0.74L903.02,384.05L903.02,384.05z","name":"Solomon Islands"},"vu":{"path":"M920.87,397.22l-1.24,1.66l0.52,1.87l0.62,0.42l1.13-1.46L920.87,397.22L920.87,397.22M921.49,402.31l0.1,1.35l1.34,0.42l0.93-0.52l-0.93-1.46L921.49,402.31L921.49,402.31M923.45,414.37l-0.62,0.94l0.93,1.04l1.55-0.52L923.45,414.37L923.45,414.37z","name":"Vanuatu"},"fj":{"path":"M948.62,412.29l-1.24,1.66l-0.1,1.87l1.44,1.46L948.62,412.29L948.62,412.29z","name":"Fiji"},"ph":{"path":"M789.37,297.53l-0.86,1.64l-0.48,2.02l-4.78,6.07l0.29,1.25l2.01-0.29l6.21-6.94L789.37,297.53L789.37,297.53M797.11,295.22l-0.1,5.01l1.82,1.83l0.67,3.56l1.82,0.39l0.86-2.22l-1.43-1.06l-0.38-6.26L797.11,295.22L797.11,295.22M802.28,297.15l-0.1,4.43l1.05,1.73l1.82-2.12l-0.48-3.85L802.28,297.15L802.28,297.15M803.42,293.29l1.82,2.41l0.86,2.31h1.63l-0.29-3.95l-1.82-1.25L803.42,293.29L803.42,293.29M806.96,302.35l0.38,2.89l-3.35,2.7l-2.77,0.29l-2.96,3.18l0.1,1.45l2.77-0.87l1.91-1.25l1.63,4.14l2.87,2.02l1.15-0.39l1.05-1.25l-2.29-2.31l1.34-1.06l1.53,1.25l1.05-1.73l-1.05-2.12l-0.19-4.72L806.96,302.35L806.96,302.35M791.38,272.97l-2.58,1.83l-0.29,5.78l4.02,7.8l1.34,1.06l1.72-1.16l2.96,0.48l0.57,2.6l2.2,0.19l1.05-1.44l-1.34-1.83l-1.63-1.54l-3.44-0.38l-1.82-2.99l2.1-3.18l0.19-2.79l-1.43-3.56L791.38,272.97L791.38,272.97M792.72,290.21l0.76,2.7l1.34,0.87l0.96-1.25l-1.53-2.12L792.72,290.21L792.72,290.21z","name":"Philippines"},"cn":{"path":"M759.83,270.17l-2.39,0.67l-1.72,2.12l1.43,2.79l2.1,0.19l2.39-2.12l0.57-2.79L759.83,270.17L759.83,270.17M670.4,170.07l-3.46,8.7l-4.77-0.25l-5.03,11.01l4.27,5.44l-8.8,12.15l-4.52-0.76l-3.02,3.8l0.75,2.28l3.52,0.25l1.76,4.05l3.52,0.76l10.81,13.93v7.09l5.28,3.29l5.78-1.01l7.29,4.3l8.8,2.53l4.27-0.51l4.78-0.51l10.05-6.58l3.27,0.51l1.25,2.97l2.77,0.83l3.77,5.57l-2.51,5.57l1.51,3.8l4.27,1.52l0.75,4.56l5.03,0.51l0.75-2.28l7.29-3.8l4.52,0.25l5.28,5.82l3.52-1.52l2.26,0.25l1.01,2.79l1.76,0.25l2.51-3.54l10.05-3.8l9.05-10.89l3.02-10.38l-0.25-6.84l-3.77-0.76l2.26-2.53l-0.5-4.05l-9.55-9.62v-4.81l2.76-3.54l2.76-1.27l0.25-2.79h-7.04l-1.26,3.8l-3.27-0.76l-4.02-4.3l2.51-6.58l3.52-3.8l3.27,0.25l-0.5,5.82l1.76,1.52l4.27-4.3l1.51-0.25l-0.5-3.29l4.02-4.81l3.02,0.25l1.76-5.57l2.06-1.09l0.21-3.47l-2-2.1l-0.17-5.48l3.85-0.25l-0.25-14.13l-2.7,1.62l-1.01,3.62l-4.51-0.01l-13.07-7.35l-9.44-11.38l-9.58-0.1l-2.44,2.12l3.1,7.1l-1.08,6.66l-3.86,1.6l-2.17-0.17l-0.16,6.59l2.26,0.51l4.02-1.77l5.28,2.53v2.53l-3.77,0.25l-3.02,6.58l-2.76,0.25l-9.8,12.91l-10.3,4.56l-7.04,0.51l-4.77-3.29l-6.79,3.55l-7.29-2.28l-1.76-4.81l-12.31-0.76l-6.53-10.63h-2.76l-2.22-4.93L670.4,170.07z","name":"China"},"tw":{"path":"M787.46,248.31l-3.54,2.7l-0.19,5.2l3.06,3.56l0.76-0.67L787.46,248.31L787.46,248.31z","name":"Taiwan"},"jp":{"path":"M803.23,216.42l-1.63,1.64l0.67,2.31l1.43,0.1l0.96,5.01l1.15,1.25l2.01-1.83l0.86-3.28l-2.49-3.56L803.23,216.42L803.23,216.42M812.03,213.15l-2.77,2.6l-0.1,2.99l0.67,0.87l3.73-3.18l-0.29-3.18L812.03,213.15L812.03,213.15M808.2,206.98l-4.88,5.59l0.86,1.35l2.39,0.29l4.49-3.47l3.16-0.58l2.87,3.37l2.2-0.77l0.86-3.28l4.11-0.1l4.02-4.82l-2.1-8l-0.96-4.24l2.1-1.73l-4.78-7.22l-1.24,0.1l-2.58,2.89v2.41l1.15,1.35l0.38,6.36l-2.96,3.66l-1.72-1.06l-1.34,2.99l-0.29,2.79l1.05,1.64l-0.67,1.25l-2.2-1.83h-1.53l-1.34,0.77L808.2,206.98L808.2,206.98M816.43,163.44l-1.53,1.35l0.77,2.89l1.34,1.35l-0.1,4.43l-1.72,0.67l-1.34,2.99l3.92,5.39l2.58-0.87l0.48-1.35l-2.77-2.5l1.72-2.22l1.82,0.29l1.43,1.54l0.1-3.18l3.92-3.18l2.2-0.58l-1.82-3.08l-0.86-1.35l-1.43,0.96l-1.24,1.54l-2.68-0.58l-2.77-1.83L816.43,163.44L816.43,163.44z","name":"Japan"},"ru":{"path":"M506.61,151.72l-1.5-0.15l-2.7,3.23v1.51l0.9,0.35l1.75,0.05l2.9-2.37l0.4-0.81L506.61,151.72L506.61,151.72M830.86,160.45l-2.68,3.76l0.19,1.83l1.34-0.58l3.15-3.95L830.86,160.45L830.86,160.45M834.4,154.96l-0.96,2.6l0.1,1.73l1.63-1.06l1.53-3.08V154L834.4,154.96L834.4,154.96M840.04,132.03l-1.24,1.54l0.1,2.41l1.15-0.1l1.91-3.37L840.04,132.03L840.04,132.03M837.75,137.91v4.24l1.34,0.48l0.96-1.54v-3.27L837.75,137.91L837.75,137.91M798.64,122.59l-0.09,6.17l7.74,11.95l2.77,10.4l4.88,9.25l1.91,0.67l1.63-1.35l0.76-2.22l-6.98-7.61l0.19-3.95l1.53-0.67l0.38-2.31l-13.67-19.36L798.64,122.59L798.64,122.59M852.57,103.42l-1.91,0.19l1.15,1.64l2.39,1.64l0.67-0.77L852.57,103.42L852.57,103.42M856.29,104.58l0.29,1.64l2.96,0.87l0.29-1.16L856.29,104.58L856.29,104.58M547.82,38.79l1.72,0.69l-1.21,2.08v2.95l-2.58,1.56H543l-1.55-1.91l0.17-2.08l1.21-1.56h2.41L547.82,38.79L547.82,38.79M554.36,36.88v2.08l1.72,1.39l2.41-0.17l2.07-1.91v-1.39h-1.89l-1.55,0.52l-1.21-1.39L554.36,36.88L554.36,36.88M564.18,37.06l1.21,2.6l2.41,0.17l1.72-0.69l-0.86-2.43l-2.24-0.52L564.18,37.06L564.18,37.06M573.99,33.59l-1.89-0.35l-1.72,1.74l0.86,1.56l0.52,2.43l2.24-1.73l0.52-1.91L573.99,33.59L573.99,33.59M584.49,51.98l-0.52,2.43l-3.96,3.47l-8.44,1.91l-6.89,11.45l-1.21,3.3l6.89,1.74l1.03-4.16l2.07-6.42l5.34-2.78l4.48-3.47l3.27-1.39h1.72v-4.68L584.49,51.98L584.49,51.98M562.28,77.31l4.65,0.52l1.55,5.38l3.96,4.16l-1.38,2.78h-2.41l-2.24-2.6l-4.99-0.17l-2.07-2.78v-1.91l3.1-0.87L562.28,77.31L562.28,77.31M634.95,18.15l-2.24-1.39h-2.58l-0.52,1.56l-2.75,1.56l-2.07,0.69l-0.34,2.08l4.82,0.35L634.95,18.15L634.95,18.15M640.28,18.67l-1.21,2.6l-2.41-0.17l-3.79,2.78l-1.03,3.47h2.41l1.38-2.26l3.27,2.43l3.1-1.39l2.24-1.91l-0.86-2.95l-1.21-2.08L640.28,18.67L640.28,18.67M645.28,20.58l1.21,4.86l1.89,4.51l2.07-3.64l3.96-0.87v-2.6l-2.58-1.91L645.28,20.58L645.28,20.58M739.76,12.8l2.69,2.26l1.91-0.79l0.56-3.17L741,8.39l-2.58,1.7l-6.28,0.57v2.83l-6.62,0.11v4.63l7.74,5.76l2.02-1.47l-0.45-4.07l4.94-1.24l-1.01-1.92l-1.79-1.81L739.76,12.8L739.76,12.8M746.94,10.09l1.79,3.39l6.96-0.79l1.91-2.49l-0.45-2.15l-1.91-0.79l-1.79,1.36l-5.16,1.13L746.94,10.09L746.94,10.09M746.49,23.31l-3.48-0.9L741,24.56l-0.9,2.94l4.71-0.45l3.59-1.81L746.49,23.31L746.49,23.31M836.68,3.76l-2.92-0.9L830.4,4.1l-1.68,2.49l2.13,2.83l5.61-2.49l1.12-1.24L836.68,3.76L836.68,3.76M817.97,72.93l1.76,6.08l3.52,1.01l3.52-5.57l-2.01-3.8l0.75-3.29h5.28l-1.26,2.53l0.5,9.12l-7.54,18.74l0.75,4.05l-0.25,6.84l14.07,20.51l2.76,0.76l0.25-16.71l2.76-2.53l-3.02-6.58l2.51-2.79l-5.53-7.34l-3.02,0.25l-1-12.15l7.79-2.03l0.5-3.55l4.02-1.01l2.26,2.03l2.76-11.14l4.77-8.1l3.77-2.03l3.27,0.25v-3.8l-5.28-1.01l-7.29-6.08l3.52-4.05l-3.02-6.84l2.51-2.53l3.02,4.05l7.54,2.79l8.29,0.76l1.01-3.54l-4.27-4.3l4.77-6.58l-10.81-3.8l-2.76,5.57l-3.52-4.56l-19.85-6.84l-18.85,3.29l-2.76,1.52v1.52l4.02,2.03l-0.5,4.81l-7.29-3.04l-16.08,6.33l-2.76-5.82h-11.06l-5.03,5.32l-17.84-4.05l-16.33,3.29l-2.01,5.06l2.51,0.76l-0.25,3.8l-15.83,1.77l1.01,5.06l-14.58-2.53l3.52-6.58l-14.83-0.76l1.26,6.84l-4.77,2.28l-4.02-3.8l-16.33,2.79l-6.28,5.82l-0.25,3.54l-4.02,0.25l-0.5-4.05l12.82-11.14v-7.6l-8.29-2.28l-10.81,3.54l-4.52-4.56h-2.01l-2.51,5.06l2.01,2.28l-14.33,7.85l-12.31,9.37l-7.54,10.38v4.3l8.04,3.29l-4.02,3.04l-8.54-3.04l-3.52,3.04l-5.28-6.08l-1.01,2.28l5.78,18.23l1.51,0.51l4.02-2.03l2.01,1.52v3.29l-3.77-1.52l-2.26,1.77l1.51,3.29l-1.26,8.61l-7.79,0.76l-0.5-2.79l4.52-2.79l1.01-7.6l-5.03-6.58l-1.76-11.39l-8.04-1.27l-0.75,4.05l1.51,2.03l-3.27,2.79l1.26,7.6l4.77,2.03l1.01,5.57l-4.78-3.04l-12.31-2.28l-1.51,4.05l-9.8,3.54l-1.51-2.53l-12.82,7.09l-0.25,4.81l-5.03,0.76l1.51-3.54v-3.54l-5.03-1.77l-3.27,1.27l2.76,5.32l2.01,3.54v2.79l-3.77-0.76l-0.75-0.76l-3.77,4.05l2.01,3.54l-8.54-0.25l2.76,3.55l-0.75,1.52h-4.52l-3.27-2.28l-0.75-6.33l-5.28-2.03v-2.53l11.06,2.28l6.03,0.51l2.51-3.8l-2.26-4.05l-16.08-6.33l-5.55,1.38l-1.9,1.63l0.59,3.75l2.36,0.41l-0.55,5.9l7.28,17.1l-5.26,8.34l-0.36,1.88l2.67,1.88l-2.41,1.59l-1.6,0.03l0.3,7.35l2.21,3.13l0.03,3.04l2.83,0.26l4.33,1.65l4.58,6.3l0.05,1.66l-1.49,2.55l3.42-0.19l3.33,0.96l4.5,6.37l11.08,1.01l-0.48,7.58l-3.82,3.27l0.79,1.28l-3.77,4.05l-1,3.8l2.26,3.29l7.29,2.53l3.02-1.77l19.35,7.34l0.75-2.03l-4.02-3.8v-4.81l-2.51-0.76l0.5-4.05l4.02-4.81l-7.21-5.4l0.5-7.51l7.71-5.07l9.05,0.51l1.51,2.79l9.3,0.51l6.79-3.8l-3.52-3.8l0.75-7.09l17.59-8.61l13.53,6.1l4.52-4.05l13.32,12.66l10.05-1.01l3.52,3.54l9.55,1.01l6.28-8.61l8.04,3.55l4.27,0.76l4.27-3.8l-3.77-2.53l3.27-5.06l9.3,3.04l2.01,4.05l4.02,0.25l2.51-1.77l6.79-0.25l0.75,1.77l7.79,0.51l5.28-5.57l10.81,1.27l3.27-1.27l1-6.08l-3.27-7.34l3.27-2.79h10.3l9.8,11.65l12.56,7.09h3.77l0.5-3.04l4.52-2.79l0.5,16.46l-4.02,0.25v4.05l2.26,2.79l-0.42,3.62l1.67,0.69l1.01-2.53l1.51,0.51l1,1.01l4.52-1.01l4.52-13.17l0.5-16.46l-5.78-13.17l-7.29-8.86l-3.52,0.51v2.79l-8.54-3.29l3.27-7.09l2.76-18.74l11.56-3.54l5.53-3.54h6.03L805.86,96l1.51,2.53l5.28-5.57l3.02,0.25l-0.5-3.29l-4.78-1.01l3.27-11.9L817.97,72.93L817.97,72.93z","name":"Russian Federation"},"us":{"path":"M69.17,53.35l3.46,6.47l2.22-0.5v-2.24L69.17,53.35L69.17,53.35M49.66,110.26l-0.17,3.01l2.16-0.5v-1.34L49.66,110.26L49.66,110.26M46.34,111.6l-4.32,2.18l0.67,2.34l1.66-1.34l3.32-1.51L46.34,111.6L46.34,111.6M28.39,114.44l-2.99-0.67l-0.5,1.34l0.33,2.51L28.39,114.44L28.39,114.44M22.07,114.28l-2.83-1.17l-1,1.84l1.83,1.84L22.07,114.28L22.07,114.28M12.27,111.6l-1.33-1.84l-1.33,0.5v2.51l1.5,1L12.27,111.6L12.27,111.6M1.47,99.71l1.66,1.17l-0.5,1.34H1.47V99.71L1.47,99.71M10,248.7l-0.14,2.33l2.04,1.37l1.22-1.09L10,248.7L10,248.7M15.29,252.13l-1.9,1.37l1.63,2.05l1.9-1.64L15.29,252.13L15.29,252.13M19.1,255.41l-1.63,2.19l0.54,1.37l2.31-1.09L19.1,255.41L19.1,255.41M21.81,259.65l-0.95,5.47l0.95,2.05l3.12-0.96l1.63-2.74l-3.4-3.15L21.81,259.65L21.81,259.65M271.05,281.06l-2.64-0.89l-2.12,1.33l1.06,1.24l3.61,0.53L271.05,281.06L271.05,281.06M93.11,44.89l-8.39,1.99l1.73,9.45l9.13,2.49l0.49,1.99L82.5,65.04l-7.65,12.68l2.71,13.43L82,94.13l3.46-3.23l0.99,1.99l-4.2,4.97l-16.29,7.46l-10.37,2.49l-0.25,3.73l23.94-6.96l9.87-2.74l9.13-11.19l10.12-6.71l-5.18,8.7l5.68,0.75l9.63-4.23l1.73,6.96l6.66,1.49l6.91,6.71l0.49,4.97l-0.99,1.24l1.23,4.72h1.73l0.25-7.96h1.97l0.49,19.64l4.94-4.23l-3.46-20.39h-5.18l-5.68-7.21l27.89-47.25l-27.64-21.63l-30.85,5.97l-1.23,9.45l6.66,3.98l-2.47,6.47L93.11,44.89L93.11,44.89M148.76,158.34l-1,4.02l-3.49-2.26h-1.74l-1,4.27l-12.21,27.36l3.24,23.84l3.99,2.01l0.75,6.53h8.22l7.97,6.02l15.69,1.51l1.74,8.03l2.49,1.76l3.49-3.51l2.74,1.25l2.49,11.54l4.23,2.76l3.49-6.53l10.71-7.78l6.97,3.26l5.98,0.5l0.25-3.76l12.45,0.25l2.49,2.76l0.5,6.27l-1.49,3.51l1.74,6.02h3.74l3.74-5.77l-1.49-2.76l-1.49-6.02l2.24-6.78l10.21-8.78l7.72-2.26l-1-7.28l10.71-11.55l10.71-1.76L272.8,199l10.46-6.02v-8.03l-1-0.5l-3.74,1.25l-0.5,4.92l-12.43,0.15l-9.74,6.47l-15.29,5l-2.44-2.99l6.94-10.5l-3.43-3.27l-2.33-4.44l-4.83-3.88l-5.25-0.44l-9.92-6.77L148.76,158.34L148.76,158.34z","name":"United States of America"},"mu":{"path":"M613.01,398.99l-1.52,1.99l0.3,2.15l3.2-2.61L613.01,398.99L613.01,398.99z","name":"Mauritius"},"re":{"path":"M607.38,402.37l-2.28,0.15l-0.15,1.99l1.52,0.31l2.28-1.07L607.38,402.37L607.38,402.37z","name":"Reunion"},"mg":{"path":"M592.3,372.92l-2.13,5.06l-3.65,6.44l-6.39,0.46l-2.74,3.22l0.46,9.82l-3.96,4.6l0.46,7.82l3.35,3.83l3.96-0.46l3.96-2.92l-0.91-4.6l9.13-15.8l-1.83-1.99l1.83-3.83l1.98,0.61l0.61-1.53l-1.83-7.82l-1.07-3.22L592.3,372.92L592.3,372.92z","name":"Madagascar"},"km":{"path":"M577.69,371.23l0.46,1.53l1.98,0.31l0.76-1.99L577.69,371.23L577.69,371.23M580.58,374.3l0.76,1.69h1.22l0.61-2.15L580.58,374.3L580.58,374.3z","name":"Comoros"},"sc":{"path":"M602.35,358.34l-0.61,1.23l1.67,1.38l1.22-1.38L602.35,358.34L602.35,358.34M610.88,349.14l-1.83,1.23l1.37,2.15h1.83L610.88,349.14L610.88,349.14M611.64,354.51l-1.22,1.38l0.91,1.38l1.67,0.31l0.15-2.92L611.64,354.51L611.64,354.51z","name":"Seychelles"},"mv":{"path":"M656.4,320.76l0.3,2.61l1.67,0.61l0.3-2.3L656.4,320.76L656.4,320.76M658.53,326.28l-0.15,3.22l1.22,0.61l1.07-2.15L658.53,326.28L658.53,326.28M658.84,332.57l-1.07,1.07l1.22,1.07l1.52-1.07L658.84,332.57L658.84,332.57z","name":"Maldives"},"pt":{"path":"M372.64,217.02l-1.36,1.37l2.44,1.37l0.27-1.91L372.64,217.02L372.64,217.02M379.97,216.2l-1.63,1.09l1.36,1.09l2.17-0.55L379.97,216.2L379.97,216.2M381.05,220.03l-0.81,2.19l1.08,1.37l1.36-1.09L381.05,220.03L381.05,220.03M387.56,224.4l-0.54,1.37l0.81,0.82l2.17-1.37L387.56,224.4L387.56,224.4M408.18,236.42l-1.08,1.37l1.08,1.37l1.63-0.82L408.18,236.42L408.18,236.42M430.93,211.24l-0.62,8.65l-1.77,1.6l0.18,0.98l1.24,2.05l-0.8,2.5l1.33,0.45l3.1-0.36l-0.18-2.5l2.03-11.59l-0.44-1.6L430.93,211.24L430.93,211.24z","name":"Portugal"},"es":{"path":"M415.62,253.73l-1.75,1.01l0.81,0.82L415.62,253.73L415.62,253.73M409.54,253.92l-2.17,0.55l1.08,1.64h1.63L409.54,253.92L409.54,253.92M404.38,252.28l-1.36,1.37l1.9,1.64l1.08-2.46L404.38,252.28L404.38,252.28M448.36,205h-12.74l-2.57-1.16l-1.24,0.09l-1.5,3.12l0.53,3.21l4.87,0.45l0.62,2.05l-2.12,11.95l0.09,2.14l3.45,1.87l3.98,0.27l7.96-1.96l3.89-4.9l0.09-4.99l6.9-6.24l0.35-2.76l-6.28-0.09L448.36,205L448.36,205M461.1,217.21l-1.59,0.54l0.35,1.43h2.3l0.97-1.07L461.1,217.21L461.1,217.21z","name":"Spain"},"cv":{"path":"M387.56,290.54l-1.9,1.09l1.36,1.09l1.63-0.82L387.56,290.54L387.56,290.54M392.23,292.74l-1.24,1.1l0.88,1.63l2.12-0.95L392.23,292.74L392.23,292.74M389.52,295.83l-1.59,0.95l1.71,2.29l1.35-0.71L389.52,295.83L389.52,295.83z","name":"Cape Verde"},"pf":{"path":"M27.25,402.68l-1.9-0.14l-0.14,1.78l1.49,0.96l1.77-1.09L27.25,402.68L27.25,402.68M33.77,404.6l-2.72,1.78l2.04,2.46l1.77-0.41l0.95-1.23L33.77,404.6L33.77,404.6z","name":"French Polynesia"},"kn":{"path":"M276.6,283.37l-1.5,0.62l0.53,1.33l1.76-1.15l-0.35-0.36L276.6,283.37L276.6,283.37z","name":"Saint Kitts and Nevis"},"ag":{"path":"M279.07,284.88l-0.88,1.87l1.06,1.42l1.32-1.15L279.07,284.88L279.07,284.88z","name":"Antigua and Barbuda"},"dm":{"path":"M282.07,290.03l-1.06,0.98l0.79,1.6l1.5-0.44L282.07,290.03L282.07,290.03z","name":"Dominica"},"lc":{"path":"M281.98,294.03l-0.71,1.51l1.15,1.24l1.5-0.8L281.98,294.03L281.98,294.03z","name":"Saint Lucia"},"bb":{"path":"M282.07,297.85l-1.23,0.89l0.97,1.78l1.59-0.89L282.07,297.85L282.07,297.85z","name":"Barbados"},"gd":{"path":"M280.57,301.31l-1.15,1.15l0.44,0.71h1.41l0.44-1.16L280.57,301.31L280.57,301.31z","name":"Grenada"},"tt":{"path":"M282.24,304.78l-1.06,0.98l-1.15,0.18v1.42l2.12,1.95l0.88-1.42l0.53-1.6l-0.18-1.33L282.24,304.78L282.24,304.78z","name":"Trinidad and Tobago"},"do":{"path":"M263.11,280.44l-5.29-3.46l-2.5-0.85l-0.84,6l0.88,1.69l1.15-1.33l3.35-0.89l2.91,0.62L263.11,280.44L263.11,280.44z","name":"Dominican Republic"},"ht":{"path":"M250.86,275.38l3.44,0.36l-0.41,4.22l-0.34,2.22l-4.01-0.22l-0.71,1.07l-1.23-0.09l-0.44-2.31l4.23-0.35l-0.26-2.4l-1.94-0.8L250.86,275.38L250.86,275.38z","name":"Haiti"},"fk":{"path":"M307.95,508.18l-2.63-0.29l-2.62,1.76l1.9,2.06L307.95,508.18L307.95,508.18M310.57,506.86l-0.87,2.79l-2.48,2.2l0.15,0.73l4.23-1.62l1.75-2.2L310.57,506.86L310.57,506.86z","name":"Falkland Islands"},"is":{"path":"M406.36,117.31l-1.96-1.11l-2.64,1.67l-2.27,2.1l0.06,1.17l2.94,0.37l-0.18,2.1l-1.04,1.05l0.25,0.68l2.94,0.19v3.4l4.23,0.74l2.51,1.42l2.82,0.12l4.84-2.41l3.74-4.94l0.06-3.34l-2.27-1.92l-1.9-1.61l-0.86,0.62l-1.29,1.67l-1.47-0.19l-1.47-1.61l-1.9,0.18l-2.76,2.29l-1.66,1.79l-0.92-0.8l-0.06-1.98l0.92-0.62L406.36,117.31L406.36,117.31z","name":"Iceland"},"no":{"path":"M488.26,53.96l-1.65-1.66l-3.66,1.78h-6.72L475.17,58l3.77,3.33l1.65-0.24l2.36-4.04l2,1.43l-1.42,2.85l-0.71,4.16l1.65,2.61l3.54-5.94l4.6-5.59l-1.77-1.54L488.26,53.96L488.26,53.96M490.26,46.83l-2.95,2.73l1.77,2.73h3.18l1.3,1.78l3.89,2.02l4.48-2.61l3.07-2.61l-1.06-2.14l-3.07-1.78l-2.24,2.02l-1.53-1.9l-1.18,0.12l-1.53,3.33l-2.24-2.26l-0.24-1.54L490.26,46.83L490.26,46.83M496.98,59.07l-2.36,2.14l-2,1.54l0.94,1.66l1.89,0.59l3.07-1.43l1.42-1.78l-1.3-2.14L496.98,59.07L496.98,59.07M515.46,102.14l2.02-1.48L517.3,99l-1.28-0.74l0.18-2.03h1.1v-1.11l-4.77-1.29l-7.15,0.74l-0.73,3.14L503,97.16l-1.1-1.85l-3.49,0.18L498.04,99l-1.65,0.74l-0.92-1.85l-7.34,5.91l1.47,1.66l-2.75,1.29l-6.24,12.38l-2.2,1.48l0.18,1.11l2.2,1.11l-0.55,2.4l-3.67-0.19l-1.1-1.29l-2.38,2.77l-1.47,1.11l-0.37,2.59l-1.28,0.74l-3.3,0.74l-1.65,5.18l1.1,8.5l1.28,3.88l1.47,1.48l3.3-0.18l4.77-4.62l1.83-3.14l0.55,4.62l3.12-5.54l0.18-15.53l2.54-1.6l0.76-8.57l7.7-11.09l3.67-1.29l1.65-2.03l5.5,1.29l2.75,1.66l0.92-4.62l4.59-2.77L515.46,102.14L515.46,102.14z","name":"Norway"},"lk":{"path":"M680.54,308.05l0.25,2.72l0.25,1.98l-1.47,0.25l0.74,4.45l2.21,1.24l3.43-1.98l-0.98-4.69l0.25-1.73l-3.19-2.96L680.54,308.05L680.54,308.05z","name":"Sri Lanka"},"cu":{"path":"M220.85,266.92v1.27l5.32,0.1l2.51-1.46l0.39,1.07l5.22,1.27l4.64,4.19l-1.06,1.46l0.19,1.66l3.87,0.97l3.87-1.75l1.74-1.75l-2.51-1.27l-12.95-7.6l-4.54-0.49L220.85,266.92L220.85,266.92z","name":"Cuba"},"bs":{"path":"M239.61,259.13l-1.26-0.39l-0.1,2.43l1.55,1.56l1.06-1.56L239.61,259.13L239.61,259.13M242.12,262.93l-1.74,0.97l1.64,2.34l0.87-1.17L242.12,262.93L242.12,262.93M247.73,264.68l-1.84-0.1l0.19,1.17l1.35,1.95l1.16-1.27L247.73,264.68L247.73,264.68M246.86,262.35l-3-1.27l-0.58-3.02l1.16-0.49l1.16,2.34l1.16,0.88L246.86,262.35L246.86,262.35M243.96,256.21l-1.55-0.39l-0.29-1.95l-1.64-0.58l1.06-1.07l1.93,0.68l1.45,0.88L243.96,256.21L243.96,256.21z","name":"Bahamas"},"jm":{"path":"M238.93,279.59l-3.48,0.88v0.97l2.03,1.17h2.13l1.35-1.56L238.93,279.59L238.93,279.59z","name":"Jamaica"},"ec":{"path":"M230.2,335.85l-4.73,2.94l-0.34,4.36l-0.95,1.43l2.98,2.86l-1.29,1.41l0.3,3.6l5.33,1.27l8.07-9.55l-0.02-3.33l-3.87-0.25L230.2,335.85L230.2,335.85z","name":"Ecuador"},"ca":{"path":"M203.73,35.89l0.22,4.02l-7.98,8.27l2,6.7l5.76-1.56l3.33-4.92l8.42-3.13l6.87-0.45l-5.32-5.81l-2.66,2.01l-2-0.67l-1.11-2.46l-2.44-2.46L203.73,35.89L203.73,35.89M214.15,24.05l-1.77,3.13l8.65,3.13l3.1-4.69l1.33,3.13h2.22l4.21-4.69l-5.1-1.34l-2-1.56l-2.66,2.68L214.15,24.05L214.15,24.05M229.23,30.31l-6.87,2.9v2.23l8.87,3.35l-2,2.23l1.33,2.9l5.54-2.46h4.66l2.22,3.57l3.77-3.8l-0.89-3.58l-3.1,1.12l-0.44-4.47l1.55-2.68h-1.55l-2.44,1.56l-1.11,0.89l0.67,3.13l-1.77,1.34l-2.66-0.22l-0.67-4.02L229.23,30.31L229.23,30.31M238.32,23.38l-0.67,2.23l4.21,2.01l3.1-1.79l-0.22-1.34L238.32,23.38L238.32,23.38M241.64,19.58l-3.1,1.12l0.22,1.56l6.87-0.45l-0.22-1.56L241.64,19.58L241.64,19.58M256.5,23.38l-0.44,1.56l-1.11,1.56v2.23l4.21-0.67l4.43,3.8h1.55v-3.8l-4.43-4.92L256.5,23.38L256.5,23.38M267.81,27.85l1.77,2.01l-1.55,2.68l1.11,2.9l4.88-2.68v-2.01l-2.88-3.35L267.81,27.85L267.81,27.85M274.24,22.71l0.22,3.57h5.99l1.55,1.34l-0.22,1.56l-5.32,0.67l3.77,5.14l5.1,0.89l7.09-3.13l-10.2-15.42l-3.1,2.01l0.22,2.68l-3.55-1.34L274.24,22.71L274.24,22.71M222.58,47.96l-8.42,2.23l-4.88,4.25l0.44,4.69l8.87,2.68l-2,4.47l-6.43-4.02l-1.77,3.35l4.21,2.9l-0.22,4.69l6.43,1.79l7.76-0.45l1.33-2.46l5.76,6.48l3.99-1.34l0.67-4.47l2.88,2.01l0.44-4.47l-3.55-2.23l0.22-14.07l-3.1-2.46L231.89,56L222.58,47.96L222.58,47.96M249.63,57.79l-2.88-1.34l-1.55,2.01l3.1,4.92l0.22,4.69l6.65-4.02v-5.81l2.44-2.46l-2.44-1.79h-3.99L249.63,57.79L249.63,57.79M263.82,55.78l-4.66,3.8l1.11,4.69h2.88l1.33-2.46l2,2.01l2-0.22l5.32-4.47L263.82,55.78L263.82,55.78M263.37,48.4l-1.11,2.23l4.88,1.79l1.33-2.01L263.37,48.4L263.37,48.4M260.49,39.91l-4.88,0.67l-2.88,2.68l5.32,0.22l-1.55,4.02l1.11,1.79l1.55-0.22l3.77-6.03L260.49,39.91L260.49,39.91M268.92,38.35l-2.66,0.89l0.44,3.57l4.43,2.9l0.22,2.23l-1.33,1.34l0.67,4.47l17.07,5.58l4.66,1.56l4.66-4.02l-5.54-4.47l-5.1,1.34l-7.09-0.67l-2.66-2.68l-0.67-7.37l-4.43-2.23L268.92,38.35L268.92,38.35M282.88,61.59L278,61.14l-5.76,2.23l-3.1,4.24l0.89,11.62l9.53,0.45l9.09,4.47l6.43,7.37l4.88-0.22l-1.33,6.92l-4.43,7.37l-4.88,2.23l-3.55-0.67l-1.77-1.56l-2.66,3.57l1.11,3.57l3.77,0.22l4.66-2.23l3.99,10.28l9.98,6.48l6.87-8.71l-5.76-9.38l3.33-3.8l4.66,7.82l8.42-7.37l-1.55-3.35l-5.76,1.79l-3.99-10.95l3.77-6.25l-7.54-8.04l-4.21,2.9l-3.99-8.71l-8.42,1.12l-2.22-10.5l-6.87,4.69l-0.67,5.81h-3.77l0.44-5.14L282.88,61.59L282.88,61.59M292.86,65.61l-1.77,1.79l1.55,2.46l7.32,0.89l-4.66-4.92L292.86,65.61L292.86,65.61M285.77,40.36v2.01l-4.88,1.12l1.33,2.23l5.54,2.23l6.21,0.67l4.43,3.13l4.43-2.46l-3.1-3.13h3.99l2.44-2.68l5.99-0.89v-1.34l-3.33-2.23l0.44-2.46l9.31,1.56l13.75-5.36l-5.1-1.56l1.33-1.79h10.64l1.77-1.79l-21.51-7.6l-5.1-1.79l-5.54,4.02l-6.21-5.14l-3.33-0.22l-0.67,4.25l-4.21-3.8l-4.88,1.56l0.89,2.46l7.32,1.56l-0.44,3.57l3.99,2.46l9.76-2.46l0.22,3.35l-7.98,3.8l-4.88-3.8l-4.43,0.45l4.43,6.26l-2.22,1.12l-3.33-2.9l-2.44,1.56l2.22,4.24h3.77l-0.89,4.02l-3.1-0.45l-3.99-4.25L285.77,40.36L285.77,40.36M266.01,101.85l-4.23,5.32l-0.26,5.86l3.7-2.13h4.49l3.17,2.93l2.91-2.4L266.01,101.85L266.01,101.85M317.52,171.05l-10.57,10.12l1.06,2.4l12.94,4.79l1.85-3.19l-1.06-5.32l-4.23,0.53l-2.38-2.66l3.96-3.99L317.52,171.05L317.52,171.05M158.22,48.66l1.99,3.01l1,4.02l4.98,1.25l3.49-3.76l2.99,1.51l8.47,0.75l5.98-2.51l1,8.28h3.49V57.7l3.49,0.25l8.72,10.29l5.73,3.51l-2.99,4.77l1.25,1.25L219,80.03l0.25,5.02l2.99,0.5l0.75-7.53l4.73-1.25l3.49,5.27l7.47,3.51l3.74,0.75l2.49-3.01l0.25-4.77l4.48-2.76l1.49,4.02l-3.99,7.03l0.5,3.51l2.24-3.51l4.48-4.02l0.25-5.27l-2.49-4.02l0.75-3.26l5.98-3.01l2.74,2.01l0.5,17.57l4.23-3.76l2.49,1.51l-3.49,6.02l4.48,1l6.48-10.04l5.48,5.77l-2.24,10.29l-5.48,3.01l-5.23-2.51l-9.46,2.01l1,3.26l-2.49,4.02l-7.72,1.76l-8.72,6.78l-7.72,10.29l-1,3.26l5.23,2.01l1.99,5.02l7.22,7.28l11.46,5.02l-2.49,11.54l-0.25,3.26l2.99,2.01l3.99-5.27l0.5-10.04l6.23-0.25l2.99-5.77l0.5-8.78l7.97-15.56l9.96,3.51l5.23,7.28l-2.24,7.28l3.99,2.26l9.71-6.53l2.74,17.82l8.97,10.79l0.25,5.52l-9.96,2.51l-4.73,5.02l-9.96-2.26l-4.98-0.25l-8.72,6.78l5.23-1.25l6.48-1.25l1.25,1.51l-1.74,5.52l0.25,5.02l2.99,2.01l2.99-0.75l1.5-2.26h1.99l-3.24,6.02l-6.23,0.25l-2.74,4.02h-3.49l-1-3.01l4.98-5.02l-5.98,2.01l-0.27-8.53l-1.72-1l-5.23,2.26l-0.5,4.27h-11.96l-10.21,7.03l-13.7,4.52l-1.49-2.01l6.9-10.3l-3.92-3.77l-2.49-4.78l-5.07-3.87l-5.44-0.45l-9.75-6.83l-70.71-11.62l-1.17-4.79l-6.48-6.02v-5.02l1-4.52l-0.5-2.51l-2.49-2.51l-0.5-4.02l6.48-4.52l-3.99-21.58l-5.48-0.25l-4.98-6.53L158.22,48.66L158.22,48.66M133.83,128.41l-1.7,3.26l0.59,2.31l1.11,0.69l-0.26,0.94l-1.19,0.34l0.34,3.43l1.28,1.29l1.02-1.11l-1.28-3.34l0.76-2.66l1.87-2.49l-1.36-2.31L133.83,128.41L133.83,128.41M139.45,147.95l-1.53,0.6l2.81,3.26l0.68,3.86l2.81,3l2.38-0.43v-3.94l-2.89-1.8L139.45,147.95L139.45,147.95z","name":"Canada"},"gt":{"path":"M194.88,291.52l5.93,4.34l5.98-7.43l-1.02-1.54l-2.04-0.07v-4.35l-1.53-0.93l-4.63,1.38l1.77,4.08L194.88,291.52L194.88,291.52z","name":"Guatemala"},"hn":{"path":"M207.55,288.78l9.24-0.35l2.74,3.26l-1.71-0.39l-3.29,0.14l-4.3,4.04l-1.84,4.09l-1.21-0.64l-0.01-4.48l-2.66-1.78L207.55,288.78L207.55,288.78z","name":"Honduras"},"sv":{"path":"M201.65,296.27l4.7,2.34l-0.07-3.71l-2.41-1.47L201.65,296.27L201.65,296.27z","name":"El Salvador"},"ni":{"path":"M217.74,292.11l2.19,0.44l0.07,4.49l-2.55,7.28l-6.87-0.68l-1.53-3.51l2.04-4.26l3.87-3.6L217.74,292.11L217.74,292.11z","name":"Nicaragua"},"cr":{"path":"M217.38,304.98l1.39,2.72l1.13,1.5l-1.52,4.51l-2.9-2.04l-4.74-4.34v-2.87L217.38,304.98L217.38,304.98z","name":"Costa Rica"},"pa":{"path":"M220.59,309.61l-1.46,4.56l4.82,1.25l2.99,0.59l0.51-3.53l3.21-1.62l2.85,1.47l1.12,1.79l1.36-0.16l1.07-3.25l-3.56-1.47l-2.7-1.47l-2.7,1.84l-3.21,1.62l-3.28-1.32L220.59,309.61L220.59,309.61z","name":"Panama"},"co":{"path":"M253.73,299.78l-2.06-0.21l-13.62,11.23l-1.44,3.95l-1.86,0.21l0.83,8.73l-4.75,11.65l5.16,4.37l6.61,0.42l4.54,6.66l6.6,0.21l-0.21,4.99H256l2.68-9.15l-2.48-3.12l0.62-5.82l5.16-0.42l-0.62-13.52l-11.56-3.74l-2.68-7.28L253.73,299.78L253.73,299.78z","name":"Colombia"},"ve":{"path":"M250.46,305.92l0.44,2.59l3.25,1.03l0.74-4.77l3.43-3.55l3.43,4.02l7.89,2.15l6.68-1.4l4.55,5.61l3.43,2.15l-3.76,5.73l1.26,4.34l-2.15,2.66l-2.23,1.87l-4.83-2.43l-1.11,1.12v3.46l3.53,1.68l-2.6,2.81l-2.6,2.81l-3.43-0.28l-3.45-3.79l-0.73-14.26l-11.78-4.02l-2.14-6.27L250.46,305.92L250.46,305.92z","name":"Venezuela"},"gy":{"path":"M285.05,314.13l7.22,6.54l-2.87,3.32l-0.23,1.97l3.77,3.89l-0.09,3.74l-6.56,2.5l-3.93-5.31l0.84-6.38l-1.68-4.75L285.05,314.13L285.05,314.13z","name":"Guyana"},"sr":{"path":"M293.13,321.14l2.04,1.87l3.16-1.96l2.88,0.09l-0.37,1.12l-1.21,2.52l-0.19,6.27l-5.75,2.34l0.28-4.02l-3.71-3.46l0.19-1.78L293.13,321.14L293.13,321.14z","name":"Suriname"},"gf":{"path":"M302.13,321.8l5.85,3.65l-3.06,6.08l-1.11,1.4l-3.25-1.87l0.09-6.55L302.13,321.8L302.13,321.8z","name":"French Guiana"},"pe":{"path":"M225.03,349.52l-1.94,1.96l0.13,3.13l16.94,30.88l17.59,11.34l2.72-4.56l0.65-10.03l-1.42-6.25l-4.79-8.08l-2.85,0.91l-1.29,1.43l-5.69-6.52l1.42-7.69l6.6-4.3l-0.52-4.04l-6.72-0.26l-3.49-5.86l-1.94-0.65l0.13,3.52l-8.66,10.29l-6.47-1.56L225.03,349.52L225.03,349.52z","name":"Peru"},"bo":{"path":"M258.71,372.79l8.23-3.59l2.72,0.26l1.81,7.56l12.54,4.17l2.07,6.39l5.17,0.65l2.2,5.47l-1.55,4.95l-8.41,0.65l-3.1,7.95l-6.6-0.13l-2.07-0.39l-3.81,3.7l-1.88-0.18l-6.47-14.99l1.79-2.68l0.63-10.6l-1.6-6.31L258.71,372.79L258.71,372.79z","name":"Bolivia"},"py":{"path":"M291.76,399.51l2.2,2.4l-0.26,5.08l6.34-0.39l4.79,6.13l-0.39,5.47l-3.1,4.69l-6.34,0.26l-0.26-2.61l1.81-4.3l-6.21-3.91h-5.17l-3.88-4.17l2.82-8.06L291.76,399.51L291.76,399.51z","name":"Paraguay"},"uy":{"path":"M300.36,431.93l-2.05,2.19l0.85,11.78l6.44,1.87l8.19-8.21L300.36,431.93L300.36,431.93z","name":"Uruguay"},"ar":{"path":"M305.47,418.2l1.94,1.82l-7.37,10.95l-2.59,2.87l0.9,12.51l5.69,6.91l-4.78,8.34l-3.62,1.56h-4.14l1.16,6.51l-6.47,2.22l1.55,5.47l-3.88,12.38l4.79,3.91l-2.59,6.38l-4.4,6.91l2.33,4.82l-5.69,0.91l-4.66-5.73l-0.78-17.85l-7.24-30.32l2.19-10.6l-4.66-13.55l3.1-17.59l2.85-3.39l-0.7-2.57l3.66-3.34l8.16,0.56l4.56,4.87l5.27,0.09l5.4,3.3l-1.59,3.72l0.38,3.76l7.65-0.36L305.47,418.2L305.47,418.2M288.92,518.79l0.26,5.73l4.4-0.39l3.75-2.48l-6.34-1.3L288.92,518.79L288.92,518.79z","name":"Argentina"},"cl":{"path":"M285.04,514.1l-4.27,9.38l7.37,0.78l0.13-6.25L285.04,514.1L285.04,514.1M283.59,512.63l-3.21,3.55l-0.39,4.17l-6.21-3.52l-6.6-9.51l-1.94-3.39l2.72-3.52l-0.26-4.43l-3.1-1.3l-2.46-1.82l0.52-2.48l3.23-0.91l0.65-14.33l-5.04-2.87l-3.29-74.59l0.85-1.48l6.44,14.85l2.06,0.04l0.67,2.37l-2.74,3.32l-3.15,17.87l4.48,13.76l-2.07,10.42l7.3,30.64l0.77,17.92l5.23,6.05L283.59,512.63L283.59,512.63M262.28,475.14l-1.29,1.95l0.65,3.39l1.29,0.13l0.65-4.3L262.28,475.14L262.28,475.14z","name":"Chile"},"br":{"path":"M314.24,438.85l6.25-12.02l0.23-10.1l11.66-7.52h6.53l5.13-8.69l0.93-16.68l-2.1-4.46l12.36-11.28l0.47-12.45l-16.79-8.22l-20.28-6.34l-9.56-0.94l2.57-5.4l-0.7-8.22l-2.09-0.69l-3.09,6.14l-1.62,2.03l-4.16-1.84l-13.99,4.93l-4.66-5.87l0.75-6.13l-4.4,4.48l-4.86-2.62l-0.49,0.69l0.01,2.13l4.19,2.25l-6.29,6.63l-3.97-0.04l-4.02-4.09l-4.55,0.14l-0.56,4.86l2.61,3.17l-3.08,9.87l-3.6,0.28l-5.73,3.62l-1.4,7.11l4.97,5.32l0.91-1.03l3.49-0.94l2.98,5.02l8.53-3.66l3.31,0.19l2.28,8.07l12.17,3.86l2.1,6.44l5.18,0.62l2.47,6.15l-1.67,5.47l2.18,2.86l-0.32,4.26l5.84-0.55l5.35,6.76l-0.42,4.75l3.17,2.68l-7.6,11.51L314.24,438.85L314.24,438.85z","name":"Brazil"},"bz":{"path":"M204.56,282.4l-0.05,3.65h0.84l2.86-5.34h-1.94L204.56,282.4L204.56,282.4z","name":"Belize"},"mn":{"path":"M673.8,170.17l5.82-7.72l6.99,3.23l4.75,1.27l5.82-5.34l-3.95-2.91l2.6-3.67l7.76,2.74l2.69,4.41l4.86,0.13l2.54-1.89l5.23-0.21l1.14,1.94l8.69,0.44l5.5-5.61l7.61,0.8l-0.44,7.64l3.33,0.76l4.09-1.86l4.33,2.14l-0.1,1.08l-3.14,0.09l-3.27,6.86l-2.54,0.25l-9.88,12.91l-10.09,4.45l-6.31,0.49l-5.24-3.38l-6.7,3.58l-6.6-2.05l-1.87-4.79l-12.5-0.88l-6.4-10.85l-3.11-0.2L673.8,170.17L673.8,170.17z","name":"Mongolia"},"kp":{"path":"M778.28,194.27l1.84,0.77l0.56,6.44l3.65,0.21l3.44-4.03l-1.19-1.06l0.14-4.32l3.16-3.82l-1.61-2.9l1.05-1.2l0.58-3l-1.83-0.83l-1.56,0.79l-1.93,5.86l-3.12-0.27l-3.61,4.26L778.28,194.27L778.28,194.27z","name":"North Korea"},"kr":{"path":"M788.34,198.2l6.18,5.04l1.05,4.88l-0.21,2.62l-3.02,3.4l-2.6,0.14l-2.95-6.37l-1.12-3.04l1.19-0.92l-0.28-1.27l-1.47-0.66L788.34,198.2L788.34,198.2z","name":"South Korea"},"kz":{"path":"M576.69,188.62l4.1-1.75l4.58-0.16l0.32,7h-2.68l-2.05,3.34l2.68,4.45l3.95,2.23l0.36,2.55l1.45-0.48l1.34-1.59l2.21,0.48l1.11,2.23h2.84v-2.86l-1.74-5.09l-0.79-4.13l5.05-2.23l6.79,1.11l4.26,4.29l9.63-0.95l5.37,7.63l6.31,0.32l1.74-2.86l2.21-0.48l0.32-3.18l3.31-0.16l1.74,2.07l1.74-4.13l14.99,2.07l2.52-3.34l-4.26-5.25l5.68-12.4l4.58,0.32l3.16-7.63l-6.31-0.64l-3.63-3.5l-10,1.16l-12.88-12.45l-4.54,4.03l-13.77-6.25l-16.89,8.27l-0.47,5.88l3.95,4.61l-7.7,4.35l-9.99-0.22l-2.09-3.07l-7.83-0.43l-7.42,4.77l-0.16,6.52L576.69,188.62L576.69,188.62z","name":"Kazakhstan"},"tm":{"path":"M593.85,207.59l-0.62,2.63h-4.15v3.56l4.46,2.94l-1.38,4.03v1.86l1.85,0.31l2.46-3.25l5.54-1.24l11.84,4.49l0.15,3.25l6.61,0.62l7.38-7.75l-0.92-2.48l-4.92-1.08l-13.84-8.99l-0.62-3.25h-5.23l-2.31,4.34h-2.31L593.85,207.59L593.85,207.59z","name":"Turkmenistan"},"uz":{"path":"M628.92,219.06l3.08,0.16v-5.27l-2.92-1.7l4.92-6.2h2l2,2.33l5.23-2.01l-7.23-2.48l-0.28-1.5l-1.72,0.42l-1.69,2.94l-7.29-0.24l-5.35-7.57l-9.4,0.93l-4.48-4.44l-6.2-1.05l-4.5,1.83l2.61,8.68l0.03,2.92l1.9,0.04l2.33-4.44l6.2,0.08l0.92,3.41l13.29,8.82l5.14,1.18L628.92,219.06L628.92,219.06z","name":"Uzbekistan"},"tj":{"path":"M630.19,211.84l4.11-5.1h1.55l0.54,1.14l-1.9,1.38v1.14l1.25,0.9l6.01,0.36l1.96-0.84l0.89,0.18l0.6,1.92l3.57,0.36l1.79,3.78l-0.54,1.14l-0.71,0.06l-0.71-1.44l-1.55-0.12l-2.68,0.36l-0.18,2.52l-2.68-0.18l0.12-3.18l-1.96-1.92l-2.98,2.46l0.06,1.62l-2.62,0.9h-1.55l0.12-5.58L630.19,211.84L630.19,211.84z","name":"Tajikistan"},"kg":{"path":"M636.81,199.21l-0.31,2.53l0.25,1.56l8.7,2.92l-7.64,3.08l-0.87-0.72l-1.65,1.06l0.08,0.58l0.88,0.4l5.36,0.14l2.72-0.82l3.49-4.4l4.37,0.76l5.27-7.3l-14.1-1.92l-1.95,4.73l-2.46-2.64L636.81,199.21L636.81,199.21z","name":"Kyrgyz Republic"},"af":{"path":"M614.12,227.05l1.59,12.46l3.96,0.87l0.37,2.24l-2.84,2.37l5.29,4.27l10.28-3.7l0.82-4.38l6.47-4.04l2.48-9.36l1.85-1.99l-1.92-3.34l6.26-3.87l-0.8-1.12l-2.89,0.18l-0.26,2.66l-3.88-0.04l-0.07-3.55l-1.25-1.49l-2.1,1.91l0.06,1.75l-3.17,1.2l-5.85-0.37l-7.6,7.96L614.12,227.05L614.12,227.05z","name":"Afghanistan"},"pk":{"path":"M623.13,249.84l2.6,3.86l-0.25,1.99l-3.46,1.37l-0.25,3.24h3.96l1.36-1.12h7.54l6.8,5.98l0.87-2.87h5.07l0.12-3.61l-5.19-4.98l1.11-2.74l5.32-0.37l7.17-14.95l-3.96-3.11l-1.48-5.23l9.64-0.87l-5.69-8.1l-3.03-0.82l-1.24,1.5l-0.93,0.07l-5.69,3.61l1.86,3.12l-2.1,2.24l-2.6,9.59l-6.43,4.11l-0.87,4.49L623.13,249.84L623.13,249.84z","name":"Pakistan"},"in":{"path":"M670.98,313.01l4.58-2.24l2.72-9.84l-0.12-12.08l15.58-16.82v-3.99l3.21-1.25l-0.12-4.61l-3.46-6.73l1.98-3.61l4.33,3.99l5.56,0.25v2.24l-1.73,1.87l0.37,1l2.97,0.12l0.62,3.36h0.87l2.23-3.99l1.11-10.46l3.71-2.62l0.12-3.61l-1.48-2.87l-2.35-0.12l-9.2,6.08l0.58,3.91l-6.46-0.02l-2.28-2.79l-1.24,0.16l0.42,3.88l-13.97-1l-8.66-3.86l-0.46-4.75l-5.77-3.58l-0.07-7.37l-3.96-4.53l-9.1,0.87l0.99,3.96l4.46,3.61l-7.71,15.78l-5.16,0.39l-0.85,1.9l5.08,4.7l-0.25,4.75l-5.19-0.08l-0.56,2.36l4.31-0.19l0.12,1.87l-3.09,1.62l1.98,3.74l3.83,1.25l2.35-1.74l1.11-3.11l1.36-0.62l1.61,1.62l-0.49,3.99l-1.11,1.87l0.25,3.24L670.98,313.01L670.98,313.01z","name":"India"},"np":{"path":"M671.19,242.56l0.46,4.27l8.08,3.66l12.95,0.96l-0.49-3.13l-8.65-2.38l-7.34-4.37L671.19,242.56L671.19,242.56z","name":"Nepal"},"bt":{"path":"M695.4,248.08l1.55,2.12l5.24,0.04l-0.53-2.9L695.4,248.08L695.4,248.08z","name":"Bhutan"},"bd":{"path":"M695.57,253.11l-1.31,2.37l3.4,6.46l0.1,5.04l0.62,1.35l3.99,0.07l2.26-2.17l1.64,0.99l0.33,3.07l1.31-0.82l0.08-3.92l-1.1-0.13l-0.69-3.33l-2.78-0.1l-0.69-1.85l1.7-2.27l0.03-1.12h-4.94L695.57,253.11L695.57,253.11z","name":"Bangladesh"},"mm":{"path":"M729.44,303.65l-2.77-4.44l2.01-2.82l-1.9-3.49l-1.79-0.34l-0.34-5.86l-2.68-5.19l-0.78,1.24l-1.79,3.04l-2.24,0.34l-1.12-1.47l-0.56-3.95l-1.68-3.16l-6.84-6.45l1.68-1.11l0.31-4.67l2.5-4.2l1.08-10.45l3.62-2.47l0.12-3.81l2.17,0.72l3.42,4.95l-2.54,5.44l1.71,4.27l4.23,1.66l0.77,4.65l5.68,0.88l-1.57,2.71l-7.16,2.82l-0.78,4.62l5.26,6.76l0.22,3.61l-1.23,1.24l0.11,1.13l3.92,5.75l0.11,5.97L729.44,303.65L729.44,303.65z","name":"Myanmar"},"th":{"path":"M730.03,270.47l3.24,4.17v5.07l1.12,0.56l5.15-2.48l1.01,0.34l6.15,7.1l-0.22,4.85l-2.01-0.34l-1.79-1.13l-1.34,0.11l-2.35,3.94l0.45,2.14l1.9,1.01l-0.11,2.37l-1.34,0.68l-4.59-3.16v-2.82l-1.9-0.11l-0.78,1.24l-0.4,12.62l2.97,5.42l5.26,5.07l-0.22,1.47l-2.8-0.11l-2.57-3.83h-2.69l-3.36-2.71l-1.01-2.82l1.45-2.37l0.5-2.14l1.58-2.8l-0.07-6.44l-3.86-5.58l-0.16-0.68l1.25-1.26l-0.29-4.43l-5.14-6.51l0.6-3.75L730.03,270.47L730.03,270.47z","name":"Thailand"},"kh":{"path":"M740.48,299.47l4.09,4.37l7.61-5.64l0.67-8.9l-3.93,2.71l-2.04-1.14l-2.77-0.37l-1.55-1.09l-0.75,0.04l-2.03,3.33l0.33,1.54l2.06,1.15l-0.25,3.13L740.48,299.47L740.48,299.47z","name":"Cambodia"},"la":{"path":"M735.47,262.93l-2.42,1.23l-2.01,5.86l3.36,4.28l-0.56,4.73l0.56,0.23l5.59-2.71l7.5,8.38l-0.18,5.28l1.63,0.88l4.03-3.27l-0.33-2.59l-11.63-11.05l0.11-1.69l1.45-1.01l-1.01-2.82l-4.81-0.79L735.47,262.93L735.47,262.93z","name":"Lao People's Democratic Republic"},"vn":{"path":"M745.06,304.45l1.19,1.87l0.22,2.14l3.13,0.34l3.8-5.07l3.58-1.01l1.9-5.18l-0.89-8.34l-3.69-5.07l-3.89-3.11l-4.95-8.5l3.55-5.94l-5.08-5.83l-4.07-0.18l-3.66,1.97l1.09,4.71l4.88,0.86l1.31,3.63l-1.72,1.12l0.11,0.9l11.45,11.2l0.45,3.29l-0.69,10.4L745.06,304.45L745.06,304.45z","name":"Vietnam"},"ge":{"path":"M555.46,204.16l3.27,4.27l4.08,1.88l2.51-0.01l4.31-1.17l1.08-1.69l-12.75-4.77L555.46,204.16L555.46,204.16z","name":"Georgia"},"am":{"path":"M569.72,209.89l4.8,6.26l-1.41,1.65l-3.4-0.59l-4.22-3.78l0.23-2.48L569.72,209.89L569.72,209.89z","name":"Armenia"},"az":{"path":"M571.41,207.72l-1.01,1.72l4.71,6.18l1.64-0.53l2.7,2.83l1.17-4.96l2.93,0.47l-0.12-1.42l-4.82-4.22l-0.92,2.48L571.41,207.72L571.41,207.72z","name":"Azerbaijan"},"ir":{"path":"M569.65,217.95l-1.22,1.27l0.12,2.01l1.52,2.13l5.39,5.9l-0.82,2.36h-0.94l-0.47,2.36l3.05,3.9l2.81,0.24l5.63,7.79l3.16,0.24l2.46,1.77l0.12,3.54l9.73,5.67h3.63l2.23-1.89l2.81-0.12l1.64,3.78l10.51,1.46l0.31-3.86l3.48-1.26l0.16-1.38l-2.77-3.78l-6.17-4.96l3.24-2.95l-0.23-1.3l-4.06-0.63l-1.72-13.7l-0.2-3.15l-11.01-4.21l-4.88,1.1l-2.73,3.35l-2.42-0.16l-0.7,0.59l-5.39-0.35l-6.8-4.96l-2.53-2.77l-1.16,0.28l-2.09,2.39L569.65,217.95L569.65,217.95z","name":"Iran"},"tr":{"path":"M558.7,209.19l-2.23,2.36l-8.2-0.24l-4.92-2.95l-4.8-0.12l-5.51,3.9l-5.16,0.24l-0.47,2.95h-5.86l-2.34,2.13v1.18l1.41,1.18v1.3l-0.59,1.54l0.59,1.3l1.88-0.94l1.88,2.01l-0.47,1.42l-0.7,0.95l1.05,1.18l5.16,1.06l3.63-1.54v-2.24l1.76,0.35l4.22,2.48l4.57-0.71l1.99-1.89l1.29,0.47v2.13h1.76l1.52-2.95l13.36-1.42l5.83-0.71l-1.54-2.02l-0.03-2.73l1.17-1.4l-4.26-3.42l0.23-2.95h-2.34L558.7,209.19L558.7,209.19M523.02,209.7l-0.16,3.55l3.1-0.95l1.42-0.95l-0.42-1.54l-1.47-1.17L523.02,209.7L523.02,209.7z","name":"Turkey"},"om":{"path":"M598.38,280.84l7.39-4.26l1.31-6.25l-1.62-0.93l0.67-6.7l1.41-0.82l1.51,2.37l8.99,4.7v2.61l-10.89,16.03l-5.01,0.17L598.38,280.84L598.38,280.84z","name":"Oman"},"ae":{"path":"M594.01,264.94l0.87,3.48l9.86,0.87l0.69-7.14l1.9-1.04l0.52-2.61l-3.11,0.87l-3.46,5.23L594.01,264.94L594.01,264.94z","name":"United Arab Emirates"},"qa":{"path":"M592.63,259.02l-0.52,4.01l1.54,1.17l1.4-0.13l0.52-5.05l-1.21-0.87L592.63,259.02L592.63,259.02z","name":"Qatar"},"kw":{"path":"M583.29,247.17l-2.25-1.22l-1.56,1.57l0.17,3.14l3.63,1.39L583.29,247.17L583.29,247.17z","name":"Kuwait"},"sa":{"path":"M584,253.24l7.01,9.77l2.26,1.8l1.01,4.38l10.79,0.85l1.22,0.64l-1.21,5.4l-7.09,4.18l-10.37,3.14l-5.53,5.4l-6.57-3.83l-3.98,3.48L566,279.4l-3.8-1.74l-1.38-2.09v-4.53l-13.83-16.72l-0.52-2.96h3.98l4.84-4.18l0.17-2.09l-1.38-1.39l2.77-2.26l5.88,0.35l10.03,8.36l5.92-0.27l0.38,1.46L584,253.24L584,253.24z","name":"Saudi Arabia"},"sy":{"path":"M546.67,229.13l-0.35,2.54l2.82,1.18l-0.12,7.04l2.82-0.06l2.82-2.13l1.06-0.18l6.4-5.09l1.29-7.39l-12.79,1.3l-1.35,2.96L546.67,229.13L546.67,229.13z","name":"Syrian Arab Republic"},"iq":{"path":"M564.31,225.03l-1.56,7.71l-6.46,5.38l0.41,2.54l6.31,0.43l10.05,8.18l5.62-0.16l0.15-1.89l2.06-2.21l2.88,1.63l0.38-0.36l-5.57-7.41l-2.64-0.16l-3.51-4.51l0.7-3.32l1.07-0.14l0.37-1.47l-4.78-5.03L564.31,225.03L564.31,225.03z","name":"Iraq"},"jo":{"path":"M548.9,240.78l-2.46,8.58l-0.11,1.31h3.87l4.33-3.82l0.11-1.45l-1.77-1.81l3.17-2.63l-0.46-2.44l-0.87,0.2l-2.64,1.89L548.9,240.78L548.9,240.78z","name":"Jordan"},"lb":{"path":"M546.2,232.44l0.06,1.95l-0.82,2.96l2.82,0.24l0.18-4.2L546.2,232.44L546.2,232.44z","name":"Lebanon"},"il":{"path":"M545.32,238.06l-1.58,5.03l2.05,6.03l2.35-8.81v-1.89L545.32,238.06L545.32,238.06z","name":"Israel"},"cy":{"path":"M543.21,229.84l1.23,0.89l-3.81,3.61l-1.82-0.06l-1.35-0.95l0.18-1.77l2.76-0.18L543.21,229.84L543.21,229.84z","name":"Cyprus"},"gb":{"path":"M446.12,149.08l-1.83,2.77l0.73,1.11h4.22v1.85l-1.1,1.48l0.73,3.88l2.38,4.62l1.83,4.25l2.93,1.11l1.28,2.22l-0.18,2.03l-1.83,1.11l-0.18,0.92l1.28,0.74l-1.1,1.48l-2.57,1.11l-4.95-0.55l-7.71,3.51l-2.57-1.29l7.34-4.25l-0.92-0.55l-3.85-0.37l2.38-3.51l0.37-2.96l3.12-0.37l-0.55-5.73l-3.67-0.18l-1.1-1.29l0.18-4.25l-2.2,0.18l2.2-7.39l4.04-2.96L446.12,149.08L446.12,149.08M438.42,161.47l-3.3,0.37l-0.18,2.96l2.2,1.48l2.38-0.55l0.92-1.66L438.42,161.47L438.42,161.47z","name":"United Kingdom"},"ie":{"path":"M439.51,166.55l-0.91,6l-8.07,2.96h-2.57l-1.83-1.29v-1.11l4.04-2.59l-1.1-2.22l0.18-3.14l3.49,0.18l1.6-3.76l-0.21,3.34l2.71,2.15L439.51,166.55L439.51,166.55z","name":"Ireland"},"se":{"path":"M497.72,104.58l1.96,1.81h3.67l2.02,3.88l0.55,6.65l-4.95,3.51v3.51l-3.49,4.81l-2.02,0.18l-2.75,4.62l0.18,4.44l4.77,3.51l-0.37,2.03l-1.83,2.77l-2.75,2.4l0.18,7.95l-4.22,1.48l-1.47,3.14h-2.02l-1.1-5.54l-4.59-7.04l3.77-6.31l0.26-15.59l2.6-1.43l0.63-8.92l7.41-10.61L497.72,104.58L497.72,104.58M498.49,150.17l-2.11,1.67l1.06,2.45l1.87-1.82L498.49,150.17L498.49,150.17z","name":"Sweden"},"fi":{"path":"M506.79,116.94l2.07,0.91l1.28,2.4l-1.28,1.66l-6.42,7.02l-1.1,3.7l1.47,5.36l4.95,3.7l6.6-3.14l5.32-0.74l4.95-7.95l-3.67-8.69l-3.49-8.32l0.55-5.36l-2.2-0.37l-0.57-3.91l-2.96-4.83l-3.28,2.27l-1.29,5.27l-3.48-2.09l-4.84-1.18l-1.08,1.26l1.86,1.68l3.39-0.06l2.73,4.41L506.79,116.94L506.79,116.94z","name":"Finland"},"lv":{"path":"M518.07,151.37l-6.85-1.11l0.15,3.83l6.35,3.88l2.6-0.76l-0.15-2.92L518.07,151.37L518.07,151.37z","name":"Latvia"},"lt":{"path":"M510.81,154.7l-2.15-0.05l-2.95,2.82h-2.5l0.15,3.53l-1.5,2.77l5.4,0.05l1.55-0.2l1.55,1.87l3.55-0.15l3.4-4.33l-0.2-2.57L510.81,154.7L510.81,154.7z","name":"Lithuania"},"by":{"path":"M510.66,166.29l1.5,2.47l-0.6,1.97l0.1,1.56l0.55,1.87l3.1-1.76l3.85,0.1l2.7,1.11h6.85l2-4.79l1.2-1.81v-1.21l-4.3-6.05l-3.8-1.51l-3.1-0.35l-2.7,0.86l0.1,2.72l-3.75,4.74L510.66,166.29L510.66,166.29z","name":"Belarus"},"pl":{"path":"M511.46,174.76l0.85,1.56l0.2,1.66l-0.7,1.61l-1.6,3.08l-1.35,0.61l-1.75-0.76l-1.05,0.05l-2.55,0.96l-2.9-0.86l-4.7-3.33l-4.6-2.47l-1.85-2.82l-0.35-6.65l3.6-3.13l4.7-1.56l1.75-0.2l-0.7,1.41l0.45,0.55l7.91,0.15l1.7-0.05l2.8,4.29l-0.7,1.76l0.3,2.07L511.46,174.76L511.46,174.76z","name":"Poland"},"it":{"path":"M477.56,213.38l-2.65,1.34l0.35,5.17l2.12,0.36l1.59-1.52v-4.9L477.56,213.38L477.56,213.38M472.27,196.98l-0.62,1.57l0.17,1.71l2.39,2.79l3.76-0.13l8.3,9.64l5.18,1.5l3.06,2.89l0.73,6.59l1.64-0.96l1.42-3.59l-0.35-2.58l2.43-0.22l0.35-1.46l-6.85-3.28l-6.5-6.39l-2.59-3.82l-0.63-3.63l3.31-0.79l-0.85-2.39l-2.03-1.71l-1.75-0.08l-2.44,0.67l-2.3,3.22l-1.39,0.92l-2.15-1.32L472.27,196.98L472.27,196.98M492.44,223.02l-1.45-0.78l-4.95,0.78l0.17,1.34l4.45,2.24l0.67,0.73l1.17,0.17L492.44,223.02L492.44,223.02z","name":"Italy"},"fr":{"path":"M477.83,206.96l-1.95,1.96l-0.18,1.78l1.59,0.98l0.62-0.09l0.35-2.59L477.83,206.96L477.83,206.96M460.4,178.7l-2.21,0.54l-4.42,4.81l-1.33,0.09l-1.77-1.25l-1.15,0.27l-0.88,2.76l-6.46,0.18l0.18,1.43l4.42,2.94l5.13,4.1l-0.09,4.9l-2.74,4.81l5.93,2.85l6.02,0.18l1.86-2.14l3.8,0.09l1.06,0.98l3.8-0.27l1.95-2.5l-2.48-2.94l-0.18-1.87l0.53-2.05l-1.24-1.78l-2.12,0.62l-0.27-1.6l4.69-5.17v-3.12l-3.1-1.78l-1.59-0.27L460.4,178.7L460.4,178.7z","name":"France"},"nl":{"path":"M470.09,168.27l-4.53,2.23l0.96,0.87l0.1,2.23l-0.96-0.19l-1.06-1.65l-2.53,4.01l3.89,0.81l1.45,1.53l0.77,0.02l0.51-3.46l2.45-1.03L470.09,168.27L470.09,168.27z","name":"Netherlands"},"be":{"path":"M461.61,176.52l-0.64,1.6l6.88,4.54l1.98,0.47l0.07-2.15l-1.73-1.94h-1.06l-1.45-1.65L461.61,176.52L461.61,176.52z","name":"Belgium"},"de":{"path":"M471.14,167.88l3.57-0.58v-2.52l2.99-0.49l1.64,1.65l1.73,0.19l2.7-1.17l2.41,0.68l2.12,1.84l0.29,6.89l2.12,2.82l-2.79,0.39l-4.63,2.91l0.39,0.97l4.14,3.88l-0.29,1.94l-3.85,1.94l-3.57,0.1l-0.87,1.84h-1.83l-0.87-1.94l-3.18-0.78l-0.1-3.2l-2.7-1.84l0.29-2.33l-1.83-2.52l0.48-3.3l2.5-1.17L471.14,167.88L471.14,167.88z","name":"Germany"},"dk":{"path":"M476.77,151.5l-4.15,4.59l-0.15,2.99l1.89,4.93l2.96-0.56l-0.37-4.03l2.04-2.28l-0.04-1.79l-1.44-3.73L476.77,151.5L476.77,151.5M481.44,159.64l-0.93-0.04l-1.22,1.12l0.15,1.75l2.89,0.08l0.15-1.98L481.44,159.64L481.44,159.64z","name":"Denmark"},"ch":{"path":"M472.91,189.38l-4.36,4.64l0.09,0.47l1.79-0.56l1.61,2.24l2.72-0.96l1.88,1.46l0.77-0.44l2.32-3.64l-0.59-0.56l-2.29-0.06l-1.11-2.27L472.91,189.38L472.91,189.38z","name":"Switzerland"},"cz":{"path":"M488.43,184.87h2.97h1.46l2.37,1.69l4.39-3.65l-4.26-3.04l-4.22-2.04l-2.89,0.52l-3.92,2.52L488.43,184.87L488.43,184.87z","name":"Czech Republic"},"sk":{"path":"M495.84,187.13l0.69,0.61l0.09,1.04l7.63-0.17l5.64-2.43l-0.09-2.47l-1.08,0.48l-1.55-0.83l-0.95-0.04l-2.5,1l-3.4-0.82L495.84,187.13L495.84,187.13z","name":"Slovakia"},"at":{"path":"M480.63,190.12l-0.65,1.35l0.56,0.96l2.33-0.48h1.98l2.15,1.82l4.57-0.83l3.36-2l0.86-1.35l-0.13-1.74l-3.02-2.26l-4.05,0.04l-0.34,2.3l-4.26,2.08L480.63,190.12L480.63,190.12z","name":"Austria"},"hu":{"path":"M496.74,189.6l-1.16,1.82l0.09,2.78l1.85,0.95l5.69,0.17l7.93-6.68l0.04-1.48l-0.86-0.43l-5.73,2.6L496.74,189.6L496.74,189.6z","name":"Hungary"},"si":{"path":"M494.8,191.99l-2.54,1.52l-4.74,1.04l0.95,2.74l3.32,0.04l3.06-2.56L494.8,191.99L494.8,191.99z","name":"Slovenia"},"hr":{"path":"M495.62,195.16l-3.53,2.91h-3.58l-0.43,2.52l1.64,0.43l0.82-1.22l1.29,1.13l1.03,3.6l7.07,3.3l0.7-0.8l-7.17-7.4l0.73-1.35l6.81-0.26l0.69-2.17l-4.44,0.13L495.62,195.16L495.62,195.16z","name":"Croatia"},"ba":{"path":"M494.8,198.94l-0.37,0.61l6.71,6.92l2.46-3.62l-0.09-1.43l-2.15-2.61L494.8,198.94L494.8,198.94z","name":"Bosnia and Herzegovina"},"mt":{"path":"M492.61,230.47l-1.67,0.34l0.06,1.85l1.5,0.5l0.67-0.56L492.61,230.47L492.61,230.47z","name":"Malta"},"ua":{"path":"M515.57,173.15l-2.9,1.63l0.72,3.08l-2.68,5.65l0.02,2.49l1.26,0.8l8.08,0.4l2.26-1.87l2.42,0.81l3.47,4.63l-2.54,4.56l3.02,0.88l3.95-4.55l2.26,0.41l2.1,1.46l-1.85,2.44l2.5,3.9h2.66l1.37-2.6l2.82-0.57l0.08-2.11l-5.24-0.81l0.16-2.27h5.08l5.48-4.39l2.42-2.11l0.4-6.66l-10.8-0.97l-4.43-6.25l-3.06-1.05l-3.71,0.16l-1.67,4.13l-7.6,0.1l-2.47-1.14L515.57,173.15L515.57,173.15z","name":"Ukraine"},"md":{"path":"M520.75,187.71l3.1,4.77l-0.26,2.7l1.11,0.05l2.63-4.45l-3.16-3.92l-1.79-0.74L520.75,187.71L520.75,187.71z","name":"Moldova"},"ro":{"path":"M512.18,187.6l-0.26,1.48l-5.79,4.82l4.84,7.1l3.1,2.17h5.58l1.84-1.54l2.47-0.32l1.84,1.11l3.26-3.71l-0.63-1.86l-3.31-0.85l-2.26-0.11l0.11-3.18l-3-4.72L512.18,187.6L512.18,187.6z","name":"Romania"},"rs":{"path":"M505.55,194.54l-2.05,1.54h-1l-0.68,2.12l2.42,2.81l0.16,2.23l-3,4.24l0.42,1.27l1.74,0.32l1.37-1.86l0.74-0.05l1.26,1.22l3.84-1.17l-0.32-5.46L505.55,194.54L505.55,194.54z","name":"Serbia"},"bg":{"path":"M511.44,202.39l0.16,4.98l1.68,3.5l6.31,0.11l2.84-2.01l2.79-1.11l-0.68-3.18l0.63-1.7l-1.42-0.74l-1.95,0.16l-1.53,1.54l-6.42,0.05L511.44,202.39L511.44,202.39z","name":"Bulgaria"},"al":{"path":"M504.02,209.76v4.61l1.32,2.49l0.95-0.11l1.63-2.97l-0.95-1.33l-0.37-3.29l-1.26-1.17L504.02,209.76L504.02,209.76z","name":"Albania"},"mk":{"path":"M510.92,208.01l-3.37,1.11l0.16,2.86l0.79,1.01l4-1.86L510.92,208.01L510.92,208.01z","name":"Macedonia"},"gr":{"path":"M506.71,217.6l-0.11,1.33l4.63,2.33l2.21,0.85l-1.16,1.22l-2.58,0.26l-0.37,1.17l0.89,2.01l2.89,1.54l1.26,0.11l0.16-3.45l1.89-2.28l-5.16-6.1l0.68-2.07l1.21-0.05l1.84,1.48l1.16-0.58l0.37-2.07l5.42,0.05l0.21-3.18l-2.26,1.59l-6.63-0.16l-4.31,2.23L506.71,217.6L506.71,217.6M516.76,230.59l1.63,0.05l0.68,1.01h2.37l1.58-0.58l0.53,0.64l-1.05,1.38l-4.63,0.16l-0.84-1.11l-0.89-0.53L516.76,230.59L516.76,230.59z","name":"Greece"}}});

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJtYXBNYWluLmpzIiwic3JjL2pzb25Mb2FkZXIuanMiLCJzcmMvbGliL0NMRFJQbHVyYWxSdWxlUGFyc2VyL0NMRFJQbHVyYWxSdWxlUGFyc2VyLmpzIiwic3JjL2xpYi9oaXN0b3J5L2pxdWVyeS5oaXN0b3J5LmpzIiwic3JjL2xpYi9qcXVlcnkuaTE4bi9qcXVlcnkuaTE4bi5lbWl0dGVyLmJpZGkuanMiLCJzcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLmVtaXR0ZXIuanMiLCJzcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLmZhbGxiYWNrcy5qcyIsInNyYy9saWIvanF1ZXJ5LmkxOG4vanF1ZXJ5LmkxOG4uanMiLCJzcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLmxhbmd1YWdlLmpzIiwic3JjL2xpYi9qcXVlcnkuaTE4bi9qcXVlcnkuaTE4bi5tZXNzYWdlc3RvcmUuanMiLCJzcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLnBhcnNlci5qcyIsInNyYy9saWIvdXJsL3VybC5taW4uanMiLCJzcmMvbWFwTGFuZ3VhZ2VQaWNrZXIuanMiLCJzcmMvbWFwUHJlc2VudGVyLmpzIiwic3JjL21hcC9qcXVlcnkudm1hcC5qcyIsInNyYy9tYXAvbWFwcy9qcXVlcnkudm1hcC53b3JsZC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDL2xCQTtBQUNBOztBQ0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDOUZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3hLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMxTEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM3U0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbGZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNyVEE7QUFDQTs7QUNEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ25FQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlMQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzN3Q0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKXtmdW5jdGlvbiByKGUsbix0KXtmdW5jdGlvbiBvKGksZil7aWYoIW5baV0pe2lmKCFlW2ldKXt2YXIgYz1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlO2lmKCFmJiZjKXJldHVybiBjKGksITApO2lmKHUpcmV0dXJuIHUoaSwhMCk7dmFyIGE9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitpK1wiJ1wiKTt0aHJvdyBhLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsYX12YXIgcD1uW2ldPXtleHBvcnRzOnt9fTtlW2ldWzBdLmNhbGwocC5leHBvcnRzLGZ1bmN0aW9uKHIpe3ZhciBuPWVbaV1bMV1bcl07cmV0dXJuIG8obnx8cil9LHAscC5leHBvcnRzLHIsZSxuLHQpfXJldHVybiBuW2ldLmV4cG9ydHN9Zm9yKHZhciB1PVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmUsaT0wO2k8dC5sZW5ndGg7aSsrKW8odFtpXSk7cmV0dXJuIG99cmV0dXJuIHJ9KSgpIiwiLy9NYXAgSlMgY29kZVxyXG52YXIgbWFwUHJlc2VudGVyID0gcmVxdWlyZSgnLi9zcmMvbWFwUHJlc2VudGVyLmpzJyk7XHJcblxyXG52YXIganZtYXAgPSByZXF1aXJlKCcuL3NyYy9tYXAvanF1ZXJ5LnZtYXAuanMnKTtcclxuXHJcbnZhciB3b3JsZE1hcCA9IHJlcXVpcmUoJy4vc3JjL21hcC9tYXBzL2pxdWVyeS52bWFwLndvcmxkLmpzJyk7XHJcblxyXG4vL0ludGVybmF0aW9uYWxpemF0aW9ucyBsaWJzXHJcbnZhciBjbGRycCA9IHJlcXVpcmUoJy4vc3JjL2xpYi9DTERSUGx1cmFsUnVsZVBhcnNlci9DTERSUGx1cmFsUnVsZVBhcnNlci5qcycpO1xyXG5cclxudmFyIGpxdWVyeWkxOG4gPSByZXF1aXJlKCcuL3NyYy9saWIvanF1ZXJ5LmkxOG4vanF1ZXJ5LmkxOG4uanMnKTtcclxuXHJcbnZhciBtZXNzYWdlc3RvcmUgPSByZXF1aXJlKCcuL3NyYy9saWIvanF1ZXJ5LmkxOG4vanF1ZXJ5LmkxOG4ubWVzc2FnZXN0b3JlLmpzJyk7XHJcblxyXG52YXIgZmFsbGJhY2tzID0gcmVxdWlyZSgnLi9zcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLmZhbGxiYWNrcy5qcycpO1xyXG5cclxudmFyIGxhbmd1YWdlID0gcmVxdWlyZSgnLi9zcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLmxhbmd1YWdlLmpzJyk7XHJcblxyXG52YXIgcGFyc2VyID0gcmVxdWlyZSgnLi9zcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLnBhcnNlci5qcycpO1xyXG5cclxudmFyIGVtaXR0ZXIgPSByZXF1aXJlKCcuL3NyYy9saWIvanF1ZXJ5LmkxOG4vanF1ZXJ5LmkxOG4uZW1pdHRlci5qcycpO1xyXG5cclxudmFyIGVtaXR0ZXJiaWRpID0gcmVxdWlyZSgnLi9zcmMvbGliL2pxdWVyeS5pMThuL2pxdWVyeS5pMThuLmVtaXR0ZXIuYmlkaS5qcycpO1xyXG5cclxudmFyIGhpc3RvcnkgPSByZXF1aXJlKCcuL3NyYy9saWIvaGlzdG9yeS9qcXVlcnkuaGlzdG9yeS5qcycpO1xyXG5cclxudmFyIHVybCA9IHJlcXVpcmUoJy4vc3JjL2xpYi91cmwvdXJsLm1pbi5qcycpO1xyXG5cclxudmFyIG1hcExhbmd1YWdlUGlja2VyID0gcmVxdWlyZSgnLi9zcmMvbWFwTGFuZ3VhZ2VQaWNrZXIuanMnKTtcclxuXHJcbnZhciBqc29uTG9hZGVyID0gcmVxdWlyZSgnLi9zcmMvanNvbkxvYWRlci5qcycpOyIsImZ1bmN0aW9uIGxvYWRUZXh0RmlsZUFqYXhTeW5jKGZpbGUpIHtcclxuICB2YXIgeG9iaiA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG4gIHhvYmoub3ZlcnJpZGVNaW1lVHlwZShcImFwcGxpY2F0aW9uL2pzb25cIik7XHJcbiAgeG9iai5vcGVuKFwiR0VUXCIsIGZpbGUsIGZhbHNlKTsgLy8gUmVwbGFjZSBmaWxlIHdpdGggdGhlIHBhdGggdG8geW91ciBmaWxlXHJcbiAgeG9iai5zZW5kKCk7XHJcbiAgaWYgKHhvYmouc3RhdHVzID09IDIwMCkge1xyXG4gICAgcmV0dXJuIHhvYmoucmVzcG9uc2VUZXh0O1xyXG4gIH0gZWxzZSB7XHJcbiAgICAvLyBUT0RPIFRocm93IGV4Y2VwdGlvblxyXG4gICAgcmV0dXJuIG51bGw7XHJcbiAgfVxyXG59XHJcblxyXG5tb2R1bGUuZXhwb3J0cy5sb2FkSnNvbkZpbGUgPSBmdW5jdGlvbiBsb2FkSnNvbihmaWxlUGF0aCkge1xyXG4gIC8vIExvYWQganNvbiBmaWxlO1xyXG4gIHZhciBqc29uID0gbG9hZFRleHRGaWxlQWpheFN5bmMoZmlsZVBhdGgpO1xyXG4gIC8vIFBhcnNlIGpzb25cclxuICByZXR1cm4gSlNPTi5wYXJzZShqc29uKTtcclxufTtcclxuXHJcbiIsIi8qKlxyXG4gKiBjbGRycGx1cmFscGFyc2VyLmpzXHJcbiAqIEEgcGFyc2VyIGVuZ2luZSBmb3IgQ0xEUiBwbHVyYWwgcnVsZXMuXHJcbiAqXHJcbiAqIENvcHlyaWdodCAyMDEyLTIwMTQgU2FudGhvc2ggVGhvdHRpbmdhbCBhbmQgb3RoZXIgY29udHJpYnV0b3JzXHJcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxyXG4gKiBodHRwOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUXHJcbiAqXHJcbiAqIEBzb3VyY2UgaHR0cHM6Ly9naXRodWIuY29tL3NhbnRob3NodHIvQ0xEUlBsdXJhbFJ1bGVQYXJzZXJcclxuICogQGF1dGhvciBTYW50aG9zaCBUaG90dGluZ2FsIDxzYW50aG9zaC50aG90dGluZ2FsQGdtYWlsLmNvbT5cclxuICogQGF1dGhvciBUaW1vIFRpamhvZlxyXG4gKiBAYXV0aG9yIEFtaXIgQWhhcm9uaVxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiBFdmFsdWF0ZXMgYSBwbHVyYWwgcnVsZSBpbiBDTERSIHN5bnRheCBmb3IgYSBudW1iZXJcclxuICogQHBhcmFtIHtzdHJpbmd9IHJ1bGVcclxuICogQHBhcmFtIHtpbnRlZ2VyfSBudW1iZXJcclxuICogQHJldHVybiB7Ym9vbGVhbn0gdHJ1ZSBpZiBldmFsdWF0aW9uIHBhc3NlZCwgZmFsc2UgaWYgZXZhbHVhdGlvbiBmYWlsZWQuXHJcbiAqL1xyXG5cclxuLy8gVU1EIHJldHVybkV4cG9ydHMgaHR0cHM6Ly9naXRodWIuY29tL3VtZGpzL3VtZC9ibG9iL21hc3Rlci9yZXR1cm5FeHBvcnRzLmpzXHJcbihmdW5jdGlvbihyb290LCBmYWN0b3J5KSB7XHJcblx0aWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xyXG5cdFx0Ly8gQU1ELiBSZWdpc3RlciBhcyBhbiBhbm9ueW1vdXMgbW9kdWxlLlxyXG5cdFx0ZGVmaW5lKGZhY3RvcnkpO1xyXG5cdH0gZWxzZSBpZiAodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKSB7XHJcblx0XHQvLyBOb2RlLiBEb2VzIG5vdCB3b3JrIHdpdGggc3RyaWN0IENvbW1vbkpTLCBidXRcclxuXHRcdC8vIG9ubHkgQ29tbW9uSlMtbGlrZSBlbnZpcm9ubWVudHMgdGhhdCBzdXBwb3J0IG1vZHVsZS5leHBvcnRzLFxyXG5cdFx0Ly8gbGlrZSBOb2RlLlxyXG5cdFx0bW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KCk7XHJcblx0fSBlbHNlIHtcclxuXHRcdC8vIEJyb3dzZXIgZ2xvYmFscyAocm9vdCBpcyB3aW5kb3cpXHJcblx0XHRyb290LnBsdXJhbFJ1bGVQYXJzZXIgPSBmYWN0b3J5KCk7XHJcblx0fVxyXG59KHRoaXMsIGZ1bmN0aW9uKCkge1xyXG5cclxuZnVuY3Rpb24gcGx1cmFsUnVsZVBhcnNlcihydWxlLCBudW1iZXIpIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblxyXG5cdC8qXHJcblx0U3ludGF4OiBzZWUgaHR0cDovL3VuaWNvZGUub3JnL3JlcG9ydHMvdHIzNS8jTGFuZ3VhZ2VfUGx1cmFsX1J1bGVzXHJcblx0LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRjb25kaXRpb24gICAgID0gYW5kX2NvbmRpdGlvbiAoJ29yJyBhbmRfY29uZGl0aW9uKSpcclxuXHRcdCgnQGludGVnZXInIHNhbXBsZXMpP1xyXG5cdFx0KCdAZGVjaW1hbCcgc2FtcGxlcyk/XHJcblx0YW5kX2NvbmRpdGlvbiA9IHJlbGF0aW9uICgnYW5kJyByZWxhdGlvbikqXHJcblx0cmVsYXRpb24gICAgICA9IGlzX3JlbGF0aW9uIHwgaW5fcmVsYXRpb24gfCB3aXRoaW5fcmVsYXRpb25cclxuXHRpc19yZWxhdGlvbiAgID0gZXhwciAnaXMnICgnbm90Jyk/IHZhbHVlXHJcblx0aW5fcmVsYXRpb24gICA9IGV4cHIgKCgnbm90Jyk/ICdpbicgfCAnPScgfCAnIT0nKSByYW5nZV9saXN0XHJcblx0d2l0aGluX3JlbGF0aW9uID0gZXhwciAoJ25vdCcpPyAnd2l0aGluJyByYW5nZV9saXN0XHJcblx0ZXhwciAgICAgICAgICA9IG9wZXJhbmQgKCgnbW9kJyB8ICclJykgdmFsdWUpP1xyXG5cdG9wZXJhbmQgICAgICAgPSAnbicgfCAnaScgfCAnZicgfCAndCcgfCAndicgfCAndydcclxuXHRyYW5nZV9saXN0ICAgID0gKHJhbmdlIHwgdmFsdWUpICgnLCcgcmFuZ2VfbGlzdCkqXHJcblx0dmFsdWUgICAgICAgICA9IGRpZ2l0K1xyXG5cdGRpZ2l0ICAgICAgICAgPSAwfDF8MnwzfDR8NXw2fDd8OHw5XHJcblx0cmFuZ2UgICAgICAgICA9IHZhbHVlJy4uJ3ZhbHVlXHJcblx0c2FtcGxlcyAgICAgICA9IHNhbXBsZVJhbmdlICgnLCcgc2FtcGxlUmFuZ2UpKiAoJywnICgn4oCmJ3wnLi4uJykpP1xyXG5cdHNhbXBsZVJhbmdlICAgPSBkZWNpbWFsVmFsdWUgJ34nIGRlY2ltYWxWYWx1ZVxyXG5cdGRlY2ltYWxWYWx1ZSAgPSB2YWx1ZSAoJy4nIHZhbHVlKT9cclxuXHQqL1xyXG5cclxuXHQvLyBXZSBkb24ndCBldmFsdWF0ZSB0aGUgc2FtcGxlcyBzZWN0aW9uIG9mIHRoZSBydWxlLiBJZ25vcmUgaXQuXHJcblx0cnVsZSA9IHJ1bGUuc3BsaXQoJ0AnKVswXS5yZXBsYWNlKC9eXFxzKi8sICcnKS5yZXBsYWNlKC9cXHMqJC8sICcnKTtcclxuXHJcblx0aWYgKCFydWxlLmxlbmd0aCkge1xyXG5cdFx0Ly8gRW1wdHkgcnVsZSBvciAnb3RoZXInIHJ1bGUuXHJcblx0XHRyZXR1cm4gdHJ1ZTtcclxuXHR9XHJcblxyXG5cdC8vIEluZGljYXRlcyB0aGUgY3VycmVudCBwb3NpdGlvbiBpbiB0aGUgcnVsZSBhcyB3ZSBwYXJzZSB0aHJvdWdoIGl0LlxyXG5cdC8vIFNoYXJlZCBhbW9uZyBhbGwgcGFyc2luZyBmdW5jdGlvbnMgYmVsb3cuXHJcblx0dmFyIHBvcyA9IDAsXHJcblx0XHRvcGVyYW5kLFxyXG5cdFx0ZXhwcmVzc2lvbixcclxuXHRcdHJlbGF0aW9uLFxyXG5cdFx0cmVzdWx0LFxyXG5cdFx0d2hpdGVzcGFjZSA9IG1ha2VSZWdleFBhcnNlcigvXlxccysvKSxcclxuXHRcdHZhbHVlID0gbWFrZVJlZ2V4UGFyc2VyKC9eXFxkKy8pLFxyXG5cdFx0X25fID0gbWFrZVN0cmluZ1BhcnNlcignbicpLFxyXG5cdFx0X2lfID0gbWFrZVN0cmluZ1BhcnNlcignaScpLFxyXG5cdFx0X2ZfID0gbWFrZVN0cmluZ1BhcnNlcignZicpLFxyXG5cdFx0X3RfID0gbWFrZVN0cmluZ1BhcnNlcigndCcpLFxyXG5cdFx0X3ZfID0gbWFrZVN0cmluZ1BhcnNlcigndicpLFxyXG5cdFx0X3dfID0gbWFrZVN0cmluZ1BhcnNlcigndycpLFxyXG5cdFx0X2lzXyA9IG1ha2VTdHJpbmdQYXJzZXIoJ2lzJyksXHJcblx0XHRfaXNub3RfID0gbWFrZVN0cmluZ1BhcnNlcignaXMgbm90JyksXHJcblx0XHRfaXNub3Rfc2lnbl8gPSBtYWtlU3RyaW5nUGFyc2VyKCchPScpLFxyXG5cdFx0X2VxdWFsXyA9IG1ha2VTdHJpbmdQYXJzZXIoJz0nKSxcclxuXHRcdF9tb2RfID0gbWFrZVN0cmluZ1BhcnNlcignbW9kJyksXHJcblx0XHRfcGVyY2VudF8gPSBtYWtlU3RyaW5nUGFyc2VyKCclJyksXHJcblx0XHRfbm90XyA9IG1ha2VTdHJpbmdQYXJzZXIoJ25vdCcpLFxyXG5cdFx0X2luXyA9IG1ha2VTdHJpbmdQYXJzZXIoJ2luJyksXHJcblx0XHRfd2l0aGluXyA9IG1ha2VTdHJpbmdQYXJzZXIoJ3dpdGhpbicpLFxyXG5cdFx0X3JhbmdlXyA9IG1ha2VTdHJpbmdQYXJzZXIoJy4uJyksXHJcblx0XHRfY29tbWFfID0gbWFrZVN0cmluZ1BhcnNlcignLCcpLFxyXG5cdFx0X29yXyA9IG1ha2VTdHJpbmdQYXJzZXIoJ29yJyksXHJcblx0XHRfYW5kXyA9IG1ha2VTdHJpbmdQYXJzZXIoJ2FuZCcpO1xyXG5cclxuXHRmdW5jdGlvbiBkZWJ1ZygpIHtcclxuXHRcdC8vIGNvbnNvbGUubG9nLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XHJcblx0fVxyXG5cclxuXHRkZWJ1ZygncGx1cmFsUnVsZVBhcnNlcicsIHJ1bGUsIG51bWJlcik7XHJcblxyXG5cdC8vIFRyeSBwYXJzZXJzIHVudGlsIG9uZSB3b3JrcywgaWYgbm9uZSB3b3JrIHJldHVybiBudWxsXHJcblx0ZnVuY3Rpb24gY2hvaWNlKHBhcnNlclN5bnRheCkge1xyXG5cdFx0cmV0dXJuIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgaSwgcmVzdWx0O1xyXG5cclxuXHRcdFx0Zm9yIChpID0gMDsgaSA8IHBhcnNlclN5bnRheC5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRcdHJlc3VsdCA9IHBhcnNlclN5bnRheFtpXSgpO1xyXG5cclxuXHRcdFx0XHRpZiAocmVzdWx0ICE9PSBudWxsKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIG51bGw7XHJcblx0XHR9O1xyXG5cdH1cclxuXHJcblx0Ly8gVHJ5IHNldmVyYWwgcGFyc2VyU3ludGF4LWVzIGluIGEgcm93LlxyXG5cdC8vIEFsbCBtdXN0IHN1Y2NlZWQ7IG90aGVyd2lzZSwgcmV0dXJuIG51bGwuXHJcblx0Ly8gVGhpcyBpcyB0aGUgb25seSBlYWdlciBvbmUuXHJcblx0ZnVuY3Rpb24gc2VxdWVuY2UocGFyc2VyU3ludGF4KSB7XHJcblx0XHR2YXIgaSwgcGFyc2VyUmVzLFxyXG5cdFx0XHRvcmlnaW5hbFBvcyA9IHBvcyxcclxuXHRcdFx0cmVzdWx0ID0gW107XHJcblxyXG5cdFx0Zm9yIChpID0gMDsgaSA8IHBhcnNlclN5bnRheC5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRwYXJzZXJSZXMgPSBwYXJzZXJTeW50YXhbaV0oKTtcclxuXHJcblx0XHRcdGlmIChwYXJzZXJSZXMgPT09IG51bGwpIHtcclxuXHRcdFx0XHRwb3MgPSBvcmlnaW5hbFBvcztcclxuXHJcblx0XHRcdFx0cmV0dXJuIG51bGw7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJlc3VsdC5wdXNoKHBhcnNlclJlcyk7XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIHJlc3VsdDtcclxuXHR9XHJcblxyXG5cdC8vIFJ1biB0aGUgc2FtZSBwYXJzZXIgb3ZlciBhbmQgb3ZlciB1bnRpbCBpdCBmYWlscy5cclxuXHQvLyBNdXN0IHN1Y2NlZWQgYSBtaW5pbXVtIG9mIG4gdGltZXM7IG90aGVyd2lzZSwgcmV0dXJuIG51bGwuXHJcblx0ZnVuY3Rpb24gbk9yTW9yZShuLCBwKSB7XHJcblx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBvcmlnaW5hbFBvcyA9IHBvcyxcclxuXHRcdFx0XHRyZXN1bHQgPSBbXSxcclxuXHRcdFx0XHRwYXJzZWQgPSBwKCk7XHJcblxyXG5cdFx0XHR3aGlsZSAocGFyc2VkICE9PSBudWxsKSB7XHJcblx0XHRcdFx0cmVzdWx0LnB1c2gocGFyc2VkKTtcclxuXHRcdFx0XHRwYXJzZWQgPSBwKCk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmIChyZXN1bHQubGVuZ3RoIDwgbikge1xyXG5cdFx0XHRcdHBvcyA9IG9yaWdpbmFsUG9zO1xyXG5cclxuXHRcdFx0XHRyZXR1cm4gbnVsbDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdH07XHJcblx0fVxyXG5cclxuXHQvLyBIZWxwZXJzIC0ganVzdCBtYWtlIHBhcnNlclN5bnRheCBvdXQgb2Ygc2ltcGxlciBKUyBidWlsdGluIHR5cGVzXHJcblx0ZnVuY3Rpb24gbWFrZVN0cmluZ1BhcnNlcihzKSB7XHJcblx0XHR2YXIgbGVuID0gcy5sZW5ndGg7XHJcblxyXG5cdFx0cmV0dXJuIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgcmVzdWx0ID0gbnVsbDtcclxuXHJcblx0XHRcdGlmIChydWxlLnN1YnN0cihwb3MsIGxlbikgPT09IHMpIHtcclxuXHRcdFx0XHRyZXN1bHQgPSBzO1xyXG5cdFx0XHRcdHBvcyArPSBsZW47XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJldHVybiByZXN1bHQ7XHJcblx0XHR9O1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gbWFrZVJlZ2V4UGFyc2VyKHJlZ2V4KSB7XHJcblx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciBtYXRjaGVzID0gcnVsZS5zdWJzdHIocG9zKS5tYXRjaChyZWdleCk7XHJcblxyXG5cdFx0XHRpZiAobWF0Y2hlcyA9PT0gbnVsbCkge1xyXG5cdFx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRwb3MgKz0gbWF0Y2hlc1swXS5sZW5ndGg7XHJcblxyXG5cdFx0XHRyZXR1cm4gbWF0Y2hlc1swXTtcclxuXHRcdH07XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBJbnRlZ2VyIGRpZ2l0cyBvZiBuLlxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIGkoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gX2lfKCk7XHJcblxyXG5cdFx0aWYgKHJlc3VsdCA9PT0gbnVsbCkge1xyXG5cdFx0XHRkZWJ1ZygnIC0tIGZhaWxlZCBpJywgcGFyc2VJbnQobnVtYmVyLCAxMCkpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdH1cclxuXHJcblx0XHRyZXN1bHQgPSBwYXJzZUludChudW1iZXIsIDEwKTtcclxuXHRcdGRlYnVnKCcgLS0gcGFzc2VkIGkgJywgcmVzdWx0KTtcclxuXHJcblx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogQWJzb2x1dGUgdmFsdWUgb2YgdGhlIHNvdXJjZSBudW1iZXIgKGludGVnZXIgYW5kIGRlY2ltYWxzKS5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiBuKCkge1xyXG5cdFx0dmFyIHJlc3VsdCA9IF9uXygpO1xyXG5cclxuXHRcdGlmIChyZXN1bHQgPT09IG51bGwpIHtcclxuXHRcdFx0ZGVidWcoJyAtLSBmYWlsZWQgbiAnLCBudW1iZXIpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdH1cclxuXHJcblx0XHRyZXN1bHQgPSBwYXJzZUZsb2F0KG51bWJlciwgMTApO1xyXG5cdFx0ZGVidWcoJyAtLSBwYXNzZWQgbiAnLCByZXN1bHQpO1xyXG5cclxuXHRcdHJldHVybiByZXN1bHQ7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBWaXNpYmxlIGZyYWN0aW9uYWwgZGlnaXRzIGluIG4sIHdpdGggdHJhaWxpbmcgemVyb3MuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gZigpIHtcclxuXHRcdHZhciByZXN1bHQgPSBfZl8oKTtcclxuXHJcblx0XHRpZiAocmVzdWx0ID09PSBudWxsKSB7XHJcblx0XHRcdGRlYnVnKCcgLS0gZmFpbGVkIGYgJywgbnVtYmVyKTtcclxuXHJcblx0XHRcdHJldHVybiByZXN1bHQ7XHJcblx0XHR9XHJcblxyXG5cdFx0cmVzdWx0ID0gKG51bWJlciArICcuJykuc3BsaXQoJy4nKVsxXSB8fCAwO1xyXG5cdFx0ZGVidWcoJyAtLSBwYXNzZWQgZiAnLCByZXN1bHQpO1xyXG5cclxuXHRcdHJldHVybiByZXN1bHQ7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBWaXNpYmxlIGZyYWN0aW9uYWwgZGlnaXRzIGluIG4sIHdpdGhvdXQgdHJhaWxpbmcgemVyb3MuXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gdCgpIHtcclxuXHRcdHZhciByZXN1bHQgPSBfdF8oKTtcclxuXHJcblx0XHRpZiAocmVzdWx0ID09PSBudWxsKSB7XHJcblx0XHRcdGRlYnVnKCcgLS0gZmFpbGVkIHQgJywgbnVtYmVyKTtcclxuXHJcblx0XHRcdHJldHVybiByZXN1bHQ7XHJcblx0XHR9XHJcblxyXG5cdFx0cmVzdWx0ID0gKG51bWJlciArICcuJykuc3BsaXQoJy4nKVsxXS5yZXBsYWNlKC8wJC8sICcnKSB8fCAwO1xyXG5cdFx0ZGVidWcoJyAtLSBwYXNzZWQgdCAnLCByZXN1bHQpO1xyXG5cclxuXHRcdHJldHVybiByZXN1bHQ7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBOdW1iZXIgb2YgdmlzaWJsZSBmcmFjdGlvbiBkaWdpdHMgaW4gbiwgd2l0aCB0cmFpbGluZyB6ZXJvcy5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiB2KCkge1xyXG5cdFx0dmFyIHJlc3VsdCA9IF92XygpO1xyXG5cclxuXHRcdGlmIChyZXN1bHQgPT09IG51bGwpIHtcclxuXHRcdFx0ZGVidWcoJyAtLSBmYWlsZWQgdiAnLCBudW1iZXIpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdH1cclxuXHJcblx0XHRyZXN1bHQgPSAobnVtYmVyICsgJy4nKS5zcGxpdCgnLicpWzFdLmxlbmd0aCB8fCAwO1xyXG5cdFx0ZGVidWcoJyAtLSBwYXNzZWQgdiAnLCByZXN1bHQpO1xyXG5cclxuXHRcdHJldHVybiByZXN1bHQ7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBOdW1iZXIgb2YgdmlzaWJsZSBmcmFjdGlvbiBkaWdpdHMgaW4gbiwgd2l0aG91dCB0cmFpbGluZyB6ZXJvcy5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiB3KCkge1xyXG5cdFx0dmFyIHJlc3VsdCA9IF93XygpO1xyXG5cclxuXHRcdGlmIChyZXN1bHQgPT09IG51bGwpIHtcclxuXHRcdFx0ZGVidWcoJyAtLSBmYWlsZWQgdyAnLCBudW1iZXIpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdH1cclxuXHJcblx0XHRyZXN1bHQgPSAobnVtYmVyICsgJy4nKS5zcGxpdCgnLicpWzFdLnJlcGxhY2UoLzAkLywgJycpLmxlbmd0aCB8fCAwO1xyXG5cdFx0ZGVidWcoJyAtLSBwYXNzZWQgdyAnLCByZXN1bHQpO1xyXG5cclxuXHRcdHJldHVybiByZXN1bHQ7XHJcblx0fVxyXG5cclxuXHQvLyBvcGVyYW5kICAgICAgID0gJ24nIHwgJ2knIHwgJ2YnIHwgJ3QnIHwgJ3YnIHwgJ3cnXHJcblx0b3BlcmFuZCA9IGNob2ljZShbbiwgaSwgZiwgdCwgdiwgd10pO1xyXG5cclxuXHQvLyBleHByICAgICAgICAgID0gb3BlcmFuZCAoKCdtb2QnIHwgJyUnKSB2YWx1ZSk/XHJcblx0ZXhwcmVzc2lvbiA9IGNob2ljZShbbW9kLCBvcGVyYW5kXSk7XHJcblxyXG5cdGZ1bmN0aW9uIG1vZCgpIHtcclxuXHRcdHZhciByZXN1bHQgPSBzZXF1ZW5jZShcclxuXHRcdFx0W29wZXJhbmQsIHdoaXRlc3BhY2UsIGNob2ljZShbX21vZF8sIF9wZXJjZW50X10pLCB3aGl0ZXNwYWNlLCB2YWx1ZV1cclxuXHRcdCk7XHJcblxyXG5cdFx0aWYgKHJlc3VsdCA9PT0gbnVsbCkge1xyXG5cdFx0XHRkZWJ1ZygnIC0tIGZhaWxlZCBtb2QnKTtcclxuXHJcblx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0fVxyXG5cclxuXHRcdGRlYnVnKCcgLS0gcGFzc2VkICcgKyBwYXJzZUludChyZXN1bHRbMF0sIDEwKSArICcgJyArIHJlc3VsdFsyXSArICcgJyArIHBhcnNlSW50KHJlc3VsdFs0XSwgMTApKTtcclxuXHJcblx0XHRyZXR1cm4gcGFyc2VGbG9hdChyZXN1bHRbMF0pICUgcGFyc2VJbnQocmVzdWx0WzRdLCAxMCk7XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBub3QoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gc2VxdWVuY2UoW3doaXRlc3BhY2UsIF9ub3RfXSk7XHJcblxyXG5cdFx0aWYgKHJlc3VsdCA9PT0gbnVsbCkge1xyXG5cdFx0XHRkZWJ1ZygnIC0tIGZhaWxlZCBub3QnKTtcclxuXHJcblx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybiByZXN1bHRbMV07XHJcblx0fVxyXG5cclxuXHQvLyBpc19yZWxhdGlvbiAgID0gZXhwciAnaXMnICgnbm90Jyk/IHZhbHVlXHJcblx0ZnVuY3Rpb24gaXMoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gc2VxdWVuY2UoW2V4cHJlc3Npb24sIHdoaXRlc3BhY2UsIGNob2ljZShbX2lzX10pLCB3aGl0ZXNwYWNlLCB2YWx1ZV0pO1xyXG5cclxuXHRcdGlmIChyZXN1bHQgIT09IG51bGwpIHtcclxuXHRcdFx0ZGVidWcoJyAtLSBwYXNzZWQgaXMgOiAnICsgcmVzdWx0WzBdICsgJyA9PSAnICsgcGFyc2VJbnQocmVzdWx0WzRdLCAxMCkpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdFswXSA9PT0gcGFyc2VJbnQocmVzdWx0WzRdLCAxMCk7XHJcblx0XHR9XHJcblxyXG5cdFx0ZGVidWcoJyAtLSBmYWlsZWQgaXMnKTtcclxuXHJcblx0XHRyZXR1cm4gbnVsbDtcclxuXHR9XHJcblxyXG5cdC8vIGlzX3JlbGF0aW9uICAgPSBleHByICdpcycgKCdub3QnKT8gdmFsdWVcclxuXHRmdW5jdGlvbiBpc25vdCgpIHtcclxuXHRcdHZhciByZXN1bHQgPSBzZXF1ZW5jZShcclxuXHRcdFx0W2V4cHJlc3Npb24sIHdoaXRlc3BhY2UsIGNob2ljZShbX2lzbm90XywgX2lzbm90X3NpZ25fXSksIHdoaXRlc3BhY2UsIHZhbHVlXVxyXG5cdFx0KTtcclxuXHJcblx0XHRpZiAocmVzdWx0ICE9PSBudWxsKSB7XHJcblx0XHRcdGRlYnVnKCcgLS0gcGFzc2VkIGlzbm90OiAnICsgcmVzdWx0WzBdICsgJyAhPSAnICsgcGFyc2VJbnQocmVzdWx0WzRdLCAxMCkpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdFswXSAhPT0gcGFyc2VJbnQocmVzdWx0WzRdLCAxMCk7XHJcblx0XHR9XHJcblxyXG5cdFx0ZGVidWcoJyAtLSBmYWlsZWQgaXNub3QnKTtcclxuXHJcblx0XHRyZXR1cm4gbnVsbDtcclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIG5vdF9pbigpIHtcclxuXHRcdHZhciBpLCByYW5nZV9saXN0LFxyXG5cdFx0XHRyZXN1bHQgPSBzZXF1ZW5jZShbZXhwcmVzc2lvbiwgd2hpdGVzcGFjZSwgX2lzbm90X3NpZ25fLCB3aGl0ZXNwYWNlLCByYW5nZUxpc3RdKTtcclxuXHJcblx0XHRpZiAocmVzdWx0ICE9PSBudWxsKSB7XHJcblx0XHRcdGRlYnVnKCcgLS0gcGFzc2VkIG5vdF9pbjogJyArIHJlc3VsdFswXSArICcgIT0gJyArIHJlc3VsdFs0XSk7XHJcblx0XHRcdHJhbmdlX2xpc3QgPSByZXN1bHRbNF07XHJcblxyXG5cdFx0XHRmb3IgKGkgPSAwOyBpIDwgcmFuZ2VfbGlzdC5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRcdGlmIChwYXJzZUludChyYW5nZV9saXN0W2ldLCAxMCkgPT09IHBhcnNlSW50KHJlc3VsdFswXSwgMTApKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdH1cclxuXHJcblx0XHRkZWJ1ZygnIC0tIGZhaWxlZCBub3RfaW4nKTtcclxuXHJcblx0XHRyZXR1cm4gbnVsbDtcclxuXHR9XHJcblxyXG5cdC8vIHJhbmdlX2xpc3QgICAgPSAocmFuZ2UgfCB2YWx1ZSkgKCcsJyByYW5nZV9saXN0KSpcclxuXHRmdW5jdGlvbiByYW5nZUxpc3QoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gc2VxdWVuY2UoW2Nob2ljZShbcmFuZ2UsIHZhbHVlXSksIG5Pck1vcmUoMCwgcmFuZ2VUYWlsKV0pLFxyXG5cdFx0XHRyZXN1bHRMaXN0ID0gW107XHJcblxyXG5cdFx0aWYgKHJlc3VsdCAhPT0gbnVsbCkge1xyXG5cdFx0XHRyZXN1bHRMaXN0ID0gcmVzdWx0TGlzdC5jb25jYXQocmVzdWx0WzBdKTtcclxuXHJcblx0XHRcdGlmIChyZXN1bHRbMV1bMF0pIHtcclxuXHRcdFx0XHRyZXN1bHRMaXN0ID0gcmVzdWx0TGlzdC5jb25jYXQocmVzdWx0WzFdWzBdKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdExpc3Q7XHJcblx0XHR9XHJcblxyXG5cdFx0ZGVidWcoJyAtLSBmYWlsZWQgcmFuZ2VMaXN0Jyk7XHJcblxyXG5cdFx0cmV0dXJuIG51bGw7XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiByYW5nZVRhaWwoKSB7XHJcblx0XHQvLyAnLCcgcmFuZ2VfbGlzdFxyXG5cdFx0dmFyIHJlc3VsdCA9IHNlcXVlbmNlKFtfY29tbWFfLCByYW5nZUxpc3RdKTtcclxuXHJcblx0XHRpZiAocmVzdWx0ICE9PSBudWxsKSB7XHJcblx0XHRcdHJldHVybiByZXN1bHRbMV07XHJcblx0XHR9XHJcblxyXG5cdFx0ZGVidWcoJyAtLSBmYWlsZWQgcmFuZ2VUYWlsJyk7XHJcblxyXG5cdFx0cmV0dXJuIG51bGw7XHJcblx0fVxyXG5cclxuXHQvLyByYW5nZSAgICAgICAgID0gdmFsdWUnLi4ndmFsdWVcclxuXHRmdW5jdGlvbiByYW5nZSgpIHtcclxuXHRcdHZhciBpLCBhcnJheSwgbGVmdCwgcmlnaHQsXHJcblx0XHRcdHJlc3VsdCA9IHNlcXVlbmNlKFt2YWx1ZSwgX3JhbmdlXywgdmFsdWVdKTtcclxuXHJcblx0XHRpZiAocmVzdWx0ICE9PSBudWxsKSB7XHJcblx0XHRcdGRlYnVnKCcgLS0gcGFzc2VkIHJhbmdlJyk7XHJcblxyXG5cdFx0XHRhcnJheSA9IFtdO1xyXG5cdFx0XHRsZWZ0ID0gcGFyc2VJbnQocmVzdWx0WzBdLCAxMCk7XHJcblx0XHRcdHJpZ2h0ID0gcGFyc2VJbnQocmVzdWx0WzJdLCAxMCk7XHJcblxyXG5cdFx0XHRmb3IgKGkgPSBsZWZ0OyBpIDw9IHJpZ2h0OyBpKyspIHtcclxuXHRcdFx0XHRhcnJheS5wdXNoKGkpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRyZXR1cm4gYXJyYXk7XHJcblx0XHR9XHJcblxyXG5cdFx0ZGVidWcoJyAtLSBmYWlsZWQgcmFuZ2UnKTtcclxuXHJcblx0XHRyZXR1cm4gbnVsbDtcclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIF9pbigpIHtcclxuXHRcdHZhciByZXN1bHQsIHJhbmdlX2xpc3QsIGk7XHJcblxyXG5cdFx0Ly8gaW5fcmVsYXRpb24gICA9IGV4cHIgKCdub3QnKT8gJ2luJyByYW5nZV9saXN0XHJcblx0XHRyZXN1bHQgPSBzZXF1ZW5jZShcclxuXHRcdFx0W2V4cHJlc3Npb24sIG5Pck1vcmUoMCwgbm90KSwgd2hpdGVzcGFjZSwgY2hvaWNlKFtfaW5fLCBfZXF1YWxfXSksIHdoaXRlc3BhY2UsIHJhbmdlTGlzdF1cclxuXHRcdCk7XHJcblxyXG5cdFx0aWYgKHJlc3VsdCAhPT0gbnVsbCkge1xyXG5cdFx0XHRkZWJ1ZygnIC0tIHBhc3NlZCBfaW46JyArIHJlc3VsdCk7XHJcblxyXG5cdFx0XHRyYW5nZV9saXN0ID0gcmVzdWx0WzVdO1xyXG5cclxuXHRcdFx0Zm9yIChpID0gMDsgaSA8IHJhbmdlX2xpc3QubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRpZiAocGFyc2VJbnQocmFuZ2VfbGlzdFtpXSwgMTApID09PSBwYXJzZUZsb2F0KHJlc3VsdFswXSkpIHtcclxuXHRcdFx0XHRcdHJldHVybiAocmVzdWx0WzFdWzBdICE9PSAnbm90Jyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRyZXR1cm4gKHJlc3VsdFsxXVswXSA9PT0gJ25vdCcpO1xyXG5cdFx0fVxyXG5cclxuXHRcdGRlYnVnKCcgLS0gZmFpbGVkIF9pbiAnKTtcclxuXHJcblx0XHRyZXR1cm4gbnVsbDtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIFRoZSBkaWZmZXJlbmNlIGJldHdlZW4gXCJpblwiIGFuZCBcIndpdGhpblwiIGlzIHRoYXRcclxuXHQgKiBcImluXCIgb25seSBpbmNsdWRlcyBpbnRlZ2VycyBpbiB0aGUgc3BlY2lmaWVkIHJhbmdlLFxyXG5cdCAqIHdoaWxlIFwid2l0aGluXCIgaW5jbHVkZXMgYWxsIHZhbHVlcy5cclxuXHQgKi9cclxuXHRmdW5jdGlvbiB3aXRoaW4oKSB7XHJcblx0XHR2YXIgcmFuZ2VfbGlzdCwgcmVzdWx0O1xyXG5cclxuXHRcdC8vIHdpdGhpbl9yZWxhdGlvbiA9IGV4cHIgKCdub3QnKT8gJ3dpdGhpbicgcmFuZ2VfbGlzdFxyXG5cdFx0cmVzdWx0ID0gc2VxdWVuY2UoXHJcblx0XHRcdFtleHByZXNzaW9uLCBuT3JNb3JlKDAsIG5vdCksIHdoaXRlc3BhY2UsIF93aXRoaW5fLCB3aGl0ZXNwYWNlLCByYW5nZUxpc3RdXHJcblx0XHQpO1xyXG5cclxuXHRcdGlmIChyZXN1bHQgIT09IG51bGwpIHtcclxuXHRcdFx0ZGVidWcoJyAtLSBwYXNzZWQgd2l0aGluJyk7XHJcblxyXG5cdFx0XHRyYW5nZV9saXN0ID0gcmVzdWx0WzVdO1xyXG5cclxuXHRcdFx0aWYgKChyZXN1bHRbMF0gPj0gcGFyc2VJbnQocmFuZ2VfbGlzdFswXSwgMTApKSAmJlxyXG5cdFx0XHRcdChyZXN1bHRbMF0gPCBwYXJzZUludChyYW5nZV9saXN0W3JhbmdlX2xpc3QubGVuZ3RoIC0gMV0sIDEwKSkpIHtcclxuXHJcblx0XHRcdFx0cmV0dXJuIChyZXN1bHRbMV1bMF0gIT09ICdub3QnKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIChyZXN1bHRbMV1bMF0gPT09ICdub3QnKTtcclxuXHRcdH1cclxuXHJcblx0XHRkZWJ1ZygnIC0tIGZhaWxlZCB3aXRoaW4gJyk7XHJcblxyXG5cdFx0cmV0dXJuIG51bGw7XHJcblx0fVxyXG5cclxuXHQvLyByZWxhdGlvbiAgICAgID0gaXNfcmVsYXRpb24gfCBpbl9yZWxhdGlvbiB8IHdpdGhpbl9yZWxhdGlvblxyXG5cdHJlbGF0aW9uID0gY2hvaWNlKFtpcywgbm90X2luLCBpc25vdCwgX2luLCB3aXRoaW5dKTtcclxuXHJcblx0Ly8gYW5kX2NvbmRpdGlvbiA9IHJlbGF0aW9uICgnYW5kJyByZWxhdGlvbikqXHJcblx0ZnVuY3Rpb24gYW5kKCkge1xyXG5cdFx0dmFyIGksXHJcblx0XHRcdHJlc3VsdCA9IHNlcXVlbmNlKFtyZWxhdGlvbiwgbk9yTW9yZSgwLCBhbmRUYWlsKV0pO1xyXG5cclxuXHRcdGlmIChyZXN1bHQpIHtcclxuXHRcdFx0aWYgKCFyZXN1bHRbMF0pIHtcclxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGZvciAoaSA9IDA7IGkgPCByZXN1bHRbMV0ubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRpZiAoIXJlc3VsdFsxXVtpXSkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHR9XHJcblxyXG5cdFx0ZGVidWcoJyAtLSBmYWlsZWQgYW5kJyk7XHJcblxyXG5cdFx0cmV0dXJuIG51bGw7XHJcblx0fVxyXG5cclxuXHQvLyAoJ2FuZCcgcmVsYXRpb24pKlxyXG5cdGZ1bmN0aW9uIGFuZFRhaWwoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gc2VxdWVuY2UoW3doaXRlc3BhY2UsIF9hbmRfLCB3aGl0ZXNwYWNlLCByZWxhdGlvbl0pO1xyXG5cclxuXHRcdGlmIChyZXN1bHQgIT09IG51bGwpIHtcclxuXHRcdFx0ZGVidWcoJyAtLSBwYXNzZWQgYW5kVGFpbCcgKyByZXN1bHQpO1xyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdFszXTtcclxuXHRcdH1cclxuXHJcblx0XHRkZWJ1ZygnIC0tIGZhaWxlZCBhbmRUYWlsJyk7XHJcblxyXG5cdFx0cmV0dXJuIG51bGw7XHJcblxyXG5cdH1cclxuXHQvLyAgKCdvcicgYW5kX2NvbmRpdGlvbikqXHJcblx0ZnVuY3Rpb24gb3JUYWlsKCkge1xyXG5cdFx0dmFyIHJlc3VsdCA9IHNlcXVlbmNlKFt3aGl0ZXNwYWNlLCBfb3JfLCB3aGl0ZXNwYWNlLCBhbmRdKTtcclxuXHJcblx0XHRpZiAocmVzdWx0ICE9PSBudWxsKSB7XHJcblx0XHRcdGRlYnVnKCcgLS0gcGFzc2VkIG9yVGFpbDogJyArIHJlc3VsdFszXSk7XHJcblxyXG5cdFx0XHRyZXR1cm4gcmVzdWx0WzNdO1xyXG5cdFx0fVxyXG5cclxuXHRcdGRlYnVnKCcgLS0gZmFpbGVkIG9yVGFpbCcpO1xyXG5cclxuXHRcdHJldHVybiBudWxsO1xyXG5cdH1cclxuXHJcblx0Ly8gY29uZGl0aW9uICAgICA9IGFuZF9jb25kaXRpb24gKCdvcicgYW5kX2NvbmRpdGlvbikqXHJcblx0ZnVuY3Rpb24gY29uZGl0aW9uKCkge1xyXG5cdFx0dmFyIGksXHJcblx0XHRcdHJlc3VsdCA9IHNlcXVlbmNlKFthbmQsIG5Pck1vcmUoMCwgb3JUYWlsKV0pO1xyXG5cclxuXHRcdGlmIChyZXN1bHQpIHtcclxuXHRcdFx0Zm9yIChpID0gMDsgaSA8IHJlc3VsdFsxXS5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRcdGlmIChyZXN1bHRbMV1baV0pIHtcclxuXHRcdFx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdFswXTtcclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG5cclxuXHRyZXN1bHQgPSBjb25kaXRpb24oKTtcclxuXHJcblx0LyoqXHJcblx0ICogRm9yIHN1Y2Nlc3MsIHRoZSBwb3MgbXVzdCBoYXZlIGdvdHRlbiB0byB0aGUgZW5kIG9mIHRoZSBydWxlXHJcblx0ICogYW5kIHJldHVybmVkIGEgbm9uLW51bGwuXHJcblx0ICogbi5iLiBUaGlzIGlzIHBhcnQgb2YgbGFuZ3VhZ2UgaW5mcmFzdHJ1Y3R1cmUsXHJcblx0ICogc28gd2UgZG8gbm90IHRocm93IGFuIGludGVybmF0aW9uYWxpemFibGUgbWVzc2FnZS5cclxuXHQgKi9cclxuXHRpZiAocmVzdWx0ID09PSBudWxsKSB7XHJcblx0XHR0aHJvdyBuZXcgRXJyb3IoJ1BhcnNlIGVycm9yIGF0IHBvc2l0aW9uICcgKyBwb3MudG9TdHJpbmcoKSArICcgZm9yIHJ1bGU6ICcgKyBydWxlKTtcclxuXHR9XHJcblxyXG5cdGlmIChwb3MgIT09IHJ1bGUubGVuZ3RoKSB7XHJcblx0XHRkZWJ1ZygnV2FybmluZzogUnVsZSBub3QgcGFyc2VkIGNvbXBsZXRlbHkuIFBhcnNlciBzdG9wcGVkIGF0ICcgKyBydWxlLnN1YnN0cigwLCBwb3MpICsgJyBmb3IgcnVsZTogJyArIHJ1bGUpO1xyXG5cdH1cclxuXHJcblx0cmV0dXJuIHJlc3VsdDtcclxufVxyXG5cclxucmV0dXJuIHBsdXJhbFJ1bGVQYXJzZXI7XHJcblxyXG59KSk7XHJcbiIsInR5cGVvZiBKU09OIT1cIm9iamVjdFwiJiYoSlNPTj17fSksZnVuY3Rpb24oKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBmKGUpe3JldHVybiBlPDEwP1wiMFwiK2U6ZX1mdW5jdGlvbiBxdW90ZShlKXtyZXR1cm4gZXNjYXBhYmxlLmxhc3RJbmRleD0wLGVzY2FwYWJsZS50ZXN0KGUpPydcIicrZS5yZXBsYWNlKGVzY2FwYWJsZSxmdW5jdGlvbihlKXt2YXIgdD1tZXRhW2VdO3JldHVybiB0eXBlb2YgdD09XCJzdHJpbmdcIj90OlwiXFxcXHVcIisoXCIwMDAwXCIrZS5jaGFyQ29kZUF0KDApLnRvU3RyaW5nKDE2KSkuc2xpY2UoLTQpfSkrJ1wiJzonXCInK2UrJ1wiJ31mdW5jdGlvbiBzdHIoZSx0KXt2YXIgbixyLGkscyxvPWdhcCx1LGE9dFtlXTthJiZ0eXBlb2YgYT09XCJvYmplY3RcIiYmdHlwZW9mIGEudG9KU09OPT1cImZ1bmN0aW9uXCImJihhPWEudG9KU09OKGUpKSx0eXBlb2YgcmVwPT1cImZ1bmN0aW9uXCImJihhPXJlcC5jYWxsKHQsZSxhKSk7c3dpdGNoKHR5cGVvZiBhKXtjYXNlXCJzdHJpbmdcIjpyZXR1cm4gcXVvdGUoYSk7Y2FzZVwibnVtYmVyXCI6cmV0dXJuIGlzRmluaXRlKGEpP1N0cmluZyhhKTpcIm51bGxcIjtjYXNlXCJib29sZWFuXCI6Y2FzZVwibnVsbFwiOnJldHVybiBTdHJpbmcoYSk7Y2FzZVwib2JqZWN0XCI6aWYoIWEpcmV0dXJuXCJudWxsXCI7Z2FwKz1pbmRlbnQsdT1bXTtpZihPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmFwcGx5KGEpPT09XCJbb2JqZWN0IEFycmF5XVwiKXtzPWEubGVuZ3RoO2ZvcihuPTA7bjxzO24rPTEpdVtuXT1zdHIobixhKXx8XCJudWxsXCI7cmV0dXJuIGk9dS5sZW5ndGg9PT0wP1wiW11cIjpnYXA/XCJbXFxuXCIrZ2FwK3Uuam9pbihcIixcXG5cIitnYXApK1wiXFxuXCIrbytcIl1cIjpcIltcIit1LmpvaW4oXCIsXCIpK1wiXVwiLGdhcD1vLGl9aWYocmVwJiZ0eXBlb2YgcmVwPT1cIm9iamVjdFwiKXtzPXJlcC5sZW5ndGg7Zm9yKG49MDtuPHM7bis9MSl0eXBlb2YgcmVwW25dPT1cInN0cmluZ1wiJiYocj1yZXBbbl0saT1zdHIocixhKSxpJiZ1LnB1c2gocXVvdGUocikrKGdhcD9cIjogXCI6XCI6XCIpK2kpKX1lbHNlIGZvcihyIGluIGEpT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKGEscikmJihpPXN0cihyLGEpLGkmJnUucHVzaChxdW90ZShyKSsoZ2FwP1wiOiBcIjpcIjpcIikraSkpO3JldHVybiBpPXUubGVuZ3RoPT09MD9cInt9XCI6Z2FwP1wie1xcblwiK2dhcCt1LmpvaW4oXCIsXFxuXCIrZ2FwKStcIlxcblwiK28rXCJ9XCI6XCJ7XCIrdS5qb2luKFwiLFwiKStcIn1cIixnYXA9byxpfX10eXBlb2YgRGF0ZS5wcm90b3R5cGUudG9KU09OIT1cImZ1bmN0aW9uXCImJihEYXRlLnByb3RvdHlwZS50b0pTT049ZnVuY3Rpb24oZSl7cmV0dXJuIGlzRmluaXRlKHRoaXMudmFsdWVPZigpKT90aGlzLmdldFVUQ0Z1bGxZZWFyKCkrXCItXCIrZih0aGlzLmdldFVUQ01vbnRoKCkrMSkrXCItXCIrZih0aGlzLmdldFVUQ0RhdGUoKSkrXCJUXCIrZih0aGlzLmdldFVUQ0hvdXJzKCkpK1wiOlwiK2YodGhpcy5nZXRVVENNaW51dGVzKCkpK1wiOlwiK2YodGhpcy5nZXRVVENTZWNvbmRzKCkpK1wiWlwiOm51bGx9LFN0cmluZy5wcm90b3R5cGUudG9KU09OPU51bWJlci5wcm90b3R5cGUudG9KU09OPUJvb2xlYW4ucHJvdG90eXBlLnRvSlNPTj1mdW5jdGlvbihlKXtyZXR1cm4gdGhpcy52YWx1ZU9mKCl9KTt2YXIgY3g9L1tcXHUwMDAwXFx1MDBhZFxcdTA2MDAtXFx1MDYwNFxcdTA3MGZcXHUxN2I0XFx1MTdiNVxcdTIwMGMtXFx1MjAwZlxcdTIwMjgtXFx1MjAyZlxcdTIwNjAtXFx1MjA2ZlxcdWZlZmZcXHVmZmYwLVxcdWZmZmZdL2csZXNjYXBhYmxlPS9bXFxcXFxcXCJcXHgwMC1cXHgxZlxceDdmLVxceDlmXFx1MDBhZFxcdTA2MDAtXFx1MDYwNFxcdTA3MGZcXHUxN2I0XFx1MTdiNVxcdTIwMGMtXFx1MjAwZlxcdTIwMjgtXFx1MjAyZlxcdTIwNjAtXFx1MjA2ZlxcdWZlZmZcXHVmZmYwLVxcdWZmZmZdL2csZ2FwLGluZGVudCxtZXRhPXtcIlxcYlwiOlwiXFxcXGJcIixcIiBcIjpcIlxcXFx0XCIsXCJcXG5cIjpcIlxcXFxuXCIsXCJcXGZcIjpcIlxcXFxmXCIsXCJcXHJcIjpcIlxcXFxyXCIsJ1wiJzonXFxcXFwiJyxcIlxcXFxcIjpcIlxcXFxcXFxcXCJ9LHJlcDt0eXBlb2YgSlNPTi5zdHJpbmdpZnkhPVwiZnVuY3Rpb25cIiYmKEpTT04uc3RyaW5naWZ5PWZ1bmN0aW9uKGUsdCxuKXt2YXIgcjtnYXA9XCJcIixpbmRlbnQ9XCJcIjtpZih0eXBlb2Ygbj09XCJudW1iZXJcIilmb3Iocj0wO3I8bjtyKz0xKWluZGVudCs9XCIgXCI7ZWxzZSB0eXBlb2Ygbj09XCJzdHJpbmdcIiYmKGluZGVudD1uKTtyZXA9dDtpZighdHx8dHlwZW9mIHQ9PVwiZnVuY3Rpb25cInx8dHlwZW9mIHQ9PVwib2JqZWN0XCImJnR5cGVvZiB0Lmxlbmd0aD09XCJudW1iZXJcIilyZXR1cm4gc3RyKFwiXCIse1wiXCI6ZX0pO3Rocm93IG5ldyBFcnJvcihcIkpTT04uc3RyaW5naWZ5XCIpfSksdHlwZW9mIEpTT04ucGFyc2UhPVwiZnVuY3Rpb25cIiYmKEpTT04ucGFyc2U9ZnVuY3Rpb24odGV4dCxyZXZpdmVyKXtmdW5jdGlvbiB3YWxrKGUsdCl7dmFyIG4scixpPWVbdF07aWYoaSYmdHlwZW9mIGk9PVwib2JqZWN0XCIpZm9yKG4gaW4gaSlPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoaSxuKSYmKHI9d2FsayhpLG4pLHIhPT11bmRlZmluZWQ/aVtuXT1yOmRlbGV0ZSBpW25dKTtyZXR1cm4gcmV2aXZlci5jYWxsKGUsdCxpKX12YXIgajt0ZXh0PVN0cmluZyh0ZXh0KSxjeC5sYXN0SW5kZXg9MCxjeC50ZXN0KHRleHQpJiYodGV4dD10ZXh0LnJlcGxhY2UoY3gsZnVuY3Rpb24oZSl7cmV0dXJuXCJcXFxcdVwiKyhcIjAwMDBcIitlLmNoYXJDb2RlQXQoMCkudG9TdHJpbmcoMTYpKS5zbGljZSgtNCl9KSk7aWYoL15bXFxdLDp7fVxcc10qJC8udGVzdCh0ZXh0LnJlcGxhY2UoL1xcXFwoPzpbXCJcXFxcXFwvYmZucnRdfHVbMC05YS1mQS1GXXs0fSkvZyxcIkBcIikucmVwbGFjZSgvXCJbXlwiXFxcXFxcblxccl0qXCJ8dHJ1ZXxmYWxzZXxudWxsfC0/XFxkKyg/OlxcLlxcZCopPyg/OltlRV1bK1xcLV0/XFxkKyk/L2csXCJdXCIpLnJlcGxhY2UoLyg/Ol58OnwsKSg/OlxccypcXFspKy9nLFwiXCIpKSlyZXR1cm4gaj1ldmFsKFwiKFwiK3RleHQrXCIpXCIpLHR5cGVvZiByZXZpdmVyPT1cImZ1bmN0aW9uXCI/d2Fsayh7XCJcIjpqfSxcIlwiKTpqO3Rocm93IG5ldyBTeW50YXhFcnJvcihcIkpTT04ucGFyc2VcIil9KX0oKSxmdW5jdGlvbihlLHQpe1widXNlIHN0cmljdFwiO3ZhciBuPWUuSGlzdG9yeT1lLkhpc3Rvcnl8fHt9LHI9ZS5qUXVlcnk7aWYodHlwZW9mIG4uQWRhcHRlciE9XCJ1bmRlZmluZWRcIil0aHJvdyBuZXcgRXJyb3IoXCJIaXN0b3J5LmpzIEFkYXB0ZXIgaGFzIGFscmVhZHkgYmVlbiBsb2FkZWQuLi5cIik7bi5BZGFwdGVyPXtiaW5kOmZ1bmN0aW9uKGUsdCxuKXtyKGUpLmJpbmQodCxuKX0sdHJpZ2dlcjpmdW5jdGlvbihlLHQsbil7cihlKS50cmlnZ2VyKHQsbil9LGV4dHJhY3RFdmVudERhdGE6ZnVuY3Rpb24oZSxuLHIpe3ZhciBpPW4mJm4ub3JpZ2luYWxFdmVudCYmbi5vcmlnaW5hbEV2ZW50W2VdfHxyJiZyW2VdfHx0O3JldHVybiBpfSxvbkRvbUxvYWQ6ZnVuY3Rpb24oZSl7cihlKX19LHR5cGVvZiBuLmluaXQhPVwidW5kZWZpbmVkXCImJm4uaW5pdCgpfSh3aW5kb3cpLGZ1bmN0aW9uKGUsdCl7XCJ1c2Ugc3RyaWN0XCI7dmFyIG49ZS5kb2N1bWVudCxyPWUuc2V0VGltZW91dHx8cixpPWUuY2xlYXJUaW1lb3V0fHxpLHM9ZS5zZXRJbnRlcnZhbHx8cyxvPWUuSGlzdG9yeT1lLkhpc3Rvcnl8fHt9O2lmKHR5cGVvZiBvLmluaXRIdG1sNCE9XCJ1bmRlZmluZWRcIil0aHJvdyBuZXcgRXJyb3IoXCJIaXN0b3J5LmpzIEhUTUw0IFN1cHBvcnQgaGFzIGFscmVhZHkgYmVlbiBsb2FkZWQuLi5cIik7by5pbml0SHRtbDQ9ZnVuY3Rpb24oKXtpZih0eXBlb2Ygby5pbml0SHRtbDQuaW5pdGlhbGl6ZWQhPVwidW5kZWZpbmVkXCIpcmV0dXJuITE7by5pbml0SHRtbDQuaW5pdGlhbGl6ZWQ9ITAsby5lbmFibGVkPSEwLG8uc2F2ZWRIYXNoZXM9W10sby5pc0xhc3RIYXNoPWZ1bmN0aW9uKGUpe3ZhciB0PW8uZ2V0SGFzaEJ5SW5kZXgoKSxuO3JldHVybiBuPWU9PT10LG59LG8uaXNIYXNoRXF1YWw9ZnVuY3Rpb24oZSx0KXtyZXR1cm4gZT1lbmNvZGVVUklDb21wb25lbnQoZSkucmVwbGFjZSgvJTI1L2csXCIlXCIpLHQ9ZW5jb2RlVVJJQ29tcG9uZW50KHQpLnJlcGxhY2UoLyUyNS9nLFwiJVwiKSxlPT09dH0sby5zYXZlSGFzaD1mdW5jdGlvbihlKXtyZXR1cm4gby5pc0xhc3RIYXNoKGUpPyExOihvLnNhdmVkSGFzaGVzLnB1c2goZSksITApfSxvLmdldEhhc2hCeUluZGV4PWZ1bmN0aW9uKGUpe3ZhciB0PW51bGw7cmV0dXJuIHR5cGVvZiBlPT1cInVuZGVmaW5lZFwiP3Q9by5zYXZlZEhhc2hlc1tvLnNhdmVkSGFzaGVzLmxlbmd0aC0xXTplPDA/dD1vLnNhdmVkSGFzaGVzW28uc2F2ZWRIYXNoZXMubGVuZ3RoK2VdOnQ9by5zYXZlZEhhc2hlc1tlXSx0fSxvLmRpc2NhcmRlZEhhc2hlcz17fSxvLmRpc2NhcmRlZFN0YXRlcz17fSxvLmRpc2NhcmRTdGF0ZT1mdW5jdGlvbihlLHQsbil7dmFyIHI9by5nZXRIYXNoQnlTdGF0ZShlKSxpO3JldHVybiBpPXtkaXNjYXJkZWRTdGF0ZTplLGJhY2tTdGF0ZTpuLGZvcndhcmRTdGF0ZTp0fSxvLmRpc2NhcmRlZFN0YXRlc1tyXT1pLCEwfSxvLmRpc2NhcmRIYXNoPWZ1bmN0aW9uKGUsdCxuKXt2YXIgcj17ZGlzY2FyZGVkSGFzaDplLGJhY2tTdGF0ZTpuLGZvcndhcmRTdGF0ZTp0fTtyZXR1cm4gby5kaXNjYXJkZWRIYXNoZXNbZV09ciwhMH0sby5kaXNjYXJkZWRTdGF0ZT1mdW5jdGlvbihlKXt2YXIgdD1vLmdldEhhc2hCeVN0YXRlKGUpLG47cmV0dXJuIG49by5kaXNjYXJkZWRTdGF0ZXNbdF18fCExLG59LG8uZGlzY2FyZGVkSGFzaD1mdW5jdGlvbihlKXt2YXIgdD1vLmRpc2NhcmRlZEhhc2hlc1tlXXx8ITE7cmV0dXJuIHR9LG8ucmVjeWNsZVN0YXRlPWZ1bmN0aW9uKGUpe3ZhciB0PW8uZ2V0SGFzaEJ5U3RhdGUoZSk7cmV0dXJuIG8uZGlzY2FyZGVkU3RhdGUoZSkmJmRlbGV0ZSBvLmRpc2NhcmRlZFN0YXRlc1t0XSwhMH0sby5lbXVsYXRlZC5oYXNoQ2hhbmdlJiYoby5oYXNoQ2hhbmdlSW5pdD1mdW5jdGlvbigpe28uY2hlY2tlckZ1bmN0aW9uPW51bGw7dmFyIHQ9XCJcIixyLGksdSxhLGY9Qm9vbGVhbihvLmdldEhhc2goKSk7cmV0dXJuIG8uaXNJbnRlcm5ldEV4cGxvcmVyKCk/KHI9XCJoaXN0b3J5anMtaWZyYW1lXCIsaT1uLmNyZWF0ZUVsZW1lbnQoXCJpZnJhbWVcIiksaS5zZXRBdHRyaWJ1dGUoXCJpZFwiLHIpLGkuc2V0QXR0cmlidXRlKFwic3JjXCIsXCIjXCIpLGkuc3R5bGUuZGlzcGxheT1cIm5vbmVcIixuLmJvZHkuYXBwZW5kQ2hpbGQoaSksaS5jb250ZW50V2luZG93LmRvY3VtZW50Lm9wZW4oKSxpLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQuY2xvc2UoKSx1PVwiXCIsYT0hMSxvLmNoZWNrZXJGdW5jdGlvbj1mdW5jdGlvbigpe2lmKGEpcmV0dXJuITE7YT0hMDt2YXIgbj1vLmdldEhhc2goKSxyPW8uZ2V0SGFzaChpLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQpO3JldHVybiBuIT09dD8odD1uLHIhPT1uJiYodT1yPW4saS5jb250ZW50V2luZG93LmRvY3VtZW50Lm9wZW4oKSxpLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQuY2xvc2UoKSxpLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQubG9jYXRpb24uaGFzaD1vLmVzY2FwZUhhc2gobikpLG8uQWRhcHRlci50cmlnZ2VyKGUsXCJoYXNoY2hhbmdlXCIpKTpyIT09dSYmKHU9cixmJiZyPT09XCJcIj9vLmJhY2soKTpvLnNldEhhc2gociwhMSkpLGE9ITEsITB9KTpvLmNoZWNrZXJGdW5jdGlvbj1mdW5jdGlvbigpe3ZhciBuPW8uZ2V0SGFzaCgpfHxcIlwiO3JldHVybiBuIT09dCYmKHQ9bixvLkFkYXB0ZXIudHJpZ2dlcihlLFwiaGFzaGNoYW5nZVwiKSksITB9LG8uaW50ZXJ2YWxMaXN0LnB1c2gocyhvLmNoZWNrZXJGdW5jdGlvbixvLm9wdGlvbnMuaGFzaENoYW5nZUludGVydmFsKSksITB9LG8uQWRhcHRlci5vbkRvbUxvYWQoby5oYXNoQ2hhbmdlSW5pdCkpLG8uZW11bGF0ZWQucHVzaFN0YXRlJiYoby5vbkhhc2hDaGFuZ2U9ZnVuY3Rpb24odCl7dmFyIG49dCYmdC5uZXdVUkx8fG8uZ2V0TG9jYXRpb25IcmVmKCkscj1vLmdldEhhc2hCeVVybChuKSxpPW51bGwscz1udWxsLHU9bnVsbCxhO3JldHVybiBvLmlzTGFzdEhhc2gocik/KG8uYnVzeSghMSksITEpOihvLmRvdWJsZUNoZWNrQ29tcGxldGUoKSxvLnNhdmVIYXNoKHIpLHImJm8uaXNUcmFkaXRpb25hbEFuY2hvcihyKT8oby5BZGFwdGVyLnRyaWdnZXIoZSxcImFuY2hvcmNoYW5nZVwiKSxvLmJ1c3koITEpLCExKTooaT1vLmV4dHJhY3RTdGF0ZShvLmdldEZ1bGxVcmwocnx8by5nZXRMb2NhdGlvbkhyZWYoKSksITApLG8uaXNMYXN0U2F2ZWRTdGF0ZShpKT8oby5idXN5KCExKSwhMSk6KHM9by5nZXRIYXNoQnlTdGF0ZShpKSxhPW8uZGlzY2FyZGVkU3RhdGUoaSksYT8oby5nZXRIYXNoQnlJbmRleCgtMik9PT1vLmdldEhhc2hCeVN0YXRlKGEuZm9yd2FyZFN0YXRlKT9vLmJhY2soITEpOm8uZm9yd2FyZCghMSksITEpOihvLnB1c2hTdGF0ZShpLmRhdGEsaS50aXRsZSxlbmNvZGVVUkkoaS51cmwpLCExKSwhMCkpKSl9LG8uQWRhcHRlci5iaW5kKGUsXCJoYXNoY2hhbmdlXCIsby5vbkhhc2hDaGFuZ2UpLG8ucHVzaFN0YXRlPWZ1bmN0aW9uKHQsbixyLGkpe3I9ZW5jb2RlVVJJKHIpLnJlcGxhY2UoLyUyNS9nLFwiJVwiKTtpZihvLmdldEhhc2hCeVVybChyKSl0aHJvdyBuZXcgRXJyb3IoXCJIaXN0b3J5LmpzIGRvZXMgbm90IHN1cHBvcnQgc3RhdGVzIHdpdGggZnJhZ21lbnQtaWRlbnRpZmllcnMgKGhhc2hlcy9hbmNob3JzKS5cIik7aWYoaSE9PSExJiZvLmJ1c3koKSlyZXR1cm4gby5wdXNoUXVldWUoe3Njb3BlOm8sY2FsbGJhY2s6by5wdXNoU3RhdGUsYXJnczphcmd1bWVudHMscXVldWU6aX0pLCExO28uYnVzeSghMCk7dmFyIHM9by5jcmVhdGVTdGF0ZU9iamVjdCh0LG4sciksdT1vLmdldEhhc2hCeVN0YXRlKHMpLGE9by5nZXRTdGF0ZSghMSksZj1vLmdldEhhc2hCeVN0YXRlKGEpLGw9by5nZXRIYXNoKCksYz1vLmV4cGVjdGVkU3RhdGVJZD09cy5pZDtyZXR1cm4gby5zdG9yZVN0YXRlKHMpLG8uZXhwZWN0ZWRTdGF0ZUlkPXMuaWQsby5yZWN5Y2xlU3RhdGUocyksby5zZXRUaXRsZShzKSx1PT09Zj8oby5idXN5KCExKSwhMSk6KG8uc2F2ZVN0YXRlKHMpLGN8fG8uQWRhcHRlci50cmlnZ2VyKGUsXCJzdGF0ZWNoYW5nZVwiKSwhby5pc0hhc2hFcXVhbCh1LGwpJiYhby5pc0hhc2hFcXVhbCh1LG8uZ2V0U2hvcnRVcmwoby5nZXRMb2NhdGlvbkhyZWYoKSkpJiZvLnNldEhhc2godSwhMSksby5idXN5KCExKSwhMCl9LG8ucmVwbGFjZVN0YXRlPWZ1bmN0aW9uKHQsbixyLGkpe3I9ZW5jb2RlVVJJKHIpLnJlcGxhY2UoLyUyNS9nLFwiJVwiKTtpZihvLmdldEhhc2hCeVVybChyKSl0aHJvdyBuZXcgRXJyb3IoXCJIaXN0b3J5LmpzIGRvZXMgbm90IHN1cHBvcnQgc3RhdGVzIHdpdGggZnJhZ21lbnQtaWRlbnRpZmllcnMgKGhhc2hlcy9hbmNob3JzKS5cIik7aWYoaSE9PSExJiZvLmJ1c3koKSlyZXR1cm4gby5wdXNoUXVldWUoe3Njb3BlOm8sY2FsbGJhY2s6by5yZXBsYWNlU3RhdGUsYXJnczphcmd1bWVudHMscXVldWU6aX0pLCExO28uYnVzeSghMCk7dmFyIHM9by5jcmVhdGVTdGF0ZU9iamVjdCh0LG4sciksdT1vLmdldEhhc2hCeVN0YXRlKHMpLGE9by5nZXRTdGF0ZSghMSksZj1vLmdldEhhc2hCeVN0YXRlKGEpLGw9by5nZXRTdGF0ZUJ5SW5kZXgoLTIpO3JldHVybiBvLmRpc2NhcmRTdGF0ZShhLHMsbCksdT09PWY/KG8uc3RvcmVTdGF0ZShzKSxvLmV4cGVjdGVkU3RhdGVJZD1zLmlkLG8ucmVjeWNsZVN0YXRlKHMpLG8uc2V0VGl0bGUocyksby5zYXZlU3RhdGUocyksby5BZGFwdGVyLnRyaWdnZXIoZSxcInN0YXRlY2hhbmdlXCIpLG8uYnVzeSghMSkpOm8ucHVzaFN0YXRlKHMuZGF0YSxzLnRpdGxlLHMudXJsLCExKSwhMH0pLG8uZW11bGF0ZWQucHVzaFN0YXRlJiZvLmdldEhhc2goKSYmIW8uZW11bGF0ZWQuaGFzaENoYW5nZSYmby5BZGFwdGVyLm9uRG9tTG9hZChmdW5jdGlvbigpe28uQWRhcHRlci50cmlnZ2VyKGUsXCJoYXNoY2hhbmdlXCIpfSl9LHR5cGVvZiBvLmluaXQhPVwidW5kZWZpbmVkXCImJm8uaW5pdCgpfSh3aW5kb3cpLGZ1bmN0aW9uKGUsdCl7XCJ1c2Ugc3RyaWN0XCI7dmFyIG49ZS5jb25zb2xlfHx0LHI9ZS5kb2N1bWVudCxpPWUubmF2aWdhdG9yLHM9ITEsbz1lLnNldFRpbWVvdXQsdT1lLmNsZWFyVGltZW91dCxhPWUuc2V0SW50ZXJ2YWwsZj1lLmNsZWFySW50ZXJ2YWwsbD1lLkpTT04sYz1lLmFsZXJ0LGg9ZS5IaXN0b3J5PWUuSGlzdG9yeXx8e30scD1lLmhpc3Rvcnk7dHJ5e3M9ZS5zZXNzaW9uU3RvcmFnZSxzLnNldEl0ZW0oXCJURVNUXCIsXCIxXCIpLHMucmVtb3ZlSXRlbShcIlRFU1RcIil9Y2F0Y2goZCl7cz0hMX1sLnN0cmluZ2lmeT1sLnN0cmluZ2lmeXx8bC5lbmNvZGUsbC5wYXJzZT1sLnBhcnNlfHxsLmRlY29kZTtpZih0eXBlb2YgaC5pbml0IT1cInVuZGVmaW5lZFwiKXRocm93IG5ldyBFcnJvcihcIkhpc3RvcnkuanMgQ29yZSBoYXMgYWxyZWFkeSBiZWVuIGxvYWRlZC4uLlwiKTtoLmluaXQ9ZnVuY3Rpb24oZSl7cmV0dXJuIHR5cGVvZiBoLkFkYXB0ZXI9PVwidW5kZWZpbmVkXCI/ITE6KHR5cGVvZiBoLmluaXRDb3JlIT1cInVuZGVmaW5lZFwiJiZoLmluaXRDb3JlKCksdHlwZW9mIGguaW5pdEh0bWw0IT1cInVuZGVmaW5lZFwiJiZoLmluaXRIdG1sNCgpLCEwKX0saC5pbml0Q29yZT1mdW5jdGlvbihkKXtpZih0eXBlb2YgaC5pbml0Q29yZS5pbml0aWFsaXplZCE9XCJ1bmRlZmluZWRcIilyZXR1cm4hMTtoLmluaXRDb3JlLmluaXRpYWxpemVkPSEwLGgub3B0aW9ucz1oLm9wdGlvbnN8fHt9LGgub3B0aW9ucy5oYXNoQ2hhbmdlSW50ZXJ2YWw9aC5vcHRpb25zLmhhc2hDaGFuZ2VJbnRlcnZhbHx8MTAwLGgub3B0aW9ucy5zYWZhcmlQb2xsSW50ZXJ2YWw9aC5vcHRpb25zLnNhZmFyaVBvbGxJbnRlcnZhbHx8NTAwLGgub3B0aW9ucy5kb3VibGVDaGVja0ludGVydmFsPWgub3B0aW9ucy5kb3VibGVDaGVja0ludGVydmFsfHw1MDAsaC5vcHRpb25zLmRpc2FibGVTdWlkPWgub3B0aW9ucy5kaXNhYmxlU3VpZHx8ITEsaC5vcHRpb25zLnN0b3JlSW50ZXJ2YWw9aC5vcHRpb25zLnN0b3JlSW50ZXJ2YWx8fDFlMyxoLm9wdGlvbnMuYnVzeURlbGF5PWgub3B0aW9ucy5idXN5RGVsYXl8fDI1MCxoLm9wdGlvbnMuZGVidWc9aC5vcHRpb25zLmRlYnVnfHwhMSxoLm9wdGlvbnMuaW5pdGlhbFRpdGxlPWgub3B0aW9ucy5pbml0aWFsVGl0bGV8fHIudGl0bGUsaC5vcHRpb25zLmh0bWw0TW9kZT1oLm9wdGlvbnMuaHRtbDRNb2RlfHwhMSxoLm9wdGlvbnMuZGVsYXlJbml0PWgub3B0aW9ucy5kZWxheUluaXR8fCExLGguaW50ZXJ2YWxMaXN0PVtdLGguY2xlYXJBbGxJbnRlcnZhbHM9ZnVuY3Rpb24oKXt2YXIgZSx0PWguaW50ZXJ2YWxMaXN0O2lmKHR5cGVvZiB0IT1cInVuZGVmaW5lZFwiJiZ0IT09bnVsbCl7Zm9yKGU9MDtlPHQubGVuZ3RoO2UrKylmKHRbZV0pO2guaW50ZXJ2YWxMaXN0PW51bGx9fSxoLmRlYnVnPWZ1bmN0aW9uKCl7KGgub3B0aW9ucy5kZWJ1Z3x8ITEpJiZoLmxvZy5hcHBseShoLGFyZ3VtZW50cyl9LGgubG9nPWZ1bmN0aW9uKCl7dmFyIGU9dHlwZW9mIG4hPVwidW5kZWZpbmVkXCImJnR5cGVvZiBuLmxvZyE9XCJ1bmRlZmluZWRcIiYmdHlwZW9mIG4ubG9nLmFwcGx5IT1cInVuZGVmaW5lZFwiLHQ9ci5nZXRFbGVtZW50QnlJZChcImxvZ1wiKSxpLHMsbyx1LGE7ZT8odT1BcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpLGk9dS5zaGlmdCgpLHR5cGVvZiBuLmRlYnVnIT1cInVuZGVmaW5lZFwiP24uZGVidWcuYXBwbHkobixbaSx1XSk6bi5sb2cuYXBwbHkobixbaSx1XSkpOmk9XCJcXG5cIithcmd1bWVudHNbMF0rXCJcXG5cIjtmb3Iocz0xLG89YXJndW1lbnRzLmxlbmd0aDtzPG87KytzKXthPWFyZ3VtZW50c1tzXTtpZih0eXBlb2YgYT09XCJvYmplY3RcIiYmdHlwZW9mIGwhPVwidW5kZWZpbmVkXCIpdHJ5e2E9bC5zdHJpbmdpZnkoYSl9Y2F0Y2goZil7fWkrPVwiXFxuXCIrYStcIlxcblwifXJldHVybiB0Pyh0LnZhbHVlKz1pK1wiXFxuLS0tLS1cXG5cIix0LnNjcm9sbFRvcD10LnNjcm9sbEhlaWdodC10LmNsaWVudEhlaWdodCk6ZXx8YyhpKSwhMH0saC5nZXRJbnRlcm5ldEV4cGxvcmVyTWFqb3JWZXJzaW9uPWZ1bmN0aW9uKCl7dmFyIGU9aC5nZXRJbnRlcm5ldEV4cGxvcmVyTWFqb3JWZXJzaW9uLmNhY2hlZD10eXBlb2YgaC5nZXRJbnRlcm5ldEV4cGxvcmVyTWFqb3JWZXJzaW9uLmNhY2hlZCE9XCJ1bmRlZmluZWRcIj9oLmdldEludGVybmV0RXhwbG9yZXJNYWpvclZlcnNpb24uY2FjaGVkOmZ1bmN0aW9uKCl7dmFyIGU9Myx0PXIuY3JlYXRlRWxlbWVudChcImRpdlwiKSxuPXQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJpXCIpO3doaWxlKCh0LmlubmVySFRNTD1cIjwhLS1baWYgZ3QgSUUgXCIrICsrZStcIl0+PGk+PC9pPjwhW2VuZGlmXS0tPlwiKSYmblswXSk7cmV0dXJuIGU+ND9lOiExfSgpO3JldHVybiBlfSxoLmlzSW50ZXJuZXRFeHBsb3Jlcj1mdW5jdGlvbigpe3ZhciBlPWguaXNJbnRlcm5ldEV4cGxvcmVyLmNhY2hlZD10eXBlb2YgaC5pc0ludGVybmV0RXhwbG9yZXIuY2FjaGVkIT1cInVuZGVmaW5lZFwiP2guaXNJbnRlcm5ldEV4cGxvcmVyLmNhY2hlZDpCb29sZWFuKGguZ2V0SW50ZXJuZXRFeHBsb3Jlck1ham9yVmVyc2lvbigpKTtyZXR1cm4gZX0saC5vcHRpb25zLmh0bWw0TW9kZT9oLmVtdWxhdGVkPXtwdXNoU3RhdGU6ITAsaGFzaENoYW5nZTohMH06aC5lbXVsYXRlZD17cHVzaFN0YXRlOiFCb29sZWFuKGUuaGlzdG9yeSYmZS5oaXN0b3J5LnB1c2hTdGF0ZSYmZS5oaXN0b3J5LnJlcGxhY2VTdGF0ZSYmIS8gTW9iaWxlXFwvKFsxLTddW2Etel18KDgoW2FiY2RlXXxmKDFbMC04XSkpKSkvaS50ZXN0KGkudXNlckFnZW50KSYmIS9BcHBsZVdlYktpdFxcLzUoWzAtMl18M1swLTJdKS9pLnRlc3QoaS51c2VyQWdlbnQpKSxoYXNoQ2hhbmdlOkJvb2xlYW4oIShcIm9uaGFzaGNoYW5nZVwiaW4gZXx8XCJvbmhhc2hjaGFuZ2VcImluIHIpfHxoLmlzSW50ZXJuZXRFeHBsb3JlcigpJiZoLmdldEludGVybmV0RXhwbG9yZXJNYWpvclZlcnNpb24oKTw4KX0saC5lbmFibGVkPSFoLmVtdWxhdGVkLnB1c2hTdGF0ZSxoLmJ1Z3M9e3NldEhhc2g6Qm9vbGVhbighaC5lbXVsYXRlZC5wdXNoU3RhdGUmJmkudmVuZG9yPT09XCJBcHBsZSBDb21wdXRlciwgSW5jLlwiJiYvQXBwbGVXZWJLaXRcXC81KFswLTJdfDNbMC0zXSkvLnRlc3QoaS51c2VyQWdlbnQpKSxzYWZhcmlQb2xsOkJvb2xlYW4oIWguZW11bGF0ZWQucHVzaFN0YXRlJiZpLnZlbmRvcj09PVwiQXBwbGUgQ29tcHV0ZXIsIEluYy5cIiYmL0FwcGxlV2ViS2l0XFwvNShbMC0yXXwzWzAtM10pLy50ZXN0KGkudXNlckFnZW50KSksaWVEb3VibGVDaGVjazpCb29sZWFuKGguaXNJbnRlcm5ldEV4cGxvcmVyKCkmJmguZ2V0SW50ZXJuZXRFeHBsb3Jlck1ham9yVmVyc2lvbigpPDgpLGhhc2hFc2NhcGU6Qm9vbGVhbihoLmlzSW50ZXJuZXRFeHBsb3JlcigpJiZoLmdldEludGVybmV0RXhwbG9yZXJNYWpvclZlcnNpb24oKTw3KX0saC5pc0VtcHR5T2JqZWN0PWZ1bmN0aW9uKGUpe2Zvcih2YXIgdCBpbiBlKWlmKGUuaGFzT3duUHJvcGVydHkodCkpcmV0dXJuITE7cmV0dXJuITB9LGguY2xvbmVPYmplY3Q9ZnVuY3Rpb24oZSl7dmFyIHQsbjtyZXR1cm4gZT8odD1sLnN0cmluZ2lmeShlKSxuPWwucGFyc2UodCkpOm49e30sbn0saC5nZXRSb290VXJsPWZ1bmN0aW9uKCl7dmFyIGU9ci5sb2NhdGlvbi5wcm90b2NvbCtcIi8vXCIrKHIubG9jYXRpb24uaG9zdG5hbWV8fHIubG9jYXRpb24uaG9zdCk7aWYoci5sb2NhdGlvbi5wb3J0fHwhMSllKz1cIjpcIityLmxvY2F0aW9uLnBvcnQ7cmV0dXJuIGUrPVwiL1wiLGV9LGguZ2V0QmFzZUhyZWY9ZnVuY3Rpb24oKXt2YXIgZT1yLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiYmFzZVwiKSx0PW51bGwsbj1cIlwiO3JldHVybiBlLmxlbmd0aD09PTEmJih0PWVbMF0sbj10LmhyZWYucmVwbGFjZSgvW15cXC9dKyQvLFwiXCIpKSxuPW4ucmVwbGFjZSgvXFwvKyQvLFwiXCIpLG4mJihuKz1cIi9cIiksbn0saC5nZXRCYXNlVXJsPWZ1bmN0aW9uKCl7dmFyIGU9aC5nZXRCYXNlSHJlZigpfHxoLmdldEJhc2VQYWdlVXJsKCl8fGguZ2V0Um9vdFVybCgpO3JldHVybiBlfSxoLmdldFBhZ2VVcmw9ZnVuY3Rpb24oKXt2YXIgZT1oLmdldFN0YXRlKCExLCExKSx0PShlfHx7fSkudXJsfHxoLmdldExvY2F0aW9uSHJlZigpLG47cmV0dXJuIG49dC5yZXBsYWNlKC9cXC8rJC8sXCJcIikucmVwbGFjZSgvW15cXC9dKyQvLGZ1bmN0aW9uKGUsdCxuKXtyZXR1cm4vXFwuLy50ZXN0KGUpP2U6ZStcIi9cIn0pLG59LGguZ2V0QmFzZVBhZ2VVcmw9ZnVuY3Rpb24oKXt2YXIgZT1oLmdldExvY2F0aW9uSHJlZigpLnJlcGxhY2UoL1sjXFw/XS4qLyxcIlwiKS5yZXBsYWNlKC9bXlxcL10rJC8sZnVuY3Rpb24oZSx0LG4pe3JldHVybi9bXlxcL10kLy50ZXN0KGUpP1wiXCI6ZX0pLnJlcGxhY2UoL1xcLyskLyxcIlwiKStcIi9cIjtyZXR1cm4gZX0saC5nZXRGdWxsVXJsPWZ1bmN0aW9uKGUsdCl7dmFyIG49ZSxyPWUuc3Vic3RyaW5nKDAsMSk7cmV0dXJuIHQ9dHlwZW9mIHQ9PVwidW5kZWZpbmVkXCI/ITA6dCwvW2Etel0rXFw6XFwvXFwvLy50ZXN0KGUpfHwocj09PVwiL1wiP249aC5nZXRSb290VXJsKCkrZS5yZXBsYWNlKC9eXFwvKy8sXCJcIik6cj09PVwiI1wiP249aC5nZXRQYWdlVXJsKCkucmVwbGFjZSgvIy4qLyxcIlwiKStlOnI9PT1cIj9cIj9uPWguZ2V0UGFnZVVybCgpLnJlcGxhY2UoL1tcXD8jXS4qLyxcIlwiKStlOnQ/bj1oLmdldEJhc2VVcmwoKStlLnJlcGxhY2UoL14oXFwuXFwvKSsvLFwiXCIpOm49aC5nZXRCYXNlUGFnZVVybCgpK2UucmVwbGFjZSgvXihcXC5cXC8pKy8sXCJcIikpLG4ucmVwbGFjZSgvXFwjJC8sXCJcIil9LGguZ2V0U2hvcnRVcmw9ZnVuY3Rpb24oZSl7dmFyIHQ9ZSxuPWguZ2V0QmFzZVVybCgpLHI9aC5nZXRSb290VXJsKCk7cmV0dXJuIGguZW11bGF0ZWQucHVzaFN0YXRlJiYodD10LnJlcGxhY2UobixcIlwiKSksdD10LnJlcGxhY2UocixcIi9cIiksaC5pc1RyYWRpdGlvbmFsQW5jaG9yKHQpJiYodD1cIi4vXCIrdCksdD10LnJlcGxhY2UoL14oXFwuXFwvKSsvZyxcIi4vXCIpLnJlcGxhY2UoL1xcIyQvLFwiXCIpLHR9LGguZ2V0TG9jYXRpb25IcmVmPWZ1bmN0aW9uKGUpe3JldHVybiBlPWV8fHIsZS5VUkw9PT1lLmxvY2F0aW9uLmhyZWY/ZS5sb2NhdGlvbi5ocmVmOmUubG9jYXRpb24uaHJlZj09PWRlY29kZVVSSUNvbXBvbmVudChlLlVSTCk/ZS5VUkw6ZS5sb2NhdGlvbi5oYXNoJiZkZWNvZGVVUklDb21wb25lbnQoZS5sb2NhdGlvbi5ocmVmLnJlcGxhY2UoL15bXiNdKy8sXCJcIikpPT09ZS5sb2NhdGlvbi5oYXNoP2UubG9jYXRpb24uaHJlZjplLlVSTC5pbmRleE9mKFwiI1wiKT09LTEmJmUubG9jYXRpb24uaHJlZi5pbmRleE9mKFwiI1wiKSE9LTE/ZS5sb2NhdGlvbi5ocmVmOmUuVVJMfHxlLmxvY2F0aW9uLmhyZWZ9LGguc3RvcmU9e30saC5pZFRvU3RhdGU9aC5pZFRvU3RhdGV8fHt9LGguc3RhdGVUb0lkPWguc3RhdGVUb0lkfHx7fSxoLnVybFRvSWQ9aC51cmxUb0lkfHx7fSxoLnN0b3JlZFN0YXRlcz1oLnN0b3JlZFN0YXRlc3x8W10saC5zYXZlZFN0YXRlcz1oLnNhdmVkU3RhdGVzfHxbXSxoLm5vcm1hbGl6ZVN0b3JlPWZ1bmN0aW9uKCl7aC5zdG9yZS5pZFRvU3RhdGU9aC5zdG9yZS5pZFRvU3RhdGV8fHt9LGguc3RvcmUudXJsVG9JZD1oLnN0b3JlLnVybFRvSWR8fHt9LGguc3RvcmUuc3RhdGVUb0lkPWguc3RvcmUuc3RhdGVUb0lkfHx7fX0saC5nZXRTdGF0ZT1mdW5jdGlvbihlLHQpe3R5cGVvZiBlPT1cInVuZGVmaW5lZFwiJiYoZT0hMCksdHlwZW9mIHQ9PVwidW5kZWZpbmVkXCImJih0PSEwKTt2YXIgbj1oLmdldExhc3RTYXZlZFN0YXRlKCk7cmV0dXJuIW4mJnQmJihuPWguY3JlYXRlU3RhdGVPYmplY3QoKSksZSYmKG49aC5jbG9uZU9iamVjdChuKSxuLnVybD1uLmNsZWFuVXJsfHxuLnVybCksbn0saC5nZXRJZEJ5U3RhdGU9ZnVuY3Rpb24oZSl7dmFyIHQ9aC5leHRyYWN0SWQoZS51cmwpLG47aWYoIXQpe249aC5nZXRTdGF0ZVN0cmluZyhlKTtpZih0eXBlb2YgaC5zdGF0ZVRvSWRbbl0hPVwidW5kZWZpbmVkXCIpdD1oLnN0YXRlVG9JZFtuXTtlbHNlIGlmKHR5cGVvZiBoLnN0b3JlLnN0YXRlVG9JZFtuXSE9XCJ1bmRlZmluZWRcIil0PWguc3RvcmUuc3RhdGVUb0lkW25dO2Vsc2V7Zm9yKDs7KXt0PShuZXcgRGF0ZSkuZ2V0VGltZSgpK1N0cmluZyhNYXRoLnJhbmRvbSgpKS5yZXBsYWNlKC9cXEQvZyxcIlwiKTtpZih0eXBlb2YgaC5pZFRvU3RhdGVbdF09PVwidW5kZWZpbmVkXCImJnR5cGVvZiBoLnN0b3JlLmlkVG9TdGF0ZVt0XT09XCJ1bmRlZmluZWRcIilicmVha31oLnN0YXRlVG9JZFtuXT10LGguaWRUb1N0YXRlW3RdPWV9fXJldHVybiB0fSxoLm5vcm1hbGl6ZVN0YXRlPWZ1bmN0aW9uKGUpe3ZhciB0LG47aWYoIWV8fHR5cGVvZiBlIT1cIm9iamVjdFwiKWU9e307aWYodHlwZW9mIGUubm9ybWFsaXplZCE9XCJ1bmRlZmluZWRcIilyZXR1cm4gZTtpZighZS5kYXRhfHx0eXBlb2YgZS5kYXRhIT1cIm9iamVjdFwiKWUuZGF0YT17fTtyZXR1cm4gdD17fSx0Lm5vcm1hbGl6ZWQ9ITAsdC50aXRsZT1lLnRpdGxlfHxcIlwiLHQudXJsPWguZ2V0RnVsbFVybChlLnVybD9lLnVybDpoLmdldExvY2F0aW9uSHJlZigpKSx0Lmhhc2g9aC5nZXRTaG9ydFVybCh0LnVybCksdC5kYXRhPWguY2xvbmVPYmplY3QoZS5kYXRhKSx0LmlkPWguZ2V0SWRCeVN0YXRlKHQpLHQuY2xlYW5Vcmw9dC51cmwucmVwbGFjZSgvXFw/P1xcJl9zdWlkLiovLFwiXCIpLHQudXJsPXQuY2xlYW5Vcmwsbj0haC5pc0VtcHR5T2JqZWN0KHQuZGF0YSksKHQudGl0bGV8fG4pJiZoLm9wdGlvbnMuZGlzYWJsZVN1aWQhPT0hMCYmKHQuaGFzaD1oLmdldFNob3J0VXJsKHQudXJsKS5yZXBsYWNlKC9cXD8/XFwmX3N1aWQuKi8sXCJcIiksL1xcPy8udGVzdCh0Lmhhc2gpfHwodC5oYXNoKz1cIj9cIiksdC5oYXNoKz1cIiZfc3VpZD1cIit0LmlkKSx0Lmhhc2hlZFVybD1oLmdldEZ1bGxVcmwodC5oYXNoKSwoaC5lbXVsYXRlZC5wdXNoU3RhdGV8fGguYnVncy5zYWZhcmlQb2xsKSYmaC5oYXNVcmxEdXBsaWNhdGUodCkmJih0LnVybD10Lmhhc2hlZFVybCksdH0saC5jcmVhdGVTdGF0ZU9iamVjdD1mdW5jdGlvbihlLHQsbil7dmFyIHI9e2RhdGE6ZSx0aXRsZTp0LHVybDpufTtyZXR1cm4gcj1oLm5vcm1hbGl6ZVN0YXRlKHIpLHJ9LGguZ2V0U3RhdGVCeUlkPWZ1bmN0aW9uKGUpe2U9U3RyaW5nKGUpO3ZhciBuPWguaWRUb1N0YXRlW2VdfHxoLnN0b3JlLmlkVG9TdGF0ZVtlXXx8dDtyZXR1cm4gbn0saC5nZXRTdGF0ZVN0cmluZz1mdW5jdGlvbihlKXt2YXIgdCxuLHI7cmV0dXJuIHQ9aC5ub3JtYWxpemVTdGF0ZShlKSxuPXtkYXRhOnQuZGF0YSx0aXRsZTplLnRpdGxlLHVybDplLnVybH0scj1sLnN0cmluZ2lmeShuKSxyfSxoLmdldFN0YXRlSWQ9ZnVuY3Rpb24oZSl7dmFyIHQsbjtyZXR1cm4gdD1oLm5vcm1hbGl6ZVN0YXRlKGUpLG49dC5pZCxufSxoLmdldEhhc2hCeVN0YXRlPWZ1bmN0aW9uKGUpe3ZhciB0LG47cmV0dXJuIHQ9aC5ub3JtYWxpemVTdGF0ZShlKSxuPXQuaGFzaCxufSxoLmV4dHJhY3RJZD1mdW5jdGlvbihlKXt2YXIgdCxuLHIsaTtyZXR1cm4gZS5pbmRleE9mKFwiI1wiKSE9LTE/aT1lLnNwbGl0KFwiI1wiKVswXTppPWUsbj0vKC4qKVxcJl9zdWlkPShbMC05XSspJC8uZXhlYyhpKSxyPW4/blsxXXx8ZTplLHQ9bj9TdHJpbmcoblsyXXx8XCJcIik6XCJcIix0fHwhMX0saC5pc1RyYWRpdGlvbmFsQW5jaG9yPWZ1bmN0aW9uKGUpe3ZhciB0PSEvW1xcL1xcP1xcLl0vLnRlc3QoZSk7cmV0dXJuIHR9LGguZXh0cmFjdFN0YXRlPWZ1bmN0aW9uKGUsdCl7dmFyIG49bnVsbCxyLGk7cmV0dXJuIHQ9dHx8ITEscj1oLmV4dHJhY3RJZChlKSxyJiYobj1oLmdldFN0YXRlQnlJZChyKSksbnx8KGk9aC5nZXRGdWxsVXJsKGUpLHI9aC5nZXRJZEJ5VXJsKGkpfHwhMSxyJiYobj1oLmdldFN0YXRlQnlJZChyKSksIW4mJnQmJiFoLmlzVHJhZGl0aW9uYWxBbmNob3IoZSkmJihuPWguY3JlYXRlU3RhdGVPYmplY3QobnVsbCxudWxsLGkpKSksbn0saC5nZXRJZEJ5VXJsPWZ1bmN0aW9uKGUpe3ZhciBuPWgudXJsVG9JZFtlXXx8aC5zdG9yZS51cmxUb0lkW2VdfHx0O3JldHVybiBufSxoLmdldExhc3RTYXZlZFN0YXRlPWZ1bmN0aW9uKCl7cmV0dXJuIGguc2F2ZWRTdGF0ZXNbaC5zYXZlZFN0YXRlcy5sZW5ndGgtMV18fHR9LGguZ2V0TGFzdFN0b3JlZFN0YXRlPWZ1bmN0aW9uKCl7cmV0dXJuIGguc3RvcmVkU3RhdGVzW2guc3RvcmVkU3RhdGVzLmxlbmd0aC0xXXx8dH0saC5oYXNVcmxEdXBsaWNhdGU9ZnVuY3Rpb24oZSl7dmFyIHQ9ITEsbjtyZXR1cm4gbj1oLmV4dHJhY3RTdGF0ZShlLnVybCksdD1uJiZuLmlkIT09ZS5pZCx0fSxoLnN0b3JlU3RhdGU9ZnVuY3Rpb24oZSl7cmV0dXJuIGgudXJsVG9JZFtlLnVybF09ZS5pZCxoLnN0b3JlZFN0YXRlcy5wdXNoKGguY2xvbmVPYmplY3QoZSkpLGV9LGguaXNMYXN0U2F2ZWRTdGF0ZT1mdW5jdGlvbihlKXt2YXIgdD0hMSxuLHIsaTtyZXR1cm4gaC5zYXZlZFN0YXRlcy5sZW5ndGgmJihuPWUuaWQscj1oLmdldExhc3RTYXZlZFN0YXRlKCksaT1yLmlkLHQ9bj09PWkpLHR9LGguc2F2ZVN0YXRlPWZ1bmN0aW9uKGUpe3JldHVybiBoLmlzTGFzdFNhdmVkU3RhdGUoZSk/ITE6KGguc2F2ZWRTdGF0ZXMucHVzaChoLmNsb25lT2JqZWN0KGUpKSwhMCl9LGguZ2V0U3RhdGVCeUluZGV4PWZ1bmN0aW9uKGUpe3ZhciB0PW51bGw7cmV0dXJuIHR5cGVvZiBlPT1cInVuZGVmaW5lZFwiP3Q9aC5zYXZlZFN0YXRlc1toLnNhdmVkU3RhdGVzLmxlbmd0aC0xXTplPDA/dD1oLnNhdmVkU3RhdGVzW2guc2F2ZWRTdGF0ZXMubGVuZ3RoK2VdOnQ9aC5zYXZlZFN0YXRlc1tlXSx0fSxoLmdldEN1cnJlbnRJbmRleD1mdW5jdGlvbigpe3ZhciBlPW51bGw7cmV0dXJuIGguc2F2ZWRTdGF0ZXMubGVuZ3RoPDE/ZT0wOmU9aC5zYXZlZFN0YXRlcy5sZW5ndGgtMSxlfSxoLmdldEhhc2g9ZnVuY3Rpb24oZSl7dmFyIHQ9aC5nZXRMb2NhdGlvbkhyZWYoZSksbjtyZXR1cm4gbj1oLmdldEhhc2hCeVVybCh0KSxufSxoLnVuZXNjYXBlSGFzaD1mdW5jdGlvbihlKXt2YXIgdD1oLm5vcm1hbGl6ZUhhc2goZSk7cmV0dXJuIHQ9ZGVjb2RlVVJJQ29tcG9uZW50KHQpLHR9LGgubm9ybWFsaXplSGFzaD1mdW5jdGlvbihlKXt2YXIgdD1lLnJlcGxhY2UoL1teI10qIy8sXCJcIikucmVwbGFjZSgvIy4qLyxcIlwiKTtyZXR1cm4gdH0saC5zZXRIYXNoPWZ1bmN0aW9uKGUsdCl7dmFyIG4saTtyZXR1cm4gdCE9PSExJiZoLmJ1c3koKT8oaC5wdXNoUXVldWUoe3Njb3BlOmgsY2FsbGJhY2s6aC5zZXRIYXNoLGFyZ3M6YXJndW1lbnRzLHF1ZXVlOnR9KSwhMSk6KGguYnVzeSghMCksbj1oLmV4dHJhY3RTdGF0ZShlLCEwKSxuJiYhaC5lbXVsYXRlZC5wdXNoU3RhdGU/aC5wdXNoU3RhdGUobi5kYXRhLG4udGl0bGUsbi51cmwsITEpOmguZ2V0SGFzaCgpIT09ZSYmKGguYnVncy5zZXRIYXNoPyhpPWguZ2V0UGFnZVVybCgpLGgucHVzaFN0YXRlKG51bGwsbnVsbCxpK1wiI1wiK2UsITEpKTpyLmxvY2F0aW9uLmhhc2g9ZSksaCl9LGguZXNjYXBlSGFzaD1mdW5jdGlvbih0KXt2YXIgbj1oLm5vcm1hbGl6ZUhhc2godCk7cmV0dXJuIG49ZS5lbmNvZGVVUklDb21wb25lbnQobiksaC5idWdzLmhhc2hFc2NhcGV8fChuPW4ucmVwbGFjZSgvXFwlMjEvZyxcIiFcIikucmVwbGFjZSgvXFwlMjYvZyxcIiZcIikucmVwbGFjZSgvXFwlM0QvZyxcIj1cIikucmVwbGFjZSgvXFwlM0YvZyxcIj9cIikpLG59LGguZ2V0SGFzaEJ5VXJsPWZ1bmN0aW9uKGUpe3ZhciB0PVN0cmluZyhlKS5yZXBsYWNlKC8oW14jXSopIz8oW14jXSopIz8oLiopLyxcIiQyXCIpO3JldHVybiB0PWgudW5lc2NhcGVIYXNoKHQpLHR9LGguc2V0VGl0bGU9ZnVuY3Rpb24oZSl7dmFyIHQ9ZS50aXRsZSxuO3R8fChuPWguZ2V0U3RhdGVCeUluZGV4KDApLG4mJm4udXJsPT09ZS51cmwmJih0PW4udGl0bGV8fGgub3B0aW9ucy5pbml0aWFsVGl0bGUpKTt0cnl7ci5nZXRFbGVtZW50c0J5VGFnTmFtZShcInRpdGxlXCIpWzBdLmlubmVySFRNTD10LnJlcGxhY2UoXCI8XCIsXCImbHQ7XCIpLnJlcGxhY2UoXCI+XCIsXCImZ3Q7XCIpLnJlcGxhY2UoXCIgJiBcIixcIiAmYW1wOyBcIil9Y2F0Y2goaSl7fXJldHVybiByLnRpdGxlPXQsaH0saC5xdWV1ZXM9W10saC5idXN5PWZ1bmN0aW9uKGUpe3R5cGVvZiBlIT1cInVuZGVmaW5lZFwiP2guYnVzeS5mbGFnPWU6dHlwZW9mIGguYnVzeS5mbGFnPT1cInVuZGVmaW5lZFwiJiYoaC5idXN5LmZsYWc9ITEpO2lmKCFoLmJ1c3kuZmxhZyl7dShoLmJ1c3kudGltZW91dCk7dmFyIHQ9ZnVuY3Rpb24oKXt2YXIgZSxuLHI7aWYoaC5idXN5LmZsYWcpcmV0dXJuO2ZvcihlPWgucXVldWVzLmxlbmd0aC0xO2U+PTA7LS1lKXtuPWgucXVldWVzW2VdO2lmKG4ubGVuZ3RoPT09MCljb250aW51ZTtyPW4uc2hpZnQoKSxoLmZpcmVRdWV1ZUl0ZW0ociksaC5idXN5LnRpbWVvdXQ9byh0LGgub3B0aW9ucy5idXN5RGVsYXkpfX07aC5idXN5LnRpbWVvdXQ9byh0LGgub3B0aW9ucy5idXN5RGVsYXkpfXJldHVybiBoLmJ1c3kuZmxhZ30saC5idXN5LmZsYWc9ITEsaC5maXJlUXVldWVJdGVtPWZ1bmN0aW9uKGUpe3JldHVybiBlLmNhbGxiYWNrLmFwcGx5KGUuc2NvcGV8fGgsZS5hcmdzfHxbXSl9LGgucHVzaFF1ZXVlPWZ1bmN0aW9uKGUpe3JldHVybiBoLnF1ZXVlc1tlLnF1ZXVlfHwwXT1oLnF1ZXVlc1tlLnF1ZXVlfHwwXXx8W10saC5xdWV1ZXNbZS5xdWV1ZXx8MF0ucHVzaChlKSxofSxoLnF1ZXVlPWZ1bmN0aW9uKGUsdCl7cmV0dXJuIHR5cGVvZiBlPT1cImZ1bmN0aW9uXCImJihlPXtjYWxsYmFjazplfSksdHlwZW9mIHQhPVwidW5kZWZpbmVkXCImJihlLnF1ZXVlPXQpLGguYnVzeSgpP2gucHVzaFF1ZXVlKGUpOmguZmlyZVF1ZXVlSXRlbShlKSxofSxoLmNsZWFyUXVldWU9ZnVuY3Rpb24oKXtyZXR1cm4gaC5idXN5LmZsYWc9ITEsaC5xdWV1ZXM9W10saH0saC5zdGF0ZUNoYW5nZWQ9ITEsaC5kb3VibGVDaGVja2VyPSExLGguZG91YmxlQ2hlY2tDb21wbGV0ZT1mdW5jdGlvbigpe3JldHVybiBoLnN0YXRlQ2hhbmdlZD0hMCxoLmRvdWJsZUNoZWNrQ2xlYXIoKSxofSxoLmRvdWJsZUNoZWNrQ2xlYXI9ZnVuY3Rpb24oKXtyZXR1cm4gaC5kb3VibGVDaGVja2VyJiYodShoLmRvdWJsZUNoZWNrZXIpLGguZG91YmxlQ2hlY2tlcj0hMSksaH0saC5kb3VibGVDaGVjaz1mdW5jdGlvbihlKXtyZXR1cm4gaC5zdGF0ZUNoYW5nZWQ9ITEsaC5kb3VibGVDaGVja0NsZWFyKCksaC5idWdzLmllRG91YmxlQ2hlY2smJihoLmRvdWJsZUNoZWNrZXI9byhmdW5jdGlvbigpe3JldHVybiBoLmRvdWJsZUNoZWNrQ2xlYXIoKSxoLnN0YXRlQ2hhbmdlZHx8ZSgpLCEwfSxoLm9wdGlvbnMuZG91YmxlQ2hlY2tJbnRlcnZhbCkpLGh9LGguc2FmYXJpU3RhdGVQb2xsPWZ1bmN0aW9uKCl7dmFyIHQ9aC5leHRyYWN0U3RhdGUoaC5nZXRMb2NhdGlvbkhyZWYoKSksbjtpZighaC5pc0xhc3RTYXZlZFN0YXRlKHQpKXJldHVybiBuPXQsbnx8KG49aC5jcmVhdGVTdGF0ZU9iamVjdCgpKSxoLkFkYXB0ZXIudHJpZ2dlcihlLFwicG9wc3RhdGVcIiksaDtyZXR1cm59LGguYmFjaz1mdW5jdGlvbihlKXtyZXR1cm4gZSE9PSExJiZoLmJ1c3koKT8oaC5wdXNoUXVldWUoe3Njb3BlOmgsY2FsbGJhY2s6aC5iYWNrLGFyZ3M6YXJndW1lbnRzLHF1ZXVlOmV9KSwhMSk6KGguYnVzeSghMCksaC5kb3VibGVDaGVjayhmdW5jdGlvbigpe2guYmFjayghMSl9KSxwLmdvKC0xKSwhMCl9LGguZm9yd2FyZD1mdW5jdGlvbihlKXtyZXR1cm4gZSE9PSExJiZoLmJ1c3koKT8oaC5wdXNoUXVldWUoe3Njb3BlOmgsY2FsbGJhY2s6aC5mb3J3YXJkLGFyZ3M6YXJndW1lbnRzLHF1ZXVlOmV9KSwhMSk6KGguYnVzeSghMCksaC5kb3VibGVDaGVjayhmdW5jdGlvbigpe2guZm9yd2FyZCghMSl9KSxwLmdvKDEpLCEwKX0saC5nbz1mdW5jdGlvbihlLHQpe3ZhciBuO2lmKGU+MClmb3Iobj0xO248PWU7KytuKWguZm9yd2FyZCh0KTtlbHNle2lmKCEoZTwwKSl0aHJvdyBuZXcgRXJyb3IoXCJIaXN0b3J5LmdvOiBIaXN0b3J5LmdvIHJlcXVpcmVzIGEgcG9zaXRpdmUgb3IgbmVnYXRpdmUgaW50ZWdlciBwYXNzZWQuXCIpO2ZvcihuPS0xO24+PWU7LS1uKWguYmFjayh0KX1yZXR1cm4gaH07aWYoaC5lbXVsYXRlZC5wdXNoU3RhdGUpe3ZhciB2PWZ1bmN0aW9uKCl7fTtoLnB1c2hTdGF0ZT1oLnB1c2hTdGF0ZXx8dixoLnJlcGxhY2VTdGF0ZT1oLnJlcGxhY2VTdGF0ZXx8dn1lbHNlIGgub25Qb3BTdGF0ZT1mdW5jdGlvbih0LG4pe3ZhciByPSExLGk9ITEscyxvO3JldHVybiBoLmRvdWJsZUNoZWNrQ29tcGxldGUoKSxzPWguZ2V0SGFzaCgpLHM/KG89aC5leHRyYWN0U3RhdGUoc3x8aC5nZXRMb2NhdGlvbkhyZWYoKSwhMCksbz9oLnJlcGxhY2VTdGF0ZShvLmRhdGEsby50aXRsZSxvLnVybCwhMSk6KGguQWRhcHRlci50cmlnZ2VyKGUsXCJhbmNob3JjaGFuZ2VcIiksaC5idXN5KCExKSksaC5leHBlY3RlZFN0YXRlSWQ9ITEsITEpOihyPWguQWRhcHRlci5leHRyYWN0RXZlbnREYXRhKFwic3RhdGVcIix0LG4pfHwhMSxyP2k9aC5nZXRTdGF0ZUJ5SWQocik6aC5leHBlY3RlZFN0YXRlSWQ/aT1oLmdldFN0YXRlQnlJZChoLmV4cGVjdGVkU3RhdGVJZCk6aT1oLmV4dHJhY3RTdGF0ZShoLmdldExvY2F0aW9uSHJlZigpKSxpfHwoaT1oLmNyZWF0ZVN0YXRlT2JqZWN0KG51bGwsbnVsbCxoLmdldExvY2F0aW9uSHJlZigpKSksaC5leHBlY3RlZFN0YXRlSWQ9ITEsaC5pc0xhc3RTYXZlZFN0YXRlKGkpPyhoLmJ1c3koITEpLCExKTooaC5zdG9yZVN0YXRlKGkpLGguc2F2ZVN0YXRlKGkpLGguc2V0VGl0bGUoaSksaC5BZGFwdGVyLnRyaWdnZXIoZSxcInN0YXRlY2hhbmdlXCIpLGguYnVzeSghMSksITApKX0saC5BZGFwdGVyLmJpbmQoZSxcInBvcHN0YXRlXCIsaC5vblBvcFN0YXRlKSxoLnB1c2hTdGF0ZT1mdW5jdGlvbih0LG4scixpKXtpZihoLmdldEhhc2hCeVVybChyKSYmaC5lbXVsYXRlZC5wdXNoU3RhdGUpdGhyb3cgbmV3IEVycm9yKFwiSGlzdG9yeS5qcyBkb2VzIG5vdCBzdXBwb3J0IHN0YXRlcyB3aXRoIGZyYWdlbWVudC1pZGVudGlmaWVycyAoaGFzaGVzL2FuY2hvcnMpLlwiKTtpZihpIT09ITEmJmguYnVzeSgpKXJldHVybiBoLnB1c2hRdWV1ZSh7c2NvcGU6aCxjYWxsYmFjazpoLnB1c2hTdGF0ZSxhcmdzOmFyZ3VtZW50cyxxdWV1ZTppfSksITE7aC5idXN5KCEwKTt2YXIgcz1oLmNyZWF0ZVN0YXRlT2JqZWN0KHQsbixyKTtyZXR1cm4gaC5pc0xhc3RTYXZlZFN0YXRlKHMpP2guYnVzeSghMSk6KGguc3RvcmVTdGF0ZShzKSxoLmV4cGVjdGVkU3RhdGVJZD1zLmlkLHAucHVzaFN0YXRlKHMuaWQscy50aXRsZSxzLnVybCksaC5BZGFwdGVyLnRyaWdnZXIoZSxcInBvcHN0YXRlXCIpKSwhMH0saC5yZXBsYWNlU3RhdGU9ZnVuY3Rpb24odCxuLHIsaSl7aWYoaC5nZXRIYXNoQnlVcmwocikmJmguZW11bGF0ZWQucHVzaFN0YXRlKXRocm93IG5ldyBFcnJvcihcIkhpc3RvcnkuanMgZG9lcyBub3Qgc3VwcG9ydCBzdGF0ZXMgd2l0aCBmcmFnZW1lbnQtaWRlbnRpZmllcnMgKGhhc2hlcy9hbmNob3JzKS5cIik7aWYoaSE9PSExJiZoLmJ1c3koKSlyZXR1cm4gaC5wdXNoUXVldWUoe3Njb3BlOmgsY2FsbGJhY2s6aC5yZXBsYWNlU3RhdGUsYXJnczphcmd1bWVudHMscXVldWU6aX0pLCExO2guYnVzeSghMCk7dmFyIHM9aC5jcmVhdGVTdGF0ZU9iamVjdCh0LG4scik7cmV0dXJuIGguaXNMYXN0U2F2ZWRTdGF0ZShzKT9oLmJ1c3koITEpOihoLnN0b3JlU3RhdGUocyksaC5leHBlY3RlZFN0YXRlSWQ9cy5pZCxwLnJlcGxhY2VTdGF0ZShzLmlkLHMudGl0bGUscy51cmwpLGguQWRhcHRlci50cmlnZ2VyKGUsXCJwb3BzdGF0ZVwiKSksITB9O2lmKHMpe3RyeXtoLnN0b3JlPWwucGFyc2Uocy5nZXRJdGVtKFwiSGlzdG9yeS5zdG9yZVwiKSl8fHt9fWNhdGNoKG0pe2guc3RvcmU9e319aC5ub3JtYWxpemVTdG9yZSgpfWVsc2UgaC5zdG9yZT17fSxoLm5vcm1hbGl6ZVN0b3JlKCk7aC5BZGFwdGVyLmJpbmQoZSxcInVubG9hZFwiLGguY2xlYXJBbGxJbnRlcnZhbHMpLGguc2F2ZVN0YXRlKGguc3RvcmVTdGF0ZShoLmV4dHJhY3RTdGF0ZShoLmdldExvY2F0aW9uSHJlZigpLCEwKSkpLHMmJihoLm9uVW5sb2FkPWZ1bmN0aW9uKCl7dmFyIGUsdCxuO3RyeXtlPWwucGFyc2Uocy5nZXRJdGVtKFwiSGlzdG9yeS5zdG9yZVwiKSl8fHt9fWNhdGNoKHIpe2U9e319ZS5pZFRvU3RhdGU9ZS5pZFRvU3RhdGV8fHt9LGUudXJsVG9JZD1lLnVybFRvSWR8fHt9LGUuc3RhdGVUb0lkPWUuc3RhdGVUb0lkfHx7fTtmb3IodCBpbiBoLmlkVG9TdGF0ZSl7aWYoIWguaWRUb1N0YXRlLmhhc093blByb3BlcnR5KHQpKWNvbnRpbnVlO2UuaWRUb1N0YXRlW3RdPWguaWRUb1N0YXRlW3RdfWZvcih0IGluIGgudXJsVG9JZCl7aWYoIWgudXJsVG9JZC5oYXNPd25Qcm9wZXJ0eSh0KSljb250aW51ZTtlLnVybFRvSWRbdF09aC51cmxUb0lkW3RdfWZvcih0IGluIGguc3RhdGVUb0lkKXtpZighaC5zdGF0ZVRvSWQuaGFzT3duUHJvcGVydHkodCkpY29udGludWU7ZS5zdGF0ZVRvSWRbdF09aC5zdGF0ZVRvSWRbdF19aC5zdG9yZT1lLGgubm9ybWFsaXplU3RvcmUoKSxuPWwuc3RyaW5naWZ5KGUpO3RyeXtzLnNldEl0ZW0oXCJIaXN0b3J5LnN0b3JlXCIsbil9Y2F0Y2goaSl7aWYoaS5jb2RlIT09RE9NRXhjZXB0aW9uLlFVT1RBX0VYQ0VFREVEX0VSUil0aHJvdyBpO3MubGVuZ3RoJiYocy5yZW1vdmVJdGVtKFwiSGlzdG9yeS5zdG9yZVwiKSxzLnNldEl0ZW0oXCJIaXN0b3J5LnN0b3JlXCIsbikpfX0saC5pbnRlcnZhbExpc3QucHVzaChhKGgub25VbmxvYWQsaC5vcHRpb25zLnN0b3JlSW50ZXJ2YWwpKSxoLkFkYXB0ZXIuYmluZChlLFwiYmVmb3JldW5sb2FkXCIsaC5vblVubG9hZCksaC5BZGFwdGVyLmJpbmQoZSxcInVubG9hZFwiLGgub25VbmxvYWQpKTtpZighaC5lbXVsYXRlZC5wdXNoU3RhdGUpe2guYnVncy5zYWZhcmlQb2xsJiZoLmludGVydmFsTGlzdC5wdXNoKGEoaC5zYWZhcmlTdGF0ZVBvbGwsaC5vcHRpb25zLnNhZmFyaVBvbGxJbnRlcnZhbCkpO2lmKGkudmVuZG9yPT09XCJBcHBsZSBDb21wdXRlciwgSW5jLlwifHwoaS5hcHBDb2RlTmFtZXx8XCJcIik9PT1cIk1vemlsbGFcIiloLkFkYXB0ZXIuYmluZChlLFwiaGFzaGNoYW5nZVwiLGZ1bmN0aW9uKCl7aC5BZGFwdGVyLnRyaWdnZXIoZSxcInBvcHN0YXRlXCIpfSksaC5nZXRIYXNoKCkmJmguQWRhcHRlci5vbkRvbUxvYWQoZnVuY3Rpb24oKXtoLkFkYXB0ZXIudHJpZ2dlcihlLFwiaGFzaGNoYW5nZVwiKX0pfX0sKCFoLm9wdGlvbnN8fCFoLm9wdGlvbnMuZGVsYXlJbml0KSYmaC5pbml0KCl9KHdpbmRvdylcclxuIiwiLyohXHJcbiAqIEJJREkgZW1iZWRkaW5nIHN1cHBvcnQgZm9yIGpRdWVyeS5pMThuXHJcbiAqXHJcbiAqIENvcHlyaWdodCAoQykgMjAxNSwgRGF2aWQgQ2hhblxyXG4gKlxyXG4gKiBUaGlzIGNvZGUgaXMgZHVhbCBsaWNlbnNlZCBHUEx2MiBvciBsYXRlciBhbmQgTUlULiBZb3UgZG9uJ3QgaGF2ZSB0byBkb1xyXG4gKiBhbnl0aGluZyBzcGVjaWFsIHRvIGNob29zZSBvbmUgbGljZW5zZSBvciB0aGUgb3RoZXIgYW5kIHlvdSBkb24ndCBoYXZlIHRvXHJcbiAqIG5vdGlmeSBhbnlvbmUgd2hpY2ggbGljZW5zZSB5b3UgYXJlIHVzaW5nLiBZb3UgYXJlIGZyZWUgdG8gdXNlIHRoaXMgY29kZVxyXG4gKiBpbiBjb21tZXJjaWFsIHByb2plY3RzIGFzIGxvbmcgYXMgdGhlIGNvcHlyaWdodCBoZWFkZXIgaXMgbGVmdCBpbnRhY3QuXHJcbiAqIFNlZSBmaWxlcyBHUEwtTElDRU5TRSBhbmQgTUlULUxJQ0VOU0UgZm9yIGRldGFpbHMuXHJcbiAqXHJcbiAqIEBsaWNlbmNlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbmNlIDIuMCBvciBsYXRlclxyXG4gKiBAbGljZW5jZSBNSVQgTGljZW5zZVxyXG4gKi9cclxuXHJcbiggZnVuY3Rpb24gKCAkICkge1xyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHR2YXIgc3Ryb25nRGlyUmVnRXhwO1xyXG5cclxuXHQvKipcclxuXHQgKiBNYXRjaGVzIHRoZSBmaXJzdCBzdHJvbmcgZGlyZWN0aW9uYWxpdHkgY29kZXBvaW50OlxyXG5cdCAqIC0gaW4gZ3JvdXAgMSBpZiBpdCBpcyBMVFJcclxuXHQgKiAtIGluIGdyb3VwIDIgaWYgaXQgaXMgUlRMXHJcblx0ICogRG9lcyBub3QgbWF0Y2ggaWYgdGhlcmUgaXMgbm8gc3Ryb25nIGRpcmVjdGlvbmFsaXR5IGNvZGVwb2ludC5cclxuXHQgKlxyXG5cdCAqIEdlbmVyYXRlZCBieSBVbmljb2RlSlMgKHNlZSB0b29scy9zdHJvbmdEaXIpIGZyb20gdGhlIFVDRDsgc2VlXHJcblx0ICogaHR0cHM6Ly9waGFicmljYXRvci53aWtpbWVkaWEub3JnL2RpZmZ1c2lvbi9HVUpTLyAuXHJcblx0ICovXHJcblx0c3Ryb25nRGlyUmVnRXhwID0gbmV3IFJlZ0V4cChcclxuXHRcdCcoPzonICtcclxuXHRcdFx0JygnICtcclxuXHRcdFx0XHQnW1xcdTAwNDEtXFx1MDA1YVxcdTAwNjEtXFx1MDA3YVxcdTAwYWFcXHUwMGI1XFx1MDBiYVxcdTAwYzAtXFx1MDBkNlxcdTAwZDgtXFx1MDBmNlxcdTAwZjgtXFx1MDJiOFxcdTAyYmItXFx1MDJjMVxcdTAyZDBcXHUwMmQxXFx1MDJlMC1cXHUwMmU0XFx1MDJlZVxcdTAzNzAtXFx1MDM3M1xcdTAzNzZcXHUwMzc3XFx1MDM3YS1cXHUwMzdkXFx1MDM3ZlxcdTAzODZcXHUwMzg4LVxcdTAzOGFcXHUwMzhjXFx1MDM4ZS1cXHUwM2ExXFx1MDNhMy1cXHUwM2Y1XFx1MDNmNy1cXHUwNDgyXFx1MDQ4YS1cXHUwNTJmXFx1MDUzMS1cXHUwNTU2XFx1MDU1OS1cXHUwNTVmXFx1MDU2MS1cXHUwNTg3XFx1MDU4OVxcdTA5MDMtXFx1MDkzOVxcdTA5M2JcXHUwOTNkLVxcdTA5NDBcXHUwOTQ5LVxcdTA5NGNcXHUwOTRlLVxcdTA5NTBcXHUwOTU4LVxcdTA5NjFcXHUwOTY0LVxcdTA5ODBcXHUwOTgyXFx1MDk4M1xcdTA5ODUtXFx1MDk4Y1xcdTA5OGZcXHUwOTkwXFx1MDk5My1cXHUwOWE4XFx1MDlhYS1cXHUwOWIwXFx1MDliMlxcdTA5YjYtXFx1MDliOVxcdTA5YmQtXFx1MDljMFxcdTA5YzdcXHUwOWM4XFx1MDljYlxcdTA5Y2NcXHUwOWNlXFx1MDlkN1xcdTA5ZGNcXHUwOWRkXFx1MDlkZi1cXHUwOWUxXFx1MDllNi1cXHUwOWYxXFx1MDlmNC1cXHUwOWZhXFx1MGEwM1xcdTBhMDUtXFx1MGEwYVxcdTBhMGZcXHUwYTEwXFx1MGExMy1cXHUwYTI4XFx1MGEyYS1cXHUwYTMwXFx1MGEzMlxcdTBhMzNcXHUwYTM1XFx1MGEzNlxcdTBhMzhcXHUwYTM5XFx1MGEzZS1cXHUwYTQwXFx1MGE1OS1cXHUwYTVjXFx1MGE1ZVxcdTBhNjYtXFx1MGE2ZlxcdTBhNzItXFx1MGE3NFxcdTBhODNcXHUwYTg1LVxcdTBhOGRcXHUwYThmLVxcdTBhOTFcXHUwYTkzLVxcdTBhYThcXHUwYWFhLVxcdTBhYjBcXHUwYWIyXFx1MGFiM1xcdTBhYjUtXFx1MGFiOVxcdTBhYmQtXFx1MGFjMFxcdTBhYzlcXHUwYWNiXFx1MGFjY1xcdTBhZDBcXHUwYWUwXFx1MGFlMVxcdTBhZTYtXFx1MGFmMFxcdTBhZjlcXHUwYjAyXFx1MGIwM1xcdTBiMDUtXFx1MGIwY1xcdTBiMGZcXHUwYjEwXFx1MGIxMy1cXHUwYjI4XFx1MGIyYS1cXHUwYjMwXFx1MGIzMlxcdTBiMzNcXHUwYjM1LVxcdTBiMzlcXHUwYjNkXFx1MGIzZVxcdTBiNDBcXHUwYjQ3XFx1MGI0OFxcdTBiNGJcXHUwYjRjXFx1MGI1N1xcdTBiNWNcXHUwYjVkXFx1MGI1Zi1cXHUwYjYxXFx1MGI2Ni1cXHUwYjc3XFx1MGI4M1xcdTBiODUtXFx1MGI4YVxcdTBiOGUtXFx1MGI5MFxcdTBiOTItXFx1MGI5NVxcdTBiOTlcXHUwYjlhXFx1MGI5Y1xcdTBiOWVcXHUwYjlmXFx1MGJhM1xcdTBiYTRcXHUwYmE4LVxcdTBiYWFcXHUwYmFlLVxcdTBiYjlcXHUwYmJlXFx1MGJiZlxcdTBiYzFcXHUwYmMyXFx1MGJjNi1cXHUwYmM4XFx1MGJjYS1cXHUwYmNjXFx1MGJkMFxcdTBiZDdcXHUwYmU2LVxcdTBiZjJcXHUwYzAxLVxcdTBjMDNcXHUwYzA1LVxcdTBjMGNcXHUwYzBlLVxcdTBjMTBcXHUwYzEyLVxcdTBjMjhcXHUwYzJhLVxcdTBjMzlcXHUwYzNkXFx1MGM0MS1cXHUwYzQ0XFx1MGM1OC1cXHUwYzVhXFx1MGM2MFxcdTBjNjFcXHUwYzY2LVxcdTBjNmZcXHUwYzdmXFx1MGM4MlxcdTBjODNcXHUwYzg1LVxcdTBjOGNcXHUwYzhlLVxcdTBjOTBcXHUwYzkyLVxcdTBjYThcXHUwY2FhLVxcdTBjYjNcXHUwY2I1LVxcdTBjYjlcXHUwY2JkLVxcdTBjYzRcXHUwY2M2LVxcdTBjYzhcXHUwY2NhXFx1MGNjYlxcdTBjZDVcXHUwY2Q2XFx1MGNkZVxcdTBjZTBcXHUwY2UxXFx1MGNlNi1cXHUwY2VmXFx1MGNmMVxcdTBjZjJcXHUwZDAyXFx1MGQwM1xcdTBkMDUtXFx1MGQwY1xcdTBkMGUtXFx1MGQxMFxcdTBkMTItXFx1MGQzYVxcdTBkM2QtXFx1MGQ0MFxcdTBkNDYtXFx1MGQ0OFxcdTBkNGEtXFx1MGQ0Y1xcdTBkNGVcXHUwZDU3XFx1MGQ1Zi1cXHUwZDYxXFx1MGQ2Ni1cXHUwZDc1XFx1MGQ3OS1cXHUwZDdmXFx1MGQ4MlxcdTBkODNcXHUwZDg1LVxcdTBkOTZcXHUwZDlhLVxcdTBkYjFcXHUwZGIzLVxcdTBkYmJcXHUwZGJkXFx1MGRjMC1cXHUwZGM2XFx1MGRjZi1cXHUwZGQxXFx1MGRkOC1cXHUwZGRmXFx1MGRlNi1cXHUwZGVmXFx1MGRmMi1cXHUwZGY0XFx1MGUwMS1cXHUwZTMwXFx1MGUzMlxcdTBlMzNcXHUwZTQwLVxcdTBlNDZcXHUwZTRmLVxcdTBlNWJcXHUwZTgxXFx1MGU4MlxcdTBlODRcXHUwZTg3XFx1MGU4OFxcdTBlOGFcXHUwZThkXFx1MGU5NC1cXHUwZTk3XFx1MGU5OS1cXHUwZTlmXFx1MGVhMS1cXHUwZWEzXFx1MGVhNVxcdTBlYTdcXHUwZWFhXFx1MGVhYlxcdTBlYWQtXFx1MGViMFxcdTBlYjJcXHUwZWIzXFx1MGViZFxcdTBlYzAtXFx1MGVjNFxcdTBlYzZcXHUwZWQwLVxcdTBlZDlcXHUwZWRjLVxcdTBlZGZcXHUwZjAwLVxcdTBmMTdcXHUwZjFhLVxcdTBmMzRcXHUwZjM2XFx1MGYzOFxcdTBmM2UtXFx1MGY0N1xcdTBmNDktXFx1MGY2Y1xcdTBmN2ZcXHUwZjg1XFx1MGY4OC1cXHUwZjhjXFx1MGZiZS1cXHUwZmM1XFx1MGZjNy1cXHUwZmNjXFx1MGZjZS1cXHUwZmRhXFx1MTAwMC1cXHUxMDJjXFx1MTAzMVxcdTEwMzhcXHUxMDNiXFx1MTAzY1xcdTEwM2YtXFx1MTA1N1xcdTEwNWEtXFx1MTA1ZFxcdTEwNjEtXFx1MTA3MFxcdTEwNzUtXFx1MTA4MVxcdTEwODNcXHUxMDg0XFx1MTA4Ny1cXHUxMDhjXFx1MTA4ZS1cXHUxMDljXFx1MTA5ZS1cXHUxMGM1XFx1MTBjN1xcdTEwY2RcXHUxMGQwLVxcdTEyNDhcXHUxMjRhLVxcdTEyNGRcXHUxMjUwLVxcdTEyNTZcXHUxMjU4XFx1MTI1YS1cXHUxMjVkXFx1MTI2MC1cXHUxMjg4XFx1MTI4YS1cXHUxMjhkXFx1MTI5MC1cXHUxMmIwXFx1MTJiMi1cXHUxMmI1XFx1MTJiOC1cXHUxMmJlXFx1MTJjMFxcdTEyYzItXFx1MTJjNVxcdTEyYzgtXFx1MTJkNlxcdTEyZDgtXFx1MTMxMFxcdTEzMTItXFx1MTMxNVxcdTEzMTgtXFx1MTM1YVxcdTEzNjAtXFx1MTM3Y1xcdTEzODAtXFx1MTM4ZlxcdTEzYTAtXFx1MTNmNVxcdTEzZjgtXFx1MTNmZFxcdTE0MDEtXFx1MTY3ZlxcdTE2ODEtXFx1MTY5YVxcdTE2YTAtXFx1MTZmOFxcdTE3MDAtXFx1MTcwY1xcdTE3MGUtXFx1MTcxMVxcdTE3MjAtXFx1MTczMVxcdTE3MzVcXHUxNzM2XFx1MTc0MC1cXHUxNzUxXFx1MTc2MC1cXHUxNzZjXFx1MTc2ZS1cXHUxNzcwXFx1MTc4MC1cXHUxN2IzXFx1MTdiNlxcdTE3YmUtXFx1MTdjNVxcdTE3YzdcXHUxN2M4XFx1MTdkNC1cXHUxN2RhXFx1MTdkY1xcdTE3ZTAtXFx1MTdlOVxcdTE4MTAtXFx1MTgxOVxcdTE4MjAtXFx1MTg3N1xcdTE4ODAtXFx1MThhOFxcdTE4YWFcXHUxOGIwLVxcdTE4ZjVcXHUxOTAwLVxcdTE5MWVcXHUxOTIzLVxcdTE5MjZcXHUxOTI5LVxcdTE5MmJcXHUxOTMwXFx1MTkzMVxcdTE5MzMtXFx1MTkzOFxcdTE5NDYtXFx1MTk2ZFxcdTE5NzAtXFx1MTk3NFxcdTE5ODAtXFx1MTlhYlxcdTE5YjAtXFx1MTljOVxcdTE5ZDAtXFx1MTlkYVxcdTFhMDAtXFx1MWExNlxcdTFhMTlcXHUxYTFhXFx1MWExZS1cXHUxYTU1XFx1MWE1N1xcdTFhNjFcXHUxYTYzXFx1MWE2NFxcdTFhNmQtXFx1MWE3MlxcdTFhODAtXFx1MWE4OVxcdTFhOTAtXFx1MWE5OVxcdTFhYTAtXFx1MWFhZFxcdTFiMDQtXFx1MWIzM1xcdTFiMzVcXHUxYjNiXFx1MWIzZC1cXHUxYjQxXFx1MWI0My1cXHUxYjRiXFx1MWI1MC1cXHUxYjZhXFx1MWI3NC1cXHUxYjdjXFx1MWI4Mi1cXHUxYmExXFx1MWJhNlxcdTFiYTdcXHUxYmFhXFx1MWJhZS1cXHUxYmU1XFx1MWJlN1xcdTFiZWEtXFx1MWJlY1xcdTFiZWVcXHUxYmYyXFx1MWJmM1xcdTFiZmMtXFx1MWMyYlxcdTFjMzRcXHUxYzM1XFx1MWMzYi1cXHUxYzQ5XFx1MWM0ZC1cXHUxYzdmXFx1MWNjMC1cXHUxY2M3XFx1MWNkM1xcdTFjZTFcXHUxY2U5LVxcdTFjZWNcXHUxY2VlLVxcdTFjZjNcXHUxY2Y1XFx1MWNmNlxcdTFkMDAtXFx1MWRiZlxcdTFlMDAtXFx1MWYxNVxcdTFmMTgtXFx1MWYxZFxcdTFmMjAtXFx1MWY0NVxcdTFmNDgtXFx1MWY0ZFxcdTFmNTAtXFx1MWY1N1xcdTFmNTlcXHUxZjViXFx1MWY1ZFxcdTFmNWYtXFx1MWY3ZFxcdTFmODAtXFx1MWZiNFxcdTFmYjYtXFx1MWZiY1xcdTFmYmVcXHUxZmMyLVxcdTFmYzRcXHUxZmM2LVxcdTFmY2NcXHUxZmQwLVxcdTFmZDNcXHUxZmQ2LVxcdTFmZGJcXHUxZmUwLVxcdTFmZWNcXHUxZmYyLVxcdTFmZjRcXHUxZmY2LVxcdTFmZmNcXHUyMDBlXFx1MjA3MVxcdTIwN2ZcXHUyMDkwLVxcdTIwOWNcXHUyMTAyXFx1MjEwN1xcdTIxMGEtXFx1MjExM1xcdTIxMTVcXHUyMTE5LVxcdTIxMWRcXHUyMTI0XFx1MjEyNlxcdTIxMjhcXHUyMTJhLVxcdTIxMmRcXHUyMTJmLVxcdTIxMzlcXHUyMTNjLVxcdTIxM2ZcXHUyMTQ1LVxcdTIxNDlcXHUyMTRlXFx1MjE0ZlxcdTIxNjAtXFx1MjE4OFxcdTIzMzYtXFx1MjM3YVxcdTIzOTVcXHUyNDljLVxcdTI0ZTlcXHUyNmFjXFx1MjgwMC1cXHUyOGZmXFx1MmMwMC1cXHUyYzJlXFx1MmMzMC1cXHUyYzVlXFx1MmM2MC1cXHUyY2U0XFx1MmNlYi1cXHUyY2VlXFx1MmNmMlxcdTJjZjNcXHUyZDAwLVxcdTJkMjVcXHUyZDI3XFx1MmQyZFxcdTJkMzAtXFx1MmQ2N1xcdTJkNmZcXHUyZDcwXFx1MmQ4MC1cXHUyZDk2XFx1MmRhMC1cXHUyZGE2XFx1MmRhOC1cXHUyZGFlXFx1MmRiMC1cXHUyZGI2XFx1MmRiOC1cXHUyZGJlXFx1MmRjMC1cXHUyZGM2XFx1MmRjOC1cXHUyZGNlXFx1MmRkMC1cXHUyZGQ2XFx1MmRkOC1cXHUyZGRlXFx1MzAwNS1cXHUzMDA3XFx1MzAyMS1cXHUzMDI5XFx1MzAyZVxcdTMwMmZcXHUzMDMxLVxcdTMwMzVcXHUzMDM4LVxcdTMwM2NcXHUzMDQxLVxcdTMwOTZcXHUzMDlkLVxcdTMwOWZcXHUzMGExLVxcdTMwZmFcXHUzMGZjLVxcdTMwZmZcXHUzMTA1LVxcdTMxMmRcXHUzMTMxLVxcdTMxOGVcXHUzMTkwLVxcdTMxYmFcXHUzMWYwLVxcdTMyMWNcXHUzMjIwLVxcdTMyNGZcXHUzMjYwLVxcdTMyN2JcXHUzMjdmLVxcdTMyYjBcXHUzMmMwLVxcdTMyY2JcXHUzMmQwLVxcdTMyZmVcXHUzMzAwLVxcdTMzNzZcXHUzMzdiLVxcdTMzZGRcXHUzM2UwLVxcdTMzZmVcXHUzNDAwLVxcdTRkYjVcXHU0ZTAwLVxcdTlmZDVcXHVhMDAwLVxcdWE0OGNcXHVhNGQwLVxcdWE2MGNcXHVhNjEwLVxcdWE2MmJcXHVhNjQwLVxcdWE2NmVcXHVhNjgwLVxcdWE2OWRcXHVhNmEwLVxcdWE2ZWZcXHVhNmYyLVxcdWE2ZjdcXHVhNzIyLVxcdWE3ODdcXHVhNzg5LVxcdWE3YWRcXHVhN2IwLVxcdWE3YjdcXHVhN2Y3LVxcdWE4MDFcXHVhODAzLVxcdWE4MDVcXHVhODA3LVxcdWE4MGFcXHVhODBjLVxcdWE4MjRcXHVhODI3XFx1YTgzMC1cXHVhODM3XFx1YTg0MC1cXHVhODczXFx1YTg4MC1cXHVhOGMzXFx1YThjZS1cXHVhOGQ5XFx1YThmMi1cXHVhOGZkXFx1YTkwMC1cXHVhOTI1XFx1YTkyZS1cXHVhOTQ2XFx1YTk1MlxcdWE5NTNcXHVhOTVmLVxcdWE5N2NcXHVhOTgzLVxcdWE5YjJcXHVhOWI0XFx1YTliNVxcdWE5YmFcXHVhOWJiXFx1YTliZC1cXHVhOWNkXFx1YTljZi1cXHVhOWQ5XFx1YTlkZS1cXHVhOWU0XFx1YTllNi1cXHVhOWZlXFx1YWEwMC1cXHVhYTI4XFx1YWEyZlxcdWFhMzBcXHVhYTMzXFx1YWEzNFxcdWFhNDAtXFx1YWE0MlxcdWFhNDQtXFx1YWE0YlxcdWFhNGRcXHVhYTUwLVxcdWFhNTlcXHVhYTVjLVxcdWFhN2JcXHVhYTdkLVxcdWFhYWZcXHVhYWIxXFx1YWFiNVxcdWFhYjZcXHVhYWI5LVxcdWFhYmRcXHVhYWMwXFx1YWFjMlxcdWFhZGItXFx1YWFlYlxcdWFhZWUtXFx1YWFmNVxcdWFiMDEtXFx1YWIwNlxcdWFiMDktXFx1YWIwZVxcdWFiMTEtXFx1YWIxNlxcdWFiMjAtXFx1YWIyNlxcdWFiMjgtXFx1YWIyZVxcdWFiMzAtXFx1YWI2NVxcdWFiNzAtXFx1YWJlNFxcdWFiZTZcXHVhYmU3XFx1YWJlOS1cXHVhYmVjXFx1YWJmMC1cXHVhYmY5XFx1YWMwMC1cXHVkN2EzXFx1ZDdiMC1cXHVkN2M2XFx1ZDdjYi1cXHVkN2ZiXFx1ZTAwMC1cXHVmYTZkXFx1ZmE3MC1cXHVmYWQ5XFx1ZmIwMC1cXHVmYjA2XFx1ZmIxMy1cXHVmYjE3XFx1ZmYyMS1cXHVmZjNhXFx1ZmY0MS1cXHVmZjVhXFx1ZmY2Ni1cXHVmZmJlXFx1ZmZjMi1cXHVmZmM3XFx1ZmZjYS1cXHVmZmNmXFx1ZmZkMi1cXHVmZmQ3XFx1ZmZkYS1cXHVmZmRjXXxcXHVkODAwW1xcdWRjMDAtXFx1ZGMwYl18XFx1ZDgwMFtcXHVkYzBkLVxcdWRjMjZdfFxcdWQ4MDBbXFx1ZGMyOC1cXHVkYzNhXXxcXHVkODAwXFx1ZGMzY3xcXHVkODAwXFx1ZGMzZHxcXHVkODAwW1xcdWRjM2YtXFx1ZGM0ZF18XFx1ZDgwMFtcXHVkYzUwLVxcdWRjNWRdfFxcdWQ4MDBbXFx1ZGM4MC1cXHVkY2ZhXXxcXHVkODAwXFx1ZGQwMHxcXHVkODAwXFx1ZGQwMnxcXHVkODAwW1xcdWRkMDctXFx1ZGQzM118XFx1ZDgwMFtcXHVkZDM3LVxcdWRkM2ZdfFxcdWQ4MDBbXFx1ZGRkMC1cXHVkZGZjXXxcXHVkODAwW1xcdWRlODAtXFx1ZGU5Y118XFx1ZDgwMFtcXHVkZWEwLVxcdWRlZDBdfFxcdWQ4MDBbXFx1ZGYwMC1cXHVkZjIzXXxcXHVkODAwW1xcdWRmMzAtXFx1ZGY0YV18XFx1ZDgwMFtcXHVkZjUwLVxcdWRmNzVdfFxcdWQ4MDBbXFx1ZGY4MC1cXHVkZjlkXXxcXHVkODAwW1xcdWRmOWYtXFx1ZGZjM118XFx1ZDgwMFtcXHVkZmM4LVxcdWRmZDVdfFxcdWQ4MDFbXFx1ZGMwMC1cXHVkYzlkXXxcXHVkODAxW1xcdWRjYTAtXFx1ZGNhOV18XFx1ZDgwMVtcXHVkZDAwLVxcdWRkMjddfFxcdWQ4MDFbXFx1ZGQzMC1cXHVkZDYzXXxcXHVkODAxXFx1ZGQ2ZnxcXHVkODAxW1xcdWRlMDAtXFx1ZGYzNl18XFx1ZDgwMVtcXHVkZjQwLVxcdWRmNTVdfFxcdWQ4MDFbXFx1ZGY2MC1cXHVkZjY3XXxcXHVkODA0XFx1ZGMwMHxcXHVkODA0W1xcdWRjMDItXFx1ZGMzN118XFx1ZDgwNFtcXHVkYzQ3LVxcdWRjNGRdfFxcdWQ4MDRbXFx1ZGM2Ni1cXHVkYzZmXXxcXHVkODA0W1xcdWRjODItXFx1ZGNiMl18XFx1ZDgwNFxcdWRjYjd8XFx1ZDgwNFxcdWRjYjh8XFx1ZDgwNFtcXHVkY2JiLVxcdWRjYzFdfFxcdWQ4MDRbXFx1ZGNkMC1cXHVkY2U4XXxcXHVkODA0W1xcdWRjZjAtXFx1ZGNmOV18XFx1ZDgwNFtcXHVkZDAzLVxcdWRkMjZdfFxcdWQ4MDRcXHVkZDJjfFxcdWQ4MDRbXFx1ZGQzNi1cXHVkZDQzXXxcXHVkODA0W1xcdWRkNTAtXFx1ZGQ3Ml18XFx1ZDgwNFtcXHVkZDc0LVxcdWRkNzZdfFxcdWQ4MDRbXFx1ZGQ4Mi1cXHVkZGI1XXxcXHVkODA0W1xcdWRkYmYtXFx1ZGRjOV18XFx1ZDgwNFxcdWRkY2R8XFx1ZDgwNFtcXHVkZGQwLVxcdWRkZGZdfFxcdWQ4MDRbXFx1ZGRlMS1cXHVkZGY0XXxcXHVkODA0W1xcdWRlMDAtXFx1ZGUxMV18XFx1ZDgwNFtcXHVkZTEzLVxcdWRlMmVdfFxcdWQ4MDRcXHVkZTMyfFxcdWQ4MDRcXHVkZTMzfFxcdWQ4MDRcXHVkZTM1fFxcdWQ4MDRbXFx1ZGUzOC1cXHVkZTNkXXxcXHVkODA0W1xcdWRlODAtXFx1ZGU4Nl18XFx1ZDgwNFxcdWRlODh8XFx1ZDgwNFtcXHVkZThhLVxcdWRlOGRdfFxcdWQ4MDRbXFx1ZGU4Zi1cXHVkZTlkXXxcXHVkODA0W1xcdWRlOWYtXFx1ZGVhOV18XFx1ZDgwNFtcXHVkZWIwLVxcdWRlZGVdfFxcdWQ4MDRbXFx1ZGVlMC1cXHVkZWUyXXxcXHVkODA0W1xcdWRlZjAtXFx1ZGVmOV18XFx1ZDgwNFxcdWRmMDJ8XFx1ZDgwNFxcdWRmMDN8XFx1ZDgwNFtcXHVkZjA1LVxcdWRmMGNdfFxcdWQ4MDRcXHVkZjBmfFxcdWQ4MDRcXHVkZjEwfFxcdWQ4MDRbXFx1ZGYxMy1cXHVkZjI4XXxcXHVkODA0W1xcdWRmMmEtXFx1ZGYzMF18XFx1ZDgwNFxcdWRmMzJ8XFx1ZDgwNFxcdWRmMzN8XFx1ZDgwNFtcXHVkZjM1LVxcdWRmMzldfFxcdWQ4MDRbXFx1ZGYzZC1cXHVkZjNmXXxcXHVkODA0W1xcdWRmNDEtXFx1ZGY0NF18XFx1ZDgwNFxcdWRmNDd8XFx1ZDgwNFxcdWRmNDh8XFx1ZDgwNFtcXHVkZjRiLVxcdWRmNGRdfFxcdWQ4MDRcXHVkZjUwfFxcdWQ4MDRcXHVkZjU3fFxcdWQ4MDRbXFx1ZGY1ZC1cXHVkZjYzXXxcXHVkODA1W1xcdWRjODAtXFx1ZGNiMl18XFx1ZDgwNVxcdWRjYjl8XFx1ZDgwNVtcXHVkY2JiLVxcdWRjYmVdfFxcdWQ4MDVcXHVkY2MxfFxcdWQ4MDVbXFx1ZGNjNC1cXHVkY2M3XXxcXHVkODA1W1xcdWRjZDAtXFx1ZGNkOV18XFx1ZDgwNVtcXHVkZDgwLVxcdWRkYjFdfFxcdWQ4MDVbXFx1ZGRiOC1cXHVkZGJiXXxcXHVkODA1XFx1ZGRiZXxcXHVkODA1W1xcdWRkYzEtXFx1ZGRkYl18XFx1ZDgwNVtcXHVkZTAwLVxcdWRlMzJdfFxcdWQ4MDVcXHVkZTNifFxcdWQ4MDVcXHVkZTNjfFxcdWQ4MDVcXHVkZTNlfFxcdWQ4MDVbXFx1ZGU0MS1cXHVkZTQ0XXxcXHVkODA1W1xcdWRlNTAtXFx1ZGU1OV18XFx1ZDgwNVtcXHVkZTgwLVxcdWRlYWFdfFxcdWQ4MDVcXHVkZWFjfFxcdWQ4MDVcXHVkZWFlfFxcdWQ4MDVcXHVkZWFmfFxcdWQ4MDVcXHVkZWI2fFxcdWQ4MDVbXFx1ZGVjMC1cXHVkZWM5XXxcXHVkODA1W1xcdWRmMDAtXFx1ZGYxOV18XFx1ZDgwNVxcdWRmMjB8XFx1ZDgwNVxcdWRmMjF8XFx1ZDgwNVxcdWRmMjZ8XFx1ZDgwNVtcXHVkZjMwLVxcdWRmM2ZdfFxcdWQ4MDZbXFx1ZGNhMC1cXHVkY2YyXXxcXHVkODA2XFx1ZGNmZnxcXHVkODA2W1xcdWRlYzAtXFx1ZGVmOF18XFx1ZDgwOFtcXHVkYzAwLVxcdWRmOTldfFxcdWQ4MDlbXFx1ZGMwMC1cXHVkYzZlXXxcXHVkODA5W1xcdWRjNzAtXFx1ZGM3NF18XFx1ZDgwOVtcXHVkYzgwLVxcdWRkNDNdfFxcdWQ4MGNbXFx1ZGMwMC1cXHVkZmZmXXxcXHVkODBkW1xcdWRjMDAtXFx1ZGMyZV18XFx1ZDgxMVtcXHVkYzAwLVxcdWRlNDZdfFxcdWQ4MWFbXFx1ZGMwMC1cXHVkZTM4XXxcXHVkODFhW1xcdWRlNDAtXFx1ZGU1ZV18XFx1ZDgxYVtcXHVkZTYwLVxcdWRlNjldfFxcdWQ4MWFcXHVkZTZlfFxcdWQ4MWFcXHVkZTZmfFxcdWQ4MWFbXFx1ZGVkMC1cXHVkZWVkXXxcXHVkODFhXFx1ZGVmNXxcXHVkODFhW1xcdWRmMDAtXFx1ZGYyZl18XFx1ZDgxYVtcXHVkZjM3LVxcdWRmNDVdfFxcdWQ4MWFbXFx1ZGY1MC1cXHVkZjU5XXxcXHVkODFhW1xcdWRmNWItXFx1ZGY2MV18XFx1ZDgxYVtcXHVkZjYzLVxcdWRmNzddfFxcdWQ4MWFbXFx1ZGY3ZC1cXHVkZjhmXXxcXHVkODFiW1xcdWRmMDAtXFx1ZGY0NF18XFx1ZDgxYltcXHVkZjUwLVxcdWRmN2VdfFxcdWQ4MWJbXFx1ZGY5My1cXHVkZjlmXXxcXHVkODJjXFx1ZGMwMHxcXHVkODJjXFx1ZGMwMXxcXHVkODJmW1xcdWRjMDAtXFx1ZGM2YV18XFx1ZDgyZltcXHVkYzcwLVxcdWRjN2NdfFxcdWQ4MmZbXFx1ZGM4MC1cXHVkYzg4XXxcXHVkODJmW1xcdWRjOTAtXFx1ZGM5OV18XFx1ZDgyZlxcdWRjOWN8XFx1ZDgyZlxcdWRjOWZ8XFx1ZDgzNFtcXHVkYzAwLVxcdWRjZjVdfFxcdWQ4MzRbXFx1ZGQwMC1cXHVkZDI2XXxcXHVkODM0W1xcdWRkMjktXFx1ZGQ2Nl18XFx1ZDgzNFtcXHVkZDZhLVxcdWRkNzJdfFxcdWQ4MzRcXHVkZDgzfFxcdWQ4MzRcXHVkZDg0fFxcdWQ4MzRbXFx1ZGQ4Yy1cXHVkZGE5XXxcXHVkODM0W1xcdWRkYWUtXFx1ZGRlOF18XFx1ZDgzNFtcXHVkZjYwLVxcdWRmNzFdfFxcdWQ4MzVbXFx1ZGMwMC1cXHVkYzU0XXxcXHVkODM1W1xcdWRjNTYtXFx1ZGM5Y118XFx1ZDgzNVxcdWRjOWV8XFx1ZDgzNVxcdWRjOWZ8XFx1ZDgzNVxcdWRjYTJ8XFx1ZDgzNVxcdWRjYTV8XFx1ZDgzNVxcdWRjYTZ8XFx1ZDgzNVtcXHVkY2E5LVxcdWRjYWNdfFxcdWQ4MzVbXFx1ZGNhZS1cXHVkY2I5XXxcXHVkODM1XFx1ZGNiYnxcXHVkODM1W1xcdWRjYmQtXFx1ZGNjM118XFx1ZDgzNVtcXHVkY2M1LVxcdWRkMDVdfFxcdWQ4MzVbXFx1ZGQwNy1cXHVkZDBhXXxcXHVkODM1W1xcdWRkMGQtXFx1ZGQxNF18XFx1ZDgzNVtcXHVkZDE2LVxcdWRkMWNdfFxcdWQ4MzVbXFx1ZGQxZS1cXHVkZDM5XXxcXHVkODM1W1xcdWRkM2ItXFx1ZGQzZV18XFx1ZDgzNVtcXHVkZDQwLVxcdWRkNDRdfFxcdWQ4MzVcXHVkZDQ2fFxcdWQ4MzVbXFx1ZGQ0YS1cXHVkZDUwXXxcXHVkODM1W1xcdWRkNTItXFx1ZGVhNV18XFx1ZDgzNVtcXHVkZWE4LVxcdWRlZGFdfFxcdWQ4MzVbXFx1ZGVkYy1cXHVkZjE0XXxcXHVkODM1W1xcdWRmMTYtXFx1ZGY0ZV18XFx1ZDgzNVtcXHVkZjUwLVxcdWRmODhdfFxcdWQ4MzVbXFx1ZGY4YS1cXHVkZmMyXXxcXHVkODM1W1xcdWRmYzQtXFx1ZGZjYl18XFx1ZDgzNltcXHVkYzAwLVxcdWRkZmZdfFxcdWQ4MzZbXFx1ZGUzNy1cXHVkZTNhXXxcXHVkODM2W1xcdWRlNmQtXFx1ZGU3NF18XFx1ZDgzNltcXHVkZTc2LVxcdWRlODNdfFxcdWQ4MzZbXFx1ZGU4NS1cXHVkZThiXXxcXHVkODNjW1xcdWRkMTAtXFx1ZGQyZV18XFx1ZDgzY1tcXHVkZDMwLVxcdWRkNjldfFxcdWQ4M2NbXFx1ZGQ3MC1cXHVkZDlhXXxcXHVkODNjW1xcdWRkZTYtXFx1ZGUwMl18XFx1ZDgzY1tcXHVkZTEwLVxcdWRlM2FdfFxcdWQ4M2NbXFx1ZGU0MC1cXHVkZTQ4XXxcXHVkODNjXFx1ZGU1MHxcXHVkODNjXFx1ZGU1MXxbXFx1ZDg0MC1cXHVkODY4XVtcXHVkYzAwLVxcdWRmZmZdfFxcdWQ4NjlbXFx1ZGMwMC1cXHVkZWQ2XXxcXHVkODY5W1xcdWRmMDAtXFx1ZGZmZl18W1xcdWQ4NmEtXFx1ZDg2Y11bXFx1ZGMwMC1cXHVkZmZmXXxcXHVkODZkW1xcdWRjMDAtXFx1ZGYzNF18XFx1ZDg2ZFtcXHVkZjQwLVxcdWRmZmZdfFxcdWQ4NmVbXFx1ZGMwMC1cXHVkYzFkXXxcXHVkODZlW1xcdWRjMjAtXFx1ZGZmZl18W1xcdWQ4NmYtXFx1ZDg3Ml1bXFx1ZGMwMC1cXHVkZmZmXXxcXHVkODczW1xcdWRjMDAtXFx1ZGVhMV18XFx1ZDg3ZVtcXHVkYzAwLVxcdWRlMWRdfFtcXHVkYjgwLVxcdWRiYmVdW1xcdWRjMDAtXFx1ZGZmZl18XFx1ZGJiZltcXHVkYzAwLVxcdWRmZmRdfFtcXHVkYmMwLVxcdWRiZmVdW1xcdWRjMDAtXFx1ZGZmZl18XFx1ZGJmZltcXHVkYzAwLVxcdWRmZmRdJyArXHJcblx0XHRcdCcpfCgnICtcclxuXHRcdFx0XHQnW1xcdTA1OTBcXHUwNWJlXFx1MDVjMFxcdTA1YzNcXHUwNWM2XFx1MDVjOC1cXHUwNWZmXFx1MDdjMC1cXHUwN2VhXFx1MDdmNFxcdTA3ZjVcXHUwN2ZhLVxcdTA4MTVcXHUwODFhXFx1MDgyNFxcdTA4MjhcXHUwODJlLVxcdTA4NThcXHUwODVjLVxcdTA4OWZcXHUyMDBmXFx1ZmIxZFxcdWZiMWYtXFx1ZmIyOFxcdWZiMmEtXFx1ZmI0ZlxcdTA2MDhcXHUwNjBiXFx1MDYwZFxcdTA2MWItXFx1MDY0YVxcdTA2NmQtXFx1MDY2ZlxcdTA2NzEtXFx1MDZkNVxcdTA2ZTVcXHUwNmU2XFx1MDZlZVxcdTA2ZWZcXHUwNmZhLVxcdTA3MTBcXHUwNzEyLVxcdTA3MmZcXHUwNzRiLVxcdTA3YTVcXHUwN2IxLVxcdTA3YmZcXHUwOGEwLVxcdTA4ZTJcXHVmYjUwLVxcdWZkM2RcXHVmZDQwLVxcdWZkY2ZcXHVmZGYwLVxcdWZkZmNcXHVmZGZlXFx1ZmRmZlxcdWZlNzAtXFx1ZmVmZV18XFx1ZDgwMltcXHVkYzAwLVxcdWRkMWVdfFxcdWQ4MDJbXFx1ZGQyMC1cXHVkZTAwXXxcXHVkODAyXFx1ZGUwNHxcXHVkODAyW1xcdWRlMDctXFx1ZGUwYl18XFx1ZDgwMltcXHVkZTEwLVxcdWRlMzddfFxcdWQ4MDJbXFx1ZGUzYi1cXHVkZTNlXXxcXHVkODAyW1xcdWRlNDAtXFx1ZGVlNF18XFx1ZDgwMltcXHVkZWU3LVxcdWRmMzhdfFxcdWQ4MDJbXFx1ZGY0MC1cXHVkZmZmXXxcXHVkODAzW1xcdWRjMDAtXFx1ZGU1Zl18XFx1ZDgwM1tcXHVkZTdmLVxcdWRmZmZdfFxcdWQ4M2FbXFx1ZGMwMC1cXHVkY2NmXXxcXHVkODNhW1xcdWRjZDctXFx1ZGZmZl18XFx1ZDgzYltcXHVkYzAwLVxcdWRkZmZdfFxcdWQ4M2JbXFx1ZGYwMC1cXHVkZmZmXXxcXHVkODNiW1xcdWRmMDAtXFx1ZGZmZl18XFx1ZDgzYltcXHVkZjAwLVxcdWRmZmZdfFxcdWQ4M2JbXFx1ZGYwMC1cXHVkZmZmXXxcXHVkODNiW1xcdWRmMDAtXFx1ZGZmZl18XFx1ZDgzYltcXHVkZjAwLVxcdWRmZmZdfFxcdWQ4M2JbXFx1ZGYwMC1cXHVkZmZmXXxcXHVkODNiW1xcdWRmMDAtXFx1ZGZmZl18XFx1ZDgzYltcXHVkZjAwLVxcdWRmZmZdfFxcdWQ4M2JbXFx1ZGYwMC1cXHVkZmZmXXxcXHVkODNiW1xcdWRmMDAtXFx1ZGZmZl18XFx1ZDgzYltcXHVkZjAwLVxcdWRmZmZdfFxcdWQ4M2JbXFx1ZGYwMC1cXHVkZmZmXXxcXHVkODNiW1xcdWRlMDAtXFx1ZGVlZl18XFx1ZDgzYltcXHVkZWYyLVxcdWRlZmZdJyArXHJcblx0XHRcdCcpJyArXHJcblx0XHQnKSdcclxuXHQpO1xyXG5cclxuXHQvKipcclxuXHQgKiBHZXRzIGRpcmVjdGlvbmFsaXR5IG9mIHRoZSBmaXJzdCBzdHJvbmdseSBkaXJlY3Rpb25hbCBjb2RlcG9pbnRcclxuXHQgKlxyXG5cdCAqIFRoaXMgaXMgdGhlIHJ1bGUgdGhlIEJJREkgYWxnb3JpdGhtIHVzZXMgdG8gZGV0ZXJtaW5lIHRoZSBkaXJlY3Rpb25hbGl0eSBvZlxyXG5cdCAqIHBhcmFncmFwaHMgKCBodHRwOi8vdW5pY29kZS5vcmcvcmVwb3J0cy90cjkvI1RoZV9QYXJhZ3JhcGhfTGV2ZWwgKSBhbmRcclxuXHQgKiBGU0kgaXNvbGF0ZXMgKCBodHRwOi8vdW5pY29kZS5vcmcvcmVwb3J0cy90cjkvI0V4cGxpY2l0X0RpcmVjdGlvbmFsX0lzb2xhdGVzICkuXHJcblx0ICpcclxuXHQgKiBUT0RPOiBEb2VzIG5vdCBoYW5kbGUgQklESSBjb250cm9sIGNoYXJhY3RlcnMgaW5zaWRlIHRoZSB0ZXh0LlxyXG5cdCAqIFRPRE86IERvZXMgbm90IGhhbmRsZSB1bmFsbG9jYXRlZCBjaGFyYWN0ZXJzLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtzdHJpbmd9IHRleHQgVGhlIHRleHQgZnJvbSB3aGljaCB0byBleHRyYWN0IGluaXRpYWwgZGlyZWN0aW9uYWxpdHkuXHJcblx0ICogQHJldHVybiB7c3RyaW5nfSBEaXJlY3Rpb25hbGl0eSAoZWl0aGVyICdsdHInIG9yICdydGwnKVxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIHN0cm9uZ0RpckZyb21Db250ZW50KCB0ZXh0ICkge1xyXG5cdFx0dmFyIG0gPSB0ZXh0Lm1hdGNoKCBzdHJvbmdEaXJSZWdFeHAgKTtcclxuXHRcdGlmICggIW0gKSB7XHJcblx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0fVxyXG5cdFx0aWYgKCBtWyAyIF0gPT09IHVuZGVmaW5lZCApIHtcclxuXHRcdFx0cmV0dXJuICdsdHInO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuICdydGwnO1xyXG5cdH1cclxuXHJcblx0JC5leHRlbmQoICQuaTE4bi5wYXJzZXIuZW1pdHRlciwge1xyXG5cdFx0LyoqXHJcblx0XHQgKiBXcmFwcyBhcmd1bWVudCB3aXRoIHVuaWNvZGUgY29udHJvbCBjaGFyYWN0ZXJzIGZvciBkaXJlY3Rpb25hbGl0eSBzYWZldHlcclxuXHRcdCAqXHJcblx0XHQgKiBUaGlzIHNvbHZlcyB0aGUgcHJvYmxlbSB3aGVyZSBkaXJlY3Rpb25hbGl0eS1uZXV0cmFsIGNoYXJhY3RlcnMgYXQgdGhlIGVkZ2Ugb2ZcclxuXHRcdCAqIHRoZSBhcmd1bWVudCBzdHJpbmcgZ2V0IGludGVycHJldGVkIHdpdGggdGhlIHdyb25nIGRpcmVjdGlvbmFsaXR5IGZyb20gdGhlXHJcblx0XHQgKiBlbmNsb3NpbmcgY29udGV4dCwgZ2l2aW5nIHJlbmRlcmluZ3MgdGhhdCBsb29rIGNvcnJ1cHRlZCBsaWtlIFwiKEJlbl8oV01GXCIuXHJcblx0XHQgKlxyXG5cdFx0ICogVGhlIHdyYXBwaW5nIGlzIExSRS4uLlBERiBvciBSTEUuLi5QREYsIGRlcGVuZGluZyBvbiB0aGUgZGV0ZWN0ZWRcclxuXHRcdCAqIGRpcmVjdGlvbmFsaXR5IG9mIHRoZSBhcmd1bWVudCBzdHJpbmcsIHVzaW5nIHRoZSBCSURJIGFsZ29yaXRobSdzIG93biBcIkZpcnN0XHJcblx0XHQgKiBzdHJvbmcgZGlyZWN0aW9uYWwgY29kZXBvaW50XCIgcnVsZS4gRXNzZW50aWFsbHksIHRoaXMgd29ya3Mgcm91bmQgdGhlIGZhY3QgdGhhdFxyXG5cdFx0ICogdGhlcmUgaXMgbm8gZW1iZWRkaW5nIGVxdWl2YWxlbnQgb2YgVSsyMDY4IEZTSSAoaXNvbGF0aW9uIHdpdGggaGV1cmlzdGljXHJcblx0XHQgKiBkaXJlY3Rpb24gaW5mZXJlbmNlKS4gVGhlIGxhdHRlciBpcyBjbGVhbmVyIGJ1dCBzdGlsbCBub3Qgd2lkZWx5IHN1cHBvcnRlZC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge3N0cmluZ1tdfSBub2RlcyBUaGUgdGV4dCBub2RlcyBmcm9tIHdoaWNoIHRvIHRha2UgdGhlIGZpcnN0IGl0ZW0uXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9IFdyYXBwZWQgU3RyaW5nIG9mIGNvbnRlbnQgYXMgbmVlZGVkLlxyXG5cdFx0ICovXHJcblx0XHRiaWRpOiBmdW5jdGlvbiAoIG5vZGVzICkge1xyXG5cdFx0XHR2YXIgZGlyID0gc3Ryb25nRGlyRnJvbUNvbnRlbnQoIG5vZGVzWyAwIF0gKTtcclxuXHRcdFx0aWYgKCBkaXIgPT09ICdsdHInICkge1xyXG5cdFx0XHRcdC8vIFdyYXAgaW4gTEVGVC1UTy1SSUdIVCBFTUJFRERJTkcgLi4uIFBPUCBESVJFQ1RJT05BTCBGT1JNQVRUSU5HXHJcblx0XHRcdFx0cmV0dXJuICdcXHUyMDJBJyArIG5vZGVzWyAwIF0gKyAnXFx1MjAyQyc7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKCBkaXIgPT09ICdydGwnICkge1xyXG5cdFx0XHRcdC8vIFdyYXAgaW4gUklHSFQtVE8tTEVGVCBFTUJFRERJTkcgLi4uIFBPUCBESVJFQ1RJT05BTCBGT1JNQVRUSU5HXHJcblx0XHRcdFx0cmV0dXJuICdcXHUyMDJCJyArIG5vZGVzWyAwIF0gKyAnXFx1MjAyQyc7XHJcblx0XHRcdH1cclxuXHRcdFx0Ly8gTm8gc3Ryb25nIGRpcmVjdGlvbmFsaXR5OiBkbyBub3Qgd3JhcFxyXG5cdFx0XHRyZXR1cm4gbm9kZXNbIDAgXTtcclxuXHRcdH1cclxuXHR9ICk7XHJcbn0oIGpRdWVyeSApICk7XHJcbiIsIi8qIVxyXG4gKiBqUXVlcnkgSW50ZXJuYXRpb25hbGl6YXRpb24gbGlicmFyeVxyXG4gKlxyXG4gKiBDb3B5cmlnaHQgKEMpIDIwMTEtMjAxMyBTYW50aG9zaCBUaG90dGluZ2FsLCBOZWlsIEthbmRhbGdhb25rYXJcclxuICpcclxuICoganF1ZXJ5LmkxOG4gaXMgZHVhbCBsaWNlbnNlZCBHUEx2MiBvciBsYXRlciBhbmQgTUlULiBZb3UgZG9uJ3QgaGF2ZSB0byBkb1xyXG4gKiBhbnl0aGluZyBzcGVjaWFsIHRvIGNob29zZSBvbmUgbGljZW5zZSBvciB0aGUgb3RoZXIgYW5kIHlvdSBkb24ndCBoYXZlIHRvXHJcbiAqIG5vdGlmeSBhbnlvbmUgd2hpY2ggbGljZW5zZSB5b3UgYXJlIHVzaW5nLiBZb3UgYXJlIGZyZWUgdG8gdXNlXHJcbiAqIFVuaXZlcnNhbExhbmd1YWdlU2VsZWN0b3IgaW4gY29tbWVyY2lhbCBwcm9qZWN0cyBhcyBsb25nIGFzIHRoZSBjb3B5cmlnaHRcclxuICogaGVhZGVyIGlzIGxlZnQgaW50YWN0LiBTZWUgZmlsZXMgR1BMLUxJQ0VOU0UgYW5kIE1JVC1MSUNFTlNFIGZvciBkZXRhaWxzLlxyXG4gKlxyXG4gKiBAbGljZW5jZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5jZSAyLjAgb3IgbGF0ZXJcclxuICogQGxpY2VuY2UgTUlUIExpY2Vuc2VcclxuICovXHJcblxyXG4oIGZ1bmN0aW9uICggJCApIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblxyXG5cdHZhciBNZXNzYWdlUGFyc2VyRW1pdHRlciA9IGZ1bmN0aW9uICgpIHtcclxuXHRcdHRoaXMubGFuZ3VhZ2UgPSAkLmkxOG4ubGFuZ3VhZ2VzWyBTdHJpbmcubG9jYWxlIF0gfHwgJC5pMThuLmxhbmd1YWdlc1sgJ2RlZmF1bHQnIF07XHJcblx0fTtcclxuXHJcblx0TWVzc2FnZVBhcnNlckVtaXR0ZXIucHJvdG90eXBlID0ge1xyXG5cdFx0Y29uc3RydWN0b3I6IE1lc3NhZ2VQYXJzZXJFbWl0dGVyLFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogKFdlIHB1dCB0aGlzIG1ldGhvZCBkZWZpbml0aW9uIGhlcmUsIGFuZCBub3QgaW4gcHJvdG90eXBlLCB0byBtYWtlXHJcblx0XHQgKiBzdXJlIGl0J3Mgbm90IG92ZXJ3cml0dGVuIGJ5IGFueSBtYWdpYy4pIFdhbGsgZW50aXJlIG5vZGUgc3RydWN0dXJlLFxyXG5cdFx0ICogYXBwbHlpbmcgcmVwbGFjZW1lbnRzIGFuZCB0ZW1wbGF0ZSBmdW5jdGlvbnMgd2hlbiBhcHByb3ByaWF0ZVxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7TWl4ZWR9IG5vZGUgYWJzdHJhY3Qgc3ludGF4IHRyZWUgKHRvcCBub2RlIG9yIHN1Ym5vZGUpXHJcblx0XHQgKiBAcGFyYW0ge0FycmF5fSByZXBsYWNlbWVudHMgZm9yICQxLCAkMiwgLi4uICRuXHJcblx0XHQgKiBAcmV0dXJuIHtNaXhlZH0gc2luZ2xlLXN0cmluZyBub2RlIG9yIGFycmF5IG9mIG5vZGVzIHN1aXRhYmxlIGZvclxyXG5cdFx0ICogIGpRdWVyeSBhcHBlbmRpbmcuXHJcblx0XHQgKi9cclxuXHRcdGVtaXQ6IGZ1bmN0aW9uICggbm9kZSwgcmVwbGFjZW1lbnRzICkge1xyXG5cdFx0XHR2YXIgcmV0LCBzdWJub2Rlcywgb3BlcmF0aW9uLFxyXG5cdFx0XHRcdG1lc3NhZ2VQYXJzZXJFbWl0dGVyID0gdGhpcztcclxuXHJcblx0XHRcdHN3aXRjaCAoIHR5cGVvZiBub2RlICkge1xyXG5cdFx0XHRcdGNhc2UgJ3N0cmluZyc6XHJcblx0XHRcdFx0Y2FzZSAnbnVtYmVyJzpcclxuXHRcdFx0XHRcdHJldCA9IG5vZGU7XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRjYXNlICdvYmplY3QnOlxyXG5cdFx0XHRcdC8vIG5vZGUgaXMgYW4gYXJyYXkgb2Ygbm9kZXNcclxuXHRcdFx0XHRcdHN1Ym5vZGVzID0gJC5tYXAoIG5vZGUuc2xpY2UoIDEgKSwgZnVuY3Rpb24gKCBuICkge1xyXG5cdFx0XHRcdFx0XHRyZXR1cm4gbWVzc2FnZVBhcnNlckVtaXR0ZXIuZW1pdCggbiwgcmVwbGFjZW1lbnRzICk7XHJcblx0XHRcdFx0XHR9ICk7XHJcblxyXG5cdFx0XHRcdFx0b3BlcmF0aW9uID0gbm9kZVsgMCBdLnRvTG93ZXJDYXNlKCk7XHJcblxyXG5cdFx0XHRcdFx0aWYgKCB0eXBlb2YgbWVzc2FnZVBhcnNlckVtaXR0ZXJbIG9wZXJhdGlvbiBdID09PSAnZnVuY3Rpb24nICkge1xyXG5cdFx0XHRcdFx0XHRyZXQgPSBtZXNzYWdlUGFyc2VyRW1pdHRlclsgb3BlcmF0aW9uIF0oIHN1Ym5vZGVzLCByZXBsYWNlbWVudHMgKTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdHRocm93IG5ldyBFcnJvciggJ3Vua25vd24gb3BlcmF0aW9uIFwiJyArIG9wZXJhdGlvbiArICdcIicgKTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRjYXNlICd1bmRlZmluZWQnOlxyXG5cdFx0XHRcdC8vIFBhcnNpbmcgdGhlIGVtcHR5IHN0cmluZyAoYXMgYW4gZW50aXJlIGV4cHJlc3Npb24sIG9yIGFzIGFcclxuXHRcdFx0XHQvLyBwYXJhbUV4cHJlc3Npb24gaW4gYSB0ZW1wbGF0ZSkgcmVzdWx0cyBpbiB1bmRlZmluZWRcclxuXHRcdFx0XHQvLyBQZXJoYXBzIGEgbW9yZSBjbGV2ZXIgcGFyc2VyIGNhbiBkZXRlY3QgdGhpcywgYW5kIHJldHVybiB0aGVcclxuXHRcdFx0XHQvLyBlbXB0eSBzdHJpbmc/IE9yIGlzIHRoYXQgdXNlZnVsIGluZm9ybWF0aW9uP1xyXG5cdFx0XHRcdC8vIFRoZSBsb2dpY2FsIHRoaW5nIGlzIHByb2JhYmx5IHRvIHJldHVybiB0aGUgZW1wdHkgc3RyaW5nIGhlcmVcclxuXHRcdFx0XHQvLyB3aGVuIHdlIGVuY291bnRlciB1bmRlZmluZWQuXHJcblx0XHRcdFx0XHRyZXQgPSAnJztcclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdGRlZmF1bHQ6XHJcblx0XHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoICd1bmV4cGVjdGVkIHR5cGUgaW4gQVNUOiAnICsgdHlwZW9mIG5vZGUgKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHJldDtcclxuXHRcdH0sXHJcblxyXG5cdFx0LyoqXHJcblx0XHQgKiBQYXJzaW5nIGhhcyBiZWVuIGFwcGxpZWQgZGVwdGgtZmlyc3Qgd2UgY2FuIGFzc3VtZSB0aGF0IGFsbCBub2Rlc1xyXG5cdFx0ICogaGVyZSBhcmUgc2luZ2xlIG5vZGVzIE11c3QgcmV0dXJuIGEgc2luZ2xlIG5vZGUgdG8gcGFyZW50cyAtLSBhXHJcblx0XHQgKiBqUXVlcnkgd2l0aCBzeW50aGV0aWMgc3BhbiBIb3dldmVyLCB1bndyYXAgYW55IG90aGVyIHN5bnRoZXRpYyBzcGFuc1xyXG5cdFx0ICogaW4gb3VyIGNoaWxkcmVuIGFuZCBwYXNzIHRoZW0gdXB3YXJkc1xyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7QXJyYXl9IG5vZGVzIE1peGVkLCBzb21lIHNpbmdsZSBub2Rlcywgc29tZSBhcnJheXMgb2Ygbm9kZXMuXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9XHJcblx0XHQgKi9cclxuXHRcdGNvbmNhdDogZnVuY3Rpb24gKCBub2RlcyApIHtcclxuXHRcdFx0dmFyIHJlc3VsdCA9ICcnO1xyXG5cclxuXHRcdFx0JC5lYWNoKCBub2RlcywgZnVuY3Rpb24gKCBpLCBub2RlICkge1xyXG5cdFx0XHRcdC8vIHN0cmluZ3MsIGludGVnZXJzLCBhbnl0aGluZyBlbHNlXHJcblx0XHRcdFx0cmVzdWx0ICs9IG5vZGU7XHJcblx0XHRcdH0gKTtcclxuXHJcblx0XHRcdHJldHVybiByZXN1bHQ7XHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogUmV0dXJuIGVzY2FwZWQgcmVwbGFjZW1lbnQgb2YgY29ycmVjdCBpbmRleCwgb3Igc3RyaW5nIGlmXHJcblx0XHQgKiB1bmF2YWlsYWJsZS4gTm90ZSB0aGF0IHdlIGV4cGVjdCB0aGUgcGFyc2VkIHBhcmFtZXRlciB0byBiZVxyXG5cdFx0ICogemVyby1iYXNlZC4gaS5lLiAkMSBzaG91bGQgaGF2ZSBiZWNvbWUgWyAwIF0uIGlmIHRoZSBzcGVjaWZpZWRcclxuXHRcdCAqIHBhcmFtZXRlciBpcyBub3QgZm91bmQgcmV0dXJuIHRoZSBzYW1lIHN0cmluZyAoZS5nLiBcIiQ5OVwiIC0+XHJcblx0XHQgKiBwYXJhbWV0ZXIgOTggLT4gbm90IGZvdW5kIC0+IHJldHVybiBcIiQ5OVwiICkgVE9ETyB0aHJvdyBlcnJvciBpZlxyXG5cdFx0ICogbm9kZXMubGVuZ3RoID4gMSA/XHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtBcnJheX0gbm9kZXMgT25lIGVsZW1lbnQsIGludGVnZXIsIG4gPj0gMFxyXG5cdFx0ICogQHBhcmFtIHtBcnJheX0gcmVwbGFjZW1lbnRzIGZvciAkMSwgJDIsIC4uLiAkblxyXG5cdFx0ICogQHJldHVybiB7c3RyaW5nfSByZXBsYWNlbWVudFxyXG5cdFx0ICovXHJcblx0XHRyZXBsYWNlOiBmdW5jdGlvbiAoIG5vZGVzLCByZXBsYWNlbWVudHMgKSB7XHJcblx0XHRcdHZhciBpbmRleCA9IHBhcnNlSW50KCBub2Rlc1sgMCBdLCAxMCApO1xyXG5cclxuXHRcdFx0aWYgKCBpbmRleCA8IHJlcGxhY2VtZW50cy5sZW5ndGggKSB7XHJcblx0XHRcdFx0Ly8gcmVwbGFjZW1lbnQgaXMgbm90IGEgc3RyaW5nLCBkb24ndCB0b3VjaCFcclxuXHRcdFx0XHRyZXR1cm4gcmVwbGFjZW1lbnRzWyBpbmRleCBdO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdC8vIGluZGV4IG5vdCBmb3VuZCwgZmFsbGJhY2sgdG8gZGlzcGxheWluZyB2YXJpYWJsZVxyXG5cdFx0XHRcdHJldHVybiAnJCcgKyAoIGluZGV4ICsgMSApO1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogVHJhbnNmb3JtIHBhcnNlZCBzdHJ1Y3R1cmUgaW50byBwbHVyYWxpemF0aW9uIG4uYi4gVGhlIGZpcnN0IG5vZGUgbWF5XHJcblx0XHQgKiBiZSBhIG5vbi1pbnRlZ2VyIChmb3IgaW5zdGFuY2UsIGEgc3RyaW5nIHJlcHJlc2VudGluZyBhbiBBcmFiaWNcclxuXHRcdCAqIG51bWJlcikuIFNvIGNvbnZlcnQgaXQgYmFjayB3aXRoIHRoZSBjdXJyZW50IGxhbmd1YWdlJ3NcclxuXHRcdCAqIGNvbnZlcnROdW1iZXIuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtBcnJheX0gbm9kZXMgTGlzdCBbIHtTdHJpbmd8TnVtYmVyfSwge1N0cmluZ30sIHtTdHJpbmd9IC4uLiBdXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9IHNlbGVjdGVkIHBsdXJhbGl6ZWQgZm9ybSBhY2NvcmRpbmcgdG8gY3VycmVudFxyXG5cdFx0ICogIGxhbmd1YWdlLlxyXG5cdFx0ICovXHJcblx0XHRwbHVyYWw6IGZ1bmN0aW9uICggbm9kZXMgKSB7XHJcblx0XHRcdHZhciBjb3VudCA9IHBhcnNlRmxvYXQoIHRoaXMubGFuZ3VhZ2UuY29udmVydE51bWJlciggbm9kZXNbIDAgXSwgMTAgKSApLFxyXG5cdFx0XHRcdGZvcm1zID0gbm9kZXMuc2xpY2UoIDEgKTtcclxuXHJcblx0XHRcdHJldHVybiBmb3Jtcy5sZW5ndGggPyB0aGlzLmxhbmd1YWdlLmNvbnZlcnRQbHVyYWwoIGNvdW50LCBmb3JtcyApIDogJyc7XHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogVHJhbnNmb3JtIHBhcnNlZCBzdHJ1Y3R1cmUgaW50byBnZW5kZXIgVXNhZ2VcclxuXHRcdCAqIHt7Z2VuZGVyOmdlbmRlcnxtYXNjdWxpbmV8ZmVtaW5pbmV8bmV1dHJhbH19LlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7QXJyYXl9IG5vZGVzIExpc3QgWyB7U3RyaW5nfSwge1N0cmluZ30sIHtTdHJpbmd9ICwge1N0cmluZ30gXVxyXG5cdFx0ICogQHJldHVybiB7c3RyaW5nfSBzZWxlY3RlZCBnZW5kZXIgZm9ybSBhY2NvcmRpbmcgdG8gY3VycmVudCBsYW5ndWFnZVxyXG5cdFx0ICovXHJcblx0XHRnZW5kZXI6IGZ1bmN0aW9uICggbm9kZXMgKSB7XHJcblx0XHRcdHZhciBnZW5kZXIgPSBub2Rlc1sgMCBdLFxyXG5cdFx0XHRcdGZvcm1zID0gbm9kZXMuc2xpY2UoIDEgKTtcclxuXHJcblx0XHRcdHJldHVybiB0aGlzLmxhbmd1YWdlLmdlbmRlciggZ2VuZGVyLCBmb3JtcyApO1xyXG5cdFx0fSxcclxuXHJcblx0XHQvKipcclxuXHRcdCAqIFRyYW5zZm9ybSBwYXJzZWQgc3RydWN0dXJlIGludG8gZ3JhbW1hciBjb252ZXJzaW9uLiBJbnZva2VkIGJ5XHJcblx0XHQgKiBwdXR0aW5nIHt7Z3JhbW1hcjpmb3JtfHdvcmR9fSBpbiBhIG1lc3NhZ2VcclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge0FycmF5fSBub2RlcyBMaXN0IFt7R3JhbW1hciBjYXNlIGVnOiBnZW5pdGl2ZX0sIHtTdHJpbmcgd29yZH1dXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9IHNlbGVjdGVkIGdyYW1tYXRpY2FsIGZvcm0gYWNjb3JkaW5nIHRvIGN1cnJlbnRcclxuXHRcdCAqICBsYW5ndWFnZS5cclxuXHRcdCAqL1xyXG5cdFx0Z3JhbW1hcjogZnVuY3Rpb24gKCBub2RlcyApIHtcclxuXHRcdFx0dmFyIGZvcm0gPSBub2Rlc1sgMCBdLFxyXG5cdFx0XHRcdHdvcmQgPSBub2Rlc1sgMSBdO1xyXG5cclxuXHRcdFx0cmV0dXJuIHdvcmQgJiYgZm9ybSAmJiB0aGlzLmxhbmd1YWdlLmNvbnZlcnRHcmFtbWFyKCB3b3JkLCBmb3JtICk7XHJcblx0XHR9XHJcblx0fTtcclxuXHJcblx0JC5leHRlbmQoICQuaTE4bi5wYXJzZXIuZW1pdHRlciwgbmV3IE1lc3NhZ2VQYXJzZXJFbWl0dGVyKCkgKTtcclxufSggalF1ZXJ5ICkgKTtcclxuIiwiLyohXHJcbiAqIGpRdWVyeSBJbnRlcm5hdGlvbmFsaXphdGlvbiBsaWJyYXJ5XHJcbiAqXHJcbiAqIENvcHlyaWdodCAoQykgMjAxMiBTYW50aG9zaCBUaG90dGluZ2FsXHJcbiAqXHJcbiAqIGpxdWVyeS5pMThuIGlzIGR1YWwgbGljZW5zZWQgR1BMdjIgb3IgbGF0ZXIgYW5kIE1JVC4gWW91IGRvbid0IGhhdmUgdG8gZG8gYW55dGhpbmcgc3BlY2lhbCB0b1xyXG4gKiBjaG9vc2Ugb25lIGxpY2Vuc2Ugb3IgdGhlIG90aGVyIGFuZCB5b3UgZG9uJ3QgaGF2ZSB0byBub3RpZnkgYW55b25lIHdoaWNoIGxpY2Vuc2UgeW91IGFyZSB1c2luZy5cclxuICogWW91IGFyZSBmcmVlIHRvIHVzZSBVbml2ZXJzYWxMYW5ndWFnZVNlbGVjdG9yIGluIGNvbW1lcmNpYWwgcHJvamVjdHMgYXMgbG9uZyBhcyB0aGUgY29weXJpZ2h0XHJcbiAqIGhlYWRlciBpcyBsZWZ0IGludGFjdC4gU2VlIGZpbGVzIEdQTC1MSUNFTlNFIGFuZCBNSVQtTElDRU5TRSBmb3IgZGV0YWlscy5cclxuICpcclxuICogQGxpY2VuY2UgR05VIEdlbmVyYWwgUHVibGljIExpY2VuY2UgMi4wIG9yIGxhdGVyXHJcbiAqIEBsaWNlbmNlIE1JVCBMaWNlbnNlXHJcbiAqL1xyXG4oIGZ1bmN0aW9uICggJCApIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblxyXG5cdCQuaTE4biA9ICQuaTE4biB8fCB7fTtcclxuXHQkLmV4dGVuZCggJC5pMThuLmZhbGxiYWNrcywge1xyXG5cdFx0YWI6IFsgJ3J1JyBdLFxyXG5cdFx0YWNlOiBbICdpZCcgXSxcclxuXHRcdGFsbjogWyAnc3EnIF0sXHJcblx0XHQvLyBOb3Qgc28gc3RhbmRhcmQgLSBhbHMgaXMgc3VwcG9zZWQgdG8gYmUgVG9zayBBbGJhbmlhbixcclxuXHRcdC8vIGJ1dCBpbiBXaWtpcGVkaWEgaXQncyB1c2VkIGZvciBhIEdlcm1hbmljIGxhbmd1YWdlLlxyXG5cdFx0YWxzOiBbICdnc3cnLCAnZGUnIF0sXHJcblx0XHRhbjogWyAnZXMnIF0sXHJcblx0XHRhbnA6IFsgJ2hpJyBdLFxyXG5cdFx0YXJuOiBbICdlcycgXSxcclxuXHRcdGFyejogWyAnYXInIF0sXHJcblx0XHRhdjogWyAncnUnIF0sXHJcblx0XHRheTogWyAnZXMnIF0sXHJcblx0XHRiYTogWyAncnUnIF0sXHJcblx0XHRiYXI6IFsgJ2RlJyBdLFxyXG5cdFx0J2JhdC1zbWcnOiBbICdzZ3MnLCAnbHQnIF0sXHJcblx0XHRiY2M6IFsgJ2ZhJyBdLFxyXG5cdFx0J2JlLXgtb2xkJzogWyAnYmUtdGFyYXNrJyBdLFxyXG5cdFx0Ymg6IFsgJ2JobycgXSxcclxuXHRcdGJqbjogWyAnaWQnIF0sXHJcblx0XHRibTogWyAnZnInIF0sXHJcblx0XHRicHk6IFsgJ2JuJyBdLFxyXG5cdFx0YnFpOiBbICdmYScgXSxcclxuXHRcdGJ1ZzogWyAnaWQnIF0sXHJcblx0XHQnY2JrLXphbSc6IFsgJ2VzJyBdLFxyXG5cdFx0Y2U6IFsgJ3J1JyBdLFxyXG5cdFx0Y3JoOiBbICdjcmgtbGF0bicgXSxcclxuXHRcdCdjcmgtY3lybCc6IFsgJ3J1JyBdLFxyXG5cdFx0Y3NiOiBbICdwbCcgXSxcclxuXHRcdGN2OiBbICdydScgXSxcclxuXHRcdCdkZS1hdCc6IFsgJ2RlJyBdLFxyXG5cdFx0J2RlLWNoJzogWyAnZGUnIF0sXHJcblx0XHQnZGUtZm9ybWFsJzogWyAnZGUnIF0sXHJcblx0XHRkc2I6IFsgJ2RlJyBdLFxyXG5cdFx0ZHRwOiBbICdtcycgXSxcclxuXHRcdGVnbDogWyAnaXQnIF0sXHJcblx0XHRlbWw6IFsgJ2l0JyBdLFxyXG5cdFx0ZmY6IFsgJ2ZyJyBdLFxyXG5cdFx0Zml0OiBbICdmaScgXSxcclxuXHRcdCdmaXUtdnJvJzogWyAndnJvJywgJ2V0JyBdLFxyXG5cdFx0ZnJjOiBbICdmcicgXSxcclxuXHRcdGZycDogWyAnZnInIF0sXHJcblx0XHRmcnI6IFsgJ2RlJyBdLFxyXG5cdFx0ZnVyOiBbICdpdCcgXSxcclxuXHRcdGdhZzogWyAndHInIF0sXHJcblx0XHRnYW46IFsgJ2dhbi1oYW50JywgJ3poLWhhbnQnLCAnemgtaGFucycgXSxcclxuXHRcdCdnYW4taGFucyc6IFsgJ3poLWhhbnMnIF0sXHJcblx0XHQnZ2FuLWhhbnQnOiBbICd6aC1oYW50JywgJ3poLWhhbnMnIF0sXHJcblx0XHRnbDogWyAncHQnIF0sXHJcblx0XHRnbGs6IFsgJ2ZhJyBdLFxyXG5cdFx0Z246IFsgJ2VzJyBdLFxyXG5cdFx0Z3N3OiBbICdkZScgXSxcclxuXHRcdGhpZjogWyAnaGlmLWxhdG4nIF0sXHJcblx0XHRoc2I6IFsgJ2RlJyBdLFxyXG5cdFx0aHQ6IFsgJ2ZyJyBdLFxyXG5cdFx0aWk6IFsgJ3poLWNuJywgJ3poLWhhbnMnIF0sXHJcblx0XHRpbmg6IFsgJ3J1JyBdLFxyXG5cdFx0aXU6IFsgJ2lrZS1jYW5zJyBdLFxyXG5cdFx0anV0OiBbICdkYScgXSxcclxuXHRcdGp2OiBbICdpZCcgXSxcclxuXHRcdGthYTogWyAna2stbGF0bicsICdray1jeXJsJyBdLFxyXG5cdFx0a2JkOiBbICdrYmQtY3lybCcgXSxcclxuXHRcdGtodzogWyAndXInIF0sXHJcblx0XHRraXU6IFsgJ3RyJyBdLFxyXG5cdFx0a2s6IFsgJ2trLWN5cmwnIF0sXHJcblx0XHQna2stYXJhYic6IFsgJ2trLWN5cmwnIF0sXHJcblx0XHQna2stbGF0bic6IFsgJ2trLWN5cmwnIF0sXHJcblx0XHQna2stY24nOiBbICdray1hcmFiJywgJ2trLWN5cmwnIF0sXHJcblx0XHQna2sta3onOiBbICdray1jeXJsJyBdLFxyXG5cdFx0J2trLXRyJzogWyAna2stbGF0bicsICdray1jeXJsJyBdLFxyXG5cdFx0a2w6IFsgJ2RhJyBdLFxyXG5cdFx0J2tvLWtwJzogWyAna28nIF0sXHJcblx0XHRrb2k6IFsgJ3J1JyBdLFxyXG5cdFx0a3JjOiBbICdydScgXSxcclxuXHRcdGtzOiBbICdrcy1hcmFiJyBdLFxyXG5cdFx0a3NoOiBbICdkZScgXSxcclxuXHRcdGt1OiBbICdrdS1sYXRuJyBdLFxyXG5cdFx0J2t1LWFyYWInOiBbICdja2InIF0sXHJcblx0XHRrdjogWyAncnUnIF0sXHJcblx0XHRsYWQ6IFsgJ2VzJyBdLFxyXG5cdFx0bGI6IFsgJ2RlJyBdLFxyXG5cdFx0bGJlOiBbICdydScgXSxcclxuXHRcdGxlejogWyAncnUnIF0sXHJcblx0XHRsaTogWyAnbmwnIF0sXHJcblx0XHRsaWo6IFsgJ2l0JyBdLFxyXG5cdFx0bGl2OiBbICdldCcgXSxcclxuXHRcdGxtbzogWyAnaXQnIF0sXHJcblx0XHRsbjogWyAnZnInIF0sXHJcblx0XHRsdGc6IFsgJ2x2JyBdLFxyXG5cdFx0bHp6OiBbICd0cicgXSxcclxuXHRcdG1haTogWyAnaGknIF0sXHJcblx0XHQnbWFwLWJtcyc6IFsgJ2p2JywgJ2lkJyBdLFxyXG5cdFx0bWc6IFsgJ2ZyJyBdLFxyXG5cdFx0bWhyOiBbICdydScgXSxcclxuXHRcdG1pbjogWyAnaWQnIF0sXHJcblx0XHRtbzogWyAncm8nIF0sXHJcblx0XHRtcmo6IFsgJ3J1JyBdLFxyXG5cdFx0bXdsOiBbICdwdCcgXSxcclxuXHRcdG15djogWyAncnUnIF0sXHJcblx0XHRtem46IFsgJ2ZhJyBdLFxyXG5cdFx0bmFoOiBbICdlcycgXSxcclxuXHRcdG5hcDogWyAnaXQnIF0sXHJcblx0XHRuZHM6IFsgJ2RlJyBdLFxyXG5cdFx0J25kcy1ubCc6IFsgJ25sJyBdLFxyXG5cdFx0J25sLWluZm9ybWFsJzogWyAnbmwnIF0sXHJcblx0XHRubzogWyAnbmInIF0sXHJcblx0XHRvczogWyAncnUnIF0sXHJcblx0XHRwY2Q6IFsgJ2ZyJyBdLFxyXG5cdFx0cGRjOiBbICdkZScgXSxcclxuXHRcdHBkdDogWyAnZGUnIF0sXHJcblx0XHRwZmw6IFsgJ2RlJyBdLFxyXG5cdFx0cG1zOiBbICdpdCcgXSxcclxuXHRcdHB0OiBbICdwdC1icicgXSxcclxuXHRcdCdwdC1icic6IFsgJ3B0JyBdLFxyXG5cdFx0cXU6IFsgJ2VzJyBdLFxyXG5cdFx0cXVnOiBbICdxdScsICdlcycgXSxcclxuXHRcdHJnbjogWyAnaXQnIF0sXHJcblx0XHRybXk6IFsgJ3JvJyBdLFxyXG5cdFx0J3JvYS1ydXAnOiBbICdydXAnIF0sXHJcblx0XHRydWU6IFsgJ3VrJywgJ3J1JyBdLFxyXG5cdFx0cnVxOiBbICdydXEtbGF0bicsICdybycgXSxcclxuXHRcdCdydXEtY3lybCc6IFsgJ21rJyBdLFxyXG5cdFx0J3J1cS1sYXRuJzogWyAncm8nIF0sXHJcblx0XHRzYTogWyAnaGknIF0sXHJcblx0XHRzYWg6IFsgJ3J1JyBdLFxyXG5cdFx0c2NuOiBbICdpdCcgXSxcclxuXHRcdHNnOiBbICdmcicgXSxcclxuXHRcdHNnczogWyAnbHQnIF0sXHJcblx0XHRzbGk6IFsgJ2RlJyBdLFxyXG5cdFx0c3I6IFsgJ3NyLWVjJyBdLFxyXG5cdFx0c3JuOiBbICdubCcgXSxcclxuXHRcdHN0cTogWyAnZGUnIF0sXHJcblx0XHRzdTogWyAnaWQnIF0sXHJcblx0XHRzemw6IFsgJ3BsJyBdLFxyXG5cdFx0dGN5OiBbICdrbicgXSxcclxuXHRcdHRnOiBbICd0Zy1jeXJsJyBdLFxyXG5cdFx0dHQ6IFsgJ3R0LWN5cmwnLCAncnUnIF0sXHJcblx0XHQndHQtY3lybCc6IFsgJ3J1JyBdLFxyXG5cdFx0dHk6IFsgJ2ZyJyBdLFxyXG5cdFx0dWRtOiBbICdydScgXSxcclxuXHRcdHVnOiBbICd1Zy1hcmFiJyBdLFxyXG5cdFx0dWs6IFsgJ3J1JyBdLFxyXG5cdFx0dmVjOiBbICdpdCcgXSxcclxuXHRcdHZlcDogWyAnZXQnIF0sXHJcblx0XHR2bHM6IFsgJ25sJyBdLFxyXG5cdFx0dm1mOiBbICdkZScgXSxcclxuXHRcdHZvdDogWyAnZmknIF0sXHJcblx0XHR2cm86IFsgJ2V0JyBdLFxyXG5cdFx0d2E6IFsgJ2ZyJyBdLFxyXG5cdFx0d286IFsgJ2ZyJyBdLFxyXG5cdFx0d3V1OiBbICd6aC1oYW5zJyBdLFxyXG5cdFx0eGFsOiBbICdydScgXSxcclxuXHRcdHhtZjogWyAna2EnIF0sXHJcblx0XHR5aTogWyAnaGUnIF0sXHJcblx0XHR6YTogWyAnemgtaGFucycgXSxcclxuXHRcdHplYTogWyAnbmwnIF0sXHJcblx0XHR6aDogWyAnemgtaGFucycgXSxcclxuXHRcdCd6aC1jbGFzc2ljYWwnOiBbICdsemgnIF0sXHJcblx0XHQnemgtY24nOiBbICd6aC1oYW5zJyBdLFxyXG5cdFx0J3poLWhhbnQnOiBbICd6aC1oYW5zJyBdLFxyXG5cdFx0J3poLWhrJzogWyAnemgtaGFudCcsICd6aC1oYW5zJyBdLFxyXG5cdFx0J3poLW1pbi1uYW4nOiBbICduYW4nIF0sXHJcblx0XHQnemgtbW8nOiBbICd6aC1oaycsICd6aC1oYW50JywgJ3poLWhhbnMnIF0sXHJcblx0XHQnemgtbXknOiBbICd6aC1zZycsICd6aC1oYW5zJyBdLFxyXG5cdFx0J3poLXNnJzogWyAnemgtaGFucycgXSxcclxuXHRcdCd6aC10dyc6IFsgJ3poLWhhbnQnLCAnemgtaGFucycgXSxcclxuXHRcdCd6aC15dWUnOiBbICd5dWUnIF1cclxuXHR9ICk7XHJcbn0oIGpRdWVyeSApICk7XHJcbiIsIi8qIVxyXG4gKiBqUXVlcnkgSW50ZXJuYXRpb25hbGl6YXRpb24gbGlicmFyeVxyXG4gKlxyXG4gKiBDb3B5cmlnaHQgKEMpIDIwMTIgU2FudGhvc2ggVGhvdHRpbmdhbFxyXG4gKlxyXG4gKiBqcXVlcnkuaTE4biBpcyBkdWFsIGxpY2Vuc2VkIEdQTHYyIG9yIGxhdGVyIGFuZCBNSVQuIFlvdSBkb24ndCBoYXZlIHRvIGRvXHJcbiAqIGFueXRoaW5nIHNwZWNpYWwgdG8gY2hvb3NlIG9uZSBsaWNlbnNlIG9yIHRoZSBvdGhlciBhbmQgeW91IGRvbid0IGhhdmUgdG9cclxuICogbm90aWZ5IGFueW9uZSB3aGljaCBsaWNlbnNlIHlvdSBhcmUgdXNpbmcuIFlvdSBhcmUgZnJlZSB0byB1c2VcclxuICogVW5pdmVyc2FsTGFuZ3VhZ2VTZWxlY3RvciBpbiBjb21tZXJjaWFsIHByb2plY3RzIGFzIGxvbmcgYXMgdGhlIGNvcHlyaWdodFxyXG4gKiBoZWFkZXIgaXMgbGVmdCBpbnRhY3QuIFNlZSBmaWxlcyBHUEwtTElDRU5TRSBhbmQgTUlULUxJQ0VOU0UgZm9yIGRldGFpbHMuXHJcbiAqXHJcbiAqIEBsaWNlbmNlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbmNlIDIuMCBvciBsYXRlclxyXG4gKiBAbGljZW5jZSBNSVQgTGljZW5zZVxyXG4gKi9cclxuXHJcbiggZnVuY3Rpb24gKCAkICkge1xyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHJcblx0dmFyIG5hdiwgSTE4TixcclxuXHRcdHNsaWNlID0gQXJyYXkucHJvdG90eXBlLnNsaWNlO1xyXG5cdC8qKlxyXG5cdCAqIEBjb25zdHJ1Y3RvclxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXHJcblx0ICovXHJcblx0STE4TiA9IGZ1bmN0aW9uICggb3B0aW9ucyApIHtcclxuXHRcdC8vIExvYWQgZGVmYXVsdHNcclxuXHRcdHRoaXMub3B0aW9ucyA9ICQuZXh0ZW5kKCB7fSwgSTE4Ti5kZWZhdWx0cywgb3B0aW9ucyApO1xyXG5cclxuXHRcdHRoaXMucGFyc2VyID0gdGhpcy5vcHRpb25zLnBhcnNlcjtcclxuXHRcdHRoaXMubG9jYWxlID0gdGhpcy5vcHRpb25zLmxvY2FsZTtcclxuXHRcdHRoaXMubWVzc2FnZVN0b3JlID0gdGhpcy5vcHRpb25zLm1lc3NhZ2VTdG9yZTtcclxuXHRcdHRoaXMubGFuZ3VhZ2VzID0ge307XHJcblxyXG5cdFx0dGhpcy5pbml0KCk7XHJcblx0fTtcclxuXHJcblx0STE4Ti5wcm90b3R5cGUgPSB7XHJcblx0XHQvKipcclxuXHRcdCAqIEluaXRpYWxpemUgYnkgbG9hZGluZyBsb2NhbGVzIGFuZCBzZXR0aW5nIHVwXHJcblx0XHQgKiBTdHJpbmcucHJvdG90eXBlLnRvTG9jYWxlU3RyaW5nIGFuZCBTdHJpbmcubG9jYWxlLlxyXG5cdFx0ICovXHJcblx0XHRpbml0OiBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdHZhciBpMThuID0gdGhpcztcclxuXHJcblx0XHRcdC8vIFNldCBsb2NhbGUgb2YgU3RyaW5nIGVudmlyb25tZW50XHJcblx0XHRcdFN0cmluZy5sb2NhbGUgPSBpMThuLmxvY2FsZTtcclxuXHJcblx0XHRcdC8vIE92ZXJyaWRlIFN0cmluZy5sb2NhbGVTdHJpbmcgbWV0aG9kXHJcblx0XHRcdFN0cmluZy5wcm90b3R5cGUudG9Mb2NhbGVTdHJpbmcgPSBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0dmFyIGxvY2FsZVBhcnRzLCBsb2NhbGVQYXJ0SW5kZXgsIHZhbHVlLCBsb2NhbGUsIGZhbGxiYWNrSW5kZXgsXHJcblx0XHRcdFx0XHR0cnlpbmdMb2NhbGUsIG1lc3NhZ2U7XHJcblxyXG5cdFx0XHRcdHZhbHVlID0gdGhpcy52YWx1ZU9mKCk7XHJcblx0XHRcdFx0bG9jYWxlID0gaTE4bi5sb2NhbGU7XHJcblx0XHRcdFx0ZmFsbGJhY2tJbmRleCA9IDA7XHJcblxyXG5cdFx0XHRcdHdoaWxlICggbG9jYWxlICkge1xyXG5cdFx0XHRcdFx0Ly8gSXRlcmF0ZSB0aHJvdWdoIGxvY2FsZXMgc3RhcnRpbmcgYXQgbW9zdC1zcGVjaWZpYyB1bnRpbFxyXG5cdFx0XHRcdFx0Ly8gbG9jYWxpemF0aW9uIGlzIGZvdW5kLiBBcyBpbiBmaS1MYXRuLUZJLCBmaS1MYXRuIGFuZCBmaS5cclxuXHRcdFx0XHRcdGxvY2FsZVBhcnRzID0gbG9jYWxlLnNwbGl0KCAnLScgKTtcclxuXHRcdFx0XHRcdGxvY2FsZVBhcnRJbmRleCA9IGxvY2FsZVBhcnRzLmxlbmd0aDtcclxuXHJcblx0XHRcdFx0XHRkbyB7XHJcblx0XHRcdFx0XHRcdHRyeWluZ0xvY2FsZSA9IGxvY2FsZVBhcnRzLnNsaWNlKCAwLCBsb2NhbGVQYXJ0SW5kZXggKS5qb2luKCAnLScgKTtcclxuXHRcdFx0XHRcdFx0bWVzc2FnZSA9IGkxOG4ubWVzc2FnZVN0b3JlLmdldCggdHJ5aW5nTG9jYWxlLCB2YWx1ZSApO1xyXG5cclxuXHRcdFx0XHRcdFx0aWYgKCBtZXNzYWdlICkge1xyXG5cdFx0XHRcdFx0XHRcdHJldHVybiBtZXNzYWdlO1xyXG5cdFx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0XHRsb2NhbGVQYXJ0SW5kZXgtLTtcclxuXHRcdFx0XHRcdH0gd2hpbGUgKCBsb2NhbGVQYXJ0SW5kZXggKTtcclxuXHJcblx0XHRcdFx0XHRpZiAoIGxvY2FsZSA9PT0gJ2VuJyApIHtcclxuXHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0bG9jYWxlID0gKCAkLmkxOG4uZmFsbGJhY2tzWyBpMThuLmxvY2FsZSBdICYmICQuaTE4bi5mYWxsYmFja3NbIGkxOG4ubG9jYWxlIF1bIGZhbGxiYWNrSW5kZXggXSApIHx8XHJcblx0XHRcdFx0XHRcdGkxOG4ub3B0aW9ucy5mYWxsYmFja0xvY2FsZTtcclxuXHRcdFx0XHRcdCQuaTE4bi5sb2coICdUcnlpbmcgZmFsbGJhY2sgbG9jYWxlIGZvciAnICsgaTE4bi5sb2NhbGUgKyAnOiAnICsgbG9jYWxlICsgJyAoJyArIHZhbHVlICsgJyknICk7XHJcblxyXG5cdFx0XHRcdFx0ZmFsbGJhY2tJbmRleCsrO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0Ly8ga2V5IG5vdCBmb3VuZFxyXG5cdFx0XHRcdHJldHVybiAnJztcclxuXHRcdFx0fTtcclxuXHRcdH0sXHJcblxyXG5cdFx0LypcclxuXHRcdCAqIERlc3Ryb3kgdGhlIGkxOG4gaW5zdGFuY2UuXHJcblx0XHQgKi9cclxuXHRcdGRlc3Ryb3k6IGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0JC5yZW1vdmVEYXRhKCBkb2N1bWVudCwgJ2kxOG4nICk7XHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogR2VuZXJhbCBtZXNzYWdlIGxvYWRpbmcgQVBJIFRoaXMgY2FuIHRha2UgYSBVUkwgc3RyaW5nIGZvclxyXG5cdFx0ICogdGhlIGpzb24gZm9ybWF0dGVkIG1lc3NhZ2VzLiBFeGFtcGxlOlxyXG5cdFx0ICogPGNvZGU+bG9hZCgncGF0aC90by9hbGxfbG9jYWxpemF0aW9ucy5qc29uJyk7PC9jb2RlPlxyXG5cdFx0ICpcclxuXHRcdCAqIFRvIGxvYWQgYSBsb2NhbGl6YXRpb24gZmlsZSBmb3IgYSBsb2NhbGU6XHJcblx0XHQgKiA8Y29kZT5cclxuXHRcdCAqIGxvYWQoJ3BhdGgvdG8vZGUtbWVzc2FnZXMuanNvbicsICdkZScgKTtcclxuXHRcdCAqIDwvY29kZT5cclxuXHRcdCAqXHJcblx0XHQgKiBUbyBsb2FkIGEgbG9jYWxpemF0aW9uIGZpbGUgZnJvbSBhIGRpcmVjdG9yeTpcclxuXHRcdCAqIDxjb2RlPlxyXG5cdFx0ICogbG9hZCgncGF0aC90by9pMThuL2RpcmVjdG9yeScsICdkZScgKTtcclxuXHRcdCAqIDwvY29kZT5cclxuXHRcdCAqIFRoZSBhYm92ZSBtZXRob2QgaGFzIHRoZSBhZHZhbnRhZ2Ugb2YgZmFsbGJhY2sgcmVzb2x1dGlvbi5cclxuXHRcdCAqIGllLCBpdCB3aWxsIGF1dG9tYXRpY2FsbHkgbG9hZCB0aGUgZmFsbGJhY2sgbG9jYWxlcyBmb3IgZGUuXHJcblx0XHQgKiBGb3IgbW9zdCB1c2VjYXNlcywgdGhpcyBpcyB0aGUgcmVjb21tZW5kZWQgbWV0aG9kLlxyXG5cdFx0ICogSXQgaXMgb3B0aW9uYWwgdG8gaGF2ZSB0cmFpbGluZyBzbGFzaCBhdCBlbmQuXHJcblx0XHQgKlxyXG5cdFx0ICogQSBkYXRhIG9iamVjdCBjb250YWluaW5nIG1lc3NhZ2Uga2V5LSBtZXNzYWdlIHRyYW5zbGF0aW9uIG1hcHBpbmdzXHJcblx0XHQgKiBjYW4gYWxzbyBiZSBwYXNzZWQuIEV4YW1wbGU6XHJcblx0XHQgKiA8Y29kZT5cclxuXHRcdCAqIGxvYWQoIHsgJ2hlbGxvJyA6ICdIZWxsbycgfSwgb3B0aW9uYWxMb2NhbGUgKTtcclxuXHRcdCAqIDwvY29kZT5cclxuXHRcdCAqXHJcblx0XHQgKiBBIHNvdXJjZSBtYXAgY29udGFpbmluZyBrZXktdmFsdWUgcGFpciBvZiBsYW5ndWFnZW5hbWUgYW5kIGxvY2F0aW9uc1xyXG5cdFx0ICogY2FuIGFsc28gYmUgcGFzc2VkLiBFeGFtcGxlOlxyXG5cdFx0ICogPGNvZGU+XHJcblx0XHQgKiBsb2FkKCB7XHJcblx0XHQgKiBibjogJ2kxOG4vYm4uanNvbicsXHJcblx0XHQgKiBoZTogJ2kxOG4vaGUuanNvbicsXHJcblx0XHQgKiBlbjogJ2kxOG4vZW4uanNvbidcclxuXHRcdCAqIH0gKVxyXG5cdFx0ICogPC9jb2RlPlxyXG5cdFx0ICpcclxuXHRcdCAqIElmIHRoZSBkYXRhIGFyZ3VtZW50IGlzIG51bGwvdW5kZWZpbmVkL2ZhbHNlLFxyXG5cdFx0ICogYWxsIGNhY2hlZCBtZXNzYWdlcyBmb3IgdGhlIGkxOG4gaW5zdGFuY2Ugd2lsbCBnZXQgcmVzZXQuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd8T2JqZWN0fSBzb3VyY2VcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBsb2NhbGUgTGFuZ3VhZ2UgdGFnXHJcblx0XHQgKiBAcmV0dXJuIHtqUXVlcnkuUHJvbWlzZX1cclxuXHRcdCAqL1xyXG5cdFx0bG9hZDogZnVuY3Rpb24gKCBzb3VyY2UsIGxvY2FsZSApIHtcclxuXHRcdFx0dmFyIGZhbGxiYWNrTG9jYWxlcywgbG9jSW5kZXgsIGZhbGxiYWNrTG9jYWxlLCBzb3VyY2VNYXAgPSB7fTtcclxuXHRcdFx0aWYgKCAhc291cmNlICYmICFsb2NhbGUgKSB7XHJcblx0XHRcdFx0c291cmNlID0gJ2kxOG4vJyArICQuaTE4bigpLmxvY2FsZSArICcuanNvbic7XHJcblx0XHRcdFx0bG9jYWxlID0gJC5pMThuKCkubG9jYWxlO1xyXG5cdFx0XHR9XHJcblx0XHRcdGlmICggdHlwZW9mIHNvdXJjZSA9PT0gJ3N0cmluZycgJiZcclxuXHRcdFx0XHQvLyBzb3VyY2UgZXh0ZW5zaW9uIHNob3VsZCBiZSBqc29uLCBidXQgY2FuIGhhdmUgcXVlcnkgcGFyYW1zIGFmdGVyIHRoYXQuXHJcblx0XHRcdFx0c291cmNlLnNwbGl0KCAnPycgKVsgMCBdLnNwbGl0KCAnLicgKS5wb3AoKSAhPT0gJ2pzb24nXHJcblx0XHRcdCkge1xyXG5cdFx0XHRcdC8vIExvYWQgc3BlY2lmaWVkIGxvY2FsZSB0aGVuIGNoZWNrIGZvciBmYWxsYmFja3Mgd2hlbiBkaXJlY3RvcnkgaXMgc3BlY2lmaWVkIGluIGxvYWQoKVxyXG5cdFx0XHRcdHNvdXJjZU1hcFsgbG9jYWxlIF0gPSBzb3VyY2UgKyAnLycgKyBsb2NhbGUgKyAnLmpzb24nO1xyXG5cdFx0XHRcdGZhbGxiYWNrTG9jYWxlcyA9ICggJC5pMThuLmZhbGxiYWNrc1sgbG9jYWxlIF0gfHwgW10gKVxyXG5cdFx0XHRcdFx0LmNvbmNhdCggdGhpcy5vcHRpb25zLmZhbGxiYWNrTG9jYWxlICk7XHJcblx0XHRcdFx0Zm9yICggbG9jSW5kZXggPSAwOyBsb2NJbmRleCA8IGZhbGxiYWNrTG9jYWxlcy5sZW5ndGg7IGxvY0luZGV4KysgKSB7XHJcblx0XHRcdFx0XHRmYWxsYmFja0xvY2FsZSA9IGZhbGxiYWNrTG9jYWxlc1sgbG9jSW5kZXggXTtcclxuXHRcdFx0XHRcdHNvdXJjZU1hcFsgZmFsbGJhY2tMb2NhbGUgXSA9IHNvdXJjZSArICcvJyArIGZhbGxiYWNrTG9jYWxlICsgJy5qc29uJztcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuIHRoaXMubG9hZCggc291cmNlTWFwICk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0cmV0dXJuIHRoaXMubWVzc2FnZVN0b3JlLmxvYWQoIHNvdXJjZSwgbG9jYWxlICk7XHJcblx0XHRcdH1cclxuXHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogRG9lcyBwYXJhbWV0ZXIgYW5kIG1hZ2ljIHdvcmQgc3Vic3RpdHV0aW9uLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgTWVzc2FnZSBrZXlcclxuXHRcdCAqIEBwYXJhbSB7QXJyYXl9IHBhcmFtZXRlcnMgTWVzc2FnZSBwYXJhbWV0ZXJzXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9XHJcblx0XHQgKi9cclxuXHRcdHBhcnNlOiBmdW5jdGlvbiAoIGtleSwgcGFyYW1ldGVycyApIHtcclxuXHRcdFx0dmFyIG1lc3NhZ2UgPSBrZXkudG9Mb2NhbGVTdHJpbmcoKTtcclxuXHRcdFx0Ly8gRklYTUU6IFRoaXMgY2hhbmdlcyB0aGUgc3RhdGUgb2YgdGhlIEkxOE4gb2JqZWN0LFxyXG5cdFx0XHQvLyBzaG91bGQgcHJvYmFibHkgbm90IGNoYW5nZSB0aGUgJ3RoaXMucGFyc2VyJyBidXQganVzdFxyXG5cdFx0XHQvLyBwYXNzIGl0IHRvIHRoZSBwYXJzZXIuXHJcblx0XHRcdHRoaXMucGFyc2VyLmxhbmd1YWdlID0gJC5pMThuLmxhbmd1YWdlc1sgJC5pMThuKCkubG9jYWxlIF0gfHwgJC5pMThuLmxhbmd1YWdlc1sgJ2RlZmF1bHQnIF07XHJcblx0XHRcdGlmICggbWVzc2FnZSA9PT0gJycgKSB7XHJcblx0XHRcdFx0bWVzc2FnZSA9IGtleTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdGhpcy5wYXJzZXIucGFyc2UoIG1lc3NhZ2UsIHBhcmFtZXRlcnMgKTtcclxuXHRcdH1cclxuXHR9O1xyXG5cclxuXHQvKipcclxuXHQgKiBQcm9jZXNzIGEgbWVzc2FnZSBmcm9tIHRoZSAkLkkxOE4gaW5zdGFuY2VcclxuXHQgKiBmb3IgdGhlIGN1cnJlbnQgZG9jdW1lbnQsIHN0b3JlZCBpbiBqUXVlcnkuZGF0YShkb2N1bWVudCkuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge3N0cmluZ30ga2V5IEtleSBvZiB0aGUgbWVzc2FnZS5cclxuXHQgKiBAcGFyYW0ge3N0cmluZ30gcGFyYW0xIFtwYXJhbS4uLl0gVmFyaWFkaWMgbGlzdCBvZiBwYXJhbWV0ZXJzIGZvciB7a2V5fS5cclxuXHQgKiBAcmV0dXJuIHtzdHJpbmd8JC5JMThOfSBQYXJzZWQgbWVzc2FnZSwgb3IgaWYgbm8ga2V5IHdhcyBnaXZlblxyXG5cdCAqIHRoZSBpbnN0YW5jZSBvZiAkLkkxOE4gaXMgcmV0dXJuZWQuXHJcblx0ICovXHJcblx0JC5pMThuID0gZnVuY3Rpb24gKCBrZXksIHBhcmFtMSApIHtcclxuXHRcdHZhciBwYXJhbWV0ZXJzLFxyXG5cdFx0XHRpMThuID0gJC5kYXRhKCBkb2N1bWVudCwgJ2kxOG4nICksXHJcblx0XHRcdG9wdGlvbnMgPSB0eXBlb2Yga2V5ID09PSAnb2JqZWN0JyAmJiBrZXk7XHJcblxyXG5cdFx0Ly8gSWYgdGhlIGxvY2FsZSBvcHRpb24gZm9yIHRoaXMgY2FsbCBpcyBkaWZmZXJlbnQgdGhlbiB0aGUgc2V0dXAgc28gZmFyLFxyXG5cdFx0Ly8gdXBkYXRlIGl0IGF1dG9tYXRpY2FsbHkuIFRoaXMgZG9lc24ndCBqdXN0IGNoYW5nZSB0aGUgY29udGV4dCBmb3IgdGhpc1xyXG5cdFx0Ly8gY2FsbCBidXQgZm9yIGFsbCBmdXR1cmUgY2FsbCBhcyB3ZWxsLlxyXG5cdFx0Ly8gSWYgdGhlcmUgaXMgbm8gaTE4biBzZXR1cCB5ZXQsIGRvbid0IGRvIHRoaXMuIEl0IHdpbGwgYmUgdGFrZW4gY2FyZSBvZlxyXG5cdFx0Ly8gYnkgdGhlIGBuZXcgSTE4TmAgY29uc3RydWN0aW9uIGJlbG93LlxyXG5cdFx0Ly8gTk9URTogSXQgc2hvdWxkIG9ubHkgY2hhbmdlIGxhbmd1YWdlIGZvciB0aGlzIG9uZSBjYWxsLlxyXG5cdFx0Ly8gVGhlbiBjYWNoZSBpbnN0YW5jZXMgb2YgSTE4TiBzb21ld2hlcmUuXHJcblx0XHRpZiAoIG9wdGlvbnMgJiYgb3B0aW9ucy5sb2NhbGUgJiYgaTE4biAmJiBpMThuLmxvY2FsZSAhPT0gb3B0aW9ucy5sb2NhbGUgKSB7XHJcblx0XHRcdFN0cmluZy5sb2NhbGUgPSBpMThuLmxvY2FsZSA9IG9wdGlvbnMubG9jYWxlO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmICggIWkxOG4gKSB7XHJcblx0XHRcdGkxOG4gPSBuZXcgSTE4Tiggb3B0aW9ucyApO1xyXG5cdFx0XHQkLmRhdGEoIGRvY3VtZW50LCAnaTE4bicsIGkxOG4gKTtcclxuXHRcdH1cclxuXHJcblx0XHRpZiAoIHR5cGVvZiBrZXkgPT09ICdzdHJpbmcnICkge1xyXG5cdFx0XHRpZiAoIHBhcmFtMSAhPT0gdW5kZWZpbmVkICkge1xyXG5cdFx0XHRcdHBhcmFtZXRlcnMgPSBzbGljZS5jYWxsKCBhcmd1bWVudHMsIDEgKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRwYXJhbWV0ZXJzID0gW107XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJldHVybiBpMThuLnBhcnNlKCBrZXksIHBhcmFtZXRlcnMgKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdC8vIEZJWE1FOiByZW1vdmUgdGhpcyBmZWF0dXJlL2J1Zy5cclxuXHRcdFx0cmV0dXJuIGkxOG47XHJcblx0XHR9XHJcblx0fTtcclxuXHJcblx0JC5mbi5pMThuID0gZnVuY3Rpb24gKCkge1xyXG5cdFx0dmFyIGkxOG4gPSAkLmRhdGEoIGRvY3VtZW50LCAnaTE4bicgKTtcclxuXHJcblx0XHRpZiAoICFpMThuICkge1xyXG5cdFx0XHRpMThuID0gbmV3IEkxOE4oKTtcclxuXHRcdFx0JC5kYXRhKCBkb2N1bWVudCwgJ2kxOG4nLCBpMThuICk7XHJcblx0XHR9XHJcblx0XHRTdHJpbmcubG9jYWxlID0gaTE4bi5sb2NhbGU7XHJcblx0XHRyZXR1cm4gdGhpcy5lYWNoKCBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdHZhciAkdGhpcyA9ICQoIHRoaXMgKSxcclxuXHRcdFx0XHRtZXNzYWdlS2V5ID0gJHRoaXMuZGF0YSggJ2kxOG4nICksXHJcblx0XHRcdFx0bEJyYWNrZXQsIHJCcmFja2V0LCB0eXBlLCBrZXk7XHJcblxyXG5cdFx0XHRpZiAoIG1lc3NhZ2VLZXkgKSB7XHJcblx0XHRcdFx0bEJyYWNrZXQgPSBtZXNzYWdlS2V5LmluZGV4T2YoICdbJyApO1xyXG5cdFx0XHRcdHJCcmFja2V0ID0gbWVzc2FnZUtleS5pbmRleE9mKCAnXScgKTtcclxuXHRcdFx0XHRpZiAoIGxCcmFja2V0ICE9PSAtMSAmJiByQnJhY2tldCAhPT0gLTEgJiYgbEJyYWNrZXQgPCByQnJhY2tldCApIHtcclxuXHRcdFx0XHRcdHR5cGUgPSBtZXNzYWdlS2V5LnNsaWNlKCBsQnJhY2tldCArIDEsIHJCcmFja2V0ICk7XHJcblx0XHRcdFx0XHRrZXkgPSBtZXNzYWdlS2V5LnNsaWNlKCByQnJhY2tldCArIDEgKTtcclxuXHRcdFx0XHRcdGlmICggdHlwZSA9PT0gJ2h0bWwnICkge1xyXG5cdFx0XHRcdFx0XHQkdGhpcy5odG1sKCBpMThuLnBhcnNlKCBrZXkgKSApO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0JHRoaXMuYXR0ciggdHlwZSwgaTE4bi5wYXJzZSgga2V5ICkgKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0JHRoaXMudGV4dCggaTE4bi5wYXJzZSggbWVzc2FnZUtleSApICk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdCR0aGlzLmZpbmQoICdbZGF0YS1pMThuXScgKS5pMThuKCk7XHJcblx0XHRcdH1cclxuXHRcdH0gKTtcclxuXHR9O1xyXG5cclxuXHRTdHJpbmcubG9jYWxlID0gU3RyaW5nLmxvY2FsZSB8fCAkKCAnaHRtbCcgKS5hdHRyKCAnbGFuZycgKTtcclxuXHJcblx0aWYgKCAhU3RyaW5nLmxvY2FsZSApIHtcclxuXHRcdGlmICggdHlwZW9mIHdpbmRvdy5uYXZpZ2F0b3IgIT09IHVuZGVmaW5lZCApIHtcclxuXHRcdFx0bmF2ID0gd2luZG93Lm5hdmlnYXRvcjtcclxuXHRcdFx0U3RyaW5nLmxvY2FsZSA9IG5hdi5sYW5ndWFnZSB8fCBuYXYudXNlckxhbmd1YWdlIHx8ICcnO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0U3RyaW5nLmxvY2FsZSA9ICcnO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0JC5pMThuLmxhbmd1YWdlcyA9IHt9O1xyXG5cdCQuaTE4bi5tZXNzYWdlU3RvcmUgPSAkLmkxOG4ubWVzc2FnZVN0b3JlIHx8IHt9O1xyXG5cdCQuaTE4bi5wYXJzZXIgPSB7XHJcblx0XHQvLyBUaGUgZGVmYXVsdCBwYXJzZXIgb25seSBoYW5kbGVzIHZhcmlhYmxlIHN1YnN0aXR1dGlvblxyXG5cdFx0cGFyc2U6IGZ1bmN0aW9uICggbWVzc2FnZSwgcGFyYW1ldGVycyApIHtcclxuXHRcdFx0cmV0dXJuIG1lc3NhZ2UucmVwbGFjZSggL1xcJChcXGQrKS9nLCBmdW5jdGlvbiAoIHN0ciwgbWF0Y2ggKSB7XHJcblx0XHRcdFx0dmFyIGluZGV4ID0gcGFyc2VJbnQoIG1hdGNoLCAxMCApIC0gMTtcclxuXHRcdFx0XHRyZXR1cm4gcGFyYW1ldGVyc1sgaW5kZXggXSAhPT0gdW5kZWZpbmVkID8gcGFyYW1ldGVyc1sgaW5kZXggXSA6ICckJyArIG1hdGNoO1xyXG5cdFx0XHR9ICk7XHJcblx0XHR9LFxyXG5cdFx0ZW1pdHRlcjoge31cclxuXHR9O1xyXG5cdCQuaTE4bi5mYWxsYmFja3MgPSB7fTtcclxuXHQkLmkxOG4uZGVidWcgPSBmYWxzZTtcclxuXHQkLmkxOG4ubG9nID0gZnVuY3Rpb24gKCAvKiBhcmd1bWVudHMgKi8gKSB7XHJcblx0XHRpZiAoIHdpbmRvdy5jb25zb2xlICYmICQuaTE4bi5kZWJ1ZyApIHtcclxuXHRcdFx0d2luZG93LmNvbnNvbGUubG9nLmFwcGx5KCB3aW5kb3cuY29uc29sZSwgYXJndW1lbnRzICk7XHJcblx0XHR9XHJcblx0fTtcclxuXHQvKiBTdGF0aWMgbWVtYmVycyAqL1xyXG5cdEkxOE4uZGVmYXVsdHMgPSB7XHJcblx0XHRsb2NhbGU6IFN0cmluZy5sb2NhbGUsXHJcblx0XHRmYWxsYmFja0xvY2FsZTogJ2VuJyxcclxuXHRcdHBhcnNlcjogJC5pMThuLnBhcnNlcixcclxuXHRcdG1lc3NhZ2VTdG9yZTogJC5pMThuLm1lc3NhZ2VTdG9yZVxyXG5cdH07XHJcblxyXG5cdC8vIEV4cG9zZSBjb25zdHJ1Y3RvclxyXG5cdCQuaTE4bi5jb25zdHJ1Y3RvciA9IEkxOE47XHJcbn0oIGpRdWVyeSApICk7XHJcbiIsIi8qIGdsb2JhbCBwbHVyYWxSdWxlUGFyc2VyICovXHJcbiggZnVuY3Rpb24gKCAkICkge1xyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHJcblx0Ly8ganNjczpkaXNhYmxlXHJcblx0dmFyIGxhbmd1YWdlID0ge1xyXG5cdFx0Ly8gQ0xEUiBwbHVyYWwgcnVsZXMgZ2VuZXJhdGVkIHVzaW5nXHJcblx0XHQvLyBsaWJzL0NMRFJQbHVyYWxSdWxlUGFyc2VyL3Rvb2xzL1BsdXJhbFhNTDJKU09OLmh0bWxcclxuXHRcdHBsdXJhbFJ1bGVzOiB7XHJcblx0XHRcdGFrOiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDAuLjEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGFtOiB7XHJcblx0XHRcdFx0b25lOiAnaSA9IDAgb3IgbiA9IDEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGFyOiB7XHJcblx0XHRcdFx0emVybzogJ24gPSAwJyxcclxuXHRcdFx0XHRvbmU6ICduID0gMScsXHJcblx0XHRcdFx0dHdvOiAnbiA9IDInLFxyXG5cdFx0XHRcdGZldzogJ24gJSAxMDAgPSAzLi4xMCcsXHJcblx0XHRcdFx0bWFueTogJ24gJSAxMDAgPSAxMS4uOTknXHJcblx0XHRcdH0sXHJcblx0XHRcdGFyczoge1xyXG5cdFx0XHRcdHplcm86ICduID0gMCcsXHJcblx0XHRcdFx0b25lOiAnbiA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ24gPSAyJyxcclxuXHRcdFx0XHRmZXc6ICduICUgMTAwID0gMy4uMTAnLFxyXG5cdFx0XHRcdG1hbnk6ICduICUgMTAwID0gMTEuLjk5J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRhczoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAwIG9yIG4gPSAxJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRiZToge1xyXG5cdFx0XHRcdG9uZTogJ24gJSAxMCA9IDEgYW5kIG4gJSAxMDAgIT0gMTEnLFxyXG5cdFx0XHRcdGZldzogJ24gJSAxMCA9IDIuLjQgYW5kIG4gJSAxMDAgIT0gMTIuLjE0JyxcclxuXHRcdFx0XHRtYW55OiAnbiAlIDEwID0gMCBvciBuICUgMTAgPSA1Li45IG9yIG4gJSAxMDAgPSAxMS4uMTQnXHJcblx0XHRcdH0sXHJcblx0XHRcdGJoOiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDAuLjEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGJuOiB7XHJcblx0XHRcdFx0b25lOiAnaSA9IDAgb3IgbiA9IDEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGJyOiB7XHJcblx0XHRcdFx0b25lOiAnbiAlIDEwID0gMSBhbmQgbiAlIDEwMCAhPSAxMSw3MSw5MScsXHJcblx0XHRcdFx0dHdvOiAnbiAlIDEwID0gMiBhbmQgbiAlIDEwMCAhPSAxMiw3Miw5MicsXHJcblx0XHRcdFx0ZmV3OiAnbiAlIDEwID0gMy4uNCw5IGFuZCBuICUgMTAwICE9IDEwLi4xOSw3MC4uNzksOTAuLjk5JyxcclxuXHRcdFx0XHRtYW55OiAnbiAhPSAwIGFuZCBuICUgMTAwMDAwMCA9IDAnXHJcblx0XHRcdH0sXHJcblx0XHRcdGJzOiB7XHJcblx0XHRcdFx0b25lOiAndiA9IDAgYW5kIGkgJSAxMCA9IDEgYW5kIGkgJSAxMDAgIT0gMTEgb3IgZiAlIDEwID0gMSBhbmQgZiAlIDEwMCAhPSAxMScsXHJcblx0XHRcdFx0ZmV3OiAndiA9IDAgYW5kIGkgJSAxMCA9IDIuLjQgYW5kIGkgJSAxMDAgIT0gMTIuLjE0IG9yIGYgJSAxMCA9IDIuLjQgYW5kIGYgJSAxMDAgIT0gMTIuLjE0J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRjczoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAxIGFuZCB2ID0gMCcsXHJcblx0XHRcdFx0ZmV3OiAnaSA9IDIuLjQgYW5kIHYgPSAwJyxcclxuXHRcdFx0XHRtYW55OiAndiAhPSAwJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRjeToge1xyXG5cdFx0XHRcdHplcm86ICduID0gMCcsXHJcblx0XHRcdFx0b25lOiAnbiA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ24gPSAyJyxcclxuXHRcdFx0XHRmZXc6ICduID0gMycsXHJcblx0XHRcdFx0bWFueTogJ24gPSA2J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRkYToge1xyXG5cdFx0XHRcdG9uZTogJ24gPSAxIG9yIHQgIT0gMCBhbmQgaSA9IDAsMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0ZHNiOiB7XHJcblx0XHRcdFx0b25lOiAndiA9IDAgYW5kIGkgJSAxMDAgPSAxIG9yIGYgJSAxMDAgPSAxJyxcclxuXHRcdFx0XHR0d286ICd2ID0gMCBhbmQgaSAlIDEwMCA9IDIgb3IgZiAlIDEwMCA9IDInLFxyXG5cdFx0XHRcdGZldzogJ3YgPSAwIGFuZCBpICUgMTAwID0gMy4uNCBvciBmICUgMTAwID0gMy4uNCdcclxuXHRcdFx0fSxcclxuXHRcdFx0ZmE6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCBvciBuID0gMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0ZmY6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCwxJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRmaWw6IHtcclxuXHRcdFx0XHRvbmU6ICd2ID0gMCBhbmQgaSA9IDEsMiwzIG9yIHYgPSAwIGFuZCBpICUgMTAgIT0gNCw2LDkgb3IgdiAhPSAwIGFuZCBmICUgMTAgIT0gNCw2LDknXHJcblx0XHRcdH0sXHJcblx0XHRcdGZyOiB7XHJcblx0XHRcdFx0b25lOiAnaSA9IDAsMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0Z2E6IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMScsXHJcblx0XHRcdFx0dHdvOiAnbiA9IDInLFxyXG5cdFx0XHRcdGZldzogJ24gPSAzLi42JyxcclxuXHRcdFx0XHRtYW55OiAnbiA9IDcuLjEwJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRnZDoge1xyXG5cdFx0XHRcdG9uZTogJ24gPSAxLDExJyxcclxuXHRcdFx0XHR0d286ICduID0gMiwxMicsXHJcblx0XHRcdFx0ZmV3OiAnbiA9IDMuLjEwLDEzLi4xOSdcclxuXHRcdFx0fSxcclxuXHRcdFx0Z3U6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCBvciBuID0gMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0Z3V3OiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDAuLjEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGd2OiB7XHJcblx0XHRcdFx0b25lOiAndiA9IDAgYW5kIGkgJSAxMCA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ3YgPSAwIGFuZCBpICUgMTAgPSAyJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwMCA9IDAsMjAsNDAsNjAsODAnLFxyXG5cdFx0XHRcdG1hbnk6ICd2ICE9IDAnXHJcblx0XHRcdH0sXHJcblx0XHRcdGhlOiB7XHJcblx0XHRcdFx0b25lOiAnaSA9IDEgYW5kIHYgPSAwJyxcclxuXHRcdFx0XHR0d286ICdpID0gMiBhbmQgdiA9IDAnLFxyXG5cdFx0XHRcdG1hbnk6ICd2ID0gMCBhbmQgbiAhPSAwLi4xMCBhbmQgbiAlIDEwID0gMCdcclxuXHRcdFx0fSxcclxuXHRcdFx0aGk6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCBvciBuID0gMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0aHI6IHtcclxuXHRcdFx0XHRvbmU6ICd2ID0gMCBhbmQgaSAlIDEwID0gMSBhbmQgaSAlIDEwMCAhPSAxMSBvciBmICUgMTAgPSAxIGFuZCBmICUgMTAwICE9IDExJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwID0gMi4uNCBhbmQgaSAlIDEwMCAhPSAxMi4uMTQgb3IgZiAlIDEwID0gMi4uNCBhbmQgZiAlIDEwMCAhPSAxMi4uMTQnXHJcblx0XHRcdH0sXHJcblx0XHRcdGhzYjoge1xyXG5cdFx0XHRcdG9uZTogJ3YgPSAwIGFuZCBpICUgMTAwID0gMSBvciBmICUgMTAwID0gMScsXHJcblx0XHRcdFx0dHdvOiAndiA9IDAgYW5kIGkgJSAxMDAgPSAyIG9yIGYgJSAxMDAgPSAyJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwMCA9IDMuLjQgb3IgZiAlIDEwMCA9IDMuLjQnXHJcblx0XHRcdH0sXHJcblx0XHRcdGh5OiB7XHJcblx0XHRcdFx0b25lOiAnaSA9IDAsMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0aXM6IHtcclxuXHRcdFx0XHRvbmU6ICd0ID0gMCBhbmQgaSAlIDEwID0gMSBhbmQgaSAlIDEwMCAhPSAxMSBvciB0ICE9IDAnXHJcblx0XHRcdH0sXHJcblx0XHRcdGl1OiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ24gPSAyJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRpdzoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAxIGFuZCB2ID0gMCcsXHJcblx0XHRcdFx0dHdvOiAnaSA9IDIgYW5kIHYgPSAwJyxcclxuXHRcdFx0XHRtYW55OiAndiA9IDAgYW5kIG4gIT0gMC4uMTAgYW5kIG4gJSAxMCA9IDAnXHJcblx0XHRcdH0sXHJcblx0XHRcdGthYjoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAwLDEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGtuOiB7XHJcblx0XHRcdFx0b25lOiAnaSA9IDAgb3IgbiA9IDEnXHJcblx0XHRcdH0sXHJcblx0XHRcdGt3OiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ24gPSAyJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRsYWc6IHtcclxuXHRcdFx0XHR6ZXJvOiAnbiA9IDAnLFxyXG5cdFx0XHRcdG9uZTogJ2kgPSAwLDEgYW5kIG4gIT0gMCdcclxuXHRcdFx0fSxcclxuXHRcdFx0bG46IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMC4uMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0bHQ6IHtcclxuXHRcdFx0XHRvbmU6ICduICUgMTAgPSAxIGFuZCBuICUgMTAwICE9IDExLi4xOScsXHJcblx0XHRcdFx0ZmV3OiAnbiAlIDEwID0gMi4uOSBhbmQgbiAlIDEwMCAhPSAxMS4uMTknLFxyXG5cdFx0XHRcdG1hbnk6ICdmICE9IDAnXHJcblx0XHRcdH0sXHJcblx0XHRcdGx2OiB7XHJcblx0XHRcdFx0emVybzogJ24gJSAxMCA9IDAgb3IgbiAlIDEwMCA9IDExLi4xOSBvciB2ID0gMiBhbmQgZiAlIDEwMCA9IDExLi4xOScsXHJcblx0XHRcdFx0b25lOiAnbiAlIDEwID0gMSBhbmQgbiAlIDEwMCAhPSAxMSBvciB2ID0gMiBhbmQgZiAlIDEwID0gMSBhbmQgZiAlIDEwMCAhPSAxMSBvciB2ICE9IDIgYW5kIGYgJSAxMCA9IDEnXHJcblx0XHRcdH0sXHJcblx0XHRcdG1nOiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDAuLjEnXHJcblx0XHRcdH0sXHJcblx0XHRcdG1rOiB7XHJcblx0XHRcdFx0b25lOiAndiA9IDAgYW5kIGkgJSAxMCA9IDEgb3IgZiAlIDEwID0gMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0bW86IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMSBhbmQgdiA9IDAnLFxyXG5cdFx0XHRcdGZldzogJ3YgIT0gMCBvciBuID0gMCBvciBuICE9IDEgYW5kIG4gJSAxMDAgPSAxLi4xOSdcclxuXHRcdFx0fSxcclxuXHRcdFx0bXI6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCBvciBuID0gMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0bXQ6IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMScsXHJcblx0XHRcdFx0ZmV3OiAnbiA9IDAgb3IgbiAlIDEwMCA9IDIuLjEwJyxcclxuXHRcdFx0XHRtYW55OiAnbiAlIDEwMCA9IDExLi4xOSdcclxuXHRcdFx0fSxcclxuXHRcdFx0bmFxOiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ24gPSAyJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRuc286IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMC4uMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0cGE6IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMC4uMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0cGw6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMSBhbmQgdiA9IDAnLFxyXG5cdFx0XHRcdGZldzogJ3YgPSAwIGFuZCBpICUgMTAgPSAyLi40IGFuZCBpICUgMTAwICE9IDEyLi4xNCcsXHJcblx0XHRcdFx0bWFueTogJ3YgPSAwIGFuZCBpICE9IDEgYW5kIGkgJSAxMCA9IDAuLjEgb3IgdiA9IDAgYW5kIGkgJSAxMCA9IDUuLjkgb3IgdiA9IDAgYW5kIGkgJSAxMDAgPSAxMi4uMTQnXHJcblx0XHRcdH0sXHJcblx0XHRcdHByZzoge1xyXG5cdFx0XHRcdHplcm86ICduICUgMTAgPSAwIG9yIG4gJSAxMDAgPSAxMS4uMTkgb3IgdiA9IDIgYW5kIGYgJSAxMDAgPSAxMS4uMTknLFxyXG5cdFx0XHRcdG9uZTogJ24gJSAxMCA9IDEgYW5kIG4gJSAxMDAgIT0gMTEgb3IgdiA9IDIgYW5kIGYgJSAxMCA9IDEgYW5kIGYgJSAxMDAgIT0gMTEgb3IgdiAhPSAyIGFuZCBmICUgMTAgPSAxJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRwdDoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAwLi4xJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRybzoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAxIGFuZCB2ID0gMCcsXHJcblx0XHRcdFx0ZmV3OiAndiAhPSAwIG9yIG4gPSAwIG9yIG4gIT0gMSBhbmQgbiAlIDEwMCA9IDEuLjE5J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRydToge1xyXG5cdFx0XHRcdG9uZTogJ3YgPSAwIGFuZCBpICUgMTAgPSAxIGFuZCBpICUgMTAwICE9IDExJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwID0gMi4uNCBhbmQgaSAlIDEwMCAhPSAxMi4uMTQnLFxyXG5cdFx0XHRcdG1hbnk6ICd2ID0gMCBhbmQgaSAlIDEwID0gMCBvciB2ID0gMCBhbmQgaSAlIDEwID0gNS4uOSBvciB2ID0gMCBhbmQgaSAlIDEwMCA9IDExLi4xNCdcclxuXHRcdFx0fSxcclxuXHRcdFx0c2U6IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMScsXHJcblx0XHRcdFx0dHdvOiAnbiA9IDInXHJcblx0XHRcdH0sXHJcblx0XHRcdHNoOiB7XHJcblx0XHRcdFx0b25lOiAndiA9IDAgYW5kIGkgJSAxMCA9IDEgYW5kIGkgJSAxMDAgIT0gMTEgb3IgZiAlIDEwID0gMSBhbmQgZiAlIDEwMCAhPSAxMScsXHJcblx0XHRcdFx0ZmV3OiAndiA9IDAgYW5kIGkgJSAxMCA9IDIuLjQgYW5kIGkgJSAxMDAgIT0gMTIuLjE0IG9yIGYgJSAxMCA9IDIuLjQgYW5kIGYgJSAxMDAgIT0gMTIuLjE0J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRzaGk6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCBvciBuID0gMScsXHJcblx0XHRcdFx0ZmV3OiAnbiA9IDIuLjEwJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRzaToge1xyXG5cdFx0XHRcdG9uZTogJ24gPSAwLDEgb3IgaSA9IDAgYW5kIGYgPSAxJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRzazoge1xyXG5cdFx0XHRcdG9uZTogJ2kgPSAxIGFuZCB2ID0gMCcsXHJcblx0XHRcdFx0ZmV3OiAnaSA9IDIuLjQgYW5kIHYgPSAwJyxcclxuXHRcdFx0XHRtYW55OiAndiAhPSAwJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRzbDoge1xyXG5cdFx0XHRcdG9uZTogJ3YgPSAwIGFuZCBpICUgMTAwID0gMScsXHJcblx0XHRcdFx0dHdvOiAndiA9IDAgYW5kIGkgJSAxMDAgPSAyJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwMCA9IDMuLjQgb3IgdiAhPSAwJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRzbWE6IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMScsXHJcblx0XHRcdFx0dHdvOiAnbiA9IDInXHJcblx0XHRcdH0sXHJcblx0XHRcdHNtaToge1xyXG5cdFx0XHRcdG9uZTogJ24gPSAxJyxcclxuXHRcdFx0XHR0d286ICduID0gMidcclxuXHRcdFx0fSxcclxuXHRcdFx0c21qOiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDEnLFxyXG5cdFx0XHRcdHR3bzogJ24gPSAyJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRzbW46IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMScsXHJcblx0XHRcdFx0dHdvOiAnbiA9IDInXHJcblx0XHRcdH0sXHJcblx0XHRcdHNtczoge1xyXG5cdFx0XHRcdG9uZTogJ24gPSAxJyxcclxuXHRcdFx0XHR0d286ICduID0gMidcclxuXHRcdFx0fSxcclxuXHRcdFx0c3I6IHtcclxuXHRcdFx0XHRvbmU6ICd2ID0gMCBhbmQgaSAlIDEwID0gMSBhbmQgaSAlIDEwMCAhPSAxMSBvciBmICUgMTAgPSAxIGFuZCBmICUgMTAwICE9IDExJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwID0gMi4uNCBhbmQgaSAlIDEwMCAhPSAxMi4uMTQgb3IgZiAlIDEwID0gMi4uNCBhbmQgZiAlIDEwMCAhPSAxMi4uMTQnXHJcblx0XHRcdH0sXHJcblx0XHRcdHRpOiB7XHJcblx0XHRcdFx0b25lOiAnbiA9IDAuLjEnXHJcblx0XHRcdH0sXHJcblx0XHRcdHRsOiB7XHJcblx0XHRcdFx0b25lOiAndiA9IDAgYW5kIGkgPSAxLDIsMyBvciB2ID0gMCBhbmQgaSAlIDEwICE9IDQsNiw5IG9yIHYgIT0gMCBhbmQgZiAlIDEwICE9IDQsNiw5J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHR0em06IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMC4uMSBvciBuID0gMTEuLjk5J1xyXG5cdFx0XHR9LFxyXG5cdFx0XHR1azoge1xyXG5cdFx0XHRcdG9uZTogJ3YgPSAwIGFuZCBpICUgMTAgPSAxIGFuZCBpICUgMTAwICE9IDExJyxcclxuXHRcdFx0XHRmZXc6ICd2ID0gMCBhbmQgaSAlIDEwID0gMi4uNCBhbmQgaSAlIDEwMCAhPSAxMi4uMTQnLFxyXG5cdFx0XHRcdG1hbnk6ICd2ID0gMCBhbmQgaSAlIDEwID0gMCBvciB2ID0gMCBhbmQgaSAlIDEwID0gNS4uOSBvciB2ID0gMCBhbmQgaSAlIDEwMCA9IDExLi4xNCdcclxuXHRcdFx0fSxcclxuXHRcdFx0d2E6IHtcclxuXHRcdFx0XHRvbmU6ICduID0gMC4uMSdcclxuXHRcdFx0fSxcclxuXHRcdFx0enU6IHtcclxuXHRcdFx0XHRvbmU6ICdpID0gMCBvciBuID0gMSdcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcdC8vIGpzY3M6ZW5hYmxlXHJcblxyXG5cdFx0LyoqXHJcblx0XHQgKiBQbHVyYWwgZm9ybSB0cmFuc2Zvcm1hdGlvbnMsIG5lZWRlZCBmb3Igc29tZSBsYW5ndWFnZXMuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtpbnRlZ2VyfSBjb3VudFxyXG5cdFx0ICogICAgICAgICAgICBOb24tbG9jYWxpemVkIHF1YW50aWZpZXJcclxuXHRcdCAqIEBwYXJhbSB7QXJyYXl9IGZvcm1zXHJcblx0XHQgKiAgICAgICAgICAgIExpc3Qgb2YgcGx1cmFsIGZvcm1zXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9IENvcnJlY3QgZm9ybSBmb3IgcXVhbnRpZmllciBpbiB0aGlzIGxhbmd1YWdlXHJcblx0XHQgKi9cclxuXHRcdGNvbnZlcnRQbHVyYWw6IGZ1bmN0aW9uICggY291bnQsIGZvcm1zICkge1xyXG5cdFx0XHR2YXIgcGx1cmFsUnVsZXMsXHJcblx0XHRcdFx0cGx1cmFsRm9ybUluZGV4LFxyXG5cdFx0XHRcdGluZGV4LFxyXG5cdFx0XHRcdGV4cGxpY2l0UGx1cmFsUGF0dGVybiA9IG5ldyBSZWdFeHAoICdcXFxcZCs9JywgJ2knICksXHJcblx0XHRcdFx0Zm9ybUNvdW50LFxyXG5cdFx0XHRcdGZvcm07XHJcblxyXG5cdFx0XHRpZiAoICFmb3JtcyB8fCBmb3Jtcy5sZW5ndGggPT09IDAgKSB7XHJcblx0XHRcdFx0cmV0dXJuICcnO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQvLyBIYW5kbGUgZm9yIEV4cGxpY2l0IDA9ICYgMT0gdmFsdWVzXHJcblx0XHRcdGZvciAoIGluZGV4ID0gMDsgaW5kZXggPCBmb3Jtcy5sZW5ndGg7IGluZGV4KysgKSB7XHJcblx0XHRcdFx0Zm9ybSA9IGZvcm1zWyBpbmRleCBdO1xyXG5cdFx0XHRcdGlmICggZXhwbGljaXRQbHVyYWxQYXR0ZXJuLnRlc3QoIGZvcm0gKSApIHtcclxuXHRcdFx0XHRcdGZvcm1Db3VudCA9IHBhcnNlSW50KCBmb3JtLnNsaWNlKCAwLCBmb3JtLmluZGV4T2YoICc9JyApICksIDEwICk7XHJcblx0XHRcdFx0XHRpZiAoIGZvcm1Db3VudCA9PT0gY291bnQgKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybiAoIGZvcm0uc2xpY2UoIGZvcm0uaW5kZXhPZiggJz0nICkgKyAxICkgKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGZvcm1zWyBpbmRleCBdID0gdW5kZWZpbmVkO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0Zm9ybXMgPSAkLm1hcCggZm9ybXMsIGZ1bmN0aW9uICggZm9ybSApIHtcclxuXHRcdFx0XHRpZiAoIGZvcm0gIT09IHVuZGVmaW5lZCApIHtcclxuXHRcdFx0XHRcdHJldHVybiBmb3JtO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSApO1xyXG5cclxuXHRcdFx0cGx1cmFsUnVsZXMgPSB0aGlzLnBsdXJhbFJ1bGVzWyAkLmkxOG4oKS5sb2NhbGUgXTtcclxuXHJcblx0XHRcdGlmICggIXBsdXJhbFJ1bGVzICkge1xyXG5cdFx0XHRcdC8vIGRlZmF1bHQgZmFsbGJhY2suXHJcblx0XHRcdFx0cmV0dXJuICggY291bnQgPT09IDEgKSA/IGZvcm1zWyAwIF0gOiBmb3Jtc1sgMSBdO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRwbHVyYWxGb3JtSW5kZXggPSB0aGlzLmdldFBsdXJhbEZvcm0oIGNvdW50LCBwbHVyYWxSdWxlcyApO1xyXG5cdFx0XHRwbHVyYWxGb3JtSW5kZXggPSBNYXRoLm1pbiggcGx1cmFsRm9ybUluZGV4LCBmb3Jtcy5sZW5ndGggLSAxICk7XHJcblxyXG5cdFx0XHRyZXR1cm4gZm9ybXNbIHBsdXJhbEZvcm1JbmRleCBdO1xyXG5cdFx0fSxcclxuXHJcblx0XHQvKipcclxuXHRcdCAqIEZvciB0aGUgbnVtYmVyLCBnZXQgdGhlIHBsdXJhbCBmb3IgaW5kZXhcclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge2ludGVnZXJ9IG51bWJlclxyXG5cdFx0ICogQHBhcmFtIHtPYmplY3R9IHBsdXJhbFJ1bGVzXHJcblx0XHQgKiBAcmV0dXJuIHtpbnRlZ2VyfSBwbHVyYWwgZm9ybSBpbmRleFxyXG5cdFx0ICovXHJcblx0XHRnZXRQbHVyYWxGb3JtOiBmdW5jdGlvbiAoIG51bWJlciwgcGx1cmFsUnVsZXMgKSB7XHJcblx0XHRcdHZhciBpLFxyXG5cdFx0XHRcdHBsdXJhbEZvcm1zID0gWyAnemVybycsICdvbmUnLCAndHdvJywgJ2ZldycsICdtYW55JywgJ290aGVyJyBdLFxyXG5cdFx0XHRcdHBsdXJhbEZvcm1JbmRleCA9IDA7XHJcblxyXG5cdFx0XHRmb3IgKCBpID0gMDsgaSA8IHBsdXJhbEZvcm1zLmxlbmd0aDsgaSsrICkge1xyXG5cdFx0XHRcdGlmICggcGx1cmFsUnVsZXNbIHBsdXJhbEZvcm1zWyBpIF0gXSApIHtcclxuXHRcdFx0XHRcdGlmICggcGx1cmFsUnVsZVBhcnNlciggcGx1cmFsUnVsZXNbIHBsdXJhbEZvcm1zWyBpIF0gXSwgbnVtYmVyICkgKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybiBwbHVyYWxGb3JtSW5kZXg7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0cGx1cmFsRm9ybUluZGV4Kys7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRyZXR1cm4gcGx1cmFsRm9ybUluZGV4O1xyXG5cdFx0fSxcclxuXHJcblx0XHQvKipcclxuXHRcdCAqIENvbnZlcnRzIGEgbnVtYmVyIHVzaW5nIGRpZ2l0VHJhbnNmb3JtVGFibGUuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtudW1iZXJ9IG51bSBWYWx1ZSB0byBiZSBjb252ZXJ0ZWRcclxuXHRcdCAqIEBwYXJhbSB7Ym9vbGVhbn0gaW50ZWdlciBDb252ZXJ0IHRoZSByZXR1cm4gdmFsdWUgdG8gYW4gaW50ZWdlclxyXG5cdFx0ICogQHJldHVybiB7c3RyaW5nfSBUaGUgbnVtYmVyIGNvbnZlcnRlZCBpbnRvIGEgU3RyaW5nLlxyXG5cdFx0ICovXHJcblx0XHRjb252ZXJ0TnVtYmVyOiBmdW5jdGlvbiAoIG51bSwgaW50ZWdlciApIHtcclxuXHRcdFx0dmFyIHRtcCwgaXRlbSwgaSxcclxuXHRcdFx0XHR0cmFuc2Zvcm1UYWJsZSwgbnVtYmVyU3RyaW5nLCBjb252ZXJ0ZWROdW1iZXI7XHJcblxyXG5cdFx0XHQvLyBTZXQgdGhlIHRhcmdldCBUcmFuc2Zvcm0gdGFibGU6XHJcblx0XHRcdHRyYW5zZm9ybVRhYmxlID0gdGhpcy5kaWdpdFRyYW5zZm9ybVRhYmxlKCAkLmkxOG4oKS5sb2NhbGUgKTtcclxuXHRcdFx0bnVtYmVyU3RyaW5nID0gU3RyaW5nKCBudW0gKTtcclxuXHRcdFx0Y29udmVydGVkTnVtYmVyID0gJyc7XHJcblxyXG5cdFx0XHRpZiAoICF0cmFuc2Zvcm1UYWJsZSApIHtcclxuXHRcdFx0XHRyZXR1cm4gbnVtO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQvLyBDaGVjayBpZiB0aGUgcmVzdG9yZSB0byBMYXRpbiBudW1iZXIgZmxhZyBpcyBzZXQ6XHJcblx0XHRcdGlmICggaW50ZWdlciApIHtcclxuXHRcdFx0XHRpZiAoIHBhcnNlRmxvYXQoIG51bSwgMTAgKSA9PT0gbnVtICkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIG51bTtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdHRtcCA9IFtdO1xyXG5cclxuXHRcdFx0XHRmb3IgKCBpdGVtIGluIHRyYW5zZm9ybVRhYmxlICkge1xyXG5cdFx0XHRcdFx0dG1wWyB0cmFuc2Zvcm1UYWJsZVsgaXRlbSBdIF0gPSBpdGVtO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0dHJhbnNmb3JtVGFibGUgPSB0bXA7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGZvciAoIGkgPSAwOyBpIDwgbnVtYmVyU3RyaW5nLmxlbmd0aDsgaSsrICkge1xyXG5cdFx0XHRcdGlmICggdHJhbnNmb3JtVGFibGVbIG51bWJlclN0cmluZ1sgaSBdIF0gKSB7XHJcblx0XHRcdFx0XHRjb252ZXJ0ZWROdW1iZXIgKz0gdHJhbnNmb3JtVGFibGVbIG51bWJlclN0cmluZ1sgaSBdIF07XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdGNvbnZlcnRlZE51bWJlciArPSBudW1iZXJTdHJpbmdbIGkgXTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJldHVybiBpbnRlZ2VyID8gcGFyc2VGbG9hdCggY29udmVydGVkTnVtYmVyLCAxMCApIDogY29udmVydGVkTnVtYmVyO1xyXG5cdFx0fSxcclxuXHJcblx0XHQvKipcclxuXHRcdCAqIEdyYW1tYXRpY2FsIHRyYW5zZm9ybWF0aW9ucywgbmVlZGVkIGZvciBpbmZsZWN0ZWQgbGFuZ3VhZ2VzLlxyXG5cdFx0ICogSW52b2tlZCBieSBwdXR0aW5nIHt7Z3JhbW1hcjpmb3JtfHdvcmR9fSBpbiBhIG1lc3NhZ2UuXHJcblx0XHQgKiBPdmVycmlkZSB0aGlzIG1ldGhvZCBmb3IgbGFuZ3VhZ2VzIHRoYXQgbmVlZCBzcGVjaWFsIGdyYW1tYXIgcnVsZXNcclxuXHRcdCAqIGFwcGxpZWQgZHluYW1pY2FsbHkuXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IHdvcmRcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBmb3JtXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9XHJcblx0XHQgKi9cclxuXHRcdC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby11bnVzZWQtdmFyc1xyXG5cdFx0Y29udmVydEdyYW1tYXI6IGZ1bmN0aW9uICggd29yZCwgZm9ybSApIHtcclxuXHRcdFx0cmV0dXJuIHdvcmQ7XHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogUHJvdmlkZXMgYW4gYWx0ZXJuYXRpdmUgdGV4dCBkZXBlbmRpbmcgb24gc3BlY2lmaWVkIGdlbmRlci4gVXNhZ2VcclxuXHRcdCAqIHt7Z2VuZGVyOltnZW5kZXJ8dXNlciBvYmplY3RdfG1hc2N1bGluZXxmZW1pbmluZXxuZXV0cmFsfX0uIElmIHNlY29uZFxyXG5cdFx0ICogb3IgdGhpcmQgcGFyYW1ldGVyIGFyZSBub3Qgc3BlY2lmaWVkLCBtYXNjdWxpbmUgaXMgdXNlZC5cclxuXHRcdCAqXHJcblx0XHQgKiBUaGVzZSBkZXRhaWxzIG1heSBiZSBvdmVycmlkZW4gcGVyIGxhbmd1YWdlLlxyXG5cdFx0ICpcclxuXHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBnZW5kZXJcclxuXHRcdCAqICAgICAgbWFsZSwgZmVtYWxlLCBvciBhbnl0aGluZyBlbHNlIGZvciBuZXV0cmFsLlxyXG5cdFx0ICogQHBhcmFtIHtBcnJheX0gZm9ybXNcclxuXHRcdCAqICAgICAgTGlzdCBvZiBnZW5kZXIgZm9ybXNcclxuXHRcdCAqXHJcblx0XHQgKiBAcmV0dXJuIHtzdHJpbmd9XHJcblx0XHQgKi9cclxuXHRcdGdlbmRlcjogZnVuY3Rpb24gKCBnZW5kZXIsIGZvcm1zICkge1xyXG5cdFx0XHRpZiAoICFmb3JtcyB8fCBmb3Jtcy5sZW5ndGggPT09IDAgKSB7XHJcblx0XHRcdFx0cmV0dXJuICcnO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHR3aGlsZSAoIGZvcm1zLmxlbmd0aCA8IDIgKSB7XHJcblx0XHRcdFx0Zm9ybXMucHVzaCggZm9ybXNbIGZvcm1zLmxlbmd0aCAtIDEgXSApO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAoIGdlbmRlciA9PT0gJ21hbGUnICkge1xyXG5cdFx0XHRcdHJldHVybiBmb3Jtc1sgMCBdO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAoIGdlbmRlciA9PT0gJ2ZlbWFsZScgKSB7XHJcblx0XHRcdFx0cmV0dXJuIGZvcm1zWyAxIF07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJldHVybiAoIGZvcm1zLmxlbmd0aCA9PT0gMyApID8gZm9ybXNbIDIgXSA6IGZvcm1zWyAwIF07XHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogR2V0IHRoZSBkaWdpdCB0cmFuc2Zvcm0gdGFibGUgZm9yIHRoZSBnaXZlbiBsYW5ndWFnZVxyXG5cdFx0ICogU2VlIGh0dHA6Ly9jbGRyLnVuaWNvZGUub3JnL3RyYW5zbGF0aW9uL251bWJlcmluZy1zeXN0ZW1zXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IGxhbmd1YWdlXHJcblx0XHQgKiBAcmV0dXJuIHtBcnJheXxib29sZWFufSBMaXN0IG9mIGRpZ2l0cyBpbiB0aGUgcGFzc2VkIGxhbmd1YWdlIG9yIGZhbHNlXHJcblx0XHQgKiByZXByZXNlbnRhdGlvbiwgb3IgYm9vbGVhbiBmYWxzZSBpZiB0aGVyZSBpcyBubyBpbmZvcm1hdGlvbi5cclxuXHRcdCAqL1xyXG5cdFx0ZGlnaXRUcmFuc2Zvcm1UYWJsZTogZnVuY3Rpb24gKCBsYW5ndWFnZSApIHtcclxuXHRcdFx0dmFyIHRhYmxlcyA9IHtcclxuXHRcdFx0XHRhcjogJ9mg2aHZotmj2aTZpdmm2afZqNmpJyxcclxuXHRcdFx0XHRmYTogJ9uw27Hbstuz27Tbtdu227fbuNu5JyxcclxuXHRcdFx0XHRtbDogJ+C1puC1p+C1qOC1qeC1quC1q+C1rOC1reC1ruC1rycsXHJcblx0XHRcdFx0a246ICfgs6bgs6fgs6jgs6ngs6rgs6vgs6zgs63gs67gs68nLFxyXG5cdFx0XHRcdGxvOiAn4LuQ4LuR4LuS4LuT4LuU4LuV4LuW4LuX4LuY4LuZJyxcclxuXHRcdFx0XHRvcjogJ+CtpuCtp+CtqOCtqeCtquCtq+CtrOCtreCtruCtrycsXHJcblx0XHRcdFx0a2g6ICfhn6Dhn6Hhn6Lhn6Phn6Thn6Xhn6bhn6fhn6jhn6knLFxyXG5cdFx0XHRcdHBhOiAn4Kmm4Kmn4Kmo4Kmp4Kmq4Kmr4Kms4Kmt4Kmu4KmvJyxcclxuXHRcdFx0XHRndTogJ+CrpuCrp+CrqOCrqeCrquCrq+CrrOCrreCrruCrrycsXHJcblx0XHRcdFx0aGk6ICfgpabgpafgpajgpangpargpavgpazgpa3gpa7gpa8nLFxyXG5cdFx0XHRcdG15OiAn4YGA4YGB4YGC4YGD4YGE4YGF4YGG4YGH4YGI4YGJJyxcclxuXHRcdFx0XHR0YTogJ+CvpuCvp+CvqOCvqeCvquCvq+CvrOCvreCvruCvrycsXHJcblx0XHRcdFx0dGU6ICfgsabgsafgsajgsangsargsavgsazgsa3gsa7gsa8nLFxyXG5cdFx0XHRcdHRoOiAn4LmQ4LmR4LmS4LmT4LmU4LmV4LmW4LmX4LmY4LmZJywgLy8gRklYTUUgdXNlIGlzbyA2MzkgY29kZXNcclxuXHRcdFx0XHRibzogJ+C8oOC8oeC8ouC8o+C8pOC8peC8puC8p+C8qOC8qScgLy8gRklYTUUgdXNlIGlzbyA2MzkgY29kZXNcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdGlmICggIXRhYmxlc1sgbGFuZ3VhZ2UgXSApIHtcclxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJldHVybiB0YWJsZXNbIGxhbmd1YWdlIF0uc3BsaXQoICcnICk7XHJcblx0XHR9XHJcblx0fTtcclxuXHJcblx0JC5leHRlbmQoICQuaTE4bi5sYW5ndWFnZXMsIHtcclxuXHRcdCdkZWZhdWx0JzogbGFuZ3VhZ2VcclxuXHR9ICk7XHJcbn0oIGpRdWVyeSApICk7XHJcbiIsIi8qIVxyXG4gKiBqUXVlcnkgSW50ZXJuYXRpb25hbGl6YXRpb24gbGlicmFyeSAtIE1lc3NhZ2UgU3RvcmVcclxuICpcclxuICogQ29weXJpZ2h0IChDKSAyMDEyIFNhbnRob3NoIFRob3R0aW5nYWxcclxuICpcclxuICoganF1ZXJ5LmkxOG4gaXMgZHVhbCBsaWNlbnNlZCBHUEx2MiBvciBsYXRlciBhbmQgTUlULiBZb3UgZG9uJ3QgaGF2ZSB0byBkbyBhbnl0aGluZyBzcGVjaWFsIHRvXHJcbiAqIGNob29zZSBvbmUgbGljZW5zZSBvciB0aGUgb3RoZXIgYW5kIHlvdSBkb24ndCBoYXZlIHRvIG5vdGlmeSBhbnlvbmUgd2hpY2ggbGljZW5zZSB5b3UgYXJlIHVzaW5nLlxyXG4gKiBZb3UgYXJlIGZyZWUgdG8gdXNlIFVuaXZlcnNhbExhbmd1YWdlU2VsZWN0b3IgaW4gY29tbWVyY2lhbCBwcm9qZWN0cyBhcyBsb25nIGFzIHRoZSBjb3B5cmlnaHRcclxuICogaGVhZGVyIGlzIGxlZnQgaW50YWN0LiBTZWUgZmlsZXMgR1BMLUxJQ0VOU0UgYW5kIE1JVC1MSUNFTlNFIGZvciBkZXRhaWxzLlxyXG4gKlxyXG4gKiBAbGljZW5jZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5jZSAyLjAgb3IgbGF0ZXJcclxuICogQGxpY2VuY2UgTUlUIExpY2Vuc2VcclxuICovXHJcblxyXG4oIGZ1bmN0aW9uICggJCApIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblxyXG5cdHZhciBNZXNzYWdlU3RvcmUgPSBmdW5jdGlvbiAoKSB7XHJcblx0XHR0aGlzLm1lc3NhZ2VzID0ge307XHJcblx0XHR0aGlzLnNvdXJjZXMgPSB7fTtcclxuXHR9O1xyXG5cclxuXHRmdW5jdGlvbiBqc29uTWVzc2FnZUxvYWRlciggdXJsICkge1xyXG5cdFx0dmFyIGRlZmVycmVkID0gJC5EZWZlcnJlZCgpO1xyXG5cclxuXHRcdCQuZ2V0SlNPTiggdXJsIClcclxuXHRcdFx0LmRvbmUoIGRlZmVycmVkLnJlc29sdmUgKVxyXG5cdFx0XHQuZmFpbCggZnVuY3Rpb24gKCBqcXhociwgc2V0dGluZ3MsIGV4Y2VwdGlvbiApIHtcclxuXHRcdFx0XHQkLmkxOG4ubG9nKCAnRXJyb3IgaW4gbG9hZGluZyBtZXNzYWdlcyBmcm9tICcgKyB1cmwgKyAnIEV4Y2VwdGlvbjogJyArIGV4Y2VwdGlvbiApO1xyXG5cdFx0XHRcdC8vIElnbm9yZSA0MDQgZXhjZXB0aW9uLCBiZWNhdXNlIHdlIGFyZSBoYW5kbGluZyBmYWxsYWJhY2tzIGV4cGxpY2l0bHlcclxuXHRcdFx0XHRkZWZlcnJlZC5yZXNvbHZlKCk7XHJcblx0XHRcdH0gKTtcclxuXHJcblx0XHRyZXR1cm4gZGVmZXJyZWQucHJvbWlzZSgpO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS93aWtpbWVkaWEvanF1ZXJ5LmkxOG4vd2lraS9TcGVjaWZpY2F0aW9uI3dpa2ktTWVzc2FnZV9GaWxlX0xvYWRpbmdcclxuXHQgKi9cclxuXHRNZXNzYWdlU3RvcmUucHJvdG90eXBlID0ge1xyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogR2VuZXJhbCBtZXNzYWdlIGxvYWRpbmcgQVBJIFRoaXMgY2FuIHRha2UgYSBVUkwgc3RyaW5nIGZvclxyXG5cdFx0ICogdGhlIGpzb24gZm9ybWF0dGVkIG1lc3NhZ2VzLlxyXG5cdFx0ICogPGNvZGU+bG9hZCgncGF0aC90by9hbGxfbG9jYWxpemF0aW9ucy5qc29uJyk7PC9jb2RlPlxyXG5cdFx0ICpcclxuXHRcdCAqIFRoaXMgY2FuIGFsc28gbG9hZCBhIGxvY2FsaXphdGlvbiBmaWxlIGZvciBhIGxvY2FsZSA8Y29kZT5cclxuXHRcdCAqIGxvYWQoICdwYXRoL3RvL2RlLW1lc3NhZ2VzLmpzb24nLCAnZGUnICk7XHJcblx0XHQgKiA8L2NvZGU+XHJcblx0XHQgKiBBIGRhdGEgb2JqZWN0IGNvbnRhaW5pbmcgbWVzc2FnZSBrZXktIG1lc3NhZ2UgdHJhbnNsYXRpb24gbWFwcGluZ3NcclxuXHRcdCAqIGNhbiBhbHNvIGJlIHBhc3NlZCBFZzpcclxuXHRcdCAqIDxjb2RlPlxyXG5cdFx0ICogbG9hZCggeyAnaGVsbG8nIDogJ0hlbGxvJyB9LCBvcHRpb25hbExvY2FsZSApO1xyXG5cdFx0ICogPC9jb2RlPiBJZiB0aGUgZGF0YSBhcmd1bWVudCBpc1xyXG5cdFx0ICogbnVsbC91bmRlZmluZWQvZmFsc2UsXHJcblx0XHQgKiBhbGwgY2FjaGVkIG1lc3NhZ2VzIGZvciB0aGUgaTE4biBpbnN0YW5jZSB3aWxsIGdldCByZXNldC5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge3N0cmluZ3xPYmplY3R9IHNvdXJjZVxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IGxvY2FsZSBMYW5ndWFnZSB0YWdcclxuXHRcdCAqIEByZXR1cm4ge2pRdWVyeS5Qcm9taXNlfVxyXG5cdFx0ICovXHJcblx0XHRsb2FkOiBmdW5jdGlvbiAoIHNvdXJjZSwgbG9jYWxlICkge1xyXG5cdFx0XHR2YXIga2V5ID0gbnVsbCxcclxuXHRcdFx0XHRkZWZlcnJlZCA9IG51bGwsXHJcblx0XHRcdFx0ZGVmZXJyZWRzID0gW10sXHJcblx0XHRcdFx0bWVzc2FnZVN0b3JlID0gdGhpcztcclxuXHJcblx0XHRcdGlmICggdHlwZW9mIHNvdXJjZSA9PT0gJ3N0cmluZycgKSB7XHJcblx0XHRcdFx0Ly8gVGhpcyBpcyBhIFVSTCB0byB0aGUgbWVzc2FnZXMgZmlsZS5cclxuXHRcdFx0XHQkLmkxOG4ubG9nKCAnTG9hZGluZyBtZXNzYWdlcyBmcm9tOiAnICsgc291cmNlICk7XHJcblx0XHRcdFx0ZGVmZXJyZWQgPSBqc29uTWVzc2FnZUxvYWRlciggc291cmNlIClcclxuXHRcdFx0XHRcdC5kb25lKCBmdW5jdGlvbiAoIGxvY2FsaXphdGlvbiApIHtcclxuXHRcdFx0XHRcdFx0bWVzc2FnZVN0b3JlLnNldCggbG9jYWxlLCBsb2NhbGl6YXRpb24gKTtcclxuXHRcdFx0XHRcdH0gKTtcclxuXHJcblx0XHRcdFx0cmV0dXJuIGRlZmVycmVkLnByb21pc2UoKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYgKCBsb2NhbGUgKSB7XHJcblx0XHRcdFx0Ly8gc291cmNlIGlzIGFuIGtleS12YWx1ZSBwYWlyIG9mIG1lc3NhZ2VzIGZvciBnaXZlbiBsb2NhbGVcclxuXHRcdFx0XHRtZXNzYWdlU3RvcmUuc2V0KCBsb2NhbGUsIHNvdXJjZSApO1xyXG5cclxuXHRcdFx0XHRyZXR1cm4gJC5EZWZlcnJlZCgpLnJlc29sdmUoKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHQvLyBzb3VyY2UgaXMgYSBrZXktdmFsdWUgcGFpciBvZiBsb2NhbGVzIGFuZCB0aGVpciBzb3VyY2VcclxuXHRcdFx0XHRmb3IgKCBrZXkgaW4gc291cmNlICkge1xyXG5cdFx0XHRcdFx0aWYgKCBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoIHNvdXJjZSwga2V5ICkgKSB7XHJcblx0XHRcdFx0XHRcdGxvY2FsZSA9IGtleTtcclxuXHRcdFx0XHRcdFx0Ly8gTm8ge2xvY2FsZX0gZ2l2ZW4sIGFzc3VtZSBkYXRhIGlzIGEgZ3JvdXAgb2YgbGFuZ3VhZ2VzLFxyXG5cdFx0XHRcdFx0XHQvLyBjYWxsIHRoaXMgZnVuY3Rpb24gYWdhaW4gZm9yIGVhY2ggbGFuZ3VhZ2UuXHJcblx0XHRcdFx0XHRcdGRlZmVycmVkcy5wdXNoKCBtZXNzYWdlU3RvcmUubG9hZCggc291cmNlWyBrZXkgXSwgbG9jYWxlICkgKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuICQud2hlbi5hcHBseSggJCwgZGVmZXJyZWRzICk7XHJcblx0XHRcdH1cclxuXHJcblx0XHR9LFxyXG5cclxuXHRcdC8qKlxyXG5cdFx0ICogU2V0IG1lc3NhZ2VzIHRvIHRoZSBnaXZlbiBsb2NhbGUuXHJcblx0XHQgKiBJZiBsb2NhbGUgZXhpc3RzLCBhZGQgbWVzc2FnZXMgdG8gdGhlIGxvY2FsZS5cclxuXHRcdCAqXHJcblx0XHQgKiBAcGFyYW0ge3N0cmluZ30gbG9jYWxlXHJcblx0XHQgKiBAcGFyYW0ge09iamVjdH0gbWVzc2FnZXNcclxuXHRcdCAqL1xyXG5cdFx0c2V0OiBmdW5jdGlvbiAoIGxvY2FsZSwgbWVzc2FnZXMgKSB7XHJcblx0XHRcdGlmICggIXRoaXMubWVzc2FnZXNbIGxvY2FsZSBdICkge1xyXG5cdFx0XHRcdHRoaXMubWVzc2FnZXNbIGxvY2FsZSBdID0gbWVzc2FnZXM7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0dGhpcy5tZXNzYWdlc1sgbG9jYWxlIF0gPSAkLmV4dGVuZCggdGhpcy5tZXNzYWdlc1sgbG9jYWxlIF0sIG1lc3NhZ2VzICk7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblxyXG5cdFx0LyoqXHJcblx0XHQgKlxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IGxvY2FsZVxyXG5cdFx0ICogQHBhcmFtIHtzdHJpbmd9IG1lc3NhZ2VLZXlcclxuXHRcdCAqIEByZXR1cm4ge2Jvb2xlYW59XHJcblx0XHQgKi9cclxuXHRcdGdldDogZnVuY3Rpb24gKCBsb2NhbGUsIG1lc3NhZ2VLZXkgKSB7XHJcblx0XHRcdHJldHVybiB0aGlzLm1lc3NhZ2VzWyBsb2NhbGUgXSAmJiB0aGlzLm1lc3NhZ2VzWyBsb2NhbGUgXVsgbWVzc2FnZUtleSBdO1xyXG5cdFx0fVxyXG5cdH07XHJcblxyXG5cdCQuZXh0ZW5kKCAkLmkxOG4ubWVzc2FnZVN0b3JlLCBuZXcgTWVzc2FnZVN0b3JlKCkgKTtcclxufSggalF1ZXJ5ICkgKTtcclxuIiwiLyohXHJcbiAqIGpRdWVyeSBJbnRlcm5hdGlvbmFsaXphdGlvbiBsaWJyYXJ5XHJcbiAqXHJcbiAqIENvcHlyaWdodCAoQykgMjAxMS0yMDEzIFNhbnRob3NoIFRob3R0aW5nYWwsIE5laWwgS2FuZGFsZ2FvbmthclxyXG4gKlxyXG4gKiBqcXVlcnkuaTE4biBpcyBkdWFsIGxpY2Vuc2VkIEdQTHYyIG9yIGxhdGVyIGFuZCBNSVQuIFlvdSBkb24ndCBoYXZlIHRvIGRvXHJcbiAqIGFueXRoaW5nIHNwZWNpYWwgdG8gY2hvb3NlIG9uZSBsaWNlbnNlIG9yIHRoZSBvdGhlciBhbmQgeW91IGRvbid0IGhhdmUgdG9cclxuICogbm90aWZ5IGFueW9uZSB3aGljaCBsaWNlbnNlIHlvdSBhcmUgdXNpbmcuIFlvdSBhcmUgZnJlZSB0byB1c2VcclxuICogVW5pdmVyc2FsTGFuZ3VhZ2VTZWxlY3RvciBpbiBjb21tZXJjaWFsIHByb2plY3RzIGFzIGxvbmcgYXMgdGhlIGNvcHlyaWdodFxyXG4gKiBoZWFkZXIgaXMgbGVmdCBpbnRhY3QuIFNlZSBmaWxlcyBHUEwtTElDRU5TRSBhbmQgTUlULUxJQ0VOU0UgZm9yIGRldGFpbHMuXHJcbiAqXHJcbiAqIEBsaWNlbmNlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbmNlIDIuMCBvciBsYXRlclxyXG4gKiBAbGljZW5jZSBNSVQgTGljZW5zZVxyXG4gKi9cclxuXHJcbiggZnVuY3Rpb24gKCAkICkge1xyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHJcblx0dmFyIE1lc3NhZ2VQYXJzZXIgPSBmdW5jdGlvbiAoIG9wdGlvbnMgKSB7XHJcblx0XHR0aGlzLm9wdGlvbnMgPSAkLmV4dGVuZCgge30sICQuaTE4bi5wYXJzZXIuZGVmYXVsdHMsIG9wdGlvbnMgKTtcclxuXHRcdHRoaXMubGFuZ3VhZ2UgPSAkLmkxOG4ubGFuZ3VhZ2VzWyBTdHJpbmcubG9jYWxlIF0gfHwgJC5pMThuLmxhbmd1YWdlc1sgJ2RlZmF1bHQnIF07XHJcblx0XHR0aGlzLmVtaXR0ZXIgPSAkLmkxOG4ucGFyc2VyLmVtaXR0ZXI7XHJcblx0fTtcclxuXHJcblx0TWVzc2FnZVBhcnNlci5wcm90b3R5cGUgPSB7XHJcblxyXG5cdFx0Y29uc3RydWN0b3I6IE1lc3NhZ2VQYXJzZXIsXHJcblxyXG5cdFx0c2ltcGxlUGFyc2U6IGZ1bmN0aW9uICggbWVzc2FnZSwgcGFyYW1ldGVycyApIHtcclxuXHRcdFx0cmV0dXJuIG1lc3NhZ2UucmVwbGFjZSggL1xcJChcXGQrKS9nLCBmdW5jdGlvbiAoIHN0ciwgbWF0Y2ggKSB7XHJcblx0XHRcdFx0dmFyIGluZGV4ID0gcGFyc2VJbnQoIG1hdGNoLCAxMCApIC0gMTtcclxuXHJcblx0XHRcdFx0cmV0dXJuIHBhcmFtZXRlcnNbIGluZGV4IF0gIT09IHVuZGVmaW5lZCA/IHBhcmFtZXRlcnNbIGluZGV4IF0gOiAnJCcgKyBtYXRjaDtcclxuXHRcdFx0fSApO1xyXG5cdFx0fSxcclxuXHJcblx0XHRwYXJzZTogZnVuY3Rpb24gKCBtZXNzYWdlLCByZXBsYWNlbWVudHMgKSB7XHJcblx0XHRcdGlmICggbWVzc2FnZS5pbmRleE9mKCAne3snICkgPCAwICkge1xyXG5cdFx0XHRcdHJldHVybiB0aGlzLnNpbXBsZVBhcnNlKCBtZXNzYWdlLCByZXBsYWNlbWVudHMgKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0dGhpcy5lbWl0dGVyLmxhbmd1YWdlID0gJC5pMThuLmxhbmd1YWdlc1sgJC5pMThuKCkubG9jYWxlIF0gfHxcclxuXHRcdFx0XHQkLmkxOG4ubGFuZ3VhZ2VzWyAnZGVmYXVsdCcgXTtcclxuXHJcblx0XHRcdHJldHVybiB0aGlzLmVtaXR0ZXIuZW1pdCggdGhpcy5hc3QoIG1lc3NhZ2UgKSwgcmVwbGFjZW1lbnRzICk7XHJcblx0XHR9LFxyXG5cclxuXHRcdGFzdDogZnVuY3Rpb24gKCBtZXNzYWdlICkge1xyXG5cdFx0XHR2YXIgcGlwZSwgY29sb24sIGJhY2tzbGFzaCwgYW55Q2hhcmFjdGVyLCBkb2xsYXIsIGRpZ2l0cywgcmVndWxhckxpdGVyYWwsXHJcblx0XHRcdFx0cmVndWxhckxpdGVyYWxXaXRob3V0QmFyLCByZWd1bGFyTGl0ZXJhbFdpdGhvdXRTcGFjZSwgZXNjYXBlZE9yTGl0ZXJhbFdpdGhvdXRCYXIsXHJcblx0XHRcdFx0ZXNjYXBlZE9yUmVndWxhckxpdGVyYWwsIHRlbXBsYXRlQ29udGVudHMsIHRlbXBsYXRlTmFtZSwgb3BlblRlbXBsYXRlLFxyXG5cdFx0XHRcdGNsb3NlVGVtcGxhdGUsIGV4cHJlc3Npb24sIHBhcmFtRXhwcmVzc2lvbiwgcmVzdWx0LFxyXG5cdFx0XHRcdHBvcyA9IDA7XHJcblxyXG5cdFx0XHQvLyBUcnkgcGFyc2VycyB1bnRpbCBvbmUgd29ya3MsIGlmIG5vbmUgd29yayByZXR1cm4gbnVsbFxyXG5cdFx0XHRmdW5jdGlvbiBjaG9pY2UoIHBhcnNlclN5bnRheCApIHtcclxuXHRcdFx0XHRyZXR1cm4gZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0dmFyIGksIHJlc3VsdDtcclxuXHJcblx0XHRcdFx0XHRmb3IgKCBpID0gMDsgaSA8IHBhcnNlclN5bnRheC5sZW5ndGg7IGkrKyApIHtcclxuXHRcdFx0XHRcdFx0cmVzdWx0ID0gcGFyc2VyU3ludGF4WyBpIF0oKTtcclxuXHJcblx0XHRcdFx0XHRcdGlmICggcmVzdWx0ICE9PSBudWxsICkge1xyXG5cdFx0XHRcdFx0XHRcdHJldHVybiByZXN1bHQ7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRyZXR1cm4gbnVsbDtcclxuXHRcdFx0XHR9O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQvLyBUcnkgc2V2ZXJhbCBwYXJzZXJTeW50YXgtZXMgaW4gYSByb3cuXHJcblx0XHRcdC8vIEFsbCBtdXN0IHN1Y2NlZWQ7IG90aGVyd2lzZSwgcmV0dXJuIG51bGwuXHJcblx0XHRcdC8vIFRoaXMgaXMgdGhlIG9ubHkgZWFnZXIgb25lLlxyXG5cdFx0XHRmdW5jdGlvbiBzZXF1ZW5jZSggcGFyc2VyU3ludGF4ICkge1xyXG5cdFx0XHRcdHZhciBpLCByZXMsXHJcblx0XHRcdFx0XHRvcmlnaW5hbFBvcyA9IHBvcyxcclxuXHRcdFx0XHRcdHJlc3VsdCA9IFtdO1xyXG5cclxuXHRcdFx0XHRmb3IgKCBpID0gMDsgaSA8IHBhcnNlclN5bnRheC5sZW5ndGg7IGkrKyApIHtcclxuXHRcdFx0XHRcdHJlcyA9IHBhcnNlclN5bnRheFsgaSBdKCk7XHJcblxyXG5cdFx0XHRcdFx0aWYgKCByZXMgPT09IG51bGwgKSB7XHJcblx0XHRcdFx0XHRcdHBvcyA9IG9yaWdpbmFsUG9zO1xyXG5cclxuXHRcdFx0XHRcdFx0cmV0dXJuIG51bGw7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0cmVzdWx0LnB1c2goIHJlcyApO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0Ly8gUnVuIHRoZSBzYW1lIHBhcnNlciBvdmVyIGFuZCBvdmVyIHVudGlsIGl0IGZhaWxzLlxyXG5cdFx0XHQvLyBNdXN0IHN1Y2NlZWQgYSBtaW5pbXVtIG9mIG4gdGltZXM7IG90aGVyd2lzZSwgcmV0dXJuIG51bGwuXHJcblx0XHRcdGZ1bmN0aW9uIG5Pck1vcmUoIG4sIHAgKSB7XHJcblx0XHRcdFx0cmV0dXJuIGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRcdHZhciBvcmlnaW5hbFBvcyA9IHBvcyxcclxuXHRcdFx0XHRcdFx0cmVzdWx0ID0gW10sXHJcblx0XHRcdFx0XHRcdHBhcnNlZCA9IHAoKTtcclxuXHJcblx0XHRcdFx0XHR3aGlsZSAoIHBhcnNlZCAhPT0gbnVsbCApIHtcclxuXHRcdFx0XHRcdFx0cmVzdWx0LnB1c2goIHBhcnNlZCApO1xyXG5cdFx0XHRcdFx0XHRwYXJzZWQgPSBwKCk7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0aWYgKCByZXN1bHQubGVuZ3RoIDwgbiApIHtcclxuXHRcdFx0XHRcdFx0cG9zID0gb3JpZ2luYWxQb3M7XHJcblxyXG5cdFx0XHRcdFx0XHRyZXR1cm4gbnVsbDtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdFx0XHRcdH07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdC8vIEhlbHBlcnMgLS0ganVzdCBtYWtlIHBhcnNlclN5bnRheCBvdXQgb2Ygc2ltcGxlciBKUyBidWlsdGluIHR5cGVzXHJcblxyXG5cdFx0XHRmdW5jdGlvbiBtYWtlU3RyaW5nUGFyc2VyKCBzICkge1xyXG5cdFx0XHRcdHZhciBsZW4gPSBzLmxlbmd0aDtcclxuXHJcblx0XHRcdFx0cmV0dXJuIGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRcdHZhciByZXN1bHQgPSBudWxsO1xyXG5cclxuXHRcdFx0XHRcdGlmICggbWVzc2FnZS5zbGljZSggcG9zLCBwb3MgKyBsZW4gKSA9PT0gcyApIHtcclxuXHRcdFx0XHRcdFx0cmVzdWx0ID0gcztcclxuXHRcdFx0XHRcdFx0cG9zICs9IGxlbjtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdFx0XHRcdH07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGZ1bmN0aW9uIG1ha2VSZWdleFBhcnNlciggcmVnZXggKSB7XHJcblx0XHRcdFx0cmV0dXJuIGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRcdHZhciBtYXRjaGVzID0gbWVzc2FnZS5zbGljZSggcG9zICkubWF0Y2goIHJlZ2V4ICk7XHJcblxyXG5cdFx0XHRcdFx0aWYgKCBtYXRjaGVzID09PSBudWxsICkge1xyXG5cdFx0XHRcdFx0XHRyZXR1cm4gbnVsbDtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRwb3MgKz0gbWF0Y2hlc1sgMCBdLmxlbmd0aDtcclxuXHJcblx0XHRcdFx0XHRyZXR1cm4gbWF0Y2hlc1sgMCBdO1xyXG5cdFx0XHRcdH07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHBpcGUgPSBtYWtlU3RyaW5nUGFyc2VyKCAnfCcgKTtcclxuXHRcdFx0Y29sb24gPSBtYWtlU3RyaW5nUGFyc2VyKCAnOicgKTtcclxuXHRcdFx0YmFja3NsYXNoID0gbWFrZVN0cmluZ1BhcnNlciggJ1xcXFwnICk7XHJcblx0XHRcdGFueUNoYXJhY3RlciA9IG1ha2VSZWdleFBhcnNlciggL14uLyApO1xyXG5cdFx0XHRkb2xsYXIgPSBtYWtlU3RyaW5nUGFyc2VyKCAnJCcgKTtcclxuXHRcdFx0ZGlnaXRzID0gbWFrZVJlZ2V4UGFyc2VyKCAvXlxcZCsvICk7XHJcblx0XHRcdHJlZ3VsYXJMaXRlcmFsID0gbWFrZVJlZ2V4UGFyc2VyKCAvXltee31cXFtcXF0kXFxcXF0vICk7XHJcblx0XHRcdHJlZ3VsYXJMaXRlcmFsV2l0aG91dEJhciA9IG1ha2VSZWdleFBhcnNlciggL15bXnt9XFxbXFxdJFxcXFx8XS8gKTtcclxuXHRcdFx0cmVndWxhckxpdGVyYWxXaXRob3V0U3BhY2UgPSBtYWtlUmVnZXhQYXJzZXIoIC9eW157fVxcW1xcXSRcXHNdLyApO1xyXG5cclxuXHRcdFx0Ly8gVGhlcmUgaXMgYSBnZW5lcmFsIHBhdHRlcm46XHJcblx0XHRcdC8vIHBhcnNlIGEgdGhpbmc7XHJcblx0XHRcdC8vIGlmIGl0IHdvcmtlZCwgYXBwbHkgdHJhbnNmb3JtLFxyXG5cdFx0XHQvLyBvdGhlcndpc2UgcmV0dXJuIG51bGwuXHJcblx0XHRcdC8vIEJ1dCB1c2luZyB0aGlzIGFzIGEgY29tYmluYXRvciBzZWVtcyB0byBjYXVzZSBwcm9ibGVtc1xyXG5cdFx0XHQvLyB3aGVuIGNvbWJpbmVkIHdpdGggbk9yTW9yZSgpLlxyXG5cdFx0XHQvLyBNYXkgYmUgc29tZSBzY29waW5nIGlzc3VlLlxyXG5cdFx0XHRmdW5jdGlvbiB0cmFuc2Zvcm0oIHAsIGZuICkge1xyXG5cdFx0XHRcdHJldHVybiBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHR2YXIgcmVzdWx0ID0gcCgpO1xyXG5cclxuXHRcdFx0XHRcdHJldHVybiByZXN1bHQgPT09IG51bGwgPyBudWxsIDogZm4oIHJlc3VsdCApO1xyXG5cdFx0XHRcdH07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdC8vIFVzZWQgdG8gZGVmaW5lIFwibGl0ZXJhbHNcIiB3aXRoaW4gdGVtcGxhdGUgcGFyYW1ldGVycy4gVGhlIHBpcGVcclxuXHRcdFx0Ly8gY2hhcmFjdGVyIGlzIHRoZSBwYXJhbWV0ZXIgZGVsaW1ldGVyLCBzbyBieSBkZWZhdWx0XHJcblx0XHRcdC8vIGl0IGlzIG5vdCBhIGxpdGVyYWwgaW4gdGhlIHBhcmFtZXRlclxyXG5cdFx0XHRmdW5jdGlvbiBsaXRlcmFsV2l0aG91dEJhcigpIHtcclxuXHRcdFx0XHR2YXIgcmVzdWx0ID0gbk9yTW9yZSggMSwgZXNjYXBlZE9yTGl0ZXJhbFdpdGhvdXRCYXIgKSgpO1xyXG5cclxuXHRcdFx0XHRyZXR1cm4gcmVzdWx0ID09PSBudWxsID8gbnVsbCA6IHJlc3VsdC5qb2luKCAnJyApO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRmdW5jdGlvbiBsaXRlcmFsKCkge1xyXG5cdFx0XHRcdHZhciByZXN1bHQgPSBuT3JNb3JlKCAxLCBlc2NhcGVkT3JSZWd1bGFyTGl0ZXJhbCApKCk7XHJcblxyXG5cdFx0XHRcdHJldHVybiByZXN1bHQgPT09IG51bGwgPyBudWxsIDogcmVzdWx0LmpvaW4oICcnICk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGZ1bmN0aW9uIGVzY2FwZWRMaXRlcmFsKCkge1xyXG5cdFx0XHRcdHZhciByZXN1bHQgPSBzZXF1ZW5jZSggWyBiYWNrc2xhc2gsIGFueUNoYXJhY3RlciBdICk7XHJcblxyXG5cdFx0XHRcdHJldHVybiByZXN1bHQgPT09IG51bGwgPyBudWxsIDogcmVzdWx0WyAxIF07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGNob2ljZSggWyBlc2NhcGVkTGl0ZXJhbCwgcmVndWxhckxpdGVyYWxXaXRob3V0U3BhY2UgXSApO1xyXG5cdFx0XHRlc2NhcGVkT3JMaXRlcmFsV2l0aG91dEJhciA9IGNob2ljZSggWyBlc2NhcGVkTGl0ZXJhbCwgcmVndWxhckxpdGVyYWxXaXRob3V0QmFyIF0gKTtcclxuXHRcdFx0ZXNjYXBlZE9yUmVndWxhckxpdGVyYWwgPSBjaG9pY2UoIFsgZXNjYXBlZExpdGVyYWwsIHJlZ3VsYXJMaXRlcmFsIF0gKTtcclxuXHJcblx0XHRcdGZ1bmN0aW9uIHJlcGxhY2VtZW50KCkge1xyXG5cdFx0XHRcdHZhciByZXN1bHQgPSBzZXF1ZW5jZSggWyBkb2xsYXIsIGRpZ2l0cyBdICk7XHJcblxyXG5cdFx0XHRcdGlmICggcmVzdWx0ID09PSBudWxsICkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIG51bGw7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRyZXR1cm4gWyAnUkVQTEFDRScsIHBhcnNlSW50KCByZXN1bHRbIDEgXSwgMTAgKSAtIDEgXTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0dGVtcGxhdGVOYW1lID0gdHJhbnNmb3JtKFxyXG5cdFx0XHRcdC8vIHNlZSAkd2dMZWdhbFRpdGxlQ2hhcnNcclxuXHRcdFx0XHQvLyBub3QgYWxsb3dpbmcgOiBkdWUgdG8gdGhlIG5lZWQgdG8gY2F0Y2ggXCJQTFVSQUw6JDFcIlxyXG5cdFx0XHRcdG1ha2VSZWdleFBhcnNlciggL15bICFcIiQmJygpKiwuXFwvMC05Oz0/QEEtWlxcXl9gYS16flxceDgwLVxceEZGK1xcLV0rLyApLFxyXG5cclxuXHRcdFx0XHRmdW5jdGlvbiAoIHJlc3VsdCApIHtcclxuXHRcdFx0XHRcdHJldHVybiByZXN1bHQudG9TdHJpbmcoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdCk7XHJcblxyXG5cdFx0XHRmdW5jdGlvbiB0ZW1wbGF0ZVBhcmFtKCkge1xyXG5cdFx0XHRcdHZhciBleHByLFxyXG5cdFx0XHRcdFx0cmVzdWx0ID0gc2VxdWVuY2UoIFsgcGlwZSwgbk9yTW9yZSggMCwgcGFyYW1FeHByZXNzaW9uICkgXSApO1xyXG5cclxuXHRcdFx0XHRpZiAoIHJlc3VsdCA9PT0gbnVsbCApIHtcclxuXHRcdFx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0ZXhwciA9IHJlc3VsdFsgMSBdO1xyXG5cclxuXHRcdFx0XHQvLyB1c2UgYSBcIkNPTkNBVFwiIG9wZXJhdG9yIGlmIHRoZXJlIGFyZSBtdWx0aXBsZSBub2RlcyxcclxuXHRcdFx0XHQvLyBvdGhlcndpc2UgcmV0dXJuIHRoZSBmaXJzdCBub2RlLCByYXcuXHJcblx0XHRcdFx0cmV0dXJuIGV4cHIubGVuZ3RoID4gMSA/IFsgJ0NPTkNBVCcgXS5jb25jYXQoIGV4cHIgKSA6IGV4cHJbIDAgXTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0ZnVuY3Rpb24gdGVtcGxhdGVXaXRoUmVwbGFjZW1lbnQoKSB7XHJcblx0XHRcdFx0dmFyIHJlc3VsdCA9IHNlcXVlbmNlKCBbIHRlbXBsYXRlTmFtZSwgY29sb24sIHJlcGxhY2VtZW50IF0gKTtcclxuXHJcblx0XHRcdFx0cmV0dXJuIHJlc3VsdCA9PT0gbnVsbCA/IG51bGwgOiBbIHJlc3VsdFsgMCBdLCByZXN1bHRbIDIgXSBdO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRmdW5jdGlvbiB0ZW1wbGF0ZVdpdGhPdXRSZXBsYWNlbWVudCgpIHtcclxuXHRcdFx0XHR2YXIgcmVzdWx0ID0gc2VxdWVuY2UoIFsgdGVtcGxhdGVOYW1lLCBjb2xvbiwgcGFyYW1FeHByZXNzaW9uIF0gKTtcclxuXHJcblx0XHRcdFx0cmV0dXJuIHJlc3VsdCA9PT0gbnVsbCA/IG51bGwgOiBbIHJlc3VsdFsgMCBdLCByZXN1bHRbIDIgXSBdO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHR0ZW1wbGF0ZUNvbnRlbnRzID0gY2hvaWNlKCBbXHJcblx0XHRcdFx0ZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0dmFyIHJlcyA9IHNlcXVlbmNlKCBbXHJcblx0XHRcdFx0XHRcdC8vIHRlbXBsYXRlcyBjYW4gaGF2ZSBwbGFjZWhvbGRlcnMgZm9yIGR5bmFtaWNcclxuXHRcdFx0XHRcdFx0Ly8gcmVwbGFjZW1lbnQgZWc6IHt7UExVUkFMOiQxfG9uZSBjYXJ8JDEgY2Fyc319XHJcblx0XHRcdFx0XHRcdC8vIG9yIG5vIHBsYWNlaG9sZGVycyBlZzpcclxuXHRcdFx0XHRcdFx0Ly8ge3tHUkFNTUFSOmdlbml0aXZlfHt7U0lURU5BTUV9fX1cclxuXHRcdFx0XHRcdFx0Y2hvaWNlKCBbIHRlbXBsYXRlV2l0aFJlcGxhY2VtZW50LCB0ZW1wbGF0ZVdpdGhPdXRSZXBsYWNlbWVudCBdICksXHJcblx0XHRcdFx0XHRcdG5Pck1vcmUoIDAsIHRlbXBsYXRlUGFyYW0gKVxyXG5cdFx0XHRcdFx0XSApO1xyXG5cclxuXHRcdFx0XHRcdHJldHVybiByZXMgPT09IG51bGwgPyBudWxsIDogcmVzWyAwIF0uY29uY2F0KCByZXNbIDEgXSApO1xyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0ZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0dmFyIHJlcyA9IHNlcXVlbmNlKCBbIHRlbXBsYXRlTmFtZSwgbk9yTW9yZSggMCwgdGVtcGxhdGVQYXJhbSApIF0gKTtcclxuXHJcblx0XHRcdFx0XHRpZiAoIHJlcyA9PT0gbnVsbCApIHtcclxuXHRcdFx0XHRcdFx0cmV0dXJuIG51bGw7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0cmV0dXJuIFsgcmVzWyAwIF0gXS5jb25jYXQoIHJlc1sgMSBdICk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRdICk7XHJcblxyXG5cdFx0XHRvcGVuVGVtcGxhdGUgPSBtYWtlU3RyaW5nUGFyc2VyKCAne3snICk7XHJcblx0XHRcdGNsb3NlVGVtcGxhdGUgPSBtYWtlU3RyaW5nUGFyc2VyKCAnfX0nICk7XHJcblxyXG5cdFx0XHRmdW5jdGlvbiB0ZW1wbGF0ZSgpIHtcclxuXHRcdFx0XHR2YXIgcmVzdWx0ID0gc2VxdWVuY2UoIFsgb3BlblRlbXBsYXRlLCB0ZW1wbGF0ZUNvbnRlbnRzLCBjbG9zZVRlbXBsYXRlIF0gKTtcclxuXHJcblx0XHRcdFx0cmV0dXJuIHJlc3VsdCA9PT0gbnVsbCA/IG51bGwgOiByZXN1bHRbIDEgXTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0ZXhwcmVzc2lvbiA9IGNob2ljZSggWyB0ZW1wbGF0ZSwgcmVwbGFjZW1lbnQsIGxpdGVyYWwgXSApO1xyXG5cdFx0XHRwYXJhbUV4cHJlc3Npb24gPSBjaG9pY2UoIFsgdGVtcGxhdGUsIHJlcGxhY2VtZW50LCBsaXRlcmFsV2l0aG91dEJhciBdICk7XHJcblxyXG5cdFx0XHRmdW5jdGlvbiBzdGFydCgpIHtcclxuXHRcdFx0XHR2YXIgcmVzdWx0ID0gbk9yTW9yZSggMCwgZXhwcmVzc2lvbiApKCk7XHJcblxyXG5cdFx0XHRcdGlmICggcmVzdWx0ID09PSBudWxsICkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIG51bGw7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRyZXR1cm4gWyAnQ09OQ0FUJyBdLmNvbmNhdCggcmVzdWx0ICk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJlc3VsdCA9IHN0YXJ0KCk7XHJcblxyXG5cdFx0XHQvKlxyXG5cdFx0XHQgKiBGb3Igc3VjY2VzcywgdGhlIHBvcyBtdXN0IGhhdmUgZ290dGVuIHRvIHRoZSBlbmQgb2YgdGhlIGlucHV0XHJcblx0XHRcdCAqIGFuZCByZXR1cm5lZCBhIG5vbi1udWxsLlxyXG5cdFx0XHQgKiBuLmIuIFRoaXMgaXMgcGFydCBvZiBsYW5ndWFnZSBpbmZyYXN0cnVjdHVyZSwgc28gd2UgZG8gbm90IHRocm93IGFuIGludGVybmF0aW9uYWxpemFibGUgbWVzc2FnZS5cclxuXHRcdFx0ICovXHJcblx0XHRcdGlmICggcmVzdWx0ID09PSBudWxsIHx8IHBvcyAhPT0gbWVzc2FnZS5sZW5ndGggKSB7XHJcblx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKCAnUGFyc2UgZXJyb3IgYXQgcG9zaXRpb24gJyArIHBvcy50b1N0cmluZygpICsgJyBpbiBpbnB1dDogJyArIG1lc3NhZ2UgKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cmV0dXJuIHJlc3VsdDtcclxuXHRcdH1cclxuXHJcblx0fTtcclxuXHJcblx0JC5leHRlbmQoICQuaTE4bi5wYXJzZXIsIG5ldyBNZXNzYWdlUGFyc2VyKCkgKTtcclxufSggalF1ZXJ5ICkgKTtcclxuIiwiLyohIGpzLXVybCAtIHYyLjUuMiAtIDIwMTctMDgtMzAgKi8hZnVuY3Rpb24oKXt2YXIgYT1mdW5jdGlvbigpe2Z1bmN0aW9uIGEoKXt9ZnVuY3Rpb24gYihhKXtyZXR1cm4gZGVjb2RlVVJJQ29tcG9uZW50KGEucmVwbGFjZSgvXFwrL2csXCIgXCIpKX1mdW5jdGlvbiBjKGEsYil7dmFyIGM9YS5jaGFyQXQoMCksZD1iLnNwbGl0KGMpO3JldHVybiBjPT09YT9kOihhPXBhcnNlSW50KGEuc3Vic3RyaW5nKDEpLDEwKSxkW2E8MD9kLmxlbmd0aCthOmEtMV0pfWZ1bmN0aW9uIGQoYSxjKXtmb3IodmFyIGQ9YS5jaGFyQXQoMCksZT1jLnNwbGl0KFwiJlwiKSxmPVtdLGc9e30saD1bXSxpPWEuc3Vic3RyaW5nKDEpLGo9MCxrPWUubGVuZ3RoO2o8aztqKyspaWYoZj1lW2pdLm1hdGNoKC8oLio/KT0oLiopLyksZnx8KGY9W2Vbal0sZVtqXSxcIlwiXSksXCJcIiE9PWZbMV0ucmVwbGFjZSgvXFxzL2csXCJcIikpe2lmKGZbMl09YihmWzJdfHxcIlwiKSxpPT09ZlsxXSlyZXR1cm4gZlsyXTtoPWZbMV0ubWF0Y2goLyguKilcXFsoWzAtOV0rKVxcXS8pLGg/KGdbaFsxXV09Z1toWzFdXXx8W10sZ1toWzFdXVtoWzJdXT1mWzJdKTpnW2ZbMV1dPWZbMl19cmV0dXJuIGQ9PT1hP2c6Z1tpXX1yZXR1cm4gZnVuY3Rpb24oYixlKXt2YXIgZixnPXt9O2lmKFwidGxkP1wiPT09YilyZXR1cm4gYSgpO2lmKGU9ZXx8d2luZG93LmxvY2F0aW9uLnRvU3RyaW5nKCksIWIpcmV0dXJuIGU7aWYoYj1iLnRvU3RyaW5nKCksZj1lLm1hdGNoKC9ebWFpbHRvOihbXlxcL10uKykvKSlnLnByb3RvY29sPVwibWFpbHRvXCIsZy5lbWFpbD1mWzFdO2Vsc2V7aWYoKGY9ZS5tYXRjaCgvKC4qPylcXC8jXFwhKC4qKS8pKSYmKGU9ZlsxXStmWzJdKSwoZj1lLm1hdGNoKC8oLio/KSMoLiopLykpJiYoZy5oYXNoPWZbMl0sZT1mWzFdKSxnLmhhc2gmJmIubWF0Y2goL14jLykpcmV0dXJuIGQoYixnLmhhc2gpO2lmKChmPWUubWF0Y2goLyguKj8pXFw/KC4qKS8pKSYmKGcucXVlcnk9ZlsyXSxlPWZbMV0pLGcucXVlcnkmJmIubWF0Y2goL15cXD8vKSlyZXR1cm4gZChiLGcucXVlcnkpO2lmKChmPWUubWF0Y2goLyguKj8pXFw6P1xcL1xcLyguKikvKSkmJihnLnByb3RvY29sPWZbMV0udG9Mb3dlckNhc2UoKSxlPWZbMl0pLChmPWUubWF0Y2goLyguKj8pKFxcLy4qKS8pKSYmKGcucGF0aD1mWzJdLGU9ZlsxXSksZy5wYXRoPShnLnBhdGh8fFwiXCIpLnJlcGxhY2UoL14oW15cXC9dKS8sXCIvJDFcIiksYi5tYXRjaCgvXltcXC0wLTldKyQvKSYmKGI9Yi5yZXBsYWNlKC9eKFteXFwvXSkvLFwiLyQxXCIpKSxiLm1hdGNoKC9eXFwvLykpcmV0dXJuIGMoYixnLnBhdGguc3Vic3RyaW5nKDEpKTtpZihmPWMoXCIvLTFcIixnLnBhdGguc3Vic3RyaW5nKDEpKSxmJiYoZj1mLm1hdGNoKC8oLio/KVxcLiguKikvKSkmJihnLmZpbGU9ZlswXSxnLmZpbGVuYW1lPWZbMV0sZy5maWxlZXh0PWZbMl0pLChmPWUubWF0Y2goLyguKilcXDooWzAtOV0rKSQvKSkmJihnLnBvcnQ9ZlsyXSxlPWZbMV0pLChmPWUubWF0Y2goLyguKj8pQCguKikvKSkmJihnLmF1dGg9ZlsxXSxlPWZbMl0pLGcuYXV0aCYmKGY9Zy5hdXRoLm1hdGNoKC8oLiopXFw6KC4qKS8pLGcudXNlcj1mP2ZbMV06Zy5hdXRoLGcucGFzcz1mP2ZbMl06dm9pZCAwKSxnLmhvc3RuYW1lPWUudG9Mb3dlckNhc2UoKSxcIi5cIj09PWIuY2hhckF0KDApKXJldHVybiBjKGIsZy5ob3N0bmFtZSk7YSgpJiYoZj1nLmhvc3RuYW1lLm1hdGNoKGEoKSksZiYmKGcudGxkPWZbM10sZy5kb21haW49ZlsyXT9mWzJdK1wiLlwiK2ZbM106dm9pZCAwLGcuc3ViPWZbMV18fHZvaWQgMCkpLGcucG9ydD1nLnBvcnR8fChcImh0dHBzXCI9PT1nLnByb3RvY29sP1wiNDQzXCI6XCI4MFwiKSxnLnByb3RvY29sPWcucHJvdG9jb2x8fChcIjQ0M1wiPT09Zy5wb3J0P1wiaHR0cHNcIjpcImh0dHBcIil9cmV0dXJuIGIgaW4gZz9nW2JdOlwie31cIj09PWI/Zzp2b2lkIDB9fSgpO1wiZnVuY3Rpb25cIj09dHlwZW9mIHdpbmRvdy5kZWZpbmUmJndpbmRvdy5kZWZpbmUuYW1kP3dpbmRvdy5kZWZpbmUoXCJqcy11cmxcIixbXSxmdW5jdGlvbigpe3JldHVybiBhfSk6KFwidW5kZWZpbmVkXCIhPXR5cGVvZiB3aW5kb3cualF1ZXJ5JiZ3aW5kb3cualF1ZXJ5LmV4dGVuZCh7dXJsOmZ1bmN0aW9uKGEsYil7cmV0dXJuIHdpbmRvdy51cmwoYSxiKX19KSx3aW5kb3cudXJsPWEpfSgpO1xyXG4iLCJ2YXIgbWFwQ29udHJvbGxlciA9IHJlcXVpcmUoXCIuL21hcFByZXNlbnRlci5qc1wiKTtcclxuXHJcbnZhciBsYW5nSW1hZ2VzID0ge1xyXG4gICAgZ2w6IFwiYXNzZXRzL2ltYWdlcy9nbC5wbmdcIixcclxuICAgIGVzOiBcImFzc2V0cy9pbWFnZXMvZXMucG5nXCJcclxufTtcclxuXHJcbnZhciBsYW5nVGV4dHMgPSB7XHJcbiAgICBnbDogXCJHYWxlZ29cIixcclxuICAgIGVzOiBcIkNhc3RlbGxhbm9cIlxyXG59O1xyXG5cclxuLy8gRW5hYmxlIGRlYnVnXHJcbiQuaTE4bi5kZWJ1ZyA9IHRydWU7XHJcblxyXG52YXIgY3VycmVudExvY2FsZSA9IHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50TG9jYWxlXCIpO1xyXG5cclxuZnVuY3Rpb24gc2V0X2xvY2FsZV90byhsb2NhbGUpIHtcclxuICAgIGlmIChsb2NhbGUpICQuaTE4bigpLmxvY2FsZSA9IGxvY2FsZTtcclxufVxyXG5cclxualF1ZXJ5KGZ1bmN0aW9uIHVwZGF0ZUxvY2FsZSgkKSB7XHJcbiAgICBcInVzZSBzdHJpY3RcIjtcclxuICAgIHZhciBpMThuID0gJC5pMThuKCk7XHJcblxyXG4gICAgaTE4bi5sb2NhbGUgPSBjdXJyZW50TG9jYWxlO1xyXG5cclxuICAgIGkxOG5cclxuICAgICAgICAubG9hZCh7XHJcbiAgICAgICAgICAgIGVzOiBcIi4vc3JjL2kxOG4vbWFwX2VzLmpzb25cIixcclxuICAgICAgICAgICAgZ2w6IFwiLi9zcmMvaTE4bi9tYXBfZ2wuanNvblwiXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuZG9uZShmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJJbnRlcm5hdGlvbmFsaXphdGlvbiBmaWxlcyBsb2FkZWQgY29ycmVjdGx5XCIpO1xyXG5cclxuICAgICAgICAgICAgJChcIiN0aXRsZWltYWdlXCIpLmF0dHIoXHJcbiAgICAgICAgICAgICAgICBcInNyY1wiLCBsYW5nSW1hZ2VzW2kxOG4ubG9jYWxlXSk7XHJcblxyXG4gICAgICAgICAgICAkKFwiI3RpdGxldGV4dFwiKS50ZXh0KGxhbmdUZXh0c1tpMThuLmxvY2FsZV0pO1xyXG5cclxuICAgICAgICAgICAgSGlzdG9yeS5BZGFwdGVyLmJpbmQod2luZG93LCBcInN0YXRlY2hhbmdlXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgc2V0X2xvY2FsZV90byh1cmwoXCI/bG9jYWxlXCIpKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBIaXN0b3J5LnB1c2hTdGF0ZShudWxsLCBudWxsLCBcIj9sb2NhbGU9XCIgKyBpMThuLmxvY2FsZSk7XHJcbiAgICAgICAgICAgIG1hcENvbnRyb2xsZXIudXBkYXRlUGFnZUVsZW1lbnRzKCk7XHJcbiAgICAgICAgICAgIG1hcENvbnRyb2xsZXIuc2hvd0xldmVsVW5sb2NrZWQoKTtcclxuXHJcbiAgICAgICAgICAgICQoXCIuc3dpdGNoLWxvY2FsZVwiKS5vbihcImNsaWNrXCIsIFwiYVwiLCBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImxvY2FsZSBjaGFuZ2VkXCIpO1xyXG4gICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgSGlzdG9yeS5wdXNoU3RhdGUobnVsbCwgbnVsbCwgXCI/bG9jYWxlPVwiICsgJCh0aGlzKS5kYXRhKFwibG9jYWxlXCIpKTtcclxuICAgICAgICAgICAgICAgICQuaTE4bigpLmxvY2FsZSA9ICQodGhpcykuZGF0YShcImxvY2FsZVwiKTtcclxuICAgICAgICAgICAgICAgIG1hcENvbnRyb2xsZXIudXBkYXRlUGFnZUVsZW1lbnRzKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgJChcIi5sYW5ndWFnZXMgPiBsaVwiKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICQoXCIjdGl0bGVpbWFnZVwiKS5hdHRyKFxyXG4gICAgICAgICAgICAgICAgICAgIFwic3JjXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgJCh0aGlzKVxyXG4gICAgICAgICAgICAgICAgICAgIC5jaGlsZHJlbigpXHJcbiAgICAgICAgICAgICAgICAgICAgLmNoaWxkcmVuKFwiaW1nXCIpXHJcbiAgICAgICAgICAgICAgICAgICAgLmF0dHIoXCJzcmNcIilcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAkKFwiI3RpdGxldGV4dFwiKS50ZXh0KCQodGhpcykudGV4dCgpKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbn0pOyIsInZhciBqc29uTG9hZGVyID0gcmVxdWlyZShcIi4vanNvbkxvYWRlci5qc1wiKTtcclxuXHJcbnZhciBnbG9iYWxDb25maWcgPSBqc29uTG9hZGVyLmxvYWRKc29uRmlsZShcImNvbmYvZ2xvYmFsX2NvbmZpZy5qc29uXCIpO1xyXG5cclxudmFyIGxldmVsc1VubG9ja2VkID0gW2dsb2JhbENvbmZpZy5maXJzdF9sZXZlbF91bmxvY2tlZF07XHJcblxyXG52YXIgY29sb3JzID0ge307XHJcblxyXG52YXIgZ2xvYmFsTGV2ZWxzID0gZ2xvYmFsQ29uZmlnLmxldmVscztcclxuXHJcbnZhciBsZXZlbFVubG9ja2VkO1xyXG5cclxudmFyIGZpcnN0TGV2ZWxDb2RlID0gZ2xvYmFsQ29uZmlnLmZpcnN0X2xldmVsX3VubG9ja2VkLmNvZGUucmVwbGFjZSgvW1wiXSsvZywgJycpO1xyXG5cclxudmFyIGxldmVsRmlsZXMgPSB7fTtcclxuXHJcbnZhciBfaW5pdCA9IGZ1bmN0aW9uICgpIHtcclxuICAgIGlmIChzZXNzaW9uU3RvcmFnZVtcImxldmVsc1VubG9ja2VkXCJdID09IG51bGwpIHtcclxuICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFwibGV2ZWxzVW5sb2NrZWRcIiwgSlNPTi5zdHJpbmdpZnkobGV2ZWxzVW5sb2NrZWQpKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbGV2ZWxzVW5sb2NrZWQgPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJsZXZlbHNVbmxvY2tlZFwiKSk7XHJcbiAgICB9XHJcblxyXG4gICAgbGV2ZWxGaWxlc1tmaXJzdExldmVsQ29kZV0gPSBnbG9iYWxDb25maWcuZmlyc3RfbGV2ZWxfZmlsZTtcclxuXHJcbiAgICBpZiAoc2Vzc2lvblN0b3JhZ2VbXCJsZXZlbEZpbGVzXCJdID09IG51bGwpIHtcclxuICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFwibGV2ZWxGaWxlc1wiLCBKU09OLnN0cmluZ2lmeShsZXZlbEZpbGVzKSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGxldmVsRmlsZXMgPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJsZXZlbEZpbGVzXCIpKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoc2Vzc2lvblN0b3JhZ2VbXCJjdXJyZW50TG9jYWxlXCJdID09IG51bGwpIHtcclxuICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudExvY2FsZVwiLCBcImdsXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIGZvciAodmFyIGxldmVsIGluIGdsb2JhbExldmVscykge1xyXG4gICAgICAgIGxldmVsVW5sb2NrZWQgPSBmYWxzZTtcclxuICAgICAgICBmb3IgKHZhciB1bmxvY2tlZCBpbiBsZXZlbHNVbmxvY2tlZCkge1xyXG4gICAgICAgICAgICBpZiAobGV2ZWxzVW5sb2NrZWRbdW5sb2NrZWRdLmNvZGUgPT0gZ2xvYmFsTGV2ZWxzW2xldmVsXSkge1xyXG4gICAgICAgICAgICAgICAgY29sb3JzW2dsb2JhbExldmVsc1tsZXZlbF1dID0gZ2xvYmFsQ29uZmlnLnVubG9ja2VkX2xldmVsX2NvbG9yO1xyXG4gICAgICAgICAgICAgICAgbGV2ZWxVbmxvY2tlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIWxldmVsVW5sb2NrZWQpIHtcclxuICAgICAgICAgICAgY29sb3JzW2dsb2JhbExldmVsc1tsZXZlbF1dID0gZ2xvYmFsQ29uZmlnLmxvY2tlZF9sZXZlbF9jb2xvcjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHNlc3Npb25TdG9yYWdlW1wiY29sb3JzXCJdID09IG51bGwpIHtcclxuICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFwiY29sb3JzXCIsIEpTT04uc3RyaW5naWZ5KGNvbG9ycykpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBjb2xvcnMgPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJjb2xvcnNcIikpO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZnVuY3Rpb24gZ2V0TGV2ZWxUZXh0KGxldmVsKSB7XHJcbiAgICB2YXIgbGV2ZWxUZXh0ID0gJC5pMThuKFwiZGVzY3JpcHRpb25fbGV2ZWxfXCIuY29uY2F0KGxldmVsKSk7XHJcbiAgICB2YXIgdG9yZXQgPSBcIlwiO1xyXG4gICAgdmFyIGFyciA9IGxldmVsVGV4dC5zcGxpdChcIn5cIik7XHJcbiAgICBmb3IgKGluZGV4ID0gMDsgaW5kZXggPCBhcnIubGVuZ3RoOyArK2luZGV4KSB7XHJcbiAgICAgICAgdmFyIHN0ciA9IGFycltpbmRleF0ucmVwbGFjZSgvXi8sIFwiPGxpPlwiKS5jb25jYXQoXCI8L2xpPlwiKTtcclxuICAgICAgICB0b3JldCArPSBzdHI7XHJcbiAgICB9XHJcbiAgICB0b3JldC5yZXBsYWNlKC9eLywgXCI8dWw+XCIpLmNvbmNhdChcIjwvdWw+XCIpO1xyXG4gICAgcmV0dXJuIHRvcmV0O1xyXG59XHJcblxyXG5mdW5jdGlvbiBzaG93TGV2ZWxEZXNjcmlwdGlvbihjdXJyZW50TGV2ZWwsIGxldmVsVGV4dCkge1xyXG4gICAgc3dhbCh7XHJcbiAgICAgICAgdGl0bGU6IFwiTml2ZWwgXCIgKyBjdXJyZW50TGV2ZWwgKyBcIjogXCIgKyAkLmkxOG4oXCJsZXZlbF9cIi5jb25jYXQoY3VycmVudExldmVsKSksXHJcbiAgICAgICAgaHRtbDogbGV2ZWxUZXh0LFxyXG4gICAgICAgIGFsbG93T3V0c2lkZUNsaWNrOiBmYWxzZSxcclxuICAgICAgICBhbGxvd0VzY2FwZUtleTogZmFsc2UsXHJcbiAgICAgICAgdGltZXI6IDYwMDAsXHJcblxyXG4gICAgICAgIG9uT3BlbjogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2YXIgYiA9IHN3YWwuZ2V0Q29uZmlybUJ1dHRvbigpO1xyXG4gICAgICAgICAgICBiLmhpZGRlbiA9IHRydWU7XHJcbiAgICAgICAgICAgIGIuZGlzYWJsZWQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH0pLnRoZW4oZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHN3YWwoe1xyXG4gICAgICAgICAgICB0aXRsZTogXCJOaXZlbCBcIiArIGN1cnJlbnRMZXZlbCArIFwiOiBcIiArICQuaTE4bihcImxldmVsX1wiLmNvbmNhdChjdXJyZW50TGV2ZWwpKSxcclxuICAgICAgICAgICAgaHRtbDogbGV2ZWxUZXh0LFxyXG4gICAgICAgICAgICBhbGxvd091dHNpZGVDbGljazogdHJ1ZSxcclxuICAgICAgICAgICAgYWxsb3dFc2NhcGVLZXk6IHRydWUsXHJcbiAgICAgICAgICAgIHNob3dDbG9zZUJ1dHRvbjogdHJ1ZSxcclxuICAgICAgICAgICAgc2hvd0NhbmNlbEJ1dHRvbjogdHJ1ZSxcclxuICAgICAgICAgICAgYW5pbWF0aW9uOiBmYWxzZVxyXG4gICAgICAgIH0pLnRoZW4oKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzdWx0LnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudExvY2FsZVwiLCAkLmkxOG4oKS5sb2NhbGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgbG9jYXRpb24ucmVwbGFjZShcImdhbWUuaHRtbFwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJlc3VsdC5kaXNtaXNzID09PSBzd2FsLkRpc21pc3NSZWFzb24uY2FuY2VsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxufVxyXG5cclxuX2luaXQoKTtcclxuXHJcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciBzZWxlY3RlZFJlZ2lvbnMgPSBbXTtcclxuXHJcbiAgICBmb3IgKHZhciBsZXZlbCBpbiBsZXZlbHNVbmxvY2tlZCkge1xyXG4gICAgICAgIHNlbGVjdGVkUmVnaW9ucy5wdXNoKGxldmVsc1VubG9ja2VkW2xldmVsXS5jb2RlKTtcclxuICAgIH1cclxuXHJcbiAgICBqUXVlcnkoXCIjdm1hcFwiKS52ZWN0b3JNYXAoe1xyXG4gICAgICAgIG1hcDogXCJ3b3JsZF9lblwiLFxyXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogXCIjYTViZmRkXCIsXHJcbiAgICAgICAgYm9yZGVyQ29sb3I6IFwiIzgxODE4MVwiLFxyXG4gICAgICAgIGJvcmRlck9wYWNpdHk6IDAuNSxcclxuICAgICAgICBib3JkZXJXaWR0aDogMSxcclxuICAgICAgICBjb2xvcnM6IGNvbG9ycyxcclxuICAgICAgICBlbmFibGVab29tOiB0cnVlLFxyXG4gICAgICAgIGhvdmVyQ29sb3I6IFwiIzMzZmYzM1wiLFxyXG4gICAgICAgIGhvdmVyT3BhY2l0eTogbnVsbCxcclxuICAgICAgICBub3JtYWxpemVGdW5jdGlvbjogXCJsaW5lYXJcIixcclxuICAgICAgICBzY2FsZUNvbG9yczogW1wiI2I2ZDZmZlwiLCBcIiMwMDVhY2VcIl0sXHJcbiAgICAgICAgc2VsZWN0ZWRDb2xvcjogXCIjMDBjYzAwXCIsXHJcbiAgICAgICAgc2VsZWN0ZWRSZWdpb25zOiBzZWxlY3RlZFJlZ2lvbnMsXHJcbiAgICAgICAgc2hvd1Rvb2x0aXA6IHRydWUsXHJcbiAgICAgICAgb25SZWdpb25DbGljazogZnVuY3Rpb24gKGVsZW1lbnQsIGNvZGUsIHJlZ2lvbikge1xyXG4gICAgICAgICAgICBpZiAoIXNlbGVjdGVkUmVnaW9ucy5pbmNsdWRlcyhjb2RlKSkge1xyXG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIC8vIGlmIChzdXBwb3J0c19odG1sNV9zdG9yYWdlKCkpIHtcclxuICAgICAgICAgICAgICAgIC8vICAgICBpZiAoY29kZSA9PSAnZXMnKSB7XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwibGV2ZWxGaWxlXCIsIFwiMS5qc29uXCIpO1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIH0gZWxzZSBpZiAoY29kZSA9PSAnaG4nKSB7XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwibGV2ZWxGaWxlXCIsIFwiMi5qc29uXCIpO1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oXCJsZXZlbEZpbGVcIiwgbGV2ZWxGaWxlc1tjb2RlXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGN1cnJlbnRMZXZlbCA9IGxldmVsRmlsZXNbY29kZV0uc3BsaXQoXCIuXCIpWzBdO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBsZXZlbFRleHQgPSBnZXRMZXZlbFRleHQoY3VycmVudExldmVsKTtcclxuICAgICAgICAgICAgICAgICAgICBzaG93TGV2ZWxEZXNjcmlwdGlvbihjdXJyZW50TGV2ZWwsIGxldmVsVGV4dCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0KFwiU29ycnksIHlvdXIgYnJvd3NlciBkbyBub3Qgc3VwcG9ydCBzZXNzaW9uIHN0b3JhZ2UuXCIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIG9uTG9hZDogZnVuY3Rpb24gKGV2ZW50LCBtYXApIHsgfSxcclxuICAgICAgICBvbkxhYmVsU2hvdzogZnVuY3Rpb24gKGV2ZW50LCBsYWJlbCwgY29kZSkgeyB9LFxyXG4gICAgICAgIG9uUmVnaW9uT3ZlcjogZnVuY3Rpb24gKGV2ZW50LCBjb2RlLCByZWdpb24pIHtcclxuICAgICAgICAgICAgaWYgKCFzZWxlY3RlZFJlZ2lvbnMuaW5jbHVkZXMoY29kZSkpIHtcclxuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIG9uUmVnaW9uT3V0OiBmdW5jdGlvbiAoZXZlbnQsIGNvZGUsIHJlZ2lvbikgeyB9LFxyXG5cclxuICAgICAgICBvblJlc2l6ZTogZnVuY3Rpb24gKGV2ZW50LCB3aWR0aCwgaGVpZ2h0KSB7IH1cclxuICAgIH0pO1xyXG4gICAgbW9kdWxlLmV4cG9ydHMudXBkYXRlUGFnZUVsZW1lbnRzID0gZnVuY3Rpb24gdXBkYXRlUGFnZUVsZW1lbnRzKCkge1xyXG4gICAgICAgICQoXCIjdGV0cmlzLWhlYWRlclwiKS50ZXh0KCQuaTE4bihcInRldHJpc19oZWFkZXJcIikpO1xyXG4gICAgfTtcclxuXHJcbiAgICBtb2R1bGUuZXhwb3J0cy5zaG93TGV2ZWxVbmxvY2tlZCA9IGZ1bmN0aW9uIHNob3dMZXZlbFVubG9ja2VkKCkge1xyXG4gICAgICAgIGZvciAodmFyIGxldmVsIGluIGxldmVsc1VubG9ja2VkKSB7XHJcbiAgICAgICAgICAgIGlmICghbGV2ZWxzVW5sb2NrZWRbbGV2ZWxdLnNob3duKSB7XHJcbiAgICAgICAgICAgICAgICBsZXZlbHNVbmxvY2tlZFtsZXZlbF0uc2hvd24gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShcImxldmVsc1VubG9ja2VkXCIsIEpTT04uc3RyaW5naWZ5KGxldmVsc1VubG9ja2VkKSk7XHJcbiAgICAgICAgICAgICAgICBzd2FsKHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJC5pMThuKFwibGV2ZWxfdW5sb2NrZWRfdGl0bGVcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgaHRtbDogJC5pMThuKFwibGV2ZWxfdW5sb2NrZWRfdGV4dFwiKS5jb25jYXQoJC5pMThuKGxldmVsc1VubG9ja2VkW2xldmVsXS5jb2RlKSkuY29uY2F0KFwiLlwiKSxcclxuICAgICAgICAgICAgICAgICAgICBhbGxvd091dHNpZGVDbGljazogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBhbGxvd0VzY2FwZUtleTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBzaG93Q2xvc2VCdXR0b246IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgYW5pbWF0aW9uOiB0cnVlXHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKChyZXN1bHQpID0+IHsgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pO1xyXG5cclxuZnVuY3Rpb24gc3VwcG9ydHNfaHRtbDVfc3RvcmFnZSgpIHtcclxuICAgIHRyeSB7XHJcbiAgICAgICAgcmV0dXJuIFwibG9jYWxTdG9yYWdlXCIgaW4gd2luZG93ICYmIHdpbmRvd1tcImxvY2FsU3RvcmFnZVwiXSAhPT0gbnVsbDtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbn0iLCIvKiFcclxuICogSlFWTWFwOiBqUXVlcnkgVmVjdG9yIE1hcCBMaWJyYXJ5XHJcbiAqIEBhdXRob3IgSlFWTWFwIDxtZUBwZXRlcnNjaG1hbGZlbGR0LmNvbT5cclxuICogQHZlcnNpb24gMS41LjFcclxuICogQGxpbmsgaHR0cDovL2pxdm1hcC5jb21cclxuICogQGxpY2Vuc2UgaHR0cHM6Ly9naXRodWIuY29tL21hbmlmZXN0aW50ZXJhY3RpdmUvanF2bWFwL2Jsb2IvbWFzdGVyL0xJQ0VOU0VcclxuICogQGJ1aWxkZGF0ZSAyMDE2LzA2LzAyXHJcbiAqL1xyXG5cclxudmFyIFZlY3RvckNhbnZhcyA9IGZ1bmN0aW9uKHdpZHRoLCBoZWlnaHQsIHBhcmFtcykge1xyXG4gICAgdGhpcy5tb2RlID0gd2luZG93LlNWR0FuZ2xlID8gJ3N2ZycgOiAndm1sJztcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG5cclxuICAgIGlmICh0aGlzLm1vZGUgPT09ICdzdmcnKSB7XHJcbiAgICAgICAgdGhpcy5jcmVhdGVTdmdOb2RlID0gZnVuY3Rpb24obm9kZU5hbWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGRvY3VtZW50LmNyZWF0ZUVsZW1lbnROUyh0aGlzLnN2Z25zLCBub2RlTmFtZSk7XHJcbiAgICAgICAgfTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKCFkb2N1bWVudC5uYW1lc3BhY2VzLnJ2bWwpIHtcclxuICAgICAgICAgICAgICAgIGRvY3VtZW50Lm5hbWVzcGFjZXMuYWRkKCdydm1sJywgJ3VybjpzY2hlbWFzLW1pY3Jvc29mdC1jb206dm1sJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5jcmVhdGVWbWxOb2RlID0gZnVuY3Rpb24odGFnTmFtZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJzxydm1sOicgKyB0YWdOYW1lICsgJyBjbGFzcz1cInJ2bWxcIj4nKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlVm1sTm9kZSA9IGZ1bmN0aW9uKHRhZ05hbWUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCc8JyArIHRhZ05hbWUgKyAnIHhtbG5zPVwidXJuOnNjaGVtYXMtbWljcm9zb2Z0LmNvbTp2bWxcIiBjbGFzcz1cInJ2bWxcIj4nKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGRvY3VtZW50LmNyZWF0ZVN0eWxlU2hlZXQoKS5hZGRSdWxlKCcucnZtbCcsICdiZWhhdmlvcjp1cmwoI2RlZmF1bHQjVk1MKScpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLm1vZGUgPT09ICdzdmcnKSB7XHJcbiAgICAgICAgdGhpcy5jYW52YXMgPSB0aGlzLmNyZWF0ZVN2Z05vZGUoJ3N2ZycpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmNhbnZhcyA9IHRoaXMuY3JlYXRlVm1sTm9kZSgnZ3JvdXAnKTtcclxuICAgICAgICB0aGlzLmNhbnZhcy5zdHlsZS5wb3NpdGlvbiA9ICdhYnNvbHV0ZSc7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5zZXRTaXplKHdpZHRoLCBoZWlnaHQpO1xyXG59O1xyXG5cclxuVmVjdG9yQ2FudmFzLnByb3RvdHlwZSA9IHtcclxuICAgIHN2Z25zOiAnaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnLFxyXG4gICAgbW9kZTogJ3N2ZycsXHJcbiAgICB3aWR0aDogMCxcclxuICAgIGhlaWdodDogMCxcclxuICAgIGNhbnZhczogbnVsbFxyXG59O1xyXG5cclxudmFyIENvbG9yU2NhbGUgPSBmdW5jdGlvbihjb2xvcnMsIG5vcm1hbGl6ZUZ1bmN0aW9uLCBtaW5WYWx1ZSwgbWF4VmFsdWUpIHtcclxuICAgIGlmIChjb2xvcnMpIHtcclxuICAgICAgICB0aGlzLnNldENvbG9ycyhjb2xvcnMpO1xyXG4gICAgfVxyXG4gICAgaWYgKG5vcm1hbGl6ZUZ1bmN0aW9uKSB7XHJcbiAgICAgICAgdGhpcy5zZXROb3JtYWxpemVGdW5jdGlvbihub3JtYWxpemVGdW5jdGlvbik7XHJcbiAgICB9XHJcbiAgICBpZiAobWluVmFsdWUpIHtcclxuICAgICAgICB0aGlzLnNldE1pbihtaW5WYWx1ZSk7XHJcbiAgICB9XHJcbiAgICBpZiAobWluVmFsdWUpIHtcclxuICAgICAgICB0aGlzLnNldE1heChtYXhWYWx1ZSk7XHJcbiAgICB9XHJcbn07XHJcblxyXG5Db2xvclNjYWxlLnByb3RvdHlwZSA9IHtcclxuICAgIGNvbG9yczogW11cclxufTtcclxuXHJcbnZhciBKUVZNYXAgPSBmdW5jdGlvbihwYXJhbXMpIHtcclxuICAgIHBhcmFtcyA9IHBhcmFtcyB8fCB7fTtcclxuICAgIHZhciBtYXAgPSB0aGlzO1xyXG4gICAgdmFyIG1hcERhdGEgPSBKUVZNYXAubWFwc1twYXJhbXMubWFwXTtcclxuICAgIHZhciBtYXBQaW5zO1xyXG5cclxuICAgIGlmICghbWFwRGF0YSkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcignSW52YWxpZCBcIicgKyBwYXJhbXMubWFwICsgJ1wiIG1hcCBwYXJhbWV0ZXIuIFBsZWFzZSBtYWtlIHN1cmUgeW91IGhhdmUgbG9hZGVkIHRoaXMgbWFwIGZpbGUgaW4geW91ciBIVE1MLicpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuc2VsZWN0ZWRSZWdpb25zID0gW107XHJcbiAgICB0aGlzLm11bHRpU2VsZWN0UmVnaW9uID0gcGFyYW1zLm11bHRpU2VsZWN0UmVnaW9uO1xyXG5cclxuICAgIHRoaXMuY29udGFpbmVyID0gcGFyYW1zLmNvbnRhaW5lcjtcclxuXHJcbiAgICB0aGlzLmRlZmF1bHRXaWR0aCA9IG1hcERhdGEud2lkdGg7XHJcbiAgICB0aGlzLmRlZmF1bHRIZWlnaHQgPSBtYXBEYXRhLmhlaWdodDtcclxuXHJcbiAgICB0aGlzLmNvbG9yID0gcGFyYW1zLmNvbG9yO1xyXG4gICAgdGhpcy5zZWxlY3RlZENvbG9yID0gcGFyYW1zLnNlbGVjdGVkQ29sb3I7XHJcbiAgICB0aGlzLmhvdmVyQ29sb3IgPSBwYXJhbXMuaG92ZXJDb2xvcjtcclxuICAgIHRoaXMuaG92ZXJDb2xvcnMgPSBwYXJhbXMuaG92ZXJDb2xvcnM7XHJcbiAgICB0aGlzLmhvdmVyT3BhY2l0eSA9IHBhcmFtcy5ob3Zlck9wYWNpdHk7XHJcbiAgICB0aGlzLnNldEJhY2tncm91bmRDb2xvcihwYXJhbXMuYmFja2dyb3VuZENvbG9yKTtcclxuXHJcbiAgICB0aGlzLndpZHRoID0gcGFyYW1zLmNvbnRhaW5lci53aWR0aCgpO1xyXG4gICAgdGhpcy5oZWlnaHQgPSBwYXJhbXMuY29udGFpbmVyLmhlaWdodCgpO1xyXG5cclxuICAgIHRoaXMucmVzaXplKCk7XHJcblxyXG4gICAgalF1ZXJ5KHdpbmRvdykucmVzaXplKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHZhciBuZXdXaWR0aCA9IHBhcmFtcy5jb250YWluZXIud2lkdGgoKTtcclxuICAgICAgICB2YXIgbmV3SGVpZ2h0ID0gcGFyYW1zLmNvbnRhaW5lci5oZWlnaHQoKTtcclxuXHJcbiAgICAgICAgaWYgKG5ld1dpZHRoICYmIG5ld0hlaWdodCkge1xyXG4gICAgICAgICAgICBtYXAud2lkdGggPSBuZXdXaWR0aDtcclxuICAgICAgICAgICAgbWFwLmhlaWdodCA9IG5ld0hlaWdodDtcclxuICAgICAgICAgICAgbWFwLnJlc2l6ZSgpO1xyXG4gICAgICAgICAgICBtYXAuY2FudmFzLnNldFNpemUobWFwLndpZHRoLCBtYXAuaGVpZ2h0KTtcclxuICAgICAgICAgICAgbWFwLmFwcGx5VHJhbnNmb3JtKCk7XHJcblxyXG4gICAgICAgICAgICB2YXIgcmVzaXplRXZlbnQgPSBqUXVlcnkuRXZlbnQoJ3Jlc2l6ZS5qcXZtYXAnKTtcclxuICAgICAgICAgICAgalF1ZXJ5KHBhcmFtcy5jb250YWluZXIpLnRyaWdnZXIocmVzaXplRXZlbnQsIFtuZXdXaWR0aCwgbmV3SGVpZ2h0XSk7XHJcblxyXG4gICAgICAgICAgICBpZiAobWFwUGlucykge1xyXG4gICAgICAgICAgICAgICAgalF1ZXJ5KCcuanF2bWFwLXBpbicpLnJlbW92ZSgpO1xyXG4gICAgICAgICAgICAgICAgbWFwLnBpbkhhbmRsZXJzID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBtYXAucGxhY2VQaW5zKG1hcFBpbnMucGlucywgbWFwUGlucy5tb2RlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuY2FudmFzID0gbmV3IFZlY3RvckNhbnZhcyh0aGlzLndpZHRoLCB0aGlzLmhlaWdodCwgcGFyYW1zKTtcclxuICAgIHBhcmFtcy5jb250YWluZXIuYXBwZW5kKHRoaXMuY2FudmFzLmNhbnZhcyk7XHJcblxyXG4gICAgdGhpcy5tYWtlRHJhZ2dhYmxlKCk7XHJcblxyXG4gICAgdGhpcy5yb290R3JvdXAgPSB0aGlzLmNhbnZhcy5jcmVhdGVHcm91cCh0cnVlKTtcclxuXHJcbiAgICB0aGlzLmluZGV4ID0gSlFWTWFwLm1hcEluZGV4O1xyXG4gICAgdGhpcy5sYWJlbCA9IGpRdWVyeSgnPGRpdi8+JykuYWRkQ2xhc3MoJ2pxdm1hcC1sYWJlbCcpLmFwcGVuZFRvKGpRdWVyeSgnYm9keScpKS5oaWRlKCk7XHJcblxyXG4gICAgaWYgKHBhcmFtcy5lbmFibGVab29tKSB7XHJcbiAgICAgICAgalF1ZXJ5KCc8ZGl2Lz4nKS5hZGRDbGFzcygnanF2bWFwLXpvb21pbicpLnRleHQoJysnKS5hcHBlbmRUbyhwYXJhbXMuY29udGFpbmVyKTtcclxuICAgICAgICBqUXVlcnkoJzxkaXYvPicpLmFkZENsYXNzKCdqcXZtYXAtem9vbW91dCcpLmh0bWwoJyYjeDIyMTI7JykuYXBwZW5kVG8ocGFyYW1zLmNvbnRhaW5lcik7XHJcbiAgICB9XHJcblxyXG4gICAgbWFwLmNvdW50cmllcyA9IFtdO1xyXG5cclxuICAgIGZvciAodmFyIGtleSBpbiBtYXBEYXRhLnBhdGhzKSB7XHJcbiAgICAgICAgdmFyIHBhdGggPSB0aGlzLmNhbnZhcy5jcmVhdGVQYXRoKHtcclxuICAgICAgICAgICAgcGF0aDogbWFwRGF0YS5wYXRoc1trZXldLnBhdGhcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcGF0aC5zZXRGaWxsKHRoaXMuY29sb3IpO1xyXG4gICAgICAgIHBhdGguaWQgPSBtYXAuZ2V0Q291bnRyeUlkKGtleSk7XHJcbiAgICAgICAgbWFwLmNvdW50cmllc1trZXldID0gcGF0aDtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY2FudmFzLm1vZGUgPT09ICdzdmcnKSB7XHJcbiAgICAgICAgICAgIHBhdGguc2V0QXR0cmlidXRlKCdjbGFzcycsICdqcXZtYXAtcmVnaW9uJyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgalF1ZXJ5KHBhdGgpLmFkZENsYXNzKCdqcXZtYXAtcmVnaW9uJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBqUXVlcnkodGhpcy5yb290R3JvdXApLmFwcGVuZChwYXRoKTtcclxuICAgIH1cclxuXHJcbiAgICBqUXVlcnkocGFyYW1zLmNvbnRhaW5lcikuZGVsZWdhdGUodGhpcy5jYW52YXMubW9kZSA9PT0gJ3N2ZycgPyAncGF0aCcgOiAnc2hhcGUnLCAnbW91c2VvdmVyIG1vdXNlb3V0JywgZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIHZhciBjb250YWluZXJQYXRoID0gZS50YXJnZXQsXHJcbiAgICAgICAgICAgIGNvZGUgPSBlLnRhcmdldC5pZC5zcGxpdCgnXycpLnBvcCgpLFxyXG4gICAgICAgICAgICBsYWJlbFNob3dFdmVudCA9IGpRdWVyeS5FdmVudCgnbGFiZWxTaG93Lmpxdm1hcCcpLFxyXG4gICAgICAgICAgICByZWdpb25Nb3VzZU92ZXJFdmVudCA9IGpRdWVyeS5FdmVudCgncmVnaW9uTW91c2VPdmVyLmpxdm1hcCcpO1xyXG5cclxuICAgICAgICBjb2RlID0gY29kZS50b0xvd2VyQ2FzZSgpO1xyXG5cclxuICAgICAgICBpZiAoZS50eXBlID09PSAnbW91c2VvdmVyJykge1xyXG4gICAgICAgICAgICBqUXVlcnkocGFyYW1zLmNvbnRhaW5lcikudHJpZ2dlcihyZWdpb25Nb3VzZU92ZXJFdmVudCwgW2NvZGUsIG1hcERhdGEucGF0aHNbY29kZV0ubmFtZV0pO1xyXG4gICAgICAgICAgICBpZiAoIXJlZ2lvbk1vdXNlT3ZlckV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgICAgICAgICBtYXAuaGlnaGxpZ2h0KGNvZGUsIGNvbnRhaW5lclBhdGgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChwYXJhbXMuc2hvd1Rvb2x0aXApIHtcclxuICAgICAgICAgICAgICAgIG1hcC5sYWJlbC50ZXh0KG1hcERhdGEucGF0aHNbY29kZV0ubmFtZSk7XHJcbiAgICAgICAgICAgICAgICBqUXVlcnkocGFyYW1zLmNvbnRhaW5lcikudHJpZ2dlcihsYWJlbFNob3dFdmVudCwgW21hcC5sYWJlbCwgY29kZV0pO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICghbGFiZWxTaG93RXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBtYXAubGFiZWwuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcC5sYWJlbFdpZHRoID0gbWFwLmxhYmVsLndpZHRoKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFwLmxhYmVsSGVpZ2h0ID0gbWFwLmxhYmVsLmhlaWdodCgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbWFwLnVuaGlnaGxpZ2h0KGNvZGUsIGNvbnRhaW5lclBhdGgpO1xyXG5cclxuICAgICAgICAgICAgbWFwLmxhYmVsLmhpZGUoKTtcclxuICAgICAgICAgICAgalF1ZXJ5KHBhcmFtcy5jb250YWluZXIpLnRyaWdnZXIoJ3JlZ2lvbk1vdXNlT3V0Lmpxdm1hcCcsIFtjb2RlLCBtYXBEYXRhLnBhdGhzW2NvZGVdLm5hbWVdKTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBqUXVlcnkocGFyYW1zLmNvbnRhaW5lcikuZGVsZWdhdGUodGhpcy5jYW52YXMubW9kZSA9PT0gJ3N2ZycgPyAncGF0aCcgOiAnc2hhcGUnLCAnY2xpY2snLCBmdW5jdGlvbihyZWdpb25DbGlja0V2ZW50KSB7XHJcblxyXG4gICAgICAgIHZhciB0YXJnZXRQYXRoID0gcmVnaW9uQ2xpY2tFdmVudC50YXJnZXQ7XHJcbiAgICAgICAgdmFyIGNvZGUgPSByZWdpb25DbGlja0V2ZW50LnRhcmdldC5pZC5zcGxpdCgnXycpLnBvcCgpO1xyXG4gICAgICAgIHZhciBtYXBDbGlja0V2ZW50ID0galF1ZXJ5LkV2ZW50KCdyZWdpb25DbGljay5qcXZtYXAnKTtcclxuXHJcbiAgICAgICAgY29kZSA9IGNvZGUudG9Mb3dlckNhc2UoKTtcclxuXHJcbiAgICAgICAgalF1ZXJ5KHBhcmFtcy5jb250YWluZXIpLnRyaWdnZXIobWFwQ2xpY2tFdmVudCwgW2NvZGUsIG1hcERhdGEucGF0aHNbY29kZV0ubmFtZV0pO1xyXG5cclxuICAgICAgICBpZiAoIXBhcmFtcy5tdWx0aVNlbGVjdFJlZ2lvbiAmJiAhbWFwQ2xpY2tFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkge1xyXG4gICAgICAgICAgICBmb3IgKHZhciBrZXlQYXRoIGluIG1hcERhdGEucGF0aHMpIHtcclxuICAgICAgICAgICAgICAgIG1hcC5jb3VudHJpZXNba2V5UGF0aF0uY3VycmVudEZpbGxDb2xvciA9IG1hcC5jb3VudHJpZXNba2V5UGF0aF0uZ2V0T3JpZ2luYWxGaWxsKCk7XHJcbiAgICAgICAgICAgICAgICBtYXAuY291bnRyaWVzW2tleVBhdGhdLnNldEZpbGwobWFwLmNvdW50cmllc1trZXlQYXRoXS5nZXRPcmlnaW5hbEZpbGwoKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghbWFwQ2xpY2tFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkge1xyXG4gICAgICAgICAgICBpZiAobWFwLmlzU2VsZWN0ZWQoY29kZSkpIHtcclxuICAgICAgICAgICAgICAgIG1hcC5kZXNlbGVjdChjb2RlLCB0YXJnZXRQYXRoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG1hcC5zZWxlY3QoY29kZSwgdGFyZ2V0UGF0aCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBpZiAocGFyYW1zLnNob3dUb29sdGlwKSB7XHJcbiAgICAgICAgcGFyYW1zLmNvbnRhaW5lci5tb3VzZW1vdmUoZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICAgICBpZiAobWFwLmxhYmVsLmlzKCc6dmlzaWJsZScpKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgbGVmdCA9IGUucGFnZVggLSAxNSAtIG1hcC5sYWJlbFdpZHRoO1xyXG4gICAgICAgICAgICAgICAgdmFyIHRvcCA9IGUucGFnZVkgLSAxNSAtIG1hcC5sYWJlbEhlaWdodDtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAobGVmdCA8IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBsZWZ0ID0gZS5wYWdlWCArIDE1O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHRvcCA8IDApIHtcclxuICAgICAgICAgICAgICAgICAgICB0b3AgPSBlLnBhZ2VZICsgMTU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgbWFwLmxhYmVsLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgbGVmdDogbGVmdCxcclxuICAgICAgICAgICAgICAgICAgICB0b3A6IHRvcFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnNldENvbG9ycyhwYXJhbXMuY29sb3JzKTtcclxuXHJcbiAgICB0aGlzLmNhbnZhcy5jYW52YXMuYXBwZW5kQ2hpbGQodGhpcy5yb290R3JvdXApO1xyXG5cclxuICAgIHRoaXMuYXBwbHlUcmFuc2Zvcm0oKTtcclxuXHJcbiAgICB0aGlzLmNvbG9yU2NhbGUgPSBuZXcgQ29sb3JTY2FsZShwYXJhbXMuc2NhbGVDb2xvcnMsIHBhcmFtcy5ub3JtYWxpemVGdW5jdGlvbiwgcGFyYW1zLnZhbHVlTWluLCBwYXJhbXMudmFsdWVNYXgpO1xyXG5cclxuICAgIGlmIChwYXJhbXMudmFsdWVzKSB7XHJcbiAgICAgICAgdGhpcy52YWx1ZXMgPSBwYXJhbXMudmFsdWVzO1xyXG4gICAgICAgIHRoaXMuc2V0VmFsdWVzKHBhcmFtcy52YWx1ZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChwYXJhbXMuc2VsZWN0ZWRSZWdpb25zKSB7XHJcbiAgICAgICAgaWYgKHBhcmFtcy5zZWxlY3RlZFJlZ2lvbnMgaW5zdGFuY2VvZiBBcnJheSkge1xyXG4gICAgICAgICAgICBmb3IgKHZhciBrIGluIHBhcmFtcy5zZWxlY3RlZFJlZ2lvbnMpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0KHBhcmFtcy5zZWxlY3RlZFJlZ2lvbnNba10udG9Mb3dlckNhc2UoKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdChwYXJhbXMuc2VsZWN0ZWRSZWdpb25zLnRvTG93ZXJDYXNlKCkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmJpbmRab29tQnV0dG9ucygpO1xyXG5cclxuICAgIGlmIChwYXJhbXMucGlucykge1xyXG4gICAgICAgIG1hcFBpbnMgPSB7XHJcbiAgICAgICAgICAgIHBpbnM6IHBhcmFtcy5waW5zLFxyXG4gICAgICAgICAgICBtb2RlOiBwYXJhbXMucGluTW9kZVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHRoaXMucGluSGFuZGxlcnMgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnBsYWNlUGlucyhwYXJhbXMucGlucywgcGFyYW1zLnBpbk1vZGUpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChwYXJhbXMuc2hvd0xhYmVscykge1xyXG4gICAgICAgIHRoaXMucGluSGFuZGxlcnMgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgdmFyIHBpbnMgPSB7fTtcclxuICAgICAgICBmb3IgKGtleSBpbiBtYXAuY291bnRyaWVzKSB7XHJcbiAgICAgICAgICAgIGlmICh0eXBlb2YgbWFwLmNvdW50cmllc1trZXldICE9PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXBhcmFtcy5waW5zIHx8ICFwYXJhbXMucGluc1trZXldKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcGluc1trZXldID0ga2V5LnRvVXBwZXJDYXNlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIG1hcFBpbnMgPSB7XHJcbiAgICAgICAgICAgIHBpbnM6IHBpbnMsXHJcbiAgICAgICAgICAgIG1vZGU6ICdjb250ZW50J1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHRoaXMucGxhY2VQaW5zKHBpbnMsICdjb250ZW50Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgSlFWTWFwLm1hcEluZGV4Kys7XHJcbn07XHJcblxyXG5KUVZNYXAucHJvdG90eXBlID0ge1xyXG4gICAgdHJhbnNYOiAwLFxyXG4gICAgdHJhbnNZOiAwLFxyXG4gICAgc2NhbGU6IDEsXHJcbiAgICBiYXNlVHJhbnNYOiAwLFxyXG4gICAgYmFzZVRyYW5zWTogMCxcclxuICAgIGJhc2VTY2FsZTogMSxcclxuICAgIHdpZHRoOiAwLFxyXG4gICAgaGVpZ2h0OiAwLFxyXG4gICAgY291bnRyaWVzOiB7fSxcclxuICAgIGNvdW50cmllc0NvbG9yczoge30sXHJcbiAgICBjb3VudHJpZXNEYXRhOiB7fSxcclxuICAgIHpvb21TdGVwOiAxLjQsXHJcbiAgICB6b29tTWF4U3RlcDogNCxcclxuICAgIHpvb21DdXJTdGVwOiAxXHJcbn07XHJcblxyXG5KUVZNYXAueGxpbmsgPSAnaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayc7XHJcbkpRVk1hcC5tYXBJbmRleCA9IDE7XHJcbkpRVk1hcC5tYXBzID0ge307XHJcblxyXG4oZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgdmFyIGFwaVBhcmFtcyA9IHtcclxuICAgICAgICBjb2xvcnM6IDEsXHJcbiAgICAgICAgdmFsdWVzOiAxLFxyXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogMSxcclxuICAgICAgICBzY2FsZUNvbG9yczogMSxcclxuICAgICAgICBub3JtYWxpemVGdW5jdGlvbjogMSxcclxuICAgICAgICBlbmFibGVab29tOiAxLFxyXG4gICAgICAgIHNob3dUb29sdGlwOiAxLFxyXG4gICAgICAgIGJvcmRlckNvbG9yOiAxLFxyXG4gICAgICAgIGJvcmRlcldpZHRoOiAxLFxyXG4gICAgICAgIGJvcmRlck9wYWNpdHk6IDEsXHJcbiAgICAgICAgc2VsZWN0ZWRSZWdpb25zOiAxLFxyXG4gICAgICAgIG11bHRpU2VsZWN0UmVnaW9uOiAxXHJcbiAgICB9O1xyXG5cclxuICAgIHZhciBhcGlFdmVudHMgPSB7XHJcbiAgICAgICAgb25MYWJlbFNob3c6ICdsYWJlbFNob3cnLFxyXG4gICAgICAgIG9uTG9hZDogJ2xvYWQnLFxyXG4gICAgICAgIG9uUmVnaW9uT3ZlcjogJ3JlZ2lvbk1vdXNlT3ZlcicsXHJcbiAgICAgICAgb25SZWdpb25PdXQ6ICdyZWdpb25Nb3VzZU91dCcsXHJcbiAgICAgICAgb25SZWdpb25DbGljazogJ3JlZ2lvbkNsaWNrJyxcclxuICAgICAgICBvblJlZ2lvblNlbGVjdDogJ3JlZ2lvblNlbGVjdCcsXHJcbiAgICAgICAgb25SZWdpb25EZXNlbGVjdDogJ3JlZ2lvbkRlc2VsZWN0JyxcclxuICAgICAgICBvblJlc2l6ZTogJ3Jlc2l6ZSdcclxuICAgIH07XHJcblxyXG4gICAgalF1ZXJ5LmZuLnZlY3Rvck1hcCA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcclxuXHJcbiAgICAgICAgdmFyIGRlZmF1bHRQYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICAgICBtYXA6ICd3b3JsZF9lbicsXHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6ICcjYTViZmRkJyxcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAnI2Y0ZjNmMCcsXHJcbiAgICAgICAgICAgICAgICBob3ZlckNvbG9yOiAnI2M5ZGZhZicsXHJcbiAgICAgICAgICAgICAgICBob3ZlckNvbG9yczoge30sXHJcbiAgICAgICAgICAgICAgICBzZWxlY3RlZENvbG9yOiAnI2M5ZGZhZicsXHJcbiAgICAgICAgICAgICAgICBzY2FsZUNvbG9yczogWycjYjZkNmZmJywgJyMwMDVhY2UnXSxcclxuICAgICAgICAgICAgICAgIG5vcm1hbGl6ZUZ1bmN0aW9uOiAnbGluZWFyJyxcclxuICAgICAgICAgICAgICAgIGVuYWJsZVpvb206IHRydWUsXHJcbiAgICAgICAgICAgICAgICBzaG93VG9vbHRpcDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGJvcmRlckNvbG9yOiAnIzgxODE4MScsXHJcbiAgICAgICAgICAgICAgICBib3JkZXJXaWR0aDogMSxcclxuICAgICAgICAgICAgICAgIGJvcmRlck9wYWNpdHk6IDAuMjUsXHJcbiAgICAgICAgICAgICAgICBzZWxlY3RlZFJlZ2lvbnM6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBtdWx0aVNlbGVjdFJlZ2lvbjogZmFsc2VcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgbWFwID0gdGhpcy5kYXRhKCdtYXBPYmplY3QnKTtcclxuXHJcbiAgICAgICAgaWYgKG9wdGlvbnMgPT09ICdhZGRNYXAnKSB7XHJcbiAgICAgICAgICAgIEpRVk1hcC5tYXBzW2FyZ3VtZW50c1sxXV0gPSBhcmd1bWVudHNbMl07XHJcbiAgICAgICAgfSBlbHNlIGlmIChvcHRpb25zID09PSAnc2V0JyAmJiBhcGlQYXJhbXNbYXJndW1lbnRzWzFdXSkge1xyXG4gICAgICAgICAgICBtYXBbJ3NldCcgKyBhcmd1bWVudHNbMV0uY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBhcmd1bWVudHNbMV0uc3Vic3RyKDEpXS5hcHBseShtYXAsIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMikpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIG9wdGlvbnMgPT09ICdzdHJpbmcnICYmXHJcbiAgICAgICAgICAgIHR5cGVvZiBtYXBbb3B0aW9uc10gPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG1hcFtvcHRpb25zXS5hcHBseShtYXAsIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSkpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGpRdWVyeS5leHRlbmQoZGVmYXVsdFBhcmFtcywgb3B0aW9ucyk7XHJcbiAgICAgICAgICAgIGRlZmF1bHRQYXJhbXMuY29udGFpbmVyID0gdGhpcztcclxuICAgICAgICAgICAgdGhpcy5jc3MoeyBwb3NpdGlvbjogJ3JlbGF0aXZlJywgb3ZlcmZsb3c6ICdoaWRkZW4nIH0pO1xyXG5cclxuICAgICAgICAgICAgbWFwID0gbmV3IEpRVk1hcChkZWZhdWx0UGFyYW1zKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZGF0YSgnbWFwT2JqZWN0JywgbWFwKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMudW5iaW5kKCcuanF2bWFwJyk7XHJcblxyXG4gICAgICAgICAgICBmb3IgKHZhciBlIGluIGFwaUV2ZW50cykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGRlZmF1bHRQYXJhbXNbZV0pIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmJpbmQoYXBpRXZlbnRzW2VdICsgJy5qcXZtYXAnLCBkZWZhdWx0UGFyYW1zW2VdKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdmFyIGxvYWRFdmVudCA9IGpRdWVyeS5FdmVudCgnbG9hZC5qcXZtYXAnKTtcclxuICAgICAgICAgICAgalF1ZXJ5KGRlZmF1bHRQYXJhbXMuY29udGFpbmVyKS50cmlnZ2VyKGxvYWRFdmVudCwgbWFwKTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiBtYXA7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbn0pKGpRdWVyeSk7XHJcblxyXG5Db2xvclNjYWxlLmFycmF5VG9SZ2IgPSBmdW5jdGlvbihhcikge1xyXG4gICAgdmFyIHJnYiA9ICcjJztcclxuICAgIHZhciBkO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBhci5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGQgPSBhcltpXS50b1N0cmluZygxNik7XHJcbiAgICAgICAgcmdiICs9IGQubGVuZ3RoID09PSAxID8gJzAnICsgZCA6IGQ7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gcmdiO1xyXG59O1xyXG5cclxuQ29sb3JTY2FsZS5wcm90b3R5cGUuZ2V0Q29sb3IgPSBmdW5jdGlvbih2YWx1ZSkge1xyXG4gICAgaWYgKHR5cGVvZiB0aGlzLm5vcm1hbGl6ZSA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgIHZhbHVlID0gdGhpcy5ub3JtYWxpemUodmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciBsZW5ndGhlcyA9IFtdO1xyXG4gICAgdmFyIGZ1bGxMZW5ndGggPSAwO1xyXG4gICAgdmFyIGw7XHJcblxyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmNvbG9ycy5sZW5ndGggLSAxOyBpKyspIHtcclxuICAgICAgICBsID0gdGhpcy52ZWN0b3JMZW5ndGgodGhpcy52ZWN0b3JTdWJ0cmFjdCh0aGlzLmNvbG9yc1tpICsgMV0sIHRoaXMuY29sb3JzW2ldKSk7XHJcbiAgICAgICAgbGVuZ3RoZXMucHVzaChsKTtcclxuICAgICAgICBmdWxsTGVuZ3RoICs9IGw7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIGMgPSAodGhpcy5tYXhWYWx1ZSAtIHRoaXMubWluVmFsdWUpIC8gZnVsbExlbmd0aDtcclxuXHJcbiAgICBmb3IgKGkgPSAwOyBpIDwgbGVuZ3RoZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICBsZW5ndGhlc1tpXSAqPSBjO1xyXG4gICAgfVxyXG5cclxuICAgIGkgPSAwO1xyXG4gICAgdmFsdWUgLT0gdGhpcy5taW5WYWx1ZTtcclxuXHJcbiAgICB3aGlsZSAodmFsdWUgLSBsZW5ndGhlc1tpXSA+PSAwKSB7XHJcbiAgICAgICAgdmFsdWUgLT0gbGVuZ3RoZXNbaV07XHJcbiAgICAgICAgaSsrO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciBjb2xvcjtcclxuICAgIGlmIChpID09PSB0aGlzLmNvbG9ycy5sZW5ndGggLSAxKSB7XHJcbiAgICAgICAgY29sb3IgPSB0aGlzLnZlY3RvclRvTnVtKHRoaXMuY29sb3JzW2ldKS50b1N0cmluZygxNik7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbG9yID0gKHRoaXMudmVjdG9yVG9OdW0odGhpcy52ZWN0b3JBZGQodGhpcy5jb2xvcnNbaV0sIHRoaXMudmVjdG9yTXVsdCh0aGlzLnZlY3RvclN1YnRyYWN0KHRoaXMuY29sb3JzW2kgKyAxXSwgdGhpcy5jb2xvcnNbaV0pLCAodmFsdWUpIC8gKGxlbmd0aGVzW2ldKSkpKSkudG9TdHJpbmcoMTYpO1xyXG4gICAgfVxyXG5cclxuICAgIHdoaWxlIChjb2xvci5sZW5ndGggPCA2KSB7XHJcbiAgICAgICAgY29sb3IgPSAnMCcgKyBjb2xvcjtcclxuICAgIH1cclxuICAgIHJldHVybiAnIycgKyBjb2xvcjtcclxufTtcclxuXHJcbkNvbG9yU2NhbGUucmdiVG9BcnJheSA9IGZ1bmN0aW9uKHJnYikge1xyXG4gICAgcmdiID0gcmdiLnN1YnN0cigxKTtcclxuICAgIHJldHVybiBbcGFyc2VJbnQocmdiLnN1YnN0cigwLCAyKSwgMTYpLCBwYXJzZUludChyZ2Iuc3Vic3RyKDIsIDIpLCAxNiksIHBhcnNlSW50KHJnYi5zdWJzdHIoNCwgMiksIDE2KV07XHJcbn07XHJcblxyXG5Db2xvclNjYWxlLnByb3RvdHlwZS5zZXRDb2xvcnMgPSBmdW5jdGlvbihjb2xvcnMpIHtcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY29sb3JzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgY29sb3JzW2ldID0gQ29sb3JTY2FsZS5yZ2JUb0FycmF5KGNvbG9yc1tpXSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmNvbG9ycyA9IGNvbG9ycztcclxufTtcclxuXHJcbkNvbG9yU2NhbGUucHJvdG90eXBlLnNldE1heCA9IGZ1bmN0aW9uKG1heCkge1xyXG4gICAgdGhpcy5jbGVhck1heFZhbHVlID0gbWF4O1xyXG4gICAgaWYgKHR5cGVvZiB0aGlzLm5vcm1hbGl6ZSA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgIHRoaXMubWF4VmFsdWUgPSB0aGlzLm5vcm1hbGl6ZShtYXgpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLm1heFZhbHVlID0gbWF4O1xyXG4gICAgfVxyXG59O1xyXG5cclxuQ29sb3JTY2FsZS5wcm90b3R5cGUuc2V0TWluID0gZnVuY3Rpb24obWluKSB7XHJcbiAgICB0aGlzLmNsZWFyTWluVmFsdWUgPSBtaW47XHJcblxyXG4gICAgaWYgKHR5cGVvZiB0aGlzLm5vcm1hbGl6ZSA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgIHRoaXMubWluVmFsdWUgPSB0aGlzLm5vcm1hbGl6ZShtaW4pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLm1pblZhbHVlID0gbWluO1xyXG4gICAgfVxyXG59O1xyXG5cclxuQ29sb3JTY2FsZS5wcm90b3R5cGUuc2V0Tm9ybWFsaXplRnVuY3Rpb24gPSBmdW5jdGlvbihmKSB7XHJcbiAgICBpZiAoZiA9PT0gJ3BvbHlub21pYWwnKSB7XHJcbiAgICAgICAgdGhpcy5ub3JtYWxpemUgPSBmdW5jdGlvbih2YWx1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gTWF0aC5wb3codmFsdWUsIDAuMik7XHJcbiAgICAgICAgfTtcclxuICAgIH0gZWxzZSBpZiAoZiA9PT0gJ2xpbmVhcicpIHtcclxuICAgICAgICBkZWxldGUgdGhpcy5ub3JtYWxpemU7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMubm9ybWFsaXplID0gZjtcclxuICAgIH1cclxuICAgIHRoaXMuc2V0TWluKHRoaXMuY2xlYXJNaW5WYWx1ZSk7XHJcbiAgICB0aGlzLnNldE1heCh0aGlzLmNsZWFyTWF4VmFsdWUpO1xyXG59O1xyXG5cclxuQ29sb3JTY2FsZS5wcm90b3R5cGUudmVjdG9yQWRkID0gZnVuY3Rpb24odmVjdG9yMSwgdmVjdG9yMikge1xyXG4gICAgdmFyIHZlY3RvciA9IFtdO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB2ZWN0b3IxLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgdmVjdG9yW2ldID0gdmVjdG9yMVtpXSArIHZlY3RvcjJbaV07XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdmVjdG9yO1xyXG59O1xyXG5cclxuQ29sb3JTY2FsZS5wcm90b3R5cGUudmVjdG9yTGVuZ3RoID0gZnVuY3Rpb24odmVjdG9yKSB7XHJcbiAgICB2YXIgcmVzdWx0ID0gMDtcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdmVjdG9yLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgcmVzdWx0ICs9IHZlY3RvcltpXSAqIHZlY3RvcltpXTtcclxuICAgIH1cclxuICAgIHJldHVybiBNYXRoLnNxcnQocmVzdWx0KTtcclxufTtcclxuXHJcbkNvbG9yU2NhbGUucHJvdG90eXBlLnZlY3Rvck11bHQgPSBmdW5jdGlvbih2ZWN0b3IsIG51bSkge1xyXG4gICAgdmFyIHJlc3VsdCA9IFtdO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB2ZWN0b3IubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICByZXN1bHRbaV0gPSB2ZWN0b3JbaV0gKiBudW07XHJcbiAgICB9XHJcbiAgICByZXR1cm4gcmVzdWx0O1xyXG59O1xyXG5cclxuQ29sb3JTY2FsZS5wcm90b3R5cGUudmVjdG9yU3VidHJhY3QgPSBmdW5jdGlvbih2ZWN0b3IxLCB2ZWN0b3IyKSB7XHJcbiAgICB2YXIgdmVjdG9yID0gW107XHJcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHZlY3RvcjEubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICB2ZWN0b3JbaV0gPSB2ZWN0b3IxW2ldIC0gdmVjdG9yMltpXTtcclxuICAgIH1cclxuICAgIHJldHVybiB2ZWN0b3I7XHJcbn07XHJcblxyXG5Db2xvclNjYWxlLnByb3RvdHlwZS52ZWN0b3JUb051bSA9IGZ1bmN0aW9uKHZlY3Rvcikge1xyXG4gICAgdmFyIG51bSA9IDA7XHJcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHZlY3Rvci5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIG51bSArPSBNYXRoLnJvdW5kKHZlY3RvcltpXSkgKiBNYXRoLnBvdygyNTYsIHZlY3Rvci5sZW5ndGggLSBpIC0gMSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbnVtO1xyXG59O1xyXG5cclxuSlFWTWFwLnByb3RvdHlwZS5hcHBseVRyYW5zZm9ybSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgdmFyIG1heFRyYW5zWCwgbWF4VHJhbnNZLCBtaW5UcmFuc1gsIG1pblRyYW5zWTtcclxuICAgIGlmICh0aGlzLmRlZmF1bHRXaWR0aCAqIHRoaXMuc2NhbGUgPD0gdGhpcy53aWR0aCkge1xyXG4gICAgICAgIG1heFRyYW5zWCA9ICh0aGlzLndpZHRoIC0gdGhpcy5kZWZhdWx0V2lkdGggKiB0aGlzLnNjYWxlKSAvICgyICogdGhpcy5zY2FsZSk7XHJcbiAgICAgICAgbWluVHJhbnNYID0gKHRoaXMud2lkdGggLSB0aGlzLmRlZmF1bHRXaWR0aCAqIHRoaXMuc2NhbGUpIC8gKDIgKiB0aGlzLnNjYWxlKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbWF4VHJhbnNYID0gMDtcclxuICAgICAgICBtaW5UcmFuc1ggPSAodGhpcy53aWR0aCAtIHRoaXMuZGVmYXVsdFdpZHRoICogdGhpcy5zY2FsZSkgLyB0aGlzLnNjYWxlO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmRlZmF1bHRIZWlnaHQgKiB0aGlzLnNjYWxlIDw9IHRoaXMuaGVpZ2h0KSB7XHJcbiAgICAgICAgbWF4VHJhbnNZID0gKHRoaXMuaGVpZ2h0IC0gdGhpcy5kZWZhdWx0SGVpZ2h0ICogdGhpcy5zY2FsZSkgLyAoMiAqIHRoaXMuc2NhbGUpO1xyXG4gICAgICAgIG1pblRyYW5zWSA9ICh0aGlzLmhlaWdodCAtIHRoaXMuZGVmYXVsdEhlaWdodCAqIHRoaXMuc2NhbGUpIC8gKDIgKiB0aGlzLnNjYWxlKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbWF4VHJhbnNZID0gMDtcclxuICAgICAgICBtaW5UcmFuc1kgPSAodGhpcy5oZWlnaHQgLSB0aGlzLmRlZmF1bHRIZWlnaHQgKiB0aGlzLnNjYWxlKSAvIHRoaXMuc2NhbGU7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMudHJhbnNZID4gbWF4VHJhbnNZKSB7XHJcbiAgICAgICAgdGhpcy50cmFuc1kgPSBtYXhUcmFuc1k7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMudHJhbnNZIDwgbWluVHJhbnNZKSB7XHJcbiAgICAgICAgdGhpcy50cmFuc1kgPSBtaW5UcmFuc1k7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy50cmFuc1ggPiBtYXhUcmFuc1gpIHtcclxuICAgICAgICB0aGlzLnRyYW5zWCA9IG1heFRyYW5zWDtcclxuICAgIH0gZWxzZSBpZiAodGhpcy50cmFuc1ggPCBtaW5UcmFuc1gpIHtcclxuICAgICAgICB0aGlzLnRyYW5zWCA9IG1pblRyYW5zWDtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmNhbnZhcy5hcHBseVRyYW5zZm9ybVBhcmFtcyh0aGlzLnNjYWxlLCB0aGlzLnRyYW5zWCwgdGhpcy50cmFuc1kpO1xyXG59O1xyXG5cclxuSlFWTWFwLnByb3RvdHlwZS5iaW5kWm9vbUJ1dHRvbnMgPSBmdW5jdGlvbigpIHtcclxuICAgIHZhciBtYXAgPSB0aGlzO1xyXG4gICAgdGhpcy5jb250YWluZXIuZmluZCgnLmpxdm1hcC16b29taW4nKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICBtYXAuem9vbUluKCk7XHJcbiAgICB9KTtcclxuICAgIHRoaXMuY29udGFpbmVyLmZpbmQoJy5qcXZtYXAtem9vbW91dCcpLmNsaWNrKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIG1hcC56b29tT3V0KCk7XHJcbiAgICB9KTtcclxufTtcclxuXHJcbkpRVk1hcC5wcm90b3R5cGUuZGVzZWxlY3QgPSBmdW5jdGlvbihjYywgcGF0aCkge1xyXG4gICAgY2MgPSBjYy50b0xvd2VyQ2FzZSgpO1xyXG4gICAgcGF0aCA9IHBhdGggfHwgalF1ZXJ5KCcjJyArIHRoaXMuZ2V0Q291bnRyeUlkKGNjKSlbMF07XHJcblxyXG4gICAgaWYgKHRoaXMuaXNTZWxlY3RlZChjYykpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkUmVnaW9ucy5zcGxpY2UodGhpcy5zZWxlY3RJbmRleChjYyksIDEpO1xyXG5cclxuICAgICAgICBqUXVlcnkodGhpcy5jb250YWluZXIpLnRyaWdnZXIoJ3JlZ2lvbkRlc2VsZWN0Lmpxdm1hcCcsIFtjY10pO1xyXG4gICAgICAgIHBhdGguY3VycmVudEZpbGxDb2xvciA9IHBhdGguZ2V0T3JpZ2luYWxGaWxsKCk7XHJcbiAgICAgICAgcGF0aC5zZXRGaWxsKHBhdGguZ2V0T3JpZ2luYWxGaWxsKCkpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBmb3IgKHZhciBrZXkgaW4gdGhpcy5jb3VudHJpZXMpIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFJlZ2lvbnMuc3BsaWNlKHRoaXMuc2VsZWN0ZWRSZWdpb25zLmluZGV4T2Yoa2V5KSwgMSk7XHJcbiAgICAgICAgICAgIHRoaXMuY291bnRyaWVzW2tleV0uY3VycmVudEZpbGxDb2xvciA9IHRoaXMuY29sb3I7XHJcbiAgICAgICAgICAgIHRoaXMuY291bnRyaWVzW2tleV0uc2V0RmlsbCh0aGlzLmNvbG9yKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn07XHJcblxyXG5KUVZNYXAucHJvdG90eXBlLmdldENvdW50cnlJZCA9IGZ1bmN0aW9uKGNjKSB7XHJcbiAgICByZXR1cm4gJ2pxdm1hcCcgKyB0aGlzLmluZGV4ICsgJ18nICsgY2M7XHJcbn07XHJcblxyXG5KUVZNYXAucHJvdG90eXBlLmdldFBpbiA9IGZ1bmN0aW9uKGNjKSB7XHJcbiAgICB2YXIgcGluT2JqID0galF1ZXJ5KCcjJyArIHRoaXMuZ2V0UGluSWQoY2MpKTtcclxuICAgIHJldHVybiBwaW5PYmouaHRtbCgpO1xyXG59O1xyXG5cclxuSlFWTWFwLnByb3RvdHlwZS5nZXRQaW5JZCA9IGZ1bmN0aW9uKGNjKSB7XHJcbiAgICByZXR1cm4gdGhpcy5nZXRDb3VudHJ5SWQoY2MpICsgJ19waW4nO1xyXG59O1xyXG5cclxuSlFWTWFwLnByb3RvdHlwZS5nZXRQaW5zID0gZnVuY3Rpb24oKSB7XHJcbiAgICB2YXIgcGlucyA9IHRoaXMuY29udGFpbmVyLmZpbmQoJy5qcXZtYXAtcGluJyk7XHJcbiAgICB2YXIgcmV0ID0ge307XHJcbiAgICBqUXVlcnkuZWFjaChwaW5zLCBmdW5jdGlvbihpbmRleCwgcGluT2JqKSB7XHJcbiAgICAgICAgcGluT2JqID0galF1ZXJ5KHBpbk9iaik7XHJcbiAgICAgICAgdmFyIGNjID0gcGluT2JqLmF0dHIoJ2ZvcicpLnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgdmFyIHBpbkNvbnRlbnQgPSBwaW5PYmouaHRtbCgpO1xyXG4gICAgICAgIHJldFtjY10gPSBwaW5Db250ZW50O1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gSlNPTi5zdHJpbmdpZnkocmV0KTtcclxufTtcclxuXHJcbkpRVk1hcC5wcm90b3R5cGUuaGlnaGxpZ2h0ID0gZnVuY3Rpb24oY2MsIHBhdGgpIHtcclxuICAgIHBhdGggPSBwYXRoIHx8IGpRdWVyeSgnIycgKyB0aGlzLmdldENvdW50cnlJZChjYykpWzBdO1xyXG4gICAgaWYgKHRoaXMuaG92ZXJPcGFjaXR5KSB7XHJcbiAgICAgICAgcGF0aC5zZXRPcGFjaXR5KHRoaXMuaG92ZXJPcGFjaXR5KTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5ob3ZlckNvbG9ycyAmJiAoY2MgaW4gdGhpcy5ob3ZlckNvbG9ycykpIHtcclxuICAgICAgICBwYXRoLmN1cnJlbnRGaWxsQ29sb3IgPSBwYXRoLmdldEZpbGwoKSArICcnO1xyXG4gICAgICAgIHBhdGguc2V0RmlsbCh0aGlzLmhvdmVyQ29sb3JzW2NjXSk7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuaG92ZXJDb2xvcikge1xyXG4gICAgICAgIHBhdGguY3VycmVudEZpbGxDb2xvciA9IHBhdGguZ2V0RmlsbCgpICsgJyc7XHJcbiAgICAgICAgcGF0aC5zZXRGaWxsKHRoaXMuaG92ZXJDb2xvcik7XHJcbiAgICB9XHJcbn07XHJcblxyXG5KUVZNYXAucHJvdG90eXBlLmlzU2VsZWN0ZWQgPSBmdW5jdGlvbihjYykge1xyXG4gICAgcmV0dXJuIHRoaXMuc2VsZWN0SW5kZXgoY2MpID49IDA7XHJcbn07XHJcblxyXG5KUVZNYXAucHJvdG90eXBlLm1ha2VEcmFnZ2FibGUgPSBmdW5jdGlvbigpIHtcclxuICAgIHZhciBtb3VzZURvd24gPSBmYWxzZTtcclxuICAgIHZhciBvbGRQYWdlWCwgb2xkUGFnZVk7XHJcbiAgICB2YXIgc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgc2VsZi5pc01vdmluZyA9IGZhbHNlO1xyXG4gICAgc2VsZi5pc01vdmluZ1RpbWVvdXQgPSBmYWxzZTtcclxuXHJcbiAgICB2YXIgbGFzdFRvdWNoQ291bnQ7XHJcbiAgICB2YXIgdG91Y2hDZW50ZXJYO1xyXG4gICAgdmFyIHRvdWNoQ2VudGVyWTtcclxuICAgIHZhciB0b3VjaFN0YXJ0RGlzdGFuY2U7XHJcbiAgICB2YXIgdG91Y2hTdGFydFNjYWxlO1xyXG4gICAgdmFyIHRvdWNoWDtcclxuICAgIHZhciB0b3VjaFk7XHJcblxyXG4gICAgdGhpcy5jb250YWluZXIubW91c2Vtb3ZlKGZ1bmN0aW9uKGUpIHtcclxuXHJcbiAgICAgICAgaWYgKG1vdXNlRG93bikge1xyXG4gICAgICAgICAgICBzZWxmLnRyYW5zWCAtPSAob2xkUGFnZVggLSBlLnBhZ2VYKSAvIHNlbGYuc2NhbGU7XHJcbiAgICAgICAgICAgIHNlbGYudHJhbnNZIC09IChvbGRQYWdlWSAtIGUucGFnZVkpIC8gc2VsZi5zY2FsZTtcclxuXHJcbiAgICAgICAgICAgIHNlbGYuYXBwbHlUcmFuc2Zvcm0oKTtcclxuXHJcbiAgICAgICAgICAgIG9sZFBhZ2VYID0gZS5wYWdlWDtcclxuICAgICAgICAgICAgb2xkUGFnZVkgPSBlLnBhZ2VZO1xyXG5cclxuICAgICAgICAgICAgc2VsZi5pc01vdmluZyA9IHRydWU7XHJcbiAgICAgICAgICAgIGlmIChzZWxmLmlzTW92aW5nVGltZW91dCkge1xyXG4gICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHNlbGYuaXNNb3ZpbmdUaW1lb3V0KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgc2VsZi5jb250YWluZXIudHJpZ2dlcignZHJhZycpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG5cclxuICAgIH0pLm1vdXNlZG93bihmdW5jdGlvbihlKSB7XHJcblxyXG4gICAgICAgIG1vdXNlRG93biA9IHRydWU7XHJcbiAgICAgICAgb2xkUGFnZVggPSBlLnBhZ2VYO1xyXG4gICAgICAgIG9sZFBhZ2VZID0gZS5wYWdlWTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG5cclxuICAgIH0pLm1vdXNldXAoZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIG1vdXNlRG93biA9IGZhbHNlO1xyXG5cclxuICAgICAgICBjbGVhclRpbWVvdXQoc2VsZi5pc01vdmluZ1RpbWVvdXQpO1xyXG4gICAgICAgIHNlbGYuaXNNb3ZpbmdUaW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgc2VsZi5pc01vdmluZyA9IGZhbHNlO1xyXG4gICAgICAgIH0sIDEwMCk7XHJcblxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuXHJcbiAgICB9KS5tb3VzZW91dChmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgaWYgKG1vdXNlRG93biAmJiBzZWxmLmlzTW92aW5nKSB7XHJcblxyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQoc2VsZi5pc01vdmluZ1RpbWVvdXQpO1xyXG4gICAgICAgICAgICBzZWxmLmlzTW92aW5nVGltZW91dCA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICBtb3VzZURvd24gPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHNlbGYuaXNNb3ZpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSwgMTAwKTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBqUXVlcnkodGhpcy5jb250YWluZXIpLmJpbmQoJ3RvdWNobW92ZScsIGZ1bmN0aW9uKGUpIHtcclxuXHJcbiAgICAgICAgdmFyIG9mZnNldDtcclxuICAgICAgICB2YXIgc2NhbGU7XHJcbiAgICAgICAgdmFyIHRvdWNoZXMgPSBlLm9yaWdpbmFsRXZlbnQudG91Y2hlcztcclxuICAgICAgICB2YXIgdHJhbnNmb3JtWE9sZDtcclxuICAgICAgICB2YXIgdHJhbnNmb3JtWU9sZDtcclxuXHJcbiAgICAgICAgaWYgKHRvdWNoZXMubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgICAgICAgIGlmIChsYXN0VG91Y2hDb3VudCA9PT0gMSkge1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICh0b3VjaFggPT09IHRvdWNoZXNbMF0ucGFnZVggJiYgdG91Y2hZID09PSB0b3VjaGVzWzBdLnBhZ2VZKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHRyYW5zZm9ybVhPbGQgPSBzZWxmLnRyYW5zWDtcclxuICAgICAgICAgICAgICAgIHRyYW5zZm9ybVlPbGQgPSBzZWxmLnRyYW5zWTtcclxuXHJcbiAgICAgICAgICAgICAgICBzZWxmLnRyYW5zWCAtPSAodG91Y2hYIC0gdG91Y2hlc1swXS5wYWdlWCkgLyBzZWxmLnNjYWxlO1xyXG4gICAgICAgICAgICAgICAgc2VsZi50cmFuc1kgLT0gKHRvdWNoWSAtIHRvdWNoZXNbMF0ucGFnZVkpIC8gc2VsZi5zY2FsZTtcclxuXHJcbiAgICAgICAgICAgICAgICBzZWxmLmFwcGx5VHJhbnNmb3JtKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHRyYW5zZm9ybVhPbGQgIT09IHNlbGYudHJhbnNYIHx8IHRyYW5zZm9ybVlPbGQgIT09IHNlbGYudHJhbnNZKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHNlbGYuaXNNb3ZpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgaWYgKHNlbGYuaXNNb3ZpbmdUaW1lb3V0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHNlbGYuaXNNb3ZpbmdUaW1lb3V0KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdG91Y2hYID0gdG91Y2hlc1swXS5wYWdlWDtcclxuICAgICAgICAgICAgdG91Y2hZID0gdG91Y2hlc1swXS5wYWdlWTtcclxuXHJcbiAgICAgICAgfSBlbHNlIGlmICh0b3VjaGVzLmxlbmd0aCA9PT0gMikge1xyXG5cclxuICAgICAgICAgICAgaWYgKGxhc3RUb3VjaENvdW50ID09PSAyKSB7XHJcbiAgICAgICAgICAgICAgICBzY2FsZSA9IE1hdGguc3FydChcclxuICAgICAgICAgICAgICAgICAgICBNYXRoLnBvdyh0b3VjaGVzWzBdLnBhZ2VYIC0gdG91Y2hlc1sxXS5wYWdlWCwgMikgK1xyXG4gICAgICAgICAgICAgICAgICAgIE1hdGgucG93KHRvdWNoZXNbMF0ucGFnZVkgLSB0b3VjaGVzWzFdLnBhZ2VZLCAyKVxyXG4gICAgICAgICAgICAgICAgKSAvIHRvdWNoU3RhcnREaXN0YW5jZTtcclxuXHJcbiAgICAgICAgICAgICAgICBzZWxmLnNldFNjYWxlKFxyXG4gICAgICAgICAgICAgICAgICAgIHRvdWNoU3RhcnRTY2FsZSAqIHNjYWxlLFxyXG4gICAgICAgICAgICAgICAgICAgIHRvdWNoQ2VudGVyWCxcclxuICAgICAgICAgICAgICAgICAgICB0b3VjaENlbnRlcllcclxuICAgICAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgICAgICBvZmZzZXQgPSBqUXVlcnkoc2VsZi5jb250YWluZXIpLm9mZnNldCgpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRvdWNoZXNbMF0ucGFnZVggPiB0b3VjaGVzWzFdLnBhZ2VYKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdG91Y2hDZW50ZXJYID0gdG91Y2hlc1sxXS5wYWdlWCArICh0b3VjaGVzWzBdLnBhZ2VYIC0gdG91Y2hlc1sxXS5wYWdlWCkgLyAyO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0b3VjaENlbnRlclggPSB0b3VjaGVzWzBdLnBhZ2VYICsgKHRvdWNoZXNbMV0ucGFnZVggLSB0b3VjaGVzWzBdLnBhZ2VYKSAvIDI7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHRvdWNoZXNbMF0ucGFnZVkgPiB0b3VjaGVzWzFdLnBhZ2VZKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdG91Y2hDZW50ZXJZID0gdG91Y2hlc1sxXS5wYWdlWSArICh0b3VjaGVzWzBdLnBhZ2VZIC0gdG91Y2hlc1sxXS5wYWdlWSkgLyAyO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0b3VjaENlbnRlclkgPSB0b3VjaGVzWzBdLnBhZ2VZICsgKHRvdWNoZXNbMV0ucGFnZVkgLSB0b3VjaGVzWzBdLnBhZ2VZKSAvIDI7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdG91Y2hDZW50ZXJYIC09IG9mZnNldC5sZWZ0O1xyXG4gICAgICAgICAgICAgICAgdG91Y2hDZW50ZXJZIC09IG9mZnNldC50b3A7XHJcbiAgICAgICAgICAgICAgICB0b3VjaFN0YXJ0U2NhbGUgPSBzZWxmLnNjYWxlO1xyXG5cclxuICAgICAgICAgICAgICAgIHRvdWNoU3RhcnREaXN0YW5jZSA9IE1hdGguc3FydChcclxuICAgICAgICAgICAgICAgICAgICBNYXRoLnBvdyh0b3VjaGVzWzBdLnBhZ2VYIC0gdG91Y2hlc1sxXS5wYWdlWCwgMikgK1xyXG4gICAgICAgICAgICAgICAgICAgIE1hdGgucG93KHRvdWNoZXNbMF0ucGFnZVkgLSB0b3VjaGVzWzFdLnBhZ2VZLCAyKVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGFzdFRvdWNoQ291bnQgPSB0b3VjaGVzLmxlbmd0aDtcclxuICAgIH0pO1xyXG5cclxuICAgIGpRdWVyeSh0aGlzLmNvbnRhaW5lcikuYmluZCgndG91Y2hzdGFydCcsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGxhc3RUb3VjaENvdW50ID0gMDtcclxuICAgIH0pO1xyXG5cclxuICAgIGpRdWVyeSh0aGlzLmNvbnRhaW5lcikuYmluZCgndG91Y2hlbmQnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICBsYXN0VG91Y2hDb3VudCA9IDA7XHJcbiAgICB9KTtcclxufTtcclxuXHJcbkpRVk1hcC5wcm90b3R5cGUucGxhY2VQaW5zID0gZnVuY3Rpb24ocGlucywgcGluTW9kZSkge1xyXG4gICAgdmFyIG1hcCA9IHRoaXM7XHJcblxyXG4gICAgaWYgKCFwaW5Nb2RlIHx8IChwaW5Nb2RlICE9PSAnY29udGVudCcgJiYgcGluTW9kZSAhPT0gJ2lkJykpIHtcclxuICAgICAgICBwaW5Nb2RlID0gJ2NvbnRlbnQnO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChwaW5Nb2RlID09PSAnY29udGVudCcpIHsgLy90cmVhdCBwaW4gYXMgY29udGVudFxyXG4gICAgICAgIGpRdWVyeS5lYWNoKHBpbnMsIGZ1bmN0aW9uKGluZGV4LCBwaW4pIHtcclxuICAgICAgICAgICAgaWYgKGpRdWVyeSgnIycgKyBtYXAuZ2V0Q291bnRyeUlkKGluZGV4KSkubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHZhciBwaW5JbmRleCA9IG1hcC5nZXRQaW5JZChpbmRleCk7XHJcbiAgICAgICAgICAgIHZhciAkcGluID0galF1ZXJ5KCcjJyArIHBpbkluZGV4KTtcclxuICAgICAgICAgICAgaWYgKCRwaW4ubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgJHBpbi5yZW1vdmUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBtYXAuY29udGFpbmVyLmFwcGVuZCgnPGRpdiBpZD1cIicgKyBwaW5JbmRleCArICdcIiBmb3I9XCInICsgaW5kZXggKyAnXCIgY2xhc3M9XCJqcXZtYXAtcGluXCIgc3R5bGU9XCJwb3NpdGlvbjphYnNvbHV0ZVwiPicgKyBwaW4gKyAnPC9kaXY+Jyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9IGVsc2UgeyAvL3RyZWF0IHBpbiBhcyBpZCBvZiBhbiBodG1sIGNvbnRlbnRcclxuICAgICAgICBqUXVlcnkuZWFjaChwaW5zLCBmdW5jdGlvbihpbmRleCwgcGluKSB7XHJcbiAgICAgICAgICAgIGlmIChqUXVlcnkoJyMnICsgbWFwLmdldENvdW50cnlJZChpbmRleCkpLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHZhciBwaW5JbmRleCA9IG1hcC5nZXRQaW5JZChpbmRleCk7XHJcbiAgICAgICAgICAgIHZhciAkcGluID0galF1ZXJ5KCcjJyArIHBpbkluZGV4KTtcclxuICAgICAgICAgICAgaWYgKCRwaW4ubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgJHBpbi5yZW1vdmUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBtYXAuY29udGFpbmVyLmFwcGVuZCgnPGRpdiBpZD1cIicgKyBwaW5JbmRleCArICdcIiBmb3I9XCInICsgaW5kZXggKyAnXCIgY2xhc3M9XCJqcXZtYXAtcGluXCIgc3R5bGU9XCJwb3NpdGlvbjphYnNvbHV0ZVwiPjwvZGl2PicpO1xyXG4gICAgICAgICAgICAkcGluLmFwcGVuZChqUXVlcnkoJyMnICsgcGluKSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5wb3NpdGlvblBpbnMoKTtcclxuICAgIGlmICghdGhpcy5waW5IYW5kbGVycykge1xyXG4gICAgICAgIHRoaXMucGluSGFuZGxlcnMgPSB0cnVlO1xyXG4gICAgICAgIHZhciBwb3NpdGlvbkZpeCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICBtYXAucG9zaXRpb25QaW5zKCk7XHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lci5iaW5kKCd6b29tSW4nLCBwb3NpdGlvbkZpeClcclxuICAgICAgICAgICAgLmJpbmQoJ3pvb21PdXQnLCBwb3NpdGlvbkZpeClcclxuICAgICAgICAgICAgLmJpbmQoJ2RyYWcnLCBwb3NpdGlvbkZpeCk7XHJcbiAgICB9XHJcbn07XHJcblxyXG5KUVZNYXAucHJvdG90eXBlLnBvc2l0aW9uUGlucyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgdmFyIG1hcCA9IHRoaXM7XHJcbiAgICB2YXIgcGlucyA9IHRoaXMuY29udGFpbmVyLmZpbmQoJy5qcXZtYXAtcGluJyk7XHJcbiAgICBqUXVlcnkuZWFjaChwaW5zLCBmdW5jdGlvbihpbmRleCwgcGluT2JqKSB7XHJcbiAgICAgICAgcGluT2JqID0galF1ZXJ5KHBpbk9iaik7XHJcbiAgICAgICAgdmFyIGNvdW50cnlJZCA9IG1hcC5nZXRDb3VudHJ5SWQocGluT2JqLmF0dHIoJ2ZvcicpLnRvTG93ZXJDYXNlKCkpO1xyXG4gICAgICAgIHZhciBjb3VudHJ5T2JqID0galF1ZXJ5KCcjJyArIGNvdW50cnlJZCk7XHJcbiAgICAgICAgdmFyIGJib3ggPSBjb3VudHJ5T2JqWzBdLmdldEJCb3goKTtcclxuXHJcbiAgICAgICAgdmFyIHNjYWxlID0gbWFwLnNjYWxlO1xyXG4gICAgICAgIHZhciByb290Q29vcmRzID0gbWFwLmNhbnZhcy5yb290R3JvdXAuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICAgICAgdmFyIG1hcENvb3JkcyA9IG1hcC5jb250YWluZXJbMF0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICAgICAgdmFyIGNvb3JkcyA9IHtcclxuICAgICAgICAgICAgbGVmdDogcm9vdENvb3Jkcy5sZWZ0IC0gbWFwQ29vcmRzLmxlZnQsXHJcbiAgICAgICAgICAgIHRvcDogcm9vdENvb3Jkcy50b3AgLSBtYXBDb29yZHMudG9wXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgdmFyIG1pZGRsZVggPSAoYmJveC54ICogc2NhbGUpICsgKChiYm94LndpZHRoICogc2NhbGUpIC8gMik7XHJcbiAgICAgICAgdmFyIG1pZGRsZVkgPSAoYmJveC55ICogc2NhbGUpICsgKChiYm94LmhlaWdodCAqIHNjYWxlKSAvIDIpO1xyXG5cclxuICAgICAgICBwaW5PYmouY3NzKHtcclxuICAgICAgICAgICAgbGVmdDogY29vcmRzLmxlZnQgKyBtaWRkbGVYIC0gKHBpbk9iai53aWR0aCgpIC8gMiksXHJcbiAgICAgICAgICAgIHRvcDogY29vcmRzLnRvcCArIG1pZGRsZVkgLSAocGluT2JqLmhlaWdodCgpIC8gMilcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG59O1xyXG5cclxuSlFWTWFwLnByb3RvdHlwZS5yZW1vdmVQaW4gPSBmdW5jdGlvbihjYykge1xyXG4gICAgY2MgPSBjYy50b0xvd2VyQ2FzZSgpO1xyXG4gICAgalF1ZXJ5KCcjJyArIHRoaXMuZ2V0UGluSWQoY2MpKS5yZW1vdmUoKTtcclxufTtcclxuXHJcbkpRVk1hcC5wcm90b3R5cGUucmVtb3ZlUGlucyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgdGhpcy5jb250YWluZXIuZmluZCgnLmpxdm1hcC1waW4nKS5yZW1vdmUoKTtcclxufTtcclxuXHJcbkpRVk1hcC5wcm90b3R5cGUucmVzZXQgPSBmdW5jdGlvbigpIHtcclxuICAgIGZvciAodmFyIGtleSBpbiB0aGlzLmNvdW50cmllcykge1xyXG4gICAgICAgIHRoaXMuY291bnRyaWVzW2tleV0uc2V0RmlsbCh0aGlzLmNvbG9yKTtcclxuICAgIH1cclxuICAgIHRoaXMuc2NhbGUgPSB0aGlzLmJhc2VTY2FsZTtcclxuICAgIHRoaXMudHJhbnNYID0gdGhpcy5iYXNlVHJhbnNYO1xyXG4gICAgdGhpcy50cmFuc1kgPSB0aGlzLmJhc2VUcmFuc1k7XHJcbiAgICB0aGlzLmFwcGx5VHJhbnNmb3JtKCk7XHJcbiAgICB0aGlzLnpvb21DdXJTdGVwID0gMTtcclxufTtcclxuXHJcbkpRVk1hcC5wcm90b3R5cGUucmVzaXplID0gZnVuY3Rpb24oKSB7XHJcbiAgICB2YXIgY3VyQmFzZVNjYWxlID0gdGhpcy5iYXNlU2NhbGU7XHJcbiAgICBpZiAodGhpcy53aWR0aCAvIHRoaXMuaGVpZ2h0ID4gdGhpcy5kZWZhdWx0V2lkdGggLyB0aGlzLmRlZmF1bHRIZWlnaHQpIHtcclxuICAgICAgICB0aGlzLmJhc2VTY2FsZSA9IHRoaXMuaGVpZ2h0IC8gdGhpcy5kZWZhdWx0SGVpZ2h0O1xyXG4gICAgICAgIHRoaXMuYmFzZVRyYW5zWCA9IE1hdGguYWJzKHRoaXMud2lkdGggLSB0aGlzLmRlZmF1bHRXaWR0aCAqIHRoaXMuYmFzZVNjYWxlKSAvICgyICogdGhpcy5iYXNlU2NhbGUpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmJhc2VTY2FsZSA9IHRoaXMud2lkdGggLyB0aGlzLmRlZmF1bHRXaWR0aDtcclxuICAgICAgICB0aGlzLmJhc2VUcmFuc1kgPSBNYXRoLmFicyh0aGlzLmhlaWdodCAtIHRoaXMuZGVmYXVsdEhlaWdodCAqIHRoaXMuYmFzZVNjYWxlKSAvICgyICogdGhpcy5iYXNlU2NhbGUpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5zY2FsZSAqPSB0aGlzLmJhc2VTY2FsZSAvIGN1ckJhc2VTY2FsZTtcclxuICAgIHRoaXMudHJhbnNYICo9IHRoaXMuYmFzZVNjYWxlIC8gY3VyQmFzZVNjYWxlO1xyXG4gICAgdGhpcy50cmFuc1kgKj0gdGhpcy5iYXNlU2NhbGUgLyBjdXJCYXNlU2NhbGU7XHJcbn07XHJcblxyXG5KUVZNYXAucHJvdG90eXBlLnNlbGVjdCA9IGZ1bmN0aW9uKGNjLCBwYXRoKSB7XHJcbiAgICBjYyA9IGNjLnRvTG93ZXJDYXNlKCk7XHJcbiAgICBwYXRoID0gcGF0aCB8fCBqUXVlcnkoJyMnICsgdGhpcy5nZXRDb3VudHJ5SWQoY2MpKVswXTtcclxuXHJcbiAgICBpZiAoIXRoaXMuaXNTZWxlY3RlZChjYykpIHtcclxuICAgICAgICBpZiAodGhpcy5tdWx0aVNlbGVjdFJlZ2lvbikge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkUmVnaW9ucy5wdXNoKGNjKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkUmVnaW9ucyA9IFtjY107XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBqUXVlcnkodGhpcy5jb250YWluZXIpLnRyaWdnZXIoJ3JlZ2lvblNlbGVjdC5qcXZtYXAnLCBbY2NdKTtcclxuICAgICAgICBpZiAodGhpcy5zZWxlY3RlZENvbG9yICYmIHBhdGgpIHtcclxuICAgICAgICAgICAgcGF0aC5jdXJyZW50RmlsbENvbG9yID0gdGhpcy5zZWxlY3RlZENvbG9yO1xyXG4gICAgICAgICAgICBwYXRoLnNldEZpbGwodGhpcy5zZWxlY3RlZENvbG9yKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn07XHJcblxyXG5KUVZNYXAucHJvdG90eXBlLnNlbGVjdEluZGV4ID0gZnVuY3Rpb24oY2MpIHtcclxuICAgIGNjID0gY2MudG9Mb3dlckNhc2UoKTtcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5zZWxlY3RlZFJlZ2lvbnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICBpZiAoY2MgPT09IHRoaXMuc2VsZWN0ZWRSZWdpb25zW2ldKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiAtMTtcclxufTtcclxuXHJcbkpRVk1hcC5wcm90b3R5cGUuc2V0QmFja2dyb3VuZENvbG9yID0gZnVuY3Rpb24oYmFja2dyb3VuZENvbG9yKSB7XHJcbiAgICB0aGlzLmNvbnRhaW5lci5jc3MoJ2JhY2tncm91bmQtY29sb3InLCBiYWNrZ3JvdW5kQ29sb3IpO1xyXG59O1xyXG5cclxuSlFWTWFwLnByb3RvdHlwZS5zZXRDb2xvcnMgPSBmdW5jdGlvbihrZXksIGNvbG9yKSB7XHJcbiAgICBpZiAodHlwZW9mIGtleSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICB0aGlzLmNvdW50cmllc1trZXldLnNldEZpbGwoY29sb3IpO1xyXG4gICAgICAgIHRoaXMuY291bnRyaWVzW2tleV0uc2V0QXR0cmlidXRlKCdvcmlnaW5hbCcsIGNvbG9yKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdmFyIGNvbG9ycyA9IGtleTtcclxuXHJcbiAgICAgICAgZm9yICh2YXIgY29kZSBpbiBjb2xvcnMpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuY291bnRyaWVzW2NvZGVdKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvdW50cmllc1tjb2RlXS5zZXRGaWxsKGNvbG9yc1tjb2RlXSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvdW50cmllc1tjb2RlXS5zZXRBdHRyaWJ1dGUoJ29yaWdpbmFsJywgY29sb3JzW2NvZGVdKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufTtcclxuXHJcbkpRVk1hcC5wcm90b3R5cGUuc2V0Tm9ybWFsaXplRnVuY3Rpb24gPSBmdW5jdGlvbihmKSB7XHJcbiAgICB0aGlzLmNvbG9yU2NhbGUuc2V0Tm9ybWFsaXplRnVuY3Rpb24oZik7XHJcblxyXG4gICAgaWYgKHRoaXMudmFsdWVzKSB7XHJcbiAgICAgICAgdGhpcy5zZXRWYWx1ZXModGhpcy52YWx1ZXMpO1xyXG4gICAgfVxyXG59O1xyXG5cclxuSlFWTWFwLnByb3RvdHlwZS5zZXRTY2FsZSA9IGZ1bmN0aW9uKHNjYWxlKSB7XHJcbiAgICB0aGlzLnNjYWxlID0gc2NhbGU7XHJcbiAgICB0aGlzLmFwcGx5VHJhbnNmb3JtKCk7XHJcbn07XHJcblxyXG5KUVZNYXAucHJvdG90eXBlLnNldFNjYWxlQ29sb3JzID0gZnVuY3Rpb24oY29sb3JzKSB7XHJcbiAgICB0aGlzLmNvbG9yU2NhbGUuc2V0Q29sb3JzKGNvbG9ycyk7XHJcblxyXG4gICAgaWYgKHRoaXMudmFsdWVzKSB7XHJcbiAgICAgICAgdGhpcy5zZXRWYWx1ZXModGhpcy52YWx1ZXMpO1xyXG4gICAgfVxyXG59O1xyXG5cclxuSlFWTWFwLnByb3RvdHlwZS5zZXRWYWx1ZXMgPSBmdW5jdGlvbih2YWx1ZXMpIHtcclxuICAgIHZhciBtYXggPSAwLFxyXG4gICAgICAgIG1pbiA9IE51bWJlci5NQVhfVkFMVUUsXHJcbiAgICAgICAgdmFsO1xyXG5cclxuICAgIGZvciAodmFyIGNjIGluIHZhbHVlcykge1xyXG4gICAgICAgIGNjID0gY2MudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICB2YWwgPSBwYXJzZUZsb2F0KHZhbHVlc1tjY10pO1xyXG5cclxuICAgICAgICBpZiAoaXNOYU4odmFsKSkge1xyXG4gICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHZhbCA+IG1heCkge1xyXG4gICAgICAgICAgICBtYXggPSB2YWx1ZXNbY2NdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodmFsIDwgbWluKSB7XHJcbiAgICAgICAgICAgIG1pbiA9IHZhbDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKG1pbiA9PT0gbWF4KSB7XHJcbiAgICAgICAgbWF4Kys7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5jb2xvclNjYWxlLnNldE1pbihtaW4pO1xyXG4gICAgdGhpcy5jb2xvclNjYWxlLnNldE1heChtYXgpO1xyXG5cclxuICAgIHZhciBjb2xvcnMgPSB7fTtcclxuICAgIGZvciAoY2MgaW4gdmFsdWVzKSB7XHJcbiAgICAgICAgY2MgPSBjYy50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIHZhbCA9IHBhcnNlRmxvYXQodmFsdWVzW2NjXSk7XHJcbiAgICAgICAgY29sb3JzW2NjXSA9IGlzTmFOKHZhbCkgPyB0aGlzLmNvbG9yIDogdGhpcy5jb2xvclNjYWxlLmdldENvbG9yKHZhbCk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnNldENvbG9ycyhjb2xvcnMpO1xyXG4gICAgdGhpcy52YWx1ZXMgPSB2YWx1ZXM7XHJcbn07XHJcblxyXG5KUVZNYXAucHJvdG90eXBlLnVuaGlnaGxpZ2h0ID0gZnVuY3Rpb24oY2MsIHBhdGgpIHtcclxuICAgIGNjID0gY2MudG9Mb3dlckNhc2UoKTtcclxuICAgIHBhdGggPSBwYXRoIHx8IGpRdWVyeSgnIycgKyB0aGlzLmdldENvdW50cnlJZChjYykpWzBdO1xyXG4gICAgcGF0aC5zZXRPcGFjaXR5KDEpO1xyXG4gICAgaWYgKHBhdGguY3VycmVudEZpbGxDb2xvcikge1xyXG4gICAgICAgIHBhdGguc2V0RmlsbChwYXRoLmN1cnJlbnRGaWxsQ29sb3IpO1xyXG4gICAgfVxyXG59O1xyXG5cclxuSlFWTWFwLnByb3RvdHlwZS56b29tSW4gPSBmdW5jdGlvbigpIHtcclxuICAgIHZhciBtYXAgPSB0aGlzO1xyXG4gICAgdmFyIHNsaWRlckRlbHRhID0gKGpRdWVyeSgnI3pvb20nKS5pbm5lckhlaWdodCgpIC0gNiAqIDIgLSAxNSAqIDIgLSAzICogMiAtIDcgLSA2KSAvICh0aGlzLnpvb21NYXhTdGVwIC0gdGhpcy56b29tQ3VyU3RlcCk7XHJcblxyXG4gICAgaWYgKG1hcC56b29tQ3VyU3RlcCA8IG1hcC56b29tTWF4U3RlcCkge1xyXG4gICAgICAgIG1hcC50cmFuc1ggLT0gKG1hcC53aWR0aCAvIG1hcC5zY2FsZSAtIG1hcC53aWR0aCAvIChtYXAuc2NhbGUgKiBtYXAuem9vbVN0ZXApKSAvIDI7XHJcbiAgICAgICAgbWFwLnRyYW5zWSAtPSAobWFwLmhlaWdodCAvIG1hcC5zY2FsZSAtIG1hcC5oZWlnaHQgLyAobWFwLnNjYWxlICogbWFwLnpvb21TdGVwKSkgLyAyO1xyXG4gICAgICAgIG1hcC5zZXRTY2FsZShtYXAuc2NhbGUgKiBtYXAuem9vbVN0ZXApO1xyXG4gICAgICAgIG1hcC56b29tQ3VyU3RlcCsrO1xyXG5cclxuICAgICAgICB2YXIgJHNsaWRlciA9IGpRdWVyeSgnI3pvb21TbGlkZXInKTtcclxuXHJcbiAgICAgICAgJHNsaWRlci5jc3MoJ3RvcCcsIHBhcnNlSW50KCRzbGlkZXIuY3NzKCd0b3AnKSwgMTApIC0gc2xpZGVyRGVsdGEpO1xyXG5cclxuICAgICAgICBtYXAuY29udGFpbmVyLnRyaWdnZXIoJ3pvb21JbicpO1xyXG4gICAgfVxyXG59O1xyXG5cclxuSlFWTWFwLnByb3RvdHlwZS56b29tT3V0ID0gZnVuY3Rpb24oKSB7XHJcbiAgICB2YXIgbWFwID0gdGhpcztcclxuICAgIHZhciBzbGlkZXJEZWx0YSA9IChqUXVlcnkoJyN6b29tJykuaW5uZXJIZWlnaHQoKSAtIDYgKiAyIC0gMTUgKiAyIC0gMyAqIDIgLSA3IC0gNikgLyAodGhpcy56b29tTWF4U3RlcCAtIHRoaXMuem9vbUN1clN0ZXApO1xyXG5cclxuICAgIGlmIChtYXAuem9vbUN1clN0ZXAgPiAxKSB7XHJcbiAgICAgICAgbWFwLnRyYW5zWCArPSAobWFwLndpZHRoIC8gKG1hcC5zY2FsZSAvIG1hcC56b29tU3RlcCkgLSBtYXAud2lkdGggLyBtYXAuc2NhbGUpIC8gMjtcclxuICAgICAgICBtYXAudHJhbnNZICs9IChtYXAuaGVpZ2h0IC8gKG1hcC5zY2FsZSAvIG1hcC56b29tU3RlcCkgLSBtYXAuaGVpZ2h0IC8gbWFwLnNjYWxlKSAvIDI7XHJcbiAgICAgICAgbWFwLnNldFNjYWxlKG1hcC5zY2FsZSAvIG1hcC56b29tU3RlcCk7XHJcbiAgICAgICAgbWFwLnpvb21DdXJTdGVwLS07XHJcblxyXG4gICAgICAgIHZhciAkc2xpZGVyID0galF1ZXJ5KCcjem9vbVNsaWRlcicpO1xyXG5cclxuICAgICAgICAkc2xpZGVyLmNzcygndG9wJywgcGFyc2VJbnQoJHNsaWRlci5jc3MoJ3RvcCcpLCAxMCkgKyBzbGlkZXJEZWx0YSk7XHJcblxyXG4gICAgICAgIG1hcC5jb250YWluZXIudHJpZ2dlcignem9vbU91dCcpO1xyXG4gICAgfVxyXG59O1xyXG5cclxuVmVjdG9yQ2FudmFzLnByb3RvdHlwZS5hcHBseVRyYW5zZm9ybVBhcmFtcyA9IGZ1bmN0aW9uKHNjYWxlLCB0cmFuc1gsIHRyYW5zWSkge1xyXG4gICAgaWYgKHRoaXMubW9kZSA9PT0gJ3N2ZycpIHtcclxuICAgICAgICB0aGlzLnJvb3RHcm91cC5zZXRBdHRyaWJ1dGUoJ3RyYW5zZm9ybScsICdzY2FsZSgnICsgc2NhbGUgKyAnKSB0cmFuc2xhdGUoJyArIHRyYW5zWCArICcsICcgKyB0cmFuc1kgKyAnKScpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnJvb3RHcm91cC5jb29yZG9yaWdpbiA9ICh0aGlzLndpZHRoIC0gdHJhbnNYKSArICcsJyArICh0aGlzLmhlaWdodCAtIHRyYW5zWSk7XHJcbiAgICAgICAgdGhpcy5yb290R3JvdXAuY29vcmRzaXplID0gdGhpcy53aWR0aCAvIHNjYWxlICsgJywnICsgdGhpcy5oZWlnaHQgLyBzY2FsZTtcclxuICAgIH1cclxufTtcclxuXHJcblZlY3RvckNhbnZhcy5wcm90b3R5cGUuY3JlYXRlR3JvdXAgPSBmdW5jdGlvbihpc1Jvb3QpIHtcclxuICAgIHZhciBub2RlO1xyXG4gICAgaWYgKHRoaXMubW9kZSA9PT0gJ3N2ZycpIHtcclxuICAgICAgICBub2RlID0gdGhpcy5jcmVhdGVTdmdOb2RlKCdnJyk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIG5vZGUgPSB0aGlzLmNyZWF0ZVZtbE5vZGUoJ2dyb3VwJyk7XHJcbiAgICAgICAgbm9kZS5zdHlsZS53aWR0aCA9IHRoaXMud2lkdGggKyAncHgnO1xyXG4gICAgICAgIG5vZGUuc3R5bGUuaGVpZ2h0ID0gdGhpcy5oZWlnaHQgKyAncHgnO1xyXG4gICAgICAgIG5vZGUuc3R5bGUubGVmdCA9ICcwcHgnO1xyXG4gICAgICAgIG5vZGUuc3R5bGUudG9wID0gJzBweCc7XHJcbiAgICAgICAgbm9kZS5jb29yZG9yaWdpbiA9ICcwIDAnO1xyXG4gICAgICAgIG5vZGUuY29vcmRzaXplID0gdGhpcy53aWR0aCArICcgJyArIHRoaXMuaGVpZ2h0O1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChpc1Jvb3QpIHtcclxuICAgICAgICB0aGlzLnJvb3RHcm91cCA9IG5vZGU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbm9kZTtcclxufTtcclxuXHJcblZlY3RvckNhbnZhcy5wcm90b3R5cGUuY3JlYXRlUGF0aCA9IGZ1bmN0aW9uKGNvbmZpZykge1xyXG4gICAgdmFyIG5vZGU7XHJcbiAgICBpZiAodGhpcy5tb2RlID09PSAnc3ZnJykge1xyXG4gICAgICAgIG5vZGUgPSB0aGlzLmNyZWF0ZVN2Z05vZGUoJ3BhdGgnKTtcclxuICAgICAgICBub2RlLnNldEF0dHJpYnV0ZSgnZCcsIGNvbmZpZy5wYXRoKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMucGFyYW1zLmJvcmRlckNvbG9yICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIG5vZGUuc2V0QXR0cmlidXRlKCdzdHJva2UnLCB0aGlzLnBhcmFtcy5ib3JkZXJDb2xvcik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnBhcmFtcy5ib3JkZXJXaWR0aCA+IDApIHtcclxuICAgICAgICAgICAgbm9kZS5zZXRBdHRyaWJ1dGUoJ3N0cm9rZS13aWR0aCcsIHRoaXMucGFyYW1zLmJvcmRlcldpZHRoKTtcclxuICAgICAgICAgICAgbm9kZS5zZXRBdHRyaWJ1dGUoJ3N0cm9rZS1saW5lY2FwJywgJ3JvdW5kJyk7XHJcbiAgICAgICAgICAgIG5vZGUuc2V0QXR0cmlidXRlKCdzdHJva2UtbGluZWpvaW4nLCAncm91bmQnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMucGFyYW1zLmJvcmRlck9wYWNpdHkgPiAwKSB7XHJcbiAgICAgICAgICAgIG5vZGUuc2V0QXR0cmlidXRlKCdzdHJva2Utb3BhY2l0eScsIHRoaXMucGFyYW1zLmJvcmRlck9wYWNpdHkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbm9kZS5zZXRGaWxsID0gZnVuY3Rpb24oY29sb3IpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRBdHRyaWJ1dGUoJ2ZpbGwnLCBjb2xvcik7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmdldEF0dHJpYnV0ZSgnb3JpZ2luYWwnKSA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRBdHRyaWJ1dGUoJ29yaWdpbmFsJywgY29sb3IpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgbm9kZS5nZXRGaWxsID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmdldEF0dHJpYnV0ZSgnZmlsbCcpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIG5vZGUuZ2V0T3JpZ2luYWxGaWxsID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmdldEF0dHJpYnV0ZSgnb3JpZ2luYWwnKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBub2RlLnNldE9wYWNpdHkgPSBmdW5jdGlvbihvcGFjaXR5KSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0QXR0cmlidXRlKCdmaWxsLW9wYWNpdHknLCBvcGFjaXR5KTtcclxuICAgICAgICB9O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBub2RlID0gdGhpcy5jcmVhdGVWbWxOb2RlKCdzaGFwZScpO1xyXG4gICAgICAgIG5vZGUuY29vcmRvcmlnaW4gPSAnMCAwJztcclxuICAgICAgICBub2RlLmNvb3Jkc2l6ZSA9IHRoaXMud2lkdGggKyAnICcgKyB0aGlzLmhlaWdodDtcclxuICAgICAgICBub2RlLnN0eWxlLndpZHRoID0gdGhpcy53aWR0aCArICdweCc7XHJcbiAgICAgICAgbm9kZS5zdHlsZS5oZWlnaHQgPSB0aGlzLmhlaWdodCArICdweCc7XHJcbiAgICAgICAgbm9kZS5maWxsY29sb3IgPSBKUVZNYXAuZGVmYXVsdEZpbGxDb2xvcjtcclxuICAgICAgICBub2RlLnN0cm9rZWQgPSBmYWxzZTtcclxuICAgICAgICBub2RlLnBhdGggPSBWZWN0b3JDYW52YXMucGF0aFN2Z1RvVm1sKGNvbmZpZy5wYXRoKTtcclxuXHJcbiAgICAgICAgdmFyIHNjYWxlID0gdGhpcy5jcmVhdGVWbWxOb2RlKCdza2V3Jyk7XHJcbiAgICAgICAgc2NhbGUub24gPSB0cnVlO1xyXG4gICAgICAgIHNjYWxlLm1hdHJpeCA9ICcwLjAxLDAsMCwwLjAxLDAsMCc7XHJcbiAgICAgICAgc2NhbGUub2Zmc2V0ID0gJzAsMCc7XHJcblxyXG4gICAgICAgIG5vZGUuYXBwZW5kQ2hpbGQoc2NhbGUpO1xyXG5cclxuICAgICAgICB2YXIgZmlsbCA9IHRoaXMuY3JlYXRlVm1sTm9kZSgnZmlsbCcpO1xyXG4gICAgICAgIG5vZGUuYXBwZW5kQ2hpbGQoZmlsbCk7XHJcblxyXG4gICAgICAgIG5vZGUuc2V0RmlsbCA9IGZ1bmN0aW9uKGNvbG9yKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2ZpbGwnKVswXS5jb2xvciA9IGNvbG9yO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5nZXRBdHRyaWJ1dGUoJ29yaWdpbmFsJykgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0QXR0cmlidXRlKCdvcmlnaW5hbCcsIGNvbG9yKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIG5vZGUuZ2V0RmlsbCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5nZXRFbGVtZW50c0J5VGFnTmFtZSgnZmlsbCcpWzBdLmNvbG9yO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgbm9kZS5nZXRPcmlnaW5hbEZpbGwgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0QXR0cmlidXRlKCdvcmlnaW5hbCcpO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgbm9kZS5zZXRPcGFjaXR5ID0gZnVuY3Rpb24ob3BhY2l0eSkge1xyXG4gICAgICAgICAgICB0aGlzLmdldEVsZW1lbnRzQnlUYWdOYW1lKCdmaWxsJylbMF0ub3BhY2l0eSA9IHBhcnNlSW50KG9wYWNpdHkgKiAxMDAsIDEwKSArICclJztcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG5vZGU7XHJcbn07XHJcblxyXG5WZWN0b3JDYW52YXMucHJvdG90eXBlLnBhdGhTdmdUb1ZtbCA9IGZ1bmN0aW9uKHBhdGgpIHtcclxuICAgIHZhciByZXN1bHQgPSAnJztcclxuICAgIHZhciBjeCA9IDAsXHJcbiAgICAgICAgY3kgPSAwLFxyXG4gICAgICAgIGN0cmx4LCBjdHJseTtcclxuXHJcbiAgICByZXR1cm4gcGF0aC5yZXBsYWNlKC8oW01tTGxIaFZ2Q2NTc10pKCg/Oi0/KD86XFxkKyk/KD86XFwuXFxkKyk/LD9cXHM/KSspL2csIGZ1bmN0aW9uKHNlZ21lbnQsIGxldHRlciwgY29vcmRzKSB7XHJcbiAgICAgICAgY29vcmRzID0gY29vcmRzLnJlcGxhY2UoLyhcXGQpLS9nLCAnJDEsLScpLnJlcGxhY2UoL1xccysvZywgJywnKS5zcGxpdCgnLCcpO1xyXG4gICAgICAgIGlmICghY29vcmRzWzBdKSB7XHJcbiAgICAgICAgICAgIGNvb3Jkcy5zaGlmdCgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIGwgPSBjb29yZHMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGNvb3Jkc1tpXSA9IE1hdGgucm91bmQoMTAwICogY29vcmRzW2ldKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHN3aXRjaCAobGV0dGVyKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ20nOlxyXG4gICAgICAgICAgICAgICAgY3ggKz0gY29vcmRzWzBdO1xyXG4gICAgICAgICAgICAgICAgY3kgKz0gY29vcmRzWzFdO1xyXG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gJ3QnICsgY29vcmRzLmpvaW4oJywnKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSAnTSc6XHJcbiAgICAgICAgICAgICAgICBjeCA9IGNvb3Jkc1swXTtcclxuICAgICAgICAgICAgICAgIGN5ID0gY29vcmRzWzFdO1xyXG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gJ20nICsgY29vcmRzLmpvaW4oJywnKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSAnbCc6XHJcbiAgICAgICAgICAgICAgICBjeCArPSBjb29yZHNbMF07XHJcbiAgICAgICAgICAgICAgICBjeSArPSBjb29yZHNbMV07XHJcbiAgICAgICAgICAgICAgICByZXN1bHQgPSAncicgKyBjb29yZHMuam9pbignLCcpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBjYXNlICdMJzpcclxuICAgICAgICAgICAgICAgIGN4ID0gY29vcmRzWzBdO1xyXG4gICAgICAgICAgICAgICAgY3kgPSBjb29yZHNbMV07XHJcbiAgICAgICAgICAgICAgICByZXN1bHQgPSAnbCcgKyBjb29yZHMuam9pbignLCcpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBjYXNlICdoJzpcclxuICAgICAgICAgICAgICAgIGN4ICs9IGNvb3Jkc1swXTtcclxuICAgICAgICAgICAgICAgIHJlc3VsdCA9ICdyJyArIGNvb3Jkc1swXSArICcsMCc7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgJ0gnOlxyXG4gICAgICAgICAgICAgICAgY3ggPSBjb29yZHNbMF07XHJcbiAgICAgICAgICAgICAgICByZXN1bHQgPSAnbCcgKyBjeCArICcsJyArIGN5O1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBjYXNlICd2JzpcclxuICAgICAgICAgICAgICAgIGN5ICs9IGNvb3Jkc1swXTtcclxuICAgICAgICAgICAgICAgIHJlc3VsdCA9ICdyMCwnICsgY29vcmRzWzBdO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBjYXNlICdWJzpcclxuICAgICAgICAgICAgICAgIGN5ID0gY29vcmRzWzBdO1xyXG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gJ2wnICsgY3ggKyAnLCcgKyBjeTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSAnYyc6XHJcbiAgICAgICAgICAgICAgICBjdHJseCA9IGN4ICsgY29vcmRzW2Nvb3Jkcy5sZW5ndGggLSA0XTtcclxuICAgICAgICAgICAgICAgIGN0cmx5ID0gY3kgKyBjb29yZHNbY29vcmRzLmxlbmd0aCAtIDNdO1xyXG4gICAgICAgICAgICAgICAgY3ggKz0gY29vcmRzW2Nvb3Jkcy5sZW5ndGggLSAyXTtcclxuICAgICAgICAgICAgICAgIGN5ICs9IGNvb3Jkc1tjb29yZHMubGVuZ3RoIC0gMV07XHJcbiAgICAgICAgICAgICAgICByZXN1bHQgPSAndicgKyBjb29yZHMuam9pbignLCcpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBjYXNlICdDJzpcclxuICAgICAgICAgICAgICAgIGN0cmx4ID0gY29vcmRzW2Nvb3Jkcy5sZW5ndGggLSA0XTtcclxuICAgICAgICAgICAgICAgIGN0cmx5ID0gY29vcmRzW2Nvb3Jkcy5sZW5ndGggLSAzXTtcclxuICAgICAgICAgICAgICAgIGN4ID0gY29vcmRzW2Nvb3Jkcy5sZW5ndGggLSAyXTtcclxuICAgICAgICAgICAgICAgIGN5ID0gY29vcmRzW2Nvb3Jkcy5sZW5ndGggLSAxXTtcclxuICAgICAgICAgICAgICAgIHJlc3VsdCA9ICdjJyArIGNvb3Jkcy5qb2luKCcsJyk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgJ3MnOlxyXG4gICAgICAgICAgICAgICAgY29vcmRzLnVuc2hpZnQoY3kgLSBjdHJseSk7XHJcbiAgICAgICAgICAgICAgICBjb29yZHMudW5zaGlmdChjeCAtIGN0cmx4KTtcclxuICAgICAgICAgICAgICAgIGN0cmx4ID0gY3ggKyBjb29yZHNbY29vcmRzLmxlbmd0aCAtIDRdO1xyXG4gICAgICAgICAgICAgICAgY3RybHkgPSBjeSArIGNvb3Jkc1tjb29yZHMubGVuZ3RoIC0gM107XHJcbiAgICAgICAgICAgICAgICBjeCArPSBjb29yZHNbY29vcmRzLmxlbmd0aCAtIDJdO1xyXG4gICAgICAgICAgICAgICAgY3kgKz0gY29vcmRzW2Nvb3Jkcy5sZW5ndGggLSAxXTtcclxuICAgICAgICAgICAgICAgIHJlc3VsdCA9ICd2JyArIGNvb3Jkcy5qb2luKCcsJyk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgJ1MnOlxyXG4gICAgICAgICAgICAgICAgY29vcmRzLnVuc2hpZnQoY3kgKyBjeSAtIGN0cmx5KTtcclxuICAgICAgICAgICAgICAgIGNvb3Jkcy51bnNoaWZ0KGN4ICsgY3ggLSBjdHJseCk7XHJcbiAgICAgICAgICAgICAgICBjdHJseCA9IGNvb3Jkc1tjb29yZHMubGVuZ3RoIC0gNF07XHJcbiAgICAgICAgICAgICAgICBjdHJseSA9IGNvb3Jkc1tjb29yZHMubGVuZ3RoIC0gM107XHJcbiAgICAgICAgICAgICAgICBjeCA9IGNvb3Jkc1tjb29yZHMubGVuZ3RoIC0gMl07XHJcbiAgICAgICAgICAgICAgICBjeSA9IGNvb3Jkc1tjb29yZHMubGVuZ3RoIC0gMV07XHJcbiAgICAgICAgICAgICAgICByZXN1bHQgPSAnYycgKyBjb29yZHMuam9pbignLCcpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG5cclxuICAgIH0pLnJlcGxhY2UoL3ovZywgJycpO1xyXG59O1xyXG5cclxuVmVjdG9yQ2FudmFzLnByb3RvdHlwZS5zZXRTaXplID0gZnVuY3Rpb24od2lkdGgsIGhlaWdodCkge1xyXG4gICAgaWYgKHRoaXMubW9kZSA9PT0gJ3N2ZycpIHtcclxuICAgICAgICB0aGlzLmNhbnZhcy5zZXRBdHRyaWJ1dGUoJ3dpZHRoJywgd2lkdGgpO1xyXG4gICAgICAgIHRoaXMuY2FudmFzLnNldEF0dHJpYnV0ZSgnaGVpZ2h0JywgaGVpZ2h0KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5jYW52YXMuc3R5bGUud2lkdGggPSB3aWR0aCArICdweCc7XHJcbiAgICAgICAgdGhpcy5jYW52YXMuc3R5bGUuaGVpZ2h0ID0gaGVpZ2h0ICsgJ3B4JztcclxuICAgICAgICB0aGlzLmNhbnZhcy5jb29yZHNpemUgPSB3aWR0aCArICcgJyArIGhlaWdodDtcclxuICAgICAgICB0aGlzLmNhbnZhcy5jb29yZG9yaWdpbiA9ICcwIDAnO1xyXG4gICAgICAgIGlmICh0aGlzLnJvb3RHcm91cCkge1xyXG4gICAgICAgICAgICB2YXIgcGF0aHMgPSB0aGlzLnJvb3RHcm91cC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnc2hhcGUnKTtcclxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDAsIGwgPSBwYXRocy5sZW5ndGg7IGkgPCBsOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIHBhdGhzW2ldLmNvb3Jkc2l6ZSA9IHdpZHRoICsgJyAnICsgaGVpZ2h0O1xyXG4gICAgICAgICAgICAgICAgcGF0aHNbaV0uc3R5bGUud2lkdGggPSB3aWR0aCArICdweCc7XHJcbiAgICAgICAgICAgICAgICBwYXRoc1tpXS5zdHlsZS5oZWlnaHQgPSBoZWlnaHQgKyAncHgnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMucm9vdEdyb3VwLmNvb3Jkc2l6ZSA9IHdpZHRoICsgJyAnICsgaGVpZ2h0O1xyXG4gICAgICAgICAgICB0aGlzLnJvb3RHcm91cC5zdHlsZS53aWR0aCA9IHdpZHRoICsgJ3B4JztcclxuICAgICAgICAgICAgdGhpcy5yb290R3JvdXAuc3R5bGUuaGVpZ2h0ID0gaGVpZ2h0ICsgJ3B4JztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICB0aGlzLndpZHRoID0gd2lkdGg7XHJcbiAgICB0aGlzLmhlaWdodCA9IGhlaWdodDtcclxufTsiLCIvKiogQWRkIFdvcmxkIE1hcCBEYXRhIFBvaW50cyAqL1xyXG5qUXVlcnkuZm4udmVjdG9yTWFwKCdhZGRNYXAnLCAnd29ybGRfZW4nLCB7XCJ3aWR0aFwiOjk1MCxcImhlaWdodFwiOjU1MCxcInBhdGhzXCI6e1wiaWRcIjp7XCJwYXRoXCI6XCJNNzgxLjY4LDMyNC40bC0yLjMxLDguNjhsLTEyLjUzLDQuMjNsLTMuNzUtNC40bC0xLjgyLDAuNWwzLjQsMTMuMTJsNS4wOSwwLjU3bDYuNzksMi41N3YyLjU3bDMuMTEtMC41N2w0LjUzLTYuMjd2LTUuMTNsMi41NS01LjEzbDIuODMsMC41N2wtMy40LTcuMTNsLTAuNTItNC41OUw3ODEuNjgsMzI0LjRMNzgxLjY4LDMyNC40TTcyMi40OCwzMTcuNTdsLTAuMjgsMi4yOGw2Ljc5LDExLjQxaDEuOThsMTQuMTUsMjMuNjdsNS42NiwwLjU3bDIuODMtOC4yN2wtNC41My0yLjg1bC0wLjg1LTQuNTZMNzIyLjQ4LDMxNy41N0w3MjIuNDgsMzE3LjU3TTc4OS41MywzNDkuMTFsMi4yNiwyLjc3bC0xLjQ3LDQuMTZ2MC43OWgzLjM0bDEuMTgtMTAuNGwxLjA4LDAuM2wxLjk2LDkuNWwxLjg3LDAuNWwxLjc3LTQuMDZsLTEuNzctNi4xNGwtMS40Ny0yLjY3bDQuNjItMy4zN2wtMS4wOC0xLjQ5bC00LjQyLDIuODdoLTEuMThsLTIuMTYtMy4xN2wwLjY5LTEuMzlsMy42NC0xLjc4bDUuNSwxLjY4bDEuNjctMC4xbDQuMTMtMy44NmwtMS42Ny0xLjY4bC0zLjgzLDIuOTdoLTIuNDZsLTMuNzMtMS43OGwtMi42NSwwLjFsLTIuOTUsNC43NWwtMS44Nyw4LjIyTDc4OS41MywzNDkuMTFMNzg5LjUzLDM0OS4xMU04MTQuMTksMzMwLjVsLTEuODcsNC41NWwyLjk1LDMuODZoMC45OGwxLjI4LTIuNTdsMC42OS0wLjg5bC0xLjI4LTEuMzlsLTEuODctMC42OUw4MTQuMTksMzMwLjVMODE0LjE5LDMzMC41TTgxOS45OSwzNDUuNDVsLTQuMDMsMC44OWwtMS4xOCwxLjI5bDAuOTgsMS42OGwyLjY1LTAuOTlsMS42Ny0wLjk5bDIuNDYsMS45OGwxLjA4LTAuODlsLTEuOTYtMi4zOEw4MTkuOTksMzQ1LjQ1TDgxOS45OSwzNDUuNDVNNzUzLjE3LDM1OC4zMmwtMi43NSwxLjg4bDAuNTksMS41OGw4Ljc1LDEuOThsNC40MiwwLjc5bDEuODcsMS45OGw1LjAxLDAuNGwyLjM2LDEuOThsMi4xNi0wLjVsMS45Ny0xLjc4bC0zLjY0LTEuNjhsLTMuMTQtMi42N2wtOC4xNi0xLjk4TDc1My4xNywzNTguMzJMNzUzLjE3LDM1OC4zMk03ODEuNzcsMzY2LjkzbC0yLjE2LDEuMTlsMS4yOCwxLjM5bDMuMTQtMS4xOUw3ODEuNzcsMzY2LjkzTDc4MS43NywzNjYuOTNNNzg1LjUsMzY2LjA0bDAuMzksMS44OGwyLjI2LDAuNTlsMC44OC0xLjA5bC0wLjk4LTEuNDlMNzg1LjUsMzY2LjA0TDc4NS41LDM2Ni4wNE03OTAuOTEsMzcwLjk5bC0yLjc1LDAuNGwyLjQ2LDIuMDhoMS45Nkw3OTAuOTEsMzcwLjk5TDc5MC45MSwzNzAuOTlNNzkxLjY5LDM2Ny43MmwtMC41OSwxLjE5bDQuNDIsMC42OWwzLjQ0LTEuOThsLTEuOTYtMC41OWwtMy4xNCwwLjg5bC0xLjE4LTAuOTlMNzkxLjY5LDM2Ny43Mkw3OTEuNjksMzY3LjcyTTgzMS45MywzMzkuMzRsLTQuMTcsMC40N2wtMi42OCwxLjk2bDEuMTEsMi4yNGw0LjU0LDAuODR2MC44NGwtMi44NywyLjMzbDEuMzksNC44NWwxLjM5LDAuMDlsMS4yLTQuNzZoMi4yMmwwLjkzLDQuNjZsMTAuODMsOC45NmwwLjI4LDdsMy43LDQuMDFsMS42Ny0wLjA5bDAuMzctMjQuNzJsLTYuMjktNC4zOGwtNS45Myw0LjAxbC0yLjEzLDEuMzFsLTMuNTItMi4yNGwtMC4wOS03LjA5TDgzMS45MywzMzkuMzRMODMxLjkzLDMzOS4zNHpcIixcIm5hbWVcIjpcIkluZG9uZXNpYVwifSxcInBnXCI6e1wicGF0aFwiOlwiTTg1Mi43NiwzNDguMjlsLTAuMzcsMjQuNDRsMy41Mi0wLjE5bDQuNjMtNS40MWwzLjg5LDAuMTlsMi41LDIuMjRsMC44Myw2LjlsNy45Niw0LjJsMi4wNC0wLjc1di0yLjUybC02LjM5LTUuMzJsLTMuMTUtNy4yOGwyLjUtMS4yMWwtMS44NS00LjAxbC0zLjctMC4wOWwtMC45My00LjI5bC05LjgxLTYuNjJMODUyLjc2LDM0OC4yOUw4NTIuNzYsMzQ4LjI5TTg4MC40OCwzNDlsLTAuODgsMS4yNWw0LjgxLDQuMjZsMC42NiwyLjVsMS4zMS0wLjE1bDAuMTUtMi41N2wtMS40Ni0xLjMyTDg4MC40OCwzNDlMODgwLjQ4LDM0OU04ODIuODksMzU1LjAzbC0wLjk1LDAuMjJsLTAuNTgsMi41N2wtMS44MiwxLjE4bC01LjQ3LDAuOTZsMC4yMiwyLjA2bDUuNzYtMC4yOWwzLjY1LTIuMjhsLTAuMjItMy45N0w4ODIuODksMzU1LjAzTDg4Mi44OSwzNTUuMDNNODg5LjM4LDM1OS41MWwxLjI0LDMuNDVsMi4xOSwyLjEzbDAuNjYtMC41OWwtMC4yMi0yLjI4bC0yLjQ4LTMuMDFMODg5LjM4LDM1OS41MUw4ODkuMzgsMzU5LjUxelwiLFwibmFtZVwiOlwiUGFwdWEgTmV3IEd1aW5lYVwifSxcIm14XCI6e1wicGF0aFwiOlwiTTEzNy40OSwyMjUuNDNsNC44MywxNS4yMWwtMi4yNSwxLjI2bDAuMjUsMy4wMmw0LjI1LDMuMjd2Ni4wNWw1LjI1LDUuMDRsLTIuMjUtMTQuODZsLTMtOS44M2wwLjc1LTYuOGwyLjUsMC4yNWwxLDIuMjdsLTEsNS43OWwxMywyNS40NHY5LjA3bDEwLjUsMTIuMzRsMTEuNSw1LjI5bDQuNzUtMi43N2w2Ljc1LDUuNTRsNC00LjAzbC0xLjc1LTQuNTRsNS43NS0xLjc2bDEuNzUsMS4wMWwxLjc1LTEuNzZoMi43NWw1LTguODJsLTIuNS0yLjI3bC05Ljc1LDIuMjdsLTIuMjUsNi41NWwtNS43NSwxLjAxbC02Ljc1LTIuNzdsLTMtOS41N2wyLjI3LTEyLjA3bC00LjY0LTIuODlsLTIuMjEtMTEuNTlsLTEuODUtMC43OWwtMy4zOCwzLjQzbC0zLjg4LTIuMDdsLTEuNTItNy43M2wtMTUuMzctMS42MWwtNy45NC01Ljk3TDEzNy40OSwyMjUuNDNMMTM3LjQ5LDIyNS40M3pcIixcIm5hbWVcIjpcIk1leGljb1wifSxcImVlXCI6e1wicGF0aFwiOlwiTTUxNy43NywxNDMuNjZsLTUuNi0wLjJsLTMuNTUsMi4xN2wtMC4wNSwxLjYxbDIuMywyLjE3bDcuMTUsMS4yMUw1MTcuNzcsMTQzLjY2TDUxNy43NywxNDMuNjZNNTA2Ljc2LDE0Ny42NGwtMS41NS0wLjA1bC0wLjksMC45MWwwLjY1LDAuOTZsMS41NSwwLjFsMC44LTEuMTZMNTA2Ljc2LDE0Ny42NEw1MDYuNzYsMTQ3LjY0elwiLFwibmFtZVwiOlwiRXN0b25pYVwifSxcImR6XCI6e1wicGF0aFwiOlwiTTQ3My44OCwyMjcuNDlsLTQuMDgtMS4zN2wtMTYuOTgsMy4xOWwtMy43LDIuODFsMi4yNiwxMS42N2wtNi43NSwwLjI3bC00LjA2LDYuNTNsLTkuNjcsMi4zMmwwLjAzLDQuNzVsMzEuODUsMjQuMzVsNS40MywwLjQ2bDE4LjExLTE0LjE1bC0xLjgxLTIuMjhsLTMuNC0wLjQ2bC0yLjA0LTMuNDJ2LTE0LjE1bC0xLjM2LTEuMzdsMC4yMy0zLjY1bC0zLjYyLTMuNjVsLTAuNDUtMy44OGwxLjU4LTEuMTRsLTAuNjgtNC4xMUw0NzMuODgsMjI3LjQ5TDQ3My44OCwyMjcuNDl6XCIsXCJuYW1lXCI6XCJBbGdlcmlhXCJ9LFwibWFcIjp7XCJwYXRoXCI6XCJNNDQ4LjI5LDIzMi4yOGgtMTEuNTVsLTIuMjYsNS4wMmwtNS4yMSwyLjUxbC00LjMsMTEuNjRsLTguMzgsNS4wMmwtMTEuNzcsMTkuMzlsMTEuNTUtMC4yM2wwLjQ1LTUuN2gyLjk0di03Ljc2aDEwLjE5bDAuMjMtMTAuMDRsOS43NC0yLjI4bDQuMDgtNi42Mmw2LjM0LTAuMjNMNDQ4LjI5LDIzMi4yOEw0NDguMjksMjMyLjI4elwiLFwibmFtZVwiOlwiTW9yb2Njb1wifSxcIm1yXCI6e1wicGF0aFwiOlwiTTQwNC45LDI3Ni42NmwyLjE4LDIuODVsLTAuNDUsMTIuMzJsMy4xNy0yLjI4bDIuMjYtMC40NmwzLjE3LDEuMTRsMy42Miw1LjAybDMuNC0yLjI4bDE2LjUzLTAuMjNsLTQuMDgtMjcuNjFsNC4zOC0wLjAybC04LjE2LTYuMjVsMC4wMSw0LjA2bC0xMC4zMywwLjAxbC0wLjA1LDcuNzVsLTIuOTctMC4wMWwtMC4zOCw1LjcyTDQwNC45LDI3Ni42Nkw0MDQuOSwyNzYuNjZ6XCIsXCJuYW1lXCI6XCJNYXVyaXRhbmlhXCJ9LFwic25cIjp7XCJwYXRoXCI6XCJNNDEyLjAzLDI4OS44NEw0MTAuMTIsMjkwLjMxTDQwNi4xOCwyOTMuMThMNDA1LjI4LDI5NC43OEw0MDUsMjk2LjM3TDQwNi40MywyOTcuNDBMNDExLjI4LDI5Ny4zNEw0MTQuNDAsMjk2LjVMNDE0Ljc1LDI5OC4wM0w0MTQuNDYsMzAwLjA2TDQxNC41MywzMDAuMDlMNDA2Ljc4LDMwMC4yMUw0MDguMDMsMzAzLjIxTDQwOC43MSwzMDEuMzdMNDE4LDMwMi4xNUw0MTguMDYsMzAyLjIxTDQxOS4wMywzMDIuMjVMNDIyLDMwMi4zN0w0MjIuMTIsMzAwLjYyTDQxOC41MywyOTYuMzFMNDE0LjUzLDI5MC44N0w0MTIuMDMsMjg5Ljg0elwiLFwibmFtZVwiOlwiU2VuZWdhbFwifSxcImdtXCI6e1wicGF0aFwiOlwiTTQwNi44OSwyOTguMzRsLTAuMTMsMS4xMWw2LjkyLTAuMWwwLjM1LTEuMDNsLTAuMTUtMS4wNGwtMS45OSwwLjgxTDQwNi44OSwyOTguMzRMNDA2Ljg5LDI5OC4zNHpcIixcIm5hbWVcIjpcIkdhbWJpYVwifSxcImd3XCI6e1wicGF0aFwiOlwiTTQwOC42LDMwNC41M2wxLjQsMi43N2wzLjkzLTMuMzhsMC4wNC0xLjA0bC00LjYzLTAuNjdMNDA4LjYsMzA0LjUzTDQwOC42LDMwNC41M3pcIixcIm5hbWVcIjpcIkd1aW5lYS1CaXNzYXVcIn0sXCJnblwiOntcInBhdGhcIjpcIk00MTAuNDIsMzA3Ljk0bDMuMDQsNC42OGwzLjk2LTMuNDRsNC4wNi0wLjE4bDMuMzgsNC40OWwyLjg3LDEuODlsMS4wOC0yLjFsMC45Ni0wLjU0bC0wLjA3LTQuNjJsLTEuOTEtNS40OGwtNS44NiwwLjY1bC03LjI1LTAuNThsLTAuMDQsMS44Nkw0MTAuNDIsMzA3Ljk0TDQxMC40MiwzMDcuOTR6XCIsXCJuYW1lXCI6XCJHdWluZWFcIn0sXCJzbFwiOntcInBhdGhcIjpcIk00MTMuOTMsMzEzLjEzbDUuNjUsNS40Nmw0LjAzLTQuODlsLTIuNTItMy45NWwtMy40NywwLjM1TDQxMy45MywzMTMuMTNMNDEzLjkzLDMxMy4xM3pcIixcIm5hbWVcIjpcIlNpZXJyYSBMZW9uZVwifSxcImxyXCI6e1wicGF0aFwiOlwiTTQyMC4xNywzMTkuMTlsMTAuOTgsNy4zNGwtMC4yNi01LjU2bC0zLjMyLTMuOTFsLTMuMjQtMi44N0w0MjAuMTcsMzE5LjE5TDQyMC4xNywzMTkuMTl6XCIsXCJuYW1lXCI6XCJMaWJlcmlhXCJ9LFwiY2lcIjp7XCJwYXRoXCI6XCJNNDMyLjA3LDMyNi43NWw0LjI4LTMuMDNsNS4zMi0wLjkzbDUuNDMsMS4xN2wtMi43Ny00LjE5bC0wLjgxLTIuNTZsMC44MS03LjU3bC00Ljg1LDAuMjNsLTIuMi0yLjFsLTQuNjIsMC4xMmwtMi4yLDAuMzVsMC4yMyw1LjEybC0xLjE2LDAuNDdsLTEuMzksMi41NmwzLjU4LDQuMTlMNDMyLjA3LDMyNi43NUw0MzIuMDcsMzI2Ljc1elwiLFwibmFtZVwiOlwiQ290ZSBkJ0l2b2lyZVwifSxcIm1sXCI6e1wicGF0aFwiOlwiTTQxOS40NiwyOTUuODRsMy4wOC0yLjExbDE3LjEyLTAuMWwtMy45Ni0yNy41NGw0LjUyLTAuMTNsMjEuODcsMTYuNjlsMi45NCwwLjQybC0xLjExLDkuMjhsLTEzLjc1LDEuMjVsLTEwLjYxLDcuOTJsLTEuOTMsNS40MmwtNy4zNywwLjMxbC0xLjg4LTUuNDFsLTUuNjUsMC40bDAuMjItMS43N0w0MTkuNDYsMjk1Ljg0TDQxOS40NiwyOTUuODR6XCIsXCJuYW1lXCI6XCJNYWxpXCJ9LFwiYmZcIjp7XCJwYXRoXCI6XCJNNDUwLjU5LDI5NC4yOGwzLjY0LTAuMjlsNS45Nyw4LjQ0bC01LjU0LDQuMThsLTQuMDEtMS4wM2wtNS4zOSwwLjA3bC0wLjg3LDMuMTZsLTQuNTIsMC4yMmwtMS4yNC0xLjY5bDEuNi01LjE0TDQ1MC41OSwyOTQuMjhMNDUwLjU5LDI5NC4yOHpcIixcIm5hbWVcIjpcIkJ1cmtpbmEgRmFzb1wifSxcIm5lXCI6e1wicGF0aFwiOlwiTTQ2MC44OSwzMDJsMi41NS0wLjA2bDIuMy0zLjQ1bDMuODYtMC42OWw0LjExLDIuNTFsOC43NywwLjI1bDYuNzgtMi43NmwyLjU1LTIuMTlsMC4xOS0yLjg4bDQuNzMtNC43N2wxLjI1LTEwLjUzbC0zLjExLTYuNTJsLTcuOTYtMS45NGwtMTguNDIsMTQuMzZsLTIuNjEtMC4yNWwtMS4xMiw5Ljk3bC05LjQsMC45NEw0NjAuODksMzAyTDQ2MC44OSwzMDJ6XCIsXCJuYW1lXCI6XCJOaWdlclwifSxcImdoXCI6e1wicGF0aFwiOlwiTTQ0NC4zNCwzMTcuMDVsMS4xMiwyLjYzbDIuOTIsNC41OGwxLjYyLTAuMDZsNC40Mi0yLjUxbC0wLjMxLTE0LjI5bC0zLjQyLTFsLTQuNzksMC4xM0w0NDQuMzQsMzE3LjA1TDQ0NC4zNCwzMTcuMDV6XCIsXCJuYW1lXCI6XCJHaGFuYVwifSxcInRnXCI6e1wicGF0aFwiOlwiTTQ1NS4yMiwzMjEuMjVsMi42OC0xLjU3bC0wLjA2LTEwLjM1bC0xLjc0LTIuODJsLTEuMTIsMC45NEw0NTUuMjIsMzIxLjI1TDQ1NS4yMiwzMjEuMjV6XCIsXCJuYW1lXCI6XCJUb2dvXCJ9LFwiYmpcIjp7XCJwYXRoXCI6XCJNNDU4LjcxLDMxOS40OWgyLjEybDAuMTItNi4wMmwyLjY4LTMuODlsLTAuMTItNi43N2wtMi40My0wLjA2bC00LjE3LDMuMjZsMS43NCwzLjMyTDQ1OC43MSwzMTkuNDlMNDU4LjcxLDMxOS40OXpcIixcIm5hbWVcIjpcIkJlbmluXCJ9LFwibmdcIjp7XCJwYXRoXCI6XCJNNDYxLjU3LDMxOS4zN2wzLjkyLDAuMTlsNC43Myw1LjI3bDIuMywwLjYzbDEuOC0wLjg4bDIuNzQtMC4zOGwwLjkzLTMuODJsMy43My0yLjQ1bDQuMDQtMC4xOWw3LjQtMTMuNjFsLTAuMTItMy4wN2wtMy40Mi0yLjYzbC02Ljg0LDMuMDFsLTkuMTUtMC4xM2wtNC4zNi0yLjc2bC0zLjExLDAuNjlsLTEuNjIsMi44MmwtMC4xMiw3Ljk2bC0yLjYxLDMuN0w0NjEuNTcsMzE5LjM3TDQ2MS41NywzMTkuMzd6XCIsXCJuYW1lXCI6XCJOaWdlcmlhXCJ9LFwidG5cIjp7XCJwYXRoXCI6XCJNNDc0LjkxLDIyNy4zM2w1LjUzLTIuMjNsMS44MiwxLjE4bDAuMDcsMS40NGwtMC44NSwxLjExbDAuMTMsMS45N2wwLjg1LDAuNDZ2My41NGwtMC45OCwxLjY0bDAuMTMsMS4wNWwzLjcxLDEuMzFsLTIuOTksNC42NWwtMS4xNy0wLjA3bC0wLjIsMy43NGwtMS4zLDAuMmwtMS4xMS0wLjk4bDAuMjYtMy44bC0zLjY0LTMuNTRsLTAuNDYtMy4wOGwxLjc2LTEuMzhMNDc0LjkxLDIyNy4zM0w0NzQuOTEsMjI3LjMzelwiLFwibmFtZVwiOlwiVHVuaXNpYVwifSxcImx5XCI6e1wicGF0aFwiOlwiTTQ4MC4wNSwyNDguMDNsMS41Ni0wLjI2bDAuNDYtMy42aDAuNzhsMy4xOS01LjI0bDcuODcsMi4yOWwyLjE1LDMuMzRsNy43NCwzLjU0bDQuMDMtMS43bC0wLjM5LTEuN2wtMS43Ni0xLjdsMC4yLTEuMThsMi44Ni0yLjQyaDUuNjZsMi4xNSwyLjg4bDQuNTUsMC42NmwwLjU5LDM2Ljg5bC0zLjM4LTAuMTNsLTIwLjQyLTEwLjYybC0yLjIxLDEuMjVsLTguMzktMi4xbC0yLjI4LTMuMDFsLTMuMzItMC40NmwtMS42OS0zLjAxTDQ4MC4wNSwyNDguMDNMNDgwLjA1LDI0OC4wM3pcIixcIm5hbWVcIjpcIkxpYnlhXCJ9LFwiZWdcIjp7XCJwYXRoXCI6XCJNNTIxLjkzLDI0My4wNmwyLjY3LDAuMDdsNS4yLDEuNDRsMi40NywwLjA3bDMuMDYtMi41NmgxLjQzbDIuNiwxLjQ0aDMuMjlsMC41OS0wLjA0bDIuMDgsNS45OGwwLjU5LDEuOTNsMC41NSwyLjg5bC0wLjk4LDAuNzJsLTEuNjktMC44NWwtMS45NS02LjM2bC0xLjc2LTAuMTNsLTAuMTMsMi4xNmwxLjE3LDMuNzRsOS4zNywxMS42bDAuMiw0Ljk4bC0yLjczLDMuMTVMNTIyLjMyLDI3M0w1MjEuOTMsMjQzLjA2TDUyMS45MywyNDMuMDZ6XCIsXCJuYW1lXCI6XCJFZ3lwdFwifSxcInRkXCI6e1wicGF0aFwiOlwiTTQ5Mi43OSwyOTZsMC4xMy0yLjk1bDQuNzQtNC42MWwxLjI3LTExLjMybC0zLjE2LTYuMDRsMi4yMS0xLjEzbDIxLjQsMTEuMTVsLTAuMTMsMTAuOTRsLTMuNzcsMy4yMXY1LjY0bDIuNDcsNC43OGgtNC4zNmwtNy4yMiw3LjE0bC0wLjE5LDIuMTZsLTUuMzMtMC4wN2wtMC4wNywwLjk4bC0zLjA0LTAuNGwtMi4wOC0zLjkzbC0xLjU2LTAuNzdsMC4yLTEuMmwxLjk2LTEuNXYtNy4wMmwtMi43MS0wLjQybC0zLjI3LTIuNDNMNDkyLjc5LDI5Nkw0OTIuNzksMjk2TDQ5Mi43OSwyOTZ6XCIsXCJuYW1lXCI6XCJDaGFkXCJ9LFwic2RcIjp7XCJwYXRoXCI6XCJNNTIwLjE1LDI5Mi40M2wwLjE4LTExLjgzbDIuNDYsMC4wN2wtMC4yOC02LjU3bDI1LjgsMC4yM2wzLjY5LTMuNzJsNy45NiwxMi43M2wtNC4zNiw1LjE0djcuODVsLTYuODYsMTQuNzVsLTIuMzYsMS4wNGwwLjc1LDQuMTFoMi45NGwzLjk5LDUuNzlsLTMuMiwwLjQxbC0wLjgyLDEuNDlsLTAuMDgsMi4xNWwtOS42LTAuMTdsLTAuOTgtMS40OWwtNi43MS0wLjM4bC0xMi4zMi0xMi42OGwxLjIzLTAuNzRsMC4zMy0yLjk4bC0yLjk1LTEuNzRsLTIuNjktNS4zMWwwLjE1LTQuOTRMNTIwLjE1LDI5Mi40M0w1MjAuMTUsMjkyLjQzelwiLFwibmFtZVwiOlwiU3VkYW5cIn0sXCJjbVwiOntcInBhdGhcIjpcIk00NzcuODIsMzI0LjI4bDMuMjIsMi45NmwtMC4yMyw0LjU4bDE3LjY2LTAuNDFsMS40NC0xLjYybC01LjA2LTUuNDVsLTAuNzUtMS45N2wzLjIyLTYuMDNsLTIuMTktNGwtMS44NC0wLjk5di0yLjAzbDIuMTMtMS4zOWwwLjEyLTYuMzJsLTEuNjktMC4xOWwtMC4wMywzLjMybC03LjQyLDEzLjg1bC00LjU0LDAuMjNsLTMuMTEsMi4xNEw0NzcuODIsMzI0LjI4TDQ3Ny44MiwzMjQuMjh6XCIsXCJuYW1lXCI6XCJDYW1lcm9vblwifSxcImVyXCI6e1wicGF0aFwiOlwiTTU1Ni43MSwyOTQuN2wtMC4yNS01Ljg5bDMuOTYtNC42MmwxLjA3LDAuODJsMS45NSw2LjUybDkuMzYsNi45N2wtMS43LDIuMDlsLTYuODUtNS44OUg1NTYuNzFMNTU2LjcxLDI5NC43elwiLFwibmFtZVwiOlwiRXJpdHJlYVwifSxcImRqXCI6e1wicGF0aFwiOlwiTTU3MS40OCwzMDEuNTRsLTAuNTcsMy4zNmwzLjk2LTAuMDZsMC4wNi00Ljk0bC0xLjQ1LTAuODlMNTcxLjQ4LDMwMS41NEw1NzEuNDgsMzAxLjU0elwiLFwibmFtZVwiOlwiRGppYm91dGlcIn0sXCJldFwiOntcInBhdGhcIjpcIk01NDkuNDksMzExLjc2bDcuMjgtMTYuMmw3LjIzLDAuMDRsNi40MSw1LjU3bC0wLjQ1LDQuNTloNC45N2wwLjUxLDIuNzZsOC4wNCw0LjgxbDQuOTYsMC4yNWwtOS40MywxMC4xM2wtMTIuOTUsMy45OWgtMy4yMWwtNS43Mi00Ljg4bC0yLjI2LTAuOTVsLTQuMzgtNi40NWwtMi44OSwwLjA0bC0wLjM0LTIuOTZMNTQ5LjQ5LDMxMS43Nkw1NDkuNDksMzExLjc2elwiLFwibmFtZVwiOlwiRXRoaW9waWFcIn0sXCJzb1wiOntcInBhdGhcIjpcIk01NzUuNzQsMzA1LjA0bDQuMDgsMi43OGwxLjIxLTAuMDZsMTAuMTMtMy40OGwxLjE1LDMuNzFsLTAuODEsMy4xM2wtMi4xOSwxLjc0bC01LjQ3LTAuMzVsLTcuODMtNC44MUw1NzUuNzQsMzA1LjA0TDU3NS43NCwzMDUuMDRNNTkxLjk3LDMwNC4wNWw0LjM3LTEuNjhsMS41NSwwLjkzbC0wLjE3LDMuODhsLTQuMDMsMTEuNDhsLTIxLjgxLDIzLjM2bC0yLjUzLTEuNzRsLTAuMTctOS44NmwzLjI4LTMuNzdsNi45Ni0yLjE1bDEwLjIxLTEwLjc4bDIuNjctMi4zOGwwLjc1LTMuNDhMNTkxLjk3LDMwNC4wNUw1OTEuOTcsMzA0LjA1elwiLFwibmFtZVwiOlwiU29tYWxpYVwifSxcInllXCI6e1wicGF0aFwiOlwiTTU5OS42MiwyOTkuNjVsMi4xMywyLjM4bDIuODgtMS43NGwxLjA0LTAuMzVsLTEuMzItMS4yOGwtMi41MywwLjc1TDU5OS42MiwyOTkuNjVMNTk5LjYyLDI5OS42NU01NzEuOTksMjg5LjIzbDEuNDQsNC4yOHY0LjE4bDMuNDYsMy4xNGwyNC4zOC05LjkzbDAuMjMtMi43M2wtMy45MS03LjAybC05LjgxLDMuMTNsLTUuNjMsNS41NGwtNi41My0zLjg2TDU3MS45OSwyODkuMjNMNTcxLjk5LDI4OS4yM3pcIixcIm5hbWVcIjpcIlllbWVuXCJ9LFwiY2ZcIjp7XCJwYXRoXCI6XCJNNDk1LjY2LDMyNC4wNWw0LjY2LDUuMDRsMS44NC0yLjM4bDIuOTMsMC4xMmwwLjYzLTIuMzJsMi44OC0xLjhsNS45OCw0LjEybDMuNDUtMy40MmwxMy4zOSwwLjU5TDUxOSwzMTEuMThsMS42Ny0xLjA0bDAuMjMtMi4yNmwtMi44Mi0xLjMzaC00LjE0bC02LjY3LDYuNjFsLTAuMjMsMi43MmwtNS4yOS0wLjE3bC0wLjE3LDEuMTZsLTMuNDUtMC4zNWwtMy4xMSw1LjkxTDQ5NS42NiwzMjQuMDVMNDk1LjY2LDMyNC4wNXpcIixcIm5hbWVcIjpcIkNlbnRyYWwgQWZyaWNhbiBSZXB1YmxpY1wifSxcInN0XCI6e1wicGF0aFwiOlwiTTQ3MC43NCwzMzcuMTVsMS4xNS0wLjU4bDAuODYsMC43bC0wLjg2LDEuMzNsLTEuMDQtMC40MUw0NzAuNzQsMzM3LjE1TDQ3MC43NCwzMzcuMTVNNDczLjA1LDMzMy41bDEuNzMtMC4yOWwwLjU4LDEuMWwtMC44NiwwLjkzbC0wLjg2LTAuMTJMNDczLjA1LDMzMy41TDQ3My4wNSwzMzMuNXpcIixcIm5hbWVcIjpcIlNhbyBUb21lIGFuZCBQcmluY2lwZVwifSxcImdxXCI6e1wicGF0aFwiOlwiTTQ3Ni44NCwzMjcuNDFsLTAuNDYsMS45N2wxLjM4LDAuNzVsMS4zMi0wLjk5bC0wLjQ2LTIuMDNMNDc2Ljg0LDMyNy40MUw0NzYuODQsMzI3LjQxTTQ4MC45OSwzMzIuNjlsLTAuMDYsMS4zOWw0LjU0LDAuMjNsLTAuMDYtMS41N0w0ODAuOTksMzMyLjY5TDQ4MC45OSwzMzIuNjl6XCIsXCJuYW1lXCI6XCJFcXVhdG9yaWFsIEd1aW5lYVwifSxcImdhXCI6e1wicGF0aFwiOlwiTTQ4Ni4zOSwzMzIuNjNsLTAuMTIsMi40OWwtNS42NC0wLjEybC0zLjQ1LDYuNjdsOC4xMSw4Ljg3bDIuMDEtMS42OGwtMC4wNi0xLjc0bC0xLjM4LTAuNjR2LTEuMjJsMy4xMS0xLjk3bDIuNzYsMi4wOWwzLjA1LDAuMDZsLTAuMDYtMTAuNDlsLTQuODMtMC4yM2wtMC4wNi0yLjJMNDg2LjM5LDMzMi42M0w0ODYuMzksMzMyLjYzelwiLFwibmFtZVwiOlwiR2Fib25cIn0sXCJjZ1wiOntcInBhdGhcIjpcIk00OTEsMzMyLjUybC0wLjA2LDEuNDVsNC43OCwwLjEybDAuMTcsMTIuNDFsLTQuMzctMC4xMmwtMi41My0xLjk3bC0xLjk2LDEuMWwtMC4wOSwwLjU1bDEuMDEsMC40OWwwLjI5LDIuNTVsLTIuNywyLjMybDAuNTgsMS4yMmwyLjk5LTIuMzJoMS40NGwwLjQ2LDEuMzlsMS45LDAuODFsNi4xLTUuMTZsLTAuMTItMy43N2wxLjI3LTMuMDdsMy45MS0yLjlsMS4wNS05LjgxbC0yLjc4LDAuMDFsLTMuMjIsNC40MUw0OTEsMzMyLjUyTDQ5MSwzMzIuNTJ6XCIsXCJuYW1lXCI6XCJDb25nb1wifSxcImFvXCI6e1wicGF0aFwiOlwiTTQ4Ni41NSwzNTMuMjNsMS43NCwyLjI2bDIuMjUtMi4xM2wtMC42Ni0yLjIxbC0wLjU2LTAuMDRMNDg2LjU1LDM1My4yM0w0ODYuNTUsMzUzLjIzTTQ4OC42MiwzNTYuNzFsMy40MSwxMi43M2wtMC4wOCw0LjAybC00Ljk5LDUuMzZsLTAuNzUsOC43MWwxOS4yLDAuMTdsNi4yNCwyLjI2bDUuMTUtMC42N2wtMy0zLjc2bDAuMDEtMTAuNzRsNS45LTAuMjV2LTQuMTlsLTQuNzktMC4ybC0wLjk2LTkuOTJsLTIuMDIsMC4wM2wtMS4wOS0wLjk4bC0xLjE5LDAuMDZsLTEuNTgsMy4wNkg1MDJsLTEuNDEtMS40MmwwLjQyLTIuMDFsLTEuNjYtMi40M0w0ODguNjIsMzU2LjcxTDQ4OC42MiwzNTYuNzF6XCIsXCJuYW1lXCI6XCJBbmdvbGFcIn0sXCJjZFwiOntcInBhdGhcIjpcIk00ODkuMzgsMzU1LjcxbDEwLjMxLTAuMThsMi4wOSwyLjk3bC0wLjA4LDIuMTlsMC43NywwLjdoNS4xMmwxLjQ3LTIuODloMi4wOWwwLjg1LDAuODZsMi44Ny0wLjA4bDAuODUsMTAuMDhsNC45NiwwLjE2djAuNzhsMTMuMzMsNi4wMWwwLjYyLDEuMTdoMi43OWwtMC4zMS00LjIybC01LjA0LTIuNDJsMC4zMS0zLjJsMi4xNy01LjA4bDQuOTYtMC4xNmwtNC4yNi0xNC4xNGwwLjA4LTYuMDFsNi43NC0xMC41NGwwLjA4LTEuNDhsLTEuMDEtMC41NWwwLjA0LTIuODZsLTEuMjMtMC4xMWwtMS4yNC0xLjU4bC0yMC4zNS0wLjkybC0zLjczLDMuNjNsLTYuMTEtNC4wMmwtMi4xNSwxLjMybC0xLjU2LDEzLjEzbC0zLjg2LDIuOThsLTEuMTYsMi42NGwwLjIxLDMuOTFsLTYuOTYsNS42OWwtMS44NS0wLjg0bDAuMjUsMS4wOUw0ODkuMzgsMzU1LjcxTDQ4OS4zOCwzNTUuNzF6XCIsXCJuYW1lXCI6XCJDb25nb1wifSxcInJ3XCI6e1wicGF0aFwiOlwiTTUzNy44MiwzMzkuOWwyLjgxLDIuNTlsLTAuMTIsMi43N2wtNC4zNiwwLjA5di0zLjA2TDUzNy44MiwzMzkuOUw1MzcuODIsMzM5Ljl6XCIsXCJuYW1lXCI6XCJSd2FuZGFcIn0sXCJiaVwiOntcInBhdGhcIjpcIk01MzYuMjEsMzQ2LjIxbDQuMjctMC4wOWwtMS4xMSwzLjc0bC0xLjA4LDAuOTRoLTEuMzJsLTAuOTQtMi41M0w1MzYuMjEsMzQ2LjIxTDUzNi4yMSwzNDYuMjF6XCIsXCJuYW1lXCI6XCJCdXJ1bmRpXCJ9LFwidWdcIjp7XCJwYXRoXCI6XCJNNTM4LjMsMzM5LjA5bDMuMDMsMi44NGwxLjktMS4yMWw1LjE0LTAuODRsMC44OCwwLjA5bDAuMzMtMS45NWwyLjktNi4xbC0yLjQ0LTUuMDhsLTcuOTEsMC4wNWwtMC4wNSwyLjA5bDEuMDYsMS4wMmwtMC4xNiwyLjA5TDUzOC4zLDMzOS4wOUw1MzguMywzMzkuMDl6XCIsXCJuYW1lXCI6XCJVZ2FuZGFcIn0sXCJrZVwiOntcInBhdGhcIjpcIk01NTAuODMsMzI2LjUybDIuNjYsNS4xOWwtMy4xOSw2LjY5bC0wLjQyLDIuMDNsMTUuOTMsOS44NWw0Ljk0LTcuNzZsLTIuNS0yLjAzbC0wLjA1LTEwLjIybDMuMTMtMy40MmwtNC45OSwxLjY2bC0zLjc3LDAuMDVsLTUuOS00Ljk4bC0xLjg2LTAuOGwtMy40NSwwLjMybC0wLjYxLDEuMDJMNTUwLjgzLDMyNi41Mkw1NTAuODMsMzI2LjUyelwiLFwibmFtZVwiOlwiS2VueWFcIn0sXCJ0elwiOntcInBhdGhcIjpcIk01NTAuNTcsMzcxLjQybDE3LjQ3LTIuMTRsLTMuOTMtNy42bC0wLjIxLTcuMjhsMS4yNy0zLjQ4bC0xNi42Mi0xMC40NGwtNS4yMSwwLjg2bC0xLjgxLDEuMzRsLTAuMTYsMy4wNWwtMS4xNyw0LjIzbC0xLjIyLDEuNDVsLTEuNzUsMC4xNmwzLjM1LDExLjYxbDUuNDcsMi41N2wzLjc3LDAuMTFMNTUwLjU3LDM3MS40Mkw1NTAuNTcsMzcxLjQyelwiLFwibmFtZVwiOlwiVGFuemFuaWFcIn0sXCJ6bVwiOntcInBhdGhcIjpcIk01MTQuNTUsMzg0LjdsMy4xNyw0LjRsNC45MSwwLjNsMS43NCwwLjk2bDUuMTQsMC4wNmw0LjQzLTYuMjFsMTIuMzgtNS41NGwxLjA4LTQuODhsLTEuNDQtNi45OWwtNi40Ni0zLjY4bC00LjMxLDAuM2wtMi4xNSw0Ljc2bDAuMDYsMi4xN2w1LjA4LDIuNDdsMC4zLDUuMzdsLTQuMzcsMC4yNGwtMS4wOC0xLjgxbC0xMi4xNC01LjE4bC0wLjM2LDMuOThsLTUuNzQsMC4xOEw1MTQuNTUsMzg0LjdMNTE0LjU1LDM4NC43elwiLFwibmFtZVwiOlwiWmFtYmlhXCJ9LFwibXdcIjp7XCJwYXRoXCI6XCJNNTQ3LjE2LDM3OS40bDMuMTEsMy4yNWwtMC4wNiw0LjE2bDAuNiwxLjc1bDQuMTMtNC40NmwtMC40OC01LjY3bC0yLjIxLTEuNjlsLTEuOTctOS45NWwtMy40MS0wLjEybDEuNTUsNy4xN0w1NDcuMTYsMzc5LjRMNTQ3LjE2LDM3OS40elwiLFwibmFtZVwiOlwiTWFsYXdpXCJ9LFwibXpcIjp7XCJwYXRoXCI6XCJNNTQxLjE3LDQxMy4yOGwyLjY5LDIuMjNsNi4zNC0zLjg2bDEuMDItNS43M3YtOS40NmwxMC4xNy04LjMybDEuNzQsMC4wNmw2LjE2LTUuOTFsLTAuOTYtMTIuMThMNTUyLDM3Mi4xN2wwLjQ4LDMuNjhsMi44MSwyLjE3bDAuNjYsNi42M2wtNS41LDUuMzdsLTEuMzItMy4wMWwwLjI0LTMuOThsLTMuMTctMy40NGwtNy43OCwzLjYybDcuMjQsMy42OGwwLjI0LDEwLjczbC00Ljc5LDcuMTFMNTQxLjE3LDQxMy4yOEw1NDEuMTcsNDEzLjI4elwiLFwibmFtZVwiOlwiTW96YW1iaXF1ZVwifSxcInp3XCI6e1wicGF0aFwiOlwiTTUyNC42NiwzOTIuM2w4Ljk3LDEwLjEzbDYuODgsMS43NWw0LjYxLTcuMjNsLTAuMzYtOS41OGwtNy40OC0zLjg2bC0yLjgxLDEuMjdsLTQuMTksNi4zOWwtNS44LTAuMDZMNTI0LjY2LDM5Mi4zTDUyNC42NiwzOTIuM3pcIixcIm5hbWVcIjpcIlppbWJhYndlXCJ9LFwibmFcIjp7XCJwYXRoXCI6XCJNNDk2LjU1LDQyMS45NmwzLjM1LDAuMjRsMS45NywxLjk5bDQuNjcsMC4wNmwxLjE0LTEzLjI2di04LjY4bDIuOTktMC42bDEuMTQtOS4xbDcuNi0wLjI0bDIuNjktMi4yM2wtNC41NS0wLjE4bC02LjE2LDAuODRsLTYuNjQtMi40MWgtMTguNjZsMC40OCw1LjNsNi4yMiw5LjE2bC0xLjA4LDQuN2wwLjA2LDIuNDdMNDk2LjU1LDQyMS45Nkw0OTYuNTUsNDIxLjk2elwiLFwibmFtZVwiOlwiTmFtaWJpYVwifSxcImJ3XCI6e1wicGF0aFwiOlwiTTUwOC41MSw0MTEuMjNsMi4xNSwwLjY2bC0wLjMsNi4xNWwyLjIxLDAuM2w1LjA4LTQuNThsNi4xLDAuNjZsMS42Mi00LjFsNy43Mi03LjA1bC05LjI3LTEwLjY3bC0wLjEyLTEuNzVsLTEuMDItMC4zbC0yLjgxLDIuNTlsLTcuMywwLjE4bC0xLjAyLDkuMWwtMi44NywwLjY2TDUwOC41MSw0MTEuMjNMNTA4LjUxLDQxMS4yM3pcIixcIm5hbWVcIjpcIkJvdHN3YW5hXCJ9LFwic3pcIjp7XCJwYXRoXCI6XCJNNTQwLjg3LDQxNGwtMi41MSwwLjQybC0xLjA4LDIuOTVsMS45MiwxLjc1aDIuMzNsMS45Ny0yLjgzTDU0MC44Nyw0MTRMNTQwLjg3LDQxNHpcIixcIm5hbWVcIjpcIlN3YXppbGFuZFwifSxcImxzXCI6e1wicGF0aFwiOlwiTTUyNy40MSw0MjUuMzlsMy4wNS0yLjM1bDEuNDQsMC4wNmwxLjc0LDIuMTdsLTAuMTgsMi4xN2wtMi45MywxLjA4djAuODRsLTMuMjMtMC4xOGwtMC43OC0yLjM1TDUyNy40MSw0MjUuMzlMNTI3LjQxLDQyNS4zOXpcIixcIm5hbWVcIjpcIkxlc290aG9cIn0sXCJ6YVwiOntcInBhdGhcIjpcIk01MzQuMTYsNDAzLjYzbC03LjksNy4zbC0xLjg4LDQuNTFsLTYuMjYtMC43OGwtNS4yMSw0LjYzbC0zLjQ2LTAuMzRsMC4yOC02LjRsLTEuMjMtMC40M2wtMC44NiwxMy4wOWwtNi4xNC0wLjA2bC0xLjg1LTIuMThsLTIuNzEtMC4wM2wyLjQ3LDcuMDlsNC40MSw0LjE3bC0zLjE1LDMuNjdsMi4wNCw0LjZsNC43MiwxLjhsMy43Ni0zLjJsMTAuNzcsMC4wNmwwLjc3LTAuOTZsNC43OC0wLjg0bDE2LjE3LTE2LjFsLTAuMDYtNS4wN2wtMS43MywyLjI0aC0yLjU5bC0zLjE1LTIuNjRsMS42LTMuOThsMi43NS0wLjU2bC0wLjI1LTguMThMNTM0LjE2LDQwMy42M0w1MzQuMTYsNDAzLjYzeiBNNTMwLjM3LDQyMi4xM2wxLjUxLTAuMDZsMi40NSwyLjY2bC0wLjA3LDMuMDhsLTIuODcsMS40NWwtMC4xOCwxLjAybC00LjM4LDAuMDVsLTEuMzctMy4zbDEuMjUtMi40Mkw1MzAuMzcsNDIyLjEzTDUzMC4zNyw0MjIuMTN6XCIsXCJuYW1lXCI6XCJTb3V0aCBBZnJpY2FcIn0sXCJnbFwiOntcInBhdGhcIjpcIk0zMjEuMTMsNTAuMDdsLTEuMzYsMi4xN2wyLjQ1LDIuNDVsLTEuMDksMi40NWwzLjU0LDQuNjJsNC4zNS0xLjM2bDUuNzEtMC41NGw2LjUzLDcuMDdsNC4zNSwxMS42OWwtMy41Myw3LjM0bDQuODktMC44MmwyLjcyLDEuNjNsMC4yNywzLjU0bC01Ljk4LDAuMjdsMy4yNiwzLjI2bDQuMDgsMC44MmwtOC45NywxMS45NmwtMS4wOSw3LjM0bDEuOSw1Ljk4bC0xLjM2LDMuNTRsMi40NSw3LjYxbDQuNjIsNS4xN2wxLjM2LTAuMjdsMi45OS0wLjgybDAuMjcsNC4zNWwxLjksMi43MmwzLjUzLTAuMjdsMi43Mi0xMC4wNmw4LjE2LTEwLjA2bDEyLjI0LTQuODlsNy42MS05LjUybDMuNTMsMS42M2g3LjM0bDUuOTgtNS45OGw3LjM0LTIuOTlsMC44Mi00LjYybC00LjYyLTQuMDhsLTQuMDgtMS4zNmwtMi4xOC01LjcxbDUuMTctMi45OWw4LjE2LDQuMzVsMi43Mi0yLjk5bC00LjM1LTIuNDVsOS4yNS0xMi41MWwtMS42My01LjQ0bC00LjM1LTAuMjdsMS42My00Ljg5bDUuNDQtMi40NWwxMS4xNS05Ljc5bC0zLjI2LTMuNTNsLTEyLjUxLDEuMDlsLTYuNTMsNi41M2wzLjgxLTguNDNsLTQuMzUtMS4wOWwtMi40NSw0LjM1bC0zLjUzLTIuOTlsLTkuNzksMS4wOWwyLjcyLTQuMzVsMTYuMDQtMC41NGwtNC4wOC01LjQ0bC0xNy40LTMuMjZsLTcuMDcsMS4wOWwwLjI3LDMuNTRsLTcuMzQtMi40NWwwLjI3LTIuNDVsLTUuMTcsMS4wOWwtMS4wOSwyLjcybDUuNDQsMS45bC01LjcxLDQuMDhsLTQuMDgtNC42MmwtNS43MS0xLjYzbC0wLjgyLDQuMzVoLTUuNzFsLTIuMTgtNC42MmwtOC45Ny0xLjM2bC00Ljg5LDIuNDVsLTAuMjcsMy4yNmwtNi4yNS0wLjgybC0zLjgxLDEuNjNsMC4yNywzLjgxdjEuOWwtNy4wNywxLjM2bC0zLjI2LTIuMTdsLTIuMTgsMy41M2wzLjI2LDMuNTRsNi44LTAuODJsMC41NCwyLjE4bC01LjE3LDIuNDVMMzIxLjEzLDUwLjA3TDMyMS4xMyw1MC4wN00zNDIuODksOTIuNDlsMS42MywyLjQ1bC0wLjgyLDIuOTloLTEuNjNsLTIuMTgtMi40NWwwLjU0LTEuOUwzNDIuODksOTIuNDlMMzQyLjg5LDkyLjQ5TTQxMC44Nyw4NS42OWw0LjYyLDEuMzZsLTAuMjcsMy44MWwtNC44OS0yLjQ1bC0xLjA5LTEuMzZMNDEwLjg3LDg1LjY5TDQxMC44Nyw4NS42OXpcIixcIm5hbWVcIjpcIkdyZWVubGFuZFwifSxcImF1XCI6e1wicGF0aFwiOlwiTTc2MS4xNyw0MjcuOThsLTAuMzUsMjUuMzhsLTMuOSwyLjg2bC0wLjM1LDIuNWw1LjMyLDMuNTdsMTMuMTMtMi41aDYuNzRsMi40OC0zLjU4bDE0LjktMi44NmwxMC42NCwzLjIybC0wLjcxLDQuMjlsMS40Miw0LjI5bDguMTYtMS40M2wwLjM1LDIuMTRsLTUuMzIsMy45M2wxLjc3LDEuNDNsMy45LTEuNDNsLTEuMDYsMTEuOGw3LjQ1LDUuNzJsNC4yNi0xLjQzbDIuMTMsMi4xNGwxMi40Mi0xLjc5bDExLjcxLTE4Ljk1bDQuMjYtMS4wN2w4LjUxLTE1LjczbDIuMTMtMTMuNThsLTUuMzItNi43OWwyLjEzLTEuNDNsLTQuMjYtMTMuMjNsLTQuNjEtMy4yMmwwLjcxLTE3Ljg3bC00LjI2LTMuMjJsLTEuMDYtMTAuMDFoLTIuMTNsLTcuMSwyMy41OWwtMy45LDAuMzZsLTguODctOC45NGw0Ljk3LTEzLjIzbC05LjIyLTEuNzlsLTEwLjI5LDIuODZsLTIuODQsOC4yMmwtNC42MSwxLjA3bC0wLjM1LTUuNzJsLTE4LjgsMTEuNDRsMC4zNSw0LjI5bC0yLjg0LDMuOTNoLTcuMWwtMTUuMjYsNi40M0w3NjEuMTcsNDI3Ljk4TDc2MS4xNyw0MjcuOThNODI1Ljc0LDQ5Ni4yNmwtMS43Nyw3LjE1bDAuMzUsNWw1LjMyLTAuMzZsNi4wMy05LjI5TDgyNS43NCw0OTYuMjZMODI1Ljc0LDQ5Ni4yNnpcIixcIm5hbWVcIjpcIkF1c3RyYWxpYVwifSxcIm56XCI6e1wicGF0aFwiOlwiTTkxMy4wMiw0ODEuOTZsMS4wNiwxMS44bC0xLjQyLDUuMzZsLTUuMzIsMy45M2wwLjM1LDQuNjV2NWwxLjQyLDEuNzlsMTQuNTUtMTIuNTF2LTIuODZoLTMuNTVsLTQuOTctMTYuOEw5MTMuMDIsNDgxLjk2TDkxMy4wMiw0ODEuOTZNOTAyLjM4LDUwNy43bDIuODQsNS4zNmwtNy44MSw3LjUxbC0wLjcxLDMuOTNsLTUuMzIsMC43MWwtOC44Nyw4LjIybC04LjE2LTMuOTNsLTAuNzEtMi44NmwxNC45LTYuNDNMOTAyLjM4LDUwNy43TDkwMi4zOCw1MDcuN3pcIixcIm5hbWVcIjpcIk5ldyBaZWFsYW5kXCJ9LFwibmNcIjp7XCJwYXRoXCI6XCJNOTA2LjY0LDQyMC40N2wtMC4zNSwxLjc5bDQuNjEsNi40M2wyLjQ4LDEuMDdsMC4zNS0yLjVMOTA2LjY0LDQyMC40N0w5MDYuNjQsNDIwLjQ3elwiLFwibmFtZVwiOlwiTmV3IENhbGVkb25pYVwifSxcIm15XCI6e1wicGF0aFwiOlwiTTc2NC4xNCwzMzIuOTJsMy4wMiwzLjQ5bDExLjU4LTQuMDFsMi4yOS04Ljg0bDUuMTYtMC4zN2w0LjcyLTMuNDJsLTYuMTItNC40NmwtMS40LTIuNDVsLTMuMDIsNS41N2wxLjExLDMuMmwtMS44NCwyLjY3bC0zLjQ3LTAuODlsLTguNDEsNi4xN2wwLjIyLDMuNTdMNzY0LjE0LDMzMi45Mkw3NjQuMTQsMzMyLjkyTTczMi43MSwzMTUuNDVsMi4wMSw0LjUxbDAuNDUsNS44NmwyLjY5LDQuMTdsNi40OSwzLjk0bDIuNDYsMC4yM2wtMC40NS00LjA2bC0yLjEzLTUuMThsLTMuMTItNi42M2wtMC4yNiwxLjE2bC0zLjc2LTAuMTdsLTIuNy0zLjg4TDczMi43MSwzMTUuNDVMNzMyLjcxLDMxNS40NXpcIixcIm5hbWVcIjpcIk1hbGF5c2lhXCJ9LFwiYm5cIjp7XCJwYXRoXCI6XCJNNzc5Ljc3LDMxOS4yNWwtMi44OCwzLjQ5bDIuMzYsMC43NGwxLjMzLTEuODZMNzc5Ljc3LDMxOS4yNUw3NzkuNzcsMzE5LjI1elwiLFwibmFtZVwiOlwiQnJ1bmVpIERhcnVzc2FsYW1cIn0sXCJ0bFwiOntcInBhdGhcIjpcIk04MDYuMTQsMzY4LjQybC01LjExLDQuMjZsMC40OSwxLjA5bDIuMTYtMC40bDIuNTUtMi4zOGw1LjAxLTAuNjlsLTAuOTgtMS42OEw4MDYuMTQsMzY4LjQyTDgwNi4xNCwzNjguNDJ6XCIsXCJuYW1lXCI6XCJUaW1vci1MZXN0ZVwifSxcInNiXCI6e1wicGF0aFwiOlwiTTg5NS40MywzNjQuNjVsMC4xNSwyLjI4bDEuMzksMS4zMmwxLjMxLTAuODFsLTEuMTctMi40M0w4OTUuNDMsMzY0LjY1TDg5NS40MywzNjQuNjVNODk3LjE4LDM3MC4zMWwtMS4xNywxLjI1bDEuMjQsMi4yOGwxLjQ2LDAuNDRsLTAuMDctMS41NEw4OTcuMTgsMzcwLjMxTDg5Ny4xOCwzNzAuMzFNOTAwLjAzLDM2OC45OWwxLjAyLDIuNWwxLjk3LDIuMzVsMS4wOS0xLjc2bC0xLjQ2LTIuNUw5MDAuMDMsMzY4Ljk5TDkwMC4wMywzNjguOTlNOTA1LjE0LDM3Mi43NGwwLjU4LDMuMDlsMS4zOSwxLjkxbDEuMTctMi40Mkw5MDUuMTQsMzcyLjc0TDkwNS4xNCwzNzIuNzRNOTA2Ljc0LDM3OS42NWwtMC41MSwwLjg4bDEuNjgsMi4yMWwxLjE3LDAuMDdsLTAuNzMtMi44N0w5MDYuNzQsMzc5LjY1TDkwNi43NCwzNzkuNjVNOTAzLjAyLDM4NC4wNWwtMS43NSwwLjgxbDEuNTMsMi4xM2wxLjMxLTAuNzRMOTAzLjAyLDM4NC4wNUw5MDMuMDIsMzg0LjA1elwiLFwibmFtZVwiOlwiU29sb21vbiBJc2xhbmRzXCJ9LFwidnVcIjp7XCJwYXRoXCI6XCJNOTIwLjg3LDM5Ny4yMmwtMS4yNCwxLjY2bDAuNTIsMS44N2wwLjYyLDAuNDJsMS4xMy0xLjQ2TDkyMC44NywzOTcuMjJMOTIwLjg3LDM5Ny4yMk05MjEuNDksNDAyLjMxbDAuMSwxLjM1bDEuMzQsMC40MmwwLjkzLTAuNTJsLTAuOTMtMS40Nkw5MjEuNDksNDAyLjMxTDkyMS40OSw0MDIuMzFNOTIzLjQ1LDQxNC4zN2wtMC42MiwwLjk0bDAuOTMsMS4wNGwxLjU1LTAuNTJMOTIzLjQ1LDQxNC4zN0w5MjMuNDUsNDE0LjM3elwiLFwibmFtZVwiOlwiVmFudWF0dVwifSxcImZqXCI6e1wicGF0aFwiOlwiTTk0OC42Miw0MTIuMjlsLTEuMjQsMS42NmwtMC4xLDEuODdsMS40NCwxLjQ2TDk0OC42Miw0MTIuMjlMOTQ4LjYyLDQxMi4yOXpcIixcIm5hbWVcIjpcIkZpamlcIn0sXCJwaFwiOntcInBhdGhcIjpcIk03ODkuMzcsMjk3LjUzbC0wLjg2LDEuNjRsLTAuNDgsMi4wMmwtNC43OCw2LjA3bDAuMjksMS4yNWwyLjAxLTAuMjlsNi4yMS02Ljk0TDc4OS4zNywyOTcuNTNMNzg5LjM3LDI5Ny41M003OTcuMTEsMjk1LjIybC0wLjEsNS4wMWwxLjgyLDEuODNsMC42NywzLjU2bDEuODIsMC4zOWwwLjg2LTIuMjJsLTEuNDMtMS4wNmwtMC4zOC02LjI2TDc5Ny4xMSwyOTUuMjJMNzk3LjExLDI5NS4yMk04MDIuMjgsMjk3LjE1bC0wLjEsNC40M2wxLjA1LDEuNzNsMS44Mi0yLjEybC0wLjQ4LTMuODVMODAyLjI4LDI5Ny4xNUw4MDIuMjgsMjk3LjE1TTgwMy40MiwyOTMuMjlsMS44MiwyLjQxbDAuODYsMi4zMWgxLjYzbC0wLjI5LTMuOTVsLTEuODItMS4yNUw4MDMuNDIsMjkzLjI5TDgwMy40MiwyOTMuMjlNODA2Ljk2LDMwMi4zNWwwLjM4LDIuODlsLTMuMzUsMi43bC0yLjc3LDAuMjlsLTIuOTYsMy4xOGwwLjEsMS40NWwyLjc3LTAuODdsMS45MS0xLjI1bDEuNjMsNC4xNGwyLjg3LDIuMDJsMS4xNS0wLjM5bDEuMDUtMS4yNWwtMi4yOS0yLjMxbDEuMzQtMS4wNmwxLjUzLDEuMjVsMS4wNS0xLjczbC0xLjA1LTIuMTJsLTAuMTktNC43Mkw4MDYuOTYsMzAyLjM1TDgwNi45NiwzMDIuMzVNNzkxLjM4LDI3Mi45N2wtMi41OCwxLjgzbC0wLjI5LDUuNzhsNC4wMiw3LjhsMS4zNCwxLjA2bDEuNzItMS4xNmwyLjk2LDAuNDhsMC41NywyLjZsMi4yLDAuMTlsMS4wNS0xLjQ0bC0xLjM0LTEuODNsLTEuNjMtMS41NGwtMy40NC0wLjM4bC0xLjgyLTIuOTlsMi4xLTMuMThsMC4xOS0yLjc5bC0xLjQzLTMuNTZMNzkxLjM4LDI3Mi45N0w3OTEuMzgsMjcyLjk3TTc5Mi43MiwyOTAuMjFsMC43NiwyLjdsMS4zNCwwLjg3bDAuOTYtMS4yNWwtMS41My0yLjEyTDc5Mi43MiwyOTAuMjFMNzkyLjcyLDI5MC4yMXpcIixcIm5hbWVcIjpcIlBoaWxpcHBpbmVzXCJ9LFwiY25cIjp7XCJwYXRoXCI6XCJNNzU5LjgzLDI3MC4xN2wtMi4zOSwwLjY3bC0xLjcyLDIuMTJsMS40MywyLjc5bDIuMSwwLjE5bDIuMzktMi4xMmwwLjU3LTIuNzlMNzU5LjgzLDI3MC4xN0w3NTkuODMsMjcwLjE3TTY3MC40LDE3MC4wN2wtMy40Niw4LjdsLTQuNzctMC4yNWwtNS4wMywxMS4wMWw0LjI3LDUuNDRsLTguOCwxMi4xNWwtNC41Mi0wLjc2bC0zLjAyLDMuOGwwLjc1LDIuMjhsMy41MiwwLjI1bDEuNzYsNC4wNWwzLjUyLDAuNzZsMTAuODEsMTMuOTN2Ny4wOWw1LjI4LDMuMjlsNS43OC0xLjAxbDcuMjksNC4zbDguOCwyLjUzbDQuMjctMC41MWw0Ljc4LTAuNTFsMTAuMDUtNi41OGwzLjI3LDAuNTFsMS4yNSwyLjk3bDIuNzcsMC44M2wzLjc3LDUuNTdsLTIuNTEsNS41N2wxLjUxLDMuOGw0LjI3LDEuNTJsMC43NSw0LjU2bDUuMDMsMC41MWwwLjc1LTIuMjhsNy4yOS0zLjhsNC41MiwwLjI1bDUuMjgsNS44MmwzLjUyLTEuNTJsMi4yNiwwLjI1bDEuMDEsMi43OWwxLjc2LDAuMjVsMi41MS0zLjU0bDEwLjA1LTMuOGw5LjA1LTEwLjg5bDMuMDItMTAuMzhsLTAuMjUtNi44NGwtMy43Ny0wLjc2bDIuMjYtMi41M2wtMC41LTQuMDVsLTkuNTUtOS42MnYtNC44MWwyLjc2LTMuNTRsMi43Ni0xLjI3bDAuMjUtMi43OWgtNy4wNGwtMS4yNiwzLjhsLTMuMjctMC43NmwtNC4wMi00LjNsMi41MS02LjU4bDMuNTItMy44bDMuMjcsMC4yNWwtMC41LDUuODJsMS43NiwxLjUybDQuMjctNC4zbDEuNTEtMC4yNWwtMC41LTMuMjlsNC4wMi00LjgxbDMuMDIsMC4yNWwxLjc2LTUuNTdsMi4wNi0xLjA5bDAuMjEtMy40N2wtMi0yLjFsLTAuMTctNS40OGwzLjg1LTAuMjVsLTAuMjUtMTQuMTNsLTIuNywxLjYybC0xLjAxLDMuNjJsLTQuNTEtMC4wMWwtMTMuMDctNy4zNWwtOS40NC0xMS4zOGwtOS41OC0wLjFsLTIuNDQsMi4xMmwzLjEsNy4xbC0xLjA4LDYuNjZsLTMuODYsMS42bC0yLjE3LTAuMTdsLTAuMTYsNi41OWwyLjI2LDAuNTFsNC4wMi0xLjc3bDUuMjgsMi41M3YyLjUzbC0zLjc3LDAuMjVsLTMuMDIsNi41OGwtMi43NiwwLjI1bC05LjgsMTIuOTFsLTEwLjMsNC41NmwtNy4wNCwwLjUxbC00Ljc3LTMuMjlsLTYuNzksMy41NWwtNy4yOS0yLjI4bC0xLjc2LTQuODFsLTEyLjMxLTAuNzZsLTYuNTMtMTAuNjNoLTIuNzZsLTIuMjItNC45M0w2NzAuNCwxNzAuMDd6XCIsXCJuYW1lXCI6XCJDaGluYVwifSxcInR3XCI6e1wicGF0aFwiOlwiTTc4Ny40NiwyNDguMzFsLTMuNTQsMi43bC0wLjE5LDUuMmwzLjA2LDMuNTZsMC43Ni0wLjY3TDc4Ny40NiwyNDguMzFMNzg3LjQ2LDI0OC4zMXpcIixcIm5hbWVcIjpcIlRhaXdhblwifSxcImpwXCI6e1wicGF0aFwiOlwiTTgwMy4yMywyMTYuNDJsLTEuNjMsMS42NGwwLjY3LDIuMzFsMS40MywwLjFsMC45Niw1LjAxbDEuMTUsMS4yNWwyLjAxLTEuODNsMC44Ni0zLjI4bC0yLjQ5LTMuNTZMODAzLjIzLDIxNi40Mkw4MDMuMjMsMjE2LjQyTTgxMi4wMywyMTMuMTVsLTIuNzcsMi42bC0wLjEsMi45OWwwLjY3LDAuODdsMy43My0zLjE4bC0wLjI5LTMuMThMODEyLjAzLDIxMy4xNUw4MTIuMDMsMjEzLjE1TTgwOC4yLDIwNi45OGwtNC44OCw1LjU5bDAuODYsMS4zNWwyLjM5LDAuMjlsNC40OS0zLjQ3bDMuMTYtMC41OGwyLjg3LDMuMzdsMi4yLTAuNzdsMC44Ni0zLjI4bDQuMTEtMC4xbDQuMDItNC44MmwtMi4xLThsLTAuOTYtNC4yNGwyLjEtMS43M2wtNC43OC03LjIybC0xLjI0LDAuMWwtMi41OCwyLjg5djIuNDFsMS4xNSwxLjM1bDAuMzgsNi4zNmwtMi45NiwzLjY2bC0xLjcyLTEuMDZsLTEuMzQsMi45OWwtMC4yOSwyLjc5bDEuMDUsMS42NGwtMC42NywxLjI1bC0yLjItMS44M2gtMS41M2wtMS4zNCwwLjc3TDgwOC4yLDIwNi45OEw4MDguMiwyMDYuOThNODE2LjQzLDE2My40NGwtMS41MywxLjM1bDAuNzcsMi44OWwxLjM0LDEuMzVsLTAuMSw0LjQzbC0xLjcyLDAuNjdsLTEuMzQsMi45OWwzLjkyLDUuMzlsMi41OC0wLjg3bDAuNDgtMS4zNWwtMi43Ny0yLjVsMS43Mi0yLjIybDEuODIsMC4yOWwxLjQzLDEuNTRsMC4xLTMuMThsMy45Mi0zLjE4bDIuMi0wLjU4bC0xLjgyLTMuMDhsLTAuODYtMS4zNWwtMS40MywwLjk2bC0xLjI0LDEuNTRsLTIuNjgtMC41OGwtMi43Ny0xLjgzTDgxNi40MywxNjMuNDRMODE2LjQzLDE2My40NHpcIixcIm5hbWVcIjpcIkphcGFuXCJ9LFwicnVcIjp7XCJwYXRoXCI6XCJNNTA2LjYxLDE1MS43MmwtMS41LTAuMTVsLTIuNywzLjIzdjEuNTFsMC45LDAuMzVsMS43NSwwLjA1bDIuOS0yLjM3bDAuNC0wLjgxTDUwNi42MSwxNTEuNzJMNTA2LjYxLDE1MS43Mk04MzAuODYsMTYwLjQ1bC0yLjY4LDMuNzZsMC4xOSwxLjgzbDEuMzQtMC41OGwzLjE1LTMuOTVMODMwLjg2LDE2MC40NUw4MzAuODYsMTYwLjQ1TTgzNC40LDE1NC45NmwtMC45NiwyLjZsMC4xLDEuNzNsMS42My0xLjA2bDEuNTMtMy4wOFYxNTRMODM0LjQsMTU0Ljk2TDgzNC40LDE1NC45Nk04NDAuMDQsMTMyLjAzbC0xLjI0LDEuNTRsMC4xLDIuNDFsMS4xNS0wLjFsMS45MS0zLjM3TDg0MC4wNCwxMzIuMDNMODQwLjA0LDEzMi4wM004MzcuNzUsMTM3LjkxdjQuMjRsMS4zNCwwLjQ4bDAuOTYtMS41NHYtMy4yN0w4MzcuNzUsMTM3LjkxTDgzNy43NSwxMzcuOTFNNzk4LjY0LDEyMi41OWwtMC4wOSw2LjE3bDcuNzQsMTEuOTVsMi43NywxMC40bDQuODgsOS4yNWwxLjkxLDAuNjdsMS42My0xLjM1bDAuNzYtMi4yMmwtNi45OC03LjYxbDAuMTktMy45NWwxLjUzLTAuNjdsMC4zOC0yLjMxbC0xMy42Ny0xOS4zNkw3OTguNjQsMTIyLjU5TDc5OC42NCwxMjIuNTlNODUyLjU3LDEwMy40MmwtMS45MSwwLjE5bDEuMTUsMS42NGwyLjM5LDEuNjRsMC42Ny0wLjc3TDg1Mi41NywxMDMuNDJMODUyLjU3LDEwMy40Mk04NTYuMjksMTA0LjU4bDAuMjksMS42NGwyLjk2LDAuODdsMC4yOS0xLjE2TDg1Ni4yOSwxMDQuNThMODU2LjI5LDEwNC41OE01NDcuODIsMzguNzlsMS43MiwwLjY5bC0xLjIxLDIuMDh2Mi45NWwtMi41OCwxLjU2SDU0M2wtMS41NS0xLjkxbDAuMTctMi4wOGwxLjIxLTEuNTZoMi40MUw1NDcuODIsMzguNzlMNTQ3LjgyLDM4Ljc5TTU1NC4zNiwzNi44OHYyLjA4bDEuNzIsMS4zOWwyLjQxLTAuMTdsMi4wNy0xLjkxdi0xLjM5aC0xLjg5bC0xLjU1LDAuNTJsLTEuMjEtMS4zOUw1NTQuMzYsMzYuODhMNTU0LjM2LDM2Ljg4TTU2NC4xOCwzNy4wNmwxLjIxLDIuNmwyLjQxLDAuMTdsMS43Mi0wLjY5bC0wLjg2LTIuNDNsLTIuMjQtMC41Mkw1NjQuMTgsMzcuMDZMNTY0LjE4LDM3LjA2TTU3My45OSwzMy41OWwtMS44OS0wLjM1bC0xLjcyLDEuNzRsMC44NiwxLjU2bDAuNTIsMi40M2wyLjI0LTEuNzNsMC41Mi0xLjkxTDU3My45OSwzMy41OUw1NzMuOTksMzMuNTlNNTg0LjQ5LDUxLjk4bC0wLjUyLDIuNDNsLTMuOTYsMy40N2wtOC40NCwxLjkxbC02Ljg5LDExLjQ1bC0xLjIxLDMuM2w2Ljg5LDEuNzRsMS4wMy00LjE2bDIuMDctNi40Mmw1LjM0LTIuNzhsNC40OC0zLjQ3bDMuMjctMS4zOWgxLjcydi00LjY4TDU4NC40OSw1MS45OEw1ODQuNDksNTEuOThNNTYyLjI4LDc3LjMxbDQuNjUsMC41MmwxLjU1LDUuMzhsMy45Niw0LjE2bC0xLjM4LDIuNzhoLTIuNDFsLTIuMjQtMi42bC00Ljk5LTAuMTdsLTIuMDctMi43OHYtMS45MWwzLjEtMC44N0w1NjIuMjgsNzcuMzFMNTYyLjI4LDc3LjMxTTYzNC45NSwxOC4xNWwtMi4yNC0xLjM5aC0yLjU4bC0wLjUyLDEuNTZsLTIuNzUsMS41NmwtMi4wNywwLjY5bC0wLjM0LDIuMDhsNC44MiwwLjM1TDYzNC45NSwxOC4xNUw2MzQuOTUsMTguMTVNNjQwLjI4LDE4LjY3bC0xLjIxLDIuNmwtMi40MS0wLjE3bC0zLjc5LDIuNzhsLTEuMDMsMy40N2gyLjQxbDEuMzgtMi4yNmwzLjI3LDIuNDNsMy4xLTEuMzlsMi4yNC0xLjkxbC0wLjg2LTIuOTVsLTEuMjEtMi4wOEw2NDAuMjgsMTguNjdMNjQwLjI4LDE4LjY3TTY0NS4yOCwyMC41OGwxLjIxLDQuODZsMS44OSw0LjUxbDIuMDctMy42NGwzLjk2LTAuODd2LTIuNmwtMi41OC0xLjkxTDY0NS4yOCwyMC41OEw2NDUuMjgsMjAuNThNNzM5Ljc2LDEyLjhsMi42OSwyLjI2bDEuOTEtMC43OWwwLjU2LTMuMTdMNzQxLDguMzlsLTIuNTgsMS43bC02LjI4LDAuNTd2Mi44M2wtNi42MiwwLjExdjQuNjNsNy43NCw1Ljc2bDIuMDItMS40N2wtMC40NS00LjA3bDQuOTQtMS4yNGwtMS4wMS0xLjkybC0xLjc5LTEuODFMNzM5Ljc2LDEyLjhMNzM5Ljc2LDEyLjhNNzQ2Ljk0LDEwLjA5bDEuNzksMy4zOWw2Ljk2LTAuNzlsMS45MS0yLjQ5bC0wLjQ1LTIuMTVsLTEuOTEtMC43OWwtMS43OSwxLjM2bC01LjE2LDEuMTNMNzQ2Ljk0LDEwLjA5TDc0Ni45NCwxMC4wOU03NDYuNDksMjMuMzFsLTMuNDgtMC45TDc0MSwyNC41NmwtMC45LDIuOTRsNC43MS0wLjQ1bDMuNTktMS44MUw3NDYuNDksMjMuMzFMNzQ2LjQ5LDIzLjMxTTgzNi42OCwzLjc2bC0yLjkyLTAuOUw4MzAuNCw0LjFsLTEuNjgsMi40OWwyLjEzLDIuODNsNS42MS0yLjQ5bDEuMTItMS4yNEw4MzYuNjgsMy43Nkw4MzYuNjgsMy43Nk04MTcuOTcsNzIuOTNsMS43Niw2LjA4bDMuNTIsMS4wMWwzLjUyLTUuNTdsLTIuMDEtMy44bDAuNzUtMy4yOWg1LjI4bC0xLjI2LDIuNTNsMC41LDkuMTJsLTcuNTQsMTguNzRsMC43NSw0LjA1bC0wLjI1LDYuODRsMTQuMDcsMjAuNTFsMi43NiwwLjc2bDAuMjUtMTYuNzFsMi43Ni0yLjUzbC0zLjAyLTYuNThsMi41MS0yLjc5bC01LjUzLTcuMzRsLTMuMDIsMC4yNWwtMS0xMi4xNWw3Ljc5LTIuMDNsMC41LTMuNTVsNC4wMi0xLjAxbDIuMjYsMi4wM2wyLjc2LTExLjE0bDQuNzctOC4xbDMuNzctMi4wM2wzLjI3LDAuMjV2LTMuOGwtNS4yOC0xLjAxbC03LjI5LTYuMDhsMy41Mi00LjA1bC0zLjAyLTYuODRsMi41MS0yLjUzbDMuMDIsNC4wNWw3LjU0LDIuNzlsOC4yOSwwLjc2bDEuMDEtMy41NGwtNC4yNy00LjNsNC43Ny02LjU4bC0xMC44MS0zLjhsLTIuNzYsNS41N2wtMy41Mi00LjU2bC0xOS44NS02Ljg0bC0xOC44NSwzLjI5bC0yLjc2LDEuNTJ2MS41Mmw0LjAyLDIuMDNsLTAuNSw0LjgxbC03LjI5LTMuMDRsLTE2LjA4LDYuMzNsLTIuNzYtNS44MmgtMTEuMDZsLTUuMDMsNS4zMmwtMTcuODQtNC4wNWwtMTYuMzMsMy4yOWwtMi4wMSw1LjA2bDIuNTEsMC43NmwtMC4yNSwzLjhsLTE1LjgzLDEuNzdsMS4wMSw1LjA2bC0xNC41OC0yLjUzbDMuNTItNi41OGwtMTQuODMtMC43NmwxLjI2LDYuODRsLTQuNzcsMi4yOGwtNC4wMi0zLjhsLTE2LjMzLDIuNzlsLTYuMjgsNS44MmwtMC4yNSwzLjU0bC00LjAyLDAuMjVsLTAuNS00LjA1bDEyLjgyLTExLjE0di03LjZsLTguMjktMi4yOGwtMTAuODEsMy41NGwtNC41Mi00LjU2aC0yLjAxbC0yLjUxLDUuMDZsMi4wMSwyLjI4bC0xNC4zMyw3Ljg1bC0xMi4zMSw5LjM3bC03LjU0LDEwLjM4djQuM2w4LjA0LDMuMjlsLTQuMDIsMy4wNGwtOC41NC0zLjA0bC0zLjUyLDMuMDRsLTUuMjgtNi4wOGwtMS4wMSwyLjI4bDUuNzgsMTguMjNsMS41MSwwLjUxbDQuMDItMi4wM2wyLjAxLDEuNTJ2My4yOWwtMy43Ny0xLjUybC0yLjI2LDEuNzdsMS41MSwzLjI5bC0xLjI2LDguNjFsLTcuNzksMC43NmwtMC41LTIuNzlsNC41Mi0yLjc5bDEuMDEtNy42bC01LjAzLTYuNThsLTEuNzYtMTEuMzlsLTguMDQtMS4yN2wtMC43NSw0LjA1bDEuNTEsMi4wM2wtMy4yNywyLjc5bDEuMjYsNy42bDQuNzcsMi4wM2wxLjAxLDUuNTdsLTQuNzgtMy4wNGwtMTIuMzEtMi4yOGwtMS41MSw0LjA1bC05LjgsMy41NGwtMS41MS0yLjUzbC0xMi44Miw3LjA5bC0wLjI1LDQuODFsLTUuMDMsMC43NmwxLjUxLTMuNTR2LTMuNTRsLTUuMDMtMS43N2wtMy4yNywxLjI3bDIuNzYsNS4zMmwyLjAxLDMuNTR2Mi43OWwtMy43Ny0wLjc2bC0wLjc1LTAuNzZsLTMuNzcsNC4wNWwyLjAxLDMuNTRsLTguNTQtMC4yNWwyLjc2LDMuNTVsLTAuNzUsMS41MmgtNC41MmwtMy4yNy0yLjI4bC0wLjc1LTYuMzNsLTUuMjgtMi4wM3YtMi41M2wxMS4wNiwyLjI4bDYuMDMsMC41MWwyLjUxLTMuOGwtMi4yNi00LjA1bC0xNi4wOC02LjMzbC01LjU1LDEuMzhsLTEuOSwxLjYzbDAuNTksMy43NWwyLjM2LDAuNDFsLTAuNTUsNS45bDcuMjgsMTcuMWwtNS4yNiw4LjM0bC0wLjM2LDEuODhsMi42NywxLjg4bC0yLjQxLDEuNTlsLTEuNiwwLjAzbDAuMyw3LjM1bDIuMjEsMy4xM2wwLjAzLDMuMDRsMi44MywwLjI2bDQuMzMsMS42NWw0LjU4LDYuM2wwLjA1LDEuNjZsLTEuNDksMi41NWwzLjQyLTAuMTlsMy4zMywwLjk2bDQuNSw2LjM3bDExLjA4LDEuMDFsLTAuNDgsNy41OGwtMy44MiwzLjI3bDAuNzksMS4yOGwtMy43Nyw0LjA1bC0xLDMuOGwyLjI2LDMuMjlsNy4yOSwyLjUzbDMuMDItMS43N2wxOS4zNSw3LjM0bDAuNzUtMi4wM2wtNC4wMi0zLjh2LTQuODFsLTIuNTEtMC43NmwwLjUtNC4wNWw0LjAyLTQuODFsLTcuMjEtNS40bDAuNS03LjUxbDcuNzEtNS4wN2w5LjA1LDAuNTFsMS41MSwyLjc5bDkuMywwLjUxbDYuNzktMy44bC0zLjUyLTMuOGwwLjc1LTcuMDlsMTcuNTktOC42MWwxMy41Myw2LjFsNC41Mi00LjA1bDEzLjMyLDEyLjY2bDEwLjA1LTEuMDFsMy41MiwzLjU0bDkuNTUsMS4wMWw2LjI4LTguNjFsOC4wNCwzLjU1bDQuMjcsMC43Nmw0LjI3LTMuOGwtMy43Ny0yLjUzbDMuMjctNS4wNmw5LjMsMy4wNGwyLjAxLDQuMDVsNC4wMiwwLjI1bDIuNTEtMS43N2w2Ljc5LTAuMjVsMC43NSwxLjc3bDcuNzksMC41MWw1LjI4LTUuNTdsMTAuODEsMS4yN2wzLjI3LTEuMjdsMS02LjA4bC0zLjI3LTcuMzRsMy4yNy0yLjc5aDEwLjNsOS44LDExLjY1bDEyLjU2LDcuMDloMy43N2wwLjUtMy4wNGw0LjUyLTIuNzlsMC41LDE2LjQ2bC00LjAyLDAuMjV2NC4wNWwyLjI2LDIuNzlsLTAuNDIsMy42MmwxLjY3LDAuNjlsMS4wMS0yLjUzbDEuNTEsMC41MWwxLDEuMDFsNC41Mi0xLjAxbDQuNTItMTMuMTdsMC41LTE2LjQ2bC01Ljc4LTEzLjE3bC03LjI5LTguODZsLTMuNTIsMC41MXYyLjc5bC04LjU0LTMuMjlsMy4yNy03LjA5bDIuNzYtMTguNzRsMTEuNTYtMy41NGw1LjUzLTMuNTRoNi4wM0w4MDUuODYsOTZsMS41MSwyLjUzbDUuMjgtNS41N2wzLjAyLDAuMjVsLTAuNS0zLjI5bC00Ljc4LTEuMDFsMy4yNy0xMS45TDgxNy45Nyw3Mi45M0w4MTcuOTcsNzIuOTN6XCIsXCJuYW1lXCI6XCJSdXNzaWFuIEZlZGVyYXRpb25cIn0sXCJ1c1wiOntcInBhdGhcIjpcIk02OS4xNyw1My4zNWwzLjQ2LDYuNDdsMi4yMi0wLjV2LTIuMjRMNjkuMTcsNTMuMzVMNjkuMTcsNTMuMzVNNDkuNjYsMTEwLjI2bC0wLjE3LDMuMDFsMi4xNi0wLjV2LTEuMzRMNDkuNjYsMTEwLjI2TDQ5LjY2LDExMC4yNk00Ni4zNCwxMTEuNmwtNC4zMiwyLjE4bDAuNjcsMi4zNGwxLjY2LTEuMzRsMy4zMi0xLjUxTDQ2LjM0LDExMS42TDQ2LjM0LDExMS42TTI4LjM5LDExNC40NGwtMi45OS0wLjY3bC0wLjUsMS4zNGwwLjMzLDIuNTFMMjguMzksMTE0LjQ0TDI4LjM5LDExNC40NE0yMi4wNywxMTQuMjhsLTIuODMtMS4xN2wtMSwxLjg0bDEuODMsMS44NEwyMi4wNywxMTQuMjhMMjIuMDcsMTE0LjI4TTEyLjI3LDExMS42bC0xLjMzLTEuODRsLTEuMzMsMC41djIuNTFsMS41LDFMMTIuMjcsMTExLjZMMTIuMjcsMTExLjZNMS40Nyw5OS43MWwxLjY2LDEuMTdsLTAuNSwxLjM0SDEuNDdWOTkuNzFMMS40Nyw5OS43MU0xMCwyNDguN2wtMC4xNCwyLjMzbDIuMDQsMS4zN2wxLjIyLTEuMDlMMTAsMjQ4LjdMMTAsMjQ4LjdNMTUuMjksMjUyLjEzbC0xLjksMS4zN2wxLjYzLDIuMDVsMS45LTEuNjRMMTUuMjksMjUyLjEzTDE1LjI5LDI1Mi4xM00xOS4xLDI1NS40MWwtMS42MywyLjE5bDAuNTQsMS4zN2wyLjMxLTEuMDlMMTkuMSwyNTUuNDFMMTkuMSwyNTUuNDFNMjEuODEsMjU5LjY1bC0wLjk1LDUuNDdsMC45NSwyLjA1bDMuMTItMC45NmwxLjYzLTIuNzRsLTMuNC0zLjE1TDIxLjgxLDI1OS42NUwyMS44MSwyNTkuNjVNMjcxLjA1LDI4MS4wNmwtMi42NC0wLjg5bC0yLjEyLDEuMzNsMS4wNiwxLjI0bDMuNjEsMC41M0wyNzEuMDUsMjgxLjA2TDI3MS4wNSwyODEuMDZNOTMuMTEsNDQuODlsLTguMzksMS45OWwxLjczLDkuNDVsOS4xMywyLjQ5bDAuNDksMS45OUw4Mi41LDY1LjA0bC03LjY1LDEyLjY4bDIuNzEsMTMuNDNMODIsOTQuMTNsMy40Ni0zLjIzbDAuOTksMS45OWwtNC4yLDQuOTdsLTE2LjI5LDcuNDZsLTEwLjM3LDIuNDlsLTAuMjUsMy43M2wyMy45NC02Ljk2bDkuODctMi43NGw5LjEzLTExLjE5bDEwLjEyLTYuNzFsLTUuMTgsOC43bDUuNjgsMC43NWw5LjYzLTQuMjNsMS43Myw2Ljk2bDYuNjYsMS40OWw2LjkxLDYuNzFsMC40OSw0Ljk3bC0wLjk5LDEuMjRsMS4yMyw0LjcyaDEuNzNsMC4yNS03Ljk2aDEuOTdsMC40OSwxOS42NGw0Ljk0LTQuMjNsLTMuNDYtMjAuMzloLTUuMThsLTUuNjgtNy4yMWwyNy44OS00Ny4yNWwtMjcuNjQtMjEuNjNsLTMwLjg1LDUuOTdsLTEuMjMsOS40NWw2LjY2LDMuOThsLTIuNDcsNi40N0w5My4xMSw0NC44OUw5My4xMSw0NC44OU0xNDguNzYsMTU4LjM0bC0xLDQuMDJsLTMuNDktMi4yNmgtMS43NGwtMSw0LjI3bC0xMi4yMSwyNy4zNmwzLjI0LDIzLjg0bDMuOTksMi4wMWwwLjc1LDYuNTNoOC4yMmw3Ljk3LDYuMDJsMTUuNjksMS41MWwxLjc0LDguMDNsMi40OSwxLjc2bDMuNDktMy41MWwyLjc0LDEuMjVsMi40OSwxMS41NGw0LjIzLDIuNzZsMy40OS02LjUzbDEwLjcxLTcuNzhsNi45NywzLjI2bDUuOTgsMC41bDAuMjUtMy43NmwxMi40NSwwLjI1bDIuNDksMi43NmwwLjUsNi4yN2wtMS40OSwzLjUxbDEuNzQsNi4wMmgzLjc0bDMuNzQtNS43N2wtMS40OS0yLjc2bC0xLjQ5LTYuMDJsMi4yNC02Ljc4bDEwLjIxLTguNzhsNy43Mi0yLjI2bC0xLTcuMjhsMTAuNzEtMTEuNTVsMTAuNzEtMS43NkwyNzIuOCwxOTlsMTAuNDYtNi4wMnYtOC4wM2wtMS0wLjVsLTMuNzQsMS4yNWwtMC41LDQuOTJsLTEyLjQzLDAuMTVsLTkuNzQsNi40N2wtMTUuMjksNWwtMi40NC0yLjk5bDYuOTQtMTAuNWwtMy40My0zLjI3bC0yLjMzLTQuNDRsLTQuODMtMy44OGwtNS4yNS0wLjQ0bC05LjkyLTYuNzdMMTQ4Ljc2LDE1OC4zNEwxNDguNzYsMTU4LjM0elwiLFwibmFtZVwiOlwiVW5pdGVkIFN0YXRlcyBvZiBBbWVyaWNhXCJ9LFwibXVcIjp7XCJwYXRoXCI6XCJNNjEzLjAxLDM5OC45OWwtMS41MiwxLjk5bDAuMywyLjE1bDMuMi0yLjYxTDYxMy4wMSwzOTguOTlMNjEzLjAxLDM5OC45OXpcIixcIm5hbWVcIjpcIk1hdXJpdGl1c1wifSxcInJlXCI6e1wicGF0aFwiOlwiTTYwNy4zOCw0MDIuMzdsLTIuMjgsMC4xNWwtMC4xNSwxLjk5bDEuNTIsMC4zMWwyLjI4LTEuMDdMNjA3LjM4LDQwMi4zN0w2MDcuMzgsNDAyLjM3elwiLFwibmFtZVwiOlwiUmV1bmlvblwifSxcIm1nXCI6e1wicGF0aFwiOlwiTTU5Mi4zLDM3Mi45MmwtMi4xMyw1LjA2bC0zLjY1LDYuNDRsLTYuMzksMC40NmwtMi43NCwzLjIybDAuNDYsOS44MmwtMy45Niw0LjZsMC40Niw3LjgybDMuMzUsMy44M2wzLjk2LTAuNDZsMy45Ni0yLjkybC0wLjkxLTQuNmw5LjEzLTE1LjhsLTEuODMtMS45OWwxLjgzLTMuODNsMS45OCwwLjYxbDAuNjEtMS41M2wtMS44My03LjgybC0xLjA3LTMuMjJMNTkyLjMsMzcyLjkyTDU5Mi4zLDM3Mi45MnpcIixcIm5hbWVcIjpcIk1hZGFnYXNjYXJcIn0sXCJrbVwiOntcInBhdGhcIjpcIk01NzcuNjksMzcxLjIzbDAuNDYsMS41M2wxLjk4LDAuMzFsMC43Ni0xLjk5TDU3Ny42OSwzNzEuMjNMNTc3LjY5LDM3MS4yM001ODAuNTgsMzc0LjNsMC43NiwxLjY5aDEuMjJsMC42MS0yLjE1TDU4MC41OCwzNzQuM0w1ODAuNTgsMzc0LjN6XCIsXCJuYW1lXCI6XCJDb21vcm9zXCJ9LFwic2NcIjp7XCJwYXRoXCI6XCJNNjAyLjM1LDM1OC4zNGwtMC42MSwxLjIzbDEuNjcsMS4zOGwxLjIyLTEuMzhMNjAyLjM1LDM1OC4zNEw2MDIuMzUsMzU4LjM0TTYxMC44OCwzNDkuMTRsLTEuODMsMS4yM2wxLjM3LDIuMTVoMS44M0w2MTAuODgsMzQ5LjE0TDYxMC44OCwzNDkuMTRNNjExLjY0LDM1NC41MWwtMS4yMiwxLjM4bDAuOTEsMS4zOGwxLjY3LDAuMzFsMC4xNS0yLjkyTDYxMS42NCwzNTQuNTFMNjExLjY0LDM1NC41MXpcIixcIm5hbWVcIjpcIlNleWNoZWxsZXNcIn0sXCJtdlwiOntcInBhdGhcIjpcIk02NTYuNCwzMjAuNzZsMC4zLDIuNjFsMS42NywwLjYxbDAuMy0yLjNMNjU2LjQsMzIwLjc2TDY1Ni40LDMyMC43Nk02NTguNTMsMzI2LjI4bC0wLjE1LDMuMjJsMS4yMiwwLjYxbDEuMDctMi4xNUw2NTguNTMsMzI2LjI4TDY1OC41MywzMjYuMjhNNjU4Ljg0LDMzMi41N2wtMS4wNywxLjA3bDEuMjIsMS4wN2wxLjUyLTEuMDdMNjU4Ljg0LDMzMi41N0w2NTguODQsMzMyLjU3elwiLFwibmFtZVwiOlwiTWFsZGl2ZXNcIn0sXCJwdFwiOntcInBhdGhcIjpcIk0zNzIuNjQsMjE3LjAybC0xLjM2LDEuMzdsMi40NCwxLjM3bDAuMjctMS45MUwzNzIuNjQsMjE3LjAyTDM3Mi42NCwyMTcuMDJNMzc5Ljk3LDIxNi4ybC0xLjYzLDEuMDlsMS4zNiwxLjA5bDIuMTctMC41NUwzNzkuOTcsMjE2LjJMMzc5Ljk3LDIxNi4yTTM4MS4wNSwyMjAuMDNsLTAuODEsMi4xOWwxLjA4LDEuMzdsMS4zNi0xLjA5TDM4MS4wNSwyMjAuMDNMMzgxLjA1LDIyMC4wM00zODcuNTYsMjI0LjRsLTAuNTQsMS4zN2wwLjgxLDAuODJsMi4xNy0xLjM3TDM4Ny41NiwyMjQuNEwzODcuNTYsMjI0LjRNNDA4LjE4LDIzNi40MmwtMS4wOCwxLjM3bDEuMDgsMS4zN2wxLjYzLTAuODJMNDA4LjE4LDIzNi40Mkw0MDguMTgsMjM2LjQyTTQzMC45MywyMTEuMjRsLTAuNjIsOC42NWwtMS43NywxLjZsMC4xOCwwLjk4bDEuMjQsMi4wNWwtMC44LDIuNWwxLjMzLDAuNDVsMy4xLTAuMzZsLTAuMTgtMi41bDIuMDMtMTEuNTlsLTAuNDQtMS42TDQzMC45MywyMTEuMjRMNDMwLjkzLDIxMS4yNHpcIixcIm5hbWVcIjpcIlBvcnR1Z2FsXCJ9LFwiZXNcIjp7XCJwYXRoXCI6XCJNNDE1LjYyLDI1My43M2wtMS43NSwxLjAxbDAuODEsMC44Mkw0MTUuNjIsMjUzLjczTDQxNS42MiwyNTMuNzNNNDA5LjU0LDI1My45MmwtMi4xNywwLjU1bDEuMDgsMS42NGgxLjYzTDQwOS41NCwyNTMuOTJMNDA5LjU0LDI1My45Mk00MDQuMzgsMjUyLjI4bC0xLjM2LDEuMzdsMS45LDEuNjRsMS4wOC0yLjQ2TDQwNC4zOCwyNTIuMjhMNDA0LjM4LDI1Mi4yOE00NDguMzYsMjA1aC0xMi43NGwtMi41Ny0xLjE2bC0xLjI0LDAuMDlsLTEuNSwzLjEybDAuNTMsMy4yMWw0Ljg3LDAuNDVsMC42MiwyLjA1bC0yLjEyLDExLjk1bDAuMDksMi4xNGwzLjQ1LDEuODdsMy45OCwwLjI3bDcuOTYtMS45NmwzLjg5LTQuOWwwLjA5LTQuOTlsNi45LTYuMjRsMC4zNS0yLjc2bC02LjI4LTAuMDlMNDQ4LjM2LDIwNUw0NDguMzYsMjA1TTQ2MS4xLDIxNy4yMWwtMS41OSwwLjU0bDAuMzUsMS40M2gyLjNsMC45Ny0xLjA3TDQ2MS4xLDIxNy4yMUw0NjEuMSwyMTcuMjF6XCIsXCJuYW1lXCI6XCJTcGFpblwifSxcImN2XCI6e1wicGF0aFwiOlwiTTM4Ny41NiwyOTAuNTRsLTEuOSwxLjA5bDEuMzYsMS4wOWwxLjYzLTAuODJMMzg3LjU2LDI5MC41NEwzODcuNTYsMjkwLjU0TTM5Mi4yMywyOTIuNzRsLTEuMjQsMS4xbDAuODgsMS42M2wyLjEyLTAuOTVMMzkyLjIzLDI5Mi43NEwzOTIuMjMsMjkyLjc0TTM4OS41MiwyOTUuODNsLTEuNTksMC45NWwxLjcxLDIuMjlsMS4zNS0wLjcxTDM4OS41MiwyOTUuODNMMzg5LjUyLDI5NS44M3pcIixcIm5hbWVcIjpcIkNhcGUgVmVyZGVcIn0sXCJwZlwiOntcInBhdGhcIjpcIk0yNy4yNSw0MDIuNjhsLTEuOS0wLjE0bC0wLjE0LDEuNzhsMS40OSwwLjk2bDEuNzctMS4wOUwyNy4yNSw0MDIuNjhMMjcuMjUsNDAyLjY4TTMzLjc3LDQwNC42bC0yLjcyLDEuNzhsMi4wNCwyLjQ2bDEuNzctMC40MWwwLjk1LTEuMjNMMzMuNzcsNDA0LjZMMzMuNzcsNDA0LjZ6XCIsXCJuYW1lXCI6XCJGcmVuY2ggUG9seW5lc2lhXCJ9LFwia25cIjp7XCJwYXRoXCI6XCJNMjc2LjYsMjgzLjM3bC0xLjUsMC42MmwwLjUzLDEuMzNsMS43Ni0xLjE1bC0wLjM1LTAuMzZMMjc2LjYsMjgzLjM3TDI3Ni42LDI4My4zN3pcIixcIm5hbWVcIjpcIlNhaW50IEtpdHRzIGFuZCBOZXZpc1wifSxcImFnXCI6e1wicGF0aFwiOlwiTTI3OS4wNywyODQuODhsLTAuODgsMS44N2wxLjA2LDEuNDJsMS4zMi0xLjE1TDI3OS4wNywyODQuODhMMjc5LjA3LDI4NC44OHpcIixcIm5hbWVcIjpcIkFudGlndWEgYW5kIEJhcmJ1ZGFcIn0sXCJkbVwiOntcInBhdGhcIjpcIk0yODIuMDcsMjkwLjAzbC0xLjA2LDAuOThsMC43OSwxLjZsMS41LTAuNDRMMjgyLjA3LDI5MC4wM0wyODIuMDcsMjkwLjAzelwiLFwibmFtZVwiOlwiRG9taW5pY2FcIn0sXCJsY1wiOntcInBhdGhcIjpcIk0yODEuOTgsMjk0LjAzbC0wLjcxLDEuNTFsMS4xNSwxLjI0bDEuNS0wLjhMMjgxLjk4LDI5NC4wM0wyODEuOTgsMjk0LjAzelwiLFwibmFtZVwiOlwiU2FpbnQgTHVjaWFcIn0sXCJiYlwiOntcInBhdGhcIjpcIk0yODIuMDcsMjk3Ljg1bC0xLjIzLDAuODlsMC45NywxLjc4bDEuNTktMC44OUwyODIuMDcsMjk3Ljg1TDI4Mi4wNywyOTcuODV6XCIsXCJuYW1lXCI6XCJCYXJiYWRvc1wifSxcImdkXCI6e1wicGF0aFwiOlwiTTI4MC41NywzMDEuMzFsLTEuMTUsMS4xNWwwLjQ0LDAuNzFoMS40MWwwLjQ0LTEuMTZMMjgwLjU3LDMwMS4zMUwyODAuNTcsMzAxLjMxelwiLFwibmFtZVwiOlwiR3JlbmFkYVwifSxcInR0XCI6e1wicGF0aFwiOlwiTTI4Mi4yNCwzMDQuNzhsLTEuMDYsMC45OGwtMS4xNSwwLjE4djEuNDJsMi4xMiwxLjk1bDAuODgtMS40MmwwLjUzLTEuNmwtMC4xOC0xLjMzTDI4Mi4yNCwzMDQuNzhMMjgyLjI0LDMwNC43OHpcIixcIm5hbWVcIjpcIlRyaW5pZGFkIGFuZCBUb2JhZ29cIn0sXCJkb1wiOntcInBhdGhcIjpcIk0yNjMuMTEsMjgwLjQ0bC01LjI5LTMuNDZsLTIuNS0wLjg1bC0wLjg0LDZsMC44OCwxLjY5bDEuMTUtMS4zM2wzLjM1LTAuODlsMi45MSwwLjYyTDI2My4xMSwyODAuNDRMMjYzLjExLDI4MC40NHpcIixcIm5hbWVcIjpcIkRvbWluaWNhbiBSZXB1YmxpY1wifSxcImh0XCI6e1wicGF0aFwiOlwiTTI1MC44NiwyNzUuMzhsMy40NCwwLjM2bC0wLjQxLDQuMjJsLTAuMzQsMi4yMmwtNC4wMS0wLjIybC0wLjcxLDEuMDdsLTEuMjMtMC4wOWwtMC40NC0yLjMxbDQuMjMtMC4zNWwtMC4yNi0yLjRsLTEuOTQtMC44TDI1MC44NiwyNzUuMzhMMjUwLjg2LDI3NS4zOHpcIixcIm5hbWVcIjpcIkhhaXRpXCJ9LFwiZmtcIjp7XCJwYXRoXCI6XCJNMzA3Ljk1LDUwOC4xOGwtMi42My0wLjI5bC0yLjYyLDEuNzZsMS45LDIuMDZMMzA3Ljk1LDUwOC4xOEwzMDcuOTUsNTA4LjE4TTMxMC41Nyw1MDYuODZsLTAuODcsMi43OWwtMi40OCwyLjJsMC4xNSwwLjczbDQuMjMtMS42MmwxLjc1LTIuMkwzMTAuNTcsNTA2Ljg2TDMxMC41Nyw1MDYuODZ6XCIsXCJuYW1lXCI6XCJGYWxrbGFuZCBJc2xhbmRzXCJ9LFwiaXNcIjp7XCJwYXRoXCI6XCJNNDA2LjM2LDExNy4zMWwtMS45Ni0xLjExbC0yLjY0LDEuNjdsLTIuMjcsMi4xbDAuMDYsMS4xN2wyLjk0LDAuMzdsLTAuMTgsMi4xbC0xLjA0LDEuMDVsMC4yNSwwLjY4bDIuOTQsMC4xOXYzLjRsNC4yMywwLjc0bDIuNTEsMS40MmwyLjgyLDAuMTJsNC44NC0yLjQxbDMuNzQtNC45NGwwLjA2LTMuMzRsLTIuMjctMS45MmwtMS45LTEuNjFsLTAuODYsMC42MmwtMS4yOSwxLjY3bC0xLjQ3LTAuMTlsLTEuNDctMS42MWwtMS45LDAuMThsLTIuNzYsMi4yOWwtMS42NiwxLjc5bC0wLjkyLTAuOGwtMC4wNi0xLjk4bDAuOTItMC42Mkw0MDYuMzYsMTE3LjMxTDQwNi4zNiwxMTcuMzF6XCIsXCJuYW1lXCI6XCJJY2VsYW5kXCJ9LFwibm9cIjp7XCJwYXRoXCI6XCJNNDg4LjI2LDUzLjk2bC0xLjY1LTEuNjZsLTMuNjYsMS43OGgtNi43Mkw0NzUuMTcsNThsMy43NywzLjMzbDEuNjUtMC4yNGwyLjM2LTQuMDRsMiwxLjQzbC0xLjQyLDIuODVsLTAuNzEsNC4xNmwxLjY1LDIuNjFsMy41NC01Ljk0bDQuNi01LjU5bC0xLjc3LTEuNTRMNDg4LjI2LDUzLjk2TDQ4OC4yNiw1My45Nk00OTAuMjYsNDYuODNsLTIuOTUsMi43M2wxLjc3LDIuNzNoMy4xOGwxLjMsMS43OGwzLjg5LDIuMDJsNC40OC0yLjYxbDMuMDctMi42MWwtMS4wNi0yLjE0bC0zLjA3LTEuNzhsLTIuMjQsMi4wMmwtMS41My0xLjlsLTEuMTgsMC4xMmwtMS41MywzLjMzbC0yLjI0LTIuMjZsLTAuMjQtMS41NEw0OTAuMjYsNDYuODNMNDkwLjI2LDQ2LjgzTTQ5Ni45OCw1OS4wN2wtMi4zNiwyLjE0bC0yLDEuNTRsMC45NCwxLjY2bDEuODksMC41OWwzLjA3LTEuNDNsMS40Mi0xLjc4bC0xLjMtMi4xNEw0OTYuOTgsNTkuMDdMNDk2Ljk4LDU5LjA3TTUxNS40NiwxMDIuMTRsMi4wMi0xLjQ4TDUxNy4zLDk5bC0xLjI4LTAuNzRsMC4xOC0yLjAzaDEuMXYtMS4xMWwtNC43Ny0xLjI5bC03LjE1LDAuNzRsLTAuNzMsMy4xNEw1MDMsOTcuMTZsLTEuMS0xLjg1bC0zLjQ5LDAuMThMNDk4LjA0LDk5bC0xLjY1LDAuNzRsLTAuOTItMS44NWwtNy4zNCw1LjkxbDEuNDcsMS42NmwtMi43NSwxLjI5bC02LjI0LDEyLjM4bC0yLjIsMS40OGwwLjE4LDEuMTFsMi4yLDEuMTFsLTAuNTUsMi40bC0zLjY3LTAuMTlsLTEuMS0xLjI5bC0yLjM4LDIuNzdsLTEuNDcsMS4xMWwtMC4zNywyLjU5bC0xLjI4LDAuNzRsLTMuMywwLjc0bC0xLjY1LDUuMThsMS4xLDguNWwxLjI4LDMuODhsMS40NywxLjQ4bDMuMy0wLjE4bDQuNzctNC42MmwxLjgzLTMuMTRsMC41NSw0LjYybDMuMTItNS41NGwwLjE4LTE1LjUzbDIuNTQtMS42bDAuNzYtOC41N2w3LjctMTEuMDlsMy42Ny0xLjI5bDEuNjUtMi4wM2w1LjUsMS4yOWwyLjc1LDEuNjZsMC45Mi00LjYybDQuNTktMi43N0w1MTUuNDYsMTAyLjE0TDUxNS40NiwxMDIuMTR6XCIsXCJuYW1lXCI6XCJOb3J3YXlcIn0sXCJsa1wiOntcInBhdGhcIjpcIk02ODAuNTQsMzA4LjA1bDAuMjUsMi43MmwwLjI1LDEuOThsLTEuNDcsMC4yNWwwLjc0LDQuNDVsMi4yMSwxLjI0bDMuNDMtMS45OGwtMC45OC00LjY5bDAuMjUtMS43M2wtMy4xOS0yLjk2TDY4MC41NCwzMDguMDVMNjgwLjU0LDMwOC4wNXpcIixcIm5hbWVcIjpcIlNyaSBMYW5rYVwifSxcImN1XCI6e1wicGF0aFwiOlwiTTIyMC44NSwyNjYuOTJ2MS4yN2w1LjMyLDAuMWwyLjUxLTEuNDZsMC4zOSwxLjA3bDUuMjIsMS4yN2w0LjY0LDQuMTlsLTEuMDYsMS40NmwwLjE5LDEuNjZsMy44NywwLjk3bDMuODctMS43NWwxLjc0LTEuNzVsLTIuNTEtMS4yN2wtMTIuOTUtNy42bC00LjU0LTAuNDlMMjIwLjg1LDI2Ni45MkwyMjAuODUsMjY2LjkyelwiLFwibmFtZVwiOlwiQ3ViYVwifSxcImJzXCI6e1wicGF0aFwiOlwiTTIzOS42MSwyNTkuMTNsLTEuMjYtMC4zOWwtMC4xLDIuNDNsMS41NSwxLjU2bDEuMDYtMS41NkwyMzkuNjEsMjU5LjEzTDIzOS42MSwyNTkuMTNNMjQyLjEyLDI2Mi45M2wtMS43NCwwLjk3bDEuNjQsMi4zNGwwLjg3LTEuMTdMMjQyLjEyLDI2Mi45M0wyNDIuMTIsMjYyLjkzTTI0Ny43MywyNjQuNjhsLTEuODQtMC4xbDAuMTksMS4xN2wxLjM1LDEuOTVsMS4xNi0xLjI3TDI0Ny43MywyNjQuNjhMMjQ3LjczLDI2NC42OE0yNDYuODYsMjYyLjM1bC0zLTEuMjdsLTAuNTgtMy4wMmwxLjE2LTAuNDlsMS4xNiwyLjM0bDEuMTYsMC44OEwyNDYuODYsMjYyLjM1TDI0Ni44NiwyNjIuMzVNMjQzLjk2LDI1Ni4yMWwtMS41NS0wLjM5bC0wLjI5LTEuOTVsLTEuNjQtMC41OGwxLjA2LTEuMDdsMS45MywwLjY4bDEuNDUsMC44OEwyNDMuOTYsMjU2LjIxTDI0My45NiwyNTYuMjF6XCIsXCJuYW1lXCI6XCJCYWhhbWFzXCJ9LFwiam1cIjp7XCJwYXRoXCI6XCJNMjM4LjkzLDI3OS41OWwtMy40OCwwLjg4djAuOTdsMi4wMywxLjE3aDIuMTNsMS4zNS0xLjU2TDIzOC45MywyNzkuNTlMMjM4LjkzLDI3OS41OXpcIixcIm5hbWVcIjpcIkphbWFpY2FcIn0sXCJlY1wiOntcInBhdGhcIjpcIk0yMzAuMiwzMzUuODVsLTQuNzMsMi45NGwtMC4zNCw0LjM2bC0wLjk1LDEuNDNsMi45OCwyLjg2bC0xLjI5LDEuNDFsMC4zLDMuNmw1LjMzLDEuMjdsOC4wNy05LjU1bC0wLjAyLTMuMzNsLTMuODctMC4yNUwyMzAuMiwzMzUuODVMMjMwLjIsMzM1Ljg1elwiLFwibmFtZVwiOlwiRWN1YWRvclwifSxcImNhXCI6e1wicGF0aFwiOlwiTTIwMy43MywzNS44OWwwLjIyLDQuMDJsLTcuOTgsOC4yN2wyLDYuN2w1Ljc2LTEuNTZsMy4zMy00LjkybDguNDItMy4xM2w2Ljg3LTAuNDVsLTUuMzItNS44MWwtMi42NiwyLjAxbC0yLTAuNjdsLTEuMTEtMi40NmwtMi40NC0yLjQ2TDIwMy43MywzNS44OUwyMDMuNzMsMzUuODlNMjE0LjE1LDI0LjA1bC0xLjc3LDMuMTNsOC42NSwzLjEzbDMuMS00LjY5bDEuMzMsMy4xM2gyLjIybDQuMjEtNC42OWwtNS4xLTEuMzRsLTItMS41NmwtMi42NiwyLjY4TDIxNC4xNSwyNC4wNUwyMTQuMTUsMjQuMDVNMjI5LjIzLDMwLjMxbC02Ljg3LDIuOXYyLjIzbDguODcsMy4zNWwtMiwyLjIzbDEuMzMsMi45bDUuNTQtMi40Nmg0LjY2bDIuMjIsMy41N2wzLjc3LTMuOGwtMC44OS0zLjU4bC0zLjEsMS4xMmwtMC40NC00LjQ3bDEuNTUtMi42OGgtMS41NWwtMi40NCwxLjU2bC0xLjExLDAuODlsMC42NywzLjEzbC0xLjc3LDEuMzRsLTIuNjYtMC4yMmwtMC42Ny00LjAyTDIyOS4yMywzMC4zMUwyMjkuMjMsMzAuMzFNMjM4LjMyLDIzLjM4bC0wLjY3LDIuMjNsNC4yMSwyLjAxbDMuMS0xLjc5bC0wLjIyLTEuMzRMMjM4LjMyLDIzLjM4TDIzOC4zMiwyMy4zOE0yNDEuNjQsMTkuNThsLTMuMSwxLjEybDAuMjIsMS41Nmw2Ljg3LTAuNDVsLTAuMjItMS41NkwyNDEuNjQsMTkuNThMMjQxLjY0LDE5LjU4TTI1Ni41LDIzLjM4bC0wLjQ0LDEuNTZsLTEuMTEsMS41NnYyLjIzbDQuMjEtMC42N2w0LjQzLDMuOGgxLjU1di0zLjhsLTQuNDMtNC45MkwyNTYuNSwyMy4zOEwyNTYuNSwyMy4zOE0yNjcuODEsMjcuODVsMS43NywyLjAxbC0xLjU1LDIuNjhsMS4xMSwyLjlsNC44OC0yLjY4di0yLjAxbC0yLjg4LTMuMzVMMjY3LjgxLDI3Ljg1TDI2Ny44MSwyNy44NU0yNzQuMjQsMjIuNzFsMC4yMiwzLjU3aDUuOTlsMS41NSwxLjM0bC0wLjIyLDEuNTZsLTUuMzIsMC42N2wzLjc3LDUuMTRsNS4xLDAuODlsNy4wOS0zLjEzbC0xMC4yLTE1LjQybC0zLjEsMi4wMWwwLjIyLDIuNjhsLTMuNTUtMS4zNEwyNzQuMjQsMjIuNzFMMjc0LjI0LDIyLjcxTTIyMi41OCw0Ny45NmwtOC40MiwyLjIzbC00Ljg4LDQuMjVsMC40NCw0LjY5bDguODcsMi42OGwtMiw0LjQ3bC02LjQzLTQuMDJsLTEuNzcsMy4zNWw0LjIxLDIuOWwtMC4yMiw0LjY5bDYuNDMsMS43OWw3Ljc2LTAuNDVsMS4zMy0yLjQ2bDUuNzYsNi40OGwzLjk5LTEuMzRsMC42Ny00LjQ3bDIuODgsMi4wMWwwLjQ0LTQuNDdsLTMuNTUtMi4yM2wwLjIyLTE0LjA3bC0zLjEtMi40NkwyMzEuODksNTZMMjIyLjU4LDQ3Ljk2TDIyMi41OCw0Ny45Nk0yNDkuNjMsNTcuNzlsLTIuODgtMS4zNGwtMS41NSwyLjAxbDMuMSw0LjkybDAuMjIsNC42OWw2LjY1LTQuMDJ2LTUuODFsMi40NC0yLjQ2bC0yLjQ0LTEuNzloLTMuOTlMMjQ5LjYzLDU3Ljc5TDI0OS42Myw1Ny43OU0yNjMuODIsNTUuNzhsLTQuNjYsMy44bDEuMTEsNC42OWgyLjg4bDEuMzMtMi40NmwyLDIuMDFsMi0wLjIybDUuMzItNC40N0wyNjMuODIsNTUuNzhMMjYzLjgyLDU1Ljc4TTI2My4zNyw0OC40bC0xLjExLDIuMjNsNC44OCwxLjc5bDEuMzMtMi4wMUwyNjMuMzcsNDguNEwyNjMuMzcsNDguNE0yNjAuNDksMzkuOTFsLTQuODgsMC42N2wtMi44OCwyLjY4bDUuMzIsMC4yMmwtMS41NSw0LjAybDEuMTEsMS43OWwxLjU1LTAuMjJsMy43Ny02LjAzTDI2MC40OSwzOS45MUwyNjAuNDksMzkuOTFNMjY4LjkyLDM4LjM1bC0yLjY2LDAuODlsMC40NCwzLjU3bDQuNDMsMi45bDAuMjIsMi4yM2wtMS4zMywxLjM0bDAuNjcsNC40N2wxNy4wNyw1LjU4bDQuNjYsMS41Nmw0LjY2LTQuMDJsLTUuNTQtNC40N2wtNS4xLDEuMzRsLTcuMDktMC42N2wtMi42Ni0yLjY4bC0wLjY3LTcuMzdsLTQuNDMtMi4yM0wyNjguOTIsMzguMzVMMjY4LjkyLDM4LjM1TTI4Mi44OCw2MS41OUwyNzgsNjEuMTRsLTUuNzYsMi4yM2wtMy4xLDQuMjRsMC44OSwxMS42Mmw5LjUzLDAuNDVsOS4wOSw0LjQ3bDYuNDMsNy4zN2w0Ljg4LTAuMjJsLTEuMzMsNi45MmwtNC40Myw3LjM3bC00Ljg4LDIuMjNsLTMuNTUtMC42N2wtMS43Ny0xLjU2bC0yLjY2LDMuNTdsMS4xMSwzLjU3bDMuNzcsMC4yMmw0LjY2LTIuMjNsMy45OSwxMC4yOGw5Ljk4LDYuNDhsNi44Ny04LjcxbC01Ljc2LTkuMzhsMy4zMy0zLjhsNC42Niw3LjgybDguNDItNy4zN2wtMS41NS0zLjM1bC01Ljc2LDEuNzlsLTMuOTktMTAuOTVsMy43Ny02LjI1bC03LjU0LTguMDRsLTQuMjEsMi45bC0zLjk5LTguNzFsLTguNDIsMS4xMmwtMi4yMi0xMC41bC02Ljg3LDQuNjlsLTAuNjcsNS44MWgtMy43N2wwLjQ0LTUuMTRMMjgyLjg4LDYxLjU5TDI4Mi44OCw2MS41OU0yOTIuODYsNjUuNjFsLTEuNzcsMS43OWwxLjU1LDIuNDZsNy4zMiwwLjg5bC00LjY2LTQuOTJMMjkyLjg2LDY1LjYxTDI5Mi44Niw2NS42MU0yODUuNzcsNDAuMzZ2Mi4wMWwtNC44OCwxLjEybDEuMzMsMi4yM2w1LjU0LDIuMjNsNi4yMSwwLjY3bDQuNDMsMy4xM2w0LjQzLTIuNDZsLTMuMS0zLjEzaDMuOTlsMi40NC0yLjY4bDUuOTktMC44OXYtMS4zNGwtMy4zMy0yLjIzbDAuNDQtMi40Nmw5LjMxLDEuNTZsMTMuNzUtNS4zNmwtNS4xLTEuNTZsMS4zMy0xLjc5aDEwLjY0bDEuNzctMS43OWwtMjEuNTEtNy42bC01LjEtMS43OWwtNS41NCw0LjAybC02LjIxLTUuMTRsLTMuMzMtMC4yMmwtMC42Nyw0LjI1bC00LjIxLTMuOGwtNC44OCwxLjU2bDAuODksMi40Nmw3LjMyLDEuNTZsLTAuNDQsMy41N2wzLjk5LDIuNDZsOS43Ni0yLjQ2bDAuMjIsMy4zNWwtNy45OCwzLjhsLTQuODgtMy44bC00LjQzLDAuNDVsNC40Myw2LjI2bC0yLjIyLDEuMTJsLTMuMzMtMi45bC0yLjQ0LDEuNTZsMi4yMiw0LjI0aDMuNzdsLTAuODksNC4wMmwtMy4xLTAuNDVsLTMuOTktNC4yNUwyODUuNzcsNDAuMzZMMjg1Ljc3LDQwLjM2TTI2Ni4wMSwxMDEuODVsLTQuMjMsNS4zMmwtMC4yNiw1Ljg2bDMuNy0yLjEzaDQuNDlsMy4xNywyLjkzbDIuOTEtMi40TDI2Ni4wMSwxMDEuODVMMjY2LjAxLDEwMS44NU0zMTcuNTIsMTcxLjA1bC0xMC41NywxMC4xMmwxLjA2LDIuNGwxMi45NCw0Ljc5bDEuODUtMy4xOWwtMS4wNi01LjMybC00LjIzLDAuNTNsLTIuMzgtMi42NmwzLjk2LTMuOTlMMzE3LjUyLDE3MS4wNUwzMTcuNTIsMTcxLjA1TTE1OC4yMiw0OC42NmwxLjk5LDMuMDFsMSw0LjAybDQuOTgsMS4yNWwzLjQ5LTMuNzZsMi45OSwxLjUxbDguNDcsMC43NWw1Ljk4LTIuNTFsMSw4LjI4aDMuNDlWNTcuN2wzLjQ5LDAuMjVsOC43MiwxMC4yOWw1LjczLDMuNTFsLTIuOTksNC43N2wxLjI1LDEuMjVMMjE5LDgwLjAzbDAuMjUsNS4wMmwyLjk5LDAuNWwwLjc1LTcuNTNsNC43My0xLjI1bDMuNDksNS4yN2w3LjQ3LDMuNTFsMy43NCwwLjc1bDIuNDktMy4wMWwwLjI1LTQuNzdsNC40OC0yLjc2bDEuNDksNC4wMmwtMy45OSw3LjAzbDAuNSwzLjUxbDIuMjQtMy41MWw0LjQ4LTQuMDJsMC4yNS01LjI3bC0yLjQ5LTQuMDJsMC43NS0zLjI2bDUuOTgtMy4wMWwyLjc0LDIuMDFsMC41LDE3LjU3bDQuMjMtMy43NmwyLjQ5LDEuNTFsLTMuNDksNi4wMmw0LjQ4LDFsNi40OC0xMC4wNGw1LjQ4LDUuNzdsLTIuMjQsMTAuMjlsLTUuNDgsMy4wMWwtNS4yMy0yLjUxbC05LjQ2LDIuMDFsMSwzLjI2bC0yLjQ5LDQuMDJsLTcuNzIsMS43NmwtOC43Miw2Ljc4bC03LjcyLDEwLjI5bC0xLDMuMjZsNS4yMywyLjAxbDEuOTksNS4wMmw3LjIyLDcuMjhsMTEuNDYsNS4wMmwtMi40OSwxMS41NGwtMC4yNSwzLjI2bDIuOTksMi4wMWwzLjk5LTUuMjdsMC41LTEwLjA0bDYuMjMtMC4yNWwyLjk5LTUuNzdsMC41LTguNzhsNy45Ny0xNS41Nmw5Ljk2LDMuNTFsNS4yMyw3LjI4bC0yLjI0LDcuMjhsMy45OSwyLjI2bDkuNzEtNi41M2wyLjc0LDE3LjgybDguOTcsMTAuNzlsMC4yNSw1LjUybC05Ljk2LDIuNTFsLTQuNzMsNS4wMmwtOS45Ni0yLjI2bC00Ljk4LTAuMjVsLTguNzIsNi43OGw1LjIzLTEuMjVsNi40OC0xLjI1bDEuMjUsMS41MWwtMS43NCw1LjUybDAuMjUsNS4wMmwyLjk5LDIuMDFsMi45OS0wLjc1bDEuNS0yLjI2aDEuOTlsLTMuMjQsNi4wMmwtNi4yMywwLjI1bC0yLjc0LDQuMDJoLTMuNDlsLTEtMy4wMWw0Ljk4LTUuMDJsLTUuOTgsMi4wMWwtMC4yNy04LjUzbC0xLjcyLTFsLTUuMjMsMi4yNmwtMC41LDQuMjdoLTExLjk2bC0xMC4yMSw3LjAzbC0xMy43LDQuNTJsLTEuNDktMi4wMWw2LjktMTAuM2wtMy45Mi0zLjc3bC0yLjQ5LTQuNzhsLTUuMDctMy44N2wtNS40NC0wLjQ1bC05Ljc1LTYuODNsLTcwLjcxLTExLjYybC0xLjE3LTQuNzlsLTYuNDgtNi4wMnYtNS4wMmwxLTQuNTJsLTAuNS0yLjUxbC0yLjQ5LTIuNTFsLTAuNS00LjAybDYuNDgtNC41MmwtMy45OS0yMS41OGwtNS40OC0wLjI1bC00Ljk4LTYuNTNMMTU4LjIyLDQ4LjY2TDE1OC4yMiw0OC42Nk0xMzMuODMsMTI4LjQxbC0xLjcsMy4yNmwwLjU5LDIuMzFsMS4xMSwwLjY5bC0wLjI2LDAuOTRsLTEuMTksMC4zNGwwLjM0LDMuNDNsMS4yOCwxLjI5bDEuMDItMS4xMWwtMS4yOC0zLjM0bDAuNzYtMi42NmwxLjg3LTIuNDlsLTEuMzYtMi4zMUwxMzMuODMsMTI4LjQxTDEzMy44MywxMjguNDFNMTM5LjQ1LDE0Ny45NWwtMS41MywwLjZsMi44MSwzLjI2bDAuNjgsMy44NmwyLjgxLDNsMi4zOC0wLjQzdi0zLjk0bC0yLjg5LTEuOEwxMzkuNDUsMTQ3Ljk1TDEzOS40NSwxNDcuOTV6XCIsXCJuYW1lXCI6XCJDYW5hZGFcIn0sXCJndFwiOntcInBhdGhcIjpcIk0xOTQuODgsMjkxLjUybDUuOTMsNC4zNGw1Ljk4LTcuNDNsLTEuMDItMS41NGwtMi4wNC0wLjA3di00LjM1bC0xLjUzLTAuOTNsLTQuNjMsMS4zOGwxLjc3LDQuMDhMMTk0Ljg4LDI5MS41MkwxOTQuODgsMjkxLjUyelwiLFwibmFtZVwiOlwiR3VhdGVtYWxhXCJ9LFwiaG5cIjp7XCJwYXRoXCI6XCJNMjA3LjU1LDI4OC43OGw5LjI0LTAuMzVsMi43NCwzLjI2bC0xLjcxLTAuMzlsLTMuMjksMC4xNGwtNC4zLDQuMDRsLTEuODQsNC4wOWwtMS4yMS0wLjY0bC0wLjAxLTQuNDhsLTIuNjYtMS43OEwyMDcuNTUsMjg4Ljc4TDIwNy41NSwyODguNzh6XCIsXCJuYW1lXCI6XCJIb25kdXJhc1wifSxcInN2XCI6e1wicGF0aFwiOlwiTTIwMS42NSwyOTYuMjdsNC43LDIuMzRsLTAuMDctMy43MWwtMi40MS0xLjQ3TDIwMS42NSwyOTYuMjdMMjAxLjY1LDI5Ni4yN3pcIixcIm5hbWVcIjpcIkVsIFNhbHZhZG9yXCJ9LFwibmlcIjp7XCJwYXRoXCI6XCJNMjE3Ljc0LDI5Mi4xMWwyLjE5LDAuNDRsMC4wNyw0LjQ5bC0yLjU1LDcuMjhsLTYuODctMC42OGwtMS41My0zLjUxbDIuMDQtNC4yNmwzLjg3LTMuNkwyMTcuNzQsMjkyLjExTDIxNy43NCwyOTIuMTF6XCIsXCJuYW1lXCI6XCJOaWNhcmFndWFcIn0sXCJjclwiOntcInBhdGhcIjpcIk0yMTcuMzgsMzA0Ljk4bDEuMzksMi43MmwxLjEzLDEuNWwtMS41Miw0LjUxbC0yLjktMi4wNGwtNC43NC00LjM0di0yLjg3TDIxNy4zOCwzMDQuOThMMjE3LjM4LDMwNC45OHpcIixcIm5hbWVcIjpcIkNvc3RhIFJpY2FcIn0sXCJwYVwiOntcInBhdGhcIjpcIk0yMjAuNTksMzA5LjYxbC0xLjQ2LDQuNTZsNC44MiwxLjI1bDIuOTksMC41OWwwLjUxLTMuNTNsMy4yMS0xLjYybDIuODUsMS40N2wxLjEyLDEuNzlsMS4zNi0wLjE2bDEuMDctMy4yNWwtMy41Ni0xLjQ3bC0yLjctMS40N2wtMi43LDEuODRsLTMuMjEsMS42MmwtMy4yOC0xLjMyTDIyMC41OSwzMDkuNjFMMjIwLjU5LDMwOS42MXpcIixcIm5hbWVcIjpcIlBhbmFtYVwifSxcImNvXCI6e1wicGF0aFwiOlwiTTI1My43MywyOTkuNzhsLTIuMDYtMC4yMWwtMTMuNjIsMTEuMjNsLTEuNDQsMy45NWwtMS44NiwwLjIxbDAuODMsOC43M2wtNC43NSwxMS42NWw1LjE2LDQuMzdsNi42MSwwLjQybDQuNTQsNi42Nmw2LjYsMC4yMWwtMC4yMSw0Ljk5SDI1NmwyLjY4LTkuMTVsLTIuNDgtMy4xMmwwLjYyLTUuODJsNS4xNi0wLjQybC0wLjYyLTEzLjUybC0xMS41Ni0zLjc0bC0yLjY4LTcuMjhMMjUzLjczLDI5OS43OEwyNTMuNzMsMjk5Ljc4elwiLFwibmFtZVwiOlwiQ29sb21iaWFcIn0sXCJ2ZVwiOntcInBhdGhcIjpcIk0yNTAuNDYsMzA1LjkybDAuNDQsMi41OWwzLjI1LDEuMDNsMC43NC00Ljc3bDMuNDMtMy41NWwzLjQzLDQuMDJsNy44OSwyLjE1bDYuNjgtMS40bDQuNTUsNS42MWwzLjQzLDIuMTVsLTMuNzYsNS43M2wxLjI2LDQuMzRsLTIuMTUsMi42NmwtMi4yMywxLjg3bC00LjgzLTIuNDNsLTEuMTEsMS4xMnYzLjQ2bDMuNTMsMS42OGwtMi42LDIuODFsLTIuNiwyLjgxbC0zLjQzLTAuMjhsLTMuNDUtMy43OWwtMC43My0xNC4yNmwtMTEuNzgtNC4wMmwtMi4xNC02LjI3TDI1MC40NiwzMDUuOTJMMjUwLjQ2LDMwNS45MnpcIixcIm5hbWVcIjpcIlZlbmV6dWVsYVwifSxcImd5XCI6e1wicGF0aFwiOlwiTTI4NS4wNSwzMTQuMTNsNy4yMiw2LjU0bC0yLjg3LDMuMzJsLTAuMjMsMS45N2wzLjc3LDMuODlsLTAuMDksMy43NGwtNi41NiwyLjVsLTMuOTMtNS4zMWwwLjg0LTYuMzhsLTEuNjgtNC43NUwyODUuMDUsMzE0LjEzTDI4NS4wNSwzMTQuMTN6XCIsXCJuYW1lXCI6XCJHdXlhbmFcIn0sXCJzclwiOntcInBhdGhcIjpcIk0yOTMuMTMsMzIxLjE0bDIuMDQsMS44N2wzLjE2LTEuOTZsMi44OCwwLjA5bC0wLjM3LDEuMTJsLTEuMjEsMi41MmwtMC4xOSw2LjI3bC01Ljc1LDIuMzRsMC4yOC00LjAybC0zLjcxLTMuNDZsMC4xOS0xLjc4TDI5My4xMywzMjEuMTRMMjkzLjEzLDMyMS4xNHpcIixcIm5hbWVcIjpcIlN1cmluYW1lXCJ9LFwiZ2ZcIjp7XCJwYXRoXCI6XCJNMzAyLjEzLDMyMS44bDUuODUsMy42NWwtMy4wNiw2LjA4bC0xLjExLDEuNGwtMy4yNS0xLjg3bDAuMDktNi41NUwzMDIuMTMsMzIxLjhMMzAyLjEzLDMyMS44elwiLFwibmFtZVwiOlwiRnJlbmNoIEd1aWFuYVwifSxcInBlXCI6e1wicGF0aFwiOlwiTTIyNS4wMywzNDkuNTJsLTEuOTQsMS45NmwwLjEzLDMuMTNsMTYuOTQsMzAuODhsMTcuNTksMTEuMzRsMi43Mi00LjU2bDAuNjUtMTAuMDNsLTEuNDItNi4yNWwtNC43OS04LjA4bC0yLjg1LDAuOTFsLTEuMjksMS40M2wtNS42OS02LjUybDEuNDItNy42OWw2LjYtNC4zbC0wLjUyLTQuMDRsLTYuNzItMC4yNmwtMy40OS01Ljg2bC0xLjk0LTAuNjVsMC4xMywzLjUybC04LjY2LDEwLjI5bC02LjQ3LTEuNTZMMjI1LjAzLDM0OS41MkwyMjUuMDMsMzQ5LjUyelwiLFwibmFtZVwiOlwiUGVydVwifSxcImJvXCI6e1wicGF0aFwiOlwiTTI1OC43MSwzNzIuNzlsOC4yMy0zLjU5bDIuNzIsMC4yNmwxLjgxLDcuNTZsMTIuNTQsNC4xN2wyLjA3LDYuMzlsNS4xNywwLjY1bDIuMiw1LjQ3bC0xLjU1LDQuOTVsLTguNDEsMC42NWwtMy4xLDcuOTVsLTYuNi0wLjEzbC0yLjA3LTAuMzlsLTMuODEsMy43bC0xLjg4LTAuMThsLTYuNDctMTQuOTlsMS43OS0yLjY4bDAuNjMtMTAuNmwtMS42LTYuMzFMMjU4LjcxLDM3Mi43OUwyNTguNzEsMzcyLjc5elwiLFwibmFtZVwiOlwiQm9saXZpYVwifSxcInB5XCI6e1wicGF0aFwiOlwiTTI5MS43NiwzOTkuNTFsMi4yLDIuNGwtMC4yNiw1LjA4bDYuMzQtMC4zOWw0Ljc5LDYuMTNsLTAuMzksNS40N2wtMy4xLDQuNjlsLTYuMzQsMC4yNmwtMC4yNi0yLjYxbDEuODEtNC4zbC02LjIxLTMuOTFoLTUuMTdsLTMuODgtNC4xN2wyLjgyLTguMDZMMjkxLjc2LDM5OS41MUwyOTEuNzYsMzk5LjUxelwiLFwibmFtZVwiOlwiUGFyYWd1YXlcIn0sXCJ1eVwiOntcInBhdGhcIjpcIk0zMDAuMzYsNDMxLjkzbC0yLjA1LDIuMTlsMC44NSwxMS43OGw2LjQ0LDEuODdsOC4xOS04LjIxTDMwMC4zNiw0MzEuOTNMMzAwLjM2LDQzMS45M3pcIixcIm5hbWVcIjpcIlVydWd1YXlcIn0sXCJhclwiOntcInBhdGhcIjpcIk0zMDUuNDcsNDE4LjJsMS45NCwxLjgybC03LjM3LDEwLjk1bC0yLjU5LDIuODdsMC45LDEyLjUxbDUuNjksNi45MWwtNC43OCw4LjM0bC0zLjYyLDEuNTZoLTQuMTRsMS4xNiw2LjUxbC02LjQ3LDIuMjJsMS41NSw1LjQ3bC0zLjg4LDEyLjM4bDQuNzksMy45MWwtMi41OSw2LjM4bC00LjQsNi45MWwyLjMzLDQuODJsLTUuNjksMC45MWwtNC42Ni01LjczbC0wLjc4LTE3Ljg1bC03LjI0LTMwLjMybDIuMTktMTAuNmwtNC42Ni0xMy41NWwzLjEtMTcuNTlsMi44NS0zLjM5bC0wLjctMi41N2wzLjY2LTMuMzRsOC4xNiwwLjU2bDQuNTYsNC44N2w1LjI3LDAuMDlsNS40LDMuM2wtMS41OSwzLjcybDAuMzgsMy43Nmw3LjY1LTAuMzZMMzA1LjQ3LDQxOC4yTDMwNS40Nyw0MTguMk0yODguOTIsNTE4Ljc5bDAuMjYsNS43M2w0LjQtMC4zOWwzLjc1LTIuNDhsLTYuMzQtMS4zTDI4OC45Miw1MTguNzlMMjg4LjkyLDUxOC43OXpcIixcIm5hbWVcIjpcIkFyZ2VudGluYVwifSxcImNsXCI6e1wicGF0aFwiOlwiTTI4NS4wNCw1MTQuMWwtNC4yNyw5LjM4bDcuMzcsMC43OGwwLjEzLTYuMjVMMjg1LjA0LDUxNC4xTDI4NS4wNCw1MTQuMU0yODMuNTksNTEyLjYzbC0zLjIxLDMuNTVsLTAuMzksNC4xN2wtNi4yMS0zLjUybC02LjYtOS41MWwtMS45NC0zLjM5bDIuNzItMy41MmwtMC4yNi00LjQzbC0zLjEtMS4zbC0yLjQ2LTEuODJsMC41Mi0yLjQ4bDMuMjMtMC45MWwwLjY1LTE0LjMzbC01LjA0LTIuODdsLTMuMjktNzQuNTlsMC44NS0xLjQ4bDYuNDQsMTQuODVsMi4wNiwwLjA0bDAuNjcsMi4zN2wtMi43NCwzLjMybC0zLjE1LDE3Ljg3bDQuNDgsMTMuNzZsLTIuMDcsMTAuNDJsNy4zLDMwLjY0bDAuNzcsMTcuOTJsNS4yMyw2LjA1TDI4My41OSw1MTIuNjNMMjgzLjU5LDUxMi42M00yNjIuMjgsNDc1LjE0bC0xLjI5LDEuOTVsMC42NSwzLjM5bDEuMjksMC4xM2wwLjY1LTQuM0wyNjIuMjgsNDc1LjE0TDI2Mi4yOCw0NzUuMTR6XCIsXCJuYW1lXCI6XCJDaGlsZVwifSxcImJyXCI6e1wicGF0aFwiOlwiTTMxNC4yNCw0MzguODVsNi4yNS0xMi4wMmwwLjIzLTEwLjFsMTEuNjYtNy41Mmg2LjUzbDUuMTMtOC42OWwwLjkzLTE2LjY4bC0yLjEtNC40NmwxMi4zNi0xMS4yOGwwLjQ3LTEyLjQ1bC0xNi43OS04LjIybC0yMC4yOC02LjM0bC05LjU2LTAuOTRsMi41Ny01LjRsLTAuNy04LjIybC0yLjA5LTAuNjlsLTMuMDksNi4xNGwtMS42MiwyLjAzbC00LjE2LTEuODRsLTEzLjk5LDQuOTNsLTQuNjYtNS44N2wwLjc1LTYuMTNsLTQuNCw0LjQ4bC00Ljg2LTIuNjJsLTAuNDksMC42OWwwLjAxLDIuMTNsNC4xOSwyLjI1bC02LjI5LDYuNjNsLTMuOTctMC4wNGwtNC4wMi00LjA5bC00LjU1LDAuMTRsLTAuNTYsNC44NmwyLjYxLDMuMTdsLTMuMDgsOS44N2wtMy42LDAuMjhsLTUuNzMsMy42MmwtMS40LDcuMTFsNC45Nyw1LjMybDAuOTEtMS4wM2wzLjQ5LTAuOTRsMi45OCw1LjAybDguNTMtMy42NmwzLjMxLDAuMTlsMi4yOCw4LjA3bDEyLjE3LDMuODZsMi4xLDYuNDRsNS4xOCwwLjYybDIuNDcsNi4xNWwtMS42Nyw1LjQ3bDIuMTgsMi44NmwtMC4zMiw0LjI2bDUuODQtMC41NWw1LjM1LDYuNzZsLTAuNDIsNC43NWwzLjE3LDIuNjhsLTcuNiwxMS41MUwzMTQuMjQsNDM4Ljg1TDMxNC4yNCw0MzguODV6XCIsXCJuYW1lXCI6XCJCcmF6aWxcIn0sXCJielwiOntcInBhdGhcIjpcIk0yMDQuNTYsMjgyLjRsLTAuMDUsMy42NWgwLjg0bDIuODYtNS4zNGgtMS45NEwyMDQuNTYsMjgyLjRMMjA0LjU2LDI4Mi40elwiLFwibmFtZVwiOlwiQmVsaXplXCJ9LFwibW5cIjp7XCJwYXRoXCI6XCJNNjczLjgsMTcwLjE3bDUuODItNy43Mmw2Ljk5LDMuMjNsNC43NSwxLjI3bDUuODItNS4zNGwtMy45NS0yLjkxbDIuNi0zLjY3bDcuNzYsMi43NGwyLjY5LDQuNDFsNC44NiwwLjEzbDIuNTQtMS44OWw1LjIzLTAuMjFsMS4xNCwxLjk0bDguNjksMC40NGw1LjUtNS42MWw3LjYxLDAuOGwtMC40NCw3LjY0bDMuMzMsMC43Nmw0LjA5LTEuODZsNC4zMywyLjE0bC0wLjEsMS4wOGwtMy4xNCwwLjA5bC0zLjI3LDYuODZsLTIuNTQsMC4yNWwtOS44OCwxMi45MWwtMTAuMDksNC40NWwtNi4zMSwwLjQ5bC01LjI0LTMuMzhsLTYuNywzLjU4bC02LjYtMi4wNWwtMS44Ny00Ljc5bC0xMi41LTAuODhsLTYuNC0xMC44NWwtMy4xMS0wLjJMNjczLjgsMTcwLjE3TDY3My44LDE3MC4xN3pcIixcIm5hbWVcIjpcIk1vbmdvbGlhXCJ9LFwia3BcIjp7XCJwYXRoXCI6XCJNNzc4LjI4LDE5NC4yN2wxLjg0LDAuNzdsMC41Niw2LjQ0bDMuNjUsMC4yMWwzLjQ0LTQuMDNsLTEuMTktMS4wNmwwLjE0LTQuMzJsMy4xNi0zLjgybC0xLjYxLTIuOWwxLjA1LTEuMmwwLjU4LTNsLTEuODMtMC44M2wtMS41NiwwLjc5bC0xLjkzLDUuODZsLTMuMTItMC4yN2wtMy42MSw0LjI2TDc3OC4yOCwxOTQuMjdMNzc4LjI4LDE5NC4yN3pcIixcIm5hbWVcIjpcIk5vcnRoIEtvcmVhXCJ9LFwia3JcIjp7XCJwYXRoXCI6XCJNNzg4LjM0LDE5OC4ybDYuMTgsNS4wNGwxLjA1LDQuODhsLTAuMjEsMi42MmwtMy4wMiwzLjRsLTIuNiwwLjE0bC0yLjk1LTYuMzdsLTEuMTItMy4wNGwxLjE5LTAuOTJsLTAuMjgtMS4yN2wtMS40Ny0wLjY2TDc4OC4zNCwxOTguMkw3ODguMzQsMTk4LjJ6XCIsXCJuYW1lXCI6XCJTb3V0aCBLb3JlYVwifSxcImt6XCI6e1wicGF0aFwiOlwiTTU3Ni42OSwxODguNjJsNC4xLTEuNzVsNC41OC0wLjE2bDAuMzIsN2gtMi42OGwtMi4wNSwzLjM0bDIuNjgsNC40NWwzLjk1LDIuMjNsMC4zNiwyLjU1bDEuNDUtMC40OGwxLjM0LTEuNTlsMi4yMSwwLjQ4bDEuMTEsMi4yM2gyLjg0di0yLjg2bC0xLjc0LTUuMDlsLTAuNzktNC4xM2w1LjA1LTIuMjNsNi43OSwxLjExbDQuMjYsNC4yOWw5LjYzLTAuOTVsNS4zNyw3LjYzbDYuMzEsMC4zMmwxLjc0LTIuODZsMi4yMS0wLjQ4bDAuMzItMy4xOGwzLjMxLTAuMTZsMS43NCwyLjA3bDEuNzQtNC4xM2wxNC45OSwyLjA3bDIuNTItMy4zNGwtNC4yNi01LjI1bDUuNjgtMTIuNGw0LjU4LDAuMzJsMy4xNi03LjYzbC02LjMxLTAuNjRsLTMuNjMtMy41bC0xMCwxLjE2bC0xMi44OC0xMi40NWwtNC41NCw0LjAzbC0xMy43Ny02LjI1bC0xNi44OSw4LjI3bC0wLjQ3LDUuODhsMy45NSw0LjYxbC03LjcsNC4zNWwtOS45OS0wLjIybC0yLjA5LTMuMDdsLTcuODMtMC40M2wtNy40Miw0Ljc3bC0wLjE2LDYuNTJMNTc2LjY5LDE4OC42Mkw1NzYuNjksMTg4LjYyelwiLFwibmFtZVwiOlwiS2F6YWtoc3RhblwifSxcInRtXCI6e1wicGF0aFwiOlwiTTU5My44NSwyMDcuNTlsLTAuNjIsMi42M2gtNC4xNXYzLjU2bDQuNDYsMi45NGwtMS4zOCw0LjAzdjEuODZsMS44NSwwLjMxbDIuNDYtMy4yNWw1LjU0LTEuMjRsMTEuODQsNC40OWwwLjE1LDMuMjVsNi42MSwwLjYybDcuMzgtNy43NWwtMC45Mi0yLjQ4bC00LjkyLTEuMDhsLTEzLjg0LTguOTlsLTAuNjItMy4yNWgtNS4yM2wtMi4zMSw0LjM0aC0yLjMxTDU5My44NSwyMDcuNTlMNTkzLjg1LDIwNy41OXpcIixcIm5hbWVcIjpcIlR1cmttZW5pc3RhblwifSxcInV6XCI6e1wicGF0aFwiOlwiTTYyOC45MiwyMTkuMDZsMy4wOCwwLjE2di01LjI3bC0yLjkyLTEuN2w0LjkyLTYuMmgybDIsMi4zM2w1LjIzLTIuMDFsLTcuMjMtMi40OGwtMC4yOC0xLjVsLTEuNzIsMC40MmwtMS42OSwyLjk0bC03LjI5LTAuMjRsLTUuMzUtNy41N2wtOS40LDAuOTNsLTQuNDgtNC40NGwtNi4yLTEuMDVsLTQuNSwxLjgzbDIuNjEsOC42OGwwLjAzLDIuOTJsMS45LDAuMDRsMi4zMy00LjQ0bDYuMiwwLjA4bDAuOTIsMy40MWwxMy4yOSw4LjgybDUuMTQsMS4xOEw2MjguOTIsMjE5LjA2TDYyOC45MiwyMTkuMDZ6XCIsXCJuYW1lXCI6XCJVemJla2lzdGFuXCJ9LFwidGpcIjp7XCJwYXRoXCI6XCJNNjMwLjE5LDIxMS44NGw0LjExLTUuMWgxLjU1bDAuNTQsMS4xNGwtMS45LDEuMzh2MS4xNGwxLjI1LDAuOWw2LjAxLDAuMzZsMS45Ni0wLjg0bDAuODksMC4xOGwwLjYsMS45MmwzLjU3LDAuMzZsMS43OSwzLjc4bC0wLjU0LDEuMTRsLTAuNzEsMC4wNmwtMC43MS0xLjQ0bC0xLjU1LTAuMTJsLTIuNjgsMC4zNmwtMC4xOCwyLjUybC0yLjY4LTAuMThsMC4xMi0zLjE4bC0xLjk2LTEuOTJsLTIuOTgsMi40NmwwLjA2LDEuNjJsLTIuNjIsMC45aC0xLjU1bDAuMTItNS41OEw2MzAuMTksMjExLjg0TDYzMC4xOSwyMTEuODR6XCIsXCJuYW1lXCI6XCJUYWppa2lzdGFuXCJ9LFwia2dcIjp7XCJwYXRoXCI6XCJNNjM2LjgxLDE5OS4yMWwtMC4zMSwyLjUzbDAuMjUsMS41Nmw4LjcsMi45MmwtNy42NCwzLjA4bC0wLjg3LTAuNzJsLTEuNjUsMS4wNmwwLjA4LDAuNThsMC44OCwwLjRsNS4zNiwwLjE0bDIuNzItMC44MmwzLjQ5LTQuNGw0LjM3LDAuNzZsNS4yNy03LjNsLTE0LjEtMS45MmwtMS45NSw0LjczbC0yLjQ2LTIuNjRMNjM2LjgxLDE5OS4yMUw2MzYuODEsMTk5LjIxelwiLFwibmFtZVwiOlwiS3lyZ3l6IFJlcHVibGljXCJ9LFwiYWZcIjp7XCJwYXRoXCI6XCJNNjE0LjEyLDIyNy4wNWwxLjU5LDEyLjQ2bDMuOTYsMC44N2wwLjM3LDIuMjRsLTIuODQsMi4zN2w1LjI5LDQuMjdsMTAuMjgtMy43bDAuODItNC4zOGw2LjQ3LTQuMDRsMi40OC05LjM2bDEuODUtMS45OWwtMS45Mi0zLjM0bDYuMjYtMy44N2wtMC44LTEuMTJsLTIuODksMC4xOGwtMC4yNiwyLjY2bC0zLjg4LTAuMDRsLTAuMDctMy41NWwtMS4yNS0xLjQ5bC0yLjEsMS45MWwwLjA2LDEuNzVsLTMuMTcsMS4ybC01Ljg1LTAuMzdsLTcuNiw3Ljk2TDYxNC4xMiwyMjcuMDVMNjE0LjEyLDIyNy4wNXpcIixcIm5hbWVcIjpcIkFmZ2hhbmlzdGFuXCJ9LFwicGtcIjp7XCJwYXRoXCI6XCJNNjIzLjEzLDI0OS44NGwyLjYsMy44NmwtMC4yNSwxLjk5bC0zLjQ2LDEuMzdsLTAuMjUsMy4yNGgzLjk2bDEuMzYtMS4xMmg3LjU0bDYuOCw1Ljk4bDAuODctMi44N2g1LjA3bDAuMTItMy42MWwtNS4xOS00Ljk4bDEuMTEtMi43NGw1LjMyLTAuMzdsNy4xNy0xNC45NWwtMy45Ni0zLjExbC0xLjQ4LTUuMjNsOS42NC0wLjg3bC01LjY5LTguMWwtMy4wMy0wLjgybC0xLjI0LDEuNWwtMC45MywwLjA3bC01LjY5LDMuNjFsMS44NiwzLjEybC0yLjEsMi4yNGwtMi42LDkuNTlsLTYuNDMsNC4xMWwtMC44Nyw0LjQ5TDYyMy4xMywyNDkuODRMNjIzLjEzLDI0OS44NHpcIixcIm5hbWVcIjpcIlBha2lzdGFuXCJ9LFwiaW5cIjp7XCJwYXRoXCI6XCJNNjcwLjk4LDMxMy4wMWw0LjU4LTIuMjRsMi43Mi05Ljg0bC0wLjEyLTEyLjA4bDE1LjU4LTE2Ljgydi0zLjk5bDMuMjEtMS4yNWwtMC4xMi00LjYxbC0zLjQ2LTYuNzNsMS45OC0zLjYxbDQuMzMsMy45OWw1LjU2LDAuMjV2Mi4yNGwtMS43MywxLjg3bDAuMzcsMWwyLjk3LDAuMTJsMC42MiwzLjM2aDAuODdsMi4yMy0zLjk5bDEuMTEtMTAuNDZsMy43MS0yLjYybDAuMTItMy42MWwtMS40OC0yLjg3bC0yLjM1LTAuMTJsLTkuMiw2LjA4bDAuNTgsMy45MWwtNi40Ni0wLjAybC0yLjI4LTIuNzlsLTEuMjQsMC4xNmwwLjQyLDMuODhsLTEzLjk3LTFsLTguNjYtMy44NmwtMC40Ni00Ljc1bC01Ljc3LTMuNThsLTAuMDctNy4zN2wtMy45Ni00LjUzbC05LjEsMC44N2wwLjk5LDMuOTZsNC40NiwzLjYxbC03LjcxLDE1Ljc4bC01LjE2LDAuMzlsLTAuODUsMS45bDUuMDgsNC43bC0wLjI1LDQuNzVsLTUuMTktMC4wOGwtMC41NiwyLjM2bDQuMzEtMC4xOWwwLjEyLDEuODdsLTMuMDksMS42MmwxLjk4LDMuNzRsMy44MywxLjI1bDIuMzUtMS43NGwxLjExLTMuMTFsMS4zNi0wLjYybDEuNjEsMS42MmwtMC40OSwzLjk5bC0xLjExLDEuODdsMC4yNSwzLjI0TDY3MC45OCwzMTMuMDFMNjcwLjk4LDMxMy4wMXpcIixcIm5hbWVcIjpcIkluZGlhXCJ9LFwibnBcIjp7XCJwYXRoXCI6XCJNNjcxLjE5LDI0Mi41NmwwLjQ2LDQuMjdsOC4wOCwzLjY2bDEyLjk1LDAuOTZsLTAuNDktMy4xM2wtOC42NS0yLjM4bC03LjM0LTQuMzdMNjcxLjE5LDI0Mi41Nkw2NzEuMTksMjQyLjU2elwiLFwibmFtZVwiOlwiTmVwYWxcIn0sXCJidFwiOntcInBhdGhcIjpcIk02OTUuNCwyNDguMDhsMS41NSwyLjEybDUuMjQsMC4wNGwtMC41My0yLjlMNjk1LjQsMjQ4LjA4TDY5NS40LDI0OC4wOHpcIixcIm5hbWVcIjpcIkJodXRhblwifSxcImJkXCI6e1wicGF0aFwiOlwiTTY5NS41NywyNTMuMTFsLTEuMzEsMi4zN2wzLjQsNi40NmwwLjEsNS4wNGwwLjYyLDEuMzVsMy45OSwwLjA3bDIuMjYtMi4xN2wxLjY0LDAuOTlsMC4zMywzLjA3bDEuMzEtMC44MmwwLjA4LTMuOTJsLTEuMS0wLjEzbC0wLjY5LTMuMzNsLTIuNzgtMC4xbC0wLjY5LTEuODVsMS43LTIuMjdsMC4wMy0xLjEyaC00Ljk0TDY5NS41NywyNTMuMTFMNjk1LjU3LDI1My4xMXpcIixcIm5hbWVcIjpcIkJhbmdsYWRlc2hcIn0sXCJtbVwiOntcInBhdGhcIjpcIk03MjkuNDQsMzAzLjY1bC0yLjc3LTQuNDRsMi4wMS0yLjgybC0xLjktMy40OWwtMS43OS0wLjM0bC0wLjM0LTUuODZsLTIuNjgtNS4xOWwtMC43OCwxLjI0bC0xLjc5LDMuMDRsLTIuMjQsMC4zNGwtMS4xMi0xLjQ3bC0wLjU2LTMuOTVsLTEuNjgtMy4xNmwtNi44NC02LjQ1bDEuNjgtMS4xMWwwLjMxLTQuNjdsMi41LTQuMmwxLjA4LTEwLjQ1bDMuNjItMi40N2wwLjEyLTMuODFsMi4xNywwLjcybDMuNDIsNC45NWwtMi41NCw1LjQ0bDEuNzEsNC4yN2w0LjIzLDEuNjZsMC43Nyw0LjY1bDUuNjgsMC44OGwtMS41NywyLjcxbC03LjE2LDIuODJsLTAuNzgsNC42Mmw1LjI2LDYuNzZsMC4yMiwzLjYxbC0xLjIzLDEuMjRsMC4xMSwxLjEzbDMuOTIsNS43NWwwLjExLDUuOTdMNzI5LjQ0LDMwMy42NUw3MjkuNDQsMzAzLjY1elwiLFwibmFtZVwiOlwiTXlhbm1hclwifSxcInRoXCI6e1wicGF0aFwiOlwiTTczMC4wMywyNzAuNDdsMy4yNCw0LjE3djUuMDdsMS4xMiwwLjU2bDUuMTUtMi40OGwxLjAxLDAuMzRsNi4xNSw3LjFsLTAuMjIsNC44NWwtMi4wMS0wLjM0bC0xLjc5LTEuMTNsLTEuMzQsMC4xMWwtMi4zNSwzLjk0bDAuNDUsMi4xNGwxLjksMS4wMWwtMC4xMSwyLjM3bC0xLjM0LDAuNjhsLTQuNTktMy4xNnYtMi44MmwtMS45LTAuMTFsLTAuNzgsMS4yNGwtMC40LDEyLjYybDIuOTcsNS40Mmw1LjI2LDUuMDdsLTAuMjIsMS40N2wtMi44LTAuMTFsLTIuNTctMy44M2gtMi42OWwtMy4zNi0yLjcxbC0xLjAxLTIuODJsMS40NS0yLjM3bDAuNS0yLjE0bDEuNTgtMi44bC0wLjA3LTYuNDRsLTMuODYtNS41OGwtMC4xNi0wLjY4bDEuMjUtMS4yNmwtMC4yOS00LjQzbC01LjE0LTYuNTFsMC42LTMuNzVMNzMwLjAzLDI3MC40N0w3MzAuMDMsMjcwLjQ3elwiLFwibmFtZVwiOlwiVGhhaWxhbmRcIn0sXCJraFwiOntcInBhdGhcIjpcIk03NDAuNDgsMjk5LjQ3bDQuMDksNC4zN2w3LjYxLTUuNjRsMC42Ny04LjlsLTMuOTMsMi43MWwtMi4wNC0xLjE0bC0yLjc3LTAuMzdsLTEuNTUtMS4wOWwtMC43NSwwLjA0bC0yLjAzLDMuMzNsMC4zMywxLjU0bDIuMDYsMS4xNWwtMC4yNSwzLjEzTDc0MC40OCwyOTkuNDdMNzQwLjQ4LDI5OS40N3pcIixcIm5hbWVcIjpcIkNhbWJvZGlhXCJ9LFwibGFcIjp7XCJwYXRoXCI6XCJNNzM1LjQ3LDI2Mi45M2wtMi40MiwxLjIzbC0yLjAxLDUuODZsMy4zNiw0LjI4bC0wLjU2LDQuNzNsMC41NiwwLjIzbDUuNTktMi43MWw3LjUsOC4zOGwtMC4xOCw1LjI4bDEuNjMsMC44OGw0LjAzLTMuMjdsLTAuMzMtMi41OWwtMTEuNjMtMTEuMDVsMC4xMS0xLjY5bDEuNDUtMS4wMWwtMS4wMS0yLjgybC00LjgxLTAuNzlMNzM1LjQ3LDI2Mi45M0w3MzUuNDcsMjYyLjkzelwiLFwibmFtZVwiOlwiTGFvIFBlb3BsZSdzIERlbW9jcmF0aWMgUmVwdWJsaWNcIn0sXCJ2blwiOntcInBhdGhcIjpcIk03NDUuMDYsMzA0LjQ1bDEuMTksMS44N2wwLjIyLDIuMTRsMy4xMywwLjM0bDMuOC01LjA3bDMuNTgtMS4wMWwxLjktNS4xOGwtMC44OS04LjM0bC0zLjY5LTUuMDdsLTMuODktMy4xMWwtNC45NS04LjVsMy41NS01Ljk0bC01LjA4LTUuODNsLTQuMDctMC4xOGwtMy42NiwxLjk3bDEuMDksNC43MWw0Ljg4LDAuODZsMS4zMSwzLjYzbC0xLjcyLDEuMTJsMC4xMSwwLjlsMTEuNDUsMTEuMmwwLjQ1LDMuMjlsLTAuNjksMTAuNEw3NDUuMDYsMzA0LjQ1TDc0NS4wNiwzMDQuNDV6XCIsXCJuYW1lXCI6XCJWaWV0bmFtXCJ9LFwiZ2VcIjp7XCJwYXRoXCI6XCJNNTU1LjQ2LDIwNC4xNmwzLjI3LDQuMjdsNC4wOCwxLjg4bDIuNTEtMC4wMWw0LjMxLTEuMTdsMS4wOC0xLjY5bC0xMi43NS00Ljc3TDU1NS40NiwyMDQuMTZMNTU1LjQ2LDIwNC4xNnpcIixcIm5hbWVcIjpcIkdlb3JnaWFcIn0sXCJhbVwiOntcInBhdGhcIjpcIk01NjkuNzIsMjA5Ljg5bDQuOCw2LjI2bC0xLjQxLDEuNjVsLTMuNC0wLjU5bC00LjIyLTMuNzhsMC4yMy0yLjQ4TDU2OS43MiwyMDkuODlMNTY5LjcyLDIwOS44OXpcIixcIm5hbWVcIjpcIkFybWVuaWFcIn0sXCJhelwiOntcInBhdGhcIjpcIk01NzEuNDEsMjA3LjcybC0xLjAxLDEuNzJsNC43MSw2LjE4bDEuNjQtMC41M2wyLjcsMi44M2wxLjE3LTQuOTZsMi45MywwLjQ3bC0wLjEyLTEuNDJsLTQuODItNC4yMmwtMC45MiwyLjQ4TDU3MS40MSwyMDcuNzJMNTcxLjQxLDIwNy43MnpcIixcIm5hbWVcIjpcIkF6ZXJiYWlqYW5cIn0sXCJpclwiOntcInBhdGhcIjpcIk01NjkuNjUsMjE3Ljk1bC0xLjIyLDEuMjdsMC4xMiwyLjAxbDEuNTIsMi4xM2w1LjM5LDUuOWwtMC44MiwyLjM2aC0wLjk0bC0wLjQ3LDIuMzZsMy4wNSwzLjlsMi44MSwwLjI0bDUuNjMsNy43OWwzLjE2LDAuMjRsMi40NiwxLjc3bDAuMTIsMy41NGw5LjczLDUuNjdoMy42M2wyLjIzLTEuODlsMi44MS0wLjEybDEuNjQsMy43OGwxMC41MSwxLjQ2bDAuMzEtMy44NmwzLjQ4LTEuMjZsMC4xNi0xLjM4bC0yLjc3LTMuNzhsLTYuMTctNC45NmwzLjI0LTIuOTVsLTAuMjMtMS4zbC00LjA2LTAuNjNsLTEuNzItMTMuN2wtMC4yLTMuMTVsLTExLjAxLTQuMjFsLTQuODgsMS4xbC0yLjczLDMuMzVsLTIuNDItMC4xNmwtMC43LDAuNTlsLTUuMzktMC4zNWwtNi44LTQuOTZsLTIuNTMtMi43N2wtMS4xNiwwLjI4bC0yLjA5LDIuMzlMNTY5LjY1LDIxNy45NUw1NjkuNjUsMjE3Ljk1elwiLFwibmFtZVwiOlwiSXJhblwifSxcInRyXCI6e1wicGF0aFwiOlwiTTU1OC43LDIwOS4xOWwtMi4yMywyLjM2bC04LjItMC4yNGwtNC45Mi0yLjk1bC00LjgtMC4xMmwtNS41MSwzLjlsLTUuMTYsMC4yNGwtMC40NywyLjk1aC01Ljg2bC0yLjM0LDIuMTN2MS4xOGwxLjQxLDEuMTh2MS4zbC0wLjU5LDEuNTRsMC41OSwxLjNsMS44OC0wLjk0bDEuODgsMi4wMWwtMC40NywxLjQybC0wLjcsMC45NWwxLjA1LDEuMThsNS4xNiwxLjA2bDMuNjMtMS41NHYtMi4yNGwxLjc2LDAuMzVsNC4yMiwyLjQ4bDQuNTctMC43MWwxLjk5LTEuODlsMS4yOSwwLjQ3djIuMTNoMS43NmwxLjUyLTIuOTVsMTMuMzYtMS40Mmw1LjgzLTAuNzFsLTEuNTQtMi4wMmwtMC4wMy0yLjczbDEuMTctMS40bC00LjI2LTMuNDJsMC4yMy0yLjk1aC0yLjM0TDU1OC43LDIwOS4xOUw1NTguNywyMDkuMTlNNTIzLjAyLDIwOS43bC0wLjE2LDMuNTVsMy4xLTAuOTVsMS40Mi0wLjk1bC0wLjQyLTEuNTRsLTEuNDctMS4xN0w1MjMuMDIsMjA5LjdMNTIzLjAyLDIwOS43elwiLFwibmFtZVwiOlwiVHVya2V5XCJ9LFwib21cIjp7XCJwYXRoXCI6XCJNNTk4LjM4LDI4MC44NGw3LjM5LTQuMjZsMS4zMS02LjI1bC0xLjYyLTAuOTNsMC42Ny02LjdsMS40MS0wLjgybDEuNTEsMi4zN2w4Ljk5LDQuN3YyLjYxbC0xMC44OSwxNi4wM2wtNS4wMSwwLjE3TDU5OC4zOCwyODAuODRMNTk4LjM4LDI4MC44NHpcIixcIm5hbWVcIjpcIk9tYW5cIn0sXCJhZVwiOntcInBhdGhcIjpcIk01OTQuMDEsMjY0Ljk0bDAuODcsMy40OGw5Ljg2LDAuODdsMC42OS03LjE0bDEuOS0xLjA0bDAuNTItMi42MWwtMy4xMSwwLjg3bC0zLjQ2LDUuMjNMNTk0LjAxLDI2NC45NEw1OTQuMDEsMjY0Ljk0elwiLFwibmFtZVwiOlwiVW5pdGVkIEFyYWIgRW1pcmF0ZXNcIn0sXCJxYVwiOntcInBhdGhcIjpcIk01OTIuNjMsMjU5LjAybC0wLjUyLDQuMDFsMS41NCwxLjE3bDEuNC0wLjEzbDAuNTItNS4wNWwtMS4yMS0wLjg3TDU5Mi42MywyNTkuMDJMNTkyLjYzLDI1OS4wMnpcIixcIm5hbWVcIjpcIlFhdGFyXCJ9LFwia3dcIjp7XCJwYXRoXCI6XCJNNTgzLjI5LDI0Ny4xN2wtMi4yNS0xLjIybC0xLjU2LDEuNTdsMC4xNywzLjE0bDMuNjMsMS4zOUw1ODMuMjksMjQ3LjE3TDU4My4yOSwyNDcuMTd6XCIsXCJuYW1lXCI6XCJLdXdhaXRcIn0sXCJzYVwiOntcInBhdGhcIjpcIk01ODQsMjUzLjI0bDcuMDEsOS43N2wyLjI2LDEuOGwxLjAxLDQuMzhsMTAuNzksMC44NWwxLjIyLDAuNjRsLTEuMjEsNS40bC03LjA5LDQuMThsLTEwLjM3LDMuMTRsLTUuNTMsNS40bC02LjU3LTMuODNsLTMuOTgsMy40OEw1NjYsMjc5LjRsLTMuOC0xLjc0bC0xLjM4LTIuMDl2LTQuNTNsLTEzLjgzLTE2LjcybC0wLjUyLTIuOTZoMy45OGw0Ljg0LTQuMThsMC4xNy0yLjA5bC0xLjM4LTEuMzlsMi43Ny0yLjI2bDUuODgsMC4zNWwxMC4wMyw4LjM2bDUuOTItMC4yN2wwLjM4LDEuNDZMNTg0LDI1My4yNEw1ODQsMjUzLjI0elwiLFwibmFtZVwiOlwiU2F1ZGkgQXJhYmlhXCJ9LFwic3lcIjp7XCJwYXRoXCI6XCJNNTQ2LjY3LDIyOS4xM2wtMC4zNSwyLjU0bDIuODIsMS4xOGwtMC4xMiw3LjA0bDIuODItMC4wNmwyLjgyLTIuMTNsMS4wNi0wLjE4bDYuNC01LjA5bDEuMjktNy4zOWwtMTIuNzksMS4zbC0xLjM1LDIuOTZMNTQ2LjY3LDIyOS4xM0w1NDYuNjcsMjI5LjEzelwiLFwibmFtZVwiOlwiU3lyaWFuIEFyYWIgUmVwdWJsaWNcIn0sXCJpcVwiOntcInBhdGhcIjpcIk01NjQuMzEsMjI1LjAzbC0xLjU2LDcuNzFsLTYuNDYsNS4zOGwwLjQxLDIuNTRsNi4zMSwwLjQzbDEwLjA1LDguMThsNS42Mi0wLjE2bDAuMTUtMS44OWwyLjA2LTIuMjFsMi44OCwxLjYzbDAuMzgtMC4zNmwtNS41Ny03LjQxbC0yLjY0LTAuMTZsLTMuNTEtNC41MWwwLjctMy4zMmwxLjA3LTAuMTRsMC4zNy0xLjQ3bC00Ljc4LTUuMDNMNTY0LjMxLDIyNS4wM0w1NjQuMzEsMjI1LjAzelwiLFwibmFtZVwiOlwiSXJhcVwifSxcImpvXCI6e1wicGF0aFwiOlwiTTU0OC45LDI0MC43OGwtMi40Niw4LjU4bC0wLjExLDEuMzFoMy44N2w0LjMzLTMuODJsMC4xMS0xLjQ1bC0xLjc3LTEuODFsMy4xNy0yLjYzbC0wLjQ2LTIuNDRsLTAuODcsMC4ybC0yLjY0LDEuODlMNTQ4LjksMjQwLjc4TDU0OC45LDI0MC43OHpcIixcIm5hbWVcIjpcIkpvcmRhblwifSxcImxiXCI6e1wicGF0aFwiOlwiTTU0Ni4yLDIzMi40NGwwLjA2LDEuOTVsLTAuODIsMi45NmwyLjgyLDAuMjRsMC4xOC00LjJMNTQ2LjIsMjMyLjQ0TDU0Ni4yLDIzMi40NHpcIixcIm5hbWVcIjpcIkxlYmFub25cIn0sXCJpbFwiOntcInBhdGhcIjpcIk01NDUuMzIsMjM4LjA2bC0xLjU4LDUuMDNsMi4wNSw2LjAzbDIuMzUtOC44MXYtMS44OUw1NDUuMzIsMjM4LjA2TDU0NS4zMiwyMzguMDZ6XCIsXCJuYW1lXCI6XCJJc3JhZWxcIn0sXCJjeVwiOntcInBhdGhcIjpcIk01NDMuMjEsMjI5Ljg0bDEuMjMsMC44OWwtMy44MSwzLjYxbC0xLjgyLTAuMDZsLTEuMzUtMC45NWwwLjE4LTEuNzdsMi43Ni0wLjE4TDU0My4yMSwyMjkuODRMNTQzLjIxLDIyOS44NHpcIixcIm5hbWVcIjpcIkN5cHJ1c1wifSxcImdiXCI6e1wicGF0aFwiOlwiTTQ0Ni4xMiwxNDkuMDhsLTEuODMsMi43N2wwLjczLDEuMTFoNC4yMnYxLjg1bC0xLjEsMS40OGwwLjczLDMuODhsMi4zOCw0LjYybDEuODMsNC4yNWwyLjkzLDEuMTFsMS4yOCwyLjIybC0wLjE4LDIuMDNsLTEuODMsMS4xMWwtMC4xOCwwLjkybDEuMjgsMC43NGwtMS4xLDEuNDhsLTIuNTcsMS4xMWwtNC45NS0wLjU1bC03LjcxLDMuNTFsLTIuNTctMS4yOWw3LjM0LTQuMjVsLTAuOTItMC41NWwtMy44NS0wLjM3bDIuMzgtMy41MWwwLjM3LTIuOTZsMy4xMi0wLjM3bC0wLjU1LTUuNzNsLTMuNjctMC4xOGwtMS4xLTEuMjlsMC4xOC00LjI1bC0yLjIsMC4xOGwyLjItNy4zOWw0LjA0LTIuOTZMNDQ2LjEyLDE0OS4wOEw0NDYuMTIsMTQ5LjA4TTQzOC40MiwxNjEuNDdsLTMuMywwLjM3bC0wLjE4LDIuOTZsMi4yLDEuNDhsMi4zOC0wLjU1bDAuOTItMS42Nkw0MzguNDIsMTYxLjQ3TDQzOC40MiwxNjEuNDd6XCIsXCJuYW1lXCI6XCJVbml0ZWQgS2luZ2RvbVwifSxcImllXCI6e1wicGF0aFwiOlwiTTQzOS41MSwxNjYuNTVsLTAuOTEsNmwtOC4wNywyLjk2aC0yLjU3bC0xLjgzLTEuMjl2LTEuMTFsNC4wNC0yLjU5bC0xLjEtMi4yMmwwLjE4LTMuMTRsMy40OSwwLjE4bDEuNi0zLjc2bC0wLjIxLDMuMzRsMi43MSwyLjE1TDQzOS41MSwxNjYuNTVMNDM5LjUxLDE2Ni41NXpcIixcIm5hbWVcIjpcIklyZWxhbmRcIn0sXCJzZVwiOntcInBhdGhcIjpcIk00OTcuNzIsMTA0LjU4bDEuOTYsMS44MWgzLjY3bDIuMDIsMy44OGwwLjU1LDYuNjVsLTQuOTUsMy41MXYzLjUxbC0zLjQ5LDQuODFsLTIuMDIsMC4xOGwtMi43NSw0LjYybDAuMTgsNC40NGw0Ljc3LDMuNTFsLTAuMzcsMi4wM2wtMS44MywyLjc3bC0yLjc1LDIuNGwwLjE4LDcuOTVsLTQuMjIsMS40OGwtMS40NywzLjE0aC0yLjAybC0xLjEtNS41NGwtNC41OS03LjA0bDMuNzctNi4zMWwwLjI2LTE1LjU5bDIuNi0xLjQzbDAuNjMtOC45Mmw3LjQxLTEwLjYxTDQ5Ny43MiwxMDQuNThMNDk3LjcyLDEwNC41OE00OTguNDksMTUwLjE3bC0yLjExLDEuNjdsMS4wNiwyLjQ1bDEuODctMS44Mkw0OTguNDksMTUwLjE3TDQ5OC40OSwxNTAuMTd6XCIsXCJuYW1lXCI6XCJTd2VkZW5cIn0sXCJmaVwiOntcInBhdGhcIjpcIk01MDYuNzksMTE2Ljk0bDIuMDcsMC45MWwxLjI4LDIuNGwtMS4yOCwxLjY2bC02LjQyLDcuMDJsLTEuMSwzLjdsMS40Nyw1LjM2bDQuOTUsMy43bDYuNi0zLjE0bDUuMzItMC43NGw0Ljk1LTcuOTVsLTMuNjctOC42OWwtMy40OS04LjMybDAuNTUtNS4zNmwtMi4yLTAuMzdsLTAuNTctMy45MWwtMi45Ni00LjgzbC0zLjI4LDIuMjdsLTEuMjksNS4yN2wtMy40OC0yLjA5bC00Ljg0LTEuMThsLTEuMDgsMS4yNmwxLjg2LDEuNjhsMy4zOS0wLjA2bDIuNzMsNC40MUw1MDYuNzksMTE2Ljk0TDUwNi43OSwxMTYuOTR6XCIsXCJuYW1lXCI6XCJGaW5sYW5kXCJ9LFwibHZcIjp7XCJwYXRoXCI6XCJNNTE4LjA3LDE1MS4zN2wtNi44NS0xLjExbDAuMTUsMy44M2w2LjM1LDMuODhsMi42LTAuNzZsLTAuMTUtMi45Mkw1MTguMDcsMTUxLjM3TDUxOC4wNywxNTEuMzd6XCIsXCJuYW1lXCI6XCJMYXR2aWFcIn0sXCJsdFwiOntcInBhdGhcIjpcIk01MTAuODEsMTU0LjdsLTIuMTUtMC4wNWwtMi45NSwyLjgyaC0yLjVsMC4xNSwzLjUzbC0xLjUsMi43N2w1LjQsMC4wNWwxLjU1LTAuMmwxLjU1LDEuODdsMy41NS0wLjE1bDMuNC00LjMzbC0wLjItMi41N0w1MTAuODEsMTU0LjdMNTEwLjgxLDE1NC43elwiLFwibmFtZVwiOlwiTGl0aHVhbmlhXCJ9LFwiYnlcIjp7XCJwYXRoXCI6XCJNNTEwLjY2LDE2Ni4yOWwxLjUsMi40N2wtMC42LDEuOTdsMC4xLDEuNTZsMC41NSwxLjg3bDMuMS0xLjc2bDMuODUsMC4xbDIuNywxLjExaDYuODVsMi00Ljc5bDEuMi0xLjgxdi0xLjIxbC00LjMtNi4wNWwtMy44LTEuNTFsLTMuMS0wLjM1bC0yLjcsMC44NmwwLjEsMi43MmwtMy43NSw0Ljc0TDUxMC42NiwxNjYuMjlMNTEwLjY2LDE2Ni4yOXpcIixcIm5hbWVcIjpcIkJlbGFydXNcIn0sXCJwbFwiOntcInBhdGhcIjpcIk01MTEuNDYsMTc0Ljc2bDAuODUsMS41NmwwLjIsMS42NmwtMC43LDEuNjFsLTEuNiwzLjA4bC0xLjM1LDAuNjFsLTEuNzUtMC43NmwtMS4wNSwwLjA1bC0yLjU1LDAuOTZsLTIuOS0wLjg2bC00LjctMy4zM2wtNC42LTIuNDdsLTEuODUtMi44MmwtMC4zNS02LjY1bDMuNi0zLjEzbDQuNy0xLjU2bDEuNzUtMC4ybC0wLjcsMS40MWwwLjQ1LDAuNTVsNy45MSwwLjE1bDEuNy0wLjA1bDIuOCw0LjI5bC0wLjcsMS43NmwwLjMsMi4wN0w1MTEuNDYsMTc0Ljc2TDUxMS40NiwxNzQuNzZ6XCIsXCJuYW1lXCI6XCJQb2xhbmRcIn0sXCJpdFwiOntcInBhdGhcIjpcIk00NzcuNTYsMjEzLjM4bC0yLjY1LDEuMzRsMC4zNSw1LjE3bDIuMTIsMC4zNmwxLjU5LTEuNTJ2LTQuOUw0NzcuNTYsMjEzLjM4TDQ3Ny41NiwyMTMuMzhNNDcyLjI3LDE5Ni45OGwtMC42MiwxLjU3bDAuMTcsMS43MWwyLjM5LDIuNzlsMy43Ni0wLjEzbDguMyw5LjY0bDUuMTgsMS41bDMuMDYsMi44OWwwLjczLDYuNTlsMS42NC0wLjk2bDEuNDItMy41OWwtMC4zNS0yLjU4bDIuNDMtMC4yMmwwLjM1LTEuNDZsLTYuODUtMy4yOGwtNi41LTYuMzlsLTIuNTktMy44MmwtMC42My0zLjYzbDMuMzEtMC43OWwtMC44NS0yLjM5bC0yLjAzLTEuNzFsLTEuNzUtMC4wOGwtMi40NCwwLjY3bC0yLjMsMy4yMmwtMS4zOSwwLjkybC0yLjE1LTEuMzJMNDcyLjI3LDE5Ni45OEw0NzIuMjcsMTk2Ljk4TTQ5Mi40NCwyMjMuMDJsLTEuNDUtMC43OGwtNC45NSwwLjc4bDAuMTcsMS4zNGw0LjQ1LDIuMjRsMC42NywwLjczbDEuMTcsMC4xN0w0OTIuNDQsMjIzLjAyTDQ5Mi40NCwyMjMuMDJ6XCIsXCJuYW1lXCI6XCJJdGFseVwifSxcImZyXCI6e1wicGF0aFwiOlwiTTQ3Ny44MywyMDYuOTZsLTEuOTUsMS45NmwtMC4xOCwxLjc4bDEuNTksMC45OGwwLjYyLTAuMDlsMC4zNS0yLjU5TDQ3Ny44MywyMDYuOTZMNDc3LjgzLDIwNi45Nk00NjAuNCwxNzguN2wtMi4yMSwwLjU0bC00LjQyLDQuODFsLTEuMzMsMC4wOWwtMS43Ny0xLjI1bC0xLjE1LDAuMjdsLTAuODgsMi43NmwtNi40NiwwLjE4bDAuMTgsMS40M2w0LjQyLDIuOTRsNS4xMyw0LjFsLTAuMDksNC45bC0yLjc0LDQuODFsNS45MywyLjg1bDYuMDIsMC4xOGwxLjg2LTIuMTRsMy44LDAuMDlsMS4wNiwwLjk4bDMuOC0wLjI3bDEuOTUtMi41bC0yLjQ4LTIuOTRsLTAuMTgtMS44N2wwLjUzLTIuMDVsLTEuMjQtMS43OGwtMi4xMiwwLjYybC0wLjI3LTEuNmw0LjY5LTUuMTd2LTMuMTJsLTMuMS0xLjc4bC0xLjU5LTAuMjdMNDYwLjQsMTc4LjdMNDYwLjQsMTc4Ljd6XCIsXCJuYW1lXCI6XCJGcmFuY2VcIn0sXCJubFwiOntcInBhdGhcIjpcIk00NzAuMDksMTY4LjI3bC00LjUzLDIuMjNsMC45NiwwLjg3bDAuMSwyLjIzbC0wLjk2LTAuMTlsLTEuMDYtMS42NWwtMi41Myw0LjAxbDMuODksMC44MWwxLjQ1LDEuNTNsMC43NywwLjAybDAuNTEtMy40NmwyLjQ1LTEuMDNMNDcwLjA5LDE2OC4yN0w0NzAuMDksMTY4LjI3elwiLFwibmFtZVwiOlwiTmV0aGVybGFuZHNcIn0sXCJiZVwiOntcInBhdGhcIjpcIk00NjEuNjEsMTc2LjUybC0wLjY0LDEuNmw2Ljg4LDQuNTRsMS45OCwwLjQ3bDAuMDctMi4xNWwtMS43My0xLjk0aC0xLjA2bC0xLjQ1LTEuNjVMNDYxLjYxLDE3Ni41Mkw0NjEuNjEsMTc2LjUyelwiLFwibmFtZVwiOlwiQmVsZ2l1bVwifSxcImRlXCI6e1wicGF0aFwiOlwiTTQ3MS4xNCwxNjcuODhsMy41Ny0wLjU4di0yLjUybDIuOTktMC40OWwxLjY0LDEuNjVsMS43MywwLjE5bDIuNy0xLjE3bDIuNDEsMC42OGwyLjEyLDEuODRsMC4yOSw2Ljg5bDIuMTIsMi44MmwtMi43OSwwLjM5bC00LjYzLDIuOTFsMC4zOSwwLjk3bDQuMTQsMy44OGwtMC4yOSwxLjk0bC0zLjg1LDEuOTRsLTMuNTcsMC4xbC0wLjg3LDEuODRoLTEuODNsLTAuODctMS45NGwtMy4xOC0wLjc4bC0wLjEtMy4ybC0yLjctMS44NGwwLjI5LTIuMzNsLTEuODMtMi41MmwwLjQ4LTMuM2wyLjUtMS4xN0w0NzEuMTQsMTY3Ljg4TDQ3MS4xNCwxNjcuODh6XCIsXCJuYW1lXCI6XCJHZXJtYW55XCJ9LFwiZGtcIjp7XCJwYXRoXCI6XCJNNDc2Ljc3LDE1MS41bC00LjE1LDQuNTlsLTAuMTUsMi45OWwxLjg5LDQuOTNsMi45Ni0wLjU2bC0wLjM3LTQuMDNsMi4wNC0yLjI4bC0wLjA0LTEuNzlsLTEuNDQtMy43M0w0NzYuNzcsMTUxLjVMNDc2Ljc3LDE1MS41TTQ4MS40NCwxNTkuNjRsLTAuOTMtMC4wNGwtMS4yMiwxLjEybDAuMTUsMS43NWwyLjg5LDAuMDhsMC4xNS0xLjk4TDQ4MS40NCwxNTkuNjRMNDgxLjQ0LDE1OS42NHpcIixcIm5hbWVcIjpcIkRlbm1hcmtcIn0sXCJjaFwiOntcInBhdGhcIjpcIk00NzIuOTEsMTg5LjM4bC00LjM2LDQuNjRsMC4wOSwwLjQ3bDEuNzktMC41NmwxLjYxLDIuMjRsMi43Mi0wLjk2bDEuODgsMS40NmwwLjc3LTAuNDRsMi4zMi0zLjY0bC0wLjU5LTAuNTZsLTIuMjktMC4wNmwtMS4xMS0yLjI3TDQ3Mi45MSwxODkuMzhMNDcyLjkxLDE4OS4zOHpcIixcIm5hbWVcIjpcIlN3aXR6ZXJsYW5kXCJ9LFwiY3pcIjp7XCJwYXRoXCI6XCJNNDg4LjQzLDE4NC44N2gyLjk3aDEuNDZsMi4zNywxLjY5bDQuMzktMy42NWwtNC4yNi0zLjA0bC00LjIyLTIuMDRsLTIuODksMC41MmwtMy45MiwyLjUyTDQ4OC40MywxODQuODdMNDg4LjQzLDE4NC44N3pcIixcIm5hbWVcIjpcIkN6ZWNoIFJlcHVibGljXCJ9LFwic2tcIjp7XCJwYXRoXCI6XCJNNDk1Ljg0LDE4Ny4xM2wwLjY5LDAuNjFsMC4wOSwxLjA0bDcuNjMtMC4xN2w1LjY0LTIuNDNsLTAuMDktMi40N2wtMS4wOCwwLjQ4bC0xLjU1LTAuODNsLTAuOTUtMC4wNGwtMi41LDFsLTMuNC0wLjgyTDQ5NS44NCwxODcuMTNMNDk1Ljg0LDE4Ny4xM3pcIixcIm5hbWVcIjpcIlNsb3Zha2lhXCJ9LFwiYXRcIjp7XCJwYXRoXCI6XCJNNDgwLjYzLDE5MC4xMmwtMC42NSwxLjM1bDAuNTYsMC45NmwyLjMzLTAuNDhoMS45OGwyLjE1LDEuODJsNC41Ny0wLjgzbDMuMzYtMmwwLjg2LTEuMzVsLTAuMTMtMS43NGwtMy4wMi0yLjI2bC00LjA1LDAuMDRsLTAuMzQsMi4zbC00LjI2LDIuMDhMNDgwLjYzLDE5MC4xMkw0ODAuNjMsMTkwLjEyelwiLFwibmFtZVwiOlwiQXVzdHJpYVwifSxcImh1XCI6e1wicGF0aFwiOlwiTTQ5Ni43NCwxODkuNmwtMS4xNiwxLjgybDAuMDksMi43OGwxLjg1LDAuOTVsNS42OSwwLjE3bDcuOTMtNi42OGwwLjA0LTEuNDhsLTAuODYtMC40M2wtNS43MywyLjZMNDk2Ljc0LDE4OS42TDQ5Ni43NCwxODkuNnpcIixcIm5hbWVcIjpcIkh1bmdhcnlcIn0sXCJzaVwiOntcInBhdGhcIjpcIk00OTQuOCwxOTEuOTlsLTIuNTQsMS41MmwtNC43NCwxLjA0bDAuOTUsMi43NGwzLjMyLDAuMDRsMy4wNi0yLjU2TDQ5NC44LDE5MS45OUw0OTQuOCwxOTEuOTl6XCIsXCJuYW1lXCI6XCJTbG92ZW5pYVwifSxcImhyXCI6e1wicGF0aFwiOlwiTTQ5NS42MiwxOTUuMTZsLTMuNTMsMi45MWgtMy41OGwtMC40MywyLjUybDEuNjQsMC40M2wwLjgyLTEuMjJsMS4yOSwxLjEzbDEuMDMsMy42bDcuMDcsMy4zbDAuNy0wLjhsLTcuMTctNy40bDAuNzMtMS4zNWw2LjgxLTAuMjZsMC42OS0yLjE3bC00LjQ0LDAuMTNMNDk1LjYyLDE5NS4xNkw0OTUuNjIsMTk1LjE2elwiLFwibmFtZVwiOlwiQ3JvYXRpYVwifSxcImJhXCI6e1wicGF0aFwiOlwiTTQ5NC44LDE5OC45NGwtMC4zNywwLjYxbDYuNzEsNi45MmwyLjQ2LTMuNjJsLTAuMDktMS40M2wtMi4xNS0yLjYxTDQ5NC44LDE5OC45NEw0OTQuOCwxOTguOTR6XCIsXCJuYW1lXCI6XCJCb3NuaWEgYW5kIEhlcnplZ292aW5hXCJ9LFwibXRcIjp7XCJwYXRoXCI6XCJNNDkyLjYxLDIzMC40N2wtMS42NywwLjM0bDAuMDYsMS44NWwxLjUsMC41bDAuNjctMC41Nkw0OTIuNjEsMjMwLjQ3TDQ5Mi42MSwyMzAuNDd6XCIsXCJuYW1lXCI6XCJNYWx0YVwifSxcInVhXCI6e1wicGF0aFwiOlwiTTUxNS41NywxNzMuMTVsLTIuOSwxLjYzbDAuNzIsMy4wOGwtMi42OCw1LjY1bDAuMDIsMi40OWwxLjI2LDAuOGw4LjA4LDAuNGwyLjI2LTEuODdsMi40MiwwLjgxbDMuNDcsNC42M2wtMi41NCw0LjU2bDMuMDIsMC44OGwzLjk1LTQuNTVsMi4yNiwwLjQxbDIuMSwxLjQ2bC0xLjg1LDIuNDRsMi41LDMuOWgyLjY2bDEuMzctMi42bDIuODItMC41N2wwLjA4LTIuMTFsLTUuMjQtMC44MWwwLjE2LTIuMjdoNS4wOGw1LjQ4LTQuMzlsMi40Mi0yLjExbDAuNC02LjY2bC0xMC44LTAuOTdsLTQuNDMtNi4yNWwtMy4wNi0xLjA1bC0zLjcxLDAuMTZsLTEuNjcsNC4xM2wtNy42LDAuMWwtMi40Ny0xLjE0TDUxNS41NywxNzMuMTVMNTE1LjU3LDE3My4xNXpcIixcIm5hbWVcIjpcIlVrcmFpbmVcIn0sXCJtZFwiOntcInBhdGhcIjpcIk01MjAuNzUsMTg3LjcxbDMuMSw0Ljc3bC0wLjI2LDIuN2wxLjExLDAuMDVsMi42My00LjQ1bC0zLjE2LTMuOTJsLTEuNzktMC43NEw1MjAuNzUsMTg3LjcxTDUyMC43NSwxODcuNzF6XCIsXCJuYW1lXCI6XCJNb2xkb3ZhXCJ9LFwicm9cIjp7XCJwYXRoXCI6XCJNNTEyLjE4LDE4Ny42bC0wLjI2LDEuNDhsLTUuNzksNC44Mmw0Ljg0LDcuMWwzLjEsMi4xN2g1LjU4bDEuODQtMS41NGwyLjQ3LTAuMzJsMS44NCwxLjExbDMuMjYtMy43MWwtMC42My0xLjg2bC0zLjMxLTAuODVsLTIuMjYtMC4xMWwwLjExLTMuMThsLTMtNC43Mkw1MTIuMTgsMTg3LjZMNTEyLjE4LDE4Ny42elwiLFwibmFtZVwiOlwiUm9tYW5pYVwifSxcInJzXCI6e1wicGF0aFwiOlwiTTUwNS41NSwxOTQuNTRsLTIuMDUsMS41NGgtMWwtMC42OCwyLjEybDIuNDIsMi44MWwwLjE2LDIuMjNsLTMsNC4yNGwwLjQyLDEuMjdsMS43NCwwLjMybDEuMzctMS44NmwwLjc0LTAuMDVsMS4yNiwxLjIybDMuODQtMS4xN2wtMC4zMi01LjQ2TDUwNS41NSwxOTQuNTRMNTA1LjU1LDE5NC41NHpcIixcIm5hbWVcIjpcIlNlcmJpYVwifSxcImJnXCI6e1wicGF0aFwiOlwiTTUxMS40NCwyMDIuMzlsMC4xNiw0Ljk4bDEuNjgsMy41bDYuMzEsMC4xMWwyLjg0LTIuMDFsMi43OS0xLjExbC0wLjY4LTMuMThsMC42My0xLjdsLTEuNDItMC43NGwtMS45NSwwLjE2bC0xLjUzLDEuNTRsLTYuNDIsMC4wNUw1MTEuNDQsMjAyLjM5TDUxMS40NCwyMDIuMzl6XCIsXCJuYW1lXCI6XCJCdWxnYXJpYVwifSxcImFsXCI6e1wicGF0aFwiOlwiTTUwNC4wMiwyMDkuNzZ2NC42MWwxLjMyLDIuNDlsMC45NS0wLjExbDEuNjMtMi45N2wtMC45NS0xLjMzbC0wLjM3LTMuMjlsLTEuMjYtMS4xN0w1MDQuMDIsMjA5Ljc2TDUwNC4wMiwyMDkuNzZ6XCIsXCJuYW1lXCI6XCJBbGJhbmlhXCJ9LFwibWtcIjp7XCJwYXRoXCI6XCJNNTEwLjkyLDIwOC4wMWwtMy4zNywxLjExbDAuMTYsMi44NmwwLjc5LDEuMDFsNC0xLjg2TDUxMC45MiwyMDguMDFMNTEwLjkyLDIwOC4wMXpcIixcIm5hbWVcIjpcIk1hY2Vkb25pYVwifSxcImdyXCI6e1wicGF0aFwiOlwiTTUwNi43MSwyMTcuNmwtMC4xMSwxLjMzbDQuNjMsMi4zM2wyLjIxLDAuODVsLTEuMTYsMS4yMmwtMi41OCwwLjI2bC0wLjM3LDEuMTdsMC44OSwyLjAxbDIuODksMS41NGwxLjI2LDAuMTFsMC4xNi0zLjQ1bDEuODktMi4yOGwtNS4xNi02LjFsMC42OC0yLjA3bDEuMjEtMC4wNWwxLjg0LDEuNDhsMS4xNi0wLjU4bDAuMzctMi4wN2w1LjQyLDAuMDVsMC4yMS0zLjE4bC0yLjI2LDEuNTlsLTYuNjMtMC4xNmwtNC4zMSwyLjIzTDUwNi43MSwyMTcuNkw1MDYuNzEsMjE3LjZNNTE2Ljc2LDIzMC41OWwxLjYzLDAuMDVsMC42OCwxLjAxaDIuMzdsMS41OC0wLjU4bDAuNTMsMC42NGwtMS4wNSwxLjM4bC00LjYzLDAuMTZsLTAuODQtMS4xMWwtMC44OS0wLjUzTDUxNi43NiwyMzAuNTlMNTE2Ljc2LDIzMC41OXpcIixcIm5hbWVcIjpcIkdyZWVjZVwifX19KTtcclxuIl19
