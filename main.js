var cldrp = require('./src/lib/CLDRPluralRuleParser/CLDRPluralRuleParser.js');

var jqueryi18n = require('./src/lib/jquery.i18n/jquery.i18n.js');

var messagestore = require('./src/lib/jquery.i18n/jquery.i18n.messagestore.js');

var fallbacks = require('./src/lib/jquery.i18n/jquery.i18n.fallbacks.js');

var language = require('./src/lib/jquery.i18n/jquery.i18n.language.js');

var parser = require('./src/lib/jquery.i18n/jquery.i18n.parser.js');

var emitter = require('./src/lib/jquery.i18n/jquery.i18n.emitter.js');

var emitterbidi = require('./src/lib/jquery.i18n/jquery.i18n.emitter.bidi.js');

var history = require('./src/lib/history/jquery.history.js');

var url = require('./src/lib/url/url.min.js');

var languagePicker = require('./src/languagePicker.js');

var jsonLoader = require('./src/jsonLoader.js');

var gamePresenter = require('./src/gamePresenter.js');

var blockrainlibs = require('./src/blockrain.jquery.libs.js');

var blockrain = require('./src/blockrain.jquery.src.js');

var blockrainthemes = require('./src/blockrain.jquery.themes.js');